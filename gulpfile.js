var gulp = require('gulp');
var watch = require('gulp-watch');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var imagemin = require('gulp-imagemin');
var minCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var browserSync = require('browser-sync');
var clean = require('gulp-clean');
var plumber = require('gulp-plumber');

var config = {
    proxy: 'http://thelandsite.test/', // change it to your development domain
    img: {
        jpg: '/**/*.jpg',
        png: '/**/*.png',
        gif: '/**/*.gif',
        jpeg: '/**/*.jpeg',
        svg: '/**/*.svg',
        ico: '/**/*.ico'
    },
    sync: ['**/*.php', '**/*.ctp', 'webroot/*.html', '**/*.css'],
    /**
     * Front end
     */
    front: {
        css: [
            '!./webroot/src/scss/bootstrap/bootstrap-reboot.scss',
            '!./webroot/src/scss/bootstrap/bootstrap-grid.scss',
            '!./webroot/src/scss/fontawesome/brands.scss',
            '!./webroot/src/scss/fontawesome/fontawesome.scss',
            '!./webroot/src/scss/fontawesome/regular.scss',
            '!./webroot/src/scss/fontawesome/solid.scss',
            '!./webroot/src/scss/fontawesome/v4-shims.scss',
            '!./webroot/src/scss/slick/slick-theme.scss',
            '!./webroot/src/scss/slick/slick.scss',
            './webroot/src/scss/style.scss'
        ],
        js: [
            './webroot/src/js/jquery.js',
            './webroot/src/js/popper.js',
            './webroot/src/js/bootstrap.js',
            './webroot/src/js/slick.js',
            './webroot/src/js/slick-lightbox.js',
            './webroot/src/js/select2.full.js',
            './webroot/src/js/translate.js',
            './webroot/src/js/loading.js',
            './webroot/src/js/property-attributes.js',
            './webroot/src/js/main.js'
        ],
        img: './webroot/src/img',
        imgCompress: ['./webroot/src/img/**/*.jpg', './webroot/src/img/**/*.png'],
        dest: {
            css: './webroot/css',
            js: './webroot/js',
            img: './webroot/img',
        }
    },
    /**
     * Panel
     */
    panel: {
        css: [
            '!./webroot/src/panel/scss/bootstrap/bootstrap-reboot.scss',
            '!./webroot/src/panel/scss/bootstrap/bootstrap-grid.scss',
            '!./webroot/src/panel/scss/modules/mixins.scss',
            './webroot/src/panel/scss/**/*.scss'
        ],
        js: [
            './webroot/src/panel/js/popper.js',
            './webroot/src/panel/js/bootstrap.bundle.js',
            './webroot/src/panel/js/material.js',
            './webroot/src/panel/js/color-admin.js',
            './webroot/src/panel/js/csrf.js',
            './webroot/src/panel/js/translate.js',
            './webroot/src/panel/js/loading.js',
            './webroot/src/panel/js/notifications.js',
            './webroot/src/panel/js/swal.js',
            './webroot/src/panel/js/formBuilder.js',
            './webroot/src/panel/js/lock-screen.js',
            './webroot/src/panel/js/switcher.js',
            './webroot/src/panel/js/select2.js',
            './webroot/src/panel/js/xeditable.js',
            './webroot/src/panel/js/datetime.js',
            './webroot/src/panel/js/datepicker.js',
            './webroot/src/panel/js/magnific-popup.js',
            './webroot/src/panel/js/code-mirror.js',
            './webroot/src/panel/js/page-files.js',
            './webroot/src/panel/js/custom-css.js',
            './webroot/src/panel/js/cropper.js',
            './webroot/src/panel/js/dashboard.js',
            './webroot/src/panel/js/property-attributes.js',
            './webroot/src/panel/js/contain.js',
            './webroot/src/panel/js/dropzone.js',
            './webroot/src/panel/js/acl.js',
            './webroot/src/panel/js/approve-reject-article.js',
            './webroot/src/panel/js/approve-reject-member.js',
            './webroot/src/panel/js/approve-reject-property.js',
            './webroot/src/panel/js/approve-reject-service.js',
            './webroot/src/panel/js/generate.js',
            './webroot/src/panel/js/main.js',
            './webroot/src/panel/js/discounts.js',
            './webroot/src/panel/js/reports.js'
        ],
        img: './webroot/src/panel/img',
        imgCompress: ['./webroot/src/panel/img/**/*.jpg', './webroot/src/panel/img/**/*.png'],
        dest: {
            css: './webroot/panel/css',
            js: './webroot/panel/js',
            img: './webroot/panel/img',
        }
    },
    /**
     * Cake related
     */
    cake: {
        models: './src/Model/**/*.php'
    }
}

/**
 * Removes all files inside `tmp` directory
 */
gulp.task('clean-tmp', function () {
    console.log('Cleaning tmp...');
    return gulp.src('./tmp/*')
            .pipe(clean({force: false}));
});

/**
 * Removes all images declared in `dstImages`
 */
gulp.task('clean-images', function () {
    console.log('Cleaning images...');
    return gulp.src(config.front.dest.imgCompress)
            .pipe(clean({force: false}));
});

/**
 * Compiles Sass files for front-end use
 */
gulp.task('build-css', function () {
    gulp.src(config.front.css)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'expanded'
        }).on('error', sass.logError))
        .pipe(sourcemaps.init({
            loadMaps: true
        }))
        .pipe(autoprefixer({
            browsers: ['last 5 versions'],
            cascade: false
         }))
        .pipe(gulp.dest(config.front.dest.css))
        .pipe(minCss())
        .pipe(rename({extname: '.min.css'}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.front.dest.css))
});

/**
 * Compiles Sass files for back-end use
 */
gulp.task('build-css-panel', function () {
    gulp.src(config.panel.css)
            .pipe(sourcemaps.init())
            .pipe(plumber())
            .pipe(sass().on('error', sass.logError))
            .pipe(autoprefixer())
            .pipe(gulp.dest(config.panel.dest.css))
            .pipe(minCss())
            .pipe(sourcemaps.write())
            .pipe(rename({extname: '.min.css'}))
            .pipe(gulp.dest(config.panel.dest.css))
});

/**
 * Compiles JS files for front-end use
 */
gulp.task('build-js', function () {
    return gulp.src(config.front.js)
            .pipe(plumber())
            .pipe(concat('app.js'))
            .pipe(gulp.dest(config.front.dest.js))
            .pipe(rename('app.min.js'))
            .pipe(uglify())
            .pipe(gulp.dest(config.front.dest.js));
});

/**
 * Compiles JS files for back-end use
 */
gulp.task('build-js-panel', function () {
    return gulp.src(config.panel.js)
            .pipe(plumber())
            .pipe(concat('app.js'))
            .pipe(gulp.dest(config.panel.dest.js))
            .pipe(rename('app.min.js'))
            .pipe(uglify())
            .pipe(gulp.dest(config.panel.dest.js));
});

/**
 * Browser refresh on declared files change
 */
gulp.task('browser-sync', function () {
    browserSync.init({
        files: config.sync,
        proxy: config.proxy
    });
});

/**
 * Copies images from src to dst location
 */
gulp.task('images', function () {
    let frontImages = new Array();
    for (let index in config.img) {
        frontImages.push(config.front.img + config.img[index])
    }
    gulp.src(frontImages)
            .pipe(plumber())
            .pipe(gulp.dest(config.front.dest.img));
});

/**
 * Copies images from panel src to dst location
 */
gulp.task('images-panel', function () {
    let panelImages = new Array();
    for (let index in config.img) {
        panelImages.push(config.panel.img + config.img[index])
    }
    gulp.src(panelImages)
            .pipe(plumber())
            .pipe(gulp.dest(config.panel.dest.img));
});

/**
 * Removes dst images, copresses and copies images from src to dst location
 */
gulp.task('images-compress', function () {
    gulp.start('clean-images');
    gulp.src(config.front.imgCompress)
            .pipe(plumber())
            .pipe(imagemin({optimizationLevel: 5, progressive: true, interlaced: true}))
            .pipe(gulp.dest(config.front.dest.img));
});

/**
 * Watch models and cleans tmp directory if something changed
 */
gulp.task('watch-models', function () {
    return watch(config.cake.models, function () {
        gulp.start('clean-tmp');
    });
});

/**
 * Watch images and copies images from src to dst directory
 */
gulp.task('watch-images', function () {
    return watch(config.front.img, function () {
        gulp.start('images');
    });
});

/**
 * Watch images and copies images from panel src to dst directory
 */
gulp.task('watch-images-panel', function () {
    return watch(config.panel.img, function () {
        gulp.start('images-panel');
    });
});

/**
 * Main watcher
 */
gulp.task('watcher', function () {
    gulp.watch(config.front.css, ['build-css']).on('change', function (evt) {
        console.log('[watcher] File ' + evt.path.replace(/.*(?=scss)/, '') + ' was ' + evt.type + ', compiling...');
    });
    gulp.watch(config.front.js, ['build-js']);
    gulp.watch(config.panel.js, ['build-js-panel']);
    gulp.watch(config.panel.css, ['build-css-panel']);
    gulp.start('watch-images');
    gulp.start('watch-images-panel');
    gulp.start('watch-models');
});

/**
 * Default task - front end only
 */
gulp.task('default', ['build-css', 'build-js', 'images', 'watcher']);

/**
 * Maint task for frontend devs
 */
gulp.task('frontend', ['build-css', 'build-js', 'browser-sync', 'images', 'watcher']);

/**
 * Main task for compliling everything
 */
gulp.task('all', ['build-css', 'build-js', 'build-js-panel', 'build-css-panel', 'images', 'watcher']);
