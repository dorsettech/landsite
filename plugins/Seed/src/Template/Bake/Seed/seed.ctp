<%

/* public_html/vendor/cakephp/migrations/src/Template/Bake/Seed */

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

if (!empty($records)) {
    $records = str_replace([
        '=> TRUE,',
        '=> FALSE,',
        '=> NULL,',
    ], [
        '=> true,',
        '=> false,',
        '=> null,',
    ], $records);

    if (isset($_SERVER['OS']) && stripos($_SERVER['OS'], 'windows') !== false) {
        $records = explode("\n", $records);
    } else {
        $records = explode(PHP_EOL, $records);
    }

    $nowFlag = false;
    foreach ($records as $key => $value) {
        if (stristr($value, '\'created\' => ') !== FALSE) {
            $records[$key] = '                \'created\' => $now,';
            $nowFlag = true;
        } elseif (stristr($value, '\'modified\' => ') !== FALSE) {
            $records[$key] = '                \'modified\' => $now,';
            $nowFlag = true;
        } elseif (stristr($value, '\'modified_at\' => ') !== FALSE) {
            $records[$key] = '                \'modified_at\' => $now,';
            $nowFlag = true;
        } elseif (stristr($value, '\'created_at\' => ') !== FALSE) {
            $records[$key] = '                \'created_at\' => $now,';
            $nowFlag = true;
        }
    }

    $records = implode(PHP_EOL, $records);
}

%>
<?php

/**
 * @author eConnect4u
 * @date (<%= date('Y-m-d') %>)
 * @version 1.0
 */

use Migrations\AbstractSeed;

/**
 * <%= $name %> seed.
 */
class <%= $name %>Seed extends AbstractSeed
{
    /**
     * Run Method
     *
     * @return void
     */
    public function run()
    {
<% if ($nowFlag==true): %>
        $now = GlobalSeed::getCurrentDate();
<% endif; %>
<% if ($records): %>
        $data = <%= $records %>;
<% else: %>
        $data = [];
<% endif; %>
        GlobalSeed::truncateTable($this, '<%= $table %>', $data);
    }
}
