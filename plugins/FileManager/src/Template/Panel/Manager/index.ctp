<?= $this->element('ajax_loading'); ?>
<?php
$this->Form->templates([
    'checkbox'          => '<input type="checkbox" name="{{name}}" value="{{value}}"{{attrs}}>',
    'checkboxContainer' => '{{content}}',
    'nestingLabel'      => '{{input}}'
]);
?>
<div class="row page-wrapper file-manager">
    <div class="col-12 col-sm-4 col-lg-3">

        <div class="file-manager-sidebar">
            <div class="btn-group btn-block mb-3">
                <span class="btn btn-light"><?= __('Filter'); ?>:</span>
                <?= $this->Html->link(__('All'), ['action' => 'index', 'type' => null, 'path' => $dir, 'CKEditorFuncNum' => $CKEditorFuncNum], ['class' => 'btn btn-light file-control' . ((!$type) ? ' active' : ''), 'escape' => false, 'title' => __('All'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']); ?>
                <?= $this->Html->link('<i class="fas fa-images"></i>', ['action' => 'index', 'type' => 'images', 'path' => $dir, 'CKEditorFuncNum' => $CKEditorFuncNum], ['class' => 'btn btn-light file-control' . (($type == 'images') ? ' active' : ''), 'escape' => false, 'title' => __('Images'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']); ?>
                <?= $this->Html->link('<i class="far fa-file-alt"></i>', ['action' => 'index', 'type' => 'files', 'path' => $dir, 'CKEditorFuncNum' => $CKEditorFuncNum], ['class' => 'btn btn-light file-control' . (($type == 'files') ? ' active' : ''), 'escape' => false, 'title' => __('Documents'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']); ?>
                <?= $this->Html->link('<i class="fas fa-music"></i>', ['action' => 'index', 'type' => 'audio', 'path' => $dir, 'CKEditorFuncNum' => $CKEditorFuncNum], ['class' => 'btn btn-light file-control' . (($type == 'audio') ? ' active' : ''), 'escape' => false, 'title' => __('Music'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']); ?>
            </div>

            <div class="mb-3">
                <?= $this->Html->link('<i class="fa fa-folder-plus"></i> ' . __('New folder'), ['action' => 'addFolder', 'CKEditorFuncNum' => $CKEditorFuncNum], ['class' => 'btn btn-primary btn-block', 'data-ajax-modal', 'escape' => false]); ?>
            </div>

            <?php if ($dir): ?>
                <div class="mb-3">
                    <?= $this->Form->postLink('<i class="fa fa-trash"></i> ' . __('Delete {0}', '<i>' . $dir . '</i>'), ['action' => 'deleteFolder', $dir, 'CKEditorFuncNum' => $CKEditorFuncNum], ['class' => 'btn btn-outline-danger btn-block', 'confirm' => __('Please confirm'), 'data-text' => __('Are you sure you want to delete folder {0}? Please note it will be removed only if there are no files.', $dir), 'escape' => false]); ?>
                </div>
            <?php endif; ?>

            <div class="list-group list-group-light folder-list mb-3">
                <?= $this->Html->link(((!$dir) ? '<i class="fa fa-folder-open"></i>' : '<i class="fa fa-folder"></i>') . ' ' . __('Global'), ['action' => 'index', 'CKEditorFuncNum' => $CKEditorFuncNum], ['class' => 'list-group-item file-control' . ((!$dir) ? ' active' : ''), 'escape' => false]); ?>
                <?php foreach ($dirs as $row): ?>
                    <?= $this->Html->link((($row == $dir) ? '<i class="fa fa-folder-open"></i>' : '<i class="fa fa-folder"></i>') . ' ' . ucfirst($row), ['action' => 'index', 'path' => $row, 'CKEditorFuncNum' => $CKEditorFuncNum], ['class' => 'list-group-item file-control' . (($row == $dir) ? ' active' : ''), 'escape' => false]); ?>
                <?php endforeach; ?>
            </div>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th colspan="2" class="tag-title"><?= __('Stats') ?></th>
                    </tr>
                </thead>
                <tr>
                    <td><?= __('No. of files'); ?></td>
                    <td><b><?= $countFiles; ?></b></td>
                </tr>
                <tr>
                    <td><?= __('All files size'); ?></td>
                    <td><b><?= $dirSize; ?></b></td>
                </tr>
            </table>
        </div>

    </div>
    <div class="col-12 col-sm-8 col-lg-9">

        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <?= $this->Html->link('<i class="fa fa-plus"></i>', ['action' => 'upload', $dir], ['class' => 'btn btn-xs btn-icon btn-circle btn-success', 'data-ajax-modal', 'escape' => false]); ?>
                </div>
                <h4 class="panel-title">
                    <?= __('File manager') ?>
                    <?php if ($dir): ?>
                        / <?= $dir ?>
                    <?php endif; ?>
                </h4>
            </div>
            <div class="panel-body">

                <?= $this->Form->create(null, ['url' => ['action' => 'deleteFiles', 'CKEditorFuncNum' => $CKEditorFuncNum]]); ?>
                <?= $this->Form->control('dir', ['type' => 'hidden', 'value' => $dir, 'id' => 'dir']); ?>
                
                <div class="row action-buttons">
                    <div class="col-12 text-right">
                        <div class="btn-group">
                            <?= $this->Form->button('<i class="fa fa-trash"></i> ' . __('Delete files'), ['type' => 'submit', 'class' => 'btn btn-danger btn-delete button-delete d-none', 'escape' => false, 'data-title' => __('Please confirm'), 'data-text' => __('Are you sure you want to delete files?')]); ?>
                            <?= $this->Form->button('<i class="far fa-check-square"></i> ' . __('Select all'), ['type' => 'button', 'class' => 'btn btn-light toggle-checkboxes', 'data-select' => true, 'data-class' => 'checkbox-delete']) ?>
                            <?= $this->Html->link('<i class="fa fa-upload"></i> ' . __('Upload files'), ['action' => 'upload', $dir], ['data-ajax-modal', 'data-init' => 'dropzone', 'class' => 'btn btn-primary ', 'escape' => false]); ?>
                        </div>
                    </div>
                </div>

                <?php if (empty($files)) : ?>
                    <div class="middle-box text-center">
                        <h3><?= __('Folder {0} is empty.', '<i>' . (!empty($dir) ? $dir : __('Global')) . '</i>') ?></h3>
                    </div>
                <?php endif; ?>
                <div class="files-list">
                <?php foreach ($files as $file): ?>
                    <?= $this->element('file_box', ['file' => $file]); ?>
                <?php endforeach; ?>
                </div>

                <?= $this->Form->end(); ?>

            </div>
        </div>

    </div>
</div>

<?= $this->Html->scriptStart(['block' => 'scriptBottom']); ?>
var upload = <?= json_encode($upload) ?>;
<?= $this->Html->scriptEnd() ?>

<?= $this->Html->css('FileManager.file-manager.css', ['block' => 'cssHead']); ?>
<?= $this->Html->css('FileManager.dropzone/dropzone.css', ['block' => 'cssHead']); ?>

<?= $this->Html->script('FileManager.dropzone/dropzone.js', ['block' => 'scriptBottom']); ?>
<?= $this->Html->script('FileManager.file-manager.js', ['block' => 'scriptBottom']); ?>
<?= $this->Html->script('FileManager.file-rename.js', ['block' => 'scriptBottom']); ?>
<?= $this->Html->script('FileManager.overwrite-file-name.js', ['block' => 'scriptBottom']); ?>
<?= $this->Html->script('FileManager.confirm-delete.js', ['block' => 'scriptBottom']); ?>
