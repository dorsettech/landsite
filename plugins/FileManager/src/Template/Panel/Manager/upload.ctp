<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title"><?= __('Upload') ?></h4>
            <?= $this->Form->button('&times;', ['type' => 'button', 'class' => 'close btn-link', 'data-dismiss' => 'modal']) ?>
        </div>
        <div class="modal-body">
            <form action="#" class="dropzone" id="dropzoneForm">
                <div class="fallback">
                    <?= $this->Form->control('file', ['type' => 'file', 'multiple' => 'multiple']) ?>
                </div>
            </form>
            <div class="row mt-2">
                <div class="col-12 col-md-6 col-lg-4">
                    <?= $this->Form->control('overwrite_file_name', ['type' => 'checkbox', 'label' => __('Overwrite file names'), 'value' => '1', 'data-title' => __('Please confirm overwriting'), 'data-text' => __('Are you sure you want to enable files overwrite? Files with the same name will be overwritten by new files uploaded.')]) ?>
                </div>
                <div class="col-12 col-md-6 col-lg-8 text-right">
                    <?= __('Maximum file size is {0}MB', $upload['max_file_size']) ?>
                </div>
            </div>
        </div>
        <div class="modal-footer d-block">
            <div class="row">
                <div class="col-6">
                    <?= $this->Form->button(__('Cancel'), ['type' => 'button', 'class' => 'btn btn-warning', 'data-dismiss' => 'modal', 'escape' => false]); ?>
                </div>
                <div class="col-6 text-right">
                    <?= $this->Form->button('<i class="fa fa-upload"></i> ' . __('Upload files'), ['type' => 'button', 'id' => 'dropzone-upload', 'class' => 'btn btn-primary', 'escape' => false]); ?>
                </div>
            </div>
        </div>
    </div>
</div>

