<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title"><?= __('New Folder') ?></h4>
            <?= $this->Form->button('&times;', ['type' => 'button', 'class' => 'close btn-link', 'data-dismiss' => 'modal']) ?>
        </div>
        <?= $this->Form->create(null) ?>
        <div class="modal-body">
            <?= $this->Form->control('name', ['maxlength' => 30, 'required' => true]); ?>
        </div>
        <div class="modal-footer d-block">
            <div class="row">
                <div class="col-6">
                    <?= $this->Form->button(__('Cancel'), ['type' => 'button', 'class' => 'btn btn-warning', 'data-dismiss' => 'modal', 'escape' => false]); ?>
                </div>
                <div class="col-6 text-right">
                    <?= $this->Form->button(__('Add New Folder'), ['type' => 'submit', 'class' => 'btn btn-primary', 'escape' => false]); ?>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>