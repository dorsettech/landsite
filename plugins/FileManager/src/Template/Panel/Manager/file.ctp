<?php
    $path = str_replace("\\", "/", $path);
?>
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title"><?= __('File: {0}', $file['basename']) ?></h4>
            <?= $this->Form->button('&times;', ['type' => 'button', 'class' => 'close btn-link', 'data-dismiss' => 'modal']) ?>
        </div>
        <div class="modal-body">
            <div class="row ">
                <div class="col-sm-4 file-view">
                    <?php if (in_array($file['extension'], ['jpg', 'jpeg', 'png', 'gif'])): ?>
                        <div class="image">
                            <div class="preview" style="background-image: url(<?= $this->Images->scale($path, 300, 300, 'ratio') ?>);"></div>
                        </div>
                    <?php elseif (in_array($file['extension'], ['pdf', 'doc', 'docx', 'xls'])): ?>
                        <div class="icon">
                            <i class="fa fa-file"></i>
                        </div>
                    <?php elseif (in_array($file['extension'], ['mp3', 'wav', 'mp4'])): ?>
                        <div class="icon">
                            <i class="fa fa-music"></i>
                        </div>
                    <?php else: ?>
                        <div class="icon">
                            <i class="fa fa-file-code-o"></i>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="col-sm-8">
                    <table class="table table-striped">
                        <tr>
                            <td width="80"><?= __('Name') ?></td>
                            <td class="file-basename"><?= $file['basename']; ?></td>
                        </tr>
                        <tr>
                            <td><?= __('Type') ?></td>
                            <td class="text-uppercase"><?= $file['extension']; ?></td>
                        </tr>
                        <tr>
                            <td><?= __('Size') ?></td>
                            <td><?= $file['size']; ?></td>
                        </tr>
                        <tr>
                            <td><?= __('Created') ?></td>
                            <td><span class="badge badge-light"><i class="far fa-clock"></i> <?= $file['date']; ?></span>
                            </td>
                        </tr>
                    </table>
                    <div class="row">
                        <div class="col-sm-8">
                            <input type="text" id="org-name" readonly class="form-control" value="/<?= $path; ?>" width="100%">
                        </div>
                        <div class="col-sm-4">
                            <?= $this->Form->button('<i class="far fa-copy"></i> ' . __('Copy to clipboard'), ['type' => 'button', 'class' => 'btn btn-primary btn-block copy-to-clipboard', 'data-target' => '#org-name']) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <?= $this->Form->control('new_name', ['label' => '', 'value' => $file['filename'], 'placeholder' => __('File name') ]) ?>
                        </div>
                        <div class="col-sm-4 d-flex align-items-center">
                            <?= $this->Form->control('overwrite_file_name', ['type' => 'checkbox', 'label' => __('Overwrite file name'), 'value' => '1', 'data-title' => __('Please confirm overwriting'), 'data-text' => __('Are you sure you want to enable file overwrite? File with the same name will be replaced by this file.')]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 offset-sm-4">
                            <?= $this->Html->link('<i class="fas fa-crop"></i> ' . __('Crop image'), ['plugin' => null, 'prefix' => 'panel', 'controller' => 'Cropper', 'action' => 'crop-file', 'path' => $path], ['class' => 'btn btn-info btn-block', 'escape' => false]) ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $this->Form->button('<i class="fas fa-edit"></i> ' . __('Rename'), ['type' => 'button', 'class' => 'btn btn-primary btn-block btn-file-rename' ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer d-block">
            <div class="row">
                <div class="col-6">
                    <?= $this->Form->button(__('Cancel'), ['type' => 'button', 'class' => 'btn btn-warning', 'data-dismiss' => 'modal', 'escape' => false]); ?>
                </div>
                <div class="col-6 text-right">
                    <?= $this->Html->link('<i class="fas fa-trash-alt"></i> ' . __('Delete'), ['action' => 'deleteFile', 'path' => $file['path'], 'dir' => $file['dir']], ['class' => 'btn btn-outline-danger btn-delete', 'data-title' => __('Are you sure?'),  'data-text' => __('Are you sure you want to delete files?'), 'escape' => false]); ?>
                    <?= $this->Html->link('<i class="fas fa-external-link-alt"></i> ' . __('Open in new tab'), '/' . $path, ['class' => 'btn btn-success', 'target' => '_blank', 'escape' => false]); ?>
                </div>
            </div>
        </div>
    </div>
</div>