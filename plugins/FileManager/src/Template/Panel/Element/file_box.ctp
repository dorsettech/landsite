<?php 
    $path = str_replace("\\", "/", $basePath . $DS . $file['basename']);
    $absPath = str_replace("\\", "/", $DS . $path);
?>
<div class="file-box">
    <div class="file" data-file="<?= $file['basename'] ?>">
        <?= $this->Form->control('delete[' . base64_encode($path) . ']', ['class' => 'checkbox-delete', 'type' => 'checkbox']); ?>
        <a <?php if ($CKEditorFuncNum) : ?>data-path="<?= $absPath ?>"<?php endif; ?>
           href="<?= $this->Url->build(['action' => 'file', 'path' => $absPath, 'dir' => $dir]); ?>"
           data-ajax-modal>
            <span class="corner"></span>
            <?php if (in_array($file['extension'], ['jpg', 'jpeg', 'png', 'gif'])): ?>
                <div class="image">
                    <div class="preview" style="background-image: url(<?= $this->Images->scale($path, 220, 100, 'ratio_crop') ?>);"></div>
                </div>
            <?php elseif (in_array($file['extension'], ['pdf', 'doc', 'docx', 'xls'])): ?>
                <div class="icon"><i class="fa fa-file"></i></div>
            <?php elseif (in_array($file['extension'], ['mp3', 'wav', 'mp4'])): ?>
                <div class="icon"><i class="fa fa-music"></i></div>
            <?php else: ?>
                <div class="icon"><i class="fa fa-file-code-o"></i></div>
            <?php endif; ?>
            <div class="file-name">
                <span>
                    <?=
                        $this->Text->truncate(
                            $file['basename'],
                            20,
                            [
                                'ellipsis' => '(...).' . $file['extension'],
                                'exact' => false
                            ]
                        );
                    ?>
                </span><br/>
                <small><?= __('Added') ?>: <?= $file['date']; ?></small>
            </div>
        </a>
    </div>
</div>
