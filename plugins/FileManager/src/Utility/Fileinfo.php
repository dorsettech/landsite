<?php
/**
 * @author Jakub Gałyga <biuro@ninjacode.pl>
 * @date (2018-11-02)
 * @version 1.0
 */
namespace FileManager\Utility;

use Cake\Core\Configure;

/**
 * Class Fileinfo
 */
class Fileinfo
{

    /**
     * Returns an array with information about files.
     *
     * @param array       $files Array with files.
     * @param string|null $dir   Dir name.
     * @return array
     */
    public function getFilesInfo(array $files = [], $dir = '')
    {
        $output = [];
        foreach ($files as $key => $file) {
            $output[$key]         = $this->getInfo($file);
            $output[$key]['date'] = date(Configure::read('date_format'), filemtime(Configure::read('path') . DS . $dir . DS . $file));
        }
        return $output;
    }

    /**
     * Returns information about the path to the file
     *
     * @param string|null $file Path to string.
     * @return array
     */
    public function getInfo($file = '')
    {
        return pathinfo($file);
    }

    /**
     * Return string with count bytes and Extension
     *
     * @param integer|null $bytes Count bytes.
     * @return string
     */
    public function formatSizeUnits(int $bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }
}
