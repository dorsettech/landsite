<?php
/**
 * @author Jakub Gałyga <biuro@ninjacode.pl>
 * @date (2018-11-02)
 * @version 1.0
 */
namespace FileManager\Controller;

use App\Controller\AppController as BaseController;

/**
 * Class AppController
 */
class AppController extends BaseController
{

}
