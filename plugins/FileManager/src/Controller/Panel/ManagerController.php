<?php
/**
 * @author  Jakub Gałyga <biuro@ninjacode.pl>
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-23)
 * @version 1.0
 */

namespace FileManager\Controller\Panel;

use App\Controller\Panel\PanelController;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\I18n\Time;
use Cake\Network\Exception\InternalErrorException;
use Cake\Routing\Router;
use Cake\Utility\Text;
use Exception;
use FileManager\Utility\Fileinfo;

/**
 * Manager Controller
 *
 * @description File manager module.
 */
class ManagerController extends PanelController
{
    /**
     * Path to main folder with files
     *
     * @var string
     */
    private $path;

    /**
     * Path to folder trash
     *
     * @var string
     */
    private $trashPath;

    /**
     * Upload variable set for JS
     *
     * @var array
     */
    protected $upload = [];

    /**
     * Initialize method
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->setHeading('title', __('Media Library'));

        Configure::load('FileManager.config');
        $this->path      = WWW_ROOT . Configure::read('path');
        $this->trashPath = $this->path . DS . 'trash';

        $CKEditorFuncNum = $this->getRequest()->getQuery('CKEditorFuncNum');
        $this->set('CKEditorFuncNum', $CKEditorFuncNum);
        $this->set('DS', DS);

        if (!empty($ckEditorFuncNum)) {
            $this->setLayout('clean-media');
        }
    }

    /**
     * Before render event
     *
     * @param Event $event Event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);

        $maxFileSize                    = Configure::read('max_file_size');
        $acceptedFiles                  = implode(',', Configure::read('allowed_extensions'));
        $this->upload['max_file_size']  = $maxFileSize;
        $this->upload['accepted_files'] = $acceptedFiles;

        $this->set('upload', $this->upload);
    }

    /**
     * Index method
     *
     * @description Media library
     * @return Response|void
     */
    public function index()
    {
        $ckEditorFuncNum = $this->getRequest()->getQuery('CKEditorFuncNum');
        
        $this->setLayout('clean');

        $filesinfo = new Fileinfo();
        $dir       = null;
        $type      = null;
        $path      = $this->path;

        $basePath = Configure::read('path');
        if ($this->getRequest()->getQuery('path')) {
            $path     = $this->path . DS . $this->getRequest()->getQuery('path');
            $dir      = $this->getRequest()->getQuery('path');
            $basePath = Configure::read('path') . DS . $this->getRequest()->getQuery('path');
        }

        if (!file_exists($path)) {
            $this->Flash->error(__('Folder does not exist.'));
            return $this->redirect(['action' => 'index', 'CKEditorFuncNum' => $this->getRequest()->getQuery('CKEditorFuncNum')]);
        }

        $folder = new Folder($path);

        $dirSize = $filesinfo->formatSizeUnits($folder->dirsize());

        $global = new Folder($this->path);
        $dirs   = $global->read(true, Configure::read('hidden_folder'))[0];

        if ($this->getRequest()->getQuery('type') && Configure::read($this->getRequest()->getQuery('type'))) {
            $type  = $this->getRequest()->getQuery('type');
            $files = $folder->find('.*\.(' . Configure::read($this->getRequest()->getQuery('type')) . ')');
        } else {
            $files = $folder->find();
        }

        $this->upload['action'] = Router::url(['action' => 'upload', $dir]);
        $dirName                = !empty($dir) ? $dir : __('Global');
        $this->setHeading('subtitle', $dirName);

        $countFiles = count($files);
        $files      = $filesinfo->getFilesInfo($files, $dir);
        $this->set(compact('files', 'dirs', 'dir', 'basePath', 'dirSize', 'countFiles', 'type', 'funcNum'));
    }

    /**
     * Upload method
     *
     * @description Upload files modal
     * @param string|null $folder Folder.
     * @return void
     * @throws InternalErrorException InternalErrorException.
     */
    public function upload($folder = null)
    {
        $this->setLayout('modal');

        if ($this->getRequest()->is('post')) {
            $this->autoRender = false;

            $overwriteName = filter_var($this->getRequest()->getData("overwrite-file-name"), FILTER_VALIDATE_BOOLEAN);

            $pathToCurrentFolder = $this->connectPaths($this->path, $folder);

            if ($this->getRequest()->getData('file')) {
                $file = $this->getRequest()->getData('file');

                $upload                = new \upload($file);
                $upload->mime_check    = true;
                $upload->file_max_size = (Configure::read('max_file_size') * 1024) * 1024;
                $upload->allowed       = Configure::read('allowed_extensions');

                $nameAndExt = $upload->file_src_name;
                $newName    = $this->createClearFileName($upload->file_src_name_body);

                $oldFile = new File($pathToCurrentFolder . $newName . '.' . $upload->file_src_name_ext);

                if ($overwriteName) {
                    $this->createFolderInTrashIfNotExist($folder);
                    $this->moveOldFileToTrashAndDelete($oldFile, $folder);
                    $upload->file_new_name_body = $newName;
                } else {
                    $rndString = "";
                    if ($oldFile->exists()) {
                        $rndString = '-' . $this->getRandomPrefix();
                    }
                    $newName                    .= $rndString;
                    $upload->file_new_name_body = $newName . $rndString;
                }

                $upload->process($pathToCurrentFolder);

                if (!$upload->processed) {
                    throw new InternalErrorException($upload->error);
                }
            }
        }
    }

    /**
     * File method
     *
     * @description File details modal
     * @return Response|null
     */
    public function file()
    {
        $this->setLayout('modal');
        $path = trim($this->getRequest()->getQuery('path'), "/");

        if (file_exists(WWW_ROOT . $path)) {
            $fileinfo     = new Fileinfo();
            $file         = $fileinfo->getInfo(WWW_ROOT . $path);
            $file['date'] = date(Configure::read('date_format'), filemtime($path));
            $file['size'] = $fileinfo->formatSizeUnits(filesize($path));
            $file['path'] = $path;
            $file['dir']  = $this->getRequest()->getQuery('dir');
        } else {
            $this->Flash->error(__('File does not exist.'));

            return $this->redirect(['action' => 'index']);
        }

        $this->set(compact('path', 'file'));
    }

    /**
     * AddFolder method
     *
     * @description Create folder
     * @return Response|null
     */
    public function addFolder()
    {
        $this->setLayout('modal');
        if ($this->getRequest()->is(['post'])) {
            $name = Text::slug($this->getRequest()->getData()['name']);
            $dir  = new Folder($this->path . DS . $name, true, Configure::read('folder_chmod'));
            if ($dir) {
                $this->Flash->success(__('Folder created.'));
            } else {
                $this->Flash->error(__('Folder was not created.'));
            }

            return $this->redirect(['action' => 'index', 'path' => $name, 'CKEditorFuncNum' => $this->getRequest()->getQuery('CKEditorFuncNum')]);
        }
    }

    /**
     * DeleteFolder method
     *
     * @description Delete folder
     * @param string|null $folder Folder name.
     * @return Response
     */
    public function deleteFolder($folder = null)
    {
        $this->autoRender = false;

        try {
            if (!$folder) {
                throw new Exception(__('Folder does not exist.'));
            }

            $data = new Folder($this->path . DS . $folder);
            $data = $data->read();
            if ($data[0] || $data[1]) {
                throw new Exception(__('This folder is not empty. Please remove all files first.'));
            }

            $dir  = new Folder();
            $move = $dir->move([
                'from' => $this->path . DS . $folder,
                'to'   => $this->trashPath . DS . $folder,
            ]);

            if ($move) {
                $this->Flash->success(__('Folder has been removed.'));
            } else {
                throw new Exception(__('Folder has not been removed.'));
            }
        } catch (Exception $ex) {
            $this->Flash->error($ex->getMessage());

            return $this->redirect(['action' => 'index', 'path' => $folder]);
        }

        return $this->redirect(['action' => 'index', 'CKEditorFuncNum' => $this->getRequest()->getQuery('CKEditorFuncNum')]);
    }

    /**
     * Delete file method
     *
     * @description Delete file
     * @return Response|null
     */
    public function deleteFile()
    {
        $this->autoRender = false;

        $path = $this->getRequest()->getQuery('path');
        $dir  = $this->getRequest()->getQuery('dir');

        if ($path) {
            $this->createFolderInTrashIfNotExist($dir);

            $file = new File(WWW_ROOT . $path);
            $file->copy(WWW_ROOT . Configure::read('path') . DS . 'trash' . DS . $dir . DS . $file->name);

            if ($file->delete()) {
                $this->Flash->success(__('File removed.'));
            } else {
                $this->Flash->error(__('File was not remove.'));
            }
        }

        return $this->redirect(['action' => 'index', 'path' => $dir, 'CKEditorFuncNum' => $this->getRequest()->getQuery('CKEditorFuncNum')]);
    }

    /**
     * Delete files method
     *
     * @description Delete files
     * @return Response|null
     */
    public function deleteFiles()
    {
        $this->autoRender = false;
        if ($this->getRequest()->is('post')) {
            $files = $this->getRequest()->getData()['delete'];
            $dir   = $this->getRequest()->getData()['dir'];

            $this->createFolderInTrashIfNotExist($dir);

            foreach ($files as $key => $row) {
                $file = new File(WWW_ROOT . base64_decode($key));
                $file->copy($this->trashPath . DS . $dir . DS . $file->name);
                $file->delete();
            }

            $this->Flash->success(__('Files removed.'));
            return $this->redirect(['action' => 'index', 'path' => $dir, 'CKEditorFuncNum' => $this->getRequest()->getQuery('CKEditorFuncNum')]);
        }
    }

    /**
     * Change file name
     *
     * @description Rename file
     * @return void
     */
    public function renameFile()
    {
        try {
            $this->request->allowMethod(['ajax', 'post']);
            $newName       = $this->getRequest()->getData("newName");
            $orgBasename   = $this->getRequest()->getData("orgBasename");
            $overwriteName = filter_var($this->getRequest()->getData("overwriteFileName"), FILTER_VALIDATE_BOOLEAN);

            $folder              = $this->getRequest()->getData("dir");
            $pathToCurrentFolder = $this->connectPaths($this->path, $folder);

            $orginalFile = new File($pathToCurrentFolder . $orgBasename);
            if (!$orginalFile->exists()) {
                throw new Exception(__("File not exist."));
            }

            $orginalFileNewName = $this->createClearFileName($newName);

            if (!$newName) {
                throw new Exception(__("File without name."));
            }

            $oldFile      = new File($pathToCurrentFolder . $newName . '.' . $orginalFile->ext());
            $oldFileExist = $oldFile->exists();

            if ($overwriteName) {
                $this->createFolderInTrashIfNotExist($folder);
                $this->moveOldFileToTrashAndDelete($oldFile, $folder);

                $this->changeFileName([
                    "orginalFile"    => $orginalFile,
                    "oldFile"        => $oldFile,
                    "newPathAndName" => $pathToCurrentFolder . $orginalFileNewName . '.' . $orginalFile->ext(),
                    "oldFileExist"   => $oldFileExist,
                    "folder"         => $folder
                ]);
            } else {
                $rndString = "";
                if ($oldFile->exists()) {
                    $rndString = '-' . $this->getRandomPrefix();
                }

                $orginalFileNewName .= $rndString;

                $this->changeFileName([
                    "orginalFile"    => $orginalFile,
                    "oldFile"        => $oldFile,
                    "newPathAndName" => $pathToCurrentFolder . $orginalFileNewName . '.' . $orginalFile->ext(),
                    "oldFileExist"   => $oldFileExist,
                    "folder"         => $folder
                ]);
            }

            $this->set([
                'data'       => [
                    "success"       => true,
                    "orgBasename"   => $orgBasename,
                    "newName"       => $orginalFileNewName,
                    "newBasename"   => $orginalFileNewName . '.' . strtolower($orginalFile->info["extension"]),
                    "fullPath"      => str_replace("\\", "/", "/" . $this->connectPaths(Configure::read('path'), $folder) . DS . $orginalFileNewName . '.' . strtolower($orginalFile->info["extension"])),
                    "delete"        => "delete[" . base64_encode($this->connectPaths(Configure::read('path'), $folder) . "\\" . $orginalFileNewName . '.' . strtolower($orginalFile->info["extension"])) . "]",
                    "overwriteName" => $overwriteName
                ],
                '_serialize' => 'data',
            ]);
        } catch (Exception $ex) {
            $this->set([
                'data'       => [
                    "success" => false,
                    'error'   => $ex->getMessage()
                ],
                '_serialize' => 'data',
            ]);
        }

        $this->RequestHandler->renderAs($this, 'json');
    }

    /**
     * Change file name.
     *
     * @param array $data Array with data.
     * @return void
     * @throws Exception When the file name could not be changed.
     */
    private function changeFileName(array $data = [])
    {
        if ($data["orginalFile"]->copy($data["newPathAndName"])) {
            $data["orginalFile"]->delete();
        } else {
            if ($data["oldFileExist"]) {
                $this->revertOldFileFromTrash($data["oldFile"], $data["folder"]);
            }
            throw new Exception(__('The file name change was unsuccessful.'));
        }
    }

    /**
     * Return safe string.
     *
     * @param string|null $name String to replace.
     *
     * @return string
     */
    private function createClearFileName($name = '')
    {
        return preg_replace("![^a-zA-Z0-9-]+!i", "-", strip_tags(trim($name)));
    }

    /**
     * Create folder in trash, if not exist.
     *
     * @param string|null $folder Folder name.
     *
     * @return void
     */
    private function createFolderInTrashIfNotExist($folder = '')
    {
        if ($folder) {
            new Folder($this->trashPath . DS . $folder, true, Configure::read('folder_chmod'));
        }
    }

    /**
     * Return unique prefix.
     *
     * @return string
     */
    private function getRandomPrefix()
    {
        return Time::now()->timestamp;
    }

    /**
     * If file exists, it is copied to the trash folder.
     *
     * @param File        $file   Old file to deleted.
     * @param string|null $folder Folder name.
     *
     * @return boolean
     */
    private function moveOldFileToTrashAndDelete(File $file, $folder = '')
    {
        if ($file->exists()) {
            $trashPath = $this->connectPaths($this->trashPath, $folder);
            $file->copy($trashPath . $file->name);

            return $file->delete();
        }

        return true;
    }

    /**
     * Revert file from folder trash.
     *
     * @param File        $file   File to be restored.
     * @param string|null $folder Folder name.
     *
     * @return void
     */
    private function revertOldFileFromTrash(File $file, $folder = '')
    {
        $fullTrashPath = $this->connectPaths($this->trashPath, $folder);
        $fullBasePath  = $this->connectPaths($this->path, $folder);
        $oldFile       = new File($fullTrashPath . $file->name);
        $oldFile->copy($fullBasePath . $oldFile->name);
    }

    /**
     * Connect paths.
     *
     * @param string|null $path   Base path.
     * @param string|null $folder Folder name.
     * @return string
     */
    private function connectPaths($path = '', $folder = '')
    {
        $path .= DS;
        if ($folder) {
            $path .= $folder . DS;
        }

        return $path;
    }
}
