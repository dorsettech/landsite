var OverwriteFileName = {
    id: '#overwrite-file-name',
    listeners: function () {
        $(document).on('click', this.id, function () {
            OverwriteFileName.setSwal($(this));
        });
    },
    setSwal: function (obj) {
        if (obj.prop("checked")) {
            swal({
                title: obj.data("title"),
                text: obj.data("text"),
                icon: "error",
                buttons: true,
                dangerMode: true,
            }).then(function (confirmed) {
                obj.prop('checked', confirmed);
            });
        }
        if (FileManager.dropzone !== null) {
            FileManager.dropzone.options.params["overwrite-file-name"] = obj.prop("checked");
        }
    }
};

$(document).ready(function () {
    OverwriteFileName.listeners();
});
