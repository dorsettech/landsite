var ConfirmDelete = {
    listeners: function () {
        $(document).on('click', '.btn-delete', function (e) {
            e.preventDefault();
            ConfirmDelete.confirm($(this));
            return false;
        });
    },
    confirm: function (obj) {
        obj.prop('disabled', true);
        swal({
            title: obj.data("title"),
            text: obj.data("text"),
            icon: "error",
            buttons: true,
            dangerMode: true,
        }).then(function (confirmed) {
            if (confirmed) {
                if (obj.attr("href")) {
                    window.location = obj.attr("href");
                } else {
                    obj.closest("form").submit();
                }
            }
            obj.prop('disabled', false);
        });
    }
};

$(document).ready(function () {
    ConfirmDelete.listeners();
});
