var FileRename = {
    listeners: function () {
        $(document).on('click', '.btn-file-rename', function (e) {
            e.preventDefault();
            FileRename.getName($(this));
        });
    },
    getName: function (obj) {
        $.ajax({
            url: adminPrefix + '/file-manager/manager/renameFile',
            method: 'POST',
            dataType: 'json',
            data: {
                orgBasename: $(".file-basename:first").text(),
                newName: $('#new-name').val(),
                overwriteFileName: $('#overwrite-file-name').prop("checked"),
                dir: $('#dir').val()
            },
            beforeSend: function () {
                obj.prop('disabled', true);
            },
            error: function () {
                FileRename.showError();
                obj.prop('disabled', false);
            },
            success: function (data) {
                if (data.success) {
                    obj.prop('disabled', false);
                    if (data.overwriteName) {
                        FileRename.deleteBox(data);
                    }
                    FileRename.setFileData(data);
                    FileRename.setViewData(data);
                    Notifications.create('success', 'File name has been changed.')
                } else {
                    Notifications.create('error', 'File name couldn\'t be changed.')
                    obj.prop('disabled', false);
                }
            }
        });
    },
    setFileData: function (data) {
        var container = $('.modal-content'),
            containerHtml = container.html();
        containerHtml = FileRename.replaceAll(containerHtml, data.orgBasename, data.newBasename);
        container.html(containerHtml);

        $("#new-name").val(data.newName);
    },
    setViewData: function (data) {
        var container = $('[data-file="' + data.orgBasename + '"]'),
            containerHtml = container.html();
        containerHtml = FileRename.replaceAll(containerHtml, data.orgBasename, data.newBasename);
        container.html(containerHtml);
        container.attr("data-file", data.newBasename);
        $('[data-file="' + data.orgBasename + '"] .file-name span').text(data.newBasename);
        $('[data-file="' + data.orgBasename + '"] .checkbox-delete').attr("name", data.delete);
    },
    deleteBox: function (data) {
        $('[data-file="' + data.newBasename + '"]').remove();
    },
    replaceAll: function (str, find, newText) {
        var re = new RegExp(find, 'g');

        return str.replace(re, newText);
    }
};

$(document).ready(function () {
    FileRename.listeners();
});
