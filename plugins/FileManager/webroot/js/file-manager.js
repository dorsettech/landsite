var FileManager = {
    modal: null,
    dropzone: null,
    CKEditorFuncNum: null,
    modalId: 'ajax-modal',
    loadingId: 'ajax-loading',
    dropzoneFormId: 'dropzoneForm',
    dropzoneUploadId: 'dropzone-upload',
    deleteCheckboxClass: 'checkbox-delete',
    toggleCheckboxesClass: 'toggle-checkboxes',
    copyToClipboardClass: 'copy-to-clipboard',
    init: function () {
        this.setCKEditorFuncNum();
        this.listeners();
    },
    listeners: function () {
        $(document).on('click', '[data-ajax-modal]', function (e) {
            e.preventDefault();
            var path = $(this).attr('data-path');

            if (typeof path !== typeof undefined && path !== false) {
                FileManager.getFileUrlForCKEditor(path);
            } else {
                FileManager.ajaxModal($(this));
            }
        });
        $(document).on('click', '#' + this.dropzoneUploadId, function (e) {
            e.preventDefault();
            FileManager.dropzoneUpload();
        });
        $(document).on('click', '.' + this.deleteCheckboxClass, function (e) {
            FileManager.toggleDeleteButton();
        });
        $(document).on('click', '.' + this.toggleCheckboxesClass, function (e) {
            FileManager.toggleCheckboxes($(this));
        });
        $(document).on('click', '.' + this.copyToClipboardClass, function (e) {
            FileManager.copyToClibpoard(this);
        });
    },
    showHideAjaxLoader(mode) {
        if ($('#' + this.loadingId).length > 0) {
            if (mode == true) {
                $('#' + this.loadingId).fadeIn();
            } else {
                $('#' + this.loadingId).fadeOut();
            }
        }
    },
    createModal: function () {
        var modal = '<div class="modal fade" id="' + this.modalId + '" tabindex="-1" role="dialog" aria-hidden="true"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-body"></div></div></div></div>';
        $(document.body).append(modal);
    },
    removeModal: function () {
        if (this.modal !== null) {
            this.modal.remove();
        }
    },
    ajaxModal: function (obj) {
        this.removeModal();
        this.createModal();

        FileManager.showHideAjaxLoader(true);

        var remote = obj.data('remote') || obj.attr('href');
        $('#' + this.modalId).load(remote, function (response, status, xhr) {
            if (status == 'success') {
                FileManager.modal = $('#' + FileManager.modalId).modal();
                FileManager.modal.modal('show');
                if (obj.data('init') == 'dropzone') {
                    FileManager.dropzoneInit();
                }
            } else {
                Notifications.create('error', 'An error occured while loading ' + remote);
            }
            FileManager.showHideAjaxLoader(false);
        });
    },
    dropzoneInit: function () {
        var loop = setInterval(function () {
            if (FileManager.dropzone === null || typeof FileManager.dropzone) {
                FileManager.dropzoneCreate();
                clearInterval(loop);
            }
        }, 300);
    },
    dropzoneCreate: function () {
        Dropzone.autoDiscover = false;
        this.dropzone = new Dropzone('#' + this.dropzoneFormId, {
            url: upload.action,
            autoProcessQueue: false,
            paramName: "file",
            parallelUploads: 100,
            maxFilesize: upload.max_file_size, /* MB */
            acceptedFiles: upload.accepted_files,
            dictDefaultMessage: "<strong>Drop files here or click to upload</strong>",
            headers: Csrf.getHeaders(),
            params: {"overwrite-file-name": false},
        });
    },
    dropzoneUpload: function () {
        var allFiles = this.dropzone.files.length,
                rejectedFiles = this.dropzone.getRejectedFiles().length;
        if ((allFiles - rejectedFiles) <= 0) {
            Notifications.create('info', 'There are no files to upload');
        } else {
            FileManager.showHideAjaxLoader(true);
            this.dropzone.processQueue();
            this.dropzone.on("queuecomplete", function (file) {
                FileManager.showHideAjaxLoader(false);
                location.reload();
            });
        }
    },
    toggleDeleteButton: function () {
        var checkCount = 0;
        $('.' + this.deleteCheckboxClass).each(function () {
            if (this.checked) {
                checkCount += 1;
            }
        });
        if (checkCount > 0) {
            $('.button-delete').removeClass('d-none');
        } else {
            $('.button-delete').addClass('d-none');
        }
    },
    toggleCheckboxes: function (obj) {
        var toggleSelect = obj.attr('data-select');
        var toggleClass = obj.data('class');

        if (toggleSelect == 1) {
            $('.' + toggleClass).prop('checked', true);
            obj.attr('data-select', 0);
            obj.find('i').attr('class', 'fas fa-check-square');
        } else {
            $('.' + toggleClass).prop('checked', false);
            obj.attr('data-select', 1);
            obj.find('i').attr('class', 'far fa-check-square');
        }

        this.toggleDeleteButton();
    },
    setCKEditorFuncNum: function () {
        this.CKEditorFuncNum = this.getUrlParam('CKEditorFuncNum');
    },
    getUrlParam: function (paramName) {
        var reParam = new RegExp('(?:[\?&]|&)' + paramName + '=([^&]+)', 'i'),
                match = window.location.search.match(reParam);
        return (match && match.length > 1) ? match[1] : null;
    },
    getFileUrlForCKEditor: function (path) {
        if (this.CKEditorFuncNum != null) {
            window.opener.CKEDITOR.tools.callFunction(this.CKEditorFuncNum, path);
            window.close();
        }
    },
    copyToClibpoard: function (element) {
        var target = $(element).attr('data-target');
        $(target).focus();
        $(target).select();

        if (document.execCommand('copy')) {
            Notifications.create('info', 'File path has been copied to your clipboard.');
        } else {
            Notifications.create('warning', 'File path has not been copied.');
        }
    }
}

$(document).ready(function () {
    FileManager.init();
});