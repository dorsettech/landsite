<?php

/**
 * @author Jakub Gałyga <biuro@ninjacode.pl>
 * @date (2018-11-05)
 * @version 1.0
 */

use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;
use Cake\Core\Configure;

Router::plugin('FileManager', ['path' => '/' . Configure::read('Website.admin_prefix')], function (RouteBuilder $routes) {
    $routes->scope('/file-manager', ['prefix' => 'panel', 'plugin' => 'FileManager'], function ($routes) {
        $routes->connect('/', ['prefix' => 'panel', 'controller' => 'Manager', 'action' => 'index', 'plugin' => 'FileManager']);
        $routes->connect('/:controller/:action/*', ['prefix' => 'panel', 'plugin' => 'FileManager']);
        $routes->fallbacks('InflectedRoute');
    });
    $routes->fallbacks(DashedRoute::class);
});
