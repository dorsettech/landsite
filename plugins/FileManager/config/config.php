<?php

return [
    'path' => 'files/media',
    'folder_chmod' => 0777,
    'hidden_folder' => ['trash'],
    'max_file_size' => 8,
    'file_chmod' => 0755,
    'date_format' => 'Y-m-d H:i:s',
    'allowed_extensions' => ['application/pdf', 'application/msword', 'image/*'],
    'file_random_name' => true,
    'files' => 'docx|pdf|xml|doc|csv',
    'images' => 'jpeg|jpg|gif|png',
    'audio' => 'mp3|wav|mp4'
];
