<?php
use Cake\Core\Configure;
use \Cake\Filesystem\Folder;


Configure::load('FileManager.config');


if (!file_exists(WWW_ROOT . Configure::read('path'))) {

    $path = new Folder(WWW_ROOT . Configure::read('path'), true, Configure::read('folder_chmod'));

}
