# FileManager plugin for ECommerce

## Installation

1. Add this line to `boostrap.php`

```
Plugin::load('FileManager', ['bootstrap' => true, 'routes' => true, 'autoload' => true]);
```

The recommended way to install composer packages is:

```
composer require your-name-here/FileManager
```
