<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* quarto_login_popup.html */
class __TwigTemplate_c5649b42f230f4e9103474239c7ef79b319e5905ee7f49b8e23c3babf7d8c835 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "\t\t\t<div id=\"login-box\" class=\"login-popup\">
\t\t\t    <a href=\"#\" class=\"close\"></a>
\t\t\t\t<form method=\"post\" action=\"";
        // line 3
        echo ($context["S_LOGIN_ACTION"] ?? null);
        echo "\" class=\"quick-login\">
\t\t\t\t\t<fieldset>
\t\t\t\t\t\t<div class=\"navbar_username_outer\">
\t\t\t\t\t\t\t<label for=\"username\">";
        // line 6
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("USERNAME");
        echo ":</label>&nbsp;<br /><input type=\"text\" name=\"username\" id=\"navbar_username\" size=\"10\" class=\"inputbox\" title=\"";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("USERNAME");
        echo "\" />
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"navbar_password_outer\">
\t\t\t\t\t\t\t<label for=\"password\">";
        // line 10
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("PASSWORD");
        echo ":</label>&nbsp;<br /><input type=\"password\" name=\"password\" id=\"navbar_password\" size=\"10\" class=\"inputbox\" title=\"";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("PASSWORD");
        echo "\" />
\t\t\t\t\t\t</div>




\t\t\t\t\t\t<div class=\"login-buttons\">
\t\t\t\t\t\t\t<input type=\"submit\" name=\"login\" value=\"";
        // line 17
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("LOGIN");
        echo "\" class=\"button2\" />
\t\t\t\t\t\t\t";
        // line 18
        echo ($context["S_LOGIN_REDIRECT"] ?? null);
        echo "
\t\t\t\t\t\t\t";
        // line 19
        echo ($context["S_FORM_TOKEN_LOGIN"] ?? null);
        echo "
\t\t\t\t\t\t\t";
        // line 20
        if (($context["S_AUTOLOGIN_ENABLED"] ?? null)) {
            // line 21
            echo "\t\t\t\t\t\t\t\t<label id=\"autologin_label\" for=\"autologin\"><input type=\"checkbox\" name=\"autologin\" id=\"autologin\" />&nbsp;";
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("LOG_ME_IN");
            echo "</label>
\t\t\t\t\t\t\t";
        }
        // line 23
        echo "\t\t\t\t\t\t</div>
\t\t\t\t\t</fieldset>
\t\t\t\t</form>
\t\t\t\t\t<div class=\"login-footer\">\t\t\t        
\t\t\t\t\t\t";
        // line 27
        if (($context["S_REGISTER_ENABLED"] ?? null)) {
            // line 28
            echo "\t\t\t\t\t\t\t<a class=\"register-link\" href=\"";
            echo ($context["U_REGISTER"] ?? null);
            echo "\">";
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("REGISTER");
            echo "</a>
\t\t\t\t\t\t";
        }
        // line 30
        echo "\t\t\t\t\t\t";
        if (($context["U_SEND_PASSWORD"] ?? null)) {
            // line 31
            echo "\t\t\t\t\t\t\t<a class=\"restore-password rightside\" href=\"";
            echo ($context["U_SEND_PASSWORD"] ?? null);
            echo "\">";
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("FORGOT_PASS");
            echo "</a>
\t\t\t\t\t\t";
        }
        // line 33
        echo "\t\t\t        </div>

\t\t\t</div>
\t\t\t<div id=\"mask\"></div>";
    }

    public function getTemplateName()
    {
        return "quarto_login_popup.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 33,  100 => 31,  97 => 30,  89 => 28,  87 => 27,  81 => 23,  75 => 21,  73 => 20,  69 => 19,  65 => 18,  61 => 17,  49 => 10,  40 => 6,  34 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "quarto_login_popup.html", "");
    }
}
