<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* overall_footer.html */
class __TwigTemplate_7f0d8bc2a57cb6e766ab9e5f12ca333e9725ecf1423e107e1d6ff7a6c6a93ea5 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "\t\t";
        // line 2
        echo "\t</div>

";
        // line 4
        // line 5
        echo "
<div id=\"breadcrumb-footer\" class=\"navbar\">
\t<ul class=\"linklist\">
\t\t";
        // line 8
        // line 9
        echo "\t\t<li class=\"rightside\">";
        echo ($context["S_TIMEZONE"] ?? null);
        echo "</li>
\t\t";
        // line 10
        // line 11
        echo "\t\t";
        if (($context["S_USER_LOGGED_IN"] ?? null)) {
            echo "<li class=\"responsive-center time rightside\">";
            echo ($context["CURRENT_TIME"] ?? null);
            echo "&nbsp;&nbsp;&nbsp;<span class=\"divider-vertical\">|</span></li>";
        }
        // line 12
        echo "\t\t<li class=\"small-icon icon-home breadcrumbs\">
\t\t";
        // line 13
        if (($context["U_SITE_HOME"] ?? null)) {
            echo "<span class=\"crumb\"><a href=\"";
            echo ($context["U_SITE_HOME"] ?? null);
            echo "\" data-navbar-reference=\"home\">";
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("SITE_HOME");
            echo "</a></span>";
        }
        // line 14
        echo "\t\t";
        // line 15
        echo "\t\t<span class=\"crumb\"><a href=\"";
        echo ($context["U_INDEX"] ?? null);
        echo "\" data-navbar-reference=\"index\">";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("INDEX");
        echo "</a></span>
\t\t";
        // line 16
        // line 17
        echo "\t\t</li>
\t\t";
        // line 18
        if ((($context["U_WATCH_FORUM_LINK"] ?? null) &&  !($context["S_IS_BOT"] ?? null))) {
            echo "<li class=\"small-icon icon-";
            if (($context["S_WATCHING_FORUM"] ?? null)) {
                echo "unsubscribe";
            } else {
                echo "subscribe";
            }
            echo "\" data-last-responsive=\"true\"><a href=\"";
            echo ($context["U_WATCH_FORUM_LINK"] ?? null);
            echo "\" title=\"";
            echo ($context["S_WATCH_FORUM_TITLE"] ?? null);
            echo "\" data-ajax=\"toggle_link\" data-toggle-class=\"small-icon icon-";
            if ( !($context["S_WATCHING_FORUM"] ?? null)) {
                echo "unsubscribe";
            } else {
                echo "subscribe";
            }
            echo "\" data-toggle-text=\"";
            echo ($context["S_WATCH_FORUM_TOGGLE"] ?? null);
            echo "\" data-toggle-url=\"";
            echo ($context["U_WATCH_FORUM_TOGGLE"] ?? null);
            echo "\">";
            echo ($context["S_WATCH_FORUM_TITLE"] ?? null);
            echo "</a></li>";
        }
        // line 19
        echo "\t</ul>
</div>

<div id=\"page-footer\" class=\"js-hideiframe page-footer\" role=\"contentinfo\">

\t<!-- Prefooter blocks go below -->

\t<div class=\"copyright\">

\t\t<!-- Social links go below -->
\t\t<div class=\"footer-col footer-menu\">
\t\t\t<a class=\"footer-link\" href=\"";
        // line 30
        echo ($context["U_PRIVACY"] ?? null);
        echo "\" title=\"";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("PRIVACY_LINK");
        echo "\" role=\"menuitem\">
\t\t\t\t<span class=\"footer-link-text\">Privacy policy</span>
\t\t\t</a>
\t\t\t<a class=\"footer-link\" href=\"";
        // line 33
        echo ($context["U_TERMS_USE"] ?? null);
        echo "\" title=\"";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("TERMS_LINK");
        echo "\" role=\"menuitem\">
\t\t\t\t<span class=\"footer-link-text\">Terms of use</span>
\t\t\t</a>
\t\t\t<a href=\"";
        // line 36
        echo ($context["U_FAQ"] ?? null);
        echo "\">FAQs</a>
\t\t</div>
\t\t
\t\t<div class=\"footer-col footer-contact\">
\t\t\t<a href=\"https://www.thelandsite.co.uk/contact\" target=\"_blank\">Contact Us</a>
\t\t\t<a href=\"tel:01202016230\">01202 016230</a>
\t\t\t<a href=\"mailto:info@thelandsite.co.uk\">info@thelandsite.co.uk</a>
\t\t</div>
\t\t<div class=\"footer-col align-right\">
\t\t\t<div class=\"footer-social\">
\t\t\t\t<a href=\"https://www.facebook.com/TheLandsitePropertyService\" target=\"_blank\"><i class=\"fab fa-facebook-f\"></i>
</a>
\t\t\t\t<a href=\"https://twitter.com/thelandsite\" target=\"_blank\"><i class=\"fab fa-twitter\"></i></a>
\t\t\t\t<a href=\"https://www.linkedin.com/company/the-landsite\" target=\"_blank\" class=\"linkedin\"><i class=\"fab fa-linkedin-in\"></i></a>
\t\t\t\t<a href=\"https://www.instagram.com/the_landsite\" target=\"_blank\"><i class=\"fab fa-instagram\"></i></a>
\t\t\t</div>
\t\t\t<div class=\"footer-copy\">
\t\t\t\tCopyright 2019 | The Landsite Online Ltd
\t\t\t</div>
\t\t</div>
\t\t
\t\t";
        // line 57
        // line 58
        echo "<!--
\t\t<div class=\"footer-row\">
\t\t\t<span class=\"footer-copyright\">";
        // line 60
        echo ($context["CREDIT_LINE"] ?? null);
        echo "</span>
\t\t</div>
-->
\t\t";
        // line 63
        if (($context["TRANSLATION_INFO"] ?? null)) {
            // line 64
            echo "<!--
\t\t<div class=\"footer-row\">
\t\t\t<span class=\"footer-copyright\">";
            // line 66
            echo ($context["TRANSLATION_INFO"] ?? null);
            echo "</span>
\t\t</div>
-->
\t\t";
        }
        // line 70
        echo "\t\t";
        // line 71
        echo "<!--
        <div class=\"footer-row\">
\t\t\t<a class=\"footer-link\" href=\"";
        // line 73
        echo ($context["U_PRIVACY"] ?? null);
        echo "\" title=\"";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("PRIVACY_LINK");
        echo "\" role=\"menuitem\">
\t\t\t\t<span class=\"footer-link-text\">";
        // line 74
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("PRIVACY_LINK");
        echo "</span>
\t\t\t</a>
\t\t\t|
\t\t\t<a class=\"footer-link\" href=\"";
        // line 77
        echo ($context["U_TERMS_USE"] ?? null);
        echo "\" title=\"";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("TERMS_LINK");
        echo "\" role=\"menuitem\">
\t\t\t\t<span class=\"footer-link-text\">";
        // line 78
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("TERMS_LINK");
        echo "</span>
\t\t\t</a>
\t\t</div>
-->
\t\t";
        // line 82
        if (($context["DEBUG_OUTPUT"] ?? null)) {
            // line 83
            echo "\t\t<div class=\"footer-row\">
\t\t\t<span class=\"footer-info\">";
            // line 84
            echo ($context["DEBUG_OUTPUT"] ?? null);
            echo "</span>
\t\t</div>
\t\t";
        }
        // line 87
        echo "\t</div>

\t<div id=\"darkenwrapper\" class=\"darkenwrapper\" data-ajax-error-title=\"";
        // line 89
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("AJAX_ERROR_TITLE");
        echo "\" data-ajax-error-text=\"";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("AJAX_ERROR_TEXT");
        echo "\" data-ajax-error-text-abort=\"";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("AJAX_ERROR_TEXT_ABORT");
        echo "\" data-ajax-error-text-timeout=\"";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("AJAX_ERROR_TEXT_TIMEOUT");
        echo "\" data-ajax-error-text-parsererror=\"";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("AJAX_ERROR_TEXT_PARSERERROR");
        echo "\">
\t\t<div id=\"darken\" class=\"darken\">&nbsp;</div>
\t</div>

\t<div id=\"phpbb_alert\" class=\"phpbb_alert\" data-l-err=\"";
        // line 93
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("ERROR");
        echo "\" data-l-timeout-processing-req=\"";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("TIMEOUT_PROCESSING_REQ");
        echo "\">
\t\t<a href=\"#\" class=\"alert_close\">
\t\t\t<i class=\"icon fa-times-circle fa-fw\" aria-hidden=\"true\"></i>
\t\t</a>
\t\t<h3 class=\"alert_title\">&nbsp;</h3><p class=\"alert_text\"></p>
\t</div>
\t<div id=\"phpbb_confirm\" class=\"phpbb_alert\">
\t\t<a href=\"#\" class=\"alert_close\">
\t\t\t<i class=\"icon fa-times-circle fa-fw\" aria-hidden=\"true\"></i>
\t\t</a>
\t\t<div class=\"alert_text\"></div>
\t</div>
</div>
<div id=\"page-footer-links\">
\t<ul class=\"nav-footer linklist\" role=\"menubar\">
\t\t";
        // line 108
        if (($context["U_ACP"] ?? null)) {
            echo "<li class=\"rightside\"><a href=\"";
            echo ($context["U_ACP"] ?? null);
            echo "\"><i class=\"icon fa-cogs fa-fw\" aria-hidden=\"true\"></i>";
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("ACP");
            echo "</a></li>";
        }
        // line 109
        echo "\t\t";
        if ( !($context["S_IS_BOT"] ?? null)) {
            // line 110
            echo "\t\t\t<li><a href=\"";
            echo ($context["U_DELETE_COOKIES"] ?? null);
            echo "\" data-ajax=\"true\" data-refresh=\"true\" role=\"menuitem\"><i class=\"icon fa-cogs fa-fw\" aria-hidden=\"true\"></i>";
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("DELETE_COOKIES");
            echo "</a></li>
\t\t\t";
            // line 111
            if (($context["S_DISPLAY_MEMBERLIST"] ?? null)) {
                echo "<li data-last-responsive=\"true\"><a href=\"";
                echo ($context["U_MEMBERLIST"] ?? null);
                echo "\" title=\"";
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("MEMBERLIST_EXPLAIN");
                echo "\" role=\"menuitem\"><i class=\"icon fa-group fa-fw\" aria-hidden=\"true\"></i>";
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("MEMBERLIST");
                echo "</a></li>";
            }
            // line 112
            echo "\t\t";
        }
        // line 113
        echo "\t\t";
        // line 114
        echo "\t\t";
        if (($context["U_TEAM"] ?? null)) {
            echo "<li data-last-responsive=\"true\"><a href=\"";
            echo ($context["U_TEAM"] ?? null);
            echo "\" role=\"menuitem\"><i class=\"icon fa-shield fa-fw\" aria-hidden=\"true\"></i>";
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("THE_TEAM");
            echo "</a></li>";
        }
        // line 115
        echo "\t\t";
        // line 116
        echo "\t\t";
        if (($context["U_CONTACT_US"] ?? null)) {
            echo "<li data-last-responsive=\"true\"><a href=\"";
            echo ($context["U_CONTACT_US"] ?? null);
            echo "\" role=\"menuitem\"><i class=\"icon fa-envelope fa-fw\" aria-hidden=\"true\"></i>";
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("CONTACT_US");
            echo "</a></li>";
        }
        // line 117
        echo "\t</ul>
</div>

</div>

<div class=\"cron-block\">
\t<a id=\"bottom\" class=\"anchor\" accesskey=\"z\"></a>
\t";
        // line 124
        if ( !($context["S_IS_BOT"] ?? null)) {
            echo ($context["RUN_CRON_TASK"] ?? null);
        }
        // line 125
        echo "</div>

<script src=\"";
        // line 127
        echo ($context["T_JQUERY_LINK"] ?? null);
        echo "\"></script>
";
        // line 128
        if (($context["S_ALLOW_CDN"] ?? null)) {
            echo "<script>window.jQuery || document.write('\\x3Cscript src=\"";
            echo ($context["T_ASSETS_PATH"] ?? null);
            echo "/javascript/jquery.min.js?assets_version=";
            echo ($context["T_ASSETS_VERSION"] ?? null);
            echo "\">\\x3C/script>');</script>";
        }
        // line 129
        echo "<script src=\"";
        echo ($context["T_ASSETS_PATH"] ?? null);
        echo "/javascript/core.js?assets_version=";
        echo ($context["T_ASSETS_VERSION"] ?? null);
        echo "\"></script>
";
        // line 130
        $asset_file = "forum_fn.js";
        $asset = new \phpbb\template\asset($asset_file, $this->getEnvironment()->get_path_helper(), $this->getEnvironment()->get_filesystem());
        if (substr($asset_file, 0, 2) !== './' && $asset->is_relative()) {
            $asset_path = $asset->get_path();            $local_file = $this->getEnvironment()->get_phpbb_root_path() . $asset_path;
            if (!file_exists($local_file)) {
                $local_file = $this->getEnvironment()->findTemplate($asset_path);
                $asset->set_path($local_file, true);
            }
        }
        
        if ($asset->is_relative()) {
            $asset->add_assets_version('18');
        }
        $this->getEnvironment()->get_assets_bag()->add_script($asset);        // line 131
        $asset_file = "ajax.js";
        $asset = new \phpbb\template\asset($asset_file, $this->getEnvironment()->get_path_helper(), $this->getEnvironment()->get_filesystem());
        if (substr($asset_file, 0, 2) !== './' && $asset->is_relative()) {
            $asset_path = $asset->get_path();            $local_file = $this->getEnvironment()->get_phpbb_root_path() . $asset_path;
            if (!file_exists($local_file)) {
                $local_file = $this->getEnvironment()->findTemplate($asset_path);
                $asset->set_path($local_file, true);
            }
        }
        
        if ($asset->is_relative()) {
            $asset->add_assets_version('18');
        }
        $this->getEnvironment()->get_assets_bag()->add_script($asset);        // line 132
        if (($context["S_ALLOW_CDN"] ?? null)) {
            // line 133
            echo "\t<script>
\t\t(function(\$){
\t\t\tvar \$fa_cdn = \$('head').find('link[rel=\"stylesheet\"]').first(),
\t\t\t\t\$span = \$('<span class=\"fa\" style=\"display:none\"></span>').appendTo('body');
\t\t\tif (\$span.css('fontFamily') !== 'FontAwesome' ) {
\t\t\t\t\$fa_cdn.after('<link href=\"";
            // line 138
            echo ($context["T_ASSETS_PATH"] ?? null);
            echo "/css/font-awesome.min.css\" rel=\"stylesheet\">');
\t\t\t\t\$fa_cdn.remove();
\t\t\t}
\t\t\t\$span.remove();
\t\t})(jQuery);
\t</script>
";
        }
        // line 145
        if (( !($context["S_USER_LOGGED_IN"] ?? null) &&  !($context["S_IS_BOT"] ?? null))) {
            // line 146
            $asset_file = "quarto_login_popup.js";
            $asset = new \phpbb\template\asset($asset_file, $this->getEnvironment()->get_path_helper(), $this->getEnvironment()->get_filesystem());
            if (substr($asset_file, 0, 2) !== './' && $asset->is_relative()) {
                $asset_path = $asset->get_path();                $local_file = $this->getEnvironment()->get_phpbb_root_path() . $asset_path;
                if (!file_exists($local_file)) {
                    $local_file = $this->getEnvironment()->findTemplate($asset_path);
                    $asset->set_path($local_file, true);
                }
            }
            
            if ($asset->is_relative()) {
                $asset->add_assets_version('18');
            }
            $this->getEnvironment()->get_assets_bag()->add_script($asset);        }
        // line 148
        echo "
<script>
\t\$( document ).ready(function() {
\t\t\$( \"#lstextbox\" ).load( \"_notify.php\" );
\t});
\t</script>


";
        // line 156
        if ((($context["S_USER_LOGGED_IN"] ?? null) &&  !($context["S_IS_BOT"] ?? null))) {
            // line 157
            $asset_file = "quarto_common.js";
            $asset = new \phpbb\template\asset($asset_file, $this->getEnvironment()->get_path_helper(), $this->getEnvironment()->get_filesystem());
            if (substr($asset_file, 0, 2) !== './' && $asset->is_relative()) {
                $asset_path = $asset->get_path();                $local_file = $this->getEnvironment()->get_phpbb_root_path() . $asset_path;
                if (!file_exists($local_file)) {
                    $local_file = $this->getEnvironment()->findTemplate($asset_path);
                    $asset->set_path($local_file, true);
                }
            }
            
            if ($asset->is_relative()) {
                $asset->add_assets_version('18');
            }
            $this->getEnvironment()->get_assets_bag()->add_script($asset);        }
        // line 159
        if (($context["S_COOKIE_NOTICE"] ?? null)) {
            // line 160
            echo "\t<script src=\"";
            echo ($context["T_ASSETS_PATH"] ?? null);
            echo "/cookieconsent/cookieconsent.min.js?assets_version=";
            echo ($context["T_ASSETS_VERSION"] ?? null);
            echo "\"></script>
\t<script>
\t\tif (typeof window.cookieconsent === \"object\") {
\t\twindow.addEventListener(\"load\", function(){
\t\t\twindow.cookieconsent.initialise({
\t\t\t\t\"palette\": {
\t\t\t\t\t\"popup\": {
\t\t\t\t\t\t\"background\": \"#0F538A\"
\t\t\t\t\t},
\t\t\t\t\t\"button\": {
\t\t\t\t\t\t\"background\": \"#E5E5E5\"
\t\t\t\t\t}
\t\t\t\t},
\t\t\t\t\"theme\": \"classic\",
\t\t\t\t\"content\": {
\t\t\t\t\t\"message\": \"";
            // line 175
            echo twig_escape_filter($this->env, $this->env->getExtension('phpbb\template\twig\extension')->lang("COOKIE_CONSENT_MSG"), "js");
            echo "\",
\t\t\t\t\t\"dismiss\": \"";
            // line 176
            echo twig_escape_filter($this->env, $this->env->getExtension('phpbb\template\twig\extension')->lang("COOKIE_CONSENT_OK"), "js");
            echo "\",
\t\t\t\t\t\"link\": \"";
            // line 177
            echo twig_escape_filter($this->env, $this->env->getExtension('phpbb\template\twig\extension')->lang("COOKIE_CONSENT_INFO"), "js");
            echo "\",
\t\t\t\t\t\t\"href\": \"";
            // line 178
            echo ($context["UA_PRIVACY"] ?? null);
            echo "\"
\t\t\t\t\t}
\t\t\t\t});
\t\t\t});
\t\t\t\t}
\t</script>
";
        }
        // line 185
        echo "
";
        // line 186
        $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
        $this->env->setNamespaceLookUpOrder(array('phpbb_viglink', '__main__'));
        $this->env->loadTemplate('@phpbb_viglink/event/overall_footer_after.html')->display($context);
        $this->env->setNamespaceLookUpOrder($previous_look_up_order);
        // line 187
        echo "
";
        // line 188
        if (($context["S_PLUPLOAD"] ?? null)) {
            $location = "plupload.html";
            $namespace = false;
            if (strpos($location, '@') === 0) {
                $namespace = substr($location, 1, strpos($location, '/') - 1);
                $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
                $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
            }
            $this->loadTemplate("plupload.html", "overall_footer.html", 188)->display($context);
            if ($namespace) {
                $this->env->setNamespaceLookUpOrder($previous_look_up_order);
            }
        }
        // line 189
        echo $this->getAttribute(($context["definition"] ?? null), "SCRIPTS", []);
        echo "

";
        // line 191
        // line 192
        echo "
";
        // line 193
        if ( !($context["S_USER_LOGGED_IN"] ?? null)) {
            // line 194
            $location = "quarto_login_popup.html";
            $namespace = false;
            if (strpos($location, '@') === 0) {
                $namespace = substr($location, 1, strpos($location, '/') - 1);
                $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
                $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
            }
            $this->loadTemplate("quarto_login_popup.html", "overall_footer.html", 194)->display($context);
            if ($namespace) {
                $this->env->setNamespaceLookUpOrder($previous_look_up_order);
            }
        }
        // line 196
        echo "
\t<script>
\t\t// Hide Header and footer if loaded in iframe
\t\tif (window.location !== window.parent.location) {
\t\t\tvar hideelements = document.querySelectorAll('.js-hideiframe');
\t\t\tfor(var i = 0; i < hideelements.length; i++) {
\t\t\t\thideelements[i].style.display = 'none';
\t\t\t}
\t\t}
\t</script>

</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "overall_footer.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  538 => 196,  525 => 194,  523 => 193,  520 => 192,  519 => 191,  514 => 189,  500 => 188,  497 => 187,  492 => 186,  489 => 185,  479 => 178,  475 => 177,  471 => 176,  467 => 175,  446 => 160,  444 => 159,  429 => 157,  427 => 156,  417 => 148,  402 => 146,  400 => 145,  390 => 138,  383 => 133,  381 => 132,  367 => 131,  353 => 130,  346 => 129,  338 => 128,  334 => 127,  330 => 125,  326 => 124,  317 => 117,  308 => 116,  306 => 115,  297 => 114,  295 => 113,  292 => 112,  282 => 111,  275 => 110,  272 => 109,  264 => 108,  244 => 93,  229 => 89,  225 => 87,  219 => 84,  216 => 83,  214 => 82,  207 => 78,  201 => 77,  195 => 74,  189 => 73,  185 => 71,  183 => 70,  176 => 66,  172 => 64,  170 => 63,  164 => 60,  160 => 58,  159 => 57,  135 => 36,  127 => 33,  119 => 30,  106 => 19,  80 => 18,  77 => 17,  76 => 16,  69 => 15,  67 => 14,  59 => 13,  56 => 12,  49 => 11,  48 => 10,  43 => 9,  42 => 8,  37 => 5,  36 => 4,  32 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "overall_footer.html", "");
    }
}
