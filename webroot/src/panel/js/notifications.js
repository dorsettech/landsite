/**
 * Notifications
 *
 * @description Default notifications used in administration panel. 
 * 
 * Color Admin uses Gritter as a default notifications, however it's quite old as it has been created in 2009 (last 
 * update in 2012). That's why it has been removed from plugins and replaced with Noty
 * @see https://github.com/needim/noty Github
 * @see https://ned.im/noty/ Website
 * @see https://ned.im/noty/#/options Options
 * 
 * 
 * @type object
 */
var Notifications = {
    enabled: false,
    config: {
        layout: 'topRight',
        progressBar: true,
        theme: 'metroui'    // specific CSS theme file needs to be included
    },
    init: function () {
        if (typeof Noty === 'function') {
            Notifications.enabled = true;
        } else {
            this.devError();
        }
    },
    devError: function () {
        console.error('Notifications:', 'Please include Noty plugin.');
    },
    create: function (type, text, options) {
        if (this.enabled) {
            options = options || {};
            var timeout = (typeof options.timeout !== 'undefined' && options.timeout !== null) ? options.timeout : 3000;
            
            var config = $.extend({}, this.config);
            config = $.extend(config, {
                type: type,
                text: text,
                timeout: timeout,
            });

            if (typeof options.buttons === 'object') {
                config = $.extend(config, {buttons: options.buttons});
            }
            if (typeof options.callbacks === 'object') {
                config = $.extend(config, {callbacks: options.callbacks});
            }

            var noty = new Noty(config);
            noty.show();
            
            return noty;
        }
    },
    closeAll: function() {
        if (this.enabled) {
            Noty.closeAll();
        }
    }
}

function generateAlert(type, text) {
    Notifications.init();
    Notifications.create(type, text);
}

$(document).ready(function () {
    Notifications.init();
});