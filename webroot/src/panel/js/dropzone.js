var Drop = {
    enabled: false,
    class: '.dropzone',
    config: {
        maxFiles: 10,
        maxFilesize: 10,
        paramName: 'file',
    },
    init: function () {
        if (typeof Dropzone === 'function') {
            this.enabled = true;
            
            Dropzone.autoDiscover = false;
        }
        
        if (this.enabled && $(this.class).length > 0) {
            this.setOptions();
            this.listeners();
        }
    },
    listeners: function () {
        Dropzone.instances[0].on('queuecomplete', function () {
            Drop.refreshPage();
        });
    },
    getOptions: function () {
        var config = $.extend({}, this.config);
        var element = $(this.class);
        var paramName = element.attr('data-param-name') || this.config.paramName;
        
        config.paramName = paramName;
        
        return config;
    },
    setOptions: function () {
        Dropzone.instances[0].options = $.extend(Dropzone.instances[0].options, this.getOptions());
    },
    refreshPage: function () {
        location.reload();
    }
}

$(document).ready(function () {
    Drop.init();
});