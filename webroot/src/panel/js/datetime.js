var Datetime = {
    enabled: false,
    domElements: {
        standard: '.datetime, #datetimelogs',
        time: '.time',
        timeFrom: '.time-from',
        timeTo: '.time-to',
        endDay: '#createdTo, #dateTo'
    },
    format: {
        datetime: 'YYYY-MM-DD HH:mm:ss',
        time: 'HH:mm'
    },
    
    init: function () {
        if (typeof jQuery.fn.datetimepicker === 'function') {
            this.enabled = true;
        }
        
        if (this.enabled) {
            this.elements();
        } else {
            this.devError();
        }
    },
    devError: function () {
        console.error('Datetimepicker:', 'Please include datetimepicker plugin.');
    },
    elements: function () {
        var date = new Date();
        var month = date.getMonth();
        var day = date.getDate();
        var year = date.getFullYear();
        
        if ($(this.domElements.standard).length > 0) {
            $(this.domElements.standard).datetimepicker({
                format: this.format.datetime
            });
        }
        if ($(this.domElements.time).length > 0) {
            $(this.domElements.time).datetimepicker({
                format: this.format.time,
                defaultDate: new Date(year, month, day, 07, 00)
            });
        }
        if ($(this.domElements.timeFrom).length > 0) {
            $(this.domElements.timeFrom).datetimepicker({
                format: this.format.time,
                defaultDate: new Date(year, month, day, 08, 00)
            });
        }
        if ($(this.domElements.timeTo).length > 0) {
            $(this.domElements.timeTo).datetimepicker({
                format: this.format.time,
                defaultDate: new Date(year, month, day, 16, 00)
            });
        }
        if ($(this.domElements.endDay).length > 0) {
            $(this.domElements.endDay).datetimepicker({
                format: this.format.datetime,
                defaultDate: new Date(year, month, day, 23, 59)
            });
        }
    }
};

$(document).ready(function () {
    Datetime.init();
});