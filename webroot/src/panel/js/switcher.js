var Switcher = {
    class: '.switcher',
    init: function () {
        if ($(Switcher.class).length > 0) {
            Switcher.listener();
        }
    },
    listener: function () {
        $(Switcher.class + ' input[type=checkbox]').change(function () {
            Switcher.updateValue($(this));
        });
    },
    updateValue: function (obj) {
        var data = {
            id: obj.attr('data-id'),
            model: obj.attr('data-model'),
            column: obj.attr('name')
        };

        var path = obj.attr('data-path');
        if (typeof path == 'undefined' || path == '') {
            path = window.location.pathname
        }

        $.ajax({
            url: path + '/toggle-column-value.json',
            method: 'POST',
            data: data
        }).done(function (resp) {
            if (resp.status) {
                Notifications.create('success', resp.message);
            } else {
                Notifications.create('error', resp.message);
            }
        });
    }
}

$(document).ready(function () {
    Switcher.init();
});