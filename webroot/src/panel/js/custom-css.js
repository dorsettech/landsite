var CustomCSS = {
    enabled: false,
    codeMirrorCurrent: null,
    codeMirrorVersion: null,
    cssCurrent: 'css-current',
    cssVersion: 'css-version',
    cssDiffView: 'diff',
    cssDiff: 'css-diff',
    init: function () {
        if ($('.' + this.cssCurrent).length > 0 && $('.' + this.cssVersion).length > 0) {
            this.enabled = true;
        }
        if (this.enabled) {
            if (CodeEditor.instances.length > 0 && CodeEditor.instances[this.cssCurrent] instanceof CodeMirror) {
                this.codeMirrorCurrent = CodeEditor.instances[this.cssCurrent];
            }
            if (CodeEditor.instances.length > 0 && CodeEditor.instances[this.cssVersion] instanceof CodeMirror) {
                this.codeMirrorVersion = CodeEditor.instances[this.cssVersion];
            }

            this.listeners();
            this.activeDiffWrapper();
        }
    },
    listeners: function () {
        if (this.codeMirrorCurrent !== null && this.codeMirrorVersion !== null) {
            this.codeMirrorCurrent.on('change', function () {
                CustomCSS.activeDiffWrapper(CustomCSS.codeMirrorCurrent, CustomCSS.codeMirrorVersion);
            });
            this.codeMirrorVersion.on('change', function () {
                CustomCSS.activeDiffWrapper(CustomCSS.codeMirrorCurrent, CustomCSS.codeMirrorVersion);
            });
        }
    },
    activeDiffWrapper: function (primary, secondary) {
        if (primary instanceof CodeMirror && secondary instanceof CodeMirror) {
            $(".diffWrapper").prettyTextDiff({
                originalContent: primary.getValue(),
                changedContent: this.convertString(secondary.getValue()),
                diffContainer: '.' + CustomCSS.cssDiffView
            });
        } else {
            $(".diffWrapper").prettyTextDiff({
                originalContent: $('#' + this.cssCurrent).val() || '/* custom.css */',
                changedContent: $('#' + this.cssVersion).val() || '/* custom.css */',
                diffContainer: '.' + CustomCSS.cssDiffView
            });
        }
        var diff = $('.' + CustomCSS.cssDiffView).html();
        if (diff.length <= 0) {
            diff = '/* custom.css */';
        }
        $('.' + CustomCSS.cssDiff).val(diff);
    },
    convertString: function (string) {
        var string = string.replace(/<span>/g, '');
        string = string.replace(/<\/span>/g, '');
        string = string.replace(/<ins>/g, '');
        string = string.replace(/<\/ins>/g, '');
        string = string.replace(/<del>/g, '');
        string = string.replace(/<\/del>/g, '');
        string = string.replace(/\\r\\n/g, '');
        string = string.replace(/&gt;/g, '>');
        string = string.replace(/&lt;/g, '<');
        string = string.replace(/<br\s*\/?>/mg, "\n");
        return string;
    }
};

$(document).ready(function () {
    CustomCSS.init();
});


