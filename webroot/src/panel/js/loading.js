var Loading = {
    create: function (obj) {
        obj.append('<div class="loading"><i class="fas fa-circle-notch fa-spin fa-3x"></i></div>');
    },
    remove: function (obj) {
        obj.find('.loading').remove();
    }
}
