var CodeEditor = {
    enabled: false,
    class: '.code-mirror',
    options: {
        lineNumbers: true,
        mode: 'css'
    },
    hiddenElements: [],
    hiddenElementsInterval: 400,
    instances: [],
    init: function () {
        if ($(this.class).length > 0) {
            this.initialize($(this.class));
            this.checkHiddenElements();
        }
    },
    devError: function () {
        console.error('CodeMirror:', 'Please include CodeMirror plugin.');
    },
    initialize: function (object) {
        if (typeof CodeMirror === 'function') {
            this.enabled = true;
        }
        
        if (!this.enabled) {
            this.devError();
        }

        if (this.enabled && object.length == 1) {
            this.fromTextArea(object[0], this.options);
        } else if (this.enabled && object.length > 1) {
            $.each(object, function(i, textarea) {
                CodeEditor.fromTextArea(textarea, CodeEditor.options);
            });
        }
    },
    fromTextArea: function (element, options) {
        if (this.isElementVisible(element)) {
            var object = $(element);
            if (object.data('readonly')) {
                options['readOnly'] = object.data('readonly');
            }
            if (object.data('mode')) {
                options['mode'] = object.data('mode');
            }
            var codeMirrorInstance = CodeMirror.fromTextArea(element, options);
            this.instances[$(element).attr('id')] = codeMirrorInstance;
            
            return codeMirrorInstance;
        } else {
            this.hiddenElements.push(element);
        }
    },
    isElementVisible: function (element) {
        return $(element).is(':visible');
    },
    checkHiddenElements: function () {
        setInterval(function () {
            if (CodeEditor.hiddenElements.length > 0) {
                $.each(CodeEditor.hiddenElements, function (i, element) {
                    if (CodeEditor.isElementVisible(element)) {
                        CodeEditor.fromTextArea(element, CodeEditor.options);
                        delete CodeEditor.hiddenElements[i];
                    }
                });
            }
        }, this.hiddenElementsInterval);
    }
};

$(document).ready(function () {
    CodeEditor.init();
});