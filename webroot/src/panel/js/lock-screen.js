//sessionlifetime, userEmail, adminPrefix set in head.ctp
var LockScreen = {
    enabled: false,
    lifeTime: null,
    userEmail: null,
    lockScreenId: 'lock-screen',
    progressBarId: 'session-time-bar',
    lockScreenFormId: 'locked-screen-form',
    lockScreenObj: null,
    progressBarObj: null,
    percent: 100,
    currentPercent: null,
    percentWarning: 30,
    submitTries: 0,
    init: function () {
        LockScreen.setLifeTime();
        LockScreen.setUserEmail();
        LockScreen.setObj('lockScreenObj', LockScreen.lockScreenId);
        LockScreen.setObj('progressBarObj', LockScreen.progressBarId);
        LockScreen.setEnabled();
        LockScreen.setCurrentPercent(LockScreen.percent);

        var interval = LockScreen.lifeTime / LockScreen.percent * 1000;

        LockScreen.loop();
        setInterval(function () {
            LockScreen.loop();
        }, interval);

        LockScreen.listeners();
    },
    listeners: function () {
        if (LockScreen.enabled) {
            $('#' + LockScreen.lockScreenFormId).submit(function (e) {
                e.preventDefault();
                LockScreen.submitScreenLock($(this));
            });
        }
    },
    setObj: function (name, id) {
        var obj = $('#' + id);

        if (obj.length > 0) {
            LockScreen[name] = obj;
        }
    },
    setLifeTime: function () {
        if (typeof sessionlifetime !== 'undefined') {
            LockScreen.lifeTime = sessionlifetime;
        }
    },
    setUserEmail: function () {
        if (typeof userEmail !== 'undefined') {
            LockScreen.userEmail = userEmail;
        }
    },
    setEnabled: function () {
        if (LockScreen.lifeTime !== null && LockScreen.userEmail !== null && typeof LockScreen.lockScreenObj === 'object' && typeof LockScreen.progressBarObj === 'object') {
            LockScreen.enabled = true;
        }
    },
    setCurrentPercent: function (percent) {
        LockScreen.currentPercent = percent;
    },
    setProgress: function (width) {
        $('#' + LockScreen.progressBarId).css('width', width + '%');
    },
    loop: function () {
        if (LockScreen.enabled) {
            LockScreen.setProgress(LockScreen.currentPercent);
            LockScreen.currentPercent--;
            LockScreen.checkPercent();
        }
    },
    checkPercent: function () {
        if (LockScreen.currentPercent === LockScreen.percentWarning) {
            LockScreen.displayWarningNotification();
        } else if (LockScreen.currentPercent === 0) {
            LockScreen.clearNotification();
            LockScreen.displayScreenLock();
        }
    },
    resetPercent: function () {
        LockScreen.currentPercent = LockScreen.percent;
    },
    displayWarningNotification: function () {
        warningNotification = Notifications.create('warning', 'Your session is about to expire...', {
            timeout: false,
            buttons: [
                Noty.button('Click here to renew your session', 'btn btn-primary', function () {
                    LockScreen.refreshSession(warningNotification.options.type);
                    warningNotification.close();
                }, {id: 'noty-btn-refresh', 'data-status': 'ok'}),
            ],
        });
    },
    refreshSession: function (type) {
        var data = {};
        if (type === "error") {
            data.message = "Redirect";
        } else if (type === "warning") {
            data.message = "Renew";
        }
        $.ajax({
            url: adminPrefix + '/users/refreshOnAlertClick',
            method: 'POST',
            data: data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRF-Token", Csrf.token);
            }
        }).done(function (resp) {
            if (resp.status === true) {
                LockScreen.resetPercent();
            }
        });
    },
    clearNotification: function () {
        Notifications.closeAll();
    },
    displayScreenLock: function () {
        LockScreen.lockScreenObj.removeClass('invisible').removeClass('hid').addClass('vis');
    },
    hideScreenLock: function () {
        LockScreen.lockScreenObj.removeClass('vis').addClass('hid').addClass('invisible');
    },
    submitScreenLock: function (obj) {
        var formData = obj.serializeArray();
        obj.find('input[type="password"]').val('');
        formData = {
            email: LockScreen.userEmail,
            password: formData[2]['value']
        };

        $.ajax({
            url: adminPrefix + '/users/login',
            method: 'POST',
            data: formData,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRF-Token", Csrf.token);
            }
        }).done(function (resp) {
            if (resp.status === true) {
                LockScreen.hideScreenLock()
                LockScreen.resetPercent();
                LockScreen.submitTries = 0;
            } else {
                LockScreen.submitTries++;
                if (LockScreen.submitTries > 2) {
                    window.location.replace(adminPrefix + "/users/login");
                } else {
                    errorNotification = Notifications.create('error', 'Incorrect data, please try again.');
                }
            }
        });
    }
}

$(document).ready(function () {
    LockScreen.init();
});
