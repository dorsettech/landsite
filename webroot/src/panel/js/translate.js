/**
 * Translate
 *
 * @type object
 */
var Translate = {
    SeoAnalyzer: {
        rowSaved: 'Row updated',
        rowNotSaved: 'Row not saved',
        ogImageUploaded: 'OG images uploaded',
        ogImageNotUploaded: 'OG images not uploaded'
    },
    select2: {
        placeholder: "-- Please choose --"
    },
    propertyAttributes: {
        noAttributes: 'There are no additional options for selected property type.',
        pleaseSelect: 'Please choose property type to display additional options.'
    }
};
