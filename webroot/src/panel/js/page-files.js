var PageFiles = {
    enabled: false,
    class: '.page-files',
    init: function () {
        if ($(this.class).length > 0) {
            this.enabled = true;
        }

        if (this.enabled) {
            this.listeners();
            this.sortable();
        }
    },
    listeners: function () {
        $(document).on('click', this.class + ' .status', function () {
            var id = $(this).data('id');
            PageFiles.switchStatus(id);
        });
    },
    sortable: function () {
        $(this.class).sortable({
            scroll: true,
            tolerance: "pointer",
            revert: false,
            deactivate: function (event, ui) {
                var tab = [];
                var position = 0;
                var pageId = $(this).data('page-id');

                $(this).find('.widget-card-box').each(function () {
                    tab[position] = $(this).data('id');
                    position++;
                });
                PageFiles.saveGalleryOrder(pageId, tab);
            }
        });
        $(this.class).disableSelection();
    },
    saveGalleryOrder: function (pageId, tab) {
        $.ajax({
            type: "POST",
            url: adminPrefix + "/files/saveGalleryFotoOrder",
            data: {'data': tab, 'page_id': pageId},
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success') {
                    Notifications.create('success', 'Gallery order has been updated.');
                } else {
                    Notifications.create('error', 'Gallery order has not been updated.');
                }
            }, error: function () {
                Notifications.create('error', 'An error occured while updating order of files');
            }
        });
    },
    switchStatus: function (id) {
        $.ajax({
            type: "POST",
            url: adminPrefix + "/files/switchStatus",
            data: {'id': id},
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success') {
                    var icon = $("#file-" + id + " .status > i").removeClass('fa-check').removeClass('fa-times');
                    var switchClass = (response.data.active == 1 ? 'fa-check' : 'fa-times');
                    icon.addClass(switchClass);

                    if (response.data.active) {
                        Notifications.create('success', 'File #' + id + ' has been activated');
                    } else {
                        Notifications.create('success', 'File #' + id + ' has been deactivated');
                    }
                }
            }, error: function () {
                Notifications.create('error', 'An error occured while changing active state of file #' + id);
            }
        });
    }
};

$(document).ready(function () {
    PageFiles.init();
});