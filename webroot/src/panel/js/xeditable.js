/**
 * X-Editable
 *
 * @type object
 */
var XEditable = {
    class: {
        editable: '.x-editable',
        appendClass: '.input-group-btn',
        save: '.save',
        cancel: '.cancel',
        showHide: '.showHide',
        rankingList: '.list-unstyled',
        limitFrom: '.limitFrom'
    },
    init: function () {
        this.initDropzone();
        return this;
    },
    listener: function () {
        var _this = this;
        $(this.class.editable).siblings(this.class.appendClass).find('.actions ' + this.class.save).on('click', function (e) {
            $(this).parent().parent().attr('readonly', true);
            _this.saveData($(this).parent().parent().siblings(_this.class.editable));
        });
        $(this.class.editable).siblings(this.class.appendClass).find('.actions ' + this.class.cancel).on('click', function () {
            $(this).parent().parent().siblings(_this.class.editable).attr('readonly', true);
            $(this).parent().hide();
        });
        $(this.class.editable).on('click', function () {
            $(this).attr('readonly', false);
            $(this).siblings(_this.class.appendClass).find('.actions ' + _this.class.save).attr('disabled', false);
            $(this).siblings(_this.class.appendClass).find('.actions').show();
        });
        $(this.class.showHide).on('click', function () {
            $(this).parent().siblings(_this.class.rankingList).toggleClass(function () {
                if ($(this).parent().is(".hide")) {
                    return "show";
                }
                return "hide";
            });
            $(this).toggleClass('fa-chevron-up', 'fa-chevron-down');
        });
        $(this.class.editable).on('keyup', function () {
            $(this).siblings('.input-group-addon').find(_this.class.limitFrom).text($(this).val().length);
        });
        return _this;
    },
    initDropzone: function () {
        var _this = this;
        if ($(".dropzone-seo-analyser").length > 0) {
            $(".dropzone-seo-analyser").each(function (index, item) {
                var handler = $(item).attr('id'),
                    myDropzone = handler + "_" + index,
                    obj = $('#' + handler),
                    myDropzone = new Dropzone('#' + handler, {
                        url: adminPrefix + "/seo-analyser/upload",
                        method: 'post',
                        parallelUploads: 1,
                        maxFilesize: 1,
                        maxFiles: 1,
                        paramName: "og_image",
                        acceptedFiles: 'image/*',
                        headers: Csrf.getHeaders(),
                        autoDiscover: false
                    });
                myDropzone.on("success", function (file, resp) {
                    if (resp.status == 'success') {
                        var id = $(obj).attr('data-id');
                        _this.refreshRaport(resp, id);
                        $("#og_image_" + id).val(resp.og_image);
                    }
                });
                myDropzone.on("complete", function (file) {
                    obj.attr('disabled', false);
                    if (file.dataURL != undefined) {
                        var id = obj.attr('data-id');
                        $('#og_image_thumb_' + id).attr('src', file.dataURL);
                        generateAlert('success', Translate.SeoAnalyzer.ogImageUploaded);
                    } else {
                        generateAlert('error', Translate.SeoAnalyzer.ogImageNotUploaded);
                    }
                });
                myDropzone.on('sending', function (file, xhr, formData) {
                    obj.attr('disabled', true);
                    formData.append('id', obj.attr('data-id'));
                    formData.append('model', obj.attr('data-model'));
                    formData.append('field', obj.attr('data-name'));
                });
            });
        }
    },
    saveData: function (obj) {
        var _this = this;
        $.ajax({
            url: adminPrefix + '/seo-analyser/editable',
            method: 'PUT',
            data: {
                id: $(obj).attr('data-id'),
                field: $(obj).attr('data-name'),
                value: $(obj).val(),
                model: $(obj).attr('data-model')
            }
        }).done(function (resp) {
            $(obj).attr('readonly', true);
            if (resp.status == 'success') {
                generateAlert('success', Translate.SeoAnalyzer.rowSaved);
                $(obj).siblings(_this.class.appendClass).find('.actions').hide();
                _this.refreshRaport(resp, $(obj).attr('data-id'));
            } else {
                generateAlert('error', Translate.SeoAnalyzer.rowNotSaved);
            }
        });
    },
    refreshRaport: function (resp, id) {
        var o = $("#sa-" + id),
            tmp = '';

        o.removeClass('panel-danger').removeClass('panel-success').removeClass('panel-warning');
        o.addClass('panel-' + resp.data.statusLevel);
        o.find('.from').text(resp.data.from);
        o.find('.to').text(resp.data.to)
        o.find("input[name='" + resp.field + "']").val(resp.value);

        for (var key in resp.data.report) {
            tmp += '<li class="text-' + key + '">' + resp.data.report[key].join('<br />') + '</li>';
        }
        o.find('.ranking-list').html(tmp);
    }
};

$(document).ready(function () {
    XEditable.init().listener();
});
