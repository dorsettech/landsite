var Contain = {
    enabled: false,
    maxRows: null,
    classes: {
        contains: '.contains',
        containRow: '.row',
        containFormGroup: '.form-group',
        containAdd: '.add-contain',
        containRemove: '.remove-contain',
        btnSuccess: '.btn-success',
        btnWarning: '.btn-warning',
        icoPlus: '.fa-plus',
        icoTimes: '.fa-times'
    },
    init: function () {
        if ($(this.classes.contains).length > 0) {
            this.enabled = true;
        }
        if (this.enabled) {
            this.setMaxRows();
            this.listeners();
            this.disableFirstFormButton();
        }
    },
    listeners: function () {
        $(this.classes.containAdd).on('click', function () {
            Contain.addContain();
        });
        $(document).on('click', this.classes.containRemove, function () {
            Contain.removeContain($(this));
        });
    },
    setMaxRows: function () {
        this.maxRows = $(this.classes.contains).data('max-rows');

        if (typeof this.maxRows == 'undefined') {
            this.maxRows = null;
        }
    },
    getRows: function () {
        return $(this.classes.contains + ' ' + this.classes.containRow).length;
    },
    addContain: function () {
        if (this.maxRows == null || this.getRows() < this.maxRows) {
            var contains = $(this.classes.contains + ' ' + this.classes.containRow);
            var containsCount = contains.length;
            var clonedRow = contains.first().clone(true);
            clonedRow.find('input').attr('value', '');
            clonedRow.find(this.classes.containRemove).attr('disabled', false);

            clonedRow = clonedRow
                    .prop('outerHTML')
                    .replace(/\[\d\]/g, '[' + containsCount + ']')
                    .replace(this.getClassName(this.classes.icoPlus), this.getClassName(this.classes.icoTimes))
                    .replace(this.getClassName(this.classes.btnSuccess), this.getClassName(this.classes.btnWarning))
                    .replace(this.getClassName(this.classes.containAdd), this.getClassName(this.classes.containRemove));

            $(this.classes.contains).append(clonedRow);
        } else {
            Notifications.create('warning', 'Max no. rows limit is set to ' + this.maxRows)
        }
    },
    removeContain: function (obj) {
        if ($(this.classes.contains + ' ' + this.classes.containFormGroup).length > 1) {
            obj.closest(this.classes.containRow).remove();
        }
    },
    disableFirstFormButton: function () {
        $(this.classes.contains + ' ' + this.classes.containRow).first().find(this.classes.containRemove).attr('disabled', true);
    },
    getClassName: function (className) {
        return className.replace('.', '');
    }
};

$(document).ready(function () {
    Contain.init();
});