var Csrf = {
    token: null,
    tokenName: 'csrfToken',
    init: function () {
        Csrf.setToken();
    },
    setToken: function () {
        Csrf.setTokenFromCookie();
        if (Csrf.token == null) {
            Csrf.setTokenFromForm();
        }
    },
    setTokenFromCookie: function () {
        var cookieToken = Csrf.getCookie(Csrf.tokenName);
        if (cookieToken != '' && cookieToken != 'undefined') {
            Csrf.token = cookieToken;
            Csrf.addToAjaxSetup();
        }
    },
    getCookie: function (cookieName) {
        var name = cookieName + "=",
            decodedCookie = decodeURIComponent(document.cookie),
            ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }

        return "";
    },
    setTokenFromForm: function () {
        csrfInput = $(document).find('input[name="_' + Csrf.tokenName + '"]');
        if (csrfInput.length > 0) {
            Csrf.token = csrfInput.val();
            Csrf.addToAjaxSetup();
        }
    },
    addToAjaxSetup: function () {
        $.ajaxSetup({
            headers : Csrf.getHeaders()
        });
    },
    getHeaders: function() {
        return {
            'X-CSRF-Token': Csrf.token
        }
    }
};

$(document).ready(function () {
    Csrf.init();
});
