$(document).on('click', '.approve-reject-button', function () {
    var id = $(this).data('id');
    $('input[type=radio][name=state]').prop('checked', false);
    if (!$('.reject-reason').hasClass('d-none')) {
        $('.reject-reason').addClass('d-none');
    }
    $('.approve-reject-modal, .article-id').val(id);
    $('input[type=radio][name=state]').change(function () {
        if (this.value === 'REJECTED') {
            $('.reject-reason').removeClass('d-none');
        } else {
            $('.reject-reason').addClass('d-none');
        }
    });
});
