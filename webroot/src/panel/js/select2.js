/**
 * Select2
 * 
 * Initialize by adding class 'select2' (default) to the select attribute.
 * 
 * There's additional attributes which can be set:
 * <ul>
 *  <li>`data-type` - controller which will be requesting for available list. It has to have getList() method. 
 *  (there's a GetListTrait for this)</li>
 *  <li>`min-length` - default set to 2, override it if needed by setting this attribute to any number</li>
 * </ul>
 *
 * @type object
 * @see https://select2.org
 */

var Select = {
    enabled: false,
    class: '.select2',
    data: {},
    init: function () {
        if (typeof jQuery.fn.select2 === 'function') {
            this.enabled = true;
        }

        if ($(this.class).length > 0 && this.enabled) {
            this.initSelects();
        } else if ($(this.class).length > 0 && !this.enabled) {
            this.devError();
        }
    },
    devError: function () {
        console.error('Select2:', 'Please include select2 plugin.');
    },
    initSelects: function () {
        $.each($(this.class), function (index, element) {
            var obj = $(element);
            Select.initSelect2(obj);
        });
    },
    defalutOptions: function () {
        var _this = this;
        return {
            placeholder: Translate.select2.placeholder,
            allowClear: true,
            escapeMarkup: function (markup) {
                return markup;
            },
            maximumSelectionLength: 20,
            minimumInputLength: 2,
            ajax: {
                dataType: 'json',
                method: "POST",
                delay: 1000,
                data: function (params) {
                    _this.data.keyword = params.term;
                    return _this.data;
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: true
            }
        };
    },
    initSelect2: function (obj) {
        var options = $.extend({}, this.defalutOptions());
        var dataType = obj.attr('data-type');
        var minLength = obj.attr('min-length');
        var model = dataType || '';

        if (minLength >= 0) {
            options.minimumInputLength = minLength;
        }

        if (typeof dataType != 'undefined') {
            options.ajax.url = adminPrefix + '/' + model + '/getList.json';
        } else {
            delete options.minimumInputLength;
        }

        if (typeof options.ajax.url == 'undefined') {
            delete options.ajax;
        }

        obj.select2(options);
    }
};

$(document).ready(function () {
    Select.init();
});
