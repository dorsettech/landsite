var ACL = {
    classes: {
        checkInput: '.acl-check',
        toggleModule: '.acl-toggle-module',
        toggleAll: '.acl-toggle-all',
        toggleIndex: '.acl-toggle-index',
        toggleAdd: '.acl-toggle-add',
        toggleEdit: '.acl-toggle-edit',
        toggleDelete: '.acl-toggle-delete',
    },
    init: function () {
        this.listeners();
    },
    listeners: function () {
        $(document).on('click', this.classes.toggleModule, function () {
            ACL.toggleModule(this);
        });
        $(document).on('change', this.classes.checkInput, function () {
            var obj = $(this);
            if (typeof obj.data('related') != 'undefined') {
                ACL.toggleRelated(obj);
            }
        });
        $(document).on('click', this.classes.toggleAll, function () {
            ACL.toggleSpecific($(this), 'all');
        });
        $(document).on('click', this.classes.toggleIndex, function () {
            ACL.toggleSpecific($(this), 'index');
        });
        $(document).on('click', this.classes.toggleAdd, function () {
            ACL.toggleSpecific($(this), 'add');
        });
        $(document).on('click', this.classes.toggleEdit, function () {
            ACL.toggleSpecific($(this), 'edit');
        });
        $(document).on('click', this.classes.toggleDelete, function () {
            ACL.toggleSpecific($(this), 'delete');
        });
    },
    toggleModule: function (element) {
        $(element).parent().parent().find('input:checkbox').not(element).prop('checked', element.checked);
    },
    toggleRelated: function (obj) {
        var related = JSON.parse(atob(obj.data('related')));
        var name = obj.attr('name').split('[');
        var checked = obj.prop('checked');

        $.each(related, function (k, v) {
            var inputName = name[0] + '[' + v + ']';
            $("input[name='" + inputName + "']").prop('checked', checked);
        });
    },
    toggleSpecific: function (obj, type) {
        var value = obj.data('value');
        var element = this.classes.checkInput;
        if (type != 'all') {
            element = element + '-' + type;
        }

        $(element).prop('checked', value);
        $(element).trigger('change');
        $('.acl-toggle-' + type).data('value', !value);
    }
};

$(document).ready(function () {
    ACL.init();
})

