var Attributes = {
    classes: {
        type: '.property-type',
        attributes: '.attributes'
    },
    init: function () {
        if ($(Attributes.classes.type).length > 0 && $(Attributes.classes.attributes).length > 0) {
            this.listeners();
        }
    },
    listeners: function () {
        $(document).on('change', this.classes.type, function(event) {
            Attributes.get($(this));
        });
    },
    get: function(obj) {
        var typeId = obj.val();
        
        Loading.create($(Attributes.classes.attributes));
        $.when(this.getData(typeId)).then(function (results) {
            if (results.status == 'success') {
                Attributes.parseData(results.data, typeId);
            } else {
                if (results.message != '') {
                    Notifications.create('error', results.message);
                }
                Loading.remove($(Attributes.classes.attributes));
            }
        });
    },
    getData: function (typeId) {
        return $.ajax({
            url: adminPrefix + '/property-attributes/get-by-type/' + typeId + '.json',
            method: 'GET'
        });
    },
    parseData: function (data, typeId) {
        var html = '';
        $(this.classes.attributes).html(html);
        $.each(data, function (index, details) {
            html += Attributes.generateHtml(index, details);
        });
        
        if (html == '') {
            html = '<input type="hidden" name="property_attributes[]" class="form-control" id="property-attributes-id" value="">';
            if (typeId == '') {
                html += '<p class="alert alert-info">' + Translate.propertyAttributes.pleaseSelect + '</p>';
            } else {
                html += '<p class="alert alert-warning">' + Translate.propertyAttributes.noAttributes + '</p>';
            }
        }
        
        $(this.classes.attributes).html(html);
        Loading.remove($(Attributes.classes.attributes));
    },
    generateHtml: function (index, details) {
        var html = '';
        
        html += '<input type="hidden" name="property_attributes[' + index + '][id]" class="form-control" id="property-attributes-' + index + '-id" value="' + details.id + '">';
        html += '<div class="checkbox checkbox-css">';
        html += '<input type="hidden" name="property_attributes[' + index + '][_joinData][attribute_value]" class="form-control" value="0">';
        html += '<input type="checkbox" name="property_attributes[' + index + '][_joinData][attribute_value]" value="1" id="property-attributes-' + index + '-joindata-attribute-value">';
        html += '<label class="form-check-label" for="property-attributes-' + index + '-joindata-attribute-value">' + details.name + '</label>';
        html += '</div>';
        
        return html;
    }
}

$(document).ready(function() {
    Attributes.init();
});