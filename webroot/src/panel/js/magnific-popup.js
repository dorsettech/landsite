/**
 * Magnific Popup
 *
 * @type object
 * @see http://dimsemenov.com/plugins/magnific-popup/documentation.html
 */

var MagnificPopup = {
    enabled: false,
    classImage: '.magnific-image',
    classGallery: '.magnific-gallery',
    init: function () {
        var magnificImageAvailable = $(this.classImage).length > 0;
        var magnificGalleryAvailable = $(this.classGallery).length > 0;

        if (typeof jQuery.fn.magnificPopup === 'function') {
            this.enabled = true;
        }
        
        if (this.enabled) {
            if (magnificImageAvailable) {
                $(this.classImage).magnificPopup({type: 'image'});
            }
            if (magnificGalleryAvailable) {
                $(this.classGallery).magnificPopup({
                    delegate: 'div.widget-card-box > a',
                    type: 'image',
                    gallery: {
                        enabled: true
                    }
                });
            }
        } else if (magnificImageAvailable || magnificGalleryAvailable) {
            this.devError();
        }
    },
    devError: function () {
        console.error('Magnific Popup:', 'Please include Magnific Popup plugin.');
    }
};

$(document).ready(function () {
    MagnificPopup.init();
});