/**
 * Swal 
 * 
 * @type object
 * @@see https://sweetalert.js.org/docs/
 */
var Swal = {
    enabled: false,
    class: '.confirm',
    init: function () {
        if (typeof swal === 'function') {
            Swal.enabled = true;
        } else {
            this.devError();
        }

        Swal.listeners();
    },
    devError: function () {
        console.error('Swal:', 'Please include swal plugin.');
    },
    listeners: function () {
        if (Swal.enabled) {
            $(document).on('click', 'a' + Swal.class, function () {
                Swal.elementClick($(this));
            });
        }
    },
    elementClick: function (obj) {
        var confirm = obj.data('confirm');
        var title = obj.data('message');
        var message = obj.data('text');
        var formName = obj.data('form');
        var icon = obj.data('icon');

        if (confirm === 1 && title != '' && formName != '') {
            Swal.submitConfirmation(title, message, formName, icon);
        }
    },
    submitConfirmation: function (title, text, formName, icon) {
        if (icon == undefined) {
            icon = 'error';
        }
        if (Swal.enabled) {
            swal({
                title: title,
                text: text,
                icon: icon,
                buttons: true,
                dangerMode: true,
            }).then(function (confirmed) {
                var form = $('form[name="' + formName + '"]');
                if (confirmed && typeof form !== 'undefined') {
                    form.submit();
                }
            });
        }
    }
}

$(document).ready(function () {
    Swal.init();
});

