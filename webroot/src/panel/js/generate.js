/**
 * eConnect4u
 */
var Generate = {
    enabled: false,
    length: 10,
    characters: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
    classes: {
        button: '.generate',
        target: '.code'
    },
    init: function () {
        if ($(this.classes.button).length > 0) {
            this.enabled = true;
        }

        if (this.enabled) {
            this.listeners();
        }
    },
    listeners: function () {
        $(document).on('click', this.classes.button, function () {
            Generate.setCode($(this));
        });
    },
    generateCode: function (length, characters) {
        var result = '';
        length = length || this.length
        characters = characters || this.characters;
        var charactersLength = characters.length;

        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

        return result;
    },
    setCode: function (obj) {
        length = obj.data('length') || this.classes.length;
        characters = obj.data('characters') || this.classes.characters;
        target = obj.data('target') || this.classes.target;

        var code = this.generateCode(length, characters);

        $(target).val(code);
    }
}

$(document).ready(function () {
    Generate.init();
});


