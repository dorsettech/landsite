$(document).ready(function () {
    var
        $form = $('#report-settings'),
        $button = $('#report-submit');
    $form.submit(function(e) {
        e.preventDefault();
        $button.prop('disabled', true).children('.s').removeClass('hide').next('span').text('Working ...');
        $.post('/' + adminPrefix + '/reports/generate', $form.serialize(), function(data) {
            $button.prop('disabled', false).children('.s').addClass('hide').next('span').text('Generate and download');
            if (data.success) {
                Notifications.create('success', data.msg);
                location.href = '/' + adminPrefix + data.data.url + '?' + Math.random() * 10e15;
            } else {
                Notifications.create('error', data.msg);
            }
        }, 'json');
    });
});
