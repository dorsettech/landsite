$(document).on('click', '.approve-reject-member-button', function () {
    var id = $(this).data('id');
    
    $('input[type=radio][name=state]').prop('checked', false);
    if (!$('.reject-reason').hasClass('d-none')) {
        $('.reject-reason').addClass('d-none');
    }
    $('.approve-reject-member-modal, .user-id').val(id);
    $('input[type=radio][name=state]').change(function () {
        if (this.value === 'REJECTED') {
            $('.reject-reason').removeClass('d-none');
            $('#reject-reason').prop('required', true);
        } else {
            $('.reject-reason').addClass('d-none');
            $('#reject-reason').prop('required', false);
        }
    });
});
