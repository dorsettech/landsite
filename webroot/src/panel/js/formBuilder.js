//var FormBuilder = {
//    row: null,
//    option: null,
//    currentOptionValue: {},
//    currentOptionSelector: null,
//    validators: {
//        max_length: 'ex. 10',
//        min_length: 'ex. 2',
//        max_min_length: 'ex 2/10',
//        decimal: '[0-9]+([\.,][0-9]+)?',
//        phone: '[\+]\d{2}[\(]\d{2}[\)]\d{4}[\-]\d{4}'
//    },
//    mainEmails: [],
//    ccEmails: [],
//    bccEmails: [],
//    init: function () {
//        FormBuilder.checkForm();
//        FormBuilder.events();
//        FormBuilder.listenRedirect($('#thank-you'));
//        FormBuilder.loadExistingEmails();
//    },
//    events: function () {
////add new field
//        $(document).on('click', '.moveUp', FormBuilder.moveFieldUp());
//        $(document).on('click', '.moveDown', FormBuilder.moveFieldDown());
//        $(document).on('click', '#addField', FormBuilder.addField());
//        //remove current field
//        $(document).on('click', '.remove', function () {
//            $(this).parents('tr').remove();
//            FormBuilder.checkForm();
//        });
//        //show modal to create options for select
//        $(document).on('change', '.optionsSelect', function () {
//            if ($(this).val() === 'select' || $(this).val() === 'radio') {
//                $(this).parent().children('.optionsButton').show();
//            } else {
//                $(this).parent().children('.optionsButton').hide();
//            }
//            $(this).parent().children('input[type="hidden"]').val('');
//        });
//        $(document).on('click', '.optionsButton', function () {
//            $('.optionsBody').html('');
//            FormBuilder.currentOptionSelector = $(this).parent().children('input[type="hidden"]');
//            var value = FormBuilder.currentOptionSelector.val();
//            if (value.trim() !== '') {
//                FormBuilder.currentOptionValue = value;
//            }
//            if (typeof FormBuilder.currentOptionValue === 'string'
//                    && typeof JSON.parse(FormBuilder.currentOptionValue) === 'object'
//                    && Object.keys(JSON.parse(FormBuilder.currentOptionValue)).length > 0) {
//                FormBuilder.currentOptionValue = JSON.parse(FormBuilder.currentOptionValue);
//                FormBuilder.buildExistingOption();
//            }
//        });
//        //add new option to modal
//        $(document).on('click', '#addOption', FormBuilder.addOption());
//        //save inserted options 
//        $(document).on('click', '#saveOptions', FormBuilder.saveOptions());
//        //inserting rule into input depends on selected value
//        $(document).on('change', '.validationSelect', function () {
//            FormBuilder.changeRule($(this));
//        });
//        //change name of field required to catch this field in controller by name
//        $(document).on('change', '.inputName', function () {
//            console.log($(this).val());
//            var requiredField = $(this).parents('tr').find('.requiredField');
//            requiredField.attr('name', 'required[' + $(this).val() + ']');
//            requiredField.parent().children('input[type="hidden"]').attr('name', 'required[' + $(this).val() + ']');
//        });
//        $(document).on('click', '.addMainEmail,.addCCEmail,.addBCCEmail', function () {
//            var input = $(this).parent().parent().find('input');
//            FormBuilder.addEmail(input.val(), $(this).data('email'), input);
//        });
//
//        $(document).on('click', '.emailElement', function () {
//            FormBuilder.removeEmail($(this).data('email'), $(this).data('selector'));
//        });
//        $(document).on('click', '#thank-you', function () {
//            FormBuilder.listenRedirect($(this));
//        });
//    },
//
//    listenRedirect: function (element) {
//        if (element.is(':checked')) {
//            $('.redirect').parents('.form-group').show();
//        } else {
//            $('.redirect').parents('.form-group').hide();
//        }
//    },
//
//    addEmail: function (value, selector, input) {
//        if (FormBuilder.validateEmail(value)) {
//            FormBuilder[selector].push(value);
//            FormBuilder.updateEmails(selector);
//            input.val('').css({
//                'border': '1px solid #e5e6e7'
//            });
//        } else {
//            console.log('display error');
//            input.css({
//                'border': '1px solid red'
//            });
//        }
//    },
//    validateEmail: function (email) {
//        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//        return re.test(email);
//    },
//    removeEmail: function (email, selector) {
//        var index = FormBuilder[selector].indexOf(email);
//        if (index != -1) {
//            FormBuilder[selector].splice(index, 1);
//        }
//        FormBuilder.updateEmails(selector);
//    },
//    updateEmails: function (selector) {
//        var list = FormBuilder[selector];
//        var html = '';
//        for (var i = 0; i < list.length; i++) {
//            html += '<button type="button" class="btn btn-default btn-md emailElement" data-selector="' + selector + '" data-email="' + list[i] + '">' + list[i] + ' <i class="removeEmail fa fa-close text-danger"></i></button>';
//        }
//        $('.' + selector).html(html);
//        $('.' + selector + 'Hidden').val(list);
//    },
//
//    loadExistingEmails: function () {
//        var emails = ['mainEmails', 'ccEmails', 'bccEmails'];
//        $.each(emails, function (index, value) {
//            var arrayWithEmails = $('.' + value + 'Hidden').val();
//            if (typeof arrayWithEmails !== 'undefined') {
//                var email = arrayWithEmails.split(',');
//                $.each(email, function (index, singleEmail) {
//                    if (singleEmail.trim() !== '') {
//                        FormBuilder[value].push(singleEmail);
//                    }
//                });
//            }
//        });
//    },
//    addField: function () {
//        if (typeof FormBuilder.row[0] !== 'undefined') {
//            $('.formBuilderBody').append(FormBuilder.row[0].outerHTML);
//        }
//        FormBuilder.checkForm();
//    },
//    addOption: function () {
//        if (typeof FormBuilder.option[0] !== 'undefined') {
//            $('.optionsBody').append(FormBuilder.option[0].outerHTML);
//        }
//    },
//    checkForm: function () {
//        if ($('.formBuilderTable tbody tr').length === 0) {
//            $('.formBuilderTable').hide();
//        } else {
//            $('.formBuilderTable').show();
//        }
//    },
//    saveOptions: function () {
//        var key, value = '';
//        FormBuilder.currentOptionValue = {};
//        $('.optionsBody tr').each(function () {
//            $(this).find('input').each(function () {
//                var id = $(this).attr('id');
//                if (id === 'optionvalue') {
//                    key = $(this).val();
//                } else if (id === 'optionname') {
//                    value = $(this).val();
//                } else {
//                    key = value = '';
//                }
//            });
//            FormBuilder.currentOptionValue[key] = value;
//        });
//        FormBuilder.currentOptionSelector.val(JSON.stringify(formBuilder.currentOptionValue));
//        $('.optionsBody').html('');
//        $('#optionModalDialog').modal('hide');
//    },
//    buildExistingOption: function () {
//        for (var key in FormBuilder.currentOptionValue) {
//            var option = FormBuilder.option.clone();
//            option.find('input').each(function () {
//                if ($(this).attr('id') === 'optionvalue') {
//                    $(this).val(key);
//                } else if ($(this).attr('id') === 'optionname') {
//                    $(this).val(FormBuilder.currentOptionValue[key]);
//                }
//            });
//            $('.optionsBody').append(option.addClass('asd'));
//        }
//    },
//    changeRule: function (validationSelect) {
//        var value = validationSelect.val();
//        var ruleInput = validationSelect.parents('tr').find('.rule');
//        if (typeof ruleInput[0] !== 'undefined') {
//            if (typeof FormBuilder.validators[value] !== 'undefined') {
//                ruleInput.val(FormBuilder.validators[value]);
//            } else {
//                ruleInput.val('');
//            }
//        }
//    },
//    moveFieldUp: function () {
//        var element = $(this).parents('tr');
//        var prev = element.prev();
//        element.insertBefore(prev);
//    },
//    moveFieldDown: function () {
//        var element = $(this).parents('tr');
//        var next = element.next();
//        element.insertAfter(next);
//    }
//};
//$(document).ready(function () {
//    FormBuilder.init();
//});
//$(window).load(function () {
//    FormBuilder.row = $('#row tbody > *').clone();
//    FormBuilder.option = $('#option tbody > *').clone();
//});