/**
 * eConnect4u
 */

var Discounts = {
    enabled: false,
    classes: {
        type: '.discount-type',
        amount: '.discount-amount'
    },
    init: function () {
        if ($(this.classes.type).length > 0) {
            this.enabled = true;
        }

        if (this.enabled) {
            Discounts.setByType(Discounts.getType($(this.classes.type)));
            this.listeners();
        }
    },
    listeners: function () {
        $(document).on('change', this.classes.type, function () {
            Discounts.setByType(Discounts.getType($(this)));
        });
    },
    getType: function (obj) {
        return obj.val();
    },
    setByType: function (type) {
        this.resetAmountField();
        
        switch (type) {
            case 'FIXEDSUM':
                this.setFixedSumAmountField();
                break;
            case 'FREE': 
                this.setFreeAmountField();
                break;
            case 'PERCENTAGE':
                this.setPercentageAmountField();
                break;
        }
    },
    resetAmountField: function() {
        $(this.classes.amount).attr('min', 0);
        $(this.classes.amount).attr('max', '');
        $(this.classes.amount).attr('step', '1');
        $(this.classes.amount).attr('readonly', false);
    },
    setFixedSumAmountField: function () {
        $(this.classes.amount).attr('step', '0.01');
    },
    setFreeAmountField: function () {
        $(this.classes.amount).val('');
        $(this.classes.amount).attr('readonly', true);
    },
    setPercentageAmountField: function () {
        $(this.classes.amount).attr('max', 100);
    },
}


$(document).ready(function () {
    Discounts.init();
});
