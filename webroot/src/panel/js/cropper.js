var Cropp = {
    enabled: false,
    cropper: null,
    submit: false,
    class: {
        imageSource: '.img-crop',
        imagePreview: '.img-preview',
        form: '.form-crop',
        inputImage: '.input-image',
        btnSave: '.btn-save',
        btnZoomIn: '.btn-zoom-in',
        btnZoomOut: '.btn-zoom-out',
        btnRotateLeft: '.btn-rotate-left',
        btnRotateRight: '.btn-rotate-right',
        btnRatio: '.btn-ratio',
    },
    rotateDegree: 15,
    options: {
        preview: '.img-preview',
        viewMode: 2,
    },
    init: function () {
        if (typeof Cropper === 'function') {
            this.enabled = true;
        }

        var image = $(this.class.imageSource);
        if (image.length > 0) {
            this.create(image[0], this.options);
        }

        this.listeners();
    },
    listeners: function () {
        if (this.enabled) {
            $(document).on('click', this.class.btnSave, function (event) {
                var croppedImage = Cropp.cropper.getCroppedCanvas().toDataURL();
                $(Cropp.class.inputImage).val(croppedImage);                
                $(Cropp.class.form).submit();
            });
            $(document).on('click', this.class.btnZoomIn, function () {
                Cropp.cropper.zoom(0.1);
            });
            $(document).on('click', this.class.btnZoomOut, function () {
                Cropp.cropper.zoom(-0.1);
            });
            $(document).on('click', this.class.btnRotateLeft, function () {
                Cropp.cropper.rotate(-Cropp.rotateDegree);
            });
            $(document).on('click', this.class.btnRotateRight, function () {
                Cropp.cropper.rotate(Cropp.rotateDegree);
            });
            $(document).on('click', this.class.btnRatio, function () {
                var ratio = $(this).data('ratio');
                Cropp.cropper.setAspectRatio(ratio);
                $(Cropp.class.btnRatio).removeClass('active');
                $(this).addClass('active');
            });
        }
    },
    create: function (image, options) {
        if (this.enabled) {
            this.cropper = new Cropper(image, options);
            this.scalePreviewImage(image);
            
            if (typeof aspectRatioView != 'undefined') {
                Cropp.cropper.setAspectRatio(aspectRatioView);
            }
        }
    },
    scalePreviewImage: function (image) {
        image.addEventListener('crop', function (event) {
            var maxWidth = $(Cropp.class.imagePreview).width();
            var width = event.detail.width / 2;
            var height = event.detail.height / 2;
            var ratio = maxWidth * 100 / width / 100;

            width = width * ratio;
            height = height * ratio;

            $(Cropp.class.imagePreview).css("width", width + 'px');
            $(Cropp.class.imagePreview).css("height", height + 'px');
            $(Cropp.class.imagePreview).css("min-height", height + 'px');
        });
    }
};

$(document).ready(function () {
    Cropp.init();
});