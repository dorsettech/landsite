var DatePicker = {
    enabled: false,
    domElements: {
        standard: '.date'
    },
    format: 'YYYY-MM-DD',
    init: function () {
        if (typeof jQuery.fn.datetimepicker === 'function') {
            this.enabled = true;
        }
        
        if (this.enabled) {
            this.elements();
        } else {
            this.devError();
        }
    },
    devError: function () {
        console.error('Datepicker:', 'Please include datetimepicker plugin.');
    },
    elements: function () {
        if ($(this.domElements.standard).length > 0) {
            $(this.domElements.standard).datetimepicker({
                format: this.format
            });
        }
    }
};

$(document).ready(function () {
    DatePicker.init();
});