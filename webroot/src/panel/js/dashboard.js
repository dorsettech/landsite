var AppDashboard = {
    id: {
        websiteAnalytics: '#website-analytics',
        detailAnalytics: '#detail-analytics',
        plotTooltip: 'plot-tooltip',
    },
    colors: {
        users: COLOR_GREEN_DARKER,
        newUsers: COLOR_GREEN_LIGHTER,
        sessions: COLOR_BLUE_LIGHTER,
        avgSessionDuration: COLOR_BLUE_DARKER,
        bounces: COLOR_ORANGE_DARKER,
        bounceRate: COLOR_ORANGE_LIGHTER,
        pageviews: COLOR_PURPLE_LIGHTER,
    },
    sparklineOptions: {
        width: '100%',
        height: '23px',
        fillColor: 'transparent',
        type: 'line',
        lineWidth: 2,
        spotRadius: '4',
        lineColor: COLOR_BLUE,
        highlightLineColor: COLOR_BLUE,
        highlightSpotColor: COLOR_BLUE,
        spotColor: false,
        minSpotColor: false,
        maxSpotColor: false
    },
    init: function () {
        this.handleWebsiteAnalytics();
        this.handleDetailAnalytics();
    },
    getWebsiteAnalyticsData: function () {
        return $.ajax({
            url: adminPrefix + '/dashboard/get-website-analytics-data.json',
            method: 'GET'
        });
    },
    getDetailAnalyticsData: function () {
        return $.ajax({
            url: adminPrefix + '/dashboard/get-detail-analytics-data.json',
            method: 'GET'
        });
    },
    handleWebsiteAnalytics: function () {
        var websiteAnalytics = $(AppDashboard.id.websiteAnalytics);
        if (typeof $.plot == 'function' && websiteAnalytics.length === 1) {
            Loading.create(websiteAnalytics);
            $.when(this.getWebsiteAnalyticsData()).then(function (results, textStatus, jqXHR) {
                if (textStatus == 'success') {
                    Loading.remove(websiteAnalytics);
                    AppDashboard.plotInit(websiteAnalytics, results);
                }
            });
        }
    },
    plotInit: function (obj, results) {
        var details = this.formatData(results);
        var previousPoint = null;

        $.plot(obj, details.data, details.config);
        obj.bind('plothover', function (event, pos, item) {
            $("#x").text(pos.x.toFixed(2));
            $("#y").text(pos.y.toFixed(2));
            if (item) {
                if (previousPoint !== item.dataIndex) {
                    previousPoint = item.dataIndex;
                    $('#' + AppDashboard.id.plotTooltip).remove();
                    var y = item.datapoint[1].toFixed(2);

                    var content = item.series.label + " " + y;
                    AppDashboard.showTooltip(item.pageX, item.pageY, content);
                }
            } else {
                $('#' + AppDashboard.id.plotTooltip).remove();
                previousPoint = null;
            }
            event.preventDefault();
        });
    },
    formatData: function (data) {
        var details = {data: [], config: {}};
        var xLabel = [];
        var yMax = 20;

        $.each(data.results, function (index, array) {
            details.data.push({
                data: array.data,
                label: array.title,
                color: AppDashboard.colors[index],
                lines: {show: true, fill: false, lineWidth: 2},
                points: {show: true, radius: 3, fillColor: COLOR_WHITE},
                shadowSize: 0
            });

            if (xLabel.length == 0) {
                xLabel = array.xLabel;
            }
            if (array.maxValue > yMax) {
                yMax = parseInt(array.maxValue) + (parseInt(array.maxValue) * 0.1);
                yMax = Math.ceil(yMax / 100) * 100;
            }
        });

        details.config = {
            xaxis: {ticks: xLabel, tickDecimals: 0, tickColor: COLOR_BLACK_TRANSPARENT_2},
            yaxis: {ticks: 10, tickColor: COLOR_BLACK_TRANSPARENT_2, min: 0, max: yMax},
            grid: {
                hoverable: true,
                clickable: true,
                tickColor: COLOR_BLACK_TRANSPARENT_2,
                borderWidth: 1,
                backgroundColor: 'transparent',
                borderColor: COLOR_BLACK_TRANSPARENT_2
            },
            legend: {
                labelBoxBorderColor: COLOR_BLACK_TRANSPARENT_2,
                margin: 10,
                noColumns: 1,
                show: true
            }
        };

        return details;
    },
    showTooltip: function (x, y, contents) {
        $('<div id="' + AppDashboard.id.plotTooltip + '" class="flot-tooltip">' + contents + '</div>').css({
            top: y - 45,
            left: x - 55
        }).appendTo("body").fadeIn(200);
    },
    handleDetailAnalytics: function () {
        var detailAnalytics = $(AppDashboard.id.detailAnalytics);
        if (detailAnalytics.length === 1) {
            Loading.create(detailAnalytics);
            $.when(this.getDetailAnalyticsData()).then(function (results, textStatus, jqXHR) {
                if (textStatus == 'success') {
                    AppDashboard.renderDetails(detailAnalytics, results);
                }
            });
        }
    },
    renderDetails: function (obj, results) {
        $.when(this.detailHtml(results)).then(function (html) {
            Loading.remove(obj);
            obj.append(html);
            $.each(results.results, function (index, array) {
                AppDashboard.sparklineInit(index, array.data)
            });
        });
    },
    detailHtml: function (data) {
        var html = '';
        html += '<div class="table-responsive">';
        html += '<table class="table table-valign-middle">';
        html += '<thead><tr>';
        html += '<th>Source</th>';
        html += '<th>Total</th>';
        html += '<th>Trend</th>';
        html += '</tr></thead>';
        html += '<tbody>';

        $.each(data.results, function (index, array) {
            html += '<tr>';
            html += '<td><label class="label" style="background: ' + AppDashboard.colors[index] + '; color: #ffffff">' + array.title + '</label></td>';
            switch (array.type) {
                case 'PERCENT':
                    html += '<td>' + Math.round(array.value * 100) / 100 + '%</td>';
                    break;
                case 'TIME':
                    html += '<td>' + Math.round(array.value * 100) / 100 + 's</td>';
                    break;
                default:
                    html += '<td>' + array.value + '</td>';
                    break;
            }
            html += '<td><div id="sparkline-' + index + '" class="sparkline-chart"></div></td>';
            html += '</tr>';
        });

        html += '</tbody>';
        html += '</table>';
        html += '</div>';

        return html;
    },
    sparklineInit: function (sparklineId, data) {
        var options = $.extend({}, this.sparklineOptions);
        var sparklineObj = $('#sparkline-' + sparklineId);

        if (sparklineObj.length == 1) {
            var countWidth = sparklineObj.width();
            if (countWidth >= 200) {
                options.width = '200px';
            } else {
                options.width = '100%';
            }

            var color = AppDashboard.colors[sparklineId];

            if (color != 'undefined') {
                options.lineColor = color;
                options.highlightLineColor = color;
                options.highlightSpotColor = color;
            }

            sparklineObj.sparkline(data, options);
        }
    }
};

$(document).ready(function () {
    AppDashboard.init();
});