/**
 * Translate
 *
 * @type object
 */
var Translate = {
    propertyAttributes: {
        noAttributes: 'There are no additional options for selected property type.',
        pleaseSelect: 'Please choose property type to display additional options.'
    }
};
