//
// add header class if slider is on page
//

if ($('section').hasClass('slider')) {
    $('header').addClass('transparent');
}

//
// add class to header when scroll down
//

$(function () {
    //caches a jQuery object containing the header element
    var header = $("header");
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll >= 60) {
            header.addClass("scroll");
        } else {
            header.removeClass("scroll");
        }
    });

    window.addEventListener('load', function () {
        var scroll = $(window).scrollTop();
        if (scroll >= 60) {
            $("header").addClass('scroll');
        }
        //no removing needed cause refresh did it
    });
});

//
// slick sliders & carousels
//

if ($('.slider.big .carousel').length > 0) {
    $('.slider.big .carousel').slick({
        infinite: true,
        slidesToShow: 1,
        dots: false,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 5000,
        pauseOnHover: false,
    });
}

if ($('.slider.slim .carousel').length > 0) {
    $('.slider.slim .carousel').slick({
        infinite: true,
        slidesToShow: 1,
        dots: false,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 5000,
        pauseOnHover: false,
    });
}

if ($('.offerboxes .carousel').length > 0) {
    $('.offerboxes .carousel').slick({
        infinite: true,
        slidesToShow: 4,
        dots: true,
        prevArrow: '<div class="prev-arrow slick-arrow"><button class="arrow-button"></button></div>',
        nextArrow: '<div class="next-arrow slick-arrow"><button class="arrow-button"></button></div>',
        responsive: [
            {
                breakpoint: 1600,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });
}

if ($('.posts .carousel').length > 0) {
    $('.posts .carousel').slick({
        infinite: true,
        slidesToShow: 4,
        dots: false,
        arrows: false,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    dots: true,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    dots: true,
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                }
            }
        ]
    });
}

if ($('.profile .carousel').length > 0) {
    $('.profile .carousel').slick({
        infinite: true,
        slidesToShow: 3,
        dots: false,
        arrows: true,
        prevArrow: '<div class="prev-arrow slick-arrow"><button class="arrow-button"></button></div>',
        nextArrow: '<div class="next-arrow slick-arrow"><button class="arrow-button"></button></div>',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    centerMode: true,
                    centerPadding: '20px',
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 1,
                    centerMode: true,
                    centerPadding: '20px',
                }
            }
        ]
    });
}

if ($('.lightbox').length > 0) {
    $('.lightbox').slickLightbox({
        src: 'src',
        itemSelector: 'img',
        navigateByKeyboard: true,
        layouts: {
            closeButton: '<button class="close"><i class="fas fa-times"></i></button>'
        }
    });
}

if ($('.properties .details .carousel').length > 0) {
    $('.properties .details .big.carousel').slick({
        infinite: true,
        slidesToShow: 1,
        dots: false,
        arrows: false,
        asNavFor: '.details .small.carousel'
    });
    $('.properties .details .small.carousel').slick({
        infinite: true,
        slidesToShow: 4,
        dots: false,
        arrows: false,
        asNavFor: '.details .big.carousel',
        focusOnSelect: true,
    });
}

if (('.sidebar.sidebar2 .carousel').length > 0) {
    $('.sidebar.sidebar2 .carousel').slick({
        infinite: true,
        slidesToShow: 1,
        dots: true,
        arrows: false,
        mobileFirst: true,
        responsive: [
            {
                breakpoint: 1599,
                settings: "unslick",
            }
        ]
    });
}

//
// change select to custom
//
$(".custom-select").each(function () {
    var classes = $(this).attr("class"),
            id = $(this).attr("id"),
            name = $(this).attr("name"),
            placeholder = $(this).attr("placeholder");

    var options = '';
    $(this).find("option").each(function () {
        if ($(this).attr('selected')) {
            placeholder = $(this).html();
        }
        options += '<span class="custom-option" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
    });

    var template = '<div class="' + classes + '">';
    template += '<span class="custom-select-trigger">' + placeholder + '</span>';
    template += '<div class="custom-options">' + options + '</div>';
    template += '</div>';

    $(this).wrap('<div class="custom-select-wrapper form-control"></div>'); // added "form-control" class
    $(this).after(template);
});
$(".custom-select-trigger").on("click", function (event) {
    $('html').one('click', function () {
        $(".custom-select").removeClass("opened");
    });
    $('.custom-select.opened').not($(this).parent()).each(function () {
        $(this).toggleClass('opened');
    });
    $(this).parents(".custom-select").toggleClass("opened");
    event.stopPropagation();
});
$(".custom-option").on("click", function () {
    var select = $(this).parents(".custom-select-wrapper ").find("select");
    select.val($(this).data("value"));
    select.trigger('change');
    $(this).parents(".custom-select").removeClass("opened");
    $(this).parents(".custom-select").find(".custom-select-trigger").text($(this).text());
});

//
// close section search collapse
//

if (('#search_section').length > 0) {
    $("#search_section").on("click", function (event) {
        // $(window).on('click', function(event) {
        //     $("#search_button").collapse('show');
        //     $("#search_form").collapse('hide');
        // });
        // $('#search_form').click(function(event){
        //     event.stopPropagation();
        // });

        $(document).on('click', function (event) {
            var menu_opened = $('#search_form').hasClass('show');
            if (!$(event.target).closest('#search_form').length &&
                    !$(event.target).is('#search_form') &&
                    menu_opened === true) {
                $("#search_button").collapse('show');
                $("#search_form").collapse('hide');
                if (('#advancedsearch').length > 0) {
                    $("#advancedsearch").collapse('hide');
                }
            }
        });
    });
}

//
// scrollto elements
//

if (('.scrollto').length > 0) {
    $(".scrollto").click(function (event) {
        event.preventDefault();
        if ($(window).width() < 992) {
            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top
            }, 'slow');
        } else {
            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top - 69
            }, 'slow');
        }
    });
}

//
// tooltips
//

if (('[data-toggle="tooltip"]').length > 0) {
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
}

//
// #createaccount-step2 checkradio input
//

if (('#createaccount-step2').length > 0) {
    $('[for="specificlocation"]').click(function () {
        $('#createaccount-step2 #specificlocation').prop('checked', true)
    });
}

//
// #createaccount-step2 checkradio input
//

if (('#createaccount .selectdropdown').length > 0) {
    $('.selectdropdown').select2({
        placeholder: 'Select a specific industry...',
        width: '100%',
        containerCssClass: "selectdropdown",
    });
}
if ($('.save-professional').length > 0) {
    $(document).on('submit', 'form.save-professional', function (e) {
        e.preventDefault();
        var form = new FormData($(this)[0]);
        $.ajax({
            url: '/professional-services/save',
            type: 'post',
            data: form,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (data) {
                if (data.status) {
                    if (data.status === 200) {
                        swalMessage.display(data.message, 'success');
                        $('.save-professional').remove();
                    } else if (data.status === 500) {
                        swalMessage.display(data.message, 'error');
                    }
                } else {
                    swalMessage.display('An error occurred. Try again', 'error');
                }
            },
        });
    });
}
if ($('.save-property').length > 0) {
    $(document).on('submit', 'form.save-property', function (e) {
        e.preventDefault();
        var property_id = $(this).data('id');
        var form = new FormData($(this)[0]);
        $.ajax({
            url: '/properties/save',
            type: 'post',
            data: form,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (data) {
                if (data.status) {
                    if (data.status === 200) {
                        swalMessage.display(data.message, 'success');
                        $('#save-button-' + property_id).remove();
                        $('#saved-info-' + property_id).removeClass('d-none');
                    } else if (data.status === 500) {
                        swalMessage.display(data.message, 'error');
                    }
                } else {
                    swalMessage.display('An error occurred. Try again', 'error');
                }
            },
        });
    });
}
if ($('.service-clicks').length) {
    $(document).on('click', '.service-clicks', function (e) {
        e.preventDefault();
        var url = $(this).attr('href'),
            data = {
                service_id: $(this).data('serviceId'),
                element: $(this).data('elementId')
            };
        $.ajax({
            url: '/professional-services/save-clicks',
            type: 'post',
            data: data,
            cache: false,
            success: function (data) {
                if (!data.status) return;
                if (data.status === 200) {
                    window.open(url, data.element == 0 ? '_blank' : '_self');
                } else if (data.status === 500) {
                    swalMessage.display('Something went wrong', 'error');
                }
            },
        });
    });
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
$(window).on('load', function () {
    if (getUrlParameter('enquiry') == '1') {
        if ($('#propertymodal').length > 0) {
            $('#propertymodal').modal('show');
        }
        if ($('#servicesmodal').length > 0) {
            $('#servicesmodal').modal('show');
        }
    }
});

if ($('.submit-onchange').length > 0) {
    $(document).on('change', '.submit-onchange', function () {
        $(this).closest('form').submit();
    });
}

if ($('#servicesmodalclear').length > 0) {
    $(document).on('click', '.service-contact-modal', function () {
        var data = {
            service_id: $(this).data('service-id'),
            title: $(this).data('title'),
            recipient_name: $(this).data('recipient-name'),
            recipient_email: $(this).data('recipient-email'),
            profile_link: $(this).data('profile-link'),
            image: $(this).data('image')
        };
        $('#servicesmodalclear .heading').text(data.title);
        $('#servicesmodalclear form #service-id').val(data.service_id);
        $('#servicesmodalclear form #recipient-name').val(data.recipient_name);
        $('#servicesmodalclear form #recipient-email').val(data.recipient_email);
        $('#servicesmodalclear form #profile-link').val(data.profile_link);
        if (data.image != '') {
            $('#servicesmodalclear .bottominfo .image').html('<img src="' + data.image + '" alt="' + data.title + '">');
        } else {
            $('#servicesmodalclear .bottominfo .image').html('');
        }
    });
}

function initLocation() {

  var input = /** @type {!HTMLInputElement} */(
      document.getElementById('validation03'));
  var autocomplete = new google.maps.places.Autocomplete(input, {
    types: ['(cities)'],
    componentRestrictions: {
      country: 'GB'
    }
  });
}

initLocation();
