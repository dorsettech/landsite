var Attributes = {
    classes: {
        type: '.property-type',
        attributes: '.attributes'
    },
    init: function () {
        if ($(Attributes.classes.type).length > 0 && $(Attributes.classes.attributes).length > 0) {
            this.listeners();
        }
    },
    listeners: function () {
        $(document).on('change', this.classes.type, function(event) {
            Attributes.get($(this));
        });
    },
    get: function(obj) {
        var typeId = obj.val();
        
        Loading.create($(Attributes.classes.attributes));
        $.when(this.getData(typeId)).then(function (results) {
            if (results.status == 'success') {
                Attributes.parseData(results.data, typeId);
            } else {
                if (results.message != '') {
                    Notifications.create('error', results.message);
                }
                Loading.remove($(Attributes.classes.attributes));
            }
        });
    },
    getData: function (typeId) {
        return $.ajax({
            url: '/property-attributes/get-by-type/' + typeId + '.json',
            method: 'GET'
        });
    },
    parseData: function (data, typeId) {
        var html = '';
        $(this.classes.attributes).html(html);
        $.each(data, function (index, details) {
            html += Attributes.generateHtml(index, details);
        });
        
        if (html == '') {
            html = '';
            if (typeId == '') {
                html += '<p>' + Translate.propertyAttributes.pleaseSelect + '</p>';
            } else {
                html += '<p>' + Translate.propertyAttributes.noAttributes + '</p>';
            }
        }
        
        $(this.classes.attributes).html(html);
        Loading.remove($(Attributes.classes.attributes));
    },
    generateHtml: function (index, details) {
        var html = '';
        
        html += '<div class="checkbox checkbox-css">';
        html += '<input type="hidden" name="attributes[' + details.id + ']" class="form-control" value="0">';
        html += '<input type="checkbox" name="attributes[' + details.id + ']" value="1" id="attributes-' + index + '">';
        html += '<label class="form-check-label" for="attributes-' + index + '">' + details.name + '</label>';
        html += '</div>';
        
        return html;
    }
}

$(document).ready(function() {
    Attributes.init();
});