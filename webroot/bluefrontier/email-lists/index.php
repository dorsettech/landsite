<?php
require(__DIR__  . '/../../../bluefrontier.php');

$db_conn = \BlueFrontier\Helper::dbConnection();
if (!$db_conn)
	die('No Database Connection.');

$cron_path = dirname(dirname(dirname(__DIR__)))  . '/bin/cake.php';




/*
 * Email List Types
 */
$email_types = [];
$sql = "SELECT * FROM `email_lists`
		 ORDER BY `id` ASC
		";
$emails = @mysqli_query($db_conn, $sql);
if ($emails)
{
	while ($email = $emails->fetch_assoc())
	{
		$email_types[] = $email;
	}
}




/*
 * AJAX postback
 */
$action = (isset($_POST['action']) ? $_POST['action'] : null);
if ($action)
{

	if ($action == 'email_list')
	{
		$list_id     = (isset($_POST['list_id'])     ? intval($_POST['list_id'])      : null);
		$list_active = (isset($_POST['list_active']) ? intval($_POST['list_active']) : false);
		if ($list_id)
		{
			// Set list state
			$sql = "UPDATE `email_lists`
					   SET `active` = " . ($list_active ? 1 : 0) . " 
					 WHERE `id`    = '" . $list_id . "' 
						";
var_dump($sql);
			mysqli_query($db_conn, $sql);
		}
	}
	
	if ($action == 'add')
	{
		$user_id    = (isset($_POST['user_id'])    ? intval($_POST['user_id']) : null);
		$user_email = (isset($_POST['user_email']) ? $_POST['user_email']      : null);
		if ($user_id && $user_email)
		{
			// Opt in
			$sql = "UPDATE `users`
					   SET `email_queue_imported` = 0 
					     , `email_queue_optin` = 1 
					 WHERE `id`    = '" . $user_id . "' 
					   AND `email` = '" . mysqli_real_escape_string($db_conn, $user_email) . "' 
						";
			mysqli_query($db_conn, $sql);
			// Run 'cron' script
			ob_start();
			\BlueFrontier\Helper::membersToNewsletter();
			ob_end_clean();
		}
	}
	
	if ($action == 'remove')
	{
		$user_id    = (isset($_POST['user_id'])    ? intval($_POST['user_id']) : null);
		$user_email = (isset($_POST['user_email']) ? $_POST['user_email']      : null);
		if ($user_id && $user_email)
		{
			// Opt out
			$sql = "UPDATE `users`
					   SET `email_queue_imported` = -1 
					     , `email_queue_optin` = 0 
					 WHERE `id`    = '" . $user_id . "' 
					   AND `email` = '" . mysqli_real_escape_string($db_conn, $user_email) . "' 
						";
			mysqli_query($db_conn, $sql);
			// Remove from email queue
			$sql = "DELETE FROM `email_queue_members`
					 WHERE `email` = '" . mysqli_real_escape_string($db_conn, $user_email) . "' 
						";
			mysqli_query($db_conn, $sql);
		}
	}
	
	exit('Done!');
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title></title>

	<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/css/jquery.dataTables.min.css">
	
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/popper.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
	<script src="../assets/js/jquery.dataTables.min.js"></script>
	<script src="../assets/js/dataTables.buttons.min.js"></script>
	<script src="../assets/js/buttons.html5.min.js"></script>
	<script src="../assets/js/jszip.min.js"></script>
	<style>
	body { padding: 20px; font-size: 14px; }
	table { width: 100%; visibility: hidden; }
	input[type=checkbox] { transform: scale(1.5); margin: 5px; }
	.dt-button.buttons-html5 {
		margin-bottom: 10px;
	}
	</style>
</head>
<body>

<script>
var this_url = '<?php echo $_SERVER["REQUEST_URI"]; ?>';
function email_list_toggle(elem)
{
	var list_id     = $(elem).val();
	var list_active = ($(elem).prop('checked') ? 1 : 0);
	var data = {
				action     : 'email_list', 
				list_id    : list_id, 
				list_active: list_active 
				};
	$.post( this_url, data)
	.done(function( data ) {
	});
}
function user_email_add(elem, user_id, user_email)
{
	var data = {
				action    : 'add', 
				user_id   : user_id, 
				user_email: user_email 
				};
	$.post( this_url, data)
	.done(function( data ) {
		location.reload();
		//var $tr    = $(elem).closest('tr');
		//$tr.find('td.status').html('&#x2714;');
	});
}
function user_email_remove(elem, user_id, user_email)
{
	var data = {
				action    : 'remove', 
				user_id   : user_id, 
				user_email: user_email 
				};
	$.post( this_url, data)
	.done(function( data ) {
		location.reload();
		/*
		var $table = $(elem).closest('table').DataTable();
		var $tr    = $(elem).closest('tr');
		$table
			.row( $tr )
			.remove()
			.draw();
		*/
	});
}
</script>
	
	
	<h1>Email List Management</h1>
	<br/>
	<br/>
	<fieldset>
	<?php
		foreach ($email_types as $email_type) :
			?>
			<div>
				<label class="checkbox-inline">
					<input type="checkbox" value="<?php echo $email_type['id']; ?>" onchange="email_list_toggle(this);" <?php echo (boolval($email_type['active']) ? 'checked="checked"' : '' ); ?>> 
					<?php echo ucwords(strtolower( $email_type['name'] )); ?>
				</label>
			</div>
			<?php
		endforeach;
	?>
	</fieldset>
	<br/>
	<br/>
	
	
	<h2>Add Users to Email List</h2>
	<br/>
	<br/>
	<?php
	$sql = "SELECT `u`.* 
			     , `ud`.`pref_buying`, `ud`.`pref_selling`, `ud`.`pref_professional`, `ud`.`pref_insights` 
			  FROM `users` AS `u`
				INNER JOIN `user_details` AS `ud` ON (`ud`.`user_id` = `u`.`id`) 
			WHERE `u`.`group_id` = " . \BlueFrontier\Helper::GROUP_MEMBERS . " 
			  AND `u`.`email_verified` = 1 
			  AND `u`.`active` = 1 
			  AND `u`.`deleted` = 0 
			ORDER BY `u`.`email` ASC, `u`.`last_name` ASC, `u`.`first_name` ASC
			";
	$users = @mysqli_query($db_conn, $sql);
	if ($users) :
	?>
		<table id="data-table-add" class="table table-striped">
		<thead>
			<tr>
				<th>Name</th>
				<th>Email Address</th>
				<th width="15%">Preferences</th>
				<th width="15%">Opt-in</th>
				<th width="15%">Action</th>
			</tr>
		</thead>
		<tbody>
		<?php
		// - build email data
		while ($user = $users->fetch_assoc()) :
			?>
			<tr>
				<td><?php echo $user['first_name'] . ' ' . $user['last_name']; ?></td>
				<td><a href="mailto:<?php echo $user['email']; ?>"><?php echo $user['email']; ?></a></td>
				<td><?php
					foreach ($email_types as $email_type)
					{
						if (isset($user[ $email_type['pref_key'] ]) && boolval($user[ $email_type['pref_key'] ]) === true)
						{
							echo ucwords(strtolower( $email_type['name'] )) . '<br/>';
						}
					}
				?></td>
				<td class="status"><?php
					if ($user['email_queue_optin'] == '1')
						echo '&#x2714;';
					else
						echo '&#x2716;';
					?></td>
				<td><a class="btn btn-outline-secondary btn-sm" onclick="user_email_add(this, '<?php echo $user['id']; ?>', '<?php echo htmlentities($user['email']); ?>')">Add User</td>
			</tr>
			<?php
		endwhile;
		?>
		</tbody>
		</table>
		<script>
		$(document).ready( function () {
			$('#data-table-add')
				.css('visibility', 'visible')
				.DataTable({
					stateSave: true,
					dom: 'Blfrtip',
					buttons: [
								{ extend: 'copyHtml5', text: 'Copy to clipboard', exportOptions: {columns:[0,1]} },
								{ extend: 'excelHtml5', text: 'Export to Excel', filename:'users', exportOptions: {columns:[0,1]} },
								{ extend: 'csvHtml5', text: 'Export to CSV', filename:'users', exportOptions: {columns:[0,1]} },
							 ]
				});
		} );
		</script>
	<?php
	endif;
	?>

	<br/>
	<br/>
	<br/>
	<hr/>
	
	<h2>Remove Users</h2>
	<br/>
	<br/>
	<?php
	/*
	$sql = "SELECT * FROM `email_queue_members`
			ORDER BY `email` ASC, `last_name` ASC, `first_name` ASC, `id` ASC
			";
	*/
	$sql = "SELECT `u`.* 
			     , `ud`.`pref_buying`, `ud`.`pref_selling`, `ud`.`pref_professional`, `ud`.`pref_insights` 
			  FROM `users` AS `u`
				INNER JOIN `user_details` AS `ud` ON (`ud`.`user_id` = `u`.`id`) 
			WHERE `u`.`group_id` = " . \BlueFrontier\Helper::GROUP_MEMBERS . " 
			  AND `u`.`email_queue_optin` = 1 
			  AND `u`.`email_verified` = 1 
			  AND `u`.`active` = 1 
			  AND `u`.`deleted` = 0 
			ORDER BY `u`.`email` ASC, `u`.`last_name` ASC, `u`.`first_name` ASC
			";
	$users = @mysqli_query($db_conn, $sql);
	if ($users) :
	?>
		<table id="data-table-remove" class="table table-striped">
		<thead>
			<tr>
				<th>Name</th>
				<th>Email Address</th>
				<th width="15%">Preferences</th>
				<th width="15%">Action</th>
			</tr>
		</thead>
		<tbody>
		<?php
		// - build email data
		while ($user = $users->fetch_assoc()) :
			
			?>
			<tr>
				<td><?php echo $user['first_name'] . ' ' . $user['last_name']; ?></td>
				<td><a href="mailto:<?php echo $user['email']; ?>"><?php echo $user['email']; ?></a></td>
				<td><?php
					foreach ($email_types as $email_type)
					{
						if (isset($user[ $email_type['pref_key'] ]) && boolval($user[ $email_type['pref_key'] ]) === true)
						{
							echo ucwords(strtolower( $email_type['name'] )) . '<br/>';
						}
					}
				?></td>
				<td><a class="btn btn-outline-secondary btn-sm" onclick="user_email_remove(this, '<?php echo $user['id']; ?>', '<?php echo htmlentities($user['email']); ?>')">Remove User</td>
			</tr>
			<?php
			
		endwhile;
		?>
		</tbody>
		</table>
		<script>
		$(document).ready( function () {
			$('#data-table-remove')
				.css('visibility', 'visible')
				.DataTable({
					stateSave: true,
					dom: 'Blfrtip',
					buttons: [
								{ extend: 'copyHtml5', text: 'Copy to clipboard', exportOptions: {columns:[0,1]} },
								{ extend: 'excelHtml5', text: 'Export to Excel', filename:'users_email', exportOptions: {columns:[0,1]} },
								{ extend: 'csvHtml5', text: 'Export to CSV', filename:'users_email', exportOptions: {columns:[0,1]} },
							 ]
				});
		} );
		</script>
	<?php
	endif;
	?>


</body>
</html>
