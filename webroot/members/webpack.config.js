var path = require('path');
var webpack = require('webpack');
module.exports = {
    entry: {
        login: './login.js',
        app: './app.js'
    },
    output: {
        path: path.resolve(__dirname, './dist'),
        publicPath: '/dist/',
        filename: '[name]-build.js'
    },
    mode: 'production',
    devtool: false,
    performance: {
        hints: "warning",
        maxEntrypointSize: 512000,
        maxAssetSize: 512000
    },
    module: {
        rules: [
            {
                // vue-loader config to load `.vue` files or single file components.
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        // https://vue-loader.vuejs.org/guide/scoped-css.html#mixing-local-and-global-styles
                        css: ['vue-style-loader', {
                            loader: 'css-loader'
                        }],
                        js: [
                            'babel-loader'
                        ],
                    },
                    cacheBusting: true
                }
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    presets: [
                        "@babel/preset-env"
                    ],
                    plugins: [
                        "transform-es2015-destructuring",
                        "transform-object-rest-spread",
                        "@babel/plugin-proposal-object-rest-spread"
                    ]
                }
            }
        ]
    }
}
