// noinspection ES6UnusedImports
import * as Polyfills from './src/utils/polyfills.js';
import Loader from './src/components/loader.js';
import LoginForm from './src/components/login-form.js';
import ForgotPasswordForm from './src/components/forgot-password-form.js';
import ResetPasswordForm from './src/components/reset-password-form.js';
import EmailVerificationForm from './src/components/email-verification-form.js';
import RegisterForm from './src/components/register-form.js';

// test ES6 requirement
var supportsES6 = function () {
    try {
        new Function('(a = 0) => a');
        return true;
    } catch (err) {
        return false;
    }
}();

if (!supportsES6) {
    location.href = '/upgrade-your-browser';
} else {
    Vue.use({
        install(Vue) {
            let csrf;
            try {
                csrf = document.head.querySelector('[name~=csrf-token][content]').content;
            } catch (e) {
                csrf = window.Cookies.get('csrfToken');
            }
            Vue.prototype.$api = axios.create({
                baseURL: '/',
                headers: {
                    'X-CSRF-Token': csrf
                }
            });
        }
    });

    /**
     * @class App
     * @type {Vue}
     */
    let app = new Vue({
        el: '#app',
        components: {
            Loader,
            LoginForm,
            ForgotPasswordForm,
            ResetPasswordForm,
            EmailVerificationForm,
            RegisterForm
        },
        data: {
            loading: true
        },
        methods: {
            isProduction: function () {
                return this.$el.dataset.isProduction === 'true';
            }
        },
        watch: {
            loading: function (value) {
                this.$refs.PageLoader.isActive = value;
            }
        },
        mounted() {
            (() => this.loading = false).delay(300);
        }
    });

    if (!app.isProduction()) window.app = app;
}
