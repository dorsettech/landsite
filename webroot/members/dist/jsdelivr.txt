//cdn.jsdelivr.net/npm/vue@2.6.8/dist/vue.min.js
//cdn.jsdelivr.net/npm/vue-router@3.0.2/dist/vue-router.min.js
//cdn.jsdelivr.net/npm/axios@0.18.0/dist/axios.min.js
//cdn.jsdelivr.net/npm/vue-snotify@3.2.1/vue-snotify.min.js
//cdn.jsdelivr.net/npm/vuetable-2@1.7.5/dist/vuetable-2-full.min.js
//cdn.jsdelivr.net/npm/flatpickr@4.5.7/dist/flatpickr.min.js
//cdn.jsdelivr.net/npm/vue-flatpickr-component@8.1.1/dist/vue-flatpickr.min.js
//cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js
//cdn.jsdelivr.net/npm/vue2-editor@2.6.6/dist/vue2-editor.min.js
//cdn.jsdelivr.net/npm/vue-simple-suggest@1.8.3/dist/iife.min.js

//cdn.jsdelivr.net/npm/vuetable-2@1.7.5/dist/vuetable-2.min.css
//cdn.jsdelivr.net/npm/vue-snotify@3.2.1/styles/material.min.css
//cdn.jsdelivr.net/npm/flatpickr@4.5.7/dist/flatpickr.min.css

Version aliasing:

//cdn.jsdelivr.net/npm/vue@2/dist/vue.min.js
//cdn.jsdelivr.net/npm/vue-router@3/dist/vue-router.min.js
//cdn.jsdelivr.net/npm/axios@0.18.0/dist/axios.min.js
//cdn.jsdelivr.net/npm/vue-snotify@3/vue-snotify.min.js
//cdn.jsdelivr.net/npm/vuetable-2@1/dist/vuetable-2-full.min.js
//cdn.jsdelivr.net/npm/flatpickr@4/dist/flatpickr.min.js
//cdn.jsdelivr.net/npm/vue-flatpickr-component@8/dist/vue-flatpickr.min.js
//cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js
//cdn.jsdelivr.net/npm/vue2-editor@2/dist/vue2-editor.min.js
//cdn.jsdelivr.net/npm/vue-simple-suggest@1/dist/iife.min.js

//cdn.jsdelivr.net/npm/vuetable-2@1/dist/vuetable-2.min.css
//cdn.jsdelivr.net/npm/vue-snotify@3/styles/material.min.css
//cdn.jsdelivr.net/npm/flatpickr@4/dist/flatpickr.min.css
