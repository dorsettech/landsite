// noinspection ES6UnusedImports
import * as Polyfills from './src/utils/polyfills.js';
import Loader from './src/components/loader.js';
import Dashboard from './src/components/dashboard.js';
import Home from './src/components/home.js';
import Profile from './src/components/profile.js';
import AlertsSearches from './src/components/alerts-searches.js';
import MarketingPreferences from './src/components/marketing-preferences.js';
import Properties from './src/components/properties.js';
import SavedProperties from './src/components/saved-properties.js';
import SavedBusinesses from './src/components/saved-businesses.js';
import AddProperty from './src/components/add-property.js';
import Property from './src/components/property.js';
import PropertyStats from './src/components/property/property-stats.js';
import BusinessListing from './src/components/business-listing.js';
import BusinessStats from './src/components/business-stats.js';
import CaseStudies from './src/components/case-studies.js';
import CaseStudy from './src/components/case-study.js';
import Insights from './src/components/insights.js';
import Insight from './src/components/insight.js';
import PreferredInsights from './src/components/preferred-insights.js';
import RecentInsights from './src/components/recent-insights.js';
import Forum from './src/components/forum.js';
import _ from './src/utils/utilities.js';

Vue.use({
    install(Vue) {
        const EventBus = new Vue();
        let csrf;
        try {
            csrf = document.head.querySelector('[name~=csrf-token][content]').content;
        } catch (e) {
            csrf = window.Cookies.get('csrfToken');
        }
        Vue.prototype.$api = axios.create({
            baseURL: '/',
            headers: {
                'X-CSRF-Token': csrf
            }
        });

        Vue.prototype.$log = console.log.bind(console);

        Object.defineProperties(Vue.prototype, {
            $bus: {
                get: function () {
                    return EventBus
                }
            }
        });

        Vue.use(Vuetable);
        Vue.use(VueFlatpickr);
        Vue.use(vueDirectiveTooltip);
        Vue.component('treeselect', VueTreeselect.Treeselect);
    }
});

let router = new VueRouter({
        routes: [
            {
                path: '/',
                component: Home,
                name: 'Account Dashboard',
                meta: {
                    title: 'Account Dashboard'
                }
            }, {
                path: '/welcome',
                component: Profile,
                props: {
                    first: true
                },
                meta: {
                    title: 'Welcome'
                }
            }, {
                path: '/profile',
                component: Profile,
                meta: {
                    title: 'My Details'
                }
            }, {
                path: '/alerts-searches',
                component: AlertsSearches,
                name: 'AlertsSearches',
                meta: {
                    title: 'Alerts & Searches'
                }
            }, {
                path: '/marketing-preferences',
                component: MarketingPreferences,
                meta: {
                    title: 'Marketing Preferences'
                }
            }, {
                path: '/properties',
                component: Properties,
                name: 'Properties',
                meta: {
                    title: 'My Properties'
                }
            }, {
                path: '/add-property',
                component: AddProperty,
                meta: {
                    title: 'Add a Property'
                }
            }, {
                path: '/property',
                component: Property,
                name: 'PropertyAdd',
                meta: {
                    title: 'Add a Property'
                },
                props: (route) => {
                    let props = {
                        purpose: route.query.purpose
                    }
                    _.clearUrl();
                    return props;
                }
            }, {
                path: '/property/:id',
                component: Property,
                name: 'PropertyEdit',
                meta: {
                    title: 'Edit a Property'
                },
                props: (route) => {
                    let props = {
                        record: Object.assign({}, route.query.record)
                    }
                    if (route.query.tab) {
                        props.tab = route.query.tab;
                    }
                    _.clearUrl();
                    return props;
                }
            }, {
                path: '/property/:id/stats',
                component: PropertyStats,
                name: 'PropertyStats',
                meta: {
                    title: 'Property Stats'
                },
                props: (route) => {
                    let props = {
                        record: Object.assign({}, route.query.record)
                    }
                    _.clearUrl();
                    return props;
                }
            }, {
                path: '/saved-properties',
                component: SavedProperties,
                name: 'SavedProperties',
                meta: {
                    title: 'Saved Properties'
                }
            }, {
                path: '/business-listing',
                component: BusinessListing,
                meta: {
                    title: 'My Business Profile'
                }
            }, {
                path: '/business-stats',
                component: BusinessStats,
                name: 'BusinessStats',
                meta: {
                    title: 'My Business Stats'
                }
            }, {
                path: '/saved-businesses',
                component: SavedBusinesses,
                name: 'SavedBusinesses',
                meta: {
                    title: 'Saved Businesses'
                }
            }, {
                path: '/case-studies',
                component: CaseStudies,
                meta: {
                    title: 'Case Studies List'
                }
            }, {
                path: '/case-study',
                component: CaseStudy,
                name: 'CaseStudyAdd',
                meta: {
                    title: 'Add a Case Study'
                }
            }, {
                path: '/case-study/:id',
                component: CaseStudy,
                name: 'CaseStudyEdit',
                meta: {
                    title: 'Edit a Case Study'
                },
                props: (route) => {
                    let props = {
                        record: Object.assign({}, route.query.record)
                    }
                    _.clearUrl();
                    return props;
                }
            }, {
                path: '/insights',
                component: Insights,
                meta: {
                    title: 'News'
                }
            }, {
                path: '/insight',
                component: Insight,
                name: 'InsightAdd',
                meta: {
                    title: 'Add a New Insight'
                },
                props: (route) => {
                    let props = {};
                    if (route.query.contribute) {
                        props.contribute = true;
                    }
                    return props;
                }
            }, {
                path: '/insight/:id',
                component: Insight,
                name: 'InsightEdit',
                meta: {
                    title: 'Edit an Insight'
                },
                props: (route) => {
                    let props = {
                        record: Object.assign({}, route.query.record)
                    }
                    _.clearUrl();
                    return props;
                }
            }, {
                path: '/preferred-insights',
                component: PreferredInsights,
                meta: {
                    title: 'Preferred Insights'
                }
            }, {
                path: '/recent-insights',
                component: RecentInsights,
                name: 'RecentInsights',
                meta: {
                    title: 'Recent Insights'
                }
            }, {
                path: '/forum',
                component: Forum,
                name: 'Forum',
                meta: {
                    title: 'Forum'
                }
            }
        ]
    }),

    /**
     * @class App
     * @type {Vue}
     */
    app = new Vue({
        el: '#app',
        router,
        components: {
            Loader,
            Dashboard
        },
        data: {
            loading: true
        },
        methods: {
            isProduction: function () {
                return this.$el.dataset.isProduction === 'true';
            }
        },
        watch: {
            loading: function (value) {
                this.$refs.PageLoader.isActive = value;
            }
        },
        mounted() {
            (() => {
                this.loading = false;
            }).delay(300);
        }
    });

if (!app.isProduction()) window.app = app;
