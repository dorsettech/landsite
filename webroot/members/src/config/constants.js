let URL = location.host.split('.').splice(1).join('.');

export default {
    URL: {
        WWW: URL,
        MEMBERS: location.host,
        WWW_SERVICES: `${URL}/professional-services`,
        WWW_SERVICE: `${URL}/professional-services/{0}`,
        WWW_PROPERTIES: `${URL}/properties`,
        WWW_PROPERTY: `${URL}/properties/{0}`,
        WWW_ARTICLES: `${URL}/articles`,
        WWW_ARTICLE: `${URL}/articles/{0}`, // insight
        WWW_ARTICLE_CASE_STUDY: `${URL}/case-studies/{0}` // case study
    },
    PROFILE_IMAGE_DEFAULT: '/members/assets/profile-placeholder.png',
    IMAGE_DEFAULT: '/members/assets/add-image-placeholder.png',
    IMAGE_PLACEHOLDER: '/members/assets/image-placeholder.png',
    PATH: {
        MEMBERS_LOGO: '/files/members/{0}',
        MEMBERS_SERVICE: '/files/services/{0}',
        MEMBERS_ARTICLE: '/files/articles/{0}',
        MEMBERS_PROPERTY: '/files/properties/{0}',
    },
    DASHBOARD_TYPE: {
        SELLER: 'S',
        PROFESSIONAL: 'P',
        BOTH: 'SP'
    },
    MEDIA_TYPE: {
        IMAGE: 'IMAGE',
        VIDEO: 'VIDEO',
        DOCUMENT: 'DOCUMENT'
    },
    CREDENTIAL_TYPE: {
        ACCREDITATION: 'ACC',
        ASSOCIATION: 'ASS'
    },
    ARTICLE_TYPE: {
        INSIGHT: 'INSIGHT',
        CASE_STUDY: 'CASE_STUDY'
    },
    AD_TYPE: {
        STANDARD: 'STANDARD',
        FEATURED: 'FEATURED',
        PREMIUM: 'PREMIUM'
    },
    AD_PLAN_TYPE: {
        SERVICE_1: 'service',
        SERVICE_2: 'service_2'
    },
    STATUS: {
        DRAFT: 'DRAFT',
        PUBLISHED: 'PUBLISHED',
        EXPIRED: 'EXPIRED'
    },
    PROPERTY_PURPOSE: {
        SALE: 'SALE',
        RENT: 'RENT',
        BOTH: 'BOTH'
    },
    PROPERTY_PRICE_RENT_PER: {
        MONTH: 'M',
        YEAR: 'Y',
        QUARTER: 'Q',
        FEET_SQUARE: 'FT2',
        METER_SQUARE: 'M2',
        PRICE_ON_APPLICATION: 'POA'
    },
    PROPERTY_PRICE_SALE_PER: {
        FULL: 'F',
        FEET_SQUARE: 'FT2',
        METER_SQUARE: 'M2',
        PRICE_ON_APPLICATION: 'POA'
    },
    PROPERTY_PRICE_RENT_PER_NAMES_UNIT: {
        M: 'p.m.',
        Y: 'p.a.',
        Q: 'p.q.',
        FT2: 'pft²',
        M2: 'pm²',
        POA: ''
    },
    PROPERTY_PRICE_RENT_PER_NAMES: {
        M: 'per month',
        Q: 'per quarter',
        Y: 'per annum',
        FT2: 'per ft²',
        M2: 'per m²',
        POA: 'POA'
    },
    PROPERTY_PRICE_SALE_PER_NAMES_UNIT: {
        F: '',
        FT2: 'pft²',
        M2: 'pm²',
        POA: ''
    },
    PROPERTY_PRICE_SALE_PER_NAMES: {
        F: 'full price',
        FT2: 'per ft²',
        M2: 'per m²',
        POA: 'POA'
    },
    /*
     * For SALE and RENT type
     */
    PROPERTY_PRICE_QUALIFIER: {
        DEFAULT: 'D',
        PRICE_ON_APPLICATION: 'POA',
        GUIDE_PRICE: 'GP',
        FIXED_PRICE: 'FP',
        OFFERS_IN_EXCESS_OF: 'OIEO',
        OFFERS_IN_REGION_OF: 'OIRO',
        SALE_BY_TENDER: 'SBT',
        FROM: 'F',
        SHARED_OWNERSHIP: 'SO',
        OFFERS_OVER: 'OO',
        PART_BUY: 'PB',
        PART_RENT: 'PR',
        SHARED_EQUITY: 'SE',
        OFFERS_INVITED: 'OI',
        COMING_SOON: 'CS',
        NOW_LET: 'NL',
        SOLD_SUBJECT_TO_CONTRACT: 'SSTC'
    },
    PROPERTY_PRICE_QUALIFIER_NAMES: {
        D: 'Default',
        POA: 'POA',
        GP: 'Guide Price',
        FP: 'Fixed Price',
        OIEO: 'Offers In Excess Of',
        OIRO: 'Offers In Region Of',
        SBT: 'Sale by Tender',
        F: 'From',
        SO: 'Shared Ownership',
        OO: 'Offers over',
        PB: 'Part Buy',
        PR: 'Part Rent',
        SE: 'Shared Equity',
        OI: 'Offers invited',
        CS: 'Coming soon',
        NL: 'Now Let',
        SSTC: 'SSTC'
    },
    STATE: {
        APPROVED: 'APPROVED',
        PENDING: 'PENDING',
        REJECTED: 'REJECTED'
    },
    SEARCH_TYPE: {
        PROPERTY: 'PROPERTY',
        SERVICE: 'SERVICE',
    },
    PROPERTY_EXPIRE_IN: 'Your property profile will expire in {0} {1}',
    PROPERTY_EXPIRED: 'Your property profile was expired',
    SERVICE_EXPIRE_IN: 'Your business profile will expire in {0} {1}',
    SERVICE_EXPIRED: 'Your business profile was expired',
    PAYMENT_ITEM: {
        PLAN: 'PLAN',
        LINK: 'URL',
        IMAGE: 'IMAGE'
    },
    PAYMENT_TYPE: {
        STRIPE: 'STRIPE',
        ORDER: 'ORDER'
    },
    DISCOUNT_TYPE: {
        FIXED_SUM: 'FIXEDSUM',
        FREE: 'FREE',
        PERCENTAGE: 'PERCENTAGE'
    }
}
