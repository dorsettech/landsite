import template from '../views/payment-summary.html';
import C from '../config/constants';

const DISCOUNT_INITIAL_MSG = 'If you have a discount code, please enter it in the field above.';

export default {


    data() {
        return {
            discount: false,
            discountCode: '',
            discountChecking: false,
            vat: 0,
            subtotal: 0,
            inputMessage: DISCOUNT_INITIAL_MSG
        }
    },
    props: {
        settings: Object,
        list: Array,
        hideEmpty: {
            type: Boolean,
            default: false
        },
        value: {
            type: String,
            default: ''
        }
    },
    computed: {
        inputIcons() {
            if (this.discountChecking) {
                return 'fa-cog fa-spin';
            }
            if (this.discount) {
                return 'fa-times';
            }
            return 'fa-tags';
        },
        total() {
            let total = this.list.reduce((prev, next) => {
                return {price: prev.price + next.price}
            }, {
                price: 0
            });

            total = total.price;

            // vat rate
            this.vat = this.settings.prices.vat > 0 ? total * (this.settings.prices.vat / 100) : 0;
            this.subtotal = total;
            total += this.vat;

            if (this.discount && total > 0) {
                switch (this.discount.type) {
                    case C.DISCOUNT_TYPE.FIXED_SUM:
                        this.discount.price = this.discount.value;
                        total = total - this.discount.value;
                        break;
                    case C.DISCOUNT_TYPE.FREE:
                        this.discount.price = total;
                        total = 0;
                        break;
                    case C.DISCOUNT_TYPE.PERCENTAGE:
                        this.discount.price = total * (this.discount.value / 100);
                        total = total - this.discount.price;
                        break;
                }
                total = total > 0 ? total : 0;
            }
            return total;
        }
    },
    methods: {
        checkDiscount() {
            this.discountChecking = true;
            return this.$api.get('/Discounts/verify', {
                params: {
                    code: this.discountCode
                }
            }).then(response => {
                this.discountChecking = false;
                return response.data;
            });
        },
        onDiscountClearClick() {
            if (this.discount) {
                this.clearDiscount(true);
            }
        },
        onDiscountChanged() {
            this.checkDiscount().then(response => {
                this.inputMessage = response.msg;
                if (response.success) {
                    this.discount = response.discount;
                } else {
                    this.$snotify.error(this.inputMessage);
                    this.clearDiscount();
                }
                this.$emit('input', this.discountCode);
            });
        },
        clearDiscount(emit) {
            this.discount = false;
            this.discountCode = '';
            this.inputMessage = DISCOUNT_INITIAL_MSG;
            if (emit === true) {
                this.$emit('input', this.discountCode);
            }
        },
        getDiscount() {
            return this.discount;
        }
    },
    watch: {
        total() {
            this.$emit('change', this.total, this.vat);
        },
        list() {
            if (this.discount) {
                this.checkDiscount();
            }
        }
    },
    template: template.render(),
    created() {
        this.discountCode = this.value.toString();
    }
}
