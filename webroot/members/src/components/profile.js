import template from '../views/profile.html';
import Utils from '../utils/utilities';
import CraftyClick from './crafty-click';
import InputImageUpload from './input-image-upload';
import LatestBusinessesAdded from './latest-businesses-added';
import CONSTS from "../config/constants";

export default {
    components: {
        CraftyClick,
        InputImageUpload,
        LatestBusinessesAdded
    },
    data() {
        return {
            profile: {
                details: {},
                billing_address: {}
            },
            addresses: [],
            address: '',
            postcodeMsg: '',
            isPostcodeSuccess: false,
            billing: {
                addresses: [],
                address: '',
                postcodeMsg: '',
                isPostcodeSuccess: false
            },
            imageMsg: '',
            isImageSuccess: false,
            isWorking: false,
            hasError: false,
            message: '',
            welcome: false
        };
    },
    props: {
        user: Object,
        settings: Object,
        first: false
    },
    computed: {
        getImageUrl() {
            return Utils.format(CONSTS.PATH.MEMBERS_LOGO, this.user.uid)
        }
    },
    methods: {
        onImageSuccess(msg, file) {
            this.isImageSuccess = true;
            this.imageMsg = msg;
        },
        onImageFail(msg) {
            this.isImageSuccess = false;
            this.imageMsg = msg;
        },
        onInputChange(e, type) {
            let input = e.target;
            input.checkValidity();
            if (input.validity.valid) {
                e.target.classList.remove('is-invalid');
            }

            if (['password', 'confirm_password'].indexOf(type) !== -1) {
                this.checkConfirmPassword();
            }
        },
        checkConfirmPassword() {
            if (typeof this.profile.password === 'undefined' || this.profile.password === '') {
                return true;
            }
            let input = this.$el.querySelector('[name="confirm_password"]');
            if (this.profile.password !== this.profile.confirm_password) {
                input.classList.add('is-invalid');
                return false;
            } else {
                input.classList.remove('is-invalid');
                return true;
            }
        },
        onInputInvalid(e) {
            e.target.classList.add('is-invalid');
        },
        onPostcodeSuccess(msg, data) {
            this.postcodeMsg = msg;
            this.isPostcodeSuccess = true;
            this.addresses = data;
        },
        onPostcodeFail(msg) {
            this.postcodeMsg = msg;
            this.isPostcodeSuccess = false;
            this.addresses = [];
        },
        onPostcodeCancel() {
            this.addresses = [];
            this.postcodeMsg = '';
        },
        onAddressSelect() {
            this.profile.details.address = this.address;
            this.onPostcodeCancel();
            (() => {
                let address = document.forms['profile-form'].querySelector('[name=address]');
                address.checkValidity();
                this.onInputChange({
                    target: address
                });
            }).defer();
        },
        onBillingPostcodeSuccess(msg, data) {
            this.billing.postcodeMsg = msg;
            this.billing.isPostcodeSuccess = true;
            this.billing.addresses = data;
        },
        onBillingPostcodeFail(msg) {
            this.billing.postcodeMsg = msg;
            this.billing.isPostcodeSuccess = false;
            this.billing.addresses = [];
        },
        onBillingPostcodeCancel() {
            this.billing.addresses = [];
            this.billing.postcodeMsg = '';
        },
        onBillingAddressSelect() {
            this.profile.billing_address.address = this.billing.address;
            this.onBillingPostcodeCancel();
        },
        onSubmit() {
            this.isWorking = true;
            this.$api.post('/User/update', this.profile).then(function (response) {
                this.hasError = !response.data.success;
                this.message = response.data.msg;
                if (this.hasError) {
                    this.$snotify.error(response.data.msg);
                } else {
                    this.$snotify.success(response.data.msg);
                }
                if (response.data.success) {
                    /**
                     * After welcome view (first details update) redirect to dashboard
                     */
                    if (this.welcome === true) {
                        this.welcome = false;
                        this.$router.push('/');
                    }
                    /**
                     * Update image new file name (if any)
                     */
                    this.profile.image = response.data.image;
                    this.$bus.$emit('dashboard-refresh');
                }
                (() => this.message = '').delay(3000);
            }.bind(this)).catch(function (error) {
                this.hasError = true;
                this.message = error.message;
            }.bind(this)).then(function () {
                this.isWorking = false;
            }.bind(this));
        }
    },
    template: template.render(),
    /**
     * Prevent mutations
     */
    created() {
        this.profile = Utils.deepCopy(this.user);
        this.welcome = this.first;
        if (this.welcome) {
            this.profile.details.use_billing_details = true;
            (() => document.forms['profile-form'].checkValidity()).defer();
        }
    }
}
