import consts from '../config/constants';
import utils from '../utils/utilities';
import Loader from './../components/loader';
import template from '../views/latest-businesses-added.html';

export default {
    components: {
        Loader
    },
    data() {
        return {
            list: [],
            loading: true
        };
    },
    methods: {
        getLogoStyle(item) {
            return {
                backgroundImage: utils.empty(item.image) ? `url(${consts.IMAGE_PLACEHOLDER})` : `url(${this.getProfileImagePath(item)})`
            }
        },
        getProfileImagePath(item) {
            return utils.format(consts.PATH.MEMBERS_LOGO, item.user_uid) + `/${item.image}`;
        },
        getBusinessUrl(item) {
            return '//' + utils.format(consts.URL.WWW_SERVICE, `${item.id}_${item.slug}`)
        }
    },
    template: template.render(consts),
    created() {
        this.loading = true;
        this.$api.get('/Services/latest').then(response => response.data.records).then(list => this.list = list).then(() => this.loading = false).catch(() => this.loading = false);
    }
}
