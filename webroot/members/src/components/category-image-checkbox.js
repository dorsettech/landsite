import template from '../views/category-image-checkbox.html';
import Utils from '../utils/utilities';

const IMAGE_DEFAULT = '/members/assets/image.jpg';

export default {
    data() {
        return {
            id: Utils.guid(),
            checked: false
        };
    },
    props: {
        item: Object,
        value: Array
    },
    created() {
        if (this.value.includes(this.item.id.toString())) {
            this.checked = true;
        }
    },
    methods: {
        onCheckChange(event) {
            let val = event.target.value.toString(),
                // prevent directly mutations
                value = [].concat(this.value);
            if (value.includes(val)) {
                value.splice(value.indexOf(val), 1)
            } else {
                value.push(val)
            }
            this.$emit('input', value);
        },
        onImageSrcError(e) {
            e.target.src = IMAGE_DEFAULT;
        }
    },
    template: template.render(IMAGE_DEFAULT)
}
