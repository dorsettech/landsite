import template from '../views/reset-password-form.html';

export default {
    data() {
        return {
            form: {
                password: {
                    value: '',
                    isInvalid: false
                },
                confirm_password: {
                    value: '',
                    isInvalid: false
                }
            },
            message: '',
            hasError: false,
            isWorking: false
        }
    },
    props: {
        token: {
            type: String,
            default: ''
        }
    },
    methods: {
        onSubmit: function (e) {
            this.$root.loading = true;
            this.isWorking = true;
            let data = {};
            for (const [key, obj] of Object.entries(this.form)) data[key] = obj.value;
            this.$api.patch('/User/password', Object.assign(data, {
                token: this.token
            })).then(function (response) {
                this.hasError = !response.data.success;
                this.message = response.data.msg;
                if (response.data.success) {
                    if (response.data.success) {
                        (() => location.href = location.protocol + '//' + location.host + '/login').delay(2000);
                    } else {
                        this.isWorking = false;
                    }
                } else {
                    this.isWorking = false;
                }
            }.bind(this)).catch(function (error) {
                this.hasError = true;
                this.message = error.message;
                this.isWorking = false;
            }.bind(this)).then(function () {
                this.$root.loading = false;
            }.bind(this));
        },
        onChange: function (field, e) {
            let input = e.target;
            input.checkValidity();
            this.form[field].isInvalid = this.form[field].value === '' ? false : !input.validity.valid;

            if (['password', 'confirm_password'].indexOf(field) !== -1) {
                this.checkConfirmPassword();
            }
        },
        checkConfirmPassword() {
            if (typeof this.form.password.value === 'undefined' || this.form.password.value === '') {
                return true;
            }
            let input = this.$el.querySelector('[name="confirm_password"]');
            if (this.form.password.value !== this.form.confirm_password.value) {
                input.classList.add('is-invalid');
                return false;
            } else {
                input.classList.remove('is-invalid');
                return true;
            }
        }
    },
    template: template.render()
}
