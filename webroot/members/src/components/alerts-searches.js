import template from '../views/alerts-searches.html';
import C from './../config/constants';
import _ from './../utils/utilities';
import Loader from './../components/loader';
import RecentArticlesPanel from './articles/recent-articles-panel';

export default {
    components: {
        RecentArticlesPanel,
        Loader
    },
    data() {
        return {
            loading: false,
            articles: [],
            tableCls: {
                tableWrapper: '',
                tableHeaderClass: 'fixed',
                tableBodyClass: 'vuetable-semantic-no-top fixed',
                tableClass: 'ui blue selectable unstackable celled table',
                loadingClass: 'loading',
                ascendingIcon: 'fas fa-chevron-up',
                descendingIcon: 'fas fa-chevron-down',
                ascendingClass: 'sorted-asc',
                descendingClass: 'sorted-desc',
                sortableIcon: 'grey sort icon',
                handleIcon: 'grey sidebar icon'
            },
            columns: {
                SERVICE: [
                    {
                        name: 'category.name',
                        title: 'Professional Type',
                        sortField: 'Categories.name'
                    }, {
                        name: 'location',
                        sortField: 'location',
                        formatter(value) {
                            return _.empty(value) ? '—' : value;
                        }
                    }, {
                        name: 'keywords',
                        title: 'Keyword',
                        sortField: 'keywords',
                        formatter(value) {
                            return _.empty(value) ? '—' : value;
                        }
                    }, {
                        name: 'radius',
                        sortField: 'radius',
                        formatter(value) {
                            return _.empty(value) ? '—' : `Within ${value} miles`;
                        }
                    }, {
                        name: 'alerts',
                        __slot: 'alerts',
                        sortField: 'alerts',
                        title: 'Receive Alerts?'
                    }, 'actions'
                ],
                PROPERTY: [
                    {
                        name: 'location',
                        sortField: 'location',
                        formatter(value) {
                            return _.empty(value) ? '—' : value;
                        }
                    }, {
                        name: 'min_price',
                        sortField: 'min_price',
                        title: 'Min Price',
                        formatter: function (value) {
                            return _.empty(value) ? '—' : this.settings.currency.symbol + `${value.toLocaleString()}`;
                        }.bind(this)
                    }, {
                        name: 'max_price',
                        sortField: 'max_price',
                        title: 'Max Price',
                        formatter: function (value) {
                            return _.empty(value) ? '—' : this.settings.currency.symbol + `${value.toLocaleString()}`;
                        }.bind(this)
                    }, {
                        name: 'property_type.name',
                        title: 'Property Type',
                        sortField: 'PropertyTypes.name'
                    }, {
                        name: 'radius',
                        sortField: 'radius',
                        formatter(value) {
                            return _.empty(value) ? '—' : `Within ${value} miles`;
                        }
                    }, {
                        name: 'attrs',
                        formatter(value) {
                            return _.empty(value) || value.length === 0 ? '—' : value.map(v => {
                                return v.name
                            }).join(' | ');
                        }
                    }, {
                        name: 'alerts',
                        __slot: 'alerts',
                        sortField: 'alerts',
                        title: 'Receive Alerts?'
                    }, 'actions'
                ],
            }
        };
    },
    props: {
        user: Object,
        settings: Object
    },
    watch: {
        '$route': function (to, from) {
            if (to.name === 'AlertsSearches') {
                if (to.query.refresh) {
                    this.refreshAll();
                }
            }
        }
    },
    methods: {
        onAlertChange(type, row) {
            this.$api.put('/Searches/alerts', {
                id: row.id,
                alerts: row.alerts,
                type: type
            });
        },
        onActionClick(type, action, data) {
            switch (action) {
                case 'search':
                    if (type === C.SEARCH_TYPE.PROPERTY) {
                        let url = `${C.URL.WWW_PROPERTIES}?saved=1&`,
                            params = {};

                        if (!_.empty(data.purpose)) {
                            params.purpose = data.purpose;
                        }
                        if (!_.empty(data.type)) {
                            params.type = data.type_id;
                        }
                        if (!_.empty(data.min_price)) {
                            params.min_price = data.min_price;
                        }
                        if (!_.empty(data.max_price)) {
                            params.max_price = data.max_price;
                        }
                        if (!_.empty(data.location)) {
                            params.location = data.location;
                        }
                        if (!_.empty(data.radius)) {
                            params.range = data.radius;
                        }
                        if (!_.empty(data.attributes)) {
                            data.attributes.split(',').forEach(val => params[`attributes[${val}]`] = 1);
                        }

                        if (!_.emptyObject(params)) {
                            this.$root.loading = true;
                            location.href = '//' + url + Object.entries(params).reduce((p, n) => {
                                return (Array.isArray(p) ? p[0] + '=' + p[1] : p) + '&' + n[0] + '=' + n[1]
                            });
                        }
                        return;
                    }
                    let url = `${C.URL.WWW_SERVICES}?saved=1&`,
                        params = {};

                    if (!_.empty(data.category_id)) {
                        params.service_category = data.category_id;
                    }
                    if (!_.empty(data.keywords)) {
                        params.keyword = data.keywords;
                    }
                    if (!_.empty(data.location)) {
                        params.location = data.location;
                    }
                    if (!_.empty(data.radius)) {
                        params.range = data.radius;
                    }

                    if (!_.emptyObject(params)) {
                        this.$root.loading = true;
                        location.href = '//' + url + Object.entries(params).reduce((p, n) => {
                            return (Array.isArray(p) ? p[0] + '=' + p[1] : p) + '&' + n[0] + '=' + n[1]
                        });
                    }
                    break;
                case 'delete':
                    this.$snotify.error(`Delete the selected record?`, type === C.SEARCH_TYPE.SERVICE ? 'Business Search' : 'Property Search', {
                        timeout: 5000,
                        showProgressBar: true,
                        closeOnClick: false,
                        pauseOnHover: false,
                        position: 'centerCenter',
                        buttons: [{
                            text: 'Proceed',
                            action: (toast) => {
                                this.$snotify.remove(toast.id);
                                this.loading = true;
                                this.delete(type, data).then((response) => {
                                    this.loading = false;
                                    if (response.data.success) {
                                        this.$snotify.success(response.data.msg);
                                        this.refresh(type);
                                    } else {
                                        this.$snotify.error(response.data.msg);
                                    }
                                }).catch(error => {
                                    this.$snotify.error(error);
                                });
                            },
                            bold: false
                        }, {
                            text: 'Cancel',
                            action: (toast) => {
                                this.$snotify.remove(toast.id);
                            }
                        }]
                    });
                    break;
            }
        },
        delete(type, data) {
            return this.$api.delete('/Searches/delete', {params: Object.assign({}, data, {type: type})});
        },
        refresh(type) {
            return this.$refs[type + '_Searches'].refresh();
        },
        refreshAll() {
            this.loading = true;
            this.refresh(C.SEARCH_TYPE.PROPERTY).then(() => this.refresh(C.SEARCH_TYPE.SERVICE).then(() => this.$refs.RecentArticles.refresh().then(() => this.loading = false)));
        }
    },
    template: template.render()
}
