export default {
    data() {
        return {};
    },
    props: {
        params: {
            type: Object,
            default: {
                current_page: 0,
                last_page: 0,
                per_page: 10,
                from: 0,
                to: 10,
                total: 0
            }
        }
    },
    computed: {
        pages() {
            let pages = [];
            for (let i = 1; i <= this.params.last_page; i++) {
                pages.push(i);
            }
            return pages;
        },
        isFirstDisabled() {
            return this.params.current_page === 1;
        },
        isPrevDisabled() {
            return this.params.current_page - 1 < 1;
        },
        isNextDisabled() {
            return this.params.current_page + 1 > this.params.last_page;
        },
        isLastDisabled() {
            return this.params.current_page === this.params.last_page;
        }
    },
    methods: {
        goTo(page) {
            this.$emit('page-change', page);
        }
    },
    template: `
        <div class="pagination" v-show="params.last_page > 1">
            <a class="btn-nav page-link" :class="{disabled: isFirstDisabled }" @click="goTo(1)">
                <i class="fas fa-angle-double-left"></i>
            </a> 
            
            <a class="btn-nav page-link" :class="{disabled: isPrevDisabled }" @click="goTo(params.current_page - 1)">
                <i class="fas fa-angle-left"></i>
            </a> 
            
            <a class="page-link" v-for="n in pages" :class="{active: n === params.current_page}" @click="goTo(n)">{{n}}</a>
             
            <a class="btn-nav page-link" :class="{disabled: isNextDisabled }" @click="goTo(params.current_page + 1)">
                <i class="fas fa-angle-right"></i>
            </a>
                         
            <a class="btn-nav page-link" :class="{disabled: isLastDisabled }" @click="goTo(params.last_page)">
                <i class="fas fa-angle-double-right"></i>
            </a>
        </div>
    `
}
