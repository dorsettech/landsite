import template from '../views/saved-properties.html';
import Loader from '../components/loader';
import Pagination from '../components/pagination';
import PropertyListItem from '../components/properties/property-list-item';
import LatestBusinessesAdded from "./latest-businesses-added";
import C from './../config/constants';
import _ from './../utils/utilities';
import DateTime from './../utils/date';

export default {
    components: {
        Loader,
        Pagination,
        LatestBusinessesAdded,
        PropertyListItem
    },
    data() {
        return {
            items: [],
            loading: false,
            page$: 1,
            pagination: {}
        };
    },
    props: {
        user: Object,
        settings: Object
    },
    watch: {
        '$route': function (to, from) {
            if (to.name === 'SavedProperties') {
                if (to.query.reload) {
                    this.reset();
                } else if (to.query.refresh) {
                    this.refresh();
                }
            }
        }
    },
    methods: {
        timeToMessage(ms) {
            const tc = DateTime.timeComponents(ms),
                text = 'You only have {0} {1} available to enquire about this property';
            if (tc.days > 0) {
                return _.format(text, tc.days, tc.days === 1 ? 'day' : 'days');
            } else if (tc.hours > 0) {
                return _.format(text, tc.hours, tc.hours === 1 ? 'hour' : 'hours');
            } else if (tc.minutes > 0) {
                return _.format(text, tc.minutes, tc.minutes === 1 ? 'minute' : 'minutes');
            }
            return '';
        },
        load(page = 1, perPage = 10) {
            this.loading = true;
            this.page$ = page;
            this.$api.get('/Properties/saved', {
                params: {
                    page: page,
                    per_page: perPage
                }
            }).then((response) => {
                this.items = response.data.records.map(item => {
                    item.property.created = item.created;
                    item.property.saved_id = item.id;
                    let enquiryPossible = false;
                    if (item.property.status === C.STATUS.EXPIRED) {
                        let now = new Date(),
                            exp = new Date(item.property.ad_expiry_date),
                            exp7d = new Date(exp.getTime());
                        exp7d.setDate(exp7d.getDate() + 7);
                        if (now < exp7d) {
                            enquiryPossible = true;
                            item.property.message = this.timeToMessage(Math.abs(exp7d - now));
                        } else {
                            item.property.message = 'This property expired and it is not longer available.'
                        }
                    }

                    item.property.isAvailable = item.property.status !== C.STATUS.EXPIRED || enquiryPossible;

                    return item.property;
                });
                this.pagination = response.data.pagination;
            }).then(() => this.loading = false);
        },
        refresh() {
            this.load(this.page$);
        },
        reset() {
            this.load();
        },
        onPageChange(page) {
            this.load(page);
        },
        delete(data) {
            return this.$api.delete('/Properties/deleteSaved', {params: data});
        },
        action(what, item) {
            switch (what) {
                case 'view':
                    this.loading = true;
                    location.href = '//' + _.format(C.URL.WWW_PROPERTY, `${item.id}_${item.slug}`)
                    return;
                case 'delete':
                    this.$snotify.error(`Delete the selected saved property?`, item.title, {
                        timeout: 5000,
                        showProgressBar: true,
                        closeOnClick: false,
                        pauseOnHover: false,
                        position: 'centerCenter',
                        buttons: [{
                            text: 'Proceed',
                            action: (toast) => {
                                this.$snotify.remove(toast.id);
                                this.loading = true;
                                this.delete(item).then((response) => {
                                    this.loading = false;
                                    if (response.data.success) {
                                        this.$snotify.success(response.data.msg);
                                        this.refresh();
                                    } else {
                                        this.$snotify.error(response.data.msg);
                                    }
                                }).catch(error => {
                                    this.$snotify.error(error);
                                });
                            },
                            bold: false
                        }, {
                            text: 'Cancel',
                            action: (toast) => {
                                this.$snotify.remove(toast.id);
                            }
                        }]
                    });
                    return;
                case 'enquiry':
                    this.loading = true;
                    location.href = '//' + _.format(C.URL.WWW_PROPERTY, `${item.id}_${item.slug}`) + '?enquiry=1';
                    return;
            }
        }
    },
    template: template.render(),
    created() {
        this.load();
    }
}
