import template from '../views/properties.html';
import _ from '../utils/utilities';
import Loader from '../components/loader';
import Pagination from '../components/pagination';
import PropertyListItem from '../components/properties/property-list-item';
import LatestBusinessesAdded from "./latest-businesses-added";
import consts from "../config/constants";

export default {
    components: {
        Loader,
        Pagination,
        LatestBusinessesAdded,
        PropertyListItem
    },
    data() {
        return {
            items: [],
            keywords: '',
            loading: false,
            page$: 1,
            total$: '',
            pagination: {}
        };
    },
    props: {
        user: Object,
        settings: Object
    },
    watch: {
        '$route': function (to, from) {
            if (to.name === 'Properties') {
                if (to.query.reload) {
                    this.reset();
                } else if (to.query.refresh) {
                    this.refresh();
                }
            }
        }
    },
    methods: {
        load(page = 1, perPage = 10) {
            this.loading = true;
            this.page$ = page;
            this.$api.get('/Properties', {
                params: {
                    page: page,
                    per_page: perPage,
                    filters: {
                        keywords: this.keywords
                    }
                }
            }).then((response) => {
                this.items = response.data.records;
                this.pagination = response.data.pagination;
                if (this.total$ === '') {
                    this.total$ = this.pagination.total;
                }
            }).then(() => this.loading = false);
        },
        refresh() {
            this.load(this.page$);
        },
        reset() {
            this.keywords = '';
            this.load();
        },
        delete(data) {
            return this.$api.delete('/Properties/delete', {
                params: {
                    id: data.id,
                    deletion_reason: data.deletion_reason
                }
            });
        },
        onPageChange(page) {
            this.load(page);
        },
        onSearchInput: _.debounce(function () {
            this.load(1);
        }, 500),
        action(what, item) {
            switch (what) {
                case 'stats':
                    this.$router.push({
                        path: `/property/${item.id}/stats`,
                        query: {
                            record: item
                        }
                    });
                    return;
                case 'edit':
                case 'post':
                    this.$router.push({
                        path: `/property/${item.id}`,
                        query: {
                            record: item
                        }
                    });
                    return;
                case 're-list':
                    this.$router.push({
                        path: `/property/${item.id}`,
                        query: {
                            record: item,
                            tab: 3
                        }
                    });
                    return;
                case 'view':
                    this.loading = true;
                    location.href = '//' + _.format(consts.URL.WWW_PROPERTY, `${item.id}_${item.slug}`)
                    return;
                case 'delete':
                    this.$snotify.prompt('Please enter the reason why you want to delete this property.', item.title, {
                        position: 'centerCenter',
                        backdrop: .3,
                        buttons: [{
                            text: 'Proceed',
                            action: (toast) => {
                                toast.value = _.stripTags(toast.value);
                                if (toast.value.trim() === '') {
                                    toast.valid = false;
                                    return false;
                                }
                                this.$snotify.remove(toast.id);
                                (() => {
                                    this.loading = true;
                                    item.deletion_reason = toast.value;
                                    this.delete(item).then((response) => {
                                        this.loading = false;
                                        if (response.data.success) {
                                            this.$snotify.success(response.data.msg);
                                            this.refresh();
                                        } else {
                                            this.$snotify.error(response.data.msg);
                                        }
                                    }).catch(error => {
                                        this.$snotify.error(error);
                                    });
                                    // fix for snotify backdrop
                                }).delay(200);
                            },
                            bold: false
                        }, {
                            text: 'Cancel',
                            action: (toast) => {
                                this.$snotify.remove(toast.id);
                            }
                        }]
                    }).on('mounted', function () {
                        // change toast background color to danger
                        (() => document.querySelector('.snotifyToast.snotify-prompt').classList.add('danger')).delay(250)
                    });
                    return;
            }
        }
    },
    template: template.render(),
    created() {
        this.load();
    }
}
