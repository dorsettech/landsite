import _ from '../utils/utilities';

export default {
    data() {
        return {
            prefix: '',
            rows: 0
        };
    },
    props: {
        items: Array,
        value: Array,
        wrapperCls: {
            type: [String, Object],
            default: ''
        },
        columns: {
            type: Number,
            default: 0
        }
    },
    created() {
        this.prefix = _.guid() + '_';
    },
    methods: {
        getIdFor(item) {
            return `${this.prefix}cg_${item.id}`;
        },
        onCheckChange(event) {
            let val = parseInt(event.target.value.toString()),
                // prevent directly mutations
                value = [].concat(this.value);
            if (value.includes(val)) {
                value.splice(value.indexOf(val), 1);
            } else {
                value.push(val)
            }

            this.$emit('input', value);
        },
        isChecked(item) {
            return this.value.indexOf(item.id) !== -1;
        },
        cols(row) {
            let start = (row - 1) * this.columns,
            items = this.items.slice(start, start + this.columns);
            if (this.columns > 0 && items.length && items.length < this.columns) {
                for (let i = 0; i < this.columns - items.length; i++) {
                    items.push(null);
                }
            }
            return items;
        },
        countRows() {
            this.rows = Math.ceil((this.items.length + 1) / this.columns);
        }
    },
    watch: {
        items() {
            this.countRows();
        }
    },
    template: `
        <div class="v-comp-checkbox-group" :class="wrapperCls">            
            <div v-if="columns">
                <div class="row" v-for="i in rows">
                    <div class="col" v-for="(item, index) in cols(i)">                        
                        <div class="checkbox checkbox-css checkbox-inline" v-if="item">
                            <input type="checkbox" :id="getIdFor(item)" :value="item.id" @change="onCheckChange" :checked="isChecked(item)"> 
                            <label :for="getIdFor(item)" :title="item.description">{{item.name}}</label>
                        </div>
                    </div>
                </div>
            </div>
            <div v-else>
                <div class="checkbox checkbox-css checkbox-inline" v-for="item in items">
                    <input type="checkbox" :id="getIdFor(item)" :value="item.id" @change="onCheckChange" :checked="isChecked(item)"> 
                    <label :for="getIdFor(item)" :title="item.description">{{item.name}}</label>
                </div>
            </div>
        </div>
    `
}
