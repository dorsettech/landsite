import template from '../views/business-listing.html';
import Loader from './../components/loader';
import LatestBusinessesAdded from './../components/latest-businesses-added';
import PaymentSummary from './../components/payment-summary';
import PaymentForm from '../components/payment-form';
import CategoryListItem from './category-list-item';
import ContactListItem from './contact-list-item';
import InputVideoPreview from './input-video-preview';
import InputImagesUpload from './input-images-upload';
import OpeningHours from './business-listing/opening-hours';
import CheckboxGroup from '../components/checkbox-group';
import Consts from '../config/constants';
import _ from '../utils/utilities';
import DateTime from '../utils/date';
import Slugify from '../utils/slugify';

const STEP_PAYMENT_FORM = 7;

export default {
    components: {
        Loader,
        LatestBusinessesAdded,
        PaymentSummary,
        PaymentForm,
        CategoryListItem,
        ContactListItem,
        InputVideoPreview,
        InputImagesUpload,
        OpeningHours,
        CheckboxGroup
    },
    data() {
        return {
            isEditMode: false,
            step: 0,
            form: {
                company: '',
                phone: '',
                category_id: null,
                postcode: '',
                description_short: '',
                description: '',
                website_url: '',
                google_url: '',
                facebook_url: '',
                linkedin_url: '',
                twitter_url: '',
                hubspot_url: '',
                quick_win_1: '',
                quick_win_2: '',
                quick_win_3: '',
                lat: null,
                lng: null,
                status: Consts.STATUS.DRAFT,
                opening_time_from: '',
                opening_time_to: '',
                opening_days: [],
                opening_hours: [],
                service_products: [],
                service_areas: [],
                service_media: [],
                service_credentials: [],
                service_contacts: [],
                ad_type: Consts.AD_TYPE.STANDARD,
                ad_plan_type: Consts.AD_PLAN_TYPE.SERVICE_1,
                ad_cost: 0,
                ad_total_cost: 0,
                ad_basket: [],
                // virtual fields
                ad_extend: false,
                ad_payment: null,
                ad_discount_code: null,
                ad_discount: false,
                ad_vat: 0
            },
            payments: [],
            discountCode: '',
            total$: 0,
            vat$: 0,
            categories: [],
            products: [],
            areas: [],
            credentials: [],
            serviceAccreditations: [],
            serviceAssociations: [],
            editorToolbar: [
                ['bold', 'italic', 'underline', {'script': 'sub'}, {'script': 'super'}],
                ['align', {'align': 'center'}, {'align': 'right'}, {'align': 'justify'}],
                ['blockquote'],
                [{'list': 'ordered'}, {'list': 'bullet'}],
                [{'indent': '-1'}, {'indent': '+1'}, 'clean']
            ],
            editorOptions: {
                formats: ['bold', 'italic', 'script', 'underline', 'blockquote', 'indent', 'list', 'align']
            },
            autoCompleteStyles: {
                vueSimpleSuggest: 'position-relative product-suggest',
                inputWrapper: '',
                defaultInput: 'form-control',
                suggestions: 'position-absolute list-group z-1000',
                suggestItem: 'list-group-item'
            },
            timePickerOptions: {
                enableTime: true,
                noCalendar: true,
                dateFormat: 'H:i',
                time_24hr: true,
                defaultDate: ''
            },
            buttonSubmit: {
                label: '',
                icons: {
                    'fa-cubes': true,
                    'fa-compass': false,
                    'fa-camera': false,
                    'fa-gem': false,
                    'fa-check': false,
                    'fa-address-book': false,
                    'fa-credit-card': false,
                    'fa-save': false
                },
                green: false
            },
            descriptionLimit: 10e3,
            descriptionLength: 0,
            descriptionShortLimit: 260,
            descriptionShortLength: 0,
            isPostcodeSuccess: false,
            postcodeDisabled: false,
            postcodeMsg: '',
            isWorking: false,
            loading: true,
            hasError: false,
            message: '',
            mute$: false,
            disableBackToEdit: false,
            imagesHardLimit: 0
        };
    },
    computed: {
        getImagesUrl() {
            return _.format(Consts.PATH.MEMBERS_SERVICE, this.user.uid)
        },
        video: {
            get() {
                let v = this.form.service_media.find(item => {
                    return item.type === Consts.MEDIA_TYPE.VIDEO;
                });
                if (v) {
                    try {
                        let url = new URL(v.source);
                        return url.toString();
                    } catch (e) {
                        return `https://www.youtube.com/watch?v=${v.source}`;
                    }
                }
                return '';
            },
            set(value) {
                let v = this.form.service_media.find(item => {
                    return item.type === Consts.MEDIA_TYPE.VIDEO;
                });
                if (v) {
                    if (value === '') {
                        this.form.service_media = Array.from(this.form.service_media).filter(item => {
                            return item.type !== Consts.MEDIA_TYPE.VIDEO;
                        });
                    } else {
                        v.source = value;
                    }
                } else {
                    this.form.service_media.push({
                        service_id: this.form.id,
                        type: Consts.MEDIA_TYPE.VIDEO,
                        source: value
                    });
                }
            }
        },
        images: {
            get() {
                return this.form.service_media.filter(item => {
                    return item.type === Consts.MEDIA_TYPE.IMAGE;
                });
            },
            set(value) {
                this.form.service_media = this.form.service_media.filter((item) => {
                    return item.type === Consts.MEDIA_TYPE.VIDEO || item.type === Consts.MEDIA_TYPE.DOCUMENT;
                }).concat(Array.from(value));
            }
        },
        itemProducts: {
            get() {
                return this.form.service_products.map(item => {
                    return item.product_id;
                });
            },
            set(value) {
                this.form.service_products = [...new Set(Array.from(value))].map(id => {
                    return {
                        product_id: id,
                        service_id: this.form.id
                    }
                });
            }
        },
        itemAreas: {
            get() {
                return this.form.service_areas.map(item => {
                    return item.area_id;
                });
            },
            set(value) {
                this.form.service_areas = [...new Set(Array.from(value))].map(id => {
                    return {
                        area_id: id,
                        service_id: this.form.id
                    }
                });
            }
        },
        accreditations() {
            return this.credentials.filter(item => {
                return item.type === Consts.CREDENTIAL_TYPE.ACCREDITATION;
            });
        },
        associations() {
            return this.credentials.filter(item => {
                return item.type === Consts.CREDENTIAL_TYPE.ASSOCIATION;
            });
        },
        slug() {
            this.form.slug = Slugify.toSafeURL(this.form.company);
            return this.form.slug;
        },
        plan: {
            get() {
                return [`${this.form.ad_plan_type}-${this.form.ad_type}`];
            },
            set(value) {
                let val = value.slice(-1)[0].split('-')
                this.form.ad_plan_type = val[0];
                this.form.ad_type = val[1];
            }
        },
        isPayableItemAvailable() {
            if (this.isEditMode) {
                return this.form.status === Consts.STATUS.DRAFT || this.form.status === Consts.STATUS.EXPIRED;
            } else {
                return true;
            }
        },
        getExtendedExpiryDate() {
            if (!this.form.ad_type) {
                return '';
            }
            let exp = new Date(this.form.ad_expiry_date);
            exp.setDate(exp.getDate() + this.settings.validity[`${this.form.ad_plan_type}-${this.form.ad_type.toLowerCase()}-days`]);
            return new DateTime(exp).format('dS mmmm yyyy');
        },
        hasToPay() {
            return this.total$ > 0;
        }
    },
    props: {
        user: Object,
        settings: Object
    },
    watch: {
        step(next) {
            /*
             * https://econnect4u.atlassian.net/browse/LS-266
             * If added
             */
            if (next === 5) {
                this.next();
                return;
            }

            this.setSubmitButton(next);
            if (next === STEP_PAYMENT_FORM) {
                this.$refs.PaymentForm.reset();
            }
        },
        'form.service_credentials': function () {
            this.serviceAccreditations = Array.from(this.form.service_credentials).filter(item => {
                return item.credential.type === Consts.CREDENTIAL_TYPE.ACCREDITATION;
            });
            this.serviceAssociations = Array.from(this.form.service_credentials).filter(item => {
                return item.credential.type === Consts.CREDENTIAL_TYPE.ASSOCIATION;
            });
        },
        'form.ad_type': function () {
            this.refreshSummary();
        },
        'form.ad_plan_type': function () {
            this.refreshSummary();
        },
        'form.service_media': function () {
            this.refreshSummary();
        },
        'form.ad_extend': function () {
            this.refreshSummary();
        }
    },
    methods: {
        updateCounters() {
            this.descriptionLength = this.form.description.replace(/(<([^>]+)>)/ig, '').length;
            this.descriptionShortLength = this.form.description_short.length;
        },
        refreshSummary() {
            if (typeof this.form.ad_type === 'undefined') return;
            let plan = this.form.ad_type,
                planType = this.form.ad_plan_type,
                basket = [];

            if (!this.isEditMode || this.isPayableItemAvailable || this.form.ad_extend) {
                basket.push({
                    type: Consts.PAYMENT_ITEM.PLAN,
                    price: this.settings.prices[`${planType}-${plan.toLowerCase()}`],
                    name: `Business Profile (${plan})`
                });
            }

            if (this.settings.prices['service-image'] > 0 && this.isPayableItemAvailable) {
                let imgAmount = this.form.service_media.reduce((p, n) => {
                    return p + (n.type === Consts.MEDIA_TYPE.IMAGE ? 1 : 0);
                }, 0);
                let extraImgs = imgAmount - 5;
                if (extraImgs > 0) {
                    basket.push({
                        type: Consts.PAYMENT_ITEM.IMAGE,
                        price: extraImgs * this.settings.prices['service-image'],
                        name: extraImgs > 1 ? `Extra images (${extraImgs})` : 'Extra image'
                    });
                }
            }

            this.payments = basket;
        },
        onSummaryChange(total, vat) {
            this.total$ = total;
            this.vat$ = vat;
            this.setSubmitButton(this.step);

            if (this.isPaymentPage() && this.total$ === 0) {
                this.prev();
            }
        },
        isStandardPlanOnly() {
            return
                this.settings.plans[`${Consts.AD_PLAN_TYPE.SERVICE_1}-${Consts.AD_TYPE.FEATURED.toLowerCase()}`] === false &&
                this.settings.plans[`${Consts.AD_PLAN_TYPE.SERVICE_1}-${Consts.AD_TYPE.PREMIUM.toLowerCase()}`] === false &&
                this.settings.plans[`${Consts.AD_PLAN_TYPE.SERVICE_2}-${Consts.AD_TYPE.FEATURED.toLowerCase()}`] === false &&
                this.settings.plans[`${Consts.AD_PLAN_TYPE.SERVICE_2}-${Consts.AD_TYPE.PREMIUM.toLowerCase()}`] === false;
        },
        isPaymentPage() {
            return this.step === STEP_PAYMENT_FORM;
        },
        filterCredentials(type) {
            return Array.from(this.form.service_credentials).filter(item => {
                return item.type === type;
            });
        },
        next() {
            if (this.step === 6) return;
            this.step++;
        },
        isLastStep() {
            return this.step === 6;
        },
        prev() {
            if (this.step === 0) return;
            this.step--;
        },
        isStepDisabled(step) {
            if (this.isEditMode) return false;
            return !(step < this.step);
        },
        goTo(step, force = false) {
            if (force === false && this.isStepDisabled(step)) return false;
            this.step = step;
        },
        setSubmitButton(next = this.step) {
            Object.entries(this.buttonSubmit.icons).forEach((icon) => this.buttonSubmit.icons[icon[0]] = false);
            this.buttonSubmit.green = false;

            if (this.isEditMode) {
                this.buttonSubmit.label = !this.hasToPay ? 'Save profile' : 'Proceed to payment';
                this.buttonSubmit.green = !this.hasToPay;

                if (this.hasToPay) {
                    this.buttonSubmit.icons['fa-credit-card'] = true;
                } else {
                    this.buttonSubmit.icons['fa-save'] = true;
                }

                if (next === STEP_PAYMENT_FORM) {
                    this.buttonSubmit.label = 'Submit Payment';
                }
            } else {
                switch (next) {
                    case 0:
                        this.buttonSubmit.label = 'Continue - add contact details';
                        this.buttonSubmit.icons['fa-address-book'] = true;
                        return;
                    case 1:
                        this.buttonSubmit.label = 'Continue - add products & services';
                        this.buttonSubmit.icons['fa-cubes'] = true;
                        return;
                    case 2:
                        this.buttonSubmit.label = 'Continue - add areas covered';
                        this.buttonSubmit.icons['fa-compass'] = true;
                        return;
                    case 3:
                        this.buttonSubmit.label = 'Continue - add images & video';
                        this.buttonSubmit.icons['fa-camera'] = true;
                        return;
                    /*
                     * https://econnect4u.atlassian.net/browse/LS-266
                     * case 4 - same as case 5
                     */
                    case 4:
                        // this.buttonSubmit.label = 'Continue - add credentials';
                        // this.buttonSubmit.icons['fa-gem'] = true;
                        this.buttonSubmit.label = 'Continue - make your profile stand out';
                        this.buttonSubmit.icons['fa-check'] = true;
                        return;
                    case 5:
                        this.buttonSubmit.label = 'Continue - make your profile stand out';
                        this.buttonSubmit.icons['fa-check'] = true;
                        return;
                    case 6:
                        this.buttonSubmit.label = !this.hasToPay ? 'Save profile' : 'Proceed to payment';
                        this.buttonSubmit.green = !this.hasToPay;

                        if (this.hasToPay) {
                            this.buttonSubmit.icons['fa-credit-card'] = true;
                        } else {
                            this.buttonSubmit.icons['fa-save'] = true;
                        }
                        return;
                    case STEP_PAYMENT_FORM:
                        this.buttonSubmit.icons['fa-credit-card'] = true;
                        this.buttonSubmit.label = 'Submit Payment';
                        return;
                    default:
                        this.buttonSubmit.label = 'Opss! Something wrong';
                }
            }
        },
        togglePlanTo(plan, type) {
            this.plan = [`${plan}-${type}`]
        },
        preventIfLessThanOneCheckedPlan() {
            if (typeof this.form.ad_type === 'undefined') {
                this.$nextTick(() => this.plan = [Consts.AD_TYPE.STANDARD]);
            }
        },
        onInput(name) {
            if (['description_short'].indexOf(name) !== -1) {
                this.updateCounters();
            }
        },
        onInputChange(e, type) {
            let input = e.target;
            input.checkValidity();
            if (input.validity.valid) {
                e.target.classList.remove('is-invalid');
            }

            if (type === 'postcode') {
                this.postcodeToLocation();
            }
        },
        onInputInvalid(e) {
            e.target.classList.add('is-invalid');
        },
        onEditorChange() {
            this.descriptionLength = this.form.description.replace(/(<([^>]+)>)/ig, '').length;
            if (this.descriptionLength > this.descriptionLimit) {
                this.$refs.DescriptionEditor.quill.deleteText(this.descriptionLimit, this.descriptionLength);
            }
        },
        onContactAction(what, item) {
            if (what === 'add') {
                if (this.form.service_contacts.length === 5) {
                    this.$snotify.info('Max. 5 members of your team allowed');
                    return;
                }
                this.form.service_contacts.push({name: '', email: ''});
            } else {
                this.form.service_contacts.splice(this.form.service_contacts.indexOf(item), 1);
            }
        },
        onProductSelect(product) {
            if (product === null) {
                return;
            }
            (() => {
                this.$refs.ProductAutoComplete.autocompleteText('');
            }).defer(this);
            if (this.form.service_products.find((item) => {
                return item.product_id === product.id;
            })) {
                return
            }
            this.form.service_products.push({
                product: product,
                product_id: product.id,
                service_id: this.form.id
            });
        },
        onAreaSelect(area) {
            if (area === null) {
                return;
            }
            (() => {
                this.$refs.AreaAutoComplete.autocompleteText('');
            }).defer(this);
            if (this.form.service_areas.find((item) => {
                return item.area_id === area.id;
            })) {
                return
            }
            this.form.service_areas.push({
                area: area,
                area_id: area.id,
                service_id: this.form.id
            });
        },
        onCredentialSelect(credential) {
            if (credential === null) {
                return;
            }
            let cmp = credential.type === Consts.CREDENTIAL_TYPE.ACCREDITATION ?
                this.$refs.AccreditationAutoComplete :
                this.$refs.AssociationAutoComplete;
            (() => {
                cmp.autocompleteText('');
            }).defer(this);
            if (this.form.service_credentials.find((item) => {
                return item.credential_id === credential.id;
            })) {
                return
            }
            this.form.service_credentials.push({
                credential: credential,
                credential_id: credential.id,
                service_id: this.form.id
            });
        },
        onImagesUpload(msg, results) {
            this.form.service_media = this.form.service_media.concat(results.filter(file => file.success).map(file => {
                return {
                    service_id: this.form.id,
                    type: Consts.MEDIA_TYPE.IMAGE,
                    source: file.filename,
                    extra: file.image
                }
            }));

            // re-index image elements
            let index = 0;
            this.form.service_media.forEach((media) => {
                if (media.type !== Consts.MEDIA_TYPE.IMAGE) {
                    return;
                }
                media.position = index;
                index++;
            });
        },
        onImageChangeOrder(image, dir) {
            let index = 0,
                images = this.form.service_media.filter(file => file.type === Consts.MEDIA_TYPE.IMAGE),
                currIndex = images.indexOf(image),
                newIndex = dir === 'down' ? currIndex - 1 : currIndex + 1;
            if (newIndex < 0 || newIndex >= images.length) {
                return;
            }
            _.move(images, currIndex, newIndex);
            images.forEach((media) => {
                media.position = index;
                index++;
            });
            this.form.service_media = images.concat(this.form.service_media.filter(file => file.type !== Consts.MEDIA_TYPE.IMAGE));
        },
        onOpeningHoursChange(item, type) {
            if (type === 'add') {
                this.form.opening_hours.push({
                    from: '',
                    to: '',
                    days: []
                });
            } else {
                if (this.form.opening_hours.includes(item)) {
                    this.form.opening_hours.splice(this.form.opening_hours.indexOf(item), 1);
                }
            }
        },
        postcodeToLocation() {
            if (!_.validate.postcode(this.form.postcode)) {
                this.isPostcodeSuccess = false;
                this.postcodeMsg = 'Please provide a valid postcode';
                return;
            }

            this.postcodeDisabled = true;
            let apiKey = this.settings.api['google-map'];
            axios.get('https://maps.googleapis.com/maps/api/geocode/json', {
                params: {
                    address: this.form.postcode,
                    key: apiKey
                }
            }).then(response => response.data).then(function (response) {
                this.postcodeDisabled = false;
                if (response.status !== 'OK') {
                    this.form.lat = null;
                    this.form.lng = null;
                    this.form.location = '';
                    this.postcodeMsg = _.empty(response.error_message) ? 'Please provide a valid postcode' : response.error_message;
                    this.isPostcodeSuccess = false;
                    return;
                }

                this.postcodeMsg = response.results[0].formatted_address;
                this.isPostcodeSuccess = true;
                this.form.lat = response.results[0].geometry.location.lat;
                this.form.lng = response.results[0].geometry.location.lng;
                this.form.location = response.results[0].formatted_address;
            }.bind(this));
        },
        save(type = 'draft') {
            this.isWorking = true;
            return new Promise((resolve, reject) => {
                let data = Object.assign({}, this.form);

                if (data.opening_time_from instanceof Date) {
                    data.opening_time_from = new DateTime(data.opening_time_from).format('HH:MM');
                }
                if (data.opening_time_to instanceof Date) {
                    data.opening_time_to = new DateTime(data.opening_time_to).format('HH:MM');
                }

                data.opening_time_from = !_.empty(data.opening_time_from) ? data.opening_time_from + ':00' : '';
                data.opening_time_to = !_.empty(data.opening_time_to) ? data.opening_time_to + ':00' : '';
                data.opening_hours = JSON.stringify(data.opening_hours);
                data.service_media = data.service_media.filter(item => item.source !== '');

                data.service_contacts = data.service_contacts.filter(item => item.name !== '' || item.email !== '');

                data.ad_discount_code = this.discountCode;
                data.ad_discount = this.$refs.PaymentSummary.getDiscount();

                this.$api.post(`/Services/${type}`, data).then(response => {
                    this.isWorking = false;
                    if (response.data.success) {
                        resolve(response);
                    } else {
                        reject(response.data.msg);
                    }
                }).catch(error => {
                    this.isWorking = false;
                    reject(error)
                });
            });
        },
        getCurrentForm() {
            switch (this.step) {
                case 0:
                    return document.forms['listing-details-form'];
                case 1:
                    return document.forms['business-contact-details-form'];
                case 2:
                    return document.forms['products-services-form'];
                case 3:
                    return document.forms['areas-covered-form'];
                case 6:
                    return document.forms['stand-out-form'];
                case STEP_PAYMENT_FORM:
                    return this.$refs.PaymentForm.getForm();
            }
        },
        resetFormMessage() {
            this.hasError = false;
            this.message = '';
        },
        onSaveDraft() {
            let form = this.getCurrentForm();
            this.hasError = _.empty(form) ? false : !form.checkValidity();
            this.message = this.hasError ? 'This form has errors' : '';

            const hasError = this.hasError;
            return new Promise(function (resolve, reject) {
                if (hasError) {
                    reject();
                    return;
                }
                this.save('draft').then((response) => {
                    if (!this.mute$) {
                        this.message = response.data.msg;
                        this.$snotify.success(response.data.msg);
                    }
                    this.mute$ = false;
                    this.form.service_media = response.data.media;
                    this.form.id = response.data.id;
                    resolve();
                }).catch(error => {
                    this.hasError = true;
                    this.message = error;
                    this.$snotify.success(error);
                    reject();
                });
            }.bind(this));
        },
        pay() {
            this.isWorking = true;
            this.disableBackToEdit = true;
            return new Promise((resolve, reject) => {
                this.$refs.PaymentForm.createPayment().then(result => {
                    this.form.ad_payment = result;
                    this.form.ad_basket = this.payments;
                    this.form.ad_cost = this.total$;
                    this.form.ad_vat = this.vat$;
                    resolve();
                }).catch(error => {
                    this.isWorking = false;
                    reject(error);
                });
            });
        },
        onSave() {
            let form = this.getCurrentForm();
            this.hasError = _.empty(form) ? false : !form.checkValidity();
            this.message = this.hasError ? 'This form has errors' : '';
            if (this.hasError) return;
            if (this.isEditMode) {
                let fn = () => {
                    return this.save('update').then((response) => {
                        this.resetFormMessage();
                        if (response.data.success) {
                            this.form.service_media = response.data.media;
                            this.$snotify.success(response.data.msg);
                            this.$bus.$emit('dashboard-refresh');
                        } else {
                            this.$snotify.error(response.data.msg);
                        }
                    }).catch(error => {
                        this.hasError = true;
                        this.message = error;
                    });
                };
                if (this.isPaymentPage()) {
                    this.pay().then(() => fn().then(() => this.load()).catch(error => this.$snotify.error(error)));
                } else if (this.hasToPay) {
                    this.goTo(STEP_PAYMENT_FORM, true);
                } else {
                    fn()
                }
            } else {
                let fn = () => {
                    return this.save('update').then((response) => {
                        this.resetFormMessage();
                        if (response.data.success) {
                            this.form.service_media = response.data.media;
                            this.$snotify.success(response.data.msg);
                            this.goTo(0);
                            this.isEditMode = true;
                            this.$bus.$emit('dashboard-refresh');
                        } else {
                            this.$snotify.error(response.data.msg);
                        }
                    }).catch(error => {
                        this.hasError = true;
                        this.message = error;
                    });
                };

                if (this.isPaymentPage()) {
                    this.pay().then(() => fn().then(() => this.load()).catch(error => this.$snotify.error(error)));
                } else if (this.isLastStep()) {
                    if (this.hasToPay) {
                        this.goTo(STEP_PAYMENT_FORM, true);
                    } else {
                        fn();
                    }
                } else {
                    this.mute$ = true;
                    this.onSaveDraft().then(() => {
                        this.resetFormMessage();
                        this.next();
                    });
                }
            }
        },
        setImagesHardLimit() {
            if (this.form.status !== Consts.STATUS.DRAFT) {
                this.imagesHardLimit = this.images.length > 5 ? this.images.length : 5;
            } else {
                if (this.settings.prices['service-image'] === 0) {
                    this.imagesHardLimit = 5;
                } else {
                    this.imagesHardLimit = 0;
                }
            }
            this.$nextTick(() => this.$refs.InputImagesUpload.refresh());
        },
        load() {
            this.loading = true;
            this.$api.post('/Services/business').then(response => {
                // reset payment information (if reload after payment)
                this.payments = [];
                this.form.ad_extend = false;
                this.form.ad_basket = [];
                this.$refs.PaymentSummary.clearDiscount(true);
                // all the rest
                this.categories = response.data.categories;
                this.products = response.data.products;
                this.areas = response.data.areas;
                this.credentials = response.data.credentials;
                this.form = Object.assign(this.form, response.data.service);
                if (!_.empty(this.form.opening_time_from)) {
                    this.form.opening_time_from = new Date(this.form.opening_time_from);
                }
                if (!_.empty(this.form.opening_time_to)) {
                    this.form.opening_time_to = new Date(this.form.opening_time_to);
                }
                this.form.opening_days = _.empty(this.form.opening_days) ? [] : (Array.isArray(this.form.opening_days) ? this.form.opening_days : [this.form.opening_days]);
                this.form.opening_hours = _.empty(this.form.opening_hours) ? [{
                    "from": "",
                    "to": "",
                    "days": []
                }] : (Array.isArray(this.form.opening_hours) ? this.form.opening_hours : JSON.parse(this.form.opening_hours));

                if (this.form.service_contacts.length === 0) {
                    this.form.service_contacts.push({name: '', email: ''});
                }
                this.postcodeToLocation();
                this.updateCounters();
                this.isEditMode = !_.empty(this.form.publish_date);
                this.disableBackToEdit = false;
                this.setImagesHardLimit();
                this.setSubmitButton(this.step);
                if (!_.empty(this.form.ad_basket) && !this.form.visibility) {
                    this.goTo(STEP_PAYMENT_FORM, true);
                    this.disableBackToEdit = true;
                    (() => this.payments = JSON.parse(this.form.ad_basket)).delay(100);
                } else {
                    this.refreshSummary();
                    this.goTo(0, true);
                }
            }).then(() => this.loading = false).catch(() => this.loading = false);
        }
    },
    template: template.render(),
    created() {
        this.load();
    }
}
