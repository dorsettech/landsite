import Enquiries from './../../mixins/enquiries';
import template from './../../views/business-listing/business-enquiries.html';
import Modal from '../modal';
import _ from "../../utils/utilities";
import DateTime from "../../utils/date";

export default {
    data() {
        return {
            columns: [
                {
                    name: 'id',
                    title: 'Enquiry ID',
                    sortField: 'ServiceEnquiries.id'
                }, {
                    name: 'enquiry_title',
                    title: 'Business Title',
                    sortField: 'enquiry_title'
                }, {
                    name: 'full_name',
                    title: 'Name',
                    sortField: 'full_name'
                }, {
                    name: 'email',
                    title: 'E-mail',
                    sortField: 'email'
                }, {
                    name: 'phone',
                    title: 'Phone',
                    sortField: 'ServiceEnquiries.phone'
                }, {
                    name: 'created',
                    title: 'Date received',
                    sortField: 'ServiceEnquiries.created',
                    formatter(value) {
                        return _.empty(value) ? '--' : new DateTime(value).format('d mmmm yyyy');
                    }
                }, 'actions'
            ],
            sortOrder: [{
                field: 'ServiceEnquiries.created',
                direction: 'desc'
            }]
        }
    },
    props: {
        perPage: {
            type: Number,
            default: 10
        },
        title: String,
        loadOnStart: {
            type: Boolean,
            default: false
        }
    },
    mixins: [
        Enquiries
    ],
    components: {
        Modal
    },
    template: template.render()
}
