import template from './../../views/business-listing/business-list-item.html';
import C from '../../config/constants';
import _ from '../../utils/utilities';
import DateTime from '../../utils/date';

export default {
    components: {
        // CHART HERE
    },
    data() {
        return {
            statusCls: {
                'status-draft': false,
                'status-expired': false,
                'status-published': false
            },
            button: {
                iconCls: {
                    'fa-clipboard-check': false,
                    'fa-eye': false,
                    'fa-redo-alt': false
                },
                text: '',
                disabled: false
            },
            action$: ''
        };
    },
    props: {
        currencySymbol: {
            type: String,
            default: '£'
        },
        item: Object,
        chart: {
            type: Boolean,
            default: true
        },
        cost: {
            type: Boolean,
            default: true
        },
        buttons: {
            type: Object,
            default: function () {
                return {
                    main: true,
                    stats: true,
                    edit: true,
                    delete: true,
                    view: false
                }
            }
        },
        customButton: {
            /**
             * Example:
             * { icon: 'fa-envelope', text: 'Enquiry now', action: 'enquiry', disabled: false }
             */
            type: Object,
            default: null
        },
        customStatusNames: {
            /**
             * Example:
             * { C.STATUS.PUBLISHED: 'Live' }
             */
            type: Object,
            default: null
        },
        customStatusClass: {
            type: String,
            default: null
        },
        message: {
            type: String,
            default: ''
        },
        dateLabel: {
            type: String,
            default: 'Date added'
        }
    },
    computed: {
        chartData() {
            return [
                this.item.enquiries,
                this.item.views
            ]
        },
        dateAdded() {
            return new DateTime(this.item.created).format('d mmmm yyyy');
        },
        imageStyle() {
            let image = !_.empty(this.item.user.image) ? _.format(C.PATH.MEMBERS_LOGO, `${this.item.user.uid}/${this.item.user.image}`) : '';

            if (image) {
                return {
                    backgroundImage: `url(${image})`
                };
            } else {
                return {
                    backgroundSize: '50%',
                    backgroundImage: `url(${C.IMAGE_PLACEHOLDER})`
                }
            }
        },
        statusText() {
            Object.entries(this.statusCls).forEach((cls) => this.statusCls[cls[0]] = false);
            Object.entries(this.button.iconCls).forEach((cls) => this.button.iconCls[cls[0]] = false);
            let text = '';
            switch (this.item.status) {
                case C.STATUS.DRAFT:
                    text = C.STATUS.DRAFT;
                    this.statusCls['status-draft'] = true;
                    this.button.iconCls['fa-clipboard-check'] = true;
                    this.button.text = 'Post Ad';
                    this.action$ = 'post';
                    break;
                case C.STATUS.PUBLISHED:
                    text = C.STATUS.PUBLISHED;
                    this.statusCls['status-published'] = true;
                    this.button.iconCls['fa-eye'] = true;
                    this.button.text = 'View Ad';
                    this.action$ = 'view';
                    break;
                case C.STATUS.EXPIRED:
                    text = C.STATUS.EXPIRED;
                    this.statusCls['status-expired'] = true;
                    this.button.iconCls['fa-redo-alt'] = true;
                    this.button.text = 'Re-list Ad';
                    this.action$ = 're-list';
                    break;
            }

            if (this.customButton !== null) {
                Object.entries(this.button.iconCls).forEach((cls) => this.button.iconCls[cls[0]] = false);
                this.button.iconCls[this.customButton.icon] = true;
                this.button.text = this.customButton.text;
                this.button.disabled = this.customButton.disabled;
                this.action$ = this.customButton.action;
            }

            if (this.customStatusNames && this.customStatusNames[text]) {
                text = this.customStatusNames[text];
            }

            if (this.customStatusClass) {
                Object.entries(this.statusCls).forEach((cls) => this.statusCls[cls[0]] = false);
                this.statusCls[this.customStatusClass] = true;
            }

            return _.capitalize(text.toLowerCase());
        }
    },
    methods: {
        onButtonClick(action) {
            if (action === 'action') {
                action = this.action$;
            }
            this.$emit('action', action, this.item);
        }
    },
    template: template.render()
}
