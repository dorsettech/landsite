import template from './../../views/business-listing/opening-hours.html';
import _ from "../../utils/utilities";

export default {
    data() {
        return {
            timePickerOptions: {
                enableTime: true,
                noCalendar: true,
                dateFormat: 'H:i',
                time_24hr: true,
                defaultDate: ''
            },
            item: {
                from: '',
                to: '',
                days: []
            },
            prefix: ''
        };
    },
    props: {
        value: {
            type: Object,
            default() {
                return {
                    from: '',
                    to: '',
                    days: []
                }
            }
        },
        button: {
            type: String,
            default: ''
        }
    },
    watch: {
        item: {
            handler() {
                this.$emit('input', this.item);
            },
            deep: true
        }
    },
    created() {
        this.prefix = _.guid() + '_';
        this.item = !this.value ? [] : _.shallowCopy(this.value);
    },
    computed: {},
    methods: {
        onTimeClose(type, selectedDates, dateStr, instance) {
            if (instance.hourElement) {
                instance.hourElement.dispatchEvent(new Event('blur'));
            }
            this.item[type] = dateStr;
        },
        onButtonClick(type) {
            this.$emit('change', this.item, type);
        }
    },
    template: template.render()
}
