import Enquiries from './../../mixins/enquiries';
import template from './../../views/property/property-enquiries.html';
import Modal from '../modal';

export default {
    props: {
        perPage: {
            type: Number,
            default: 10
        },
        title: String,
        loadOnStart: {
            type: Boolean,
            default: false
        }
    },
    mixins: [
        Enquiries
    ],
    components: {
        Modal
    },
    template: template.render()
}
