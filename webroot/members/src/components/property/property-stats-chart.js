import ViewportResize from './../../mixins/viewport-resize';

export default {
    mixins: [
        VueChartJs.Line,
        ViewportResize
    ],
    props: {
        chartData: Array
    },
    watch: {
        chartData() {
            this.render()
        }
    },
    methods: {
        getDataSets() {
            return {
                datasets: [{
                    label: 'Views',
                    backgroundColor: 'rgba(160,160,160,.2)',
                    borderWidth: 3,
                    data: this.chartData.map(item => {
                        return item.views
                    })
                }, {
                    label: 'Enquiries',
                    backgroundColor: 'rgba(26,179,149,.5)',
                    borderWidth: 3,
                    data: this.chartData.map(item => {
                        return item.enquiries
                    })
                }],
                labels: this.chartData.map(item => {
                    return item.label
                })
            }
        },
        render() {
            this.renderChart(this.getDataSets(), {
                responsive: true,
                maintainAspectRatio: false
            });
        },
        onViewportResize() {
            this.render();
            this.$nextTick(() => {
                this.$emit('rotate-required', window.innerWidth < 550);
            });
        }
    },
    mounted() {
        this.onViewportResize();
    }
}
