import template from '../../views/property/property-stats.html';
import ImagesCarousel from '../images-carousel';
import PropertyStatsChart from '../property/property-stats-chart';
import PropertyEnquiries from '../property/property-enquiries';
import Loader from './../loader';
import _ from "../../utils/utilities";
import C from "../../config/constants";
import DateTime from "../../utils/date";

export default {
    components: {
        ImagesCarousel,
        PropertyStatsChart,
        PropertyEnquiries,
        Loader
    },
    data() {
        return {
            loading: false,
            property: {},
            stats: [],
            statusCls: {
                'status-draft': false,
                'status-expired': false,
                'status-published': false
            },
            iconCls: {
                'fa-tv': false,
                'fa-clock': false,
                'fa-edit': false
            },
            counter: {
                days: 0,
                hours: 0,
                minutes: 0,
                seconds: 0
            },
            counterMsg: '',
            interval$: null,
            rotateRequired: false,
            message: ''
        }
    },
    computed: {
        priceRentUnit() {
            return C.PROPERTY_PRICE_RENT_PER_NAMES_UNIT[this.property.price_rent_per];
        },
        priceSaleUnit() {
            return C.PROPERTY_PRICE_SALE_PER_NAMES_UNIT[this.property.price_sale_per];
        },
        priceRentQualifierName() {
            return C.PROPERTY_PRICE_QUALIFIER_NAMES[this.property.price_rent_qualifier];
        },
        priceSaleQualifierName() {
            return C.PROPERTY_PRICE_QUALIFIER_NAMES[this.property.price_sale_qualifier];
        },
        pricesTextBoth() {
            let p = this.property,
                sale = '',
                rent = '';
            if (p.purpose !== C.PROPERTY_PURPOSE.BOTH) {
                return '';
            }
            if (p.price_sale_per === C.PROPERTY_PRICE_SALE_PER.PRICE_ON_APPLICATION) {
                sale = C.PROPERTY_PRICE_SALE_PER_NAMES.POA;
            } else {
                sale = `${this.settings.currency.symbol}${p.price_sale.toLocaleString()} ${this.priceSaleUnit}`;
            }
            if (p.price_rent_per === C.PROPERTY_PRICE_RENT_PER.PRICE_ON_APPLICATION) {
                rent = C.PROPERTY_PRICE_RENT_PER_NAMES.POA;
            } else {
                rent = `${this.settings.currency.symbol}${p.price_rent.toLocaleString()} ${this.priceRentUnit}`;
            }
            return `${sale} <span class="qualifier">${this.priceSaleQualifierName}</span><small>${rent} <span class="qualifier">${this.priceRentQualifierName}</span></small>`;
        },
        images() {
            this.property.property_media = this.property.property_media || [];
            return this.property.property_media.filter(item => item.type === C.MEDIA_TYPE.IMAGE).map(image => {
                return {
                    id: image.id,
                    src: image.source_path
                }
            });
        },
        statusText() {
            Object.entries(this.statusCls).forEach((cls) => this.statusCls[cls[0]] = false);
            Object.entries(this.iconCls).forEach((cls) => this.iconCls[cls[0]] = false);
            let text = '';
            switch (this.property.status) {
                case C.STATUS.DRAFT:
                    text = C.STATUS.DRAFT;
                    this.statusCls['status-draft'] = true;
                    this.iconCls['fa-edit'] = true;
                    break;
                case C.STATUS.PUBLISHED:
                    text = C.STATUS.PUBLISHED;
                    this.statusCls['status-published'] = true;
                    this.iconCls['fa-tv'] = true;
                    if (this.property.state === C.STATE.PENDING) {
                        text = 'Awaiting approval';
                    } else if (this.property.state === C.STATE.REJECTED) {
                        this.statusCls['status-published'] = false;
                        this.statusCls['status-expired'] = true;
                        text = C.STATE.REJECTED;
                        if (_.empty(this.message) && !_.empty(this.property.rejection_reason)) {
                            this.message = this.property.rejection_reason;
                        }
                    }
                    break;
                case C.STATUS.EXPIRED:
                    text = C.STATUS.EXPIRED;
                    this.statusCls['status-expired'] = true;
                    this.iconCls['fa-clock'] = true;
                    break;
            }
            return _.capitalize(text.toLowerCase());
        },
        isChartAvailable() {
            if (!Array.isArray(this.stats) || this.stats.length === 0) {
                return false;
            }
            return this.stats.reduce((prev, curr) => {
                return prev + curr.views + curr.enquiries;
            }, 0) > 0;
        },
        isCounterVisible() {
            return !_.empty(this.property.ad_expiry_date) && new Date(this.property.ad_expiry_date) > new Date();
        }
    },
    props: {
        settings: Object,
        record: Object
    },
    methods: {
        load(id) {
            this.loading = true;
            this.$api.get('/Properties/property', {
                params: {
                    id: id
                }
            }).then(response => {
                this.property = response.data;
                this.loading = false;
            });
        },
        onEditClick() {
            this.$router.push({
                path: `/property/${this.property.id}`,
                query: {
                    record: this.property
                }
            });
        },
        startCounter() {
            if (!this.isCounterVisible) {
                return;
            }
            this.interval$ = setInterval(() => {
                const ms = Math.abs(new Date(this.property.ad_expiry_date) - new Date())
                this.counter = DateTime.timeComponents(ms, true);
                let v = 0, t = '';
                if (this.counter.days > 0) {
                    v = this.counter.days;
                    t = this.counter.days > 1 ? 'days' : 'day';
                } else if (this.counter.hours > 0) {
                    v = this.counter.hours;
                    t = this.counter.hours > 1 ? 'hours' : 'hour';
                } else if (this.counter.minutes > 0) {
                    v = this.counter.minutes;
                    t = this.counter.minutes > 1 ? 'minutes' : 'minute';
                } else if (this.counter.seconds > 0) {
                    v = this.counter.seconds;
                    t = this.counter.seconds > 1 ? 'seconds' : 'second';
                } else {
                    v = 0;
                    t = '';
                    this.property.status = C.STATUS.EXPIRED;
                    this.stopCounter();
                }
                this.counterMsg = v === 0 ? C.PROPERTY_EXPIRED : _.format(C.PROPERTY_EXPIRE_IN, parseInt(v), t);
            }, 1000)
        },
        stopCounter() {
            clearInterval(this.interval$);
        },
        onChartRotateRequired(value) {
            this.rotateRequired = value;
        }
    },
    watch: {
        property(property) {
            this.$refs.Enquiries.params.id = property.id;
            this.$api('/Properties/stats', {
                params: {
                    id: property.id
                }
            }).then(response => response.data).then(data => {
                this.stats = data;
            });
            this.startCounter();
        },
        '$route': function (to, from) {
            if (to.name === 'PropertyStats') {
                if (!this.record.id && to.params.id) {
                    this.load(to.params.id);
                } else {
                    this.property = _.deepCopy(this.record);
                }
            }
        }
    },
    template: template.render(),
    beforeRouteLeave: function (to, from, next) {
        this.stopCounter();
        /*
         * Make sure to always call the next function, otherwise the hook will never be resolved
         * @see https://router.vuejs.org/en/advanced/navigation-guards.html
         */
        next();
    },
    created() {
        this.$nextTick(() => {
            if (!this.record.id && this.$route.params.id) {
                this.load(this.$route.params.id);
            } else {
                this.property = _.deepCopy(this.record);
            }
        });
    }
}
