import template from '../views/case-study.html';
import Article from '../mixins/article';
import CONSTS from "../config/constants";

export default {
    mixins: [
        Article
    ],
    data() {
        return {
            componentMeta: Object.freeze({
                model: {
                    title: '',
                    slug: '',
                    headline: '',
                    body: '',
                    image: null,
                    type: CONSTS.ARTICLE_TYPE.CASE_STUDY,
                    status: CONSTS.STATUS.DRAFT,
                    state: null
                },
                viewAddName: 'CaseStudyAdd',
                viewEditName: 'CaseStudyEdit',
                formName: 'case-study-form',
                parentPath: '/case-studies'
            })
        };
    },
    template: template.render()
}
