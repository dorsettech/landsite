import consts from '../config/constants';
import _ from '../utils/utilities';
import template from '../views/dashboard-sidebar.html';

const PROFILE_IMAGE_DEFAULT = consts.PROFILE_IMAGE_DEFAULT;

export default {
    props: {
        user: Object
    },
    computed: {
        profileImagePath() {
            return _.format(consts.PATH.MEMBERS_LOGO, this.user.uid) + `/${this.user.image}`;
        }
    },
    methods: {
        onToggleMenu(e) {
            let target = e.target.tagName !== 'LI' ? e.target.closest('li') : e.target;
            target.classList.toggle('active')
        },
        onImageSrcError(e) {
            e.target.src = PROFILE_IMAGE_DEFAULT;
        }
    },
    template: template.render(PROFILE_IMAGE_DEFAULT)
}
