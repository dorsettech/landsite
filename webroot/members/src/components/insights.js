import template from '../views/insights.html';
import Consts from '../config/constants';
import Articles from '../mixins/articles';

export default {
    mixins: [
        Articles
    ],
    data() {
        return {
            componentMeta: Object.freeze({
                path: '/insights',
                pathEdit: '/insight',
                name: 'Insight'
            }),
            params: {
                type: Consts.ARTICLE_TYPE.INSIGHT
            }
        };
    },
    template: template.render()
}
