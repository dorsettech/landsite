import _ from '../utils/utilities';
import template from '../views/articles-datagrid-toolbar.html';

const FILTERS = Object.freeze({
    status: '',
    publish_date: '',
    created: '',
    modified: ''
});

export default {
    data() {
        return {
            params: {},
            timePickerOptions: {
                altInput: true,
                altFormat: 'F j, Y',
                dateFormat: 'Y-m-d',
            }
        };
    },
    props: {
        value: Object,
        addUrl: String,
        addLabel: String
    },
    methods: {
        onRefresh() {
            this.$emit('refresh');
        },
        onChange() {
            this.$nextTick(() => this.$emit('input', this.params));
        },
        reset() {
            this.params.filters = _.shallowCopy(FILTERS);
            this.onChange();
        }
    },
    template: template.render(),
    watch: {
        /**
         * If component keep own state it has to listen changes on model (keep alive)
         */
        value() {
            this.params = !this.value ? {} : _.deepCopy(this.value);
        }
    },
    created() {
        this.params = !this.value ? {} : _.deepCopy(this.value);
    }
}
