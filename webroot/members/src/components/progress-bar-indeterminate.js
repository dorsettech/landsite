import template from '../views/progress-bar-indeterminate.html';

export default {
    props: {
        active: Boolean
    },
    data() {
        return {
            isActive: this.active
        }
    },
    watch: {
        active(value) {
            this.isActive = value
        }
    },
    template: template.render()
}
