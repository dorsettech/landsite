export default {
    props: {
        content: {
            type: Object,
            default: {
                status: 'Status',
                description: 'Status description'
            }
        }
    },
    template: `
        <span v-if="content.description" v-tooltip="{content: content.description}">{{content.status}}</span>
        <span v-else>{{content.status}}</span>
    `
}
