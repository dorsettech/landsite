import template from './../../views/articles/article-box-item.html';
import C from '../../config/constants';
import _ from '../../utils/utilities';
import DateTime from '../../utils/date';

export default {
    data() {
        return {};
    },
    props: {
        item: Object
    },
    computed: {
        publishDate() {
            return new DateTime(this.item.publish_date).format('dS mmmm yyyy');
        },
        articleUrl() {
            return '//' + _.format(C.URL.WWW_ARTICLE, this.item.slug);
        },
        companyUrl() {
            if (!this.item.poster.service) return '';
            return '//' + _.format(C.URL.WWW_SERVICE, `${this.item.poster.service.id}_${this.item.poster.service.slug}`);
        },
        headline() {
            return _.stripTags(this.item.headline);
        }
    },
    methods: {
        onImageSrcError(e) {
            e.target.src = C.IMAGE_PLACEHOLDER;
        },
        onExit() {
            this.$root.loading = true;
        }
    },
    template: template.render()
}
