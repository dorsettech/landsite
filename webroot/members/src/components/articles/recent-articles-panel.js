import template from './../../views/articles/recent-articles-panel.html';
import ArticleBoxItem from './article-box-item';
import C from "../../config/constants";

export default {
    components: {
        ArticleBoxItem
    },
    data() {
        return {
            articles: []
        };
    },
    props: {
        type: {
            type: String,
            default: C.ARTICLE_TYPE.INSIGHT
        },
        autoLoad: {
            type: Boolean,
            default: true
        }
    },
    methods: {
        refresh() {
            return this.load();
        },
        load(page = 1, perPage = 4) {
            return this.$api.get('/Articles/recent', {
                params: {
                    type: this.type,
                    page: page,
                    per_page: perPage
                }
            }).then(response => this.articles = response.data.records);
        }
    },
    template: template.render(),
    created() {
        if (this.autoLoad) {
            this.refresh();
        }
    }
}
