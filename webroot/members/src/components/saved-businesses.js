import template from '../views/saved-businesses.html';
import Loader from '../components/loader';
import Pagination from '../components/pagination';
import BusinessListItem from '../components/business-listing/business-list-item';
import LatestBusinessesAdded from "./latest-businesses-added";
import C from './../config/constants';
import _ from './../utils/utilities';

export default {
    components: {
        Loader,
        Pagination,
        LatestBusinessesAdded,
        BusinessListItem
    },
    data() {
        return {
            items: [],
            loading: false,
            page$: 1,
            pagination: {}
        };
    },
    props: {
        user: Object,
        settings: Object
    },
    watch: {
        '$route': function (to, from) {
            if (to.name === 'SavedBusinesses') {
                if (to.query.reload) {
                    this.reset();
                } else if (to.query.refresh) {
                    this.refresh();
                }
            }
        }
    },
    methods: {
        load(page = 1, perPage = 10) {
            this.loading = true;
            this.page$ = page;
            this.$api.get('/Services/saved', {
                params: {
                    page: page,
                    per_page: perPage
                }
            }).then((response) => {
                this.items = response.data.records.map(item => {
                    item.service.created = item.created;
                    item.service.saved_id = item.id;
                    item.service.isAvailable = item.service.status !== C.STATUS.EXPIRED;
                    if (!item.service.isAvailable) {
                        item.service.message = 'The business is not available anymore.'
                    }
                    return item.service;
                });
                this.pagination = response.data.pagination;
            }).then(() => this.loading = false);
        },
        refresh() {
            this.load(this.page$);
        },
        reset() {
            this.load();
        },
        onPageChange(page) {
            this.load(page);
        },
        delete(data) {
            return this.$api.delete('/Services/deleteSaved', {params: data});
        },
        action(what, item) {
            switch (what) {
                case 'view':
                    if (!item.isAvailable) return;
                    this.loading = true;
                    location.href = '//' + _.format(C.URL.WWW_SERVICE, `${item.id}_${item.slug}`)
                    return;
                case 'delete':
                    this.$snotify.error(`Delete the selected saved service?`, item.title, {
                        timeout: 5000,
                        showProgressBar: true,
                        closeOnClick: false,
                        pauseOnHover: false,
                        position: 'centerCenter',
                        buttons: [{
                            text: 'Proceed',
                            action: (toast) => {
                                this.$snotify.remove(toast.id);
                                this.loading = true;
                                this.delete(item).then((response) => {
                                    this.loading = false;
                                    if (response.data.success) {
                                        this.$snotify.success(response.data.msg);
                                        this.refresh();
                                    } else {
                                        this.$snotify.error(response.data.msg);
                                    }
                                }).catch(error => {
                                    this.$snotify.error(error);
                                });
                            },
                            bold: false
                        }, {
                            text: 'Cancel',
                            action: (toast) => {
                                this.$snotify.remove(toast.id);
                            }
                        }]
                    });
                    return;
                case 'enquiry':
                    if (!item.isAvailable) return;
                    this.loading = true;
                    location.href = '//' + _.format(C.URL.WWW_SERVICE, `${item.id}_${item.slug}`) + '?enquiry=1';
                    return;
            }
        }
    },
    template: template.render(),
    created() {
        this.load();
    }
}
