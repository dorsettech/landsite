import template from '../views/case-studies.html';
import Consts from '../config/constants';
import Articles from '../mixins/articles';

export default {
    mixins: [
        Articles
    ],
    data() {
        return {
            componentMeta: Object.freeze({
                path: '/case-studies',
                pathEdit: '/case-study',
                name: 'Case Study'
            }),
            params: {
                type: Consts.ARTICLE_TYPE.CASE_STUDY
            }
        };
    },
    template: template.render()
}
