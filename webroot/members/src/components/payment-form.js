import template from '../views/payment-form.html';
import ProgressBarIndeterminate from '../components/progress-bar-indeterminate';
import _ from '../utils/utilities';
import DateTime from '../utils/date';
import C from '../config/constants';

export default {
    components: {
        ProgressBarIndeterminate
    },
    data() {
        return {
            formName$: _.guid(),
            service$: null,
            card$: null,
            serviceHasError$: false,
            serviceErrorMsg$: '',
            payment: {
                type: C.PAYMENT_TYPE.STRIPE,
                purchase_order_number: ''
            },
            isPostcodeSuccess: false,
            postcodeMsg: ''
        }
    },
    props: {
        settings: Object,
        user: Object,
        active: {
            type: Boolean,
            default: false
        }
    },
    computed: {
        billing_details() {
            let obj = {
                address: '',
                company: '',
                email: '',
                first_name: '',
                last_name: '',
                phone: '',
                postcode: ''
            };
            if (!this.user.details.use_billing_details) {
                obj = Object.assign(obj, this.user.billing_address);
            } else {
                obj.address = this.user.details.address;
                obj.company = this.user.details.company;
                obj.email = this.user.email;
                obj.first_name = this.user.first_name;
                obj.last_name = this.user.last_name;
                obj.phone = this.user.phone;
                obj.postcode = this.user.details.postcode;
            }
            return obj;
        }
    },
    methods: {
        onInputChange(e, type) {
            let input = e.target;
            input.checkValidity();
            if (input.validity.valid) {
                e.target.classList.remove('is-invalid');
            }

            if (type === 'postcode') {
                this.isPostcodeSuccess = _.validate.postcode(this.billing_details.postcode);
                this.postcodeMsg = this.isPostcodeSuccess ? '' : 'Please provide a valid postcode';
            }
        },
        onInputInvalid(e) {
            e.target.classList.add('is-invalid');
        },
        getForm() {
            return document.forms[this.formName$];
        },
        reset() {
            this.getForm().reset();
            if (this.card$) {
                this.card$.clear();
            }
            this.getForm().querySelectorAll('.is-invalid').forEach(node => node.classList.remove('is-invalid'));
            this.payment.type = C.PAYMENT_TYPE.STRIPE;
            this.$el.querySelector('[name="payment_type"]').checked = true;
            this.payment.purchase_order_number = '';
            this.isPostcodeSuccess = false;
            this.postcodeMsg = '';
            this.$forceUpdate();
        },
        getBillingDetails() {
            return this.billing_details;
        },
        createPayment() {
            if (this.payment.type === C.PAYMENT_TYPE.STRIPE) {
                let extra = {
                    name: `${this.billing_details.first_name} ${this.billing_details.last_name}`,
                    address_line1: this.billing_details.address,
                    address_zip: this.billing_details.postcode,
                    address_country: 'UK',
                    currency: this.settings.api.stripe.currency
                };
                return new Promise((resolve, reject) => {
                    this.service$.createToken(this.card$, extra).then(result => {
                        if (result.error) {
                            reject(result.error.message);
                            return;
                        }
                        resolve({
                            type: C.PAYMENT_TYPE.STRIPE,
                            token: result.token,
                            billing_details: this.getBillingDetails()
                        });
                    });
                });
            } else {
                return new Promise((resolve) => {
                    _.ip().then(ip => {
                        resolve({
                            type: C.PAYMENT_TYPE.ORDER,
                            token: {
                                id: this.payment.purchase_order_number,
                                object: 'token',
                                client_ip: ip,
                                created: DateTime.ts(),
                                type: C.PAYMENT_TYPE.ORDER.toLowerCase()
                            },
                            billing_details: this.getBillingDetails()
                        })
                    })
                });
            }
        }
    },
    template: template.render(),
    mounted() {
        this.service$ = Stripe(this.settings.api.stripe['public-key']);
        let elements = this.service$.elements(),
            card = elements.create('card', {
                style: {
                    base: {
                        fontSize: '13px'
                    }
                }
            });
        card.mount(this.$el.querySelector('.payment-stripe-card-element'));
        card.addEventListener('change', (event) => {
            this.serviceHasError$ = !!event.error;
            this.serviceErrorMsg$ = event.error ? event.error.message : '';
        });
        this.card$ = card;
    }
}
