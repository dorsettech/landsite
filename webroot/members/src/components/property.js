import template from '../views/property.html';
import Loader from '../components/loader';
import LatestBusinessesAdded from '../components/latest-businesses-added';
import PaymentSummary from '../components/payment-summary';
import PaymentForm from '../components/payment-form';
import InputVideoPreview from '../components/input-video-preview';
import InputImagesUpload from '../components/input-images-upload';
import InputDocumentsUpload from '../components/input-documents-upload';
import CheckboxGroup from '../components/checkbox-group';
import Consts from '../config/constants';
import _ from '../utils/utilities';
import DateTime from '../utils/date';
import Slugify from '../utils/slugify';

const
    MODEL = Object.freeze({
        reference: '',
        type_id: null,
        purpose: Consts.PROPERTY_PURPOSE.SALE,
        title: '',
        slug: '',
        price_sale: 0,
        price_rent: 0,
        price_rent_per: Consts.PROPERTY_PRICE_RENT_PER.YEAR,
        price_sale_per: Consts.PROPERTY_PRICE_SALE_PER.FULL,
        price_rent_qualifier: Consts.PROPERTY_PRICE_QUALIFIER.DEFAULT,
        price_sale_qualifier: Consts.PROPERTY_PRICE_QUALIFIER.DEFAULT,
        location: '',
        lat: null,
        lng: null,
        postcode: '',
        town: '',
        headline: '',
        description: '',
        website_url: '',
        status: Consts.STATUS.DRAFT,
        publish_date: null,
        ad_type: Consts.AD_TYPE.STANDARD,
        ad_cost: 0,
        ad_total_cost: 0,
        ad_basket: [],
        property_media: [],
        properties_property_attributes: [],
        under_offer: false,
        auction: false,
        comm_use_class: [],
        // virtual fields
        ad_extend: false,
        ad_payment: null,
        ad_discount_code: null,
        ad_discount: false,
        ad_vat: 0
    }),
    STEP_PAYMENT_FORM = 4;

export default {
    components: {
        Loader,
        LatestBusinessesAdded,
        PaymentSummary,
        PaymentForm,
        CheckboxGroup,
        InputVideoPreview,
        InputImagesUpload,
        InputDocumentsUpload
    },
    data() {
        return {
            isEditMode: false,
            step: 0,
            form: this.record || _.shallowCopy(MODEL),
            payments: [],
            discountCode: '',
            total$: 0,
            vat$: 0,
            editorToolbar: [
                ['bold', 'italic', 'underline', {'script': 'sub'}, {'script': 'super'}],
                ['align', {'align': 'center'}, {'align': 'right'}, {'align': 'justify'}],
                ['blockquote'],
                [{'list': 'ordered'}, {'list': 'bullet'}],
                [{'indent': '-1'}, {'indent': '+1'}, 'clean']
            ],
            editorOptions: {
                formats: ['bold', 'italic', 'script', 'underline', 'blockquote', 'indent', 'list', 'align']
            },
            moneyOptions: {
                decimal: ',',
                thousands: '',
                prefix: this.settings.currency.symbol,
                suffix: '',
                precision: 0
            },
            buttonSubmit: {
                label: '',
                icons: {
                    'fa-camera': false,
                    'fa-file-alt': false,
                    'fa-credit-card': false,
                    'fa-save': false
                },
                green: false
            },
            types: [],
            useClasses: [],
            propertyAttributes: [],
            titleLimit: 70,
            titleLength: 0,
            locationLimit: 70,
            locationLength: 0,
            headlineLimit: 1000,
            headlineLength: 0,
            descriptionLimit: 32e3,
            descriptionLength: 0,
            isPostcodeSuccess: false,
            postcodeDisabled: false,
            postcodeMsg: '',
            isWorking: false,
            loading: true,
            hasError: false,
            message: '',
            mute$: false,
            disableBackToEdit: false,
            imagesHardLimit: 0
        }
    },
    computed: {
        isBothPurpose() {
            return this.form.purpose === Consts.PROPERTY_PURPOSE.BOTH;
        },
        getMediaUrl() {
            return _.format(Consts.PATH.MEMBERS_PROPERTY, this.user.uid)
        },
        isDraft() {
            return this.form.status === Consts.STATUS.DRAFT;
        },
        video: {
            get() {
                if (!this.form.property_media) return '';
                let v = this.form.property_media.find(item => {
                    return item.type === Consts.MEDIA_TYPE.VIDEO;
                });
                if (v) {
                    try {
                        let url = new URL(v.source);
                        return url.toString();
                    } catch (e) {
                        return `https://www.youtube.com/watch?v=${v.source}`;
                    }
                }
                return '';
            },
            set(value) {
                this.form.property_media = this.form.property_media || [];
                let v = this.form.property_media.find(item => {
                    return item.type === Consts.MEDIA_TYPE.VIDEO;
                });
                if (v) {
                    if (value === '') {
                        this.form.property_media = Array.from(this.form.property_media).filter(item => {
                            return item.type !== Consts.MEDIA_TYPE.VIDEO;
                        });
                    } else {
                        v.source = value;
                    }
                } else {
                    this.form.property_media.push({
                        service_id: this.form.id,
                        type: Consts.MEDIA_TYPE.VIDEO,
                        source: value
                    });
                }
            }
        },
        images: {
            get() {
                if (!this.form.property_media) return [];
                return this.form.property_media.filter(item => {
                    return item.type === Consts.MEDIA_TYPE.IMAGE;
                });
            },
            set(value) {
                this.form.property_media = this.form.property_media.filter((item) => {
                    return item.type === Consts.MEDIA_TYPE.VIDEO || item.type === Consts.MEDIA_TYPE.DOCUMENT;
                }).concat(Array.from(value));
            }
        },
        documents: {
            get() {
                if (!this.form.property_media) return [];
                return this.form.property_media.filter(item => {
                    return item.type === Consts.MEDIA_TYPE.DOCUMENT;
                });
            },
            set(value) {
                this.form.property_media = this.form.property_media.filter((item) => {
                    return item.type === Consts.MEDIA_TYPE.VIDEO || item.type === Consts.MEDIA_TYPE.IMAGE;
                }).concat(Array.from(value));
            }
        },
        property_attributes: {
            get() {
                if (!this.form.properties_property_attributes) return [];
                return this.form.properties_property_attributes.map((item) => {
                    return item.property_attribute_id;
                });
            },
            set(value) {
                value = Array.from(value);
                this.form.properties_property_attributes = this.form.properties_property_attributes || [];
                this.form.properties_property_attributes = value.map(id => {
                    return {
                        attribute_value: true,
                        property_attribute_id: id,
                        property_id: this.form.id
                    }
                });
            }
        },
        slug() {
            this.form.slug = Slugify.toSafeURL(this.form.title);
            return this.form.slug;
        },
        plan: {
            get() {
                return [this.form.ad_type];
            },
            set(value) {
                this.form.ad_type = value.slice(-1)[0];
            }
        },
        isPayableItemAvailable() {
            if (this.isEditMode) {
                return this.form.status === Consts.STATUS.DRAFT || this.form.status === Consts.STATUS.EXPIRED;
            } else {
                return true;
            }
        },
        getExtendedExpiryDate() {
            if (!this.form.ad_type) {
                return '';
            }
            let exp = new Date(this.form.ad_expiry_date);
            exp.setDate(exp.getDate() + this.settings.validity[`property-${this.form.ad_type.toLowerCase()}-days`]);
            return new DateTime(exp).format('dS mmmm yyyy');
        },
        hasToPay() {
            return this.total$ > 0;
        },
        priceSaleUnitNames() {
            return Object.keys(Consts.PROPERTY_PRICE_SALE_PER_NAMES).map(key => {
                return {
                    value: key,
                    text: Consts.PROPERTY_PRICE_SALE_PER_NAMES[key]
                }
            })
        },
        priceRentUnitNames() {
            return Object.keys(Consts.PROPERTY_PRICE_RENT_PER_NAMES).map(key => {
                return {
                    value: key,
                    text: Consts.PROPERTY_PRICE_RENT_PER_NAMES[key]
                }
            })
        }
    },
    props: {
        user: Object,
        settings: Object,
        record: Object,
        purpose: String,
        tab: Number
    },
    watch: {
        step(next) {
            this.setSubmitButton(next);
            if (next === STEP_PAYMENT_FORM) {
                this.$refs.PaymentForm.reset();
            }
        },
        'form.ad_type': function () {
            this.refreshSummary();
        },
        'form.property_media': function () {
            this.refreshSummary();
        },
        'form.ad_extend': function () {
            this.refreshSummary();
        },
        '$route': function (to) {
            if (to.name === 'PropertyAdd') {
                // add new record
                let model = _.shallowCopy(MODEL);
                if (this.purpose) {
                    model.purpose = this.purpose;
                }
                this.form = model;
                this.hasError = false;
                this.message = '';
                this.resetForm();
                this.isEditMode = false;
                this.goTo(0, true);
                this.disableBackToEdit = false;
                this.setImagesHardLimit();
            } else if (to.name === 'PropertyEdit') {
                // edit record (passed through query object)
                this.form = this.record;
                this.hasError = false;
                this.message = '';
                this.resetForm();
                this.isEditMode = true;
                this.checkAndLoad(to.params.id, this.tab);
            }
        }
    },
    methods: {
        priceQualifiers(type = null) {
            return Object.keys(Consts.PROPERTY_PRICE_QUALIFIER_NAMES).filter(item => {
                if (type === null) {
                    return true;
                }
                if (item === Consts.PROPERTY_PRICE_QUALIFIER.NOW_LET && type !== Consts.PROPERTY_PURPOSE.RENT) {
                    return false;
                } else if (item === Consts.PROPERTY_PRICE_QUALIFIER.SOLD_SUBJECT_TO_CONTRACT && type !== Consts.PROPERTY_PURPOSE.SALE) {
                    return  false;
                }
                return true;
            }).map(key => {
                return {
                    value: key,
                    text: Consts.PROPERTY_PRICE_QUALIFIER_NAMES[key]
                }
            })
        },
        resetForm() {
            document.forms['property-details-form'].querySelectorAll('.is-invalid').forEach(node => node.classList.remove('is-invalid'));
            this.titleLength = 0;
            this.descriptionLength = 0;
            this.locationLength = 0;
            this.headlineLength = 0;
            this.postcodeMsg = '';
            this.$refs.InputImagesUpload.reset();
            this.$refs.InputFilesUpload.reset();
        },
        updateCounters() {
            this.titleLength = this.form.title.length;
            this.descriptionLength = this.form.description.replace(/(<([^>]+)>)/ig, '').length;
            this.locationLength = this.form.location.length;
            this.headlineLength = this.form.headline.length;
        },
        refreshSummary() {
            if (typeof this.form.ad_type === 'undefined') return;
            let plan = this.form.ad_type,
                websiteUrl = this.form.website_url,
                basket = [];

            if (!this.isEditMode || this.isPayableItemAvailable || this.form.ad_extend) {
                basket.push({
                    type: Consts.PAYMENT_ITEM.PLAN,
                    price: this.settings.prices[`property-${plan.toLowerCase()}`],
                    name: `Ad posting (${plan})`
                });
            }

            if (!_.empty(websiteUrl) && this.settings.prices['property-website-url'] > 0 && this.isPayableItemAvailable) {
                basket.push({
                    type: Consts.PAYMENT_ITEM.LINK,
                    price: this.settings.prices['property-website-url'],
                    name: 'Website link'
                });
            }

            if (this.settings.prices['property-image'] > 0 && this.isPayableItemAvailable) {
                let imgAmount = this.form.property_media.reduce((p, n) => {
                    return p + (n.type === Consts.MEDIA_TYPE.IMAGE ? 1 : 0);
                }, 0);
                let extraImgs = imgAmount - 5;
                if (extraImgs > 0) {
                    basket.push({
                        type: Consts.PAYMENT_ITEM.IMAGE,
                        price: extraImgs * this.settings.prices['property-image'],
                        name: extraImgs > 1 ? `Extra images (${extraImgs})` : 'Extra image'
                    });
                }
            }

            this.payments = basket;
        },
        onSummaryChange(total, vat) {
            this.total$ = total;
            this.vat$ = vat;
            this.setSubmitButton(this.step);

            if (this.isPaymentPage() && this.total$ === 0) {
                this.prev();
            }
        },
        isStandardPlanOnly() {
            return this.settings.plans[`property-${Consts.AD_TYPE.FEATURED}`] === false && this.settings.plans[`property-${Consts.AD_TYPE.PREMIUM}`] === false;
        },
        isPaymentPage() {
            return this.step === STEP_PAYMENT_FORM;
        },
        next() {
            if (this.step === 3) return;
            this.step++;
        },
        isLastStep() {
            return this.step === 3;
        },
        prev() {
            if (this.step === 0) return;
            this.step--;
        },
        isStepDisabled(step) {
            if (this.isEditMode) return false;
            return !(step < this.step);
        },
        goTo(step, force = false) {
            if (force === false && this.isStepDisabled(step)) return false;
            this.step = step;
        },
        setSubmitButton(next = this.step) {
            Object.entries(this.buttonSubmit.icons).forEach((icon) => this.buttonSubmit.icons[icon[0]] = false);
            this.buttonSubmit.green = false;

            if (this.isEditMode) {

                this.buttonSubmit.label = !this.hasToPay ? 'Save property' : 'Proceed to payment';
                this.buttonSubmit.green = !this.hasToPay;

                if (this.hasToPay) {
                    this.buttonSubmit.icons['fa-credit-card'] = true;
                } else {
                    this.buttonSubmit.icons['fa-save'] = true;
                }

                if (next === STEP_PAYMENT_FORM) {
                    this.buttonSubmit.label = 'Submit Payment';
                }
            } else {
                switch (next) {
                    case 0:
                        this.buttonSubmit.label = 'Continue - add images & video';
                        this.buttonSubmit.icons['fa-camera'] = true;
                        return;
                    case 1:
                        this.buttonSubmit.label = 'Continue - add documents';
                        this.buttonSubmit.icons['fa-file-alt'] = true;
                        return;
                    case 2:
                        this.buttonSubmit.label = 'Continue - make your ad stand out';
                        this.buttonSubmit.icons['fa-check'] = true;
                        return;
                    case 3:
                        this.buttonSubmit.label = !this.hasToPay ? 'Save property' : 'Proceed to payment';
                        this.buttonSubmit.green = !this.hasToPay;

                        if (this.hasToPay) {
                            this.buttonSubmit.icons['fa-credit-card'] = true;
                        } else {
                            this.buttonSubmit.icons['fa-save'] = true;
                        }
                        return;
                    case STEP_PAYMENT_FORM:
                        this.buttonSubmit.icons['fa-credit-card'] = true;
                        this.buttonSubmit.label = 'Submit Payment';
                        return;
                    default:
                        this.buttonSubmit.label = 'Opss! Something wrong';
                }
            }
        },
        togglePlanTo(plan) {
            this.plan = [plan]
        },
        preventIfLessThanOneCheckedPlan() {
            if (typeof this.form.ad_type === 'undefined') {
                this.$nextTick(() => this.plan = [Consts.AD_TYPE.STANDARD]);
            }
        },
        onInput(name) {
            if (['title', 'location', 'headline'].indexOf(name) !== -1) {
                this.updateCounters();
            }
        },
        onInputChange(e, type) {
            let input = e.target;
            input.checkValidity();
            if (input.validity.valid) {
                e.target.classList.remove('is-invalid');
            }

            if (type === 'postcode') {
                this.postcodeToLocation();
            } else if (['title', 'location'].indexOf(type) !== -1) {
                this.updateCounters();
            } else if (type === 'website_url') {
                this.refreshSummary();
            }
        },
        onInputInvalid(e) {
            e.target.classList.add('is-invalid');
        },
        onTypeChange(value) {
            if (_.empty(value)) {
                this.propertyAttributes = [];
                return;
            }
            let fn = function (arr, id) {
                    let item;
                    for (let el of arr) {
                        if (el.id === id) {
                            return el;
                        }
                        item = fn(el.children, id);
                        if (item) return item;
                    }
                    return item;
                },
                item = fn(this.types, value);

            this.propertyAttributes = item ? item.property_attributes : [];
        },
        onEditorChange() {
            this.descriptionLength = this.form.description.replace(/(<([^>]+)>)/ig, '').length;
            if (this.descriptionLength > this.descriptionLimit) {
                this.$refs.DescriptionEditor.quill.deleteText(this.descriptionLimit, this.descriptionLength);
            }
        },
        onImagesUpload() {
            this.filesUpload(Consts.MEDIA_TYPE.IMAGE, ...arguments);
        },
        onDocumentsUpload() {
            this.filesUpload(Consts.MEDIA_TYPE.DOCUMENT, ...arguments);
        },
        onImageChangeOrder() {
            this.fileChangeOrder(Consts.MEDIA_TYPE.IMAGE, ...arguments);
        },
        onDocumentChangeOrder() {
            this.fileChangeOrder(Consts.MEDIA_TYPE.DOCUMENT, ...arguments);
        },
        filesUpload(type, msg, results) {
            this.form.property_media = this.form.property_media.concat(results.filter(file => file.success).map(file => {
                return {
                    property_id: this.form.id,
                    type: type,
                    source: file.filename,
                    extra: file[type.toLowerCase()]
                }
            }));
            // re-index elements
            let index = 0;
            this.form.property_media.forEach((media) => {
                if (media.type !== type) {
                    return;
                }
                media.position = index;
                index++;
            });
        },
        fileChangeOrder(type, file, dir) {
            let index = 0,
                images = this.form.property_media.filter(file => file.type === type),
                currIndex = images.indexOf(file),
                newIndex = dir === 'down' ? currIndex - 1 : currIndex + 1;
            if (newIndex < 0 || newIndex >= images.length) {
                return;
            }
            _.move(images, currIndex, newIndex);
            images.forEach((media) => {
                media.position = index;
                index++;
            });
            this.form.property_media = images.concat(this.form.property_media.filter(file => file.type !== type));
        },
        postcodeToLocation() {
            if (!_.validate.postcode(this.form.postcode)) {
                this.isPostcodeSuccess = false;
                this.postcodeMsg = 'Please provide a valid postcode';
                return;
            }

            this.postcodeDisabled = true;
            let apiKey = this.settings.api['google-map'];
            axios.get('https://maps.googleapis.com/maps/api/geocode/json', {
                params: {
                    address: this.form.postcode,
                    key: apiKey
                }
            }).then(response => response.data).then(function (response) {
                this.postcodeDisabled = false;
                if (response.status !== 'OK') {
                    this.form.lat = null;
                    this.form.lng = null;
                    this.postcodeMsg = _.empty(response.error_message) ? 'Please provide a valid postcode' : response.error_message;
                    this.isPostcodeSuccess = false;
                    return;
                }

                this.postcodeMsg = response.results[0].formatted_address;
                this.isPostcodeSuccess = true;
                this.form.lat = response.results[0].geometry.location.lat;
                this.form.lng = response.results[0].geometry.location.lng;
            }.bind(this));
        },
        save(type = 'draft') {
            this.isWorking = true;
            return new Promise((resolve, reject) => {
                let data = Object.assign({}, this.form);

                data.property_media = data.property_media.filter(item => item.source !== '');

                if (typeof data.price_sale === 'string') {
                    data.price_sale = parseFloat(data.price_sale.substring(1))
                }
                if (typeof data.price_rent === 'string') {
                    data.price_rent = parseFloat(data.price_rent.substring(1))
                }

                data.ad_discount_code = this.discountCode;
                data.ad_discount = this.$refs.PaymentSummary.getDiscount();

                this.$api.post(`/Properties/${type}`, data).then(response => {
                    this.isWorking = false;
                    if (response.data.success) {
                        resolve(response);
                    } else {
                        reject(response.data.msg);
                    }
                }).catch(error => {
                    this.isWorking = false;
                    reject(error)
                });
            });
        },
        getCurrentForm() {
            switch (this.step) {
                case 0:
                    return document.forms['property-details-form'];
                case 3:
                    return document.forms['property-stand-out-form'];
                case STEP_PAYMENT_FORM:
                    return this.$refs.PaymentForm.getForm();
            }
        },
        resetFormMessage() {
            this.hasError = false;
            this.message = '';
        },
        onSaveDraft() {
            let form = this.getCurrentForm();
            this.hasError = _.empty(form) ? false : !form.checkValidity();
            this.message = this.hasError ? 'This form has errors' : '';

            const hasError = this.hasError;
            return new Promise(function (resolve, reject) {
                if (hasError) {
                    reject();
                    return;
                }
                this.save('draft').then((response) => {
                    if (!this.mute$) {
                        this.message = response.data.msg;
                        this.$snotify.success(response.data.msg);
                    }
                    this.mute$ = false;
                    this.form.property_media = response.data.media;
                    this.form.id = response.data.id;
                    resolve();
                }).catch(error => {
                    this.hasError = true;
                    this.message = error;
                    this.$snotify.success(error);
                    reject();
                });
            }.bind(this));
        },
        goToProperties(reload = true) {
            this.isWorking = true;
            (() => {
                this.$router.push({
                    path: '/properties',
                    query: reload ? {
                        reload: 1
                    } : {
                        refresh: 1
                    }
                });
                this.isWorking = false;
            }).delay(2000);
        },
        setImagesHardLimit() {
            if (this.form.status !== Consts.STATUS.DRAFT) {
                this.imagesHardLimit = this.images.length > 5 ? this.images.length : 5;
            } else {
                if (this.settings.prices['property-image'] === 0) {
                    this.imagesHardLimit = 5;
                } else {
                    this.imagesHardLimit = 0;
                }
            }
            this.$nextTick(() => this.$refs.InputImagesUpload.refresh());
        },
        pay() {
            this.isWorking = true;
            this.disableBackToEdit = true;
            return new Promise((resolve, reject) => {
                this.$refs.PaymentForm.createPayment().then(result => {
                    this.form.ad_payment = result;
                    this.form.ad_basket = this.payments;
                    this.form.ad_cost = this.total$;
                    this.form.ad_vat = this.vat$;
                    resolve();
                }).catch(error => {
                    this.isWorking = false;
                    reject(error);
                });
            });
        },
        onSave() {
            let form = this.getCurrentForm();
            this.hasError = _.empty(form) ? false : !form.checkValidity();
            this.message = this.hasError ? 'This form has errors' : '';
            if (this.hasError) return;

            if (this.isEditMode) {
                let fn = () => {
                    return this.save('update').then((response) => {
                        this.resetFormMessage();
                        if (response.data.success) {
                            this.form.property_media = response.data.media;
                            this.$snotify.success(response.data.msg);
                            this.$bus.$emit('dashboard-refresh');
                            this.goToProperties(false);
                        } else {
                            this.$snotify.error(response.data.msg);
                        }
                    }).catch(error => {
                        this.hasError = true;
                        this.message = error;
                    });
                };
                if (this.isPaymentPage()) {
                    this.pay().then(() => fn()).catch(error => this.$snotify.error(error));
                } else if (this.hasToPay) {
                    this.goTo(STEP_PAYMENT_FORM, true);
                } else {
                    fn()
                }
            } else {
                let fn = () => {
                    return this.save('update').then((response) => {
                        this.resetFormMessage();
                        if (response.data.success) {
                            this.form.property_media = response.data.media;
                            this.$snotify.success(response.data.msg);
                            this.goTo(0, true);
                            this.isEditMode = true;
                            this.$bus.$emit('dashboard-refresh');
                            this.goToProperties();
                        } else {
                            this.$snotify.error(response.data.msg);
                        }

                    }).catch(error => {
                        this.hasError = true;
                        this.message = error;
                    });
                };
                if (this.isPaymentPage()) {
                    this.pay().then(() => fn()).catch(error => this.$snotify.error(error));
                } else if (this.isLastStep()) {
                    if (this.hasToPay) {
                        this.goTo(STEP_PAYMENT_FORM, true);
                    } else {
                        fn();
                    }
                } else {
                    this.mute$ = true;
                    this.onSaveDraft().then(() => {
                        this.resetFormMessage();
                        this.next();
                    });
                }
            }
        },
        checkAndLoad(id, tab = 0) {
            tab = isNaN(tab) ? 0 : tab;
            if (!id) {
                this.isEditMode = false;
            } else {
                this.isEditMode = true;
            }
            this.setSubmitButton();
            this.disableBackToEdit = false;
            if (this.isEditMode && !this.form.id || this.form.id !== id) {
                this.$api.get('/Properties/property', {
                    params: {
                        id: id
                    }
                }).then(response => {
                    this.form = response.data;
                    this.form.comm_use_class = _.empty(this.form.comm_use_class) ? [] : (Array.isArray(this.form.comm_use_class) ? this.form.comm_use_class : this.form.comm_use_class.split(','));
                    this.postcodeToLocation();
                    this.updateCounters();
                    this.onTypeChange(this.form.type_id);
                    this.$refs.PaymentSummary.clearDiscount(true);
                    if (!_.empty(response.data.ad_basket) && !response.data.visibility) {
                        this.goTo(STEP_PAYMENT_FORM, true);
                        this.disableBackToEdit = true;
                        (() => {
                            try {
                                this.payments = JSON.parse(response.data.ad_basket)
                            } catch (e) {
                                this.payments = [];
                            }
                        }).delay(100);
                    } else {
                        this.refreshSummary();
                        this.goTo(0, true);
                    }
                    this.setImagesHardLimit();
                    this.loading = false;
                });
            } else {
                this.loading = false;
                this.postcodeToLocation();
                this.updateCounters();
                this.onTypeChange(this.form.type_id);
                this.setImagesHardLimit();
                this.$refs.PaymentSummary.clearDiscount(true);
                if (!_.empty(this.form.ad_basket) && !this.form.visibility) {
                    this.goTo(STEP_PAYMENT_FORM, true);
                    this.disableBackToEdit = true;
                    (() => {
                        try {
                            this.payments = JSON.parse(this.form.ad_basket)
                        } catch (e) {
                            this.payments = [];
                        }
                    }).delay(100);
                } else {
                    this.refreshSummary();
                    this.goTo(tab, true);
                    if (this.purpose) {
                        this.form.purpose = this.purpose;
                    }
                }
            }
        }
    },
    template: template.render(),
    created() {
        this.$api('/PropertyOptions').then((response) => {
            this.types = response.data.types;
            this.useClasses = response.data.classes;
        }).then(() => this.$nextTick(() => this.checkAndLoad(this.$route.params.id, this.$route.query.tab)));
    }
}
