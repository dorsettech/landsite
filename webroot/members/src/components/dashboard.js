import DashboardHeader from './dashboard-header';
import DashboardSidebar from './dashboard-sidebar';
import DashboardContent from './dashboard-content';
import template from '../views/dashboard.html';

export default {
    components: {
        DashboardHeader,
        DashboardSidebar,
        DashboardContent
    },
    props: {
        rawConfig: String
    },
    data() {
        return {
            showContainer: false,
            sidebarCollapsed: false,
            noSidebar: false,
            config$: null
        };
    },
    computed: {
        config: {
            get: function () {
                if (this.config$ !== null) {
                    return this.config$;
                }
                return JSON.parse(atob(this.rawConfig));
            },
            set: function (value) {
                this.config$ = value;
            }
        },
        user() {
            return this.config.user;
        },
        settings() {
            return this.config.settings;
        }
    },
    watch: {
        user() {
            this.checkUserRequirements();
        }
    },
    methods: {
        toggleSidebar(value) {
            this.sidebarCollapsed = value || !this.sidebarCollapsed;
        },
        refresh() {
            this.$api.post('/Dashboard/refresh').then((response) => this.config = response.data.config);
        },
        checkUserRequirements() {
            let isProfileCompleted = this.user.details && this.user.details.profile_completion;
            if (!isProfileCompleted) {
                this.noSidebar = true;
                this.$router.push('welcome');
                return;
            }
            this.noSidebar = false;
        },
        checkViewport() {
            if (Math.max(document.documentElement.clientWidth, window.innerWidth || 0) <= 767) {
                this.toggleSidebar(true);
            }
        }
    },
    template: template.render(),
    mounted() {
        this.showContainer = true
    },
    created() {
        window.addEventListener('resize', this.checkViewport.bind(this));
        this.$bus.$on('toggle-sidebar', this.toggleSidebar);
        this.$bus.$on('dashboard-refresh', this.refresh);
        this.checkUserRequirements();
        this.checkViewport();
    }
}
