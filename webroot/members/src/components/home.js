import template from '../views/home.html';
import RecentArticlesPanel from '../components/articles/recent-articles-panel';
import PropertyEnquiries from '../components/property/property-enquiries';
import BusinessEnquiries from '../components/business-listing/business-enquiries';
import IconBox from './../components/dashboard/icon-box';
import IconBoxCounter from './../components/dashboard/icon-box-counter';
import IconBoxButton from './../components/dashboard/icon-box-button';
import SearchBox from './../components/dashboard/search-box';
import Loader from './../components/loader';
import C from './../config/constants';
import _ from './../utils/utilities';

export default {
    components: {
        Loader,
        IconBox,
        IconBoxCounter,
        IconBoxButton,
        RecentArticlesPanel,
        PropertyEnquiries,
        BusinessEnquiries,
        SearchBox
    },
    data() {
        return {
            loading: false,
            counters: {
                properties: 0,
                properties_enquiries: 0,
                properties_views: 0,
                service_enquiries: 0,
                service_views: 0
            },
            interval$: null,
            pooling$: false
        };
    },
    props: {
        user: Object,
        settings: Object
    },
    methods: {
        refresh() {
            this.$refs.RecentArticles.refresh();
            if (this.$refs.PropertiesEnquiries) {
                this.$refs.PropertiesEnquiries.refresh();
            }
        },
        onIconBoxClick(what) {
            switch (what) {
                case 'add-property':
                    this.$router.push('add-property');
                    return;
                case 'add-service':
                    this.$router.push('business-listing');
                    return;
                case 'service':
                    if (this.counters.business) {
                        this.$root.loading = true;
                        location.href = '//' + _.format(C.URL.WWW_SERVICE, `${this.counters.business.id}_${this.counters.business.slug}`);
                    }
                    return;
                case 'search-property':
                    this.$root.loading = true;
                    location.href = '//' + C.URL.WWW_PROPERTIES;
                    return;
                case 'search-service':
                    this.$root.loading = true;
                    location.href = '//' + C.URL.WWW_SERVICES;
                    return;
                case 'properties':
                    this.$router.push('properties');
                    return;
            }
        },
        pool() {
            if (this.pooling$) return;
            this.pooling$ = true;
            return this.$api.get('/Dashboard/stats', {
                type: this.user.details.dashboard_type
            }).then(response => {
                Object.assign(this.counters, response.data);
                this.pooling$ = false;
            }).catch(() => this.pooling$ = false);
        },
        startPooling() {
            if (!this.user || !this.user.details || !this.user.details.dashboard_type) return;
            this.interval$ = setInterval(() => {
                this.pool();
            }, 10000)
        },
        stopPooling() {
            clearInterval(this.interval$);
        }
    },
    watch: {
        '$route': function (to, from) {
            if (to.name === 'Dashboard') {
                this.refresh();
                this.pool().then(() => this.startPooling());
            }
        }
    },
    beforeRouteLeave: function (to, from, next) {
        this.stopPooling();
        /*
         * Make sure to always call the next function, otherwise the hook will never be resolved
         * @see https://router.vuejs.org/en/advanced/navigation-guards.html
         */
        next();
    },
    template: template.render(),
    created() {
        (() => {
            this.pool().then(() => this.startPooling());
        }).delay(200);
    }
}
