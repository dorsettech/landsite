import template from '../views/email-verification-form.html';

export default {
    data() {
        return {
            form: {
                email: {
                    value: '',
                    isInvalid: false
                }
            },
            hasError: false,
            isWorking: false
        }
    },
    props: {
        active: {
            type: Boolean,
            default: true
        },
        message: {
            type: String,
            default: ''
        }
    },
    methods: {
        onSubmit: function (e) {
            this.$root.loading = true;
            this.isWorking = true;
            let data = {};
            for (const [key, obj] of Object.entries(this.form)) data[key] = obj.value;
            this.$api.post('/User/verifyEmail', data).then(function (response) {
                this.hasError = !response.data.success;
                this.message = response.data.msg;
                if (response.data.success) {
                    (() => location.href = location.protocol + '//' + location.host + '/login').delay(5000);
                } else {
                    this.isWorking = false;
                }
            }.bind(this)).catch(function (error) {
                this.hasError = true;
                this.message = error.message;
                this.isWorking = false;
            }.bind(this)).then(function () {
                this.$root.loading = false;
            }.bind(this));
        },
        onChange: function (field, e) {
            let input = e.target;
            input.checkValidity();
            this.form[field].isInvalid = this.form[field].value === '' ? false : !input.validity.valid;
        }
    },
    template: template.render()
}
