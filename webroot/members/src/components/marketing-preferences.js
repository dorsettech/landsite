import template from '../views/marketing-preferences.html';
import LatestBusinessesAdded from './latest-businesses-added';
import Utils from "../utils/utilities";

export default {
    components: {
        LatestBusinessesAdded
    },
    data() {
        return {
            profile: {
                details: {}
            },
            isWorking: false,
            hasError: false,
            message: ''
        };
    },
    props: {
        user: Object
    },
    methods: {
        onPrefChange(e, field) {
            let checked = e.target.checked;
            if (!checked) {
                this.$nextTick(() => this.profile.details[field] = true);
                return;
            }
            switch (field) {
                case 'marketing_agreement_1':
                    this.profile.details.marketing_agreement_2 = false;
                    this.profile.details.marketing_agreement_3 = false;
                    return;
                case 'marketing_agreement_2':
                    this.profile.details.marketing_agreement_1 = false;
                    this.profile.details.marketing_agreement_3 = false;
                    return;
                case 'marketing_agreement_3':
                    this.profile.details.marketing_agreement_1 = false;
                    this.profile.details.marketing_agreement_2 = false;
                    return;
            }
        },
        onSubmit() {
            this.isWorking = true;
            this.$api.post('/User/update', this.profile).then(function (response) {
                this.hasError = !response.data.success;
                this.message = response.data.msg;
                if (this.hasError) {
                    this.$snotify.error(response.data.msg);
                } else {
                    this.$snotify.success(response.data.msg);
                }
                if (response.data.success) {
                    this.$bus.$emit('dashboard-refresh');
                }
                (() => this.message = '').delay(3000);
            }.bind(this)).catch(function (error) {
                this.hasError = true;
                this.message = error.message;
            }.bind(this)).then(function () {
                this.isWorking = false;
            }.bind(this));
        }
    },
    template: template.render(),
    /**
     * Prevent mutations
     */
    created() {
        this.profile = Utils.deepCopy(this.user);
    }
}
