import template from './../../views/properties/property-list-item.html';
import PropertyListItemChart from './property-list-item-chart';
import CONSTS from '../../config/constants';
import _ from '../../utils/utilities';
import DateTime from '../../utils/date';

export default {
    components: {
        PropertyListItemChart
    },
    data() {
        return {
            statusCls: {
                'status-draft': false,
                'status-expired': false,
                'status-published': false
            },
            button: {
                iconCls: {
                    'fa-clipboard-check': false,
                    'fa-eye': false,
                    'fa-redo-alt': false
                },
                text: ''
            },
            action$: ''
        };
    },
    props: {
        currencySymbol: {
            type: String,
            default: '£'
        },
        item: Object,
        chart: {
            type: Boolean,
            default: true
        },
        cost: {
            type: Boolean,
            default: true
        },
        buttons: {
            type: Object,
            default: function () {
                return {
                    main: true,
                    stats: true,
                    edit: true,
                    delete: true,
                    view: false
                }
            }
        },
        customButton: {
            /**
             * Example:
             * { icon: 'fa-envelope', text: 'Property Enquiry', action: 'enquiry' }
             */
            type: Object,
            default: null
        },
        customStatusNames: {
            /**
             * Example:
             * { CONSTS.STATUS.PUBLISHED: 'Live' }
             */
            type: Object,
            default: null
        },
        message: {
            type: String,
            default: ''
        },
        dateLabel: {
            type: String,
            default: 'Date added'
        }
    },
    computed: {
        chartData() {
            return [
                this.item.enquiries,
                this.item.views
            ]
        },
        priceRentUnit() {
            return CONSTS.PROPERTY_PRICE_RENT_PER_NAMES_UNIT[this.item.price_rent_per];
        },
        priceSaleUnit() {
            return CONSTS.PROPERTY_PRICE_SALE_PER_NAMES_UNIT[this.item.price_sale_per];
        },
        dateAdded() {
            return new DateTime(this.item.created).format('dS mmmm yyyy');
        },
        imageStyle() {
            let image = '',
                images = this.item.property_media.filter((item) => {
                    return item.type === CONSTS.MEDIA_TYPE.IMAGE;
                });

            if (images.length) {
                images.sort((a, b) => (a.position > b.position) ? 1 : ((b.position > a.position) ? -1 : 0));
                image = images[0];
            }
            if (image) {
                return {
                    backgroundImage: `url(${image.source_path})`
                };
            } else {
                return {
                    backgroundSize: '50%',
                    backgroundImage: `url(${CONSTS.IMAGE_PLACEHOLDER})`
                }
            }
        },
        statusText() {
            Object.entries(this.statusCls).forEach((cls) => this.statusCls[cls[0]] = false);
            Object.entries(this.button.iconCls).forEach((cls) => this.button.iconCls[cls[0]] = false);
            let text = '';
            switch (this.item.status) {
                case CONSTS.STATUS.DRAFT:
                    text = CONSTS.STATUS.DRAFT;
                    this.statusCls['status-draft'] = true;
                    this.button.iconCls['fa-clipboard-check'] = true;
                    this.button.text = 'Post Ad';
                    this.action$ = 'post';
                    break;
                case CONSTS.STATUS.PUBLISHED:
                    text = CONSTS.STATUS.PUBLISHED;
                    this.statusCls['status-published'] = true;
                    this.button.iconCls['fa-eye'] = true;
                    this.button.text = 'View Ad';
                    this.action$ = 'view';
                    if (this.item.state === CONSTS.STATE.PENDING) {
                        this.buttons.main = false;
                        text = 'Awaiting approval';
                    } else if (this.item.state === CONSTS.STATE.REJECTED) {
                        this.buttons.main = false;
                        this.statusCls['status-published'] = false;
                        this.statusCls['status-expired'] = true;
                        text = CONSTS.STATE.REJECTED;
                        if (_.empty(this.message) && !_.empty(this.item.rejection_reason)) {
                            this.message = this.item.rejection_reason;
                        }
                    }
                    break;
                case CONSTS.STATUS.EXPIRED:
                    text = CONSTS.STATUS.EXPIRED;
                    this.statusCls['status-expired'] = true;
                    this.button.iconCls['fa-redo-alt'] = true;
                    this.button.text = 'Re-list Ad';
                    this.action$ = 're-list';
                    break;
            }

            if (this.customButton !== null) {
                Object.entries(this.button.iconCls).forEach((cls) => this.button.iconCls[cls[0]] = false);
                this.button.iconCls[this.customButton.icon] = true;
                this.button.text = this.customButton.text;
                this.action$ = this.customButton.action;
            }

            if (this.customStatusNames && this.customStatusNames[text]) {
                text = this.customStatusNames[text];
            }

            return _.capitalize(text.toLowerCase());
        },
        croppedHeadline() {
            if (this.item.headline.length <= 260) {
                return this.item.headline;
            }
            let trimmedString = this.item.headline.substr(0, 260);
            trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(' ')));
            return trimmedString + '...';
        }
    },
    methods: {
        toPriceQualifierName(qualifier) {
            if (qualifier === CONSTS.PROPERTY_PRICE_QUALIFIER.DEFAULT) {
                return '';
            }
            return CONSTS.PROPERTY_PRICE_QUALIFIER_NAMES[qualifier];
        },
        onButtonClick(action) {
            if (action === 'action') {
                action = this.action$;
            }
            this.$emit('action', action, this.item);
        }
    },
    template: template.render()
}
