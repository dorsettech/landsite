export default {
    mixins: [
        VueChartJs.Doughnut
    ],
    props: {
        data: {
            type: Array,
            default() {
                return [1, 2];
            }
        }
    },
    mounted() {
        this.renderChart({
            datasets: [{
                data: this.data,
                backgroundColor: [
                    '#1ab395', '#55cdb4'
                ],
                borderWidth: 3,
            }],
            labels: [
                'Enquiries',
                'Views'
            ]
        }, {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                display: false
            },
            tooltips: {
                enabled: false,
                custom: function (tooltip) {
                    if (tooltip.body) {
                        let label = tooltip.body[0].lines[0].split(':');
                        this.$chartLabel.innerHTML = `<strong>${label[0]}</strong><br/>${label[1]}`;
                    }
                }.bind(this)
            }
        });

        // create dynamic label
        this.$chartLabel = document.createElement('span');
        this.$chartLabel.classList.add('chart-label');
        this.$el.appendChild(this.$chartLabel);
        this.$chartLabel.innerHTML = `<strong>Enquiries</strong><br/>${this.data[0]}`;
    }
}
