import template from '../views/preferred-insights.html';
import LatestBusinessesAdded from './latest-businesses-added';
import CategoryImageCheckbox from './category-image-checkbox';
import CategoryListItem from './category-list-item';
import _ from "../utils/utilities";

export default {
    components: {
        CategoryImageCheckbox,
        CategoryListItem,
        LatestBusinessesAdded
    },
    data() {
        return {
            profile: {
                details: {}
            },
            items: [],
            fields: {
                // category Ids
                recommended: [],
                // category Objects
                selected: []
            },
            autoCompleteStyles: {
                vueSimpleSuggest: 'position-relative',
                inputWrapper: '',
                defaultInput: 'form-control',
                suggestions: 'position-absolute list-group z-1000',
                suggestItem: 'list-group-item'
            },
            hasError: false,
            message: '',
            isWorking: false
        };
    },
    computed: {
        recommended() {
            return this.items.filter((item) => {
                return item.is_recommended;
            });
        },
        categoriesRecommended() {
            return this.fields.recommended;
        },
        categoriesSelected() {
            return this.fields.selected;
        }
    },
    props: {
        user: Object
    },
    watch: {
        categoriesRecommended() {
            this.onCategoriesChange()
        },
        categoriesSelected() {
            this.onCategoriesChange()
        }
    },
    methods: {
        onCategorySelect(category) {
            if (category === null) {
                return;
            }
            (() => {
                this.$refs.CategoryAutoComplete.autocompleteText('');
            }).defer(this)
            if (this.fields.selected.includes(category)) {
                return
            }
            this.fields.selected.push(category);
        },
        onCategoriesChange() {
            if (!this.fields.selected.length && !this.fields.recommended.length) {
                this.hasError = true;
                this.message = 'At least one category of your interest is required';
                return false;
            }
            this.message = '';
            this.hasError = false;
        },
        loadUserPrefs() {
            if (this.profile.details.pref_insights_list.length === 0) {
                return;
            }
            let recommendedIds = this.recommended.map((item) => {
                return parseInt(item.id);
            });

            $$('.recommended-checkbox input', this.$el).forEach(input => input.checked = false);

            this.profile.details.pref_insights_list.forEach((id) => {
                if (recommendedIds.indexOf(parseInt(id)) !== -1) {
                    this.fields.recommended.push(id);
                    $$(`.recommended-checkbox input[value="${id}"]`, this.$el).pop().checked = true;
                } else {
                    let item = this.items.find((item) => {
                        return item.id === parseInt(id);
                    });
                    if (item) {
                        this.fields.selected.push(item);
                    }
                }
            });
        },
        onSubmit() {
            this.profile.details.pref_insights_list = [...new Set(this.fields.recommended.map(Number).concat(this.fields.selected.map((item) => {
                return item.id;
            })))];

            this.isWorking = true;
            this.$api.post('/User/update', this.profile).then(function (response) {
                this.hasError = !response.data.success;
                this.message = response.data.msg;
                if (this.hasError) {
                    this.$snotify.error(response.data.msg);
                } else {
                    this.$snotify.success(response.data.msg);
                }
                if (response.data.success) {
                    this.$bus.$emit('dashboard-refresh');
                }
                (() => this.message = '').delay(3000);
            }.bind(this)).catch(function (error) {
                this.hasError = true;
                this.message = error.message;
            }.bind(this)).then(function () {
                this.isWorking = false;
            }.bind(this));
        }
    },
    template: template.render(),
    /**
     * Prevent mutations
     */
    created() {
        this.profile = _.deepCopy(this.user);
        this.$api.get('/Categories').then((response) => this.items = response.data.records).then(() => this.loadUserPrefs());
    }
}
