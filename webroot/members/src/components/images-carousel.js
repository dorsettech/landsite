export default {
    data() {
        return {
            slides: [],
            active: 0
        };
    },
    props: {
        images: Array,
        counter: {
            type: Boolean,
            default: false
        }
    },
    watch: {
        images() {
            this.slides = this.images ? this.images.slice(0) : []
        }
    },
    methods: {
        next() {
            const first = this.slides.shift()
            this.slides = this.slides.concat(first)
        },
        prev() {
            const last = this.slides.pop()
            this.slides = [last].concat(this.slides)
        },
        getSlideStyle(slide) {
            return {
                backgroundImage: `url(${slide.src})`
            }
        }
    },
    template: `
        <div class='v-comp-images-carousel'>
            <span class="counter" v-if="counter"><i class="fas fa-camera" aria-hidden="true"/> {{slides.length}}</span>
            <transition-group class="carousel" tag="div" name="slide">
                <div v-for="(slide, index) in slides" class="slide" :key="slide.id" :style="getSlideStyle(slide)"></div>
            </transition-group>
            <div class="carousel-controls" v-if="slides.length > 1">
                <a class="carousel-control prev" @click="prev">
                    <i class="fas fa-chevron-left"></i>
                </a>
                <a class="carousel-control next" @click="next">
                    <i class="fas fa-chevron-right"></i>
                </a>
            </div>
        </div>
    `
}
