import template from '../views/category-list-item.html';

export default {
    data() {
        return {

        };
    },
    props: {
        item: Object,
        value: Array,
        name: String
    },
    methods: {
        onDeleteClick(event) {
            let val = this.item,
                // prevent directly mutations
                value = [].concat(this.value);
            if (value.includes(val)) {
                value.splice(value.indexOf(val), 1)
            } else {
                value.push(val)
            }
            this.$emit('input', value);
        }
    },
    template: template.render()
}
