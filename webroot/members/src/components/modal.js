export default {
    template: `
        <transition name="modal">
            <div class="v-comp-modal">
              <div class="modal-wrapper">
                <div class="modal-container">
                  <div class="modal-header">
                    <slot name="header">Default header</slot>
                  </div>
                  <div class="modal-body">
                    <slot name="body">Default body</slot>
                  </div>
                  <div class="modal-footer">
                    <slot name="footer">
                      <button type="button" class="btn btn-secondary btn-large pull-right" @click="$emit('close')">Close</button>
                    </slot>
                  </div>
                </div>
              </div>
            </div>
        </transition>`
}
