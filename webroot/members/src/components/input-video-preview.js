import utils from '../utils/utilities';

const YOUTUBE_EMBED_URL = 'https://www.youtube.com/embed/{0}?controls=0&autoplay=0&modestbranding=1&showinfo=0';

export default {

    data() {
        return {
            val: '',
            url: ''
        };
    },
    props: {
        value: String,
        id: String,
        label: String,
        tip: String,
        preventMutationDelay: {
            type: Number,
            default: 100
        }
    },
    computed: {},
    methods: {
        getYouTubeId(url) {
            const match = url.match(/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/);
            return (match && match[7].length == 11) ? match[7] : '';
        },
        onInputChange() {
            let youTubeId = this.getYouTubeId(this.val);
            this.url = youTubeId ? utils.format(YOUTUBE_EMBED_URL, youTubeId) : '';
            this.$emit('input', youTubeId);
        }
    },
    /**
     * Prevent mutations
     */
    created() {
        (function () {
            this.val = !this.value ? '' : String(this.value).toString();
            this.onInputChange();
        }).delay(this, this.preventMutationDelay);
    },
    template: `
        <div class="form-row v-input-video-preview">
            <div class="col-md-5">
                <label :for="id">{{label}}</label>
                <input type="text" @change="onInputChange" class="form-control" v-model="val">
                <p>{{tip}}</p>
            </div>
            <div class="col-md-7">
                <div class="embed-responsive embed-responsive-16by9" v-show="url">
                    <iframe class="embed-responsive-item" :src="url" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"></iframe>
                </div>
                <img src="/members/assets/video-placeholder.jpg" class="img-fluid" v-show="url === ''"/>
            </div>
        </div>`
}
