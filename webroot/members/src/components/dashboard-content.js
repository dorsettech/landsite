import template from '../views/dashboard-content.html';

export default {
    props: {
        user: Object,
        settings: Object
    },
    template: template.render()
}
