import template from '../views/recent-insights.html';
import Loader from '../components/loader';
import Pagination from '../components/pagination';
import ArticleBoxItem from '../components/articles/article-box-item';
import C from './../config/constants';

export default {
    components: {
        Loader,
        Pagination,
        ArticleBoxItem
    },
    data() {
        return {
            items: [],
            loading: false,
            page$: 1,
            pagination: {}
        };
    },
    props: {
        user: Object,
        settings: Object
    },
    watch: {
        '$route': function (to, from) {
            if (to.name === 'RecentInsights') {
                if (to.query.reload) {
                    this.reset();
                } else if (to.query.refresh) {
                    this.refresh();
                }
            }
        }
    },
    methods: {
        load(page = 1, perPage = 8) {
            this.loading = true;
            this.page$ = page;
            this.$api.get('/Articles/recent', {
                params: {
                    type: C.ARTICLE_TYPE.INSIGHT,
                    page: page,
                    per_page: perPage
                }
            }).then((response) => {
                this.items = response.data.records;
                this.pagination = response.data.pagination;
            }).then(() => this.loading = false);
        },
        refresh() {
            this.load(this.page$);
        },
        reset() {
            this.load();
        },
        onPageChange(page) {
            this.load(page);
        }
    },
    template: template.render(),
    created() {
        this.load();
    }
}
