export default {
    props: {
        title: String,
        description: String,
        iconCls: String
    },
    template: `
        <div class="col-sm-12 col-md-6 image-box" :class="iconCls">
            <div class="panel">
                <div class="wrapper">
                    <div class="row m-0 h-100">
                        <div class="col-auto icon-wrapper">
                            <i class="fas fa-search"/>
                        </div>
                        <div class="col content-wrapper">
                            <h4 class="title">{{title}}</h4>
                            <p class="description">{{description}}</p>
                            <a class="btn btn-large btn-secondary" @click.prevent="$emit('click')">
                                <i class="fas fa-search"></i> Search now
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `
}
