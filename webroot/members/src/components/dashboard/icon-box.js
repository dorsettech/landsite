export default {
    props: {
        icon: {
            type: String,
            default: 'fa-archive'
        },
        title: String,
        description: String,
        button: {
            type: Object,
            default() {
                return {
                    title: 'Add button',
                    icon: 'fa-plus-circle',
                    label: 'Add now'
                };
            }
        },
        extraButtonIcon: Boolean
    },
    template: `
        <div class="col-sm-12 col-md-6 icon-box">
            <div class="panel">
                <div class="row m-0">
                    <div class="col-auto icon-wrapper">
                        <i class="fas icon position-relative" :class="icon">
                            <i class="fas icon-tr" :class="button.icon" v-if="extraButtonIcon"></i>
                        </i>
                    </div>
                    <div class="col content-wrapper">
                        <h4 class="title">{{title}}</h4>
                        <p class="description">{{description}}</p>
                        <a class="btn btn-large btn-secondary" :title="button.title" @click.prevent="$emit('click')">
                            <i class="fas" :class="button.icon"></i> {{button.label}}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    `
}
