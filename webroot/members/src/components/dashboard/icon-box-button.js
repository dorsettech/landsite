export default {
    props: {
        icon: {
            type: String,
            default: 'fa-archive'
        },
        title: String,
        button: {
            type: Object,
            default() {
                return {
                    title: 'Add button',
                    icon: 'fa-plus-circle',
                    label: 'Add now'
                };
            }
        },
    },
    template: `
        <div class="col-sm-6 col-md-3 icon-box button-box">
            <div class="panel">
                <div class="row m-0">
                    <div class="col-auto icon-wrapper">
                        <i class="fas icon position-relative" :class="icon"></i>
                    </div>
                    <div class="col content-wrapper">
                        <h4 class="title">{{title}}</h4>
                        <a class="btn btn-large btn-secondary" :title="button.title" @click.prevent="$emit('click')">
                            <i class="fas" :class="button.icon"></i> {{button.label}}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    `
}
