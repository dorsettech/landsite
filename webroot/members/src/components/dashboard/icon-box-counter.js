export default {
    components: {
        AnimatedNumber
    },
    props: {
        icon: {
            type: String,
            default: 'fa-archive'
        },
        title: String,
        counter: {
            type: Number,
            default: 0
        },
        link: String
    },
    template: `
        <div class="col-sm-6 col-md-3 icon-box counter-box">
            <div class="panel">
                <div class="row m-0">
                    <div class="col-auto icon-wrapper">
                        <i class="fas icon position-relative" :class="icon"></i>
                    </div>
                    <div class="col content-wrapper">
                        <h4 class="title">{{title}}</h4>
                        <p class="counter">
                            <animated-number :value="counter" :formatValue="Math.ceil"></animated-number>
                        </p>
                        <a class="link" :title="link" @click.prevent="$emit('click')" v-if="link">
                            {{link}}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    `
}
