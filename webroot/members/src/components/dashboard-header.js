import consts from '../config/constants';
import template from '../views/dashboard-header.html';

export default {
    data() {
        return {
            websiteUrl: `//${consts.URL.WWW}`,
            membersUrl: `//${consts.URL.MEMBERS}`,
            servicesUrl: `//${consts.URL.WWW_SERVICES}`,
            propertiesUrl: `//${consts.URL.WWW_PROPERTIES}`
        };
    },
    props: {},
    methods: {
        onLinkClick() {
            this.$root.loading = true;
        }
    },
    computed: {
        currentUrl() {
            return `#${this.$route.path}`;
        }
    },
    template: template.render()
}
