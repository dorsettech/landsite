import template from '../views/loader.html';

export default {
    props: {
        active: Boolean
    },
    data() {
        return {
            isActive: this.active
        }
    },
    watch: {
        active(value) {
            this.isActive = value
        }
    },
    template: template.render()
}
