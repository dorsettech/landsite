import template from '../views/add-property.html';
import LatestBusinessesAdded from "./latest-businesses-added";

export default {
    components: {
        LatestBusinessesAdded
    },
    methods: {
        add(type) {
            this.$router.push({
                path: `/property`,
                query: {
                    purpose: type
                }
            });
        }
    },
    template: template.render()
}
