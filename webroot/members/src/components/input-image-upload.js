import consts from '../config/constants';
import Utils from '../utils/utilities';

const
    EVENT_SUCCESS = 'success',
    EVENT_FAIL = 'fail';

export default {

    data() {
        return {
            val: '',
            isUploading: false,
            isEmpty: true
        };
    },
    props: {
        label: String,
        id: String,
        name: String,
        uploadUrl: {
            type: String,
            default: '/User/image'
        },
        imageUrl: String,
        value: String,
        defaultImagePath: {
            type: String,
            default: ''
        }
    },
    computed: {
        styles() {
            if (Utils.empty(this.val) || this.val === 'null') {
                this.isEmpty = true;
                if (this.defaultImagePath !== '') {
                    return {
                        backgroundImage: `url(${this.defaultImagePath})`
                    }
                }
                return {};
            }
            this.isEmpty = false;
            return {
                backgroundImage: `url(${this.imageUrl}/${this.val})`
            }
        }
    },
    methods: {
        onFileChange(e) {
            let name = e.target.name,
                files = e.target.files,
                form = new FormData();

            if (!files.length) return;

            Array.from(Array(files.length).keys()).map(i => {
                form.append(name, files[i], files[i].name);
            });

            this.isUploading = true;

            this.upload(form).then(response => {
                if (response.success) {
                    this.val = response[name];
                    this.$emit('input', response[name]);
                    this.$emit(EVENT_SUCCESS, response.msg, response[name]);
                } else {
                    this.$emit(EVENT_FAIL, response.msg);
                }
            }).catch(error => {
                this.$emit(EVENT_FAIL, error);
            }).then(() => this.isUploading = false);
        },
        upload(form) {
            return this.$api.post(this.uploadUrl, form).then(response => response.data);
        },
        reset() {
            this.val = '';
            this.$emit('input', '');
        }
    },
    watch: {
        /**
         * If component keep own state it has to listen changes on model (keep alive)
         */
        value() {
            this.val = !this.value ? '' : String(this.value).toString();
        }
    },
    /**
     * Prevent mutations
     */
    created() {
        this.val = !this.value ? '' : String(this.value).toString()
    },
    template: `
        <div class="v-input-image-upload">
            <label :for="id">{{label}}</label>
            <div class="input-group">
                <div class="drop-box" :class="{disabled: isUploading, 'not-empty': !isEmpty}" :style="styles">
                    <input type="file" :name="name" :disabled="isUploading" @change="onFileChange" accept=".jpg,.jpeg,.png,.webp" class="input-file">
                    <p v-if="isEmpty">
                        Drag your file here to begin<br> or click to browse
                    </p>
                    <transition name="fade">
                        <p v-if="isUploading" class="uploading">
                            <i class="fas fa-cog fa-spin"></i>
                        </p>
                    </transition>
                </div>
            </div>
        </div>`
}
