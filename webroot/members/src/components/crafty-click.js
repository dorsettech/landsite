import Utils from '../utils/utilities';

const
    EVENT_SUCCESS = 'success',
    EVENT_FAIL = 'fail',
    MESSAGES = {
        postcodeInvalid: 'Please recheck your postcode, it seems to be incorrect',
        postcodeNotFound: 'Your postcode could not be found. Please type in your address',
        connectionFailed: 'Unable to connect to craftyclicks.co.uk server',
        wereFound: '{0} addresses were found'
    };

export default {

    data() {
        return {
            val: '',
            isWorking$: false,
            endpoint$: '//pcls1.craftyclicks.co.uk/json/rapidaddress',
            isLookup$: false,
            data$: []
        };
    },
    props: {
        apiKey: String,
        id: String,
        name: String,
        label: String,
        buttonLabel: {
            type: String,
            default: 'Check address'
        },
        value: String,
        useCache: {
            type: Boolean,
            default: false
        },
        required: {
            type: Boolean,
            default: false
        },
        feedbackMsg: {
            type: String,
            default: 'This field is required'
        }
    },
    methods: {
        postcodeToKey(postcode) {
            return 'CraftyClicksRapidAddress.' + postcode.replace(/\s+/g, '').toUpperCase().trim();
        },
        onValueChange(value) {
            this.$emit('input', value);
        },
        onInputChange(e) {
            let input = e.target;
            input.checkValidity();
            if (input.validity.valid) {
                e.target.classList.remove('is-invalid');
            }
        },
        onInputInvalid(e) {
            e.target.classList.add('is-invalid');
        },
        onCheckAddress() {
            if (this.isLookup$) {
                return false;
            }

            let postcode = this.value;

            if (Utils.empty(postcode)) {
                this.$emit(EVENT_FAIL, MESSAGES.postcodeInvalid);
                return false;
            }

            this.isLookup$ = true;

            if (this.useCache && localStorage.getItem(this.postcodeToKey(postcode))) {
                this.data$ = JSON.parse(localStorage.getItem(this.postcodeToKey(postcode)));
                if (this.data$.length) {
                    this.$emit(EVENT_SUCCESS, Utils.format(MESSAGES.wereFound, this.data$.length), this.data$);
                } else {
                    this.$emit(EVENT_SUCCESS, MESSAGES.postcodeNotFound, this.data$);
                }
                this.isLookup$ = false;
                return true;
            }

            this.isWorking = true;

            axios.get(this.endpoint$, {
                params: {
                    'key': this.apiKey,
                    'postcode': postcode,
                    'response': 'data_formatted'
                }
            }).then(function (response) {
                if (response.data.error_msg) {
                    this.$emit(EVENT_FAIL, response.data.error_msg)
                    this.data$ = [];
                    return;
                }

                for (let i = 0; i < response.data.delivery_points.length; i += 1) {

                    this.data$.push(Object.assign({
                        id: i + 1,
                        text: [
                            response.data.delivery_points[i].organisation_name,
                            response.data.delivery_points[i].dps,
                            response.data.delivery_points[i].line_1,
                            response.data.delivery_points[i].line_2,
                            response.data.town
                        ].filter(function (v) {
                            return v !== '';
                        }).join(', ')
                    }, response.data.delivery_points[i], {
                        postcode: response.data.postcode,
                        town: response.data.town,
                        postal_county: response.data.postal_county,
                        traditional_county: response.data.traditional_county
                    }));
                }

                if (this.useCache) {
                    localStorage.setItem(this.postcodeToKey(postcode), JSON.stringify(this.data$));
                }

                if (this.data$.length) {
                    this.$emit(EVENT_SUCCESS, Utils.format(MESSAGES.wereFound, this.data$.length), this.data$);
                } else {
                    this.$emit(EVENT_SUCCESS, Utils.format(MESSAGES.postcodeNotFound), this.data$);
                }
            }.bind(this)).catch(function (error) {
                this.$emit(EVENT_FAIL, MESSAGES.connectionFailed);
            }.bind(this)).then(function () {
                this.isLookup$ = false;
                this.isWorking = false;
            }.bind(this));
        }
    },
    /**
     * Prevent mutations
     */
    created() {
        this.val = !this.value ? '' : String(this.value).toString()
    },
    template: `
        <div class="v-comp-crafty-click">
            <label :for="id">{{label}}</label>
            <div class="input-group">
                <input class="form-control" type="text" :id="id" :name="name" :value="val" @input="onValueChange($event.target.value)" :required="required" @change="onInputChange" @invalid.prevent="onInputInvalid">
                <div class="input-group-append">
                    <button type="button" class="btn action" @click.prevent="onCheckAddress" :aria-disabled="isWorking$" :disabled="isWorking$">
                    <i class="fas fa-cog fa-spin" aria-hidden="true" v-show="isWorking$"></i>
                    {{buttonLabel}}
                    </button>
                </div>
                <div class="invalid-feedback">{{feedbackMsg}}</div>
            </div>
        </div>`
}
