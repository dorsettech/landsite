import template from '../views/contact-list-item.html';

export default {
    data() {
        return {};
    },
    props: {
        item: Object,
        value: Array,
        required: {
            type: Boolean,
            default: false
        },
        cls: {
            type: [Array, String],
            default: ''
        },
        action: {
            type: String,
            default: '',
            validator: (val) => ['', 'add', 'remove'].includes(val)
        },
        labels: {
            type: Object,
            default: {
                name: 'Person Name',
                email: 'Email'
            }
        }
    },
    template: template.render()
}
