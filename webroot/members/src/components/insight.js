import template from '../views/insight.html';
import Article from '../mixins/article';
import CONSTS from "../config/constants";

export default {
    mixins: [
        Article
    ],
    data() {
        return {
            componentMeta: Object.freeze({
                model: {
                    title: '',
                    slug: '',
                    headline: '',
                    body: '',
                    image: null,
                    type: CONSTS.ARTICLE_TYPE.INSIGHT,
                    status: CONSTS.STATUS.DRAFT,
                    state: null
                },
                viewAddName: 'InsightAdd',
                viewEditName: 'InsightEdit',
                formName: 'insight-form',
                parentPath: '/insights'
            }),
            categories: [] // only used when contribute = true
        };
    },
    props: {
        contribute: {
            type: Boolean,
            default: false
        }
    },
    template: template.render(),
    created() {
        this.$api.get('/Categories').then(response => {
            this.categories = response.data.records;
        });
    }
}
