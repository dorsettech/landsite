import _ from '../utils/utilities';

const
    EVENT_SUCCESS = 'success',
    EVENT_FAIL = 'fail',
    EVENT_SORT = 'change-order';

export default {
    data() {
        return {
            val: [],
            isUploading: false,
            isEmpty: true,
            hasError: false,
            message: ''
        };
    },
    props: {
        label: String,
        name: String,
        uploadUrl: {
            type: String,
            default: '/Properties/files'
        },
        fileUrl: String,
        value: Array,
        sorting: {
            type: Boolean,
            default: false
        },
        accept: {
            type: String,
            default: '.pdf,.docx,.doc'
        }
    },
    methods: {
        reset() {
            this.hasError = false;
            this.message = '';
        },
        remove(src) {
            this.val = this.value.filter((item) => {
                return item.source !== src;
            });
            this.$emit('input', this.val);
        },
        onFileChange(e) {
            let name = e.target.name,
                files = e.target.files,
                form = new FormData();

            if (!files.length) return;

            Array.from(Array(files.length).keys()).map(i => {
                form.append(i, files[i], files[i].name);
            });

            this.isUploading = true;

            this.upload(form).then(response => {
                if (response.success) {
                    this.hasError = false;
                    this.message = response.msg;
                    this.$emit(EVENT_SUCCESS, response.msg, response.results);
                } else {
                    this.hasError = true;
                    this.message = response.msg;
                    this.$emit(EVENT_FAIL, response.msg, response.results);
                }
            }).catch(error => {
                this.$emit(EVENT_FAIL, error);
            }).then(() => this.isUploading = false);
        },
        upload(form) {
            return this.$api.post(this.uploadUrl, form).then(response => response.data);
        },
        sort(file, dir) {
            if (!this.sorting) {
                return;
            }
            this.$emit(EVENT_SORT, file, dir);
        }
    },
    /**
     * Prevent mutations
     */
    created() {
        (function () {
            this.val = !this.value ? [] : _.shallowCopy(this.value);
        }).delay(this, 1000);
    },
    template: `
        <div class="v-input-documents-upload">
            <label>{{label}}</label>            
            <div class="row m-b-20">
                <div class="col-sm-12 col-md-auto">
                    <div class="drop-zone" :class="{disabled: isUploading}">
                        <input type="file" :name="name" :disabled="isUploading" @change="onFileChange" :accept="accept" class="input-file" multiple>
                        <transition name="fade">
                            <p v-if="!isUploading" class="info">
                                Drop files here or click to upload
                            </p>
                            <p v-if="isUploading" class="uploading">
                                <i class="fas fa-cog fa-spin"></i>
                            </p>
                        </transition>
                    </div>
                </div>
                <div class="col">
                    <ul class="files">
                        <li v-for="file in value">
                            <span>{{file.extra}}</span>
                            <a @click="sort(file, 'up')" class="sort-down" title="Down" v-if="sorting">
                                <i class="fas fa-chevron-circle-down"/>
                            </a>
                            <a @click="sort(file, 'down')" class="sort-up" title="Up" v-if="sorting">
                                <i class="fas fa-chevron-circle-up"/>
                            </a> 
                            <a @click="remove(file.source)" class="delete">
                                <i class="fas fa-minus-circle"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row m-b-20" v-if="message !== ''">
                <transition name="fade">
                    <div class="alert d-inline p-b-0" :class="{'text-danger': hasError, 'text-success': !hasError }" role="alert" v-show="message !== ''">{{message}}</div>
                </transition>                
            </div>            
        </div>`
}
