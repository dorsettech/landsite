import template from '../views/business-stats.html';
import ImagesCarousel from '../components/images-carousel';
import ServiceStatsChart from '../components/business-stats/service-stats-chart';
import ServiceEnquiries from '../components/business-listing/business-enquiries';
import Loader from './../components/loader';
import _ from "../utils/utilities";
import C from "../config/constants";
import DateTime from "../utils/date";

export default {
    components: {
        ImagesCarousel,
        ServiceStatsChart,
        ServiceEnquiries,
        Loader
    },
    data() {
        return {
            loading: false,
            business: {
                service: {}
            },
            category: '',
            stats: [],
            statusCls: {
                'status-draft': false,
                'status-expired': false,
                'status-published': false
            },
            counter: {
                days: 0,
                hours: 0,
                minutes: 0,
                seconds: 0
            },
            counterMsg: '',
            interval$: null,
            rotateRequired: false,
            message: ''
        }
    },
    computed: {
        images() {
            this.business.service.service_media = this.business.service.service_media || [];
            return this.business.service.service_media.filter(item => item.type === C.MEDIA_TYPE.IMAGE).map(image => {
                return {
                    id: image.id,
                    src: image.source_path
                }
            });
        },
        statusText() {
            Object.entries(this.statusCls).forEach((cls) => this.statusCls[cls[0]] = false);
            let text = '';
            switch (this.business.service.status) {
                case C.STATUS.DRAFT:
                    text = C.STATUS.DRAFT;
                    this.statusCls['status-draft'] = true;
                    break;
                case C.STATUS.PUBLISHED:
                    text = C.STATUS.PUBLISHED;
                    this.statusCls['status-published'] = true;

                    if (this.business.service.state === C.STATE.PENDING) {
                        text = 'Awaiting approval';
                    } else if (this.business.service.state === C.STATE.REJECTED) {
                        this.statusCls['status-published'] = false;
                        this.statusCls['status-expired'] = true;
                        text = C.STATE.REJECTED;
                        if (_.empty(this.message) && !_.empty(this.business.service.rejection_reason)) {
                            this.message = this.business.service.rejection_reason;
                        }
                    }
                    break;
                case C.STATUS.EXPIRED:
                    text = C.STATUS.EXPIRED;
                    this.statusCls['status-expired'] = true;
                    break;
            }
            return _.capitalize(text.toLowerCase());
        },
        isChartAvailable() {
            if (!Array.isArray(this.stats) || this.stats.length === 0) {
                return false;
            }
            return this.stats.reduce((prev, curr) => {
                return prev + curr.views + curr.enquiries + curr.clicks;
            }, 0) > 0;
        },
        isCounterVisible() {
            return !_.empty(this.business.service.ad_expiry_date) && new Date(this.business.service.ad_expiry_date) > new Date();
        }
    },
    props: {
        settings: Object,
        user: Object,
        record: Object
    },
    methods: {
        load() {
            this.loading = true;
            this.$api.post('/Services/business').then(response => {
                this.business = response.data;
                this.loading = false;
            });
        },
        onViewClick() {
            this.$root.loading = true;
            location.href = '//' + _.format(C.URL.WWW_SERVICE, `${this.business.service.id}_${this.business.service.slug}`);
        },
        onEditClick() {
            this.$router.push({
                path: '/business-listing'
            });
        },
        startCounter() {
            if (!this.isCounterVisible) {
                return;
            }
            this.interval$ = setInterval(() => {
                const ms = Math.abs(new Date(this.business.service.ad_expiry_date) - new Date());
                this.counter = DateTime.timeComponents(ms, true);
                let v = 0, t = '';
                if (this.counter.days > 0) {
                    v = this.counter.days;
                    t = this.counter.days > 1 ? 'days' : 'day';
                } else if (this.counter.hours > 0) {
                    v = this.counter.hours;
                    t = this.counter.hours > 1 ? 'hours' : 'hour';
                } else if (this.counter.minutes > 0) {
                    v = this.counter.minutes;
                    t = this.counter.minutes > 1 ? 'minutes' : 'minute';
                } else if (this.counter.seconds > 0) {
                    v = this.counter.seconds;
                    t = this.counter.seconds > 1 ? 'seconds' : 'second';
                } else {
                    v = 0;
                    t = '';
                    this.business.service.status = C.STATUS.EXPIRED;
                    this.stopCounter();
                }
                this.counterMsg = v === 0 ? C.SERVICE_EXPIRED : _.format(C.SERVICE_EXPIRE_IN, parseInt(v), t);
            }, 1000)
        },
        stopCounter() {
            clearInterval(this.interval$);
        },
        onChartRotateRequired(value) {
            this.rotateRequired = value;
        }
    },
    watch: {
        business() {
            if (this.$refs.Enquiries) this.$refs.Enquiries.refresh();
            this.category = this.business.service.category_id ? this.business.categories.find(cat => { return cat.id === this.business.service.category_id }).name : '';
            this.$api('/Services/stats').then(response => response.data).then(data => {
                this.stats = data;
            });
            this.startCounter();
        },
        '$route': function (to, from) {
            if (to.name === 'BusinessStats') {
                if (to.query.refresh) {
                    this.load();
                }
            }
        }
    },
    template: template.render(),
    beforeRouteLeave: function (to, from, next) {
        this.stopCounter();
        /*
         * Make sure to always call the next function, otherwise the hook will never be resolved
         * @see https://router.vuejs.org/en/advanced/navigation-guards.html
         */
        next();
    },
    created() {
        this.load();
    }
}
