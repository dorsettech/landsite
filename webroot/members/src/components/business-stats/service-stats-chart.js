import ViewportResize from './../../mixins/viewport-resize';

export default {
    mixins: [
        VueChartJs.Line,
        ViewportResize
    ],
    props: {
        chartData: Array
    },
    watch: {
        chartData() {
            this.render()
        }
    },
    methods: {
        getDataSets() {
            return {
                datasets: [{
                    label: 'Views',
                    backgroundColor: 'rgba(160,160,160,.2)',
                    borderWidth: 3,
                    data: this.chartData.map(item => {
                        return item.views
                    })
                }, {
                    label: 'Enquiries',
                    backgroundColor: 'rgba(26,179,149,.5)',
                    borderWidth: 3,
                    data: this.chartData.map(item => {
                        return item.enquiries
                    })
                }, {
                    label: 'Clicks (website)',
                    backgroundColor: 'rgba(255,216,0,.5)',
                    borderWidth: 3,
                    data: this.chartData.map(item => {
                        return item.clicks
                    })
                }, {
                    label: 'Clicks (phone)',
                    backgroundColor: 'rgba(255,28,31,0.5)',
                    borderWidth: 3,
                    data: this.chartData.map(item => {
                        return item.calls
                    })
                }],
                labels: this.chartData.map(item => {
                    return item.label
                })
            }
        },
        render() {
            this.renderChart(this.getDataSets(), {
                responsive: true,
                maintainAspectRatio: false
            });
        },
        onViewportResize() {
            this.render();
            this.$nextTick(() => {
                this.$emit('rotate-required', window.innerWidth < 550);
            });
        }
    },
    mounted() {
        this.onViewportResize();
    }
}
