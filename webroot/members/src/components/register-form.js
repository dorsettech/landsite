import template from '../views/register-form.html';
import CategoryImageCheckbox from './category-image-checkbox';
import CategoryListItem from './category-list-item';

export default {
    components: {
        CategoryImageCheckbox,
        CategoryListItem
    },
    data() {
        return {
            step: 0,
            visibility: {
                step1: false,
                step2: false,
                step3: false
            },
            forms: {
                user: {
                    fields: {
                        first_name: {
                            value: '',
                            isInvalid: false
                        },
                        last_name: {
                            value: '',
                            isInvalid: false
                        },
                        company: {
                            value: '',
                            isInvalid: false
                        },
                        email: {
                            value: '',
                            isInvalid: false
                        },
                        password: {
                            value: '',
                            isInvalid: false
                        }
                    },
                    message: '',
                    isWorking: false
                },
                prefs: {
                    fields: {
                        location: 'all',
                        locationText: '',
                        buying: false,
                        selling: false,
                        professional: false,
                        insights: false
                    },
                    hasError: false
                },
                cats: {
                    items: [],
                    fields: {
                        // category Ids
                        recommended: [],
                        // category Objects
                        selected: []
                    },
                    autoCompleteStyles: {
                        vueSimpleSuggest: 'position-relative',
                        inputWrapper: '',
                        defaultInput: 'form-control',
                        suggestions: 'position-absolute list-group z-1000',
                        suggestItem: 'list-group-item'
                    },
                    hasError: false,
                    message: '',
                    isWorking: false,
                    isVisible: true
                }
            }
        }
    },
    computed: {
        recommended() {
            return this.forms.cats.items.filter((item) => {
                return item.is_recommended;
            });
        }
        /*,
        catsRecommended() {
            return this.forms.cats.fields.recommended;
        },
        catsSelected() {
            return this.forms.cats.fields.selected;
        }
        */
    },
    mounted: function () {
        let initialStep = 1;
        this.step = initialStep;
        this.visibility[`step${initialStep}`] = true;
        this.$api.get('/Categories').then((response) => this.forms.cats.items = response.data.records);
    },
    methods: {
        onCategorySelect(category) {
            if (category === null) {
                return;
            }
            (() => {
                this.$refs.CategoryAutoComplete.autocompleteText('');
            }).defer(this)
            if (this.forms.cats.fields.selected.includes(category)) {
                return
            }
            this.forms.cats.fields.selected.push(category);
        },
        reset() {
            this.step = 1;
            this.visibility.step3 = false;
            this.visibility.step2 = false;
            this.visibility.step1 = true;
        },
        next() {
            if (this.step === 3) {
                return;
            }
            this.step++;
        },
        onTransitionAfterLeave(step) {
            this.visibility[`step${step + 1}`] = true;
        },
        onChange(form, e) {
            let input = e.target,
                field = input.name,
                f = this.forms[form]['fields'][field];
            input.checkValidity();
            f.isInvalid = f.value === '' ? false : !input.validity.valid;
        },
        onInvalid(form, e) {
            let input = e.target,
                field = input.name,
                f = this.forms[form]['fields'][field];
            f.isInvalid = true;
        },
        onUserSubmit() {
            let form = this.forms.user;
            form.isWorking = true;
            this.$api.post('/User/validateEmail', {
                email: form.fields.email.value
            }).then(function (response) {
                form.hasError = !response.data.success;
                form.message = response.data.msg;
                if (response.data.success) {
                    window.dataLayer.push({
                        'event': 'VirtualPageview',
                        'virtualPageURL': '/register/step2',
                        'virtualPageTitle': 'Register - What are you interested in?'
                    });
                    this.next();
                }
            }.bind(this)).catch(function (error) {
                form.hasError = true;
                form.message = error.message;
            }.bind(this)).then(function () {
                form.isWorking = false;
            }.bind(this));
        },
        onLocationFocus() {
            this.forms.prefs.fields.location = 'select';
        },
        onLocationBlur() {
            this.forms.prefs.fields.location = this.forms.prefs.fields.locationText === '' ? 'all' : 'select';
        },
        onPrefChange() {
            let form = this.forms.prefs,
                f = form.fields;
            if (!f.buying && !f.selling && !f.professional && !f.insights) {
                form.hasError = true;
                return;
            }
            form.hasError = false;
        },
        onPrefsSubmit() {
            let form = this.forms.prefs;
            this.onPrefChange();
            if (form.hasError) {
                return false;
            }
            this.next();
            window.dataLayer.push({
                'event': 'VirtualPageview',
                'virtualPageURL': '/register/step3',
                'virtualPageTitle': 'Register - What news?'
            });
        },
        /*
        onCatsChange() {
            let form = this.forms.cats,
                f = form.fields;
            if (!f.selected.length && !f.recommended.length) {
                form.hasError = true;
                form.message = 'At least one category of your interest is required';
                return false;
            }
            form.message = '';
            form.hasError = false;
        },
        */
        onCatsSubmit() {
            // this.onCatsChange();

            let user = this.forms.user,
                prefs = this.forms.prefs,
                cats = this.forms.cats,
                data = {};

            if (user.hasError || prefs.hasError || cats.hasError) {
                return false;
            }

            for (const [key, obj] of Object.entries(user.fields)) data[key] = obj.value;
            Object.assign(data, prefs.fields, {
                insights_list: [...new Set(cats.fields.recommended.map(Number).concat(cats.fields.selected.map((item) => {
                    return item.id;
                })))]
            });

            cats.isWorking = true;
            this.$api.post('/User/register', data).then(function (response) {
                cats.hasError = !response.data.success;
                cats.message = response.data.msg;
                if (response.data.success) {
                    window.dataLayer.push({
                        'event': 'VirtualPageview',
                        'virtualPageURL': '/register/step4',
                        'virtualPageTitle': 'Register Success'
                    });
                    this.forms.cats.isVisible = false;
                    (() => {
                        this.$root.loading = true;
                        location.href = '/?r=' + Math.random() * 10e17;
                    }).delay(6000);
                }
            }.bind(this)).catch(function (error) {
                cats.hasError = true;
                cats.message = error.message;
            }.bind(this)).then(function () {
                if (cats.hasError) {
                    cats.isWorking = false;
                }
            }.bind(this));
        }
    },
    watch: {
        step(index) {
            this.visibility[`step${index - 1}`] = false;
        }
        /*,
        catsRecommended() {
            this.onCatsChange()
        },
        catsSelected() {
            this.onCatsChange()
        }
        */
    },
    template: template.render()
}
