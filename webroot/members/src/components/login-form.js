import template from '../views/login-form.html';

export default {
    data() {
        return {
            form: {
                email: {
                    value: '',
                    isInvalid: false
                },
                password: {
                    value: '',
                    isInvalid: false
                }
            },
            message: '',
            hasError: false,
            isWorking: false
        }
    },
    methods: {
        getRedirectTo() {
            const url = new URL(location.href);
            return url.searchParams.get('redirectTo');
        },
        onSubmit: function (e) {
            this.$root.loading = true;
            this.isWorking = true;
            let data = {};
            for (const [key, obj] of Object.entries(this.form)) data[key] = obj.value;
            this.$api.post('/User/login', data).then(function (response) {
                this.hasError = !response.data.success;
                this.message = response.data.msg;
                if (response.data.success) {
                    window.dataLayer.push({
                        'event':'VirtualPageview',
                        'virtualPageURL':'/login/dashboard',
                        'virtualPageTitle' : 'Member Login Successful'
                    });
                    let url = this.getRedirectTo();
                    if (url === null) {
                        url = location.protocol + '//' + location.host + '/?r=' + Math.random() * 10e17;
                    }
                    (() => location.href = url).delay(1000);
                } else {
                    this.isWorking = false;
                }
            }.bind(this)).catch(function (error) {
                this.hasError = true;
                this.message = error.message;
                this.isWorking = false;
            }.bind(this)).then(function () {
                this.$root.loading = false;
            }.bind(this));
        },
        onChange: function (field, e) {
            let input = e.target;
            input.checkValidity();
            this.form[field].isInvalid = this.form[field].value === '' ? false : !input.validity.valid;
        }
    },
    template: template.render()
}
