import Utils from '../utils/utilities';
import DateTime from '../utils/date';
import Loader from './../components/loader';
import Consts from '../config/constants';
import ArticlesDatagridToolbar from './../components/articles-datagrid-toolbar';
import StatusTooltip from './../components/articles/status-tooltip';

export default {
    components: {
        Loader,
        ArticlesDatagridToolbar,
        StatusTooltip
    },
    data() {
        return {
            loading: true,
            columns: [
                {
                    name: 'id',
                    title: 'ID',
                    sortField: 'id'
                }, {
                    name: 'title',
                    sortField: 'title'
                }, {
                    name: 'image',
                    sortField: 'image',
                    formatter: function (value) {
                        return Utils.empty(value) ?
                            'No image defined' :
                            `<img src="${Utils.format(Consts.PATH.MEMBERS_ARTICLE, this.user.uid)}/${value}" class="img-thumbnail"/>`;
                    }.bind(this)
                }, {
                    name: 'status',
                    __slot: 'status',
                    sortField: 'status'
                }, {
                    name: 'publish_date',
                    sortField: 'publish_date',
                    title: 'Publication Date',
                    formatter(value) {
                        return Utils.empty(value) ? 'Not published yet' : new DateTime(value).format('d mmmm yyyy');
                    }
                }, {
                    name: 'created',
                    sortField: 'created',
                    title: 'Created Date',
                    formatter(value) {
                        return Utils.empty(value) ? '--' : new DateTime(value).format('d mmmm yyyy');
                    }
                }, {
                    name: 'modified',
                    sortField: 'modified',
                    title: 'Modified Date',
                    formatter(value) {
                        return Utils.empty(value) ? 'Not modified yet' : new DateTime(value).format('d mmmm yyyy');
                    }
                }, 'visibility', 'actions'
            ],
            sortOrder: [{
                field: 'created',
                direction: 'desc'
            }],
            perPage: 10,
            params: {
                filters: {}
            },
            data: [],
            tableCls: {
                tableWrapper: '',
                tableHeaderClass: 'fixed',
                tableBodyClass: 'vuetable-semantic-no-top fixed',
                tableClass: 'ui blue selectable unstackable celled table',
                loadingClass: 'loading',
                ascendingIcon: 'fas fa-chevron-up',
                descendingIcon: 'fas fa-chevron-down',
                ascendingClass: 'sorted-asc',
                descendingClass: 'sorted-desc',
                sortableIcon: 'grey sort icon',
                handleIcon: 'grey sidebar icon'
            },
            paginationCls: {
                wrapperClass: 'pagination pull-right vuetable-pagination',
                activeClass: 'active',
                disabledClass: 'disabled',
                pageClass: 'page-link',
                linkClass: 'page-link',
                icons: {
                    first: 'fas fa-angle-double-left',
                    prev: 'fas fa-angle-left',
                    next: 'fas fa-angle-right',
                    last: 'fas fa-angle-double-right',
                }
            }
        };
    },
    watch: {
        params: {
            handler() {
                this.$nextTick(() => this.refresh());
            },
            deep: true
        },
        '$route': function (to, from) {
            if (to.path === this.componentMeta.path) {
                if (to.query.reload) {
                    this.$refs.Toolbar.reset();
                } else if (to.query.refresh) {
                    this.refresh();
                }
            }
        }
    },
    props: {
        user: Object
    },
    methods: {
        onPaginationData(paginationData) {
            this.$refs.pagination.setPaginationData(paginationData);
        },
        onChangePage(page) {
            this.$refs.datagrid.changePage(page);
        },
        onLoading() {
            this.loading = true;
        },
        onLoaded() {
            (() => this.loading = false).delay(this, 500);
        },
        formatStatus(row) {
            if (row.state === Consts.STATE.PENDING) {
                return {
                    status: 'Awaiting approval'
                }
            } else if (row.state === Consts.STATE.REJECTED) {
                return {
                    status: 'Rejected',
                    description: row.rejection_reason
                }
            } else {
                return {
                    status: Utils.capitalize(row.status.toLowerCase())
                }
            }
        },
        refresh() {
            this.$refs.datagrid.refresh();
        },
        delete(data) {
            return this.$api.delete('/Articles/delete', {params: data});
        },
        isVisibleOnWebsite(data) {
            return data.status === Consts.STATUS.PUBLISHED && data.state === Consts.STATE.APPROVED && data.visibility === true;
        },
        onActionClick(action, data) {
            switch (action) {
                case 'visibility':
                    this.$api.put('/Articles/visibility', data);
                    break;
                case 'delete':
                    this.$snotify.error(`Delete the selected ${this.componentMeta.name}?`, data.title, {
                        timeout: 5000,
                        showProgressBar: true,
                        closeOnClick: false,
                        pauseOnHover: false,
                        position: 'centerCenter',
                        buttons: [{
                            text: 'Proceed',
                            action: (toast) => {
                                this.$snotify.remove(toast.id);
                                this.loading = true;
                                this.delete(data).then((response) => {
                                    this.loading = false;
                                    if (response.data.success) {
                                        this.$snotify.success(response.data.msg);
                                        this.refresh();
                                    } else {
                                        this.$snotify.error(response.data.msg);
                                    }
                                }).catch(error => {
                                    this.$snotify.error(error);
                                });
                            },
                            bold: false
                        }, {
                            text: 'Cancel',
                            action: (toast) => {
                                this.$snotify.remove(toast.id);
                            }
                        }]
                    });
                    break;
                case 'edit':
                    this.$router.push({
                        path: `${this.componentMeta.pathEdit}/${data.id}`,
                        query: {
                            record: data
                        }
                    });
                    break;
                case 'view':
                    this.$root.loading = true;
                    if (data.type === Consts.ARTICLE_TYPE.INSIGHT) {
                        location.href = '//' + Utils.format(Consts.URL.WWW_ARTICLE, data.slug);
                    } else if (data.type === Consts.ARTICLE_TYPE.CASE_STUDY) {
                        location.href = '//' + Utils.format(Consts.URL.WWW_ARTICLE_CASE_STUDY, data.slug);
                    }
                    break;
            }
        }
    }
}
