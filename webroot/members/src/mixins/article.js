import LatestBusinessesAdded from './../components/latest-businesses-added';
import InputImageUpload from './../components/input-image-upload';
import Slugify from './../utils/slugify';
import _ from './../utils/utilities';
import CONSTS from './../config/constants';

export default {
    components: {
        InputImageUpload,
        LatestBusinessesAdded
    },
    data() {
        return {
            componentMeta: Object.freeze({
                model: {
                    title: '',
                    slug: '',
                    headline: '',
                    body: '',
                    image: null,
                    type: CONSTS.ARTICLE_TYPE.INSIGHT,
                    status: CONSTS.STATUS.DRAFT,
                    state: null
                },
                viewAddName: 'InsightAdd',
                viewEditName: 'InsightEdit',
                formName: 'insight-form',
                parentPath: '/insights'
            }),
            form: this.record || {},
            editorToolbar: [
                ['bold', 'italic', 'underline', {'script': 'sub'}, {'script': 'super'}],
                ['align', {'align': 'center'}, {'align': 'right'}, {'align': 'justify'}],
                ['blockquote'],
                [{'list': 'ordered'}, {'list': 'bullet'}],
                [{'indent': '-1'}, {'indent': '+1'}, 'clean']
            ],
            editorOptions: {
                formats: ['bold', 'italic', 'script', 'underline', 'blockquote', 'indent', 'list', 'align']
            },
            imageMsg: '',
            isImageSuccess: false,
            statusCls: {
                'status-draft': false, // draft
                'status-rejected': false, // rejected
                'status-pending': false, // pending
                'status-published': false // published
            },
            buttonSubmit: {
                label: '',
                icons: {
                    'fa-plus-circle': true,
                    'fa-save': false
                }
            },
            isWorking: false,
            hasError: false,
            message: '',
            isEditMode: false,
            isDraftAvailable: false
        };
    },
    props: {
        user: Object,
        record: Object
    },
    computed: {
        slug() {
            this.form.slug = Slugify.toSafeURL(this.form.title);
            return this.form.slug;
        },
        getImageUrl() {
            return _.format(CONSTS.PATH.MEMBERS_ARTICLE, this.user.uid)
        },
        statusText() {
            Object.entries(this.statusCls).forEach((cls) => this.statusCls[cls[0]] = false);

            let text = '',
                status = this.form.status,
                state = this.form.state;

            if (status === CONSTS.STATUS.PUBLISHED && state === CONSTS.STATE.APPROVED) {
                text = CONSTS.STATUS.PUBLISHED;
                this.statusCls['status-published'] = true;
            } else if (state === CONSTS.STATE.PENDING) {
                text = CONSTS.STATE.PENDING;
                this.statusCls['status-pending'] = true;
            } else if (state === CONSTS.STATE.REJECTED) {
                text = CONSTS.STATE.REJECTED;
                this.statusCls['status-rejected'] = true;
            } else {
                text = CONSTS.STATUS.DRAFT;
                this.statusCls['status-draft'] = true;
            }
            return _.capitalize(text.toLowerCase());
        },
        hasRejectedReason() {
            return this.form.state === CONSTS.STATE.REJECTED && !_.empty(this.form.rejection_reason);
        }
    },
    watch: {
        isEditMode(value) {
            this.setButtons(value === true ? 1 : 0);
        },
        '$route': function (to, from) {
            if (to.name === this.componentMeta.viewAddName) {
                // add new record
                this.form = _.shallowCopy(this.componentMeta.model);
                this.hasError = false;
                this.message = '';
                this.resetFormValidation();
                this.isEditMode = false;
            } else if (to.name === this.componentMeta.viewEditName) {
                // edit record (passed through query object)
                this.hasError = false;
                this.message = '';
                this.form = this.record;
                this.isEditMode = true;
                this.resetFormValidation();
                this.checkAndLoad(to.params.id);
            }
        }
    },
    methods: {
        resetFormValidation() {
            if (document.forms[this.componentMeta.formName]) {
                document.forms[this.componentMeta.formName].querySelectorAll('.is-invalid').forEach(node => node.classList.remove('is-invalid'));
            }
        },
        onInputChange(e, type) {
            let input = e.target;
            input.checkValidity();
            if (input.validity.valid) {
                e.target.classList.remove('is-invalid');
            }
        },
        onInputInvalid(e) {
            e.target.classList.add('is-invalid');
        },
        onImageSuccess(msg, file) {
            this.isImageSuccess = true;
            this.imageMsg = msg;
        },
        onImageFail(msg) {
            this.isImageSuccess = false;
            this.imageMsg = msg;
        },
        setButtons(mode = 0) {
            Object.entries(this.buttonSubmit.icons).forEach((icon) => this.buttonSubmit.icons[icon[0]] = false);
            if (mode === 0) {
                this.buttonSubmit.label = 'Publish now';
                this.buttonSubmit.icons['fa-plus-circle'] = true;
                this.isDraftAvailable = true;
            } else if (mode === 1) {
                this.isDraftAvailable = false;
                let status = this.form.status,
                    state = this.form.state;

                if (status === CONSTS.STATUS.PUBLISHED && state === CONSTS.STATE.APPROVED) {
                    this.buttonSubmit.label = 'Save changes and re-publish';
                } else if (state === CONSTS.STATE.PENDING) {
                    this.buttonSubmit.label = 'Save changes';
                } else if (state === CONSTS.STATE.REJECTED) {
                    this.isDraftAvailable = true;
                    this.buttonSubmit.label = 'Save changes and re-submit';
                } else {
                    this.buttonSubmit.label = 'Submit to review';
                    if (status === CONSTS.STATUS.DRAFT) {
                        this.isDraftAvailable = true;
                    }
                }
                this.buttonSubmit.icons['fa-save'] = true;
            }
        },
        save(type = 'draft') {
            let form = document.forms[this.componentMeta.formName],
                data = Object.assign({}, this.form);

            this.hasError = !form.checkValidity();
            this.message = this.hasError ? 'This form has errors' : '';

            if (this.hasError) {
                return;
            }

            if (_.empty(data.status)) {
                data.status = this.componentMeta.model.status;
            }
            if (_.empty(data.type)) {
                data.type = this.componentMeta.model.type;
            }

            this.isWorking = true;
            this.$api.post(`/Articles/${type}`, data).then(response => response.data).then(response => {
                this.isWorking = false;
                this.message = response.msg;
                if (response.success) {
                    this.hasError = false;
                    this.form.id = response.id;
                    this.form.image = response.image;
                    if (type === 'update') {
                        this.$snotify.success(response.msg, {
                            timeout: 4000
                        });
                        this.$bus.$emit('dashboard-refresh');
                        (() => this.$router.push({
                            path: this.componentMeta.parentPath,
                            query: {
                                reload: 1
                            }
                        })).delay(2000);
                    } else {
                        this.$snotify.success(response.msg);
                    }
                } else {
                    this.hasError = true;
                    this.$snotify.error(response.msg);
                }
            }).catch(error => {
                this.isWorking = false;
                this.hasError = true;
                this.message = error;
                this.$snotify.success(error);
            });
        },
        onSaveDraft() {
            this.save();
        },
        onSave() {
            this.save('update');
        },
        checkAndLoad(id) {
            if (!id) {
                this.isEditMode = false;
            } else {
                this.isEditMode = true;
            }
            this.setButtons(this.isEditMode === true ? 1 : 0);
            if (this.isEditMode && !this.form.id || this.form.id != id) {
                this.$api.get('/Articles/article', {
                    params: {
                        id: id
                    }
                }).then(response => this.form = response.data);
            }
        }
    },
    created() {
        // load record if Id provided and it is first load of component
        this.checkAndLoad(this.$route.params.id);
    }
}
