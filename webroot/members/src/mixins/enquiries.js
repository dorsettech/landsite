import _ from '../utils/utilities';
import DateTime from '../utils/date';

export default {
    data() {
        return {
            loading: false,
            columns: [
                {
                    name: 'id',
                    title: 'Enquiry ID',
                    sortField: 'PropertyEnquiries.id'
                }, {
                    name: 'enquiry_title',
                    title: 'Property Title',
                    sortField: 'enquiry_title'
                }, {
                    name: 'full_name',
                    title: 'Name',
                    sortField: 'full_name'
                }, {
                    name: 'email',
                    title: 'E-mail',
                    sortField: 'email'
                }, {
                    name: 'phone',
                    title: 'Phone',
                    sortField: 'PropertyEnquiries.phone'
                }, {
                    name: 'created',
                    title: 'Date received',
                    sortField: 'PropertyEnquiries.created',
                    formatter(value) {
                        return _.empty(value) ? '--' : new DateTime(value).format('d mmmm yyyy');
                    }
                }, 'actions'
            ],
            sortOrder: [{
                field: 'PropertyEnquiries.created',
                direction: 'desc'
            }],
            params: {
                id: null
            },
            tableCls: {
                tableWrapper: '',
                tableHeaderClass: 'fixed',
                tableBodyClass: 'vuetable-semantic-no-top fixed',
                tableClass: 'ui blue selectable unstackable celled table',
                loadingClass: 'loading',
                ascendingIcon: 'fas fa-chevron-up',
                descendingIcon: 'fas fa-chevron-down',
                ascendingClass: 'sorted-asc',
                descendingClass: 'sorted-desc',
                sortableIcon: 'grey sort icon',
                handleIcon: 'grey sidebar icon'
            },
            enquiry: {
                show: false,
                title: '',
                body: ''
            }
        }
    },
    watch: {
        params: {
            handler() {
                this.$refs.datagrid.refresh();
            },
            deep: true
        }
    },
    methods: {
        refresh() {
            return this.$refs.datagrid.refresh();
        },
        onLoading() {
            this.loading = true;
        },
        onLoaded() {
            (() => this.loading = false).delay(this, 500);
        },
        onViewClick(property) {
            this.enquiry.title = property.enquiry_title;
            this.enquiry.body = property.message;
            this.enquiry.show = true;
        }
    }
}
