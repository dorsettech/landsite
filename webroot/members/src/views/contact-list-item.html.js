import _ from './../utils/utilities';

export default {
    render() {
        let prefix = `${_.guid()}-`;
        return `
        <div class="form-row v-comp-contact-list-item" :class="cls">
            <div class="col-md-4 m-b-10">
                <label for="${prefix}name">{{labels.name}}</label>
                <input v-model.trim="item.name" class="form-control" type="text" id="${prefix}name" name="name" :required="required"
                    maxlength="255"
                    @change="$emit('change', $event)"
                    @invalid.prevent="$emit('invalid', $event)">
                <div class="invalid-feedback">This field is required</div>
            </div>
            <div class="col-md-4 m-b-10">
                <label for="${prefix}email">{{labels.email}}</label>
                <input v-model.trim="item.email" class="form-control" type="email" id="${prefix}email" name="email" :required="required"
                    maxlength="100"
                    @change="$emit('change', $event)"
                    @invalid.prevent="$emit('invalid', $event)">
                <div class="invalid-feedback">This field is required</div>
            </div>
            <div class="col-sm-12 col-md-auto m-b-10">
                <a @click.prevent="$emit('action', action, item)" class="btn btn-green btn-icon btn-circle btn-sm action" v-if="action === 'add'"><i class="fas fa-plus"></i></a>
                <a @click.prevent="$emit('action', action, item)" class="btn btn-danger btn-icon btn-circle btn-sm action" v-else><i class="fas fa-minus"></i></a>
            </div>
        </div>`;
    }
}
