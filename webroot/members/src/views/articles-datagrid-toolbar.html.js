import Consts from '../config/constants';
import _ from '../utils/utilities';

export default {
    render() {
        return `
            <form class="v-comp-articles-datagrid-toolbar w-100">
                <div class="form-group-sm row w-100 p-10 p-t-0 p-l-0 pull-right">
                    <div class="col p-l-0">
                        <a :href="addUrl" class="btn btn-primary">
                            <i class="fas fa-plus-circle"></i>
                            {{addLabel}}
                        </a>
                    </div>
                    <div class="col-md-auto">
                        <label class="col-form-label">Status:</label> 
                    </div>
                    <div class="col-md-auto">
                        <select class="form-control w-150" v-model="params.filters.status" @change="onChange">
                            <option value="">All</option>
                            <option value="${Consts.STATUS.DRAFT}">${_.capitalize(Consts.STATUS.DRAFT.toLowerCase())}</option>
                            <option value="${Consts.STATUS.PUBLISHED}">${_.capitalize(Consts.STATUS.PUBLISHED.toLowerCase())}</option>
                            <option value="${Consts.STATE.PENDING}">${_.capitalize(Consts.STATE.PENDING.toLowerCase())}</option>
                            <option value="${Consts.STATE.REJECTED}">${_.capitalize(Consts.STATE.REJECTED.toLowerCase())}</option>
                        </select>
                    </div>
                    <div class="col-md-auto">
                        <label class="col-form-label">Publication Date:</label>
                    </div>
                    <div class="col-md-auto">
                        <flat-pickr v-model="params.filters.publish_date" :config="timePickerOptions" class="form-control w-150" @on-change="onChange"></flat-pickr>
                        <a class="clear-picker" @click="params.filters.publish_date = ''">
                            <i class="fa fa-times"/>
                        </a>
                    </div>
                    <div class="col-md-auto">
                        <label class="col-form-label">Created Date:</label>
                    </div>
                    <div class="col-md-auto">
                        <flat-pickr v-model="params.filters.created" :config="timePickerOptions" class="form-control w-150" @on-change="onChange"></flat-pickr>
                        <a class="clear-picker" @click="params.filters.created = ''">
                            <i class="fa fa-times"/>
                        </a>
                    </div>
                    <div class="col-md-auto">
                        <label class="col-form-label">Modified Date:</label>
                    </div>
                    <div class="col-md-auto">
                        <flat-pickr v-model="params.filters.modified" :config="timePickerOptions" class="form-control w-150" @on-change="onChange"></flat-pickr>
                        <a class="clear-picker" @click="params.filters.modified = ''">
                            <i class="fa fa-times"/>
                        </a>
                    </div>                                        
                    <div class="col-md-auto">
                        <button type="button" class="btn btn-default" @click="onRefresh">
                            <i class="fas fa-sync-alt"></i>
                            Refresh
                        </button>
                    </div>                                        
                </div>                                        
            </form>            
        `;
    }
}
