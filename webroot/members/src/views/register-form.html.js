import C from '../config/constants';

export default {
    render() {
        return `
        <div class="account">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="logo col">
                        <a href="//${C.URL.WWW}">
                            <img src="/members/assets/logo.png" alt="logo" class="img-fluid">
                        </a>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="text col-12 col-md-6 col-xxl-4">
                        <h1 class="heading">{{__statics.n('content_title')}}</h1>
                        ${__statics.n('content_description1')}
                        <hr>                        
                        <div class="slogan">
                            <div class="image">
                                <img src="/members/assets/image.jpg" alt="slogan">
                            </div>
                            <div class="text">{{__statics.n('content_slogan')}}</div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-xxl-4">
                        <div class="accordion v-comp-register-form" id="register-form-steps">
                            <transition name="slide-fade" @after-leave="onTransitionAfterLeave(1)">
                                <div id="register-form-step-1" class="step collapse" data-parent="#register-form-steps" v-show="visibility.step1">
                                    <div class="content">
                                        <div class="heading">Create an account</div>
                                        <form @submit.prevent="onUserSubmit()">
                                            <div class="form-row">
                                                <div class="form-group col-12 col-sm-6 col-md-12 col-lg-6" required>
                                                    <input type="text" class="form-control" placeholder="First Name *"
                                                    name="first_name" maxlength="100" required 
                                                    :class="{'is-invalid': forms.user.fields.first_name.isInvalid}" 
                                                    v-model.trim="forms.user.fields.first_name.value" 
                                                    @change="onChange('user', $event)" 
                                                    @invalid.prevent="onInvalid('user', $event)"
                                                    >
                                                    <div class="invalid-feedback">First name is required</div>
                                                </div>
                                                <div class="form-group col-12 col-sm-6 col-md-12 col-lg-6" required>
                                                    <input type="text" class="form-control" placeholder="Surname *"
                                                    name="last_name" maxlength="100" required 
                                                    :class="{'is-invalid': forms.user.fields.last_name.isInvalid}" 
                                                    v-model.trim="forms.user.fields.last_name.value" 
                                                    @change="onChange('user', $event)" 
                                                    @invalid.prevent="onInvalid('user', $event)"
                                                    >
                                                    <div class="invalid-feedback">Last name is required</div>
                                                </div>
                                                <div class="form-group col-12">
                                                    <input type="text" class="form-control" placeholder="Company Name"
                                                    name="company" maxlength="255" 
                                                    v-model.trim="forms.user.fields.company.value" 
                                                    >
                                                </div>
                                                <div class="form-group col-12 col-sm-6 col-md-12 col-lg-6" required>
                                                    <input type="email" class="form-control" placeholder="Email *"
                                                    name="email" maxlength="100" required 
                                                    :class="{'is-invalid': forms.user.fields.email.isInvalid}" 
                                                    v-model.trim="forms.user.fields.email.value" 
                                                    @change="onChange('user', $event)" 
                                                    @invalid.prevent="onInvalid('user', $event)"
                                                    >
                                                    <div class="invalid-feedback">Please enter a valid email address</div>
                                                </div>
                                                <div class="form-group col-12 col-sm-6 col-md-12 col-lg-6" required>
                                                    <input type="password" class="form-control" placeholder="Password *"
                                                    name="password" minlength="6" required 
                                                    :class="{'is-invalid': forms.user.fields.password.isInvalid}" 
                                                    v-model.trim="forms.user.fields.password.value" 
                                                    @change="onChange('user', $event)" 
                                                    @invalid.prevent="onInvalid('user', $event)"
                                                    >
                                                    <div class="invalid-feedback">Password is required</div>
                                                </div>
                                                <div class="form-group col-12">
                                                    <button type="submit" class="btn btn-primary" data-toggle="collapse" data-target="#register-form-step-2" :aria-disabled="forms.user.isWorking" :disabled="forms.user.isWorking">
                                                        <i class="fas fa-cog fa-spin" aria-hidden="true" v-show="forms.user.isWorking"></i>
                                                        Continue
                                                    </button>
                                                </div>
                                                <transition name="fade">
                                                    <div class="form-group col-12" v-show="forms.user.message">
                                                        <div class="alert text-center" :class="{'alert-danger': forms.user.hasError, 'alert-success': !forms.user.hasError }" role="alert">{{forms.user.message}}</div>
                                                    </div>
                                                </transition>
                                                <div class="signin col-12">
                                                    Already have an account? <a href="/login">Sign In</a>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="step1image">
                                            <p class="text">{{__statics.n('image_text')}}</p>
                                            <a class="btn btn-primary" :href="__statics.n('image_button_href')">{{__statics.n('image_button_label')}}</a>
                                        </div>                            
                                    </div>
                                </div>
                            </transition>
                            <transition name="slide-fade" @after-leave="onTransitionAfterLeave(2)">
                                <div id="register-form-step-2" class="step collapse" data-parent="#register-form-steps" v-show="visibility.step2">
                                    <div class="content">
                                        <div class="heading">What are you interested in?</div>
                                        <form @submit.prevent="onPrefsSubmit()">
                                            <div class="customradio form-row" v-if="false">
                                                <div class="form-group col-12 col-sm-auto col-md-12 col-lg-auto">
                                                    <div class="form-check">
                                                        <input type="radio" class="custom-control-input" name="location" id="alllocations" 
                                                         v-model="forms.prefs.fields.location" value="all">
                                                        <label class="form-check-label" for="alllocations">All Locations</label>
                                                    </div>
                                                </div>
                                                <div class="form-group col-12 col-sm">
                                                    <div class="form-check">
                                                        <input type="radio" class="custom-control-input" name="location" id="specificlocation" 
                                                        v-model="forms.prefs.fields.location" value="select">
                                                        <input type="text" class="form-control" placeholder="Select a specific location" for="specificlocation"
                                                        v-model.trim="forms.prefs.fields.locationText"
                                                        @focus="onLocationFocus" @blur="onLocationBlur">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-12">
                                                    <div class="custom-control custom-switch">
                                                        <input type="checkbox" class="custom-control-input" id="customSwitch1"
                                                        v-model="forms.prefs.fields.buying"
                                                        @change="onPrefChange">
                                                        <label class="custom-control-label" for="customSwitch1">
                                                            <span>{{__statics.n('toggle1_title')}}</span><br>
                                                            {{__statics.n('toggle1_description')}}
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group col-12">
                                                    <div class="custom-control custom-switch">
                                                        <input type="checkbox" class="custom-control-input" id="customSwitch2"
                                                        v-model="forms.prefs.fields.selling"
                                                        @change="onPrefChange">
                                                        <label class="custom-control-label" for="customSwitch2">
                                                            <span>{{__statics.n('toggle2_title')}}</span><br>
                                                            {{__statics.n('toggle2_description')}}
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group col-12">
                                                    <div class="custom-control custom-switch">
                                                        <input type="checkbox" class="custom-control-input" id="customSwitch3"
                                                        v-model="forms.prefs.fields.professional"
                                                        @change="onPrefChange">
                                                        <label class="custom-control-label" for="customSwitch3">
                                                            <span>{{__statics.n('toggle3_title')}}</span><br>
                                                            {{__statics.n('toggle3_description')}}
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group col-12">
                                                    <div class="custom-control custom-switch">
                                                        <input type="checkbox" class="custom-control-input" id="customSwitch4"
                                                        v-model="forms.prefs.fields.insights"
                                                        @change="onPrefChange">
                                                        <label class="custom-control-label" for="customSwitch4">
                                                            <span>{{__statics.n('toggle4_title')}}</span><br>
                                                            {{__statics.n('toggle4_description')}}
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group col-12">
                                                    <transition name="fade">
                                                        <div class="invalid-feedback text-center options-feedback" v-show="forms.prefs.hasError">At least one of the options above is required.</div>
                                                    </transition>
                                                </div>
                                                <div class="form-group col-12">
                                                    <button type="submit" class="btn btn-primary" data-toggle="collapse" data-target="#register-form-step-3">Continue</button>
                                                </div>
                                                <div class="col-12 text-center">If you change your mind, you can amend these preferences later on.</div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </transition>
                            <transition name="slide-fade">    
                                <div id="register-form-step-3" class="step collapse" data-parent="#register-form-steps" v-show="visibility.step3">
                                    <div class="content">
                                        <div class="heading">{{__statics.n('categories_title')}}</div>
                                        <form @submit.prevent="onCatsSubmit()">
                                            <div class="form-row">
                                                <div class="form-group col-12">
                                                    {{__statics.n('categories_description')}}
                                                </div>
                                                <div class="form-group col-12">
                                                    <strong>Select one of our recommended categories:</strong>
                                                </div>
                                            </div>
                                            <div class="customcheck form-row">
                                                <category-image-checkbox
                                                  v-for="(item, index) in recommended"
                                                  v-bind:item="item"
                                                  v-bind:index="index"
                                                  v-bind:key="item.id"
                                                  v-model="forms.cats.fields.recommended"
                                                ></category-image-checkbox>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-12">
                                                    <strong>Or choose your category by searching for your preferred industries of interest:</strong>
                                                </div>
                                                <div class="col-12">
                                                    <vue-simple-suggest
                                                        ref="CategoryAutoComplete"
                                                        :list="forms.cats.items"
                                                        display-attribute="name"
                                                        value-attribute="id"
                                                        :max-suggestions=5
                                                        :styles="forms.cats.autoCompleteStyles"
                                                        :destyled=true
                                                        :filter-by-query=true
                                                        :prevent-submit=true
                                                        @select="onCategorySelect">
                                                    </vue-simple-suggest>                                    
                                                </div>
                                                <div class="col-12">
                                                    <ul class="category-list-group">
                                                        <category-list-item
                                                          v-for="item in forms.cats.fields.selected"
                                                          v-bind:item="item"
                                                          v-bind:key="item.id"
                                                          v-model="forms.cats.fields.selected"
                                                          v-bind:name="item.name"
                                                        ></category-list-item>
                                                    </ul>
                                                </div>
                                            </div>
                                            <transition name="fade">
                                                <div class="form-group col-12" v-show="forms.cats.message">
                                                    <div class="invalid-feedback text-center options-feedback" :class="{'text-danger': forms.cats.hasError, 'text-success': !forms.cats.hasError }">{{forms.cats.message}}</div>
                                                </div>
                                            </transition>
                                            <div class="form-row">
                                                <div class="form-group col-12">
                                                    <button type="submit" class="btn btn-primary" :aria-disabled="forms.cats.isWorking" :disabled="forms.cats.isWorking" v-show="forms.cats.isVisible">
                                                        <i class="fas fa-cog fa-spin" aria-hidden="true" v-show="forms.cats.isWorking"></i>
                                                        Submit
                                                    </button>
                                                </div>
                                                <div class="col-12 text-center" v-if="forms.cats.isVisible">If you change your mind, you can amend these preferences later on.</div>
                                                <div class="col-12" v-else><br/><br/></div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </transition>
                        </div>
                        <p>{{__statics.n('privacy_policy_info')}}</p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="text col-12 col-xxl-8">
                        <h2 class="heading">{{__statics.n('content_title2')}}</h2>
                        ${__statics.n('content_description2')}
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="text col-12 col-xxl-8">
                        <h2 class="heading extra">{{__statics.n('content_title3')}}</h2>
                        ${__statics.n('content_description3')}
                    </div>
                </div>
                <div class="row plans justify-content-center">
                    <div class="box col-12 col-md-3 col-xxl-3">
                        <h4 class="header">Standard</h4>
                        <p class="description">{{__statics.n('plan_standard_details')}}</p>
                        <div class="plan">
                            <img src="/members/assets/logo-white.png" class="img-fluid" alt="The Landsite"/>
                            <span>{{__statics.n('plan_standard_price_line_1')}}</span>                            
                            <span>{{__statics.n('plan_standard_price_line_2')}}</span>                            
                            <span>{{__statics.n('plan_standard_price_line_3')}}</span>                            
                        </div>
                    </div>
                    <div class="box col-12 col-md-3 col-xxl-3">
                        <h4 class="header">Premium</h4>
                        <p class="description">{{__statics.n('plan_premium_details')}}</p>
                        <div class="plan">
                            <img src="/members/assets/logo-white.png" class="img-fluid" alt="The Landsite"/>
                            <span>{{__statics.n('plan_premium_price_line_1')}}</span>                            
                            <span>{{__statics.n('plan_premium_price_line_2')}}</span>                     
                            <span>{{__statics.n('plan_premium_price_line_3')}}</span>                     
                        </div>
                    </div>
                    <div class="box col-12 col-md-3 col-xxl-3">
                        <h4 class="header">Featured</h4>
                        <p class="description">{{__statics.n('plan_featured_details')}}</p>
                        <div class="plan">
                            <img src="/members/assets/logo-white.png" class="img-fluid" alt="The Landsite"/>
                            <span>{{__statics.n('plan_featured_price_line_1')}}</span>                            
                            <span>{{__statics.n('plan_featured_price_line_2')}}</span>                     
                            <span>{{__statics.n('plan_featured_price_line_3')}}</span>                     
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="text col-12 col-xxl-8">
                        <h3 class="heading-full">{{__statics.n('bottom_slogan')}}</h3>
                    </div>
                </div>
            </div>
        </div>`;
    }
}
