import C from '../config/constants';
import Utils from "../utils/utilities";

export default {
    render() {
        let prefix = `${Utils.guid()}-`;
        return `
            <div class="v-comp-module-body v-comp-business-listing">
                <loader :active=loading></loader>
                <h1 class="page-header" v-if="isPaymentPage()">Payment Page</h1>
                <h1 class="page-header" v-else-if="!isEditMode">Add your Business Profile</h1>
                <h1 class="page-header" v-else>Edit your Business Profile</h1>
                <h6 class="page-description" v-if="isPaymentPage()">Lorem dolor sit amet situr moda eles.</h6>                
                <h6 class="page-description" v-else>
                    Get started on your business profile by entering the required details in the form below. Provide answers that are as detailed as possible to ensure that potential customers can find the information they need about your services as easily as possible.
                    <br/><br/>
                    Once you are finished, click <strong>Continue</strong> to move on to the next step.
                </h6>                
                <div class="row">
                    <div class="col-lg-12 col-xl-8">
                        <div class="sw-main sw-theme-ls">
                            
                            <ul class="nav nav-tabs step-anchor pretty-scrollbar" v-show="!isPaymentPage()">
                                <li class="nav-item" :class="{active: this.step === 0}">
                                    <a class="nav-link" :class="{disabled: isStepDisabled(0)}" @click="goTo(0)">
                                        <span class="number" v-if="!isEditMode">1</span> 
                                        <span class="info text-ellipsis">Profile Details</span>
                                    </a>
                                </li>
                                <li class="nav-item" :class="{active: this.step === 1}">
                                    <a class="nav-link" :class="{disabled: isStepDisabled(1)}" @click="goTo(1)">
                                        <span class="number" v-if="!isEditMode">2</span> 
                                        <span class="info text-ellipsis">Contact Details</span>
                                    </a>
                                </li>
                                <li class="nav-item" :class="{active: this.step === 2}">
                                    <a class="nav-link" :class="{disabled: isStepDisabled(2)}" @click="goTo(2)">
                                        <span class="number" v-if="!isEditMode">3</span> 
                                        <span class="info text-ellipsis">Products & Services</span>
                                    </a>
                                </li>
                                <li class="nav-item" :class="{active: this.step === 3}">
                                    <a class="nav-link" :class="{disabled: isStepDisabled(3)}" @click="goTo(3)">
                                        <span class="number" v-if="!isEditMode">4</span>
                                        <span class="info text-ellipsis">Areas covered</span>
                                    </a>
                                </li>
                                <li class="nav-item" :class="{active: this.step === 4}">
                                    <a class="nav-link" :class="{disabled: isStepDisabled(4)}" @click="goTo(4)">
                                        <span class="number" v-if="!isEditMode">5</span> 
                                        <span class="info text-ellipsis">Images & Video</span>
                                    </a>
                                </li>
                                
                                <!-- 
                                    https://econnect4u.atlassian.net/browse/LS-266
                                    d-none added 
                                -->
                                <li class="nav-item d-none" :class="{active: this.step === 5}">
                                    <a class="nav-link" :class="{disabled: isStepDisabled(5)}" @click="goTo(5)">
                                        <span class="number" v-if="!isEditMode">6</span> 
                                        <span class="info text-ellipsis">Credentials</span>
                                    </a>
                                </li>
                                
                                <li class="nav-item" :class="{active: this.step === 6}">
                                    <a class="nav-link" :class="{disabled: isStepDisabled(6)}" @click="goTo(6)">
                                        <!-- 
                                            https://econnect4u.atlassian.net/browse/LS-266
                                            was number 7, changed to 6  
                                        -->
                                        <span class="number" v-if="!isEditMode">6</span> 
                                        <span class="info text-ellipsis">
                                            <span class="info-small">Publish</span>
                                            <span class="info-large">Make your profile stand out</span>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                            
                            <div class="sw-container tab-content panel">
                                
                                <div class="tab-pane step-content" v-show="this.step === 0">
                                    <form onsubmit="return false" name="listing-details-form">
                                        <div class="form-row m-b-10">
                                            <div class="col-sm-12 col-md-4">
                                                <label for="${prefix}company">Company Name</label>
                                                <input v-model.trim="form.company" class="form-control" type="text" id="${prefix}company" name="company" required
                                                    maxlength="255"
                                                    @change="onInputChange"
                                                    @invalid.prevent="onInputInvalid">
                                                <div class="invalid-feedback">This field is required</div>
                                                <p class="text-muted slug" v-show="form.slug !== ''">URL: {{slug}}</p>
                                            </div>
                                            <div class="col-sm-12 col-md-4">
                                                <label for="${prefix}category_id">Business Category</label>
                                                <select class="form-control" v-model="form.category_id" @change="onInputChange"
                                                    @invalid.prevent="onInputInvalid" required>
                                                    <option v-for="item in categories" :item="item" :key="item.id" :value="item.id">{{item.name}}</option>
                                                </select>
                                                <div class="invalid-feedback">This field is required</div>
                                            </div>
                                            <div class="col-sm-12 col-md-2">
                                            <label for="${prefix}phone">Phone</label>
                                                <input v-model.trim="form.phone" class="form-control" type="text" id="${prefix}phone" name="phone" 
                                                    maxlength="50"
                                                    @change="onInputChange($event, 'phone')" 
                                                    @invalid.prevent="onInputInvalid">
                                            </div>
                                            <div class="col-sm-12 col-md-2">
                                                <label for="${prefix}postcode">Postcode</label>
                                                <input v-model.trim="form.postcode" class="form-control" type="text" id="${prefix}postcode" name="postcode" required :disabled="postcodeDisabled" :aria-disabled="postcodeDisabled"
                                                    maxlength="25"
                                                    @change="onInputChange($event, 'postcode')" 
                                                    @invalid.prevent="onInputInvalid">
                                                <div class="input-feedback position-absolute" :class="{ 'invalid-feedback': !isPostcodeSuccess, 'valid-feedback': isPostcodeSuccess}" v-show="postcodeMsg !== ''">{{postcodeMsg}}</div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <hr/>
                                        </div>
                                        <div class="form-row m-b-10">
                                            <div class="col-md-12">
                                                <label for="${prefix}description_short">The first few words about business ...</label>
                                                <span class="pull-right input-counter description-short-counter">{{descriptionShortLength}}/{{descriptionShortLimit}}</span>
                                                <textarea v-model.trim="form.description_short" class="form-control no-resizable pretty-scrollbar" id="${prefix}description_short" name="description_short" required
                                                    maxlength="260"
                                                    @input="onInput('description_short')"
                                                    @change="onInputChange" 
                                                    @invalid.prevent="onInputInvalid"></textarea>
                                                <div class="invalid-feedback">This field is required</div>
                                            </div>
                                            <div class="col-md-12">
                                                <p class="text-muted m-b-0 m-t-5 m-l-5">This is your business profile short description that will be showing on the search result pages.</p>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <label for="${prefix}description">Business Overview</label>
                                                <span class="pull-right input-counter description-counter">{{descriptionLength}}/{{descriptionLimit}}</span>
                                            </div>                                            
                                            <div class="col-md-12">
                                                <vue-editor 
                                                    id="${prefix}description"
                                                    v-model="form.description" 
                                                    class="vue-editor description-editor"
                                                    ref="DescriptionEditor"
                                                    :editorToolbar="editorToolbar"
                                                    :editorOptions="editorOptions"
                                                    @text-change="onEditorChange">
                                                </vue-editor>
                                            </div>                                            
                                            <div class="col-md-12">
                                                <p class="text-muted m-b-0 m-t-5 m-l-5">Enter as much information possible. Ads with detailed and longer descriptions get more views and enquiries!</p>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <hr/>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <label>Website and social links</label>                                                
                                            </div>
                                            <div class="form-row w-100">
                                                <div class="col-md-6 m-b-10">
                                                    <label for="${prefix}website_url" class="sr-only">Website URL</label>
                                                    <input v-model.trim="form.website_url" class="form-control" type="url" id="${prefix}website_url" name="website_url"
                                                        maxlength="255"
                                                        @change="onInputChange" 
                                                        @invalid.prevent="onInputInvalid" placeholder="Website link">
                                                    <div class="invalid-feedback">Please provide a valid URL, protocol is required http(s)://</div>
                                                </div>
                                                <div class="col-md-6 m-b-10">
                                                    <label for="${prefix}google_url" class="sr-only">Google Business</label>
                                                    <input v-model.trim="form.google_url" class="form-control" type="url" id="${prefix}google_url" name="google_url"
                                                        maxlength="255"
                                                        @change="onInputChange" 
                                                        @invalid.prevent="onInputInvalid" placeholder="Google Business link">
                                                    <div class="invalid-feedback">Please provide a valid URL, protocol is required http(s)://</div>
                                                </div>
                                            </div>
                                            <div class="form-row w-100">
                                                <div class="col-md-6 m-b-10">
                                                    <label for="${prefix}facebook_url" class="sr-only">Facebook URL</label>
                                                    <input v-model.trim="form.facebook_url" class="form-control" type="url" id="${prefix}facebook_url" name="facebook_url"
                                                        maxlength="255"
                                                        @change="onInputChange" 
                                                        @invalid.prevent="onInputInvalid" placeholder="Facebook link">
                                                    <div class="invalid-feedback">Please provide a valid URL, protocol is required http(s)://</div>
                                                </div>
                                                <div class="col-md-6 m-b-10">
                                                    <label for="${prefix}linkedin_url" class="sr-only">Linkedin URL</label>
                                                    <input v-model.trim="form.linkedin_url" class="form-control" type="url" id="${prefix}linkedin_url" name="linkedin_url"
                                                        maxlength="255"
                                                        @change="onInputChange" 
                                                        @invalid.prevent="onInputInvalid" placeholder="Linkedin link">
                                                    <div class="invalid-feedback">Please provide a valid URL, protocol is required http(s)://</div>
                                                </div>
                                            </div>
                                            <div class="form-row w-100">
                                                <div class="col-md-6 m-b-10">
                                                    <label for="${prefix}twitter_url" class="sr-only">Twitter URL</label>
                                                    <input v-model.trim="form.twitter_url" class="form-control" type="url" id="${prefix}twitter_url" name="twitter_url"
                                                        maxlength="255"
                                                        @change="onInputChange" 
                                                        @invalid.prevent="onInputInvalid" placeholder="Twitter link">
                                                    <div class="invalid-feedback">Please provide a valid URL, protocol is required http(s)://</div>
                                                </div>
                                                <div class="col-md-6 m-b-10">
                                                    <label for="${prefix}hubspot_url" class="sr-only">Hubspot URL</label>
                                                    <input v-model.trim="form.hubspot_url" class="form-control" type="url" id="${prefix}hubspot_url" name="hubspot_url"
                                                        maxlength="255"
                                                        @change="onInputChange" 
                                                        @invalid.prevent="onInputInvalid" placeholder="Hubspot link">
                                                    <div class="invalid-feedback">Please provide a valid URL, protocol is required http(s)://</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <hr/>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <label>Quick wins</label>                                                
                                            </div>
                                            <div class="form-row w-100">
                                                <div class="col-md-4 m-b-10">
                                                    <label for="${prefix}quick_win_1" class="sr-only">Quick win 1</label>
                                                    <input v-model.trim="form.quick_win_1" class="form-control" type="text" id="${prefix}quick_win_1" name="quick_win_1"
                                                        maxlength="25" placeholder="Quick win 1">
                                                </div>
                                                <div class="col-md-4 m-b-10">
                                                    <label for="${prefix}quick_win_2" class="sr-only">Quick win 2</label>
                                                    <input v-model.trim="form.quick_win_2" class="form-control" type="text" id="${prefix}quick_win_2" name="quick_win_2"
                                                        maxlength="25" placeholder="Quick win 2">
                                                </div>
                                                <div class="col-md-4 m-b-10">
                                                    <label for="${prefix}quick_win_3" class="sr-only">Quick win 3</label>
                                                    <input v-model.trim="form.quick_win_3" class="form-control" type="text" id="${prefix}quick_win_3" name="quick_win_3"
                                                        maxlength="25" placeholder="Quick win 3">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <hr/>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <label>Opening Hours</label>                                                
                                            </div>
                                            <opening-hours
                                                v-for="(item, index) in form.opening_hours"
                                                v-bind:item="item"
                                                v-bind:index="index"
                                                v-bind:key="index"
                                                v-model="form.opening_hours[index]"
                                                :button="index === 0 ? '+' : '-'"
                                                @change="onOpeningHoursChange"
                                            ></opening-hours>
                                        </div>                                        
                                    </form>
                                </div>
                                
                                <div class="tab-pane step-content" v-show="this.step === 1">
                                    <form onsubmit="return false" name="business-contact-details-form">
                                        <div class="form-row m-b-10">
                                            <p>Please add contact information for up to 5 members of your team using the fields below.</p>
                                        </div>
                                        <contact-list-item
                                          v-for="(item, index) in form.service_contacts"
                                          v-bind:item="item"
                                          v-bind:key="index"
                                          v-model="form.service_contacts"
                                          :required="index === 0"
                                          :action="index === 0 ? 'add' : 'remove'"
                                          :labels="index === 0 ? {name: 'Person Name (mandatory)', email: 'Email'} : {name: 'Person Name', email: 'Email'}"
                                          @action="onContactAction"
                                          @change="onInputChange"
                                          @invalid="onInputInvalid"
                                        ></contact-list-item>
                                    </form>
                                </div>
                                
                                <div class="tab-pane step-content" v-show="this.step === 2">
                                    <form onsubmit="return false" name="products-services-form">
                                        <div class="form-row m-b-10">
                                            <p>Showcase the specific services your business offers. Type in the search field below and list each of the professionals services your business has to offer.</p>
                                        </div>
                                        <div class="form-row m-b-20">
                                            <div class="col-12">
                                                <vue-simple-suggest
                                                    ref="ProductAutoComplete"
                                                    :list="products"
                                                    display-attribute="name"
                                                    value-attribute="id"
                                                    :max-suggestions=5
                                                    :styles="autoCompleteStyles"
                                                    :destyled=true
                                                    :filter-by-query=true
                                                    :prevent-submit=true
                                                    @select="onProductSelect" placeholder="Search for a service here...">
                                                </vue-simple-suggest>                       
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-12">
                                                <checkbox-group
                                                    ref="ProductsCheckboxGroup" 
                                                    :items="products"
                                                    :columns=3
                                                    v-model.lazy="itemProducts">
                                                </checkbox-group>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                
                                <div class="tab-pane step-content" v-show="this.step === 3">
                                    <form onsubmit="return false" name="areas-covered-form">
                                        <div class="form-row m-b-10">
                                            <p>Ensure potential customers can find you with ease. Type in the search fields below the counties you are able to offer services within and list them on your profile. If you cover the whole of UK please choose all.</p>
                                        </div>
                                        <div class="form-row m-b-20">
                                            <div class="col-12">
                                                <vue-simple-suggest
                                                    ref="AreaAutoComplete"
                                                    :list="areas"
                                                    display-attribute="name"
                                                    value-attribute="id"
                                                    :max-suggestions=5
                                                    :styles="autoCompleteStyles"
                                                    :destyled=true
                                                    :filter-by-query=true
                                                    :prevent-submit=true
                                                    @select="onAreaSelect" placeholder="Search for a location here...">
                                                </vue-simple-suggest>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-12">
                                                <checkbox-group
                                                    ref="AreasCheckboxGroup" 
                                                    :items="areas"
                                                    :columns=3
                                                    v-model.lazy="itemAreas">
                                                </checkbox-group>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                
                                <div class="tab-pane step-content" v-show="this.step === 4">
                                    <p>Now that you have added your business information, you can add a video and images to accompany it. Follow the steps below to upload your chosen imagery.</p>
                                    <p>Once you are finished, click <strong>Continue</strong> to move on to the next step, or <strong>Save as Draft</strong> to make further edits later.</p> 
                                    <input-video-preview
                                        v-model="video"
                                        :prevent-mutation-delay=1000
                                        id="${prefix}video"
                                        name="video" 
                                        label="Add a business video"
                                        tip="Enter a YouTube video link to be displayed on your business profile page">
                                    </input-video-preview>
                                    <div class="form-row m-t-10">
                                        <hr/>
                                    </div>
                                    <input-images-upload
                                        ref="InputImagesUpload"
                                        label="Add your business images - add up to 5 images"
                                        description="Recommended image size - 1170 x 780"
                                        :image-url="getImagesUrl"
                                        :sorting=true
                                        name="images"
                                        v-model="images"
                                        :limit=imagesHardLimit
                                        @success="onImagesUpload"
                                        @fail="onImagesUpload"
                                        @change-order="onImageChangeOrder">                                            
                                    </input-images-upload>
                                </div>
                                
                                <div class="tab-pane step-content" v-show="this.step === 5">
                                    <div class="form-row m-b-10">
                                        <p>Highlight your business credentials to potential customers. Type in the search field below to find your relevant accreditations and associations.</p>
                                        <p>Once you are finished, click <strong>Continue</strong> to move on to the next step, or <strong>Save as Draft</strong> to make further edits later.</p>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <vue-simple-suggest
                                                ref="AccreditationAutoComplete"
                                                :list="accreditations"
                                                display-attribute="name"
                                                value-attribute="id"
                                                :max-suggestions=5
                                                :styles="autoCompleteStyles"
                                                :destyled=true
                                                :filter-by-query=true
                                                :prevent-submit=true
                                                @select="onCredentialSelect" placeholder="Search for accreditations here...">
                                            </vue-simple-suggest>
                                        </div>
                                        <div class="col-md-6">
                                            <vue-simple-suggest
                                                ref="AssociationAutoComplete"
                                                :list="associations"
                                                display-attribute="name"
                                                value-attribute="id"
                                                :max-suggestions=5
                                                :styles="autoCompleteStyles"
                                                :destyled=true
                                                :filter-by-query=true
                                                :prevent-submit=true
                                                @select="onCredentialSelect" placeholder="Search for associations here...">
                                            </vue-simple-suggest>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <ul class="category-list-group">
                                                <category-list-item
                                                  v-for="(item, index) in serviceAccreditations"
                                                  v-bind:item="item"
                                                  v-bind:key="index"
                                                  v-model="form.service_credentials"
                                                  v-bind:name="item.credential.name"
                                                ></category-list-item>
                                            </ul>
                                        </div>
                                        <div class="col-md-6">
                                            <ul class="category-list-group">
                                                <category-list-item
                                                  v-for="(item, index) in serviceAssociations"
                                                  v-bind:item="item"
                                                  v-bind:key="index"
                                                  v-model="form.service_credentials"
                                                  v-bind:name="item.credential.name"
                                                ></category-list-item>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="tab-pane step-content" v-show="this.step === 6">
                                    <form onsubmit="return false" name="stand-out-form">
                                        <div v-if="isPayableItemAvailable">
                                            
                                            <div class="form-row ad-option">
                                                <div class="col-auto">
                                                    <span class="btn btn-warning active btn-block no-shadow text-white" @click="togglePlanTo('${C.AD_PLAN_TYPE.SERVICE_1}', '${C.AD_TYPE.STANDARD}')">
                                                        <i class="fas fa-cog d-none d-xl-inline" aria-hidden="true"></i> Standard                                                
                                                    </span>
                                                </div>
                                                <div class="col">
                                                    <p>Your listing will be visible in search results only.</p>
                                                </div>
                                                <div class="col-auto" :class="{'col-md-3': isStandardPlanOnly()}">
                                                    <span class="label label-secondary">{{settings.validity['service-standard-days']}} days - {{settings.currency.symbol}}{{settings.prices['service-standard']}}</span>
                                                    <div class="switcher switcher-success" v-if="!isStandardPlanOnly()">
                                                        <input type="checkbox" name="ad_type" id="${prefix}plan-standard" value="${C.AD_PLAN_TYPE.SERVICE_1}-${C.AD_TYPE.STANDARD}" v-model="plan" @change="preventIfLessThanOneCheckedPlan">
                                                        <label for="${prefix}plan-standard"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-row ad-option" v-show="settings.plans['service_2-${C.AD_TYPE.STANDARD.toLowerCase()}']">
                                                <div class="col-auto hidden">
                                                    <span class="btn btn-warning active btn-block no-shadow text-white" @click="togglePlanTo('${C.AD_PLAN_TYPE.SERVICE_2}', '${C.AD_TYPE.STANDARD}')">
                                                        <i class="fas fa-cog d-none d-xl-inline" aria-hidden="true"></i> Standard                                                
                                                    </span>
                                                </div>
                                                <div class="col hidden">
                                                    <p>Your listing will be visible in search results only.</p>
                                                </div>
                                                <div class="col-auto">
                                                    <span class="label label-secondary">{{settings.validity['service_2-standard-days']}} days - {{settings.currency.symbol}}{{settings.prices['service_2-standard']}}</span>
                                                    <div class="switcher switcher-success">
                                                        <input type="checkbox" name="ad_type" id="${prefix}plan-standard2" value="${C.AD_PLAN_TYPE.SERVICE_2}-${C.AD_TYPE.STANDARD}" v-model="plan" @change="preventIfLessThanOneCheckedPlan">
                                                        <label for="${prefix}plan-standard2"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <hr/>
                                            
                                            <div class="form-row ad-option" v-show="settings.plans['service-${C.AD_TYPE.PREMIUM.toLowerCase()}']">
                                                <div class="col-auto">
                                                    <span class="btn btn-green active btn-block no-shadow text-white" @click="togglePlanTo('${C.AD_PLAN_TYPE.SERVICE_1}', '${C.AD_TYPE.PREMIUM}')">
                                                        <i class="fas fa-feather d-none d-xl-inline" aria-hidden="true"></i> Premium                                                
                                                    </span>
                                                </div>
                                                <div class="col">
                                                    <p>Your listing will be visible in search results, highlighted in gold. It will be be prioritised other standard listings, meaning that it always displays at the top of the search results, and will also be showcased in the sidebar.</p>
                                                </div>
                                                <div class="col-auto">
                                                    <span class="label label-secondary">{{settings.validity['service-premium-days']}} days - {{settings.currency.symbol}}{{settings.prices['service-premium']}}</span>
                                                    <div class="switcher switcher-success">
                                                        <input type="checkbox" name="ad_type" id="${prefix}plan-premium" value="${C.AD_PLAN_TYPE.SERVICE_1}-${C.AD_TYPE.PREMIUM}" v-model="plan" @change="preventIfLessThanOneCheckedPlan">
                                                        <label for="${prefix}plan-premium"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-row ad-option" v-show="settings.plans['service_2-${C.AD_TYPE.PREMIUM.toLowerCase()}']">
                                                <div class="col-auto" :class="{hidden: settings.plans['service-${C.AD_TYPE.PREMIUM.toLowerCase()}'] && settings.plans['service_2-${C.AD_TYPE.PREMIUM.toLowerCase()}']}">
                                                    <span class="btn btn-green active btn-block no-shadow text-white" @click="togglePlanTo('${C.AD_PLAN_TYPE.SERVICE_2}', '${C.AD_TYPE.PREMIUM}')">
                                                        <i class="fas fa-feather d-none d-xl-inline" aria-hidden="true"></i> Premium                                                
                                                    </span>
                                                </div>
                                                <div class="col" :class="{hidden: settings.plans['service-${C.AD_TYPE.PREMIUM.toLowerCase()}'] && settings.plans['service_2-${C.AD_TYPE.PREMIUM.toLowerCase()}']}">
                                                    <p>Your listing will be visible in search results, highlighted in gold. It will be be prioritised other standard listings, meaning that it always displays at the top of the search results, and will also be showcased in the sidebar.</p>
                                                </div>
                                                <div class="col-auto">
                                                    <span class="label label-secondary">{{settings.validity['service_2-premium-days']}} days - {{settings.currency.symbol}}{{settings.prices['service_2-premium']}}</span>
                                                    <div class="switcher switcher-success">
                                                        <input type="checkbox" name="ad_type" id="${prefix}plan-premium2" value="${C.AD_PLAN_TYPE.SERVICE_2}-${C.AD_TYPE.PREMIUM}" v-model="plan" @change="preventIfLessThanOneCheckedPlan">
                                                        <label for="${prefix}plan-premium2"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <hr/>
                                            
                                            <div class="form-row ad-option" v-show="settings.plans['service-${C.AD_TYPE.FEATURED.toLowerCase()}']">
                                                <div class="col-auto">
                                                    <span class="btn btn-primary active btn-block no-shadow text-white" @click="togglePlanTo('${C.AD_PLAN_TYPE.SERVICE_1}', '${C.AD_TYPE.FEATURED}')">
                                                        <i class="fas fa-gem d-none d-xl-inline" aria-hidden="true"></i> Featured                                                
                                                    </span>
                                                </div>
                                                <div class="col">
                                                    <p>Your listing will be visible in search results, highlighted in gold. It will be be prioritised other standard listings, meaning that it always displays at the top of the search results.</p>
                                                </div>
                                                <div class="col-auto">
                                                    <span class="label label-secondary">{{settings.validity['service-featured-days']}} days - {{settings.currency.symbol}}{{settings.prices['service-featured']}}</span>
                                                    <div class="switcher switcher-success">
                                                        <input type="checkbox" name="ad_type" id="${prefix}plan-featured" value="${C.AD_PLAN_TYPE.SERVICE_1}-${C.AD_TYPE.FEATURED}" v-model="plan" @change="preventIfLessThanOneCheckedPlan">
                                                        <label for="${prefix}plan-featured"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-row ad-option" v-show="settings.plans['service_2-${C.AD_TYPE.FEATURED.toLowerCase()}']">
                                                <div class="col-auto" :class="{hidden: settings.plans['service-${C.AD_TYPE.FEATURED.toLowerCase()}'] && settings.plans['service_2-${C.AD_TYPE.FEATURED.toLowerCase()}']}">
                                                    <span class="btn btn-primary active btn-block no-shadow text-white" @click="togglePlanTo('${C.AD_PLAN_TYPE.SERVICE_2}', '${C.AD_TYPE.FEATURED}')">
                                                        <i class="fas fa-gem d-none d-xl-inline" aria-hidden="true"></i> Featured                                                
                                                    </span>
                                                </div>
                                                <div class="col" :class="{hidden: settings.plans['service-${C.AD_TYPE.FEATURED.toLowerCase()}'] && settings.plans['service_2-${C.AD_TYPE.FEATURED.toLowerCase()}']}">
                                                    <p>Your listing will be visible in search results, highlighted in gold. It will be be prioritised other standard listings, meaning that it always displays at the top of the search results.</p>
                                                </div>
                                                <div class="col-auto">
                                                    <span class="label label-secondary">{{settings.validity['service_2-featured-days']}} days - {{settings.currency.symbol}}{{settings.prices['service_2-featured']}}</span>
                                                    <div class="switcher switcher-success">
                                                        <input type="checkbox" name="ad_type" id="${prefix}plan-featured2" value="${C.AD_PLAN_TYPE.SERVICE_2}-${C.AD_TYPE.FEATURED}" v-model="plan" @change="preventIfLessThanOneCheckedPlan">
                                                        <label for="${prefix}plan-featured2"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div v-else-if="form.ad_expiry_date">
                                            <div class="form-row">
                                                <div class="col">
                                                    <p class="m-t-10">I would like extend my current plan - <strong>{{form.ad_type}} (extend expiration date to {{getExtendedExpiryDate}})</strong></p>
                                                </div>
                                                <div class="col-auto">
                                                    <div class="switcher switcher-success">
                                                        <input type="checkbox" name="ad_type" id="${prefix}plan-prolonged" v-model="form.ad_extend">
                                                        <label for="${prefix}plan-prolonged"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div v-else>
                                            <div class="form-row">
                                                <div class="col">
                                                    <p class="m-t-10">Please complete the payment process that has been started.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                
                                <div class="tab-pane step-content" v-show="isPaymentPage()">
                                    <payment-form 
                                        ref="PaymentForm" 
                                        :user="user" 
                                        :settings="settings" 
                                        :active=isWorking>
                                    </payment-form>
                                </div>
                                
                                <div class="btn-toolbar sw-toolbar sw-toolbar-bottom panel-footer m-t-15">
                                    <div class="row w-100 responsive-toolbar-wrapper">
                                        <div class="col-md-4 p-l-0 m-b-10">
                                            <button type="button" class="btn btn-warning btn-large" :aria-disabled="isWorking" :disabled="isWorking" @click.prevent="onSaveDraft" v-show="!isEditMode && !isPaymentPage()">
                                                <i class="fas fa-save" aria-hidden="true" v-show="!isWorking"></i>
                                                <i class="fas fa-cog fa-spin" aria-hidden="true" v-show="isWorking"></i>
                                                Save as Draft
                                            </button>
                                            
                                            <button type="button" class="btn btn-secondary btn-large" @click.prevent="prev" v-show="isPaymentPage() && !isWorking && !disableBackToEdit">
                                                Back to edit
                                            </button>
                                        </div>     
                                        <div class="col-md-8 p-r-0 m-b-10 text-right">                                           
                                            <button type="button" class="btn btn-large" :class="{'btn-primary': isPaymentPage(), 'btn-secondary': !buttonSubmit.green && !isPaymentPage(), 'btn-green': buttonSubmit.green && !isPaymentPage()}" :aria-disabled="isWorking" :disabled="isWorking" @click.prevent="onSave">
                                                <i class="fas" aria-hidden="true" :class="buttonSubmit.icons" v-show="!isWorking"></i>                                                    
                                                <i class="fas fa-cog fa-spin" aria-hidden="true" v-show="isWorking"></i>
                                                {{buttonSubmit.label}}
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row w-100">
                                        <transition name="fade">
                                            <div class="alert d-inline result" :class="{'text-danger': hasError, 'text-success': !hasError }" role="alert" v-show="message !== ''">{{message}}</div>
                                        </transition>
                                    </div>
                                </div>
                            </div>
                            
				        </div>
                    </div>
                    <div class="col-lg-12 col-xl-4">
                            <div class="row">
                                <div class="col-lg-6 col-xl-12">
                                    <payment-summary ref="PaymentSummary" @change="onSummaryChange" :settings="settings" :list="payments" v-model="discountCode" :hide-empty=true></payment-summary>
                                </div>
                                <div class="col-lg-6 col-xl-12">
                                    <latest-businesses-added></latest-businesses-added>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;
    }
}
