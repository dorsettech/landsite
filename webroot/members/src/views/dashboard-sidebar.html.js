export default {
    render(PROFILE_IMAGE_DEFAULT) {
        return `
            <div id="sidebar" class="sidebar pretty-scrollbar v-comp-dashboard-sidebar">
                <ul class="nav">
					<li class="nav-profile">
						<a href="#/profile" data-toggle="nav-profile">
							<div class="image">
							    <img v-if="user.image !== null && user.image !== ''" :src="profileImagePath" :alt="user.full_name" @error="onImageSrcError">                
                                <img v-else src="${PROFILE_IMAGE_DEFAULT}" :alt="user.full_name">
							</div>
							<div class="info">
								{{user.full_name}}
								<small v-if="user.details.company">{{user.details.company}}</small>
								<small v-else="user.email">{{user.email}}</small>
							</div>
						</a>
					</li>
				</ul>
                <ul class="nav">
					<li class="has-sub yellow expand active" @click="onToggleMenu($event)">					
						<a>
							<b class="caret"></b>
							<span>GENERAL</span>
						</a>
						<ul class="sub-menu">
							<li>
							    <router-link to="/" title="Dashboard">
							        <i class="fas fa-th-large"></i>
							        <span>Dashboard</span>
                                </router-link>							    
							</li>
							<li>
                                <router-link to="/profile" title="My Details">
                                    <i class="fas fa-user-edit"></i>
                                    <span>My Details</span>
                                </router-link>							        
                            </li>
                            <li>
                                <router-link to="/alerts-searches?refresh=1" title="Alerts & Searches">
                                    <i class="fas fa-bell"></i>
                                    <span>Alerts & Searches</span>
                                </router-link>
                            </li>
                            <li>
                                <router-link to="/marketing-preferences" title="Marketing Preferences">
							        <i class="fas fa-bullhorn"></i>
							        <span>Marketing Preferences</span>
                                </router-link>
                            </li>
						</ul>
					</li>
					<li class="has-sub blue expand active" @click="onToggleMenu($event)">					
						<a>
							<b class="caret"></b>
							<span>PROPERTIES</span>
						</a>
						<ul class="sub-menu">
							<li>
							    <router-link to="/properties?refresh=1" title="My Properties">
							        <i class="fas fa-building"></i>
							        <span>My Properties</span>
                                </router-link>
							</li>
							<li>
							    <router-link to="/add-property" title="Add a Property">
							        <i class="fas fa-plus-circle"></i>
							        <span>Add a Property</span>
                                </router-link>
                            </li>
                            <li>
                                <router-link to="/saved-properties?refresh=1" title="Saved Properties">
							        <i class="fas fa-save"></i>
							        <span>Saved Properties</span>
                                </router-link>
                            </li>
						</ul>
					</li>
					<li class="has-sub red expand active" @click="onToggleMenu($event)">					
						<a>
							<b class="caret"></b>
							<span>BUSINESS PROFILE</span>
						</a>
						<ul class="sub-menu">
							<li>
							    <router-link to="/business-listing" title="My Business Profile">
							        <i class="fas fa-business-time"></i>
							        <span>My Business Profile</span>
                                </router-link>
							</li>
							<li>
							    <router-link to="/business-stats?refresh=1" title="My Business Stats">
							        <i class="fas fa-chart-line"></i>
							        <span>My Business Stats</span>
                                </router-link>
							</li>
							<li>
							    <router-link to="/case-studies?refresh=1" title="My Case Studies">
							        <i class="far fa-file-alt"></i>
							        <span>My Case Studies</span>
                                </router-link>
                            </li>
                            <li>
                                <router-link to="/case-study" title="Add a Case Study">
							        <i class="fas fa-plus-circle"></i>
							        <span>Add a Case Study</span>							        
                                </router-link>
                            </li>
                            <li>
                                <router-link to="/insights?refresh=1" title="My News Posts">
							        <i class="far fa-newspaper"></i>
							        <span>My News Posts</span>
                                </router-link>
                            </li>
                            <li>
                                <router-link to="/insight" title="Add a News Post">
							        <i class="fas fa-plus-circle"></i>
							        <span>Add a News Post</span>							        
                                </router-link>
                            </li>
                            <li>
                                <router-link to="/saved-businesses?refresh=1" title="Saved Businesses">
							        <i class="fas fa-save"></i>
							        <span>Saved Businesses</span>							        							        
                                </router-link>
                            </li>
						</ul>
					</li>
					<li class="has-sub green expand active" @click="onToggleMenu($event)">					
						<a>
							<b class="caret"></b>
							<span>NEWS</span>
						</a>
						<ul class="sub-menu">
							<li>
							    <router-link to="/preferred-insights" title="Preferred News">
							        <i class="fas fa-star"></i>
							        <span>Preferred News</span>							        							        							        
                                </router-link>
							</li>
							<li>
							    <router-link to="/recent-insights" title="Recent News">
							        <i class="fas fa-meteor"></i>
							        <span>Recent News</span>							        							        							        
                                </router-link>
                            </li>
                            <li>
                                <router-link to="/insight?contribute=1" title="Contribute">
							        <i class="fas fa-brain"></i>
							        <span>Contribute</span>
                                </router-link>
                            </li>
						</ul>
					</li>
					<li class="has-sub expand active">
					    <ul class="sub-menu">
					        <li>
                                <a href="/logout" class="logout" title="Logout">
                                    <i class="fas fa-sign-out-alt"></i> 
                                    <span>Logout</span>    
                                </a>
                            </li>
                        </ul>
                    </li>
				</ul>
            </div>
        `;
    }
}
