import C from './../config/constants';

export default {
    render() {
        return `
            <div class="v-comp-module-body v-comp-saved-businesses">
                <h1 class="page-header">Saved Businesses</h1>
                <h6 class="page-description">
                    Here you can view the businesses you have saved, view their details and enquire directly to the owner.
                </h6>                
                <div class="row position-relative">
                    <loader :active=loading></loader>
                    <div class="col-lg-12 col-xl-8">
                        <p class="alert alert-secondary text-center no-data" role="alert" v-show="items.length === 0">No saved businesses found</p>
                        <business-list-item
                            v-for="(item, index) in items"
                            v-bind:item="item"
                            v-bind:index="index"
                            v-bind:key="item.id"
                            :chart=false
                            :buttons="{ main: true, stats: false, edit: false, delete: true, view: item.isAvailable }"
                            :message="item.message"
                            :custom-button="{icon: 'fa-envelope', text: 'Enquire today', action: 'enquiry', disabled: !item.isAvailable}"
                            :custom-status-names="{ ${C.STATUS.PUBLISHED}: item.category.name, ${C.STATUS.EXPIRED}: item.category.name }"
                            custom-status-class="status-orange"
                            date-label="Saved on"             
                            :cost=false               
                            :currency-symbol="settings.currency.symbol"
                            @action="action">
                        </business-list-item>
                    </div>
                    <div class="col-lg-12 col-xl-4">
                        <latest-businesses-added></latest-businesses-added>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <pagination :params="pagination" @page-change="onPageChange"></pagination>
                    </div>
                </div>
            </div>
        `;
    }
}
