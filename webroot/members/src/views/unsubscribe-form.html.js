import C from '../config/constants';

export default {
    render() {
        return `
<div class="account">
    <div class="container">
        <div class="row justify-content-center">
            <div class="logo col">
                <a href="//${C.URL.WWW}">
                    <img src="/members/assets/logo.png" alt="logo" class="img-fluid">
                </a>
            </div>
        </div>
    </div>
</div>`;
    }
}
