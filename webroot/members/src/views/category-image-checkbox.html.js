export default {
    render(IMAGE_DEFAULT) {
        return `
        <div class="v-comp-category-image-checkbox form-check col-12 col-sm-6 col-xl-3">
            <input type="checkbox" class="custom-control-input" :value="item.id" :id="id" @change="onCheckChange" :checked="checked">
            <label class="form-check-label" :for="id">
                <img v-if="item.image !== null && item.image !== ''" :src="item.image" :alt="item.name" @error="onImageSrcError">                
                <img v-else src="${IMAGE_DEFAULT}" :alt="item.name">
                <span>{{item.name}}</span>
            </label>
        </div>`;
    }
}
