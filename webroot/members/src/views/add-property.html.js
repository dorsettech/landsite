import Constants from '../config/constants';

export default {
    render() {
        return `
            <div class="v-comp-module-body v-comp-add-property">
                <h1 class="page-header">Add a Property</h1>
                <h6 class="page-description">
                    Share your commercial property with a whole community of potential buyers and investors. Simply choose from the options below and create your property listing.
                </h6>                
                <div class="row">
                    <div class="col-lg-12 col-xl-8">
                        <div class="panel">
                            <div class="panel-body p-30">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6 col-xl-3 m-b-20">
                                        <div class="sale-box">
                                            <img src="/members/assets/for-sale.png" alt="For Sale" class="img-fluid">
                                            <h5 class="title">Add a property for sale</h5>
                                            <p class="desc">Create a property listing and connect directly with potential buyers and investors in your area.</p>
                                            <a class="btn btn-large btn-primary" @click="add('${Constants.PROPERTY_PURPOSE.SALE}')">ADD NOW</a>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-xl-3 m-b-20">
                                        <div class="sale-box">
                                            <img src="/members/assets/for-rent.png" alt="For Rent" class="img-fluid">
                                            <h5 class="title">Add a property for rent</h5>
                                            <p class="desc">Create a property listing and connect directly with potential tenants in your area.</p>
                                            <a class="btn btn-large btn-primary" @click="add('${Constants.PROPERTY_PURPOSE.RENT}')">ADD NOW</a>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-xl-6 m-b-20">
                                        <div class="sale-box">
                                            <div class="row">
                                                <div class="col">
                                                    <img src="/members/assets/for-sale.png" alt="For Sale" class="img-fluid">                                                
                                                </div>
                                                <div class="col-auto and">AND</div>
                                                <div class="col">
                                                    <img src="/members/assets/for-rent.png" alt="For Rent" class="img-fluid">
                                                </div>
                                            </div>
                                            <h5 class="title">Add a property for sale and for rent</h5>
                                            <p class="desc">Create a property listing and connect directly with a community of potential investors and tenants.</p>
                                            <a class="btn btn-large btn-primary" @click="add('${Constants.PROPERTY_PURPOSE.BOTH}')">ADD NOW</a>
                                        </div>
                                    </div>     
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-4">
                        <latest-businesses-added></latest-businesses-added>
                    </div>
                </div>
            </div>
            `;
    }
}
