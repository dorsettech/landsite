export default {
    render() {
        return `
        <li class="v-comp-category-list-item">
            <span>{{name}}</span> <a @click.prevent="onDeleteClick"><i class="fas fa-minus-circle"></i></a>
        </li>`;
    }
}
