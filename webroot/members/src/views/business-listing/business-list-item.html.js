export default {
    render() {
        return `
            <div class="panel panel-default v-comp-panel-list-item v-comp-business-list-item">
                <div class="panel-body">
                    <div class="row position-relative">
                        <div class="col-md-3 photo">
                            <div class="image" :style="imageStyle">
                                <span class="cost" v-if="cost">Cost: {{currencySymbol}}{{item.ad_cost.toLocaleString()}}</span>
                                <span class="status" :class="statusCls">{{statusText}}</span>
                            </div>
                        </div>
                        <div class="col-md details">
                            <h3 class="title">{{item.company}}</h3>
                            <span class="date">{{dateLabel}}: <time :datetime="item.created">{{dateAdded}}</time></span>
                            <hr/>
                            <p class="headline">{{item.description_short}}</p>
                            <div class="buttons">
                                <a class="btn btn-large btn-secondary action" :class="statusCls" @click="onButtonClick('action')" v-if="buttons.main" :disabled="button.disabled">
                                    <i class="fas" :class="button.iconCls"></i>  
                                    {{button.text}}
                                </a>
                                <a v-tooltip.top="'Business stats'" class="btn btn-icon-fa chart" @click="onButtonClick('stats')" v-if="buttons.stats">
                                    <span class="fa-stack fa-2x">
                                        <i class="far fa-circle fa-stack-2x"></i>
                                        <i class="fas fa-chart-line fa-stack-1x"></i>
                                    </span>
                                </a>
                                <a v-tooltip.top="'Edit'" class="btn btn-icon-fa edit" @click="onButtonClick('edit')" v-if="buttons.edit">
                                    <span class="fa-stack fa-2x">
                                        <i class="far fa-circle fa-stack-2x"></i>
                                        <i class="fas fa-edit fa-stack-1x"></i>
                                    </span>
                                </a>
                                <a v-tooltip.top="'View business'" class="btn btn-icon-fa view" @click="onButtonClick('view')" v-if="buttons.view">
                                    <span class="fa-stack fa-2x">
                                        <i class="far fa-circle fa-stack-2x"></i>
                                        <i class="fas fa-eye fa-stack-1x"></i>
                                    </span>
                                </a>
                                <a v-tooltip.top="'Delete'" class="btn btn-icon-fa delete" @click="onButtonClick('delete')" v-if="buttons.delete">
                                    <span class="fa-stack fa-2x">
                                        <i class="far fa-circle fa-stack-2x"></i>
                                        <i class="fas fa-trash-alt fa-stack-1x"></i>
                                    </span>
                                </a>                                
                            </div>
                            <p class="text-danger f-w-600" v-if="message">{{message}}</p>
                        </div>
                        <div class="col-md-3 stats" v-if="chart">
                            <!-- CHART HERE -->
                        </div>
                    </div>
                </div>            
            </div>
        `;
    }
}
