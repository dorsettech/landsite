export default {
    render() {
        return `
            <div class="form-row w-100 m-b-10 m-l-10 v-comp-opening-hours">
                <div class="col-sm-12 col-md-auto">
                    <label :for="prefix + 'opening_time_from'" class="col-form-label">From</label>
                    <flat-pickr v-model="item.from" :config="timePickerOptions" class="form-control" @on-close="onTimeClose('from', ...arguments)"></flat-pickr>
                </div>
                <div class="col-sm-12 col-md-auto">
                    <label :for="prefix + 'opening_time_to'" class="col-form-label">to</label>
                    <flat-pickr v-model="item.to" :config="timePickerOptions" class="form-control" @on-close="onTimeClose('to', ...arguments)"></flat-pickr>
                </div>
                <div class="col m-l-20">
                    <div class="checkbox checkbox-css checkbox-inline">
                        <input type="checkbox" :id="prefix + 'opening_days_0'" v-model="item.days" value="0"/>
                        <label :for="prefix + 'opening_days_0'">Monday</label>
                    </div>
                    <div class="checkbox checkbox-css checkbox-inline">
                        <input type="checkbox" :id="prefix + 'opening_days_1'" v-model="item.days" value="1"/>
                        <label :for="prefix + 'opening_days_1'">Tuesday</label>
                    </div>
                    <div class="checkbox checkbox-css checkbox-inline">
                        <input type="checkbox" :id="prefix + 'opening_days_2'" v-model="item.days" value="2"/>
                        <label :for="prefix + 'opening_days_2'">Wednesday</label>
                    </div>
                    <div class="checkbox checkbox-css checkbox-inline">
                        <input type="checkbox" :id="prefix + 'opening_days_3'" v-model="item.days" value="3"/>
                        <label :for="prefix + 'opening_days_3'">Thursday</label>
                    </div>
                    <div class="checkbox checkbox-css checkbox-inline">
                        <input type="checkbox" :id="prefix + 'opening_days_4'" v-model="item.days" value="4"/>
                        <label :for="prefix + 'opening_days_4'">Friday</label>
                    </div>
                    <div class="checkbox checkbox-css checkbox-inline">
                        <input type="checkbox" :id="prefix + 'opening_days_5'" v-model="item.days" value="5"/>
                        <label :for="prefix + 'opening_days_5'">Saturday</label>
                    </div>
                    <div class="checkbox checkbox-css checkbox-inline">
                        <input type="checkbox" :id="prefix + 'opening_days_6'" v-model="item.days" value="6"/>
                        <label :for="prefix + 'opening_days_6'">Sunday</label>
                    </div>
                </div>
                <div class="col-auto" v-if="button">
                    <a class="btn btn-green btn-icon btn-circle btn-sm add" @click="onButtonClick('add')" v-if="button === '+'">
                        <i class="fas fa-plus"/>
                    </a>
                    <a class="btn btn-danger btn-icon btn-circle btn-sm delete" @click="onButtonClick('delete')" v-if="button === '-'">
                        <i class="fas fa-times"/>
                    </a>
                </div>
            </div>
        `;
    }
}
