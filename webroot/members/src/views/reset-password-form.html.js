import C from '../config/constants';

export default {
    render() {
        return `
<div class="account">
    <div class="container">
        <div class="row justify-content-center">
            <div class="logo col">
                <a href="//${C.URL.WWW}">
                    <img src="/members/assets/logo.png" alt="logo" class="img-fluid">
                </a>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 col-xxl-4">
                <div class="login accordion">
                    <div class="content">
                        <div class="heading">Reset your password</div>
                        <form class="v-comp-reset-password-form" @submit.prevent="onSubmit" v-if="token">
                            <div class="form-row">
                                <p class="info">Enter your new password in the form below</p>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-12 col-sm-6 col-md-12 col-lg-6">
                                    <input type="password" class="form-control"
                                    name="password" 
                                    :class="{'is-invalid': form.password.isInvalid}" 
                                    v-model.trim="form.password.value" 
                                    @change="onChange('password', $event)" 
                                    @invalid.prevent="form.password.isInvalid=true" placeholder="New Password *" minlength="8" required>
                                    <div class="invalid-feedback">New Password is required</div>
                                </div>
                                <div class="form-group col-12 col-sm-6 col-md-12 col-lg-6">
                                    <input type="password" class="form-control"
                                    name="confirm_password"
                                    :class="{'is-invalid': form.confirm_password.isInvalid}"
                                    v-model.trim="form.confirm_password.value" 
                                    @change="onChange('confirm_password', $event)"
                                    @invalid.prevent="form.confirm_password.isInvalid=true" placeholder="Re-type new password *" minlength="8" required>
                                    <div class="invalid-feedback">Password and confirm password does not match</div>
                                </div>
                                <div class="form-group col-12">
                                    <button type="submit" class="login btn btn-primary" :aria-disabled="isWorking" :disabled="isWorking">Submit</button>
                                </div>
                                <transition name="fade">
                                    <div class="form-group col-12" v-show="message">
                                        <div class="alert text-center" :class="{'text-danger': hasError, 'text-success': !hasError }" role="alert">{{message}}</div>
                                    </div>
                                </transition>
                                <div class="signin col-12">
                                    Try to sign-in <a href="/login">Sign-in now</a>
                                </div>
                            </div>
                        </form>
                        <p v-else>
                            Sorry, but your reset password token has expired. <br/><br/>
                            Go to <a class="text-danger" href="/forgot-password">Forgot Password</a> page and request new token.
                        </p>
                        <img src="/members/assets/image.jpg" class="step1image">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`;
    }
}
