export default {
    render() {
        return `
            <div class="v-comp-module-body v-comp-articles">
                <h1 class="page-header">News List</h1>
                <h6 class="page-description">
                    Below is a list of the news posts you have contributed to The Landsite’s online information centre. From here, you can view, edit and delete your posts.
                </h6>                
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-body p-30">
                                <loader :active=loading></loader>
                                <div class="row">
                                    <articles-datagrid-toolbar
                                        v-model="params"
                                        add-url="#/insight"
                                        add-label="Add a News Post"
                                        v-on:refresh="refresh"
                                        ref="Toolbar"
                                    ></articles-datagrid-toolbar>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <vuetable ref="datagrid"
                                            api-url="/Articles"
                                            :reactive-api-url=false
                                            :sort-order="sortOrder"
                                            :fields="columns"
                                            :per-page="perPage"
                                            :append-params="params"
                                            data-path="records"			                        
                                            pagination-path="pagination"
                                            :css="tableCls"
                                            @vuetable:pagination-data="onPaginationData"
                                            @vuetable:loading="onLoading"
                                            @vuetable:loaded="onLoaded">
                                            <div slot="status" slot-scope="props">
                                                <status-tooltip :content="formatStatus(props.rowData)"></status-tooltip>                                                
                                            </div>
                                            <div slot="actions" slot-scope="props" class="actions">
                                                <a v-tooltip.top="'View'" class="action" @click="onActionClick('view', props.rowData)" v-if="isVisibleOnWebsite(props.rowData)">
                                                    <i class="fas fa-eye"></i>
                                                </a>
                                                <a v-tooltip.top="'Edit'" class="action" @click="onActionClick('edit', props.rowData)">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                                <a v-tooltip.top="'Delete'" class="action text-landsite-red" @click="onActionClick('delete', props.rowData)">
                                                    <i class="fas fa-trash-alt"></i>
                                                </a>
                                            </div>
                                            <div slot="visibility" slot-scope="props">
                                                <div class="switcher switcher-success">
                                                    <input type="checkbox" name="visibility" :id="props.rowData.id + '-insight'" v-model="props.rowData.visibility" @change="onActionClick('visibility', props.rowData)">
                                                    <label :for="props.rowData.id + '-insight'"></label>
                                                </div>
                                            </div>
                                        </vuetable>
                                    </div>
                                </div>
                                <div class="row m-t-10">
                                    <div class="col-md-12">
                                        <vuetable-pagination ref="pagination"
                                            @vuetable-pagination:change-page="onChangePage"
                                            :css="paginationCls">
                                        </vuetable-pagination>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                
        `;
    }
}
