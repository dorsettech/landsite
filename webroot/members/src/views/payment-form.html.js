import _ from '../utils/utilities';
import C from '../config/constants';

export default {
    render() {
        let prefix = `${_.guid()}-`;
        return `
            <form onsubmit="return false" class="v-comp-payment position-relative" :name="formName$">
                <h4>Billing details</h4>
                <div class="form-row">
                    <hr/>
                </div>                
                <div class="form-row">
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group m-b-10 p-t-5">
                            <label for="${prefix}b-first-name">First Name</label>
                            <input v-model.trim="billing_details.first_name" class="form-control" type="text" id="${prefix}b-first-name" name="billing_first_name" 
                                maxlength="100"
                                @change="onInputChange" 
                                @invalid.prevent="onInputInvalid"
                                required>
                            <div class="invalid-feedback">This field is required</div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group m-b-10 p-t-5">
                            <label for="${prefix}b-last-name">Last Name</label>
                            <input v-model.trim="billing_details.last_name" class="form-control" type="text" id="${prefix}b-last-name" name="billing_last_name"
                                maxlength="100"
                                @change="onInputChange" 
                                @invalid.prevent="onInputInvalid"
                                required>
                            <div class="invalid-feedback">This field is required</div>                                                
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group m-b-10 p-t-5">
                            <label for="${prefix}b-company">Company Name</label>
                            <input v-model.trim="billing_details.company" class="form-control" type="text" id="${prefix}b-company" name="billing_company" maxlength="255">
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group m-b-10">
                            <label for="${prefix}b-email">Email</label>
                            <input v-model.trim="billing_details.email" class="form-control" type="email" id="${prefix}b-email" name="billing_email"
                                maxlength="100"
                                @change="onInputChange" 
                                @invalid.prevent="onInputInvalid"
                                required>
                            <div class="invalid-feedback">This field is required</div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group m-b-10">
                            <label for="${prefix}b-phone">Phone Number</label>
                            <input v-model.trim="billing_details.phone" class="form-control" type="text" id="${prefix}b-phone" name="billing_phone" 
                                maxlength="50"
                                @change="onInputChange" 
                                @invalid.prevent="onInputInvalid"
                                required>
                            <div class="invalid-feedback">This field is required</div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group m-b-10">
                            <label for="${prefix}b-postcode">Postcode</label>
                            <input v-model.trim="billing_details.postcode" class="form-control" type="text" id="${prefix}b-postcode" name="postcode" required 
                            maxlength="25"
                            @change="onInputChange($event, 'postcode')" 
                            @invalid.prevent="onInputInvalid">
                            <div class="input-feedback" :class="{ 'invalid-feedback': !isPostcodeSuccess, 'valid-feedback': isPostcodeSuccess}" v-show="postcodeMsg !== ''">{{postcodeMsg}}</div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12">
                        <div class="form-group m-b-10">
                            <label for="${prefix}b-address">Address</label>
                            <input v-model="billing_details.address" class="form-control" type="text" id="${prefix}b-address" name="billing_address"
                                maxlength="255"
                                @change="onInputChange" 
                                @invalid.prevent="onInputInvalid"
                                required>
                            <div class="invalid-feedback">This field is required</div>
                        </div>
                    </div>
                </div>
                
                <h4 class="m-t-20">Payment Method</h4>
                <div class="form-row">
                    <hr/>
                </div>              
                <div class="form-row p-l-15">
                    <div class="radio radio-css">
                        <input type="radio" name="payment_type" id="${prefix}payment-type-stripe" value="${C.PAYMENT_TYPE.STRIPE}" v-model="payment.type">
                        <label for="${prefix}payment-type-stripe">Pay by Stripe</label>
                    </div>
                </div>
                <div class="form-row p-l-30" v-show="payment.type === '${C.PAYMENT_TYPE.STRIPE}'">
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group m-t-20 m-b-10">
                            <label for="${prefix}payment-stripe-card-element">
                                Enter credit or debit card details
                                <img src="/members/assets/stripe.png" class="stripe-logo" alt="Powered by Stripe"/>
                            </label>
                            <div class="payment-stripe-card-element" id="${prefix}payment-stripe-card-element"></div>
                            <div class="alert" :class="{'text-danger': serviceHasError$, 'text-success': !serviceHasError$ }" role="alert" v-show="serviceErrorMsg$ !== ''">{{serviceErrorMsg$}}</div>
                        </div>
                    </div>
                </div>
                <div class="form-row p-l-15 m-t-15 m-b-30">
                    <div class="radio radio-css">
                        <input type="radio" name="payment_type" id="${prefix}payment-type-order" value="${C.PAYMENT_TYPE.ORDER}" v-model="payment.type">
                        <label for="${prefix}payment-type-order">Pay by Purchase Order</label>
                    </div>
                </div>  
                <div class="form-row p-l-30" v-show="payment.type === '${C.PAYMENT_TYPE.ORDER}'">
                    <div class="col-12">
                        <div class="form-group m-b-10">
                            <label for="${prefix}payment-purchase-order-number">Purchase Order Number</label>
                            <input v-model="payment.purchase_order_number" class="form-control" type="text" id="${prefix}payment-purchase-order-number" name="purchase_order_number"
                                maxlength="255"
                                @change="onInputChange" 
                                @invalid.prevent="onInputInvalid"
                                :required="payment.type === '${C.PAYMENT_TYPE.ORDER}'"
                                placeholder="ie. RM-12345-678-90">
                            <div class="invalid-feedback">This field is required</div>
                        </div>
                    </div>
                </div>
                <progress-bar-indeterminate :active=active></progress-bar-indeterminate>  
            </form>
        `;
    }
}
