export default {
    render(consts) {
        return `
            <div class="v-comp-latest-businesses-added panel panel-ls">
                <loader :active=loading></loader>
                <div class="panel-heading text-center">
                    <h4 class="panel-title">Latest Businesses Added</h4>
                </div>
                <div class="panel-body">
                    <ul class="list">
                        <li v-for="item in list" :item="item" :key="item.id">
                            <div class="logo" :style="getLogoStyle(item)"></div>
                            <div class="info">
                                <h5 class="title">{{item.company}}</h5>
                                <p class="desc">{{item.description_short}}</p>
                                <a :href="getBusinessUrl(item)">View business profile</a>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="panel-footer gray-bg text-center">
                    <a href="//${consts.URL.WWW_SERVICES}">View all businesses profiles</a>
                </div>
            </div>
        `;
    }
}
