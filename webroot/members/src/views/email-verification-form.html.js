export default {
    render() {
        return `
<div class="account">
    <div class="container">
        <div class="row justify-content-center">
            <div class="logo col">
                <a href="/">
                    <img src="/members/assets/logo.png" alt="logo" class="img-fluid">
                </a>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 col-xxl-4">
                <div class="login accordion">
                    <div class="content">
                        <div class="heading">Verify your email</div>
                            <form class="v-comp-email-verification-form" @submit.prevent="onSubmit">
                                <div class="form-row" v-if="active">
                                    <p class="info">Enter your account e-mail address and we will send you a link to verify your address.</p>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-12" v-if="active">
                                        <input type="email" class="form-control" 
                                        name="email"
                                        :class="{'is-invalid': form.email.isInvalid}" 
                                        v-model.trim="form.email.value" 
                                        @change="onChange('email', $event)" 
                                        @invalid.prevent="form.email.isInvalid=true" placeholder="Account Email *" required>
                                        <div class="invalid-feedback">Please enter a valid email address</div>
                                    </div>
                                    <div class="form-group col-12" v-if="active">
                                        <button type="submit" class="login btn btn-primary" :aria-disabled="isWorking" :disabled="isWorking">Re-send</button>
                                    </div>
                                    <transition name="fade">
                                        <div class="form-group col-12" v-show="message">
                                            <div class="alert text-center" :class="{'text-danger': hasError, 'text-success': !hasError }" role="alert">{{message}}</div>
                                        </div>
                                    </transition>
                                    <div class="signin col-12">
                                        Try to sign-in <a href="/login">Sign-in now</a>
                                    </div>
                                </div>
                            </form>
                        <img src="/members/assets/image.jpg" class="step1image">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`;
    }
}
