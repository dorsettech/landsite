export default {
    render() {
        return `<transition name="fade">
            <div class="v-comp-loader" v-if="isActive" :aria-busy="isActive" aria-label="Loading">
                <div class="loading-image">
                    <svg class="circular">
                        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
                    </svg>
                </div>
            </div>
        </transition>`;
    }
}
