import consts from '../config/constants';
import Utils from '../utils/utilities';

const PROFILE_IMAGE_DEFAULT = consts.PROFILE_IMAGE_DEFAULT;

export default {
    render() {
        let prefix = `${Utils.guid()}-`;
        return `
            <div class="v-comp-module-body v-comp-profile">
                <h1 class="page-header" v-if="welcome">Welcome</h1>
                <h1 class="page-header" v-else>My Details</h1>
                <h6 class="page-description" v-if="welcome">
                    Get started on your profile by entering your details to the form below. Once all of your information has been added, upload a profile photo or your company logo and click Save.
                </h6>
                <h6 class="page-description" v-else>
                    Here you can view and edit your account details, including your email address, billing address and phone number.
                </h6>                
                <div class="row">
                    <div class="col-lg-12 col-xl-8">
                        <div class="panel">
                            <div class="panel-body p-30">
                                <form @submit.prevent="onSubmit" name="profile-form">
                                    <div class="form-row m-b-10">
                                        <div class="col-md-6">
                                            <input-image-upload
                                                v-model="profile.image"
                                                default-image-path="${PROFILE_IMAGE_DEFAULT}"
                                                :image-url="getImageUrl"
                                                id="${prefix}image"
                                                name="image" 
                                                label="Profile Image / Company Logo"
                                                v-on:success="onImageSuccess"
                                                v-on:fail="onImageFail"
                                            ></input-image-upload>
                                            <div class="input-feedback text-center" :class="{ 'invalid-feedback': !isImageSuccess, 'valid-feedback': isImageSuccess}" v-show="imageMsg !== ''">{{imageMsg}}</div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group m-b-10 p-t-5">
                                                <label for="${prefix}first-name">First Name</label>
                                                <input v-model.trim="profile.first_name" class="form-control" type="text" id="${prefix}first-name" name="first_name" required
                                                    maxlength="100"
                                                    @change="onInputChange" 
                                                    @invalid.prevent="onInputInvalid">
                                                <div class="invalid-feedback">This field is required</div>
                                            </div>
                                            <div class="form-group m-b-10 p-t-5">
                                                <label for="${prefix}last-name">Last Name</label>
                                                <input v-model.trim="profile.last_name" class="form-control" type="text" id="${prefix}last-name" name="last_name" required
                                                    maxlength="100"
                                                    @change="onInputChange" 
                                                    @invalid.prevent="onInputInvalid">
                                                <div class="invalid-feedback">This field is required</div>                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <div class="form-group m-b-10">
                                                <label for="${prefix}email">Email</label>
                                                <input v-model.trim="profile.email" class="form-control" type="email" id="${prefix}email" name="email" required
                                                    maxlength="100"
                                                    @change="onInputChange" 
                                                    @invalid.prevent="onInputInvalid">
                                                <div class="invalid-feedback">This field is required</div>
                                            </div>
                                            <div class="form-group m-b-10">
                                                <label for="${prefix}company">Company</label>
                                                <input v-model.trim="profile.details.company" class="form-control" type="text" id="${prefix}company" name="company"
                                                maxlength="255">
                                            </div>
                                        </div>
                                        <div class="col-md-6">                                            
                                            <div class="form-group m-b-10">
                                                <label for="${prefix}phone">Phone Number</label>
                                                <input v-model.trim="profile.phone" class="form-control" type="text" id="${prefix}phone" name="phone"
                                                    maxlength="50"
                                                    @change="onInputChange" 
                                                    @invalid.prevent="onInputInvalid">
                                                <div class="invalid-feedback">This field is required</div>
                                            </div>
                                            <div class="form-group m-b-10">
                                                <crafty-click 
                                                    v-model="profile.details.postcode"
                                                    v-on:success="onPostcodeSuccess"
                                                    v-on:fail="onPostcodeFail"
                                                    :api-key="settings.api['crafty-click']"
                                                    :use-cache=true
                                                    id="${prefix}postcode" 
                                                    label="Postcode" 
                                                    name="postcode"
                                                    :required=false>
                                                </crafty-click>
                                                <div class="input-feedback" :class="{ 'invalid-feedback': !isPostcodeSuccess, 'valid-feedback': isPostcodeSuccess}" v-show="postcodeMsg !== ''">{{postcodeMsg}}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row" v-show="addresses.length">
                                        <div class="col-md-12">
                                            <div class="form-group m-b-10">
                                                <label>Please select an address</label>
                                                <div class="input-group">
                                                    <select class="form-control" v-model="address" @change="onAddressSelect">
                                                        <option v-for="item in addresses" :item="item" :key="item.id">{{item.text}}</option>
                                                    </select>
                                                    <div class="input-group-append">
                                                        <button type="button" class="btn btn-default action" @click="onPostcodeCancel">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-12">
                                            <div class="form-group m-b-10">
                                                <label for="${prefix}address">Address</label>
                                                <input v-model="profile.details.address" class="form-control" type="text" id="${prefix}address" name="address" 
                                                    maxlength="255"
                                                    @change="onInputChange" 
                                                    @invalid.prevent="onInputInvalid">
                                                <div class="invalid-feedback">This field is required</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row" v-if="!welcome">
                                        <div class="col-md-12">
                                            <div class="form-group m-b-10">
                                                <div class="checkbox checkbox-css">
                                                    <input v-model="profile.details.use_billing_details" type="checkbox" id="${prefix}billing" />
                                                    <label for="${prefix}billing">My billing details are the same as above</label>
                                                </div>
                                            </div>
                                        </div>                                        
                                    </div>
                                    <transition name="fade">
                                        <div v-show="!welcome && !profile.details.use_billing_details">
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group m-b-10 p-t-5">
                                                        <label for="${prefix}b-first-name">Billing First Name</label>
                                                        <input v-model.trim="profile.billing_address.first_name" class="form-control" type="text" id="${prefix}b-first-name" name="billing_first_name" 
                                                            maxlength="100"
                                                            @change="onInputChange" 
                                                            @invalid.prevent="onInputInvalid"
                                                            :required="!profile.details.use_billing_details">
                                                        <div class="invalid-feedback">This field is required</div>
                                                    </div>
                                                    <div class="form-group m-b-10">
                                                        <label for="${prefix}b-email">Billing Email</label>
                                                        <input v-model.trim="profile.billing_address.email" class="form-control" type="email" id="${prefix}b-email" name="billing_email"
                                                            maxlength="100"
                                                            @change="onInputChange" 
                                                            @invalid.prevent="onInputInvalid"
                                                            :required="!profile.details.use_billing_details">
                                                        <div class="invalid-feedback">This field is required</div>
                                                    </div>
                                                    <div class="form-group m-b-10">
                                                        <label for="${prefix}b-company">Billing Company</label>
                                                        <input v-model.trim="profile.billing_address.company" class="form-control" type="text" id="${prefix}b-company" name="billing_company" maxlength="255">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group m-b-10 p-t-5">
                                                        <label for="${prefix}b-last-name">Billing Last Name</label>
                                                        <input v-model.trim="profile.billing_address.last_name" class="form-control" type="text" id="${prefix}b-last-name" name="billing_last_name"
                                                            maxlength="100"
                                                            @change="onInputChange" 
                                                            @invalid.prevent="onInputInvalid"
                                                            :required="!profile.details.use_billing_details">
                                                        <div class="invalid-feedback">This field is required</div>                                                
                                                    </div>
                                                    <div class="form-group m-b-10">
                                                        <label for="${prefix}b-phone">Billing Phone Number</label>
                                                        <input v-model.trim="profile.billing_address.phone" class="form-control" type="text" id="${prefix}b-phone" name="billing_phone" 
                                                            maxlength="50"
                                                            @change="onInputChange" 
                                                            @invalid.prevent="onInputInvalid"
                                                            :required="!profile.details.use_billing_details">
                                                        <div class="invalid-feedback">This field is required</div>
                                                    </div>
                                                    <div class="form-group m-b-10">
                                                        <crafty-click 
                                                            v-model="profile.billing_address.postcode"
                                                            v-on:success="onBillingPostcodeSuccess"
                                                            v-on:fail="onBillingPostcodeFail"
                                                            api-key="0dc98-8e64e-37a6b-03266"
                                                            :use-cache=true
                                                            id="${prefix}b-postcode" 
                                                            label="Billing Postcode" 
                                                            name="billing_postcode"
                                                            :required="!profile.details.use_billing_details">
                                                        </crafty-click>
                                                        <div class="input-feedback" :class="{ 'invalid-feedback': !billing.isPostcodeSuccess, 'valid-feedback': billing.isPostcodeSuccess}" v-show="billing.postcodeMsg !== ''">{{billing.postcodeMsg}}</div>
                                                    </div>
                                                </div>                                        
                                            </div>
                                            <div class="form-row" v-show="billing.addresses.length">
                                                <div class="col-md-12">
                                                    <div class="form-group m-b-10">
                                                        <label>Please select an address</label>
                                                        <div class="input-group">
                                                            <select class="form-control" v-model="billing.address" @change="onBillingAddressSelect">
                                                                <option v-for="item in billing.addresses" :item="item" :key="item.id">{{item.text}}</option>
                                                            </select>
                                                            <div class="input-group-append">
                                                                <button type="button" class="btn btn-default action" @click="onBillingPostcodeCancel">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-12">
                                                    <div class="form-group m-b-10">
                                                        <label for="${prefix}b-address">Billing Address</label>
                                                        <input v-model="profile.billing_address.address" class="form-control" type="text" id="${prefix}b-address" name="billing_address"
                                                            maxlength="255"
                                                            @change="onInputChange" 
                                                            @invalid.prevent="onInputInvalid"
                                                            :required="!profile.details.use_billing_details">
                                                        <div class="invalid-feedback">This field is required</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </transition>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <div class="form-group m-b-10">
                                                <label for="${prefix}password" class="w-100">Password <small class="pull-right">Leave blank if you don't want to change it</small></label>
                                                <input v-model.trim="profile.password" class="form-control" type="password" id="${prefix}password" name="password" autocomplete="new-password"
                                                    minlength="6"
                                                    @change="onInputChange($event, 'password')" 
                                                    @invalid.prevent="onInputInvalid">
                                                <div class="invalid-feedback">Min. 6 chars required</div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group m-b-10">
                                                <label for="${prefix}password-retype">Confirm Password</label>
                                                <input v-model.trim="profile.confirm_password" class="form-control" type="password" id="${prefix}password-retype" name="confirm_password"
                                                    @change="onInputChange($event, 'confirm_password')">
                                                <div class="invalid-feedback">Password and confirm password does not match</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-12 panel-footer text-right">
                                            <transition name="fade">
                                                <div class="alert pull-left result" :class="{'text-warning': hasError, 'text-success': !hasError }" role="alert" v-show="message !== ''">{{message}}</div>
                                            </transition>
                                            <button type="submit" class="btn btn-secondary btn-large" :aria-disabled="isWorking" :disabled="isWorking">
                                                <i class="fas fa-save" aria-hidden="true" v-show="!isWorking"></i>
                                                <i class="fas fa-cog fa-spin" aria-hidden="true" v-show="isWorking"></i>
                                                Save
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-4">
                        <latest-businesses-added></latest-businesses-added>
                    </div>
                </div>                
            </div>
        `;
    }
}
