import consts from '../config/constants';
import Utils from "../utils/utilities";

export default {
    render() {
        let prefix = `${Utils.guid()}-`;
        return `
            <div class="v-comp-module-body v-comp-property">
                <loader :active=loading></loader>                
                <h1 class="page-header" v-if="isPaymentPage()">Payment Page</h1>
                <h1 class="page-header" v-else-if="!isEditMode">Add a Property</h1>
                <h1 class="page-header" v-else>Edit your Property</h1>
                <h6 class="page-description" v-if="isPaymentPage()">Lorem dolor sit amet situr moda eles.</h6>
                <h6 class="page-description" v-else-if="form.purpose === '${consts.PROPERTY_PURPOSE.SALE}'">
                    Get started on your property listing by entering the required details in the form below. Provide answers that are as detailed as possible to ensure that potential buyers can find the information they need about your property as easily as possible.
                    <br/><br/>
                    Once you are finished, click <strong>Continue</strong> to move on to the next step.
                </h6>
                <h6 class="page-description" v-else-if="form.purpose === '${consts.PROPERTY_PURPOSE.RENT}'">
                    Get started on your property listing by entering the required details in the form below. Provide answers that are as detailed as possible to ensure that potential tenants can find the information they need about your property as easily as possible.
                    <br/><br/>
                    Once you are finished, click <strong>Continue</strong> to move on to the next step.
                </h6>
                <h6 class="page-description" v-else>
                    Get started on your property listing by entering the required details in the form below. Provide answers that are as detailed as possible to ensure that potential tenants and buyers can find the information they need about your property as easily as possible.
                    <br/><br/>
                    Once you are finished, click <strong>Continue</strong> to move on to the next step.
                </h6>
                <div
                <div class="row">
                    <div class="col-lg-12 col-xl-8">
                        <div class="sw-main sw-theme-ls">
                            
                            <ul class="nav nav-tabs step-anchor pretty-scrollbar" v-show="!isPaymentPage()">
                                <li class="nav-item" :class="{active: this.step === 0}">
                                    <a class="nav-link" :class="{disabled: isStepDisabled(0)}" @click="goTo(0)">
                                        <span class="number" v-if="!isEditMode">1</span> 
                                        <span class="info text-ellipsis">Property Info</span>
                                    </a>
                                </li>
                                <li class="nav-item" :class="{active: this.step === 1}">
                                    <a class="nav-link" :class="{disabled: isStepDisabled(1)}" @click="goTo(1)">
                                        <span class="number" v-if="!isEditMode">2</span> 
                                        <span class="info text-ellipsis">Images & Video</span>
                                    </a>
                                </li>
                                <li class="nav-item" :class="{active: this.step === 2}">
                                    <a class="nav-link" :class="{disabled: isStepDisabled(2)}" @click="goTo(2)">
                                        <span class="number" v-if="!isEditMode">3</span>
                                        <span class="info text-ellipsis">Upload documents</span>
                                    </a>
                                </li>
                                <li class="nav-item" :class="{active: this.step === 3}">
                                    <a class="nav-link" :class="{disabled: isStepDisabled(3)}" @click="goTo(3)">
                                        <span class="number" v-if="!isEditMode">4</span> 
                                        <span class="info text-ellipsis">
                                            <span class="info-small">Publish</span>
                                            <span class="info-large">Make your ad stand out</span>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                            
                            <div class="sw-container tab-content panel">
                                
                                <div class="tab-pane step-content" v-show="this.step === 0">
                                    <form onsubmit="return false" name="property-details-form">                                    
                                        <div class="form-row m-b-20">
                                            <div class="col-md-3 p-t-30 extra-options">
                                                <div class="checkbox checkbox-css checkbox-inline">
                                                    <input type="checkbox" id="${prefix}under_offer" v-model="form.under_offer"> 
                                                    <label for="${prefix}under_offer">Under Offer</label>
                                                </div>
                                                <div class="checkbox checkbox-css checkbox-inline">
                                                    <input type="checkbox" id="${prefix}auction" v-model="form.auction"> 
                                                    <label for="${prefix}auction">Auction</label>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <label for="${prefix}comm_use_class">Common Use Class</label>
                                                <treeselect 
                                                    v-model="form.comm_use_class" 
                                                    :multiple=true 
                                                    :options="useClasses" 
                                                    @change="onInputChange" 
                                                    @invalid.prevent="onInputInvalid"></treeselect>  
                                            </div>
                                            <div class="col-md-3">
                                                <label for="${prefix}reference">Property Reference</label>
                                                <input v-model.trim="form.reference" class="form-control" type="text" id="${prefix}reference" name="reference"
                                                    maxlength="255"
                                                    @input="onInput('reference')"
                                                    @change="onInputChange($event, 'reference')"
                                                    @invalid.prevent="onInputInvalid">
                                            </div>
                                        </div>
                                        <div class="form-row m-b-10">
                                            <div :class="{'col-md-3': !isBothPurpose, 'col-md-8': isBothPurpose}">
                                                <label for="${prefix}title">Property Title</label>
                                                <span class="pull-right input-counter title-counter">{{titleLength}}/{{titleLimit}}</span>
                                                <input v-model.trim="form.title" class="form-control" type="text" id="${prefix}title" name="title" required
                                                    maxlength="70"
                                                    @input="onInput('title')"
                                                    @change="onInputChange($event, 'title')"
                                                    @invalid.prevent="onInputInvalid">
                                                <div class="invalid-feedback">This field is required</div>
                                                <p class="text-muted slug" v-show="form.slug !== ''">URL: {{slug}}</p>
                                            </div>
                                            <div :class="{'col-md-3': !isBothPurpose, 'col-md-4': isBothPurpose}">
                                                <label for="${prefix}type_id">Property Type</label>
                                                <treeselect 
                                                    v-model="form.type_id" 
                                                    :multiple=false 
                                                    :options="types" 
                                                    required 
                                                    @input="onTypeChange"
                                                    @change="onInputChange" 
                                                    @invalid.prevent="onInputInvalid"></treeselect>
                                                <div class="invalid-feedback">This field is required</div>                                                
                                            </div>
                                            <div class="col-md-2" v-if="form.purpose === '${consts.PROPERTY_PURPOSE.SALE}'">
                                                <label for="${prefix}price_sale">Property Price</label>
                                                <input v-model.lazy="form.price_sale" v-money="moneyOptions" class="form-control" id="${prefix}price_sale" name="price_sale" min="0" required
                                                    @change="onInputChange" 
                                                    @invalid.prevent="onInputInvalid">                                                
                                                <div class="invalid-feedback">This field is required</div>
                                            </div>
                                            <div class="col-md-2" v-if="form.purpose === '${consts.PROPERTY_PURPOSE.SALE}'">
                                                <label for="${prefix}price_sale_per">Price Unit</label>
                                                <select class="form-control" v-model="form.price_sale_per" @change="onInputChange"
                                                    @invalid.prevent="onInputInvalid" required>
                                                    <option v-for="option in priceSaleUnitNames" :value="option.value">{{option.text}}</option>
                                                </select>
                                                <div class="invalid-feedback">This field is required</div>
                                            </div>
                                            <div class="col-md-2" v-if="form.purpose === '${consts.PROPERTY_PURPOSE.SALE}'">
                                                <label for="${prefix}price_sale_qualifier">Price Qualifier</label>
                                                <select class="form-control" v-model="form.price_sale_qualifier" @change="onInputChange"
                                                    @invalid.prevent="onInputInvalid" required>
                                                    <option v-for="option in priceQualifiers('${consts.PROPERTY_PURPOSE.SALE}')" :value="option.value">{{option.text}}</option>
                                                </select>
                                                <div class="invalid-feedback">This field is required</div>
                                            </div>
                                            <div class="col-md-2" v-if="form.purpose === '${consts.PROPERTY_PURPOSE.RENT}'">
                                                <label for="${prefix}price_rent">Rent Price</label>
                                                <input v-model.lazy="form.price_rent" v-money="moneyOptions" class="form-control" id="${prefix}price_rent" name="price_rent" min="0" required
                                                    @change="onInputChange" 
                                                    @invalid.prevent="onInputInvalid">                                                
                                                <div class="invalid-feedback">This field is required</div>
                                            </div>
                                            <div class="col-md-2" v-if="form.purpose === '${consts.PROPERTY_PURPOSE.RENT}'">
                                                <label for="${prefix}price_rent_per">Price Unit</label>
                                                <select class="form-control" v-model="form.price_rent_per" @change="onInputChange"
                                                    @invalid.prevent="onInputInvalid" required>
                                                    <option v-for="option in priceRentUnitNames" :value="option.value">{{option.text}}</option>
                                                </select>
                                                <div class="invalid-feedback">This field is required</div>
                                            </div>
                                            <div class="col-md-2" v-if="form.purpose === '${consts.PROPERTY_PURPOSE.RENT}'">
                                                <label for="${prefix}price_rent_qualifier">Price Qualifier</label>
                                                <select class="form-control" v-model="form.price_rent_qualifier" @change="onInputChange"
                                                    @invalid.prevent="onInputInvalid" required>
                                                    <option v-for="option in priceQualifiers('${consts.PROPERTY_PURPOSE.RENT}')" :value="option.value">{{option.text}}</option>
                                                </select>
                                                <div class="invalid-feedback">This field is required</div>
                                            </div>
                                        </div>    
                                        <div class="form-row m-b-10" v-if="isBothPurpose">
                                            <div class="col-md-2">
                                                <label for="${prefix}price_sale">For Sale Price</label>
                                                <input v-model.lazy="form.price_sale" v-money="moneyOptions" class="form-control" id="${prefix}price_sale" name="price_sale" min="0" required
                                                    @change="onInputChange" 
                                                    @invalid.prevent="onInputInvalid">                                                
                                                <div class="invalid-feedback">This field is required</div>
                                            </div>
                                            <div class="col">
                                                <label for="${prefix}price_sale_per">Price Unit</label>
                                                <select class="form-control" v-model="form.price_sale_per" @change="onInputChange"
                                                    @invalid.prevent="onInputInvalid" required>
                                                    <option v-for="option in priceSaleUnitNames" :value="option.value">{{option.text}}</option>
                                                </select>
                                                <div class="invalid-feedback">This field is required</div>
                                            </div>
                                            <div class="col-md-2">
                                                <label for="${prefix}price_sale_qualifier">Price Qualifier</label>
                                                <select class="form-control" v-model="form.price_sale_qualifier" @change="onInputChange"
                                                    @invalid.prevent="onInputInvalid" required>
                                                    <option v-for="option in priceQualifiers('${consts.PROPERTY_PURPOSE.SALE}')" :value="option.value">{{option.text}}</option>
                                                </select>
                                                <div class="invalid-feedback">This field is required</div>
                                            </div>
                                            <div class="col-auto d-sm-none d-md-block separator"><span>|</span></div>
                                            <div class="col-md-2">
                                                <label for="${prefix}price_rent">For Rent Price</label>
                                                <input v-model.lazy="form.price_rent" v-money="moneyOptions" class="form-control" id="${prefix}price_rent" name="price_rent" min="0" required
                                                    @change="onInputChange" 
                                                    @invalid.prevent="onInputInvalid">                                                
                                                <div class="invalid-feedback">This field is required</div>
                                            </div>
                                            <div class="col">
                                                <label for="${prefix}price_rent_per">Price Unit</label>
                                                <select class="form-control" v-model="form.price_rent_per" @change="onInputChange"
                                                    @invalid.prevent="onInputInvalid" required>
                                                    <option v-for="option in priceRentUnitNames" :value="option.value">{{option.text}}</option>
                                                </select>
                                                <div class="invalid-feedback">This field is required</div>
                                            </div>
                                            <div class="col-md-2">
                                                <label for="${prefix}price_rent_qualifier">Price Qualifier</label>
                                                <select class="form-control" v-model="form.price_rent_qualifier" @change="onInputChange"
                                                    @invalid.prevent="onInputInvalid" required>
                                                    <option v-for="option in priceQualifiers('${consts.PROPERTY_PURPOSE.RENT}')" :value="option.value">{{option.text}}</option>
                                                </select>
                                                <div class="invalid-feedback">This field is required</div>
                                            </div>
                                        </div>       
                                        <div class="form-row">
                                            <hr/>
                                        </div>
                                        <div class="form-row m-b-10">
                                            <div class="col-md-5">
                                                <label for="${prefix}location">Property Location</label>
                                                <span class="pull-right input-counter location-counter">{{locationLength}}/{{locationLimit}}</span>
                                                <input v-model.trim="form.location" class="form-control" type="text" id="${prefix}location" name="location" required
                                                    maxlength="70"
                                                    @input="onInput('location')"
                                                    @change="onInputChange($event, 'location')"
                                                    @invalid.prevent="onInputInvalid">
                                                <div class="invalid-feedback">This field is required</div>
                                                <p class="text-muted m-b-0 m-t-5 m-l-5">To be displayed under the property title on the property page</p>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="${prefix}town">Property Town</label>
                                                <input v-model.trim="form.town" class="form-control" type="text" id="${prefix}town" name="town" required
                                                    maxlength="255"
                                                    @input="onInput('town')"
                                                    @change="onInputChange($event, 'town')"
                                                    @invalid.prevent="onInputInvalid">
                                                <div class="invalid-feedback">This field is required</div>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="${prefix}postcode">Postcode</label>
                                                <input v-model.trim="form.postcode" class="form-control" type="text" id="${prefix}postcode" name="postcode" required :disabled="postcodeDisabled" :aria-disabled="postcodeDisabled"
                                                    maxlength="25"
                                                    @change="onInputChange($event, 'postcode')" 
                                                    @invalid.prevent="onInputInvalid">
                                                <div class="input-feedback" :class="{ 'invalid-feedback': !isPostcodeSuccess, 'valid-feedback': isPostcodeSuccess}" v-show="postcodeMsg !== ''">{{postcodeMsg}}</div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <hr/>
                                        </div>
                                        <div class="form-row m-b-10">
                                            <div class="col-md-12">
                                                <label for="${prefix}headline">Headline</label>
                                                <span class="pull-right input-counter headline-counter">{{headlineLength}}/{{headlineLimit}}</span>
                                                <textarea v-model.trim="form.headline" class="form-control no-resizable pretty-scrollbar" id="${prefix}headline" name="headline" required
                                                    maxlength="1000"
                                                    @input="onInput('headline')"
                                                    @change="onInputChange" 
                                                    @invalid.prevent="onInputInvalid"></textarea>
                                                <div class="invalid-feedback">This field is required</div>                                            
                                                <p class="text-muted m-b-0 m-t-5 m-l-5">Short description of your property</p>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <label for="${prefix}description">Description</label>
                                                <span class="pull-right input-counter description-counter">{{descriptionLength}}/{{descriptionLimit}}</span>
                                            </div>                                            
                                            <div class="col-md-12">
                                                <vue-editor 
                                                    id="${prefix}description"
                                                    v-model="form.description" 
                                                    class="vue-editor description-editor"
                                                    ref="DescriptionEditor"
                                                    :editorToolbar="editorToolbar"
                                                    :editorOptions="editorOptions"
                                                    @text-change="onEditorChange">
                                                </vue-editor>
                                            </div>                                            
                                            <div class="col-md-12">
                                                <p class="text-muted m-b-0 m-t-5 m-l-5">Enter as much information possible. Ads with detailed and longer descriptions get more views and enquiries!</p>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <hr/>
                                        </div>
                                        <div class="form-row" v-show="form.type_id"> 
                                            <label class="w-100 m-l-15">Additional information</label>      
                                            <checkbox-group
                                                ref="CheckboxGroup" 
                                                :items="propertyAttributes" 
                                                v-model.lazy="property_attributes"
                                                wrapper-cls="m-l-20 m-r-15 m-b-15">
                                            </checkbox-group>                                                
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <label for="${prefix}website_url" v-if="settings.prices['property-website-url'] > 0">Website Link - Include a link to your website for {{settings.currency.symbol}}{{settings.prices['property-website-url']}}</label>
                                                <label for="${prefix}website_url" v-else>Website Link</label>
                                                <input v-model.trim="form.website_url" class="form-control" type="url" id="${prefix}website_url" name="website_url"
                                                    :disabled="!isPayableItemAvailable"
                                                    maxlength="255"
                                                    @change="onInputChange($event, 'website_url')" 
                                                    @invalid.prevent="onInputInvalid">
                                                <div class="invalid-feedback">Please provide a valid URL, protocol is required http(s)://</div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <p class="text-muted m-b-0 m-t-5 m-l-5">Copy and paste a website link here, that you think will be useful to members viewing your property - for example, a link to your property elsewhere online, or a link to further images.</p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                
                                <div class="tab-pane step-content" v-show="this.step === 1">
                                    <input-video-preview
                                        v-model="video"
                                        :prevent-mutation-delay=1000
                                        id="${prefix}video"
                                        name="video" 
                                        label="Add a property video"
                                        tip="Enter a YouTube video link to be displayed on your property page">
                                    </input-video-preview>
                                    <div class="form-row m-t-10">
                                        <hr/>
                                    </div>

                                    <input-images-upload
                                        ref="InputImagesUpload"
                                        label="Add your property images - add up to 5 images"
                                        description="Recommended image size - 1170 x 780"
                                        :image-url="getMediaUrl"
                                        uploadUrl="/Properties/images"
                                        :sorting=true
                                        name="images"
                                        v-model="images"
                                        :limit=imagesHardLimit
                                        @success="onImagesUpload"
                                        @fail="onImagesUpload"                                            
                                        @change-order="onImageChangeOrder">                                            
                                    </input-images-upload>
                                </div>
                                
                                <div class="tab-pane step-content" v-show="this.step === 2">
                                    <p>Upload each of the relevant documents regarding your property that you think would be beneficial for potential buyers or renters to see. You may add a maximum of 3 documents per property listing.</p>
                                    <p>Once you have finished uploading your documents, click <strong>Continue</strong> to move on to the final step, or <strong>Save as Draft</strong> to make further edits later.</p>
                                    <input-documents-upload
                                        ref="InputFilesUpload"
                                        label="Upload your documents below"
                                        :image-url="getMediaUrl"
                                        uploadUrl="/Properties/documents"
                                        :sorting=true
                                        name="documents"
                                        v-model="documents"
                                        @success="onDocumentsUpload"
                                        @fail="onDocumentsUpload"                                            
                                        @change-order="onDocumentChangeOrder">                                            
                                    </input-documents-upload>
                                </div>
                                
                                <div class="tab-pane step-content" v-show="this.step === 3">
                                    <form onsubmit="return false" name="property-stand-out-form">
                                        <div v-if="isPayableItemAvailable">
                                            <div class="form-row ad-option">
                                                <div class="col-auto">
                                                    <span class="btn btn-warning active btn-block no-shadow text-white" @click="togglePlanTo('${consts.AD_TYPE.STANDARD}')">
                                                        <i class="fas fa-cog d-none d-xl-inline" aria-hidden="true"></i> Standard                                                
                                                    </span>
                                                </div>
                                                <div class="col">
                                                    <p>Your property will be visible in search results only.</p>
                                                </div>
                                                <div class="col-auto" :class="{'col-md-3': isStandardPlanOnly()}">
                                                    <span class="label label-secondary">{{settings.validity['property-standard-days']}} days - {{settings.currency.symbol}}{{settings.prices['property-standard']}}</span>
                                                    <div class="switcher switcher-success" v-if="!isStandardPlanOnly()">
                                                        <input type="checkbox" name="ad_type" id="${prefix}plan-standard" value="${consts.AD_TYPE.STANDARD}" v-model="plan" @change="preventIfLessThanOneCheckedPlan">
                                                        <label for="${prefix}plan-standard"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row ad-option" v-show="settings.plans['property-${consts.AD_TYPE.PREMIUM.toLowerCase()}']">
                                                <div class="col-auto">
                                                    <span class="btn btn-green active btn-block no-shadow text-white" @click="togglePlanTo('${consts.AD_TYPE.PREMIUM}')">
                                                        <i class="fas fa-feather d-none d-xl-inline" aria-hidden="true"></i> Premium                                                
                                                    </span>
                                                </div>
                                                <div class="col">
                                                    <p>Your property will be visible in search results, highlighted in gold. It will be be prioritised other standard listings, meaning that it always displays at the top of the search results, and will also be showcased in the sidebar.</p>
                                                </div>
                                                <div class="col-auto">
                                                    <span class="label label-secondary">{{settings.validity['property-premium-days']}} days - {{settings.currency.symbol}}{{settings.prices['property-premium']}}</span>
                                                    <div class="switcher switcher-success">
                                                        <input type="checkbox" name="ad_type" id="${prefix}plan-premium" value="${consts.AD_TYPE.PREMIUM}" v-model="plan" @change="preventIfLessThanOneCheckedPlan">
                                                        <label for="${prefix}plan-premium"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row ad-option" v-show="settings.plans['property-${consts.AD_TYPE.FEATURED.toLowerCase()}']">
                                                <div class="col-auto">
                                                    <span class="btn btn-primary active btn-block no-shadow text-white" @click="togglePlanTo('${consts.AD_TYPE.FEATURED}')">
                                                        <i class="fas fa-gem d-none d-xl-inline" aria-hidden="true"></i> Featured                                                
                                                    </span>
                                                </div>
                                                <div class="col">
                                                    <p>Your property will be visible in search results, highlighted in gold. It will be be prioritised other standard listings, meaning that it always displays at the top of the search results.</p>
                                                </div>
                                                <div class="col-auto">
                                                    <span class="label label-secondary">{{settings.validity['property-featured-days']}} days - {{settings.currency.symbol}}{{settings.prices['property-featured']}}</span>
                                                    <div class="switcher switcher-success">
                                                        <input type="checkbox" name="ad_type" id="${prefix}plan-featured" value="${consts.AD_TYPE.FEATURED}" v-model="plan" @change="preventIfLessThanOneCheckedPlan">
                                                        <label for="${prefix}plan-featured"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div v-else-if="form.ad_expiry_date">
                                            <div class="form-row">
                                                <div class="col">
                                                    <p class="m-t-10">I would like extend my current plan - <strong>{{form.ad_type}} (extend expiration date to {{getExtendedExpiryDate}})</strong></p>
                                                </div>
                                                <div class="col-auto">
                                                    <div class="switcher switcher-success">
                                                        <input type="checkbox" name="ad_type" id="${prefix}plan-prolonged" v-model="form.ad_extend">
                                                        <label for="${prefix}plan-prolonged"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div v-else>
                                            <div class="form-row">
                                                <div class="col">
                                                    <p class="m-t-10">Please complete the payment process that has been started.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                
                                <div class="tab-pane step-content" v-show="isPaymentPage()">
                                    <payment-form 
                                        ref="PaymentForm" 
                                        :user="user" 
                                        :settings="settings" 
                                        :active=isWorking>
                                    </payment-form>
                                </div>
                                
                                <div class="btn-toolbar sw-toolbar sw-toolbar-bottom panel-footer m-t-15">
                                    <div class="row w-100 responsive-toolbar-wrapper">
                                        <div class="col-md-4 p-l-0 m-b-10">
                                            <button type="button" class="btn btn-warning btn-large" :aria-disabled="isWorking" :disabled="isWorking" @click.prevent="onSaveDraft" v-show="(!isEditMode || isDraft) && !isPaymentPage()">
                                                <i class="fas fa-save" aria-hidden="true" v-show="!isWorking"></i>
                                                <i class="fas fa-cog fa-spin" aria-hidden="true" v-show="isWorking"></i>
                                                Save as Draft
                                            </button>
                                            <button type="button" class="btn btn-secondary btn-large" @click.prevent="prev" v-show="isPaymentPage() && !isWorking && !disableBackToEdit">
                                                Back to edit
                                            </button>                                            
                                        </div>     
                                        <div class="col-md-8 p-r-0 m-b-10 text-right">                                           
                                            <button type="button" class="btn btn-large" :class="{'btn-primary': isPaymentPage(), 'btn-secondary': !buttonSubmit.green && !isPaymentPage(), 'btn-green': buttonSubmit.green && !isPaymentPage()}" :aria-disabled="isWorking" :disabled="isWorking" @click.prevent="onSave">
                                                <i class="fas" aria-hidden="true" :class="buttonSubmit.icons" v-show="!isWorking"></i>                                                    
                                                <i class="fas fa-cog fa-spin" aria-hidden="true" v-show="isWorking"></i>
                                                {{buttonSubmit.label}}
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row w-100">
                                        <transition name="fade">
                                            <div class="alert d-inline result" :class="{'text-danger': hasError, 'text-success': !hasError }" role="alert" v-show="message !== ''">{{message}}</div>
                                        </transition>
                                    </div>
                                </div>
                            </div>
                            
				        </div>
                    </div>
                    <div class="col-lg-12 col-xl-4">
                            <div class="row">
                                <div class="col-lg-6 col-xl-12">
                                    <payment-summary ref="PaymentSummary" @change="onSummaryChange" :settings="settings" :list="payments" v-model="discountCode" :hide-empty=true></payment-summary>
                                </div>
                                <div class="col-lg-6 col-xl-12">
                                    <latest-businesses-added></latest-businesses-added>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;
    }
}
