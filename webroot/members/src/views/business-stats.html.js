import C from './../config/constants';

export default {
    render() {
        return `
            <div class="v-comp-module-body v-comp-stats-layout v-comp-business-stats">
                <loader :active=loading></loader>
                <h1 class="page-header" v-if="user.hasService">{{business.service.company}}</h1>
                <h6 class="page-description" v-if="user.hasService">
                    View your business profile, click to edit your business details or take a look at your business statistics.
                </h6>
                <div class="row" v-if="user.hasService">
                    <div class="col-lg-12 col-xl-4">
                        <div class="panel">
                            <div class="panel-body p-30">
                                <images-carousel :images="images" :counter=true ref="ImagesCarousel"></images-carousel>
                                <div class="status" :class="statusCls">
                                    <span class="status-text">{{statusText}}</span>
                                </div>
                                <div class="details text-center">
                                    <p v-if="message" class="message">{{message}}</p>
                                    <h4 class="location">{{category}}</h4>
                                    <hr/>
                                    <p class="text-left headline">{{business.service.description_short}}</p>
                                    <button type="button" class="btn btn-secondary btn-large" @click="onViewClick" v-if="business.service.status === '${C.STATUS.PUBLISHED}'">
                                        <i aria-hidden="true" class="fas fa-eye"></i> 
                                        View business page
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-8">
                        <div class="panel chart-panel" :class="{'chart-panel': isCounterVisible }">
                            <div class="panel-body p-30 position-relative">
                                <h5>Business Stats</h5>
                                <div class="row chart-wrapper">
                                    <service-stats-chart 
                                        :chart-data="stats" 
                                        v-show="isChartAvailable && !rotateRequired"
                                        @rotate-required="onChartRotateRequired">
                                    </service-stats-chart>
                                    <p class="text-muted text-center" v-show="!isChartAvailable">No chart data available<br/>No data to display</p>
                                    <img src="/members/assets/rotate-to-landscape.png" class="rotate-device" alt="Rotate device to landscape view" v-show="isChartAvailable && rotateRequired"/>
                                </div>
                                <div class="row counter-wrapper" v-show="isCounterVisible">
                                    <div class="col-md-8">
                                        <h3 class="counter-msg">{{counterMsg}}</h3>
                                        <p class="counter-info">
                                            We will notify you when your profile is due to expire. To ensure your profile remains published, please visit <a class="link" @click="onEditClick">Business Page</a> and resubmit your business information. If you no longer wish to showcase your business after expiry, no action is required.
                                        </p>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="counter">
                                            <span class="i">
                                                <span>{{counter.days}}</span>
                                                <small>days</small>
                                            </span>
                                            <span class="i">
                                                <span>{{counter.hours}}</span>
                                                <small>hours</small>
                                            </span>
                                            <span class="i">
                                                <span>{{counter.minutes}}</span>
                                                <small>min</small>
                                            </span>
                                            <span class="i">
                                                <span>{{counter.seconds}}</span>
                                                <small>sec</small>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <service-enquiries ref="Enquiries" :per-page=1000 title="Business Enquiries" v-if="user.hasService"></service-enquiries>
                <div class="alert panel no-mandatory-data" v-if="!user.hasService">
                    No stats available yet. Please create business profile first.<br/><br/>
                    <a class="btn btn-primary" href="#/business-listing">Go to Business Profile</a>
                </div>
            </div>
        `;
    }
}
