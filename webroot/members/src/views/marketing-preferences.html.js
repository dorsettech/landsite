import Utils from '../utils/utilities';

export default {
    render() {
        let prefix = `${Utils.guid()}-`;
        return `
            <div class="v-comp-module-body v-comp-marketing-preferences">
                <h1 class="page-header">Marketing Preferences</h1>
                <h6 class="page-description">
                    We understand that different methods of communication are suited to different people. To make sure we contact you in a way that suits you, please choose from the following marketing and communication preferences so that we can keep you up to date with the latest in commercial property.
                </h6>                
                <div class="row">
                    <div class="col-lg-12 col-xl-8">
                        <div class="panel">
                            <div class="panel-body p-30">
                                <form @submit.prevent="onSubmit">
                                    <div class="form-row">
                                        <div class="col">
                                            <h5>News and updates</h5>
                                            <p>I am happy to receive regular news and updates from The Landsite via the email address I have provided.</p>
                                        </div>
                                        <div class="col-auto text-center">
                                            <div class="switcher switcher-success m-t-30">
                                                <input type="checkbox" name="marketing_agreement_1" id="${prefix}marketing-agreement-1" v-model="profile.details.marketing_agreement_1" @change="onPrefChange($event, 'marketing_agreement_1')">
                                                <label for="${prefix}marketing-agreement-1"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col">
                                            <h5>Marketing materials</h5>
                                            <p>Please only send me marketing materials related to the areas of The Landsite I have an interest in (e.g Buying or Selling a commercial property)</p>
                                        </div>
                                        <div class="col-auto text-center">
                                            <div class="switcher switcher-success m-t-30">
                                                <input type="checkbox" name="marketing_agreement_2" id="${prefix}marketing-agreement-2" v-model="profile.details.marketing_agreement_2" @change="onPrefChange($event, 'marketing_agreement_2')">
                                                <label for="${prefix}marketing-agreement-2"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col">
                                            <h5>Disagree</h5>
                                            <p>Please do not contact me with any marketing material.</p>
                                        </div>
                                        <div class="col-auto text-center">
                                            <div class="switcher switcher-success m-t-30">
                                                <input type="checkbox" name="marketing_agreement_3" id="${prefix}marketing-agreement-3" v-model="profile.details.marketing_agreement_3" @change="onPrefChange($event, 'marketing_agreement_3')">
                                                <label for="${prefix}marketing-agreement-3"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-12 panel-footer text-right">
                                            <transition name="fade">
                                                <div class="alert pull-left result" :class="{'text-warning': hasError, 'text-success': !hasError }" role="alert" v-show="message !== ''">{{message}}</div>
                                            </transition>
                                            <button type="submit" class="btn btn-secondary btn-large" :aria-disabled="isWorking" :disabled="isWorking">
                                                <i class="fas fa-save" aria-hidden="true" v-show="!isWorking"></i>
                                                <i class="fas fa-cog fa-spin" aria-hidden="true" v-show="isWorking"></i>
                                                Save Preferences
                                            </button>
                                        </div>
                                    </div>
                                </form> 
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-4">
                        <latest-businesses-added></latest-businesses-added>
                    </div>
                </div>
            </div>
            `;
    }
}
