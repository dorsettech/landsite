export default {
    render() {
        return `
            <div class="v-comp-module-body v-comp-recent-insights">
                <h1 class="page-header">Recent News</h1>
                <h6 class="page-description">
                    Stay up to date with the latest news in the land and commercial property sector, including market trends and statistics, according to your selected preferences.
                </h6>                
                <div class="row">
                    <loader :active=loading></loader>
                    <div class="panel w-100">
                        <div class="panel-body row posts">
                            <p class="no-data" v-if="items.length === 0">Sorry, but there are no records you preferred yet.</p>
                            <article-box-item
                                v-for="(item, index) in items"
                                v-bind:item="item"
                                v-bind:index="index"
                                v-bind:key="item.id">
                            </article-box-item>
                        </div>                    
                    </div>                
                </div>
                <div class="row">
                    <div class="col-12">
                        <pagination :params="pagination" @page-change="onPageChange"></pagination>
                    </div>
                </div>
            </div>
        `;
    }
}
