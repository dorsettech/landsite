import _ from '../utils/utilities';

export default {
    render() {
        let prefix = `${_.guid()}-`;
        return `
            <div class="v-comp-module-body v-comp-preferred-insights">
                <h1 class="page-header">Preferred News</h1>
                <h6 class="page-description">
                    When you created your Landsite profile, you’ll have been given the opportunity to choose your preferred news posts. You can update these preferences below. When you’re finished, click <strong>Save Preferences</strong>.
                </h6>                
                <div class="row">
                    <div class="col-lg-12 col-xl-8">
                        <div class="panel">
                            <div class="panel-body p-30">
                                <form @submit.prevent="onSubmit" name="preferred-insights">
                                    <div class="form-row">
                                        <div class="col-12">
                                            <h5>What news do you want to hear about?</h5>
                                            <p>Select the categories you are interested in from the list below:</p>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-12 col-lg-7">
                                            <div class="form-row">
                                                <div class="col-12">
                                                    <label>Select one of our recommended categories:</label>
                                                </div>
                                            </div>
                                            <div class="customcheck form-row">
                                                <category-image-checkbox
                                                  class="col-xl-4 recommended-checkbox"
                                                  v-for="(item, index) in recommended"
                                                  v-bind:item="item"
                                                  v-bind:index="index"
                                                  v-bind:key="item.id"
                                                  v-model="fields.recommended"
                                                ></category-image-checkbox>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-5">
                                            <div class="form-row m-b-5">
                                                <div class="col-12">
                                                    <label>Or manually search for categories in the search field below:</label>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-12">
                                                    <vue-simple-suggest
                                                        ref="CategoryAutoComplete"
                                                        :list="items"
                                                        display-attribute="name"
                                                        value-attribute="id"
                                                        :max-suggestions=5
                                                        :styles="autoCompleteStyles"
                                                        :destyled=true
                                                        :filter-by-query=true
                                                        :prevent-submit=true
                                                        @select="onCategorySelect">
                                                    </vue-simple-suggest>                                    
                                                </div>
                                                <div class="col-12">
                                                    <ul class="category-list-group">
                                                        <category-list-item
                                                          v-for="item in fields.selected"
                                                          v-bind:item="item"
                                                          v-bind:key="item.id"
                                                          v-model="fields.selected"
                                                          v-bind:name="item.name"
                                                        ></category-list-item>
                                                    </ul>
                                                </div>    
                                            </div>    
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-12 panel-footer text-right">
                                            <transition name="fade">
                                                <div class="alert pull-left result" :class="{'text-warning': hasError, 'text-success': !hasError }" role="alert" v-show="message !== ''">{{message}}</div>
                                            </transition>
                                            <button type="submit" class="btn btn-secondary btn-large" :aria-disabled="isWorking" :disabled="isWorking">
                                                <i class="fas fa-save" aria-hidden="true" v-show="!isWorking"></i>
                                                <i class="fas fa-cog fa-spin" aria-hidden="true" v-show="isWorking"></i>
                                                Save Preferences
                                            </button>
                                        </div>
                                    </div>                                    
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-4">
                        <latest-businesses-added></latest-businesses-added>
                    </div>
                </div>
            </div>
        `;
    }
}
