export default {
    render() {
        return `
            <div class="v-comp-module-body v-comp-properties">
                <h1 class="page-header">My Properties</h1>
                <h6 class="page-description">
                    Take a look at your property listings below. From here you can click on your individual property listings to make edits and manage your enquiries.
                    You have {{total$}} properties to manage.
                </h6>                
                <div class="row m-b-20">
                    <div class="col-sm-12 col-md-auto">
                        <a href="#/add-property" class="btn btn-primary add-btn w-100">
                            <i class="fas fa-plus-circle d-block"></i>
                            Add a Property
                        </a>
                    </div>
                    <div class="col">
                        <div class="form-row search-wrapper">
                            <input v-model="keywords" class="form-control search-input" type="search" maxlength="255" 
                            @input="onSearchInput" placeholder="Search by property title...">
                            <a class="reset-search" @click="reset" v-show="keywords !== ''">
                                <i class="fas fa-times" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row position-relative">
                    <loader :active=loading></loader>
                    <div class="col-lg-12 col-xl-8">
                        <p class="alert alert-secondary text-center no-data" role="alert" v-show="items.length === 0">No properties found</p>
                        <property-list-item
                            v-for="(item, index) in items"
                            v-bind:item="item"
                            v-bind:index="index"
                            v-bind:key="item.id"                            
                            :currency-symbol="settings.currency.symbol"
                            :message="item.visibility === false && item.ad_basket ? 'Payment interrupted. Complete the payment process to make the item visible.' : ''"
                            @action="action">
                        </property-list-item>
                    </div>
                    <div class="col-lg-12 col-xl-4">
                        <latest-businesses-added></latest-businesses-added>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <pagination :params="pagination" @page-change="onPageChange"></pagination>
                    </div>
                </div>
            </div>
        `;
    }
}
