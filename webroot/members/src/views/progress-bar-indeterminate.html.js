export default {
    render() {
        return `<transition name="fade">
            <div class="v-comp-progress-bar-indeterminate" v-if="isActive" :aria-busy="isActive" aria-label="Loading">
                <div class="line"></div>
                <div class="sub-line inc"></div>
                <div class="sub-line dec"></div>
            </div>
        </transition>`;
    }
}
