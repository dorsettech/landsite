import Consts from '../../config/constants';

export default {
    render() {
        return `
            <div class="panel panel-default v-comp-panel-list-item v-comp-property-list-item">
                <div class="panel-body">
                    <div class="row position-relative">
                        <div class="col-md-3 photo">
                            <div class="image" :style="imageStyle">
                                <span class="cost" v-if="cost">Cost: {{currencySymbol}}{{item.ad_cost.toLocaleString()}}</span>
                                <span class="under-offer" v-if="item.under_offer">Under Offer</span>
                                <span class="status" :class="statusCls">{{statusText}}</span>
                            </div>
                        </div>
                        <div class="col-md details">
                            <h3 class="title">{{item.title}}</h3>
                            <span class="date">{{dateLabel}}: <time :datetime="item.created">{{dateAdded}}</time></span>
                            <hr/>
                            <p class="headline">{{croppedHeadline}}</p>
                            <div class="buttons">
                                <a class="btn btn-large btn-secondary action" :class="statusCls" @click="onButtonClick('action')" v-if="buttons.main">
                                    <i class="fas" :class="button.iconCls"></i>  
                                    {{button.text}}
                                </a>
                                <a v-tooltip.top="'Property stats'" class="btn btn-icon-fa chart" @click="onButtonClick('stats')" v-if="buttons.stats">
                                    <span class="fa-stack fa-2x">
                                        <i class="far fa-circle fa-stack-2x"></i>
                                        <i class="fas fa-chart-line fa-stack-1x"></i>
                                    </span>
                                </a>
                                <a v-tooltip.top="'Edit'" class="btn btn-icon-fa edit" @click="onButtonClick('edit')" v-if="buttons.edit">
                                    <span class="fa-stack fa-2x">
                                        <i class="far fa-circle fa-stack-2x"></i>
                                        <i class="fas fa-edit fa-stack-1x"></i>
                                    </span>
                                </a>
                                <a v-tooltip.top="'View property'" class="btn btn-icon-fa view" @click="onButtonClick('view')" v-if="buttons.view">
                                    <span class="fa-stack fa-2x">
                                        <i class="far fa-circle fa-stack-2x"></i>
                                        <i class="fas fa-eye fa-stack-1x"></i>
                                    </span>
                                </a>
                                <a v-tooltip.top="'Delete'" class="btn btn-icon-fa delete" @click="onButtonClick('delete')" v-if="buttons.delete">
                                    <span class="fa-stack fa-2x">
                                        <i class="far fa-circle fa-stack-2x"></i>
                                        <i class="fas fa-trash-alt fa-stack-1x"></i>
                                    </span>
                                </a>                                
                            </div>
                            <p class="text-danger f-w-600" v-if="message">{{message}}</p>
                        </div>
                        <div class="col-md-3 stats" v-if="chart">
                            <div class="row top">
                                <div class="col-6" v-if="item.purpose === '${Consts.PROPERTY_PURPOSE.BOTH}'">
                                    <span>FOR SALE</span>
                                    <span class="price" v-if="item.price_sale_per !== '${Consts.PROPERTY_PRICE_SALE_PER.PRICE_ON_APPLICATION}'">{{currencySymbol}}{{item.price_sale.toLocaleString()}} {{priceSaleUnit}}</span>                                    
                                    <span class="price" v-else>${Consts.PROPERTY_PRICE_SALE_PER_NAMES.POA}</span>
                                    <span class="qualifier">{{toPriceQualifierName(item.price_sale_qualifier)}}</span>
                                </div>
                                <div class="col-6" v-if="item.purpose === '${Consts.PROPERTY_PURPOSE.BOTH}'">
                                    <span>TO RENT</span>
                                    <span class="price" v-if="item.price_rent_per !== '${Consts.PROPERTY_PRICE_RENT_PER.PRICE_ON_APPLICATION}'">{{currencySymbol}}{{item.price_rent.toLocaleString()}} {{priceRentUnit}}</span>                                    
                                    <span class="price" v-else>${Consts.PROPERTY_PRICE_RENT_PER_NAMES.POA}</span>
                                    <span class="qualifier">{{toPriceQualifierName(item.price_rent_qualifier)}}</span>
                                </div>
                                <div class="col-12" v-if="item.purpose === '${Consts.PROPERTY_PURPOSE.SALE}'">
                                    <span>FOR SALE</span>
                                    <span class="price" v-if="item.price_sale_per !== '${Consts.PROPERTY_PRICE_SALE_PER.PRICE_ON_APPLICATION}'">{{currencySymbol}}{{item.price_sale.toLocaleString()}} {{priceSaleUnit}}</span>                                    
                                    <span class="price" v-else>${Consts.PROPERTY_PRICE_SALE_PER_NAMES.POA}</span>
                                    <span class="qualifier">{{toPriceQualifierName(item.price_sale_qualifier)}}</span>
                                </div>
                                <div class="col-12 rent" v-if="item.purpose === '${Consts.PROPERTY_PURPOSE.RENT}'">
                                    <span>TO RENT</span>
                                    <span class="price" v-if="item.price_rent_per !== '${Consts.PROPERTY_PRICE_RENT_PER.PRICE_ON_APPLICATION}'">{{currencySymbol}}{{item.price_rent.toLocaleString()}} {{priceRentUnit}}</span>                                    
                                    <span class="price" v-else>${Consts.PROPERTY_PRICE_RENT_PER_NAMES.POA}</span>
                                    <span class="qualifier">{{toPriceQualifierName(item.price_rent_qualifier)}}</span>
                                </div>
                            </div>
                            <property-list-item-chart class="chart" :data="chartData"></property-list-item-chart>
                        </div>    
                        <div class="stats top" v-else="chart">
                            <div class="row top">
                                <div class="col-6" v-if="item.purpose === '${Consts.PROPERTY_PURPOSE.BOTH}'">
                                    <span>FOR SALE</span>
                                    <span class="price" v-if="item.price_sale_per !== '${Consts.PROPERTY_PRICE_SALE_PER.PRICE_ON_APPLICATION}'">{{currencySymbol}}{{item.price_sale.toLocaleString()}} {{priceSaleUnit}}</span>                                    
                                    <span class="price" v-else>${Consts.PROPERTY_PRICE_SALE_PER_NAMES.POA}</span>
                                    <span class="qualifier">{{toPriceQualifierName(item.price_sale_qualifier)}}</span>
                                </div>
                                <div class="col-6" v-if="item.purpose === '${Consts.PROPERTY_PURPOSE.BOTH}'">
                                    <span>TO RENT</span>
                                    <span class="price" v-if="item.price_rent_per !== '${Consts.PROPERTY_PRICE_RENT_PER.PRICE_ON_APPLICATION}'">{{currencySymbol}}{{item.price_rent.toLocaleString()}} {{priceRentUnit}}</span>                                    
                                    <span class="price" v-else>${Consts.PROPERTY_PRICE_RENT_PER_NAMES.POA}</span>
                                    <span class="qualifier">{{toPriceQualifierName(item.price_rent_qualifier)}}</span>
                                </div>
                                <div class="col-12" v-if="item.purpose === '${Consts.PROPERTY_PURPOSE.SALE}'">
                                    <span>FOR SALE</span>
                                    <span class="price" v-if="item.price_sale_per !== '${Consts.PROPERTY_PRICE_SALE_PER.PRICE_ON_APPLICATION}'">{{currencySymbol}}{{item.price_sale.toLocaleString()}} {{priceSaleUnit}}</span>                                    
                                    <span class="price" v-else>${Consts.PROPERTY_PRICE_SALE_PER_NAMES.POA}</span>
                                    <span class="qualifier">{{toPriceQualifierName(item.price_sale_qualifier)}}</span>
                                </div>
                                <div class="col-12 rent" v-if="item.purpose === '${Consts.PROPERTY_PURPOSE.RENT}'">
                                    <span>TO RENT</span>
                                    <span class="price" v-if="item.price_rent_per !== '${Consts.PROPERTY_PRICE_RENT_PER.PRICE_ON_APPLICATION}'">{{currencySymbol}}{{item.price_rent.toLocaleString()}} {{priceRentUnit}}</span>                                    
                                    <span class="price" v-else>${Consts.PROPERTY_PRICE_RENT_PER_NAMES.POA}</span>
                                    <span class="qualifier">{{toPriceQualifierName(item.price_rent_qualifier)}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>            
            </div>
        `;
    }
}
