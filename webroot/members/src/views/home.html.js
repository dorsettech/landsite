import C from './../config/constants';

export default {
    render() {
        return `
            <div class="v-comp-module-body v-comp-dashboard">
                <loader :active=loading></loader>
                <h1 class="page-header">Hello, {{user.full_name}}</h1>
                <h6 class="page-description">
                    Take a look at what’s been happening on your account or select from the below options.
                </h6>                
                <div class="row empty" v-if="!user.details || user.details.dashboard_type === null">
                    <icon-box 
                        icon="fa-building"
                        title="Advertise your property"
                        description="Share your property with a community of potential buyers in your area. Upload images, a full description and await enquiries from local investors."
                        :button="{title: 'Add property', icon: 'fa-plus-circle', label: 'Add now'}"
                        @click="onIconBoxClick('add-property')">
                    </icon-box>
                    <icon-box 
                        icon="fa-business-time"
                        title="Showcase your professional services"
                        description="Showcase your business and professional service before a community of potential customers in need of your help. Create a profile for your business, including testimonials and case studies and engage directly with customers in your area."
                        :button="{title: 'Add professional service', icon: 'fa-plus-circle', label: 'Add now'}"
                        @click="onIconBoxClick('add-service')">
                    </icon-box>
                    <icon-box 
                        icon="fa-building"
                        title="Search for a new property"
                        description="Search our property database for properties that suit your requirements and budget, and communicate directly with sellers to find out more about your potential future investment."
                        :button="{title: 'Search a property', icon: 'fa-search', label: 'Search now'}"
                        :extra-button-icon=true
                        @click="onIconBoxClick('search-property')">
                    </icon-box>
                    <icon-box 
                        icon="fa-business-time"
                        title="Search for businesses and professional services"
                        description="Access a full spectrum of businesses and professional service providers to ensure that your property transaction runs smoothly. From finance and loan providers to surveyors builders and architects, you’ll find everything you need on The Landsite."
                        :button="{title: 'Search professional service', icon: 'fa-search', label: 'Search now'}"
                        :extra-button-icon=true
                        @click="onIconBoxClick('search-service')">
                    </icon-box>
                </div>
                
                <div class="row" v-if="user.details && (user.details.dashboard_type === '${C.DASHBOARD_TYPE.SELLER}' || user.details.dashboard_type === '${C.DASHBOARD_TYPE.BOTH}')">
                    <icon-box-counter
                        icon="fa-building"
                        title="Total properties"
                        :counter="counters.properties"
                        link="View all properties"
                        @click="onIconBoxClick('properties')">
                    </icon-box-counter>
                    <icon-box-counter
                        icon="fa-users"
                        title="Properties visitors"
                        :counter="counters.properties_views"
                        link="View all properties"
                        @click="onIconBoxClick('properties')">
                    </icon-box-counter>
                    <icon-box-counter
                        icon="fa-envelope-open-text"
                        title="Enquiries received"
                        :counter="counters.properties_enquiries"
                        link="">
                    </icon-box-counter>
                    <icon-box-button 
                        icon="fa-building"
                        title="Add a new property"
                        :button="{title: 'Add a new property now', icon: 'fa-search', label: 'Add now'}"
                        @click="onIconBoxClick('add-property')">
                    </icon-box-button>
                </div>
                
                <property-enquiries 
                    v-if="user.details && (user.details.dashboard_type === '${C.DASHBOARD_TYPE.SELLER}' || user.details.dashboard_type === '${C.DASHBOARD_TYPE.BOTH}')"
                    ref="PropertiesEnquiries" 
                    :per-page=5
                    title="Recent Property Enquiries"
                    :load-on-start=true>
                </property-enquiries>
                
                <div class="row" v-if="user.details && (user.details.dashboard_type === '${C.DASHBOARD_TYPE.SELLER}' || user.details.dashboard_type === '${C.DASHBOARD_TYPE.BOTH}')">
                    <search-box 
                        icon-cls="properties" 
                        title="Search for Properties" 
                        description="Search our property database for properties that suit your requirements and budget, and communicate directly with sellers to find out more about your potential future investment."
                        @click="onIconBoxClick('search-property')">
                    </search-box>
                    <search-box 
                        icon-cls="services" 
                        title="Search for Professional Services" 
                        description="Access a full spectrum of businesses & professional service providers to ensure that your property transaction runs smoothly. From finance and loan providers to surveyors builders and architects, you’ll find everything you need on The Landsite."
                        @click="onIconBoxClick('search-service')">
                    </search-box>
                </div>
                
                <div class="row" v-if="user.details && (user.details.dashboard_type === '${C.DASHBOARD_TYPE.PROFESSIONAL}' || user.details.dashboard_type === '${C.DASHBOARD_TYPE.BOTH}')">
                    <icon-box-counter
                        icon="fa-users"
                        title="Profile visitors"
                        :counter="counters.service_views"
                        link="View your business profile"
                        @click="onIconBoxClick('service')">
                    </icon-box-counter>
                    <icon-box-counter
                        icon="fa-hand-point-up"
                        title="Website Button Clicks"
                        :counter="counters.service_website_clicks"
                        link="View your business profile"
                        @click="onIconBoxClick('service')">
                    </icon-box-counter>
                    <icon-box-counter
                        icon="fa-envelope-open-text"
                        title="Enquiries received"
                        :counter="counters.service_enquiries"
                        link="">
                    </icon-box-counter>
                    <icon-box-button 
                        icon="fa-building"
                        title="Edit your profile"
                        :button="{title: 'Edit your business profile', icon: 'fa-plus-circle', label: 'Edit now'}"
                        @click="onIconBoxClick('add-service')">
                    </icon-box-button>
                </div>
                                
                <business-enquiries 
                    v-if="user.details && (user.details.dashboard_type === '${C.DASHBOARD_TYPE.PROFESSIONAL}' || user.details.dashboard_type === '${C.DASHBOARD_TYPE.BOTH}')"
                    ref="BusinessEnquiries" 
                    :per-page=5
                    title="Recent Business Profile Enquiries"
                    :load-on-start=true>
                </business-enquiries>
                                
                <div class="row" v-if="user.details && user.details.dashboard_type === '${C.DASHBOARD_TYPE.PROFESSIONAL}'">
                    <search-box 
                        icon-cls="properties" 
                        title="Search for Properties" 
                        description="Search our property database for properties that suit your requirements and budget, and communicate directly with sellers to find out more about your potential future investment."
                        @click="onIconBoxClick('search-property')">
                    </search-box>
                    <search-box 
                        icon-cls="services" 
                        title="Search for Professional Services" 
                        description="Access a full spectrum of businesses & professional service providers to ensure that your property transaction runs smoothly. From finance and loan providers to surveyors builders and architects, you’ll find everything you need on The Landsite."
                        @click="onIconBoxClick('search-service')">
                    </search-box>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <recent-articles-panel ref="RecentArticles"></recent-articles-panel>
                    </div>
                </div>
            </div>
        `;
    }
}
