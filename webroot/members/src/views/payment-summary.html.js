import _ from '../utils/utilities';

export default {
    render() {
        let prefix = _.guid();
        return `
            <div v-show="!hideEmpty || (hideEmpty && (total > 0 || discount !== false))">
                <div class="panel panel-ls summary-discount">
                    <div class="panel-body">
                        <label for="${prefix}discount-code">Discount Code:</label>
                        <div class="input-wrapper d-flex">
                            <input v-model.trim="discountCode" class="form-control" type="text" name="discount_code" id="${prefix}discount-code" maxlength="24">
                            <i class="icon fas" :class="inputIcons" @click="onDiscountClearClick"></i>
                            <button class="btn btn-default flex-grow-0 m-l-10 discount-code-apply" @click="onDiscountChanged" :disabled="discountChecking">Apply Discount Code</button>                        
                        </div>
                        <p class="input-message">{{inputMessage}}</p>
                    </div>
                </div>
                <div class="panel panel-ls summary">
                    <div class="panel-heading text-left">
                        <div class="panel-heading-btn large-icons">
                          <i class="fas fa-clipboard-list"></i>
                        </div>
                        <h4 class="panel-title">Summary</h4>                                            
                    </div>
                    <div class="panel-body">
                        <table class="details">
                            <tr v-for="item in list">
                                <td>{{item.name}}</td>    
                                <td v-if="item.price > 0">{{settings.currency.symbol}}{{item.price.toFixed(2)}}</td>    
                                <td v-else>{{item.price.toFixed(2)}}</td>    
                            </tr>
                            <tr v-if="discount">
                                <td>{{discount.name}}</td>
                                <td>- {{settings.currency.symbol}}{{discount.price.toFixed(2)}}</td>
                            </tr>                            
                        </table>
                    </div>
                    <div class="panel-footer color-red-ls">
                        <div class="sub">
                            Subtotal: <span class="pull-right">{{settings.currency.symbol}}{{subtotal.toFixed(2)}}</span><br/>
                            VAT: <span class="pull-right">{{settings.currency.symbol}}{{vat.toFixed(2)}}</span>
                        </div>
                        <hr>
                        TOTAL: <span class="pull-right">{{settings.currency.symbol}}{{total.toFixed(2)}}</span>
                    </div>
                </div>
            </div>
        `;
    }
}
