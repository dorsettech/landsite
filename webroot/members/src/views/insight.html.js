import _ from "../utils/utilities";

export default {
    render() {
        let prefix = `${_.guid()}-`;
        return `
            <div class="v-comp-module-body v-comp-article">
                <h1 class="page-header" v-if="form.id && user.hasService">Edit a News post</h1>
                <h1 class="page-header" v-else-if="user.hasService || contribute === true">Add a News post</h1>
                <h6 class="page-description" v-if="user.hasService || contribute === true">
                    <span v-if="!contribute">Share property news and updates with The Landsite community in our News section. Simply add a title, the information you’d like to share, and an image below.</span>
                    <span v-else>Share your story with the Landsite community. Upload your news article with a focus on the land and commercial property sector, and submit for approval by our admin team.</span>
                    <br/><br/>
                    When you’re ready to submit your news post, click <strong>Publish Now</strong>, or <strong>Save as Draft</strong> to make further edits later.
                </h6>                
                <div class="row" v-if="user.hasService || contribute === true">
                    <div class="col-lg-12 col-xl-8">
                        <div class="panel">
                            <div class="panel-body p-30">
                                <form @submit.prevent="onSubmit" name="insight-form">
                                    <div class="form-row m-b-10">                                    
                                        <div class="col-md-8">
                                            <div class="row m-b-10">
                                                <div class="col-md-12">
                                                    <label for="${prefix}title">News Post Title</label>
                                                    <input v-model.trim="form.title" class="form-control" type="text" id="${prefix}title" name="title" required
                                                        maxlength="255"
                                                        @change="onInputChange"
                                                        @invalid.prevent="onInputInvalid">
                                                    <div class="invalid-feedback">This field is required</div>
                                                    <p class="text-muted slug" v-show="form.slug !== ''">URL: {{slug}}</p>
                                                </div>
                                            </div>
                                            <div class="row" :class="{'m-b-10': contribute}">
                                                <div class="col-md-12">
                                                    <label for="${prefix}headline">Headline</label>
                                                    <input v-model.trim="form.headline" class="form-control" type="text" id="${prefix}headline" name="headline" required
                                                        maxlength="255"
                                                        @change="onInputChange" 
                                                        @invalid.prevent="onInputInvalid">
                                                    <div class="invalid-feedback">This field is required</div>                                            
                                                </div>
                                            </div>
                                            <div class="row m-b-20" v-if="contribute">
                                                <div class="col-md-12">
                                                    <label for="${prefix}category_id">Professional Category</label>
                                                    <select class="form-control" v-model="form.category_id" @change="onInputChange"
                                                        @invalid.prevent="onInputInvalid" required>
                                                        <option v-for="item in categories" :item="item" :key="item.id" :value="item.id">{{item.name}}</option>
                                                    </select>
                                                    <div class="invalid-feedback">This field is required</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <input-image-upload
                                                ref="Image"
                                                v-model="form.image"                                                
                                                :image-url="getImageUrl"
                                                uploadUrl="/Articles/image"
                                                id="${prefix}image"
                                                name="image" 
                                                label="News Post image"
                                                v-on:success="onImageSuccess"
                                                v-on:fail="onImageFail">
                                            </input-image-upload>
                                            <div class="input-feedback text-center" :class="{ 'invalid-feedback': !isImageSuccess, 'valid-feedback': isImageSuccess}" v-show="imageMsg !== ''">{{imageMsg}}</div>
                                        </div>
                                    </div>                                          
                                    <div class="form-row m-b-20">
                                        <div class="col-md-12">
                                            <label for="${prefix}body">News Post Content</label>                                            
                                        </div>                                            
                                        <div class="col-md-12">
                                            <vue-editor 
                                                id="${prefix}body"
                                                v-model="form.body" 
                                                class="vue-editor body-editor"
                                                :editorToolbar="editorToolbar"
                                                :editorOptions="editorOptions">
                                            </vue-editor>
                                        </div>
                                    </div>
                                    <div class="form-row m-b-20">
                                        <div class="col-md-12">
                                            <label>Status:</label>
                                            <span class="label status" :class="statusCls">{{statusText}}</span>
                                        </div>
                                    </div>
                                    <div class="form-row m-t-10" v-if="hasRejectedReason">
                                        <div class="col-md-12">
                                            <div class="alert alert-danger">{{form.rejection_reason}}</div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-row panel-footer responsive-toolbar-wrapper">
                                        <div class="col-md-7 p-l-0 m-b-10">
                                            <button type="button" class="btn btn-warning btn-large" :aria-disabled="isWorking" :disabled="isWorking" @click.prevent="onSaveDraft" v-show="isDraftAvailable">
                                                <i class="fas fa-save" aria-hidden="true" v-show="!isWorking"></i>
                                                <i class="fas fa-cog fa-spin" aria-hidden="true" v-show="isWorking"></i>
                                                Save as Draft
                                            </button>
                                            <transition name="fade">
                                                <div class="alert d-inline result" :class="{'text-danger': hasError, 'text-success': !hasError }" role="alert" v-show="message !== ''">{{message}}</div>
                                            </transition>
                                        </div>     
                                        <div class="col-md-5 p-r-0 m-b-10 text-right">                                           
                                            <button type="button" class="btn btn-secondary btn-large" :aria-disabled="isWorking" :disabled="isWorking" @click.prevent="onSave">
                                                <i class="fas" aria-hidden="true" :class="buttonSubmit.icons" v-show="!isWorking"></i>                                                    
                                                <i class="fas fa-cog fa-spin" aria-hidden="true" v-show="isWorking"></i>
                                                {{buttonSubmit.label}}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-4">
                        <latest-businesses-added></latest-businesses-added>
                    </div>
                </div>
                <div class="alert panel no-mandatory-data" v-if="!user.hasService &&  contribute !== true">
                    Before publish news post, please create business profile first.<br/><br/>  
                    <a class="btn btn-primary" href="#/business-listing">Go to Business Profile</a>
                </div>
            </div>
        `;
    }
}
