import C from './../config/constants';

export default {
    render() {
        return `
            <div class="v-comp-module-body v-comp-alerts-searches">
                <loader :active=loading></loader>
                <h1 class="page-header">Alerts & Searches</h1>
                <h6 class="page-description">
                    Take a look at your recent account notifications and list of searched properties and professional service providers below. These lists are a helpful reference when looking for properties or businesses you have previously viewed.
                </h6>                
                <div class="row">
                    <div class="panel w-100">
                        <div class="panel-body p-30 property-searches">
                            <h4>Recent Property Searches</h4>
                            <vuetable ref="${C.SEARCH_TYPE.PROPERTY}_Searches"
                                api-url="/Searches"
                                :reactive-api-url=false
                                :fields="columns['${C.SEARCH_TYPE.PROPERTY}']"
                                :per-page="10"
                                :append-params="{'type': '${C.SEARCH_TYPE.PROPERTY}'}"
                                data-path="records"			                        
                                pagination-path="pagination"
                                :css="tableCls">
                                <div slot="alerts" slot-scope="props">
                                    <div class="switcher switcher-success">
                                        <input type="checkbox" name="ad_type" :id="props.rowData.id + '-ps'" v-model="props.rowData.alerts" @change="onAlertChange('${C.SEARCH_TYPE.PROPERTY}', props.rowData)">
                                        <label :for="props.rowData.id + '-ps'"></label>
                                    </div>
                                </div>
                                <div slot="actions" slot-scope="props" class="actions">
                                    <a v-tooltip.top="'Search'" class="action" @click="onActionClick('${C.SEARCH_TYPE.PROPERTY}', 'search', props.rowData)">
                                        <i class="fas fa-search"></i>
                                    </a>
                                    <a v-tooltip.top="'Delete'" class="action text-landsite-red" @click="onActionClick('${C.SEARCH_TYPE.PROPERTY}', 'delete', props.rowData)">
                                        <i class="fas fa-trash-alt"></i>
                                    </a>
                                </div>
                            </vuetable>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="panel w-100">
                        <div class="panel-body p-30 business-searches">
                            <h4>Recent Business Searches</h4>
                            <vuetable ref="${C.SEARCH_TYPE.SERVICE}_Searches"
                                api-url="/Searches"
                                :reactive-api-url=false
                                :fields="columns['${C.SEARCH_TYPE.SERVICE}']"
                                :per-page="10"
                                :append-params="{'type': '${C.SEARCH_TYPE.SERVICE}'}"
                                data-path="records"			                        
                                pagination-path="pagination"
                                :css="tableCls">
                                <div slot="alerts" slot-scope="props">
                                    <div class="switcher switcher-success">
                                        <input type="checkbox" name="ad_type" :id="props.rowData.id + '-bs'" v-model="props.rowData.alerts" @change="onAlertChange('${C.SEARCH_TYPE.SERVICE}', props.rowData)">
                                        <label :for="props.rowData.id + '-bs'"></label>
                                    </div>
                                </div>
                                <div slot="actions" slot-scope="props" class="actions">
                                    <a v-tooltip.top="'Search'" class="action" @click="onActionClick('${C.SEARCH_TYPE.SERVICE}', 'search', props.rowData)">
                                        <i class="fas fa-search"></i>
                                    </a>
                                    <a v-tooltip.top="'Delete'" class="action text-landsite-red" @click="onActionClick('${C.SEARCH_TYPE.SERVICE}', 'delete', props.rowData)">
                                        <i class="fas fa-trash-alt"></i>
                                    </a>
                                </div>
                            </vuetable>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <recent-articles-panel ref="RecentArticles"></recent-articles-panel>
                </div>
            </div>
        `;
    }
}
