export default {
    render() {
        return `
        <div id="header" class="header navbar-default v-comp-dashboard-header">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed navbar-toggle-left" @click="$bus.$emit('toggle-sidebar')">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a :href="websiteUrl" @click="onLinkClick">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a :href="membersUrl" @click="onLinkClick">My Account</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a :href="currentUrl">{{$route.meta.title}}</a>
                    </li>
                </ol>
            </div>
  
            <ul class="navbar-nav navbar-right">
                <li>
                  <a :href="servicesUrl" class="icon" @click="onLinkClick">
                    <i class="fas fa-business-time"></i> 
                    <span class="d-none d-lg-inline">Search for Professional Services</span>    
                  </a>
                </li>
                <li>
                  <a :href="propertiesUrl" class="icon" @click="onLinkClick">
                    <i class="fas fa-building"></i> 
                    <span class="d-none d-lg-inline">Search for Properties</span>    
                  </a>
                </li>
                <li>
                  <a :href="websiteUrl" class="icon text-danger logout" @click="onLinkClick">
                    <i class="fas fa-external-link-alt"></i> 
                    <span class="d-none d-lg-inline">Exit Dashboard</span>    
                  </a>
                </li>
            </ul>
        </div>`;
    }
}
