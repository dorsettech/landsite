export default {
    render() {
        return `
            <div id="content" class="content">
                <transition name="fade-fast">
                    <keep-alive>
                        <router-view :user="user" :settings="settings"></router-view>
                    </keep-alive>
                </transition>                
            </div>
        `;
    }
}
