import C from '../../config/constants';

export default {
    render() {
        return `
            <div class="v-comp-module-body v-comp-stats-layout v-comp-property-stats">
                <loader :active=loading></loader>
                <h1 class="page-header">{{property.title}}</h1>
                <h6 class="page-description">
                    View your property listing, click to edit your property details or take a look at your property statistics.
                </h6>                
                <div class="row">
                    <div class="col-lg-12 col-xl-4">
                        <div class="panel">
                            <div class="panel-body p-30 position-relative">
                                <span class="under-offer" v-if="property.under_offer">Under Offer</span>
                                <images-carousel :images="images" :counter=true ref="ImagesCarousel"></images-carousel>
                                <div class="status" :class="statusCls">
                                    <span class="price one" v-if="property.purpose === '${C.PROPERTY_PURPOSE.SALE}' && property.price_sale_per !== '${C.PROPERTY_PRICE_SALE_PER.PRICE_ON_APPLICATION}'">{{settings.currency.symbol}}{{property.price_sale.toLocaleString()}} {{priceSaleUnit}} <span class="qualifier">{{priceSaleQualifierName}}</span></span>
                                    <span class="price one" v-else-if="property.purpose === '${C.PROPERTY_PURPOSE.SALE}'">${C.PROPERTY_PRICE_SALE_PER_NAMES.POA}</span>
                                    
                                    <span class="price one" v-if="property.purpose === '${C.PROPERTY_PURPOSE.RENT}' && property.price_rent_per !== '${C.PROPERTY_PRICE_RENT_PER.PRICE_ON_APPLICATION}'">{{settings.currency.symbol}}{{property.price_rent.toLocaleString()}} {{priceRentUnit}} <span class="qualifier">{{priceRentQualifierName}}</span></span>
                                    <span class="price one" v-else-if="property.purpose === '${C.PROPERTY_PURPOSE.RENT}'">${C.PROPERTY_PRICE_RENT_PER_NAMES.POA}</span>
                                    
                                    <span class="price" v-if="property.purpose === '${C.PROPERTY_PURPOSE.BOTH}'" v-html="pricesTextBoth"></span>
                                        
                                    <span class="status-text">
                                        <i class="fas" :class="iconCls"></i> {{statusText}}
                                    </span>
                                </div>
                                <div class="details text-center">
                                    <p v-if="message" class="message">{{message}}</p>
                                    <h4 class="location">{{property.location}}</h4>
                                    <hr/>
                                    <p class="text-left headline">{{property.headline}}</p>
                                    <button type="button" class="btn btn-secondary btn-large" @click="onEditClick">
                                        <i aria-hidden="true" class="fas fa-edit"></i> 
                                        Edit property details
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-8">
                        <div class="panel chart-panel" :class="{'chart-panel': isCounterVisible }">
                            <div class="panel-body p-30 position-relative">
                                <h5>Property Stats</h5>
                                <div class="row chart-wrapper">
                                    <property-stats-chart 
                                        :chart-data="stats" 
                                        v-show="isChartAvailable && !rotateRequired"
                                        @rotate-required="onChartRotateRequired">
                                    </property-stats-chart>
                                    <p class="text-muted text-center" v-show="!isChartAvailable">No chart data available<br/>No data to display</p>
                                    <img src="/members/assets/rotate-to-landscape.png" class="rotate-device" alt="Rotate device to landscape view" v-show="isChartAvailable && rotateRequired"/>
                                </div>
                                <div class="row counter-wrapper" v-show="isCounterVisible">
                                    <div class="col-md-8">
                                        <h3 class="counter-msg">{{counterMsg}}</h3>
                                        <p class="counter-info">
                                            We will notify you when your profile is due to expire. To ensure your profile remains published, please visit <a class="link" @click="onEditClick">Property Page</a> and resubmit your property information. If you no longer wish to showcase your property after expiry, no action is required.
                                        </p>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="counter">
                                            <span class="i">
                                                <span>{{counter.days}}</span>
                                                <small>days</small>
                                            </span>
                                            <span class="i">
                                                <span>{{counter.hours}}</span>
                                                <small>hours</small>
                                            </span>
                                            <span class="i">
                                                <span>{{counter.minutes}}</span>
                                                <small>min</small>
                                            </span>
                                            <span class="i">
                                                <span>{{counter.seconds}}</span>
                                                <small>sec</small>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <property-enquiries ref="Enquiries" :per-page=1000 title="Property Enquiries"></property-enquiries>
            </div>
        `;
    }
}
