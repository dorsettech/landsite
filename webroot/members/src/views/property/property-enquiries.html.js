export default {
    render() {
        return `
            <div class="row v-comp-property-enquiries">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-body p-30">
                            <h5>{{title}}</h5>
                            <vuetable ref="datagrid"
                                api-url="/Properties/enquiries"
                                :reactive-api-url=false
                                :sort-order="sortOrder"
                                :fields="columns"
                                :append-params="params"
                                :per-page=perPage
                                :load-on-start=loadOnStart
                                data-path="records"			                        
                                :css="tableCls"
                                pagination-path=""
                                @vuetable:loading="onLoading"
                                @vuetable:loaded="onLoaded">
                                <div slot="actions" slot-scope="props" class="actions">
                                    <a class="action" @click="onViewClick(props.rowData)">
                                        <i class="far fa-eye"></i> View
                                    </a>
                                </div>
                            </vuetable>
                            <modal v-if="enquiry.show" @close="enquiry.show = false">
                                <h3 slot="header">{{enquiry.title}}</h3>
                                <p slot="body">{{enquiry.body}}</p>
                            </modal> 
                        </div>
                    </div>
                </div>
            </div>
        `;
    }
}
