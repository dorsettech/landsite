import C from '../config/constants';

export default {
    render() {
        return `
<div class="account">
    <div class="container">
        <div class="row justify-content-center">
            <div class="logo col">
                <a href="//${C.URL.WWW}">
                    <img src="/members/assets/logo.png" alt="logo" class="img-fluid">
                </a>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 col-xxl-4">
                <div class="login accordion">
                    <div class="content">
                        <div class="heading">Login to your account</div>
                            <form class="v-comp-login-form" @submit.prevent="onSubmit">
                                <div class="form-row">
                                    <div class="form-group col-12 col-sm-6 col-md-12 col-lg-6">
                                        <input type="email" class="form-control" 
                                        name="email"
                                        :class="{'is-invalid': form.email.isInvalid}" 
                                        v-model.trim="form.email.value" 
                                        @change="onChange('email', $event)" 
                                        @invalid.prevent="form.email.isInvalid=true" placeholder="Email *" required>
                                        <div class="invalid-feedback">Please enter a valid email address</div>
                                    </div>
                                    <div class="form-group col-12 col-sm-6 col-md-12 col-lg-6">
                                        <input type="password" class="form-control"
                                        name="password" 
                                        :class="{'is-invalid': form.password.isInvalid}" 
                                        v-model.trim="form.password.value" 
                                        @change="onChange('password', $event)" 
                                        @invalid.prevent="form.password.isInvalid=true" placeholder="Password *" minlength="6" required>
                                        <div class="invalid-feedback">Password is required</div>
                                    </div>
                                    <div class="form-group col-12">
                                        <button type="submit" class="login btn btn-primary" :aria-disabled="isWorking" :disabled="isWorking">Login now</button>
                                    </div>
                                    <transition name="fade">
                                        <div class="form-group col-12" v-show="message">
                                            <div class="alert text-center" :class="{'text-danger': hasError, 'text-success': !hasError }" role="alert">{{message}}</div>
                                        </div>
                                    </transition>
                                    <div class="signin forgot col-12">
                                        Forgot password? <a href="/forgot-password">Recover now</a>
                                    </div>
                                    <div class="signin col-12">
                                        Don’t have an account? <a href="/register">Register now</a>
                                    </div>
                                </div>
                            </form>
                        <img src="/members/assets/image.jpg" class="step1image">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`;
    }
}
