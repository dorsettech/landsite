export default {
    render() {
        return `
                <div class="v-comp-recent-articles-panel panel w-100">
                    <div class="panel-body position-relative p-30">
                        <h4 class="m-b-20">Recent News Added</h4>
                        <router-link to="/preferred-insights" title="Preferred News - settings" class="link f-w-600">
                            <i class="fas fa-edit"/> Choose your preferred insights
                        </router-link>
                        <div class="row posts">
                            <p class="no-data" v-if="articles.length === 0">Sorry, but there are no records you preferred yet.</p>
                            <article-box-item
                                v-for="(item, index) in articles"
                                v-bind:item="item"
                                v-bind:index="index"
                                v-bind:key="item.id">
                            </article-box-item>                            
                        </div>
                    </div>
                </div>                                                
            `;
    }
}
