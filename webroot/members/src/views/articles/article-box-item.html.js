import C from './../../config/constants';

export default {
    render() {
        return `
                <div class="v-comp-article-box-item col-md-12 col-lg-4 col-xl-3">
                    <div class="box">
                        <div class="image">
                            <img v-if="item.image" :src="item.image_path" :alt="item.title" @error="onImageSrcError">                
                            <img v-else src="/members/assets/${C.IMAGE_PLACEHOLDER}" :alt="user.full_name">                            
                            <div class="date">{{publishDate}}</div>
                        </div>
                        <div class="p-15">
                            <h5 class="title">{{item.title}}</h5>
                            <div class="author">Posted by <a :href=companyUrl v-if="item.poster.service" @click="onExit">{{item.poster.service.company}}</a> <span v-else-if="item.poster.user_detail.company">{{item.poster.user_detail.company}}</span> <span v-else>The Land Site</span></div>
                            <div class="dsc" v-html="headline"></div>
                            <a :href=articleUrl class="btn btn-large btn-primary" @click="onExit">Read more</a>
                        </div>
                    </div>                            
                </div>
            `;
    }
}
