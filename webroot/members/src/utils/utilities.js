export default class {
    static empty(obj) {
        return (!obj || typeof obj === 'undefined' || String(obj).trim() === '');
    }

    static emptyObject(obj) {
        return Object.keys(obj).length === 0 && obj.constructor === Object;
    }

    static isObject(obj) {
        return obj === Object(obj);
    }

    static debounce(func, wait, immediate) {
        var timeout;
        return function () {
            var context = this, args = arguments;
            var later = function () {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    }

    static guid() {
        return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
            (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16))
    }

    static guids(...keys) {
        let obj = {};
        keys.forEach(function (value) {
            obj[value] = guid();
        });
        return obj;
    }

    static extract(obj, context) {
        context = context || window;
        for (var key in obj) context[key] = obj[key];
    }

    static get(obj, str) {
        str = str || '';
        try {
            return str.split('.').reduce(function (o, x) {
                return o[x]
            }, obj);
        } catch (e) {
            console.warn(obj, e.message + ' 🠒 ' + str);
        }
    }

    static format(str, ...args) {

        for (var k in args) {
            str = str.replace(new RegExp("\\{" + k + "\\}", 'g'), args[k]);
        }
        return str
    }

    static capitalize(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    static deepCopy(o) {
        var newO,
            i;

        if (typeof o !== 'object') {
            return o;
        }
        if (!o) {
            return o;
        }

        if ('[object Array]' === Object.prototype.toString.apply(o)) {
            newO = [];
            for (i = 0; i < o.length; i += 1) {
                newO[i] = this.deepCopy(o[i]);
            }
            return newO;
        }

        newO = {};
        for (i in o) {
            if (o.hasOwnProperty(i)) {
                newO[i] = this.deepCopy(o[i]);
            }
        }
        return newO;
    }

    static shallowCopy(o) {
        return {...o};
    }

    static move(arr, from, to) {
        arr.splice(to, 0, arr.splice(from, 1)[0]);
    }

    static stripTags(str) {
        return str.replace(/<\/?[^>]+(>|$)/g, '');
    }

    /**
     * Remove query string parameters (silently)
     */
    static clearUrl() {
        window.history.replaceState({}, document.title, location.hash.split('?')[0]);
    }

    static ip() {
        return new Promise(r => {
            var w = window,
                a = new (w.RTCPeerConnection || w.mozRTCPeerConnection || w.webkitRTCPeerConnection)({iceServers: []}),
                b = () => {
                };
            a.createDataChannel("");
            a.createOffer(c => a.setLocalDescription(c, b, b), b);
            a.onicecandidate = c => {
                try {
                    c.candidate.candidate.match(/([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g).forEach(r)
                } catch (e) {
                }
            }
        })
    }

    static validate = {
        postcode(value) {
            return /[A-Z]{1,2}[0-9]{1,2}[A-Z]? ?[0-9][A-Z]{2}/i.test(value);
        }
    }
}
