/* *
 *  eConnect4u
 */

//file object as file name
//global variables inside, you can create also private variable if you want to prevent override
if (typeof adminPrefix !== 'undefined') {
    var Main = {
        prefix: adminPrefix, //default admin prefix
        originalOutput: ''
    };

//on load functions
    Main.init = function () {
        Main.setEvents();
    };

//events on site
    Main.setEvents = function () {
        if ($('.swalMessage').length !== 0) {
            Main.swalMessage($('.swalMessage'));
        }
    };

    Main.swalDialog = function (title, action) {
        swal({
            title: title,
            type: "warning",
            showCancelButton: true,
            cancelButtonText: 'Cancel',
            confirmButtonColor: "#ec4758",
            confirmButtonText: "Yes!",
            closeOnConfirm: false
        }, function () {
            var form = $('form[name="' + action + '"]');
            if (typeof form !== 'undefined') {
                form.submit();
            }
        });
    };
    Main.swalMessage = function (selector) {
        var message = selector.data('message');
        var type = selector.data('type');
        swal(message, "", type);
    };

    $(document).ready(Main.init);
}