function generateAlert(type, text) {
    return noty({
        text: text,
        type: type,
        dismissQueue: true,
        layout: 'topRight',
        closeWith: ['click'],
        theme: 'relax',
        timeout: 5000,
        maxVisible: 10,
        animation: {
            open: 'animated bounceInRight',
            close: 'animated bounceOutRight',
            easing: 'swing',
            speed: 500
        }
    });
}