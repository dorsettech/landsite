$(document).ready(function () {
    var _primary = CodeMirror.fromTextArea(document.getElementById("codeMirror_primary"), {
        lineNumbers: true
    });
    var _secondary = CodeMirror.fromTextArea(document.getElementById("codeMirror_secondary"), {
        lineNumbers: true
    });
    _primary.on('change', function () {
        activeDiffWrapper(_primary, _secondary);
    });
    _secondary.on('change', function () {
        activeDiffWrapper(_primary, _secondary);
    });

    activeDiffWrapper();
});

function activeDiffWrapper(primary, secondary) {
    if (primary instanceof CodeMirror) {

        $(".diffWrapper").prettyTextDiff({
            originalContent: primary.getValue(),
            changedContent: convertString(secondary.getValue()),
            diffContainer: ".diff"
        });
    } else {
        //default initialization
        $(".diffWrapper").prettyTextDiff({
            originalContent: $('#codeMirror_primary').val(),
            changedContent: convertString($('#codeMirror_secondary').val()),
            diffContainer: ".diff"
        });
    }
    var diff = $('.diff').html();
    diff = convertString(diff);
    if (diff.length <= 0) {
        diff = '/* custom.css */';
    }
    $("#diff").val(diff);
}

function convertString(string) {
    var string = string.replace(/<span>/g, '');
    string = string.replace(/<\/span>/g, '');
    string = string.replace(/<ins>/g, '');
    string = string.replace(/<\/ins>/g, '');
    string = string.replace(/<del>/g, '');
    string = string.replace(/<\/del>/g, '');
    string = string.replace(/\\r\\n/g, '');
    string = string.replace(/&gt;/g, '>');
    string = string.replace(/&lt;/g, '<');
    string = string.replace(/<br\s*\/?>/mg, "\n");
    return string;
}