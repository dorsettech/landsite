var inlineEdit = {
    data: {
        id: null,
        column: null
    },
    resetData: function () {
        this.data.id = null;
        this.data.column = null;
    },
    sendRequest: function () {
        this.data[this.data.column] = $('input#inline-edit-input-' + this.data.column + this.data.id).val();
        this.showLoader();
        that = this;

        $.ajax({
            url: window.location.href + '/changeColumnValue',
            method: 'POST',
            data: this.data
        }).success(function (resp) {
            if (resp.status) {
                generateAlert('success', resp.message);
                $('input#inline-edit-input-' + that.data.column + that.data.id).val(resp.value);
                $('#inline-edit-anchor-' + that.data.column + that.data.id).html(that.escapeHtml(resp.value));
            } else {
                generateAlert('error', resp.message);
            }
            that.hideLoader();
            that.closeInlineEdit();
        });
    },
    showLoader: function () {
        $('#inline-edit-loader-' + this.data.column + this.data.id).removeClass('hidden');
        $('#inline-edit-actions-' + this.data.column + this.data.id).addClass('hidden');
    },
    hideLoader: function () {
        $('#inline-edit-loader-' + this.data.column + this.data.id).addClass('hidden');
        $('#inline-edit-actions-' + this.data.column + this.data.id).removeClass('hidden');
    },
    openInlineEdit: function (id, model, column) {
        $('.inline-edit-box').addClass('hidden');
        this.data.id = id;
        this.data.column = column;
        this.data.model = model;
        $('#inline-edit-box-' + column + id).removeClass('hidden');
        $('#inline-edit-anchor-' + column + id).addClass('hidden');
    },
    closeInlineEdit: function () {
        $('#inline-edit-box-' + this.data.column + this.data.id).addClass('hidden');
        $('#inline-edit-anchor-' + this.data.column + this.data.id).removeClass('hidden');
        this.resetData();
    },
    escapeHtml: function (text) {
        var map = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;'
        };

        return text.replace(/[&<>"']/g, function (m) {
            return map[m];
        });
    }
};


$(document).ready(function () {
    $('.inline-edit-anchor').click(function () {
        inlineEdit.openInlineEdit($(this).attr('data-id'), $(this).attr('data-model'), $(this).attr('data-column'));
    });
    $('.inline-edit-save').click(function () {
        inlineEdit.sendRequest();
    });
    $('.inline-edit-cancel').click(function () {
        inlineEdit.closeInlineEdit();
    });
});
