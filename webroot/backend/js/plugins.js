//Every plugins on administration panel
//TIP: insert plugins before function loadAll()
var Plugins = {
    magnificPopUp: function () {
        // open one image; add class to anchor
        $('.image-link').magnificPopup({type: 'image'});
        // multiple images in container; add class to container without class on anchors
        $('.image-links').magnificPopup({delegate: 'a', type: 'image'});
        // multiple images in container as a gallery (arrows); add class to container without class on anchors
        $('.image-gallery').magnificPopup({delegate: 'a', type: 'image', gallery: {enabled: true}});
    },
    superBox: function () {
        $('.superbox').SuperBox();
    },
    datePicker: function () {
        $('.datepicker').datepicker({
            startDate: '+1d',
            format: 'yyyy-mm-dd'
        });
    },
    tooltip: function () {
        $('[data-toggle="tooltip"]').tooltip();
    },
    codeMirror: function () {
        var mirror = $('.codeMirror'),
                mode = typeof mirror.data('type') === 'undefined' ? "text/x-scss" : mirror.data('type'),
                readonly = typeof mirror.data('readonly') === 'undefined' ? false : mirror.data('readonly');

        if (typeof CodeMirror !== 'undefined' && typeof mirror[0] !== 'undefined') {
            var editor = CodeMirror.fromTextArea(document.getElementById("codeMirror"), {
                viewportMargin: Infinity,
                lineNumbers: true,
                matchBrackets: true,
                mode: mode,
                extraKeys: {"Ctrl-Space": "autocomplete", "Alt-F": "findPersistent"},
                readOnly: readonly
            });
        }
    },
    CkEditorSelect: function () {
        $('.media-file-select').click(function () {
            funcNum = $(this).attr('data-ckeditor');
            fileUrl = $(this).attr('data-url');

            window.opener.CKEDITOR.tools.callFunction(funcNum, '/' + fileUrl);
            window.close();
        });
    },
    choosenSelect: function () {
        $('.chosen-select').chosen();
    },
    dragAndDropMenu: function () {
        $('#menu-page').nestable({
            handleClass: 'dd-handle-custom',
            group: 1
        }).on('change', Main.updateOutput);
    },
    dateRangePicker: function () {
        $('.datetimepicker').datetimepicker({
            locale: 'pl'
        });
    },
    loadAll: function () {
        for (var key in Plugins) {
            if (key != 'loadAll') {
                Plugins[key]();
            }
        }
    }

};
$(document).ready(Plugins.loadAll);

//Old choosen select if need
//   var config = {
//        '.chosen-select': {},
//        '.chosen-select-deselect': {allow_single_deselect: true},
//        '.chosen-select-no-single': {disable_search_threshold: 10},
//        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
//        '.chosen-select-width': {width: "95%"}
//    }
//    for (var selector in config) {
//        $(selector).chosen(config[selector]);
//    }

