$(document).ready(function () {
    Gallery.runSortable();
    $('.editSingleImage').click(function () {
        Gallery.showEditModal($(this));
    });
    $('#saveSingleImage').click(function () {
        Gallery.updateSingleImage();
    });
    $('.deleteSingleImage').click(function () {
        Gallery.deleteSingleImage($(this));
    });
});

var Gallery = {
    _model: null,
    _entityId: null,
    _field: 'gallery',
    _key: null,
    runSortable: function () {
        var that = this;
        $('#sortable-view').sortable({
            stop: function (event, ui) {
                var data = {};
                $('.gallery-item').each(function () {
                    data[$(this).attr('data-key')] = $(this).index();
                    $(this).attr('data-key', $(this).index());
                });
                that.sendRequest(data, "updateGalleryOrder.json", 'post');
            }
        });
    },
    showEditModal: function (element) {
        this._key = element.parents('.gallery-item').attr('data-key');
        $.getJSON('/' + adminPrefix + '/Gallery/getSingleImageData.json', {model: this._model, entityId: this._entityId, key: this._key, field: this._field}, function (resp) {
            $('#editImageModal .image-handler').css('backgroundImage', 'url(' + resp.path + ')');
            $('#editImageModal').find('input[name="title"]').val(resp.title);
            $('#editImageModal').find('input[name="alt"]').val(resp.alt);
        });
        $('#editImageModal').modal('show');
    },
    updateSingleImage: function () {
        var data = {
            title: $('#updateSingleImageForm input[name=title]').val(),
            alt: $('#updateSingleImageForm input[name=alt]').val()
        };
        this.sendRequest(data, "updateSingleImage.json", 'post');
        $('#editImageModal').modal('hide');
    },
    deleteSingleImage: function (element) {
        this._key = element.parents('.gallery-item').attr('data-key');
        this.sendRequest({}, "deleteSingleGalleryImage/" + this._model + '/' + this._entityId + '/' + this._field + '/' + this._key + ".json", 'delete');
        element.parents('.gallery-item').remove();
    },
    sendRequest: function (data, action, method) {
        var that = this;
        $.ajax({
            url: "/" + adminPrefix + "/Gallery/" + action,
            data: {data: data, model: that._model, entityId: that._entityId, key: that._key, field: that._field},
            method: method
        }).success(function (resp) {
            generateAlert('success', resp.message);
        }).error(function (jqXHR, textStatus, error) {
            generateAlert('danger', resp.message);
        });
    }
};