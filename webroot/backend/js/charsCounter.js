$(document).ready(function () {
    var title = document.getElementById("seo_title");
    var keywords = document.getElementById("seo_keywords");
    var description = document.getElementById("seo_description");
    if (title !== null && keywords !== null && description !== null) {
        if (title.value !== '') {
            $('.seo_title_counter').html(title.value.length);
        }
        if (keywords.value !== '') {
            $('.seo_keywords_counter').html(keywords.value.length);
        }
        if (description.value !== '') {
            $('.seo_description_counter').html(description.value.length);
        }
    }

    $('#seo_title').keyup(function () {
        var textLength = $('#seo_title').val().length;
        $('.seo_title_counter').html(textLength);
        GrayRedBackground('.seo_title_counter', textLength, 60);
    });
    $('#seo_keywords').keyup(function () {
        var textLength = $('#seo_keywords').val().length;
        $('.keywords_counter').html(textLength);
        GrayRedBackground('.seo_keywords_counter', textLength, 320);
    });
    $('#seo_description').keyup(function () {
        var textLength = $('#seo_description').val().length;
        $('.description_counter').html(textLength);
        GrayRedBackground('.seo_description_counter', textLength, 320);
    });
    function bindSeoTagCounter(tag, limit = 320) {
        if ($('input[name="seo_' + tag + '"]').length ){
            $('label[for="seo_' + tag + '"]').after(' <span class="badge seo_' + tag + '_counter"></span>');
            $('input[name="seo_' + tag + '"]').attr('maxlength', limit);
            $('.seo_' + tag + '_counter').text($('input[name="seo_' + tag + '"').val().length);
            $(document).on('keyup', 'input[name="seo_' + tag + '"]', function () {
                var textLength = $('input[name="seo_' + tag + '"').val().length;
                $('.seo_' + tag + '_counter').html(textLength);
            });
        }
    }
    bindSeoTagCounter('title', 1024);
    bindSeoTagCounter('description', 1024);
    bindSeoTagCounter('keywords', 1024);
    function GrayRedBackground(element, lenght, max) {
        if (lenght > max) {
            $(element).css('background-color', 'red');
        } else {
            $(element).css('background-color', '#D1DADE');
        }
    }
});


