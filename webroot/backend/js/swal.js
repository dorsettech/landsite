var Swal = {
    listeners: function () {
        //need to be fix, if someone want to pop multiple swal
        var element = $('.swalMessage');
        if (element.length !== 0) {
            var msg = element.data('message'),
                    type = element.data('type');
            Swal.display(msg, type);
        }
    },
    display: function (msg, type) {
        swal(msg, "", type);
    },
    remove: function (title, action) {
        swal({
            title: title,
            type: "warning",
            showCancelButton: true,
            cancelButtonText: 'Cancel',
            confirmButtonColor: "#ec4758",
            confirmButtonText: "Yes!",
            closeOnConfirm: false
        }, function () {
            var form = $('form[name="' + action + '"]');
            if (typeof form !== 'undefined') {
                form.submit();
            }
        });
    }
};

$(document).ready(Swal.listeners);

