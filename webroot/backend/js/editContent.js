$('.lightBoxGallery .file').mousemove(function () {
    $(this).addClass('pulse');
});

$('.lightBoxGallery .file').mouseleave(function () {
    $(this).removeClass('pulse');
});

$(document).ready(function () {
    if ($(".lightBoxGallery").length > 0) {
        $(".lightBoxGallery").sortable({
            scroll: true,
            tolerance: "pointer",
            revert: false,
            deactivate: function (event, ui) {
                var tab = [];
                var position = 0;
                var page_id = $(this).attr('data-num');
                $(".lightBoxGallery li").each(function () {
                    var tmp = $(this).find('.file-box').attr('id');
                    var id = tmp.replace("f", "");
                    tab[position] = id;
                    position++;
                });
                saveGalleryFotoOrder(page_id, tab);
            }
        });
        $(".lightBoxGallery").disableSelection();

        $(".lightBoxGallery > li .status").click(function () {
            var id = parseInt($(this).attr('data-text'));
            switchStatus(id);
        });
    }
});

function switchStatus(id) {
    $.ajax({
        type: "POST",
        url: adminPrefix + "/files/switchStatus",
        data: {'id': id},
        dataType: 'json',
        success: function (response) {
            if (response.status == 'success') {
                var _this = $("#f" + id + " .status > i").removeClass('fa-check').removeClass('fa-times');
                var switchClass = (response.data.active == 1 ? 'fa-check' : 'fa-times');
                _this.addClass(switchClass);

                swal({
                    title: "OK! Change status",
                    text: "",
                    type: "info",
                    timer: 1000,
                    showConfirmButton: false
                });
            }
        }, error: function () {
            swal("Error", "Bad request switchStatus", "error");
        }
    });
}

function saveGalleryFotoOrder(page_id, tab) {
    $.ajax({
        type: "POST",
        url: adminPrefix + "/files/saveGalleryFotoOrder",
        data: {'data': tab, 'page_id': page_id},
        dataType: 'json',
        success: function (response) {
            if (response.status == 'success') {
                swal("Saved", "Click to close!", "success")
            } else {
                swal("NOT saved", "Write message to Administrator!", "error")
            }
        }, error: function () {
            swal("Error", "Bad request", "error")
        }
    });
}

$('.delete-foto').click(function () {
    var _this = $(this);
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, delete it!",
    }, function (isConfirm) {
        if (isConfirm) {
            var url = _this.attr('data-href');
            var tab = url.split("/");
            tab.reverse();
            var id = parseInt(tab[0]);
            if (id > 0) {
                $("#f" + id).remove();
                document.location = url;
            }
        } else {
            swal.close();
        }
    });
    return false;
});