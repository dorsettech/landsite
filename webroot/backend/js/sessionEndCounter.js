//sessionlifetime, userEmail, adminPrefix set in head.ctp
$(document).ready(function () {
    if (typeof sessionlifetime !== 'undefined' || typeof userEmail !== 'undefined') {
        var sessionLifeTimeInMiliseconds = sessionlifetime * 1000;
        var elem = document.getElementById("sessionTimeBar");
        var screen = $(document).find('#locked-screen');
        var width = 100;
        var data = {};
        var counter = 0;
        var sessionWarningAlert;
        if (elem !== null) {
            elem.style.width = '100%';
        }
        setInterval(frame, sessionLifeTimeInMiliseconds / width);
        $('#user_email').append(userEmail);
        $('.locked-screen-form').submit(function (e) {
            e.preventDefault();
            var formData = $(this).serializeArray();
            $('#locked-screen-password').val('');
            formData = {
                email: userEmail,
                password: formData[1]['value'],
            };
            $.ajax({
                url: '/' + adminPrefix + '/dashboard/login',
                method: 'POST',
                data: formData
            }).success(function (resp) {
                if (resp.status === true) {
                    screen.removeClass('vis').addClass('hid').addClass('invisible');
                    restartBar();
                    counter = 0;
                } else {
                    counter++;
                    if (counter > 2) {
                        window.location.replace(adminPrefix + "/dashboard/login");
                    } else {
                        alertGenerator('error', "Incorrect data, please try again.", 3000);
                    }
                }
            });
        });
    }

    function restartBar() {
        width = 100;
        elem.style.width = '100%';
    }

    function frame() {
        if (elem !== null) {
            width--;
            elem.style.width = width + '%';
            if (width === 10) {
                sessionWarningAlert = alertGenerator('warning', "Your session will expire for a moment.<br>Please click to renew it.", null);
            } else if (width === 0) {
                sessionWarningAlert.closeCleanUp();
                screen.removeClass('invisible').removeClass('hid').addClass('vis');
            }
        }
    }

    function alertGenerator(type, text, sessionLT) {
        return noty({
            text: text,
            type: type,
            dismissQueue: true,
            layout: 'topCenter',
            theme: 'relax',
            timeout: sessionLT,
            callback: {
                onCloseClick: function () {
                    if (type === "error") {
                        data.message = "Redirect";
                    } else if (type === "warning") {
                        data.message = "Renew";
                    }
                    $.ajax({
                        url: adminPrefix + '/dashboard/refreshOnAlertClick',
                        method: 'POST',
                        data: data
                    }).success(function (resp) {
                        if (resp.status === true) {
                            restartBar();
                        }
                    });
                }
            },
            animation: {
                open: 'animated bounceInDown',
                close: 'animated bounceOutUp',
                easing: 'swing',
                speed: 500
            }
        });
    }
});
