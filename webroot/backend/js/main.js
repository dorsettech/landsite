var Main = {
    originalOutput: '',
    currentInstance: null,
    listeners: function () {
        $(document).on('click', '.btn-show-details,.btn-hide-details', Main.detailsToggle)
                .on('click', '.tab-title', Main.pageTabs)
                .on('click', '#savePageButton', Main.handleRequiredFields)
                .on('change', '.select-menu', Main.updateSelectPage)
//                .on('click', '#openMediaLibrary', Main.openMediaLibrary)
                .on('change', '.select-pagetype', Main.checkRedirectionType)
                .on('click', '.openVisualComposer', Main.startVisualComposer)
                .on('click', '#hideVisualComposer', Main.hideVisualComposer)
                .on('click', '#updateComposerText', Main.updateComposerText)
                .on('keyup', "#name", Main.updateUrlname)
                .on('keyup', "#title", Main.updateUrlname);

        //autoload
        Main.activateTabs();
        // output initial serialized data
        Main.updateOutput($('#menu-page').data('output', $('#menu-page-output')));
    },
    detailsToggle: function (event) {
        var target = $(event.currentTarget),
                targetText = target.find('span'),
                content = $('.page-contents'),
                text = target.hasClass('active') ? target.data('show') : target.data('hide');

        targetText.html(text);
        content.toggleClass('hidden');
        target.toggleClass('active');
    },
    pageTabs: function (event) {
        var $this = $(event.currentTarget),
                toggle = $this.data('toggle'),
                elements = $this.parent('ul').find('li.' + toggle + '');
        $this.children('.caret').toggleClass('reversed');
        elements.slideToggle();
    },
    handleRequiredFields: function () {
        var error = 0,
                tab = null;
        $(':input[required]').each(function () {
            var $this = $(this),
                    value = $this.val();
            if (value.trim() === '') {
                error = 1;
                tab = $this.closest('.tab-pane').attr('id');
                $this.css('border', '1px solid red').attr('placeholder', 'This field is required');
                return false;
            }
        });
        if (error) {
            $('.nav-tabs a[href="#' + tab + '"]').tab('show');
            return false;
        }
    },
    updateSelectPage: function (event) {
        var target = $(event.currentTarget),
                value = target.val(),
                model = target.attr('data-model');

        //change parent element (select)
        Main.getSelectEntitiesByAjax(model, value);
    },
    checkRedirectionType: function (event) {
        var target = $(event.currentTarget),
                value = target.val(),
                pageRedirection = $('.page-redirection');

        // display redirection tab on select page change
        if (value === '4202ef115ebede37eb22297113f5fb32') {  // md5('Redirect')
            pageRedirection.removeClass('hidden');
        } else {
            pageRedirection.addClass('hidden');
        }
    },
    activateTabs: function () {
        var url = document.location.toString();
        if (url.match('#')) {
            $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
        }
        $('.nav-tabs a').on('shown.bs.tab', function (e) {

            window.location.hash = e.target.hash;
            var currentID = $('.tab-pane.active').find('textarea').attr('id');
            Main.currentInstance = currentID;
        });

        //set active ckeditor
        var currentID = $('.tab-pane.active').find('textarea').attr('id');
        Main.currentInstance = currentID;
    },
    //update Menu page
    updateOutput: function (e) {
        var list = e.length ? e : $(e.target),
                output = list.data('output');
        if (window.JSON) {
            page = window.JSON.stringify(list.nestable('serialize')); //, null, 2));
            if (typeof output !== 'undefined') {
                output.val(page);
            }
            // set the original page json string for comparing
            if (Main.originalOutput.length === 0) {
                Main.originalOutput = page;
            }
            // enable/disable update page button
            pageUpdateForm = $('page-update-form');
            pageUpdateButton = $('#page-update');
            if (page !== Main.originalOutput) {
                pageUpdateButton
                        .removeClass('disabled')
                        .removeClass('btn-default')
                        .addClass('btn-primary')
                        .removeAttr('disabled');
            } else {
                pageUpdateButton
                        .addClass('disabled')
                        .addClass('btn-default')
                        .removeClass('btn-primary')
                        .attr('disabled', 'disabled');
            }
        } else {
            output.val('JSON browser support required for this demo.');
        }
    },
    getSelectEntitiesByAjax: function (controller, id) {
        $.ajax({
            type: 'POST',
            url: adminPrefix + '/' + controller + '/ajaxList',
            data: {'id': id},
            success: function (data) {
                selectEntities = $('select.' + controller + '');
                selected = selectEntities.find(":selected");
                var slct = new Array();
                for (s = 0; s < selected.length; s++) {
                    slct[selected[s]['value']] = selected[s]['text'];
                }
                for (i = 0; i < selectEntities.length; i++) {
                    selectEntities[i].options.length = 1;
                    for (c = 0; c < data.length; c++) {
                        option = data[c];
                        if (typeof (slct[option.value]) != 'undefined') {
                            selectEntities[i].options.add(new Option(option.text, option.value, true, true));
                        } else {
                            selectEntities[i].options.add(new Option(option.text, option.value));
                        }
                    }
                }
            }
        });
    },
    startVisualComposer: function (event) {
        var element = $(event.currentTarget);
        $('#' + element.data('target')).fadeIn();
        var content = $("#contentarea");
        content.contentbuilder({
            snippetFile: 'webroot/backend/snippets/snippets.html',
            snippetOpen: true,
            toolbar: 'top',
//            iconselect: 'assets/ionicons/selecticon.html'
        });
        var currentValue = $('.tab-pane.active').find('textarea').val();
        content.data('contentbuilder').loadHTML(currentValue);
        $('html, body').animate({scrollTop: '0px'}, 300);
    },
    hideVisualComposer: function (event) {
        var element = $(event.currentTarget);
        $('#' + element.data('target')).fadeOut();
        $("#contentarea").data('contentbuilder').destroy();
        $('html, body').animate({scrollTop: '0px'}, 300);
    },
    updateComposerText: function (event) {
        Main.save();
    },
    save: function () {
        //Save all images first
        $("#contentarea").saveimages({
            handler: 'webroot/backend/js/plugins/contentbuilder/saveimage.php',
            onComplete: function () {

                //Then save the content
                var HTML = $('#contentarea').data('contentbuilder').html(); //Get content
                CKEDITOR.instances[Main.currentInstance].setData(HTML);
                $('.tab-pane.active').find('textarea').html(HTML);
                $('#visual-composer-modal').fadeOut();
                $("#contentarea").data('contentbuilder').destroy();
            }
        });
        $("#contentarea").data('saveimages').save();

    },
//    openMediaLibrary: function () {
//        var iframe = $("<iframe id='fm-iframe' class='fm-modal'/>").attr({
//            src: 'panel/fileBrowser' + // Change it to wherever  Filemanager is stored.
//                    '?CKEditorFuncNum=' + CKEDITOR.instances[event.editor.name]._.filebrowserFn +
//                    '&CKEditorCleanUpFuncNum=' + cleanUpFuncRef +
//                    '&langCode=en' +
//                    '&CKEditor=' + event.editor.name
//        });
//
//        $("body").append(iframe);
//        $("body").css("overflow-y", "hidden");  // Get rid of possible scrollbars in containing document
//    },
    updateUrlname: function (event) {
        $("#urlname").val(Main.slug($(this).val()));
    },
    slug: function (s, opt) {
        s = String(s);
        opt = Object(opt);

        var defaults = {
            'delimiter': '-',
            'limit': undefined,
            'lowercase': true,
            'replacements': {},
            'transliterate': (typeof (XRegExp) === 'undefined') ? true : false
        };

        // Merge options
        for (var k in defaults) {
            if (!opt.hasOwnProperty(k)) {
                opt[k] = defaults[k];
            }
        }

        var char_map = {
            // Latin
            'À': 'A', 'Á': 'A', 'Â': 'A', 'Ã': 'A', 'Ä': 'A', 'Å': 'A', 'Æ': 'AE', 'Ç': 'C',
            'È': 'E', 'É': 'E', 'Ê': 'E', 'Ë': 'E', 'Ì': 'I', 'Í': 'I', 'Î': 'I', 'Ï': 'I',
            'Ð': 'D', 'Ñ': 'N', 'Ò': 'O', 'Ó': 'O', 'Ô': 'O', 'Õ': 'O', 'Ö': 'O', 'Ő': 'O',
            'Ø': 'O', 'Ù': 'U', 'Ú': 'U', 'Û': 'U', 'Ü': 'U', 'Ű': 'U', 'Ý': 'Y', 'Þ': 'TH',
            'ß': 'ss',
            'à': 'a', 'á': 'a', 'â': 'a', 'ã': 'a', 'ä': 'a', 'å': 'a', 'æ': 'ae', 'ç': 'c',
            'è': 'e', 'é': 'e', 'ê': 'e', 'ë': 'e', 'ì': 'i', 'í': 'i', 'î': 'i', 'ï': 'i',
            'ð': 'd', 'ñ': 'n', 'ò': 'o', 'ó': 'o', 'ô': 'o', 'õ': 'o', 'ö': 'o', 'ő': 'o',
            'ø': 'o', 'ù': 'u', 'ú': 'u', 'û': 'u', 'ü': 'u', 'ű': 'u', 'ý': 'y', 'þ': 'th',
            'ÿ': 'y',
            // Latin symbols
            '©': '(c)',
            // Greek
            'Α': 'A', 'Β': 'B', 'Γ': 'G', 'Δ': 'D', 'Ε': 'E', 'Ζ': 'Z', 'Η': 'H', 'Θ': '8',
            'Ι': 'I', 'Κ': 'K', 'Λ': 'L', 'Μ': 'M', 'Ν': 'N', 'Ξ': '3', 'Ο': 'O', 'Π': 'P',
            'Ρ': 'R', 'Σ': 'S', 'Τ': 'T', 'Υ': 'Y', 'Φ': 'F', 'Χ': 'X', 'Ψ': 'PS', 'Ω': 'W',
            'Ά': 'A', 'Έ': 'E', 'Ί': 'I', 'Ό': 'O', 'Ύ': 'Y', 'Ή': 'H', 'Ώ': 'W', 'Ϊ': 'I',
            'Ϋ': 'Y',
            'α': 'a', 'β': 'b', 'γ': 'g', 'δ': 'd', 'ε': 'e', 'ζ': 'z', 'η': 'h', 'θ': '8',
            'ι': 'i', 'κ': 'k', 'λ': 'l', 'μ': 'm', 'ν': 'n', 'ξ': '3', 'ο': 'o', 'π': 'p',
            'ρ': 'r', 'σ': 's', 'τ': 't', 'υ': 'y', 'φ': 'f', 'χ': 'x', 'ψ': 'ps', 'ω': 'w',
            'ά': 'a', 'έ': 'e', 'ί': 'i', 'ό': 'o', 'ύ': 'y', 'ή': 'h', 'ώ': 'w', 'ς': 's',
            'ϊ': 'i', 'ΰ': 'y', 'ϋ': 'y', 'ΐ': 'i',
            // Turkish
            'Ş': 'S', 'İ': 'I', 'Ç': 'C', 'Ü': 'U', 'Ö': 'O', 'Ğ': 'G',
            'ş': 's', 'ı': 'i', 'ç': 'c', 'ü': 'u', 'ö': 'o', 'ğ': 'g',
            // Russian
            'А': 'A', 'Б': 'B', 'В': 'V', 'Г': 'G', 'Д': 'D', 'Е': 'E', 'Ё': 'Yo', 'Ж': 'Zh',
            'З': 'Z', 'И': 'I', 'Й': 'J', 'К': 'K', 'Л': 'L', 'М': 'M', 'Н': 'N', 'О': 'O',
            'П': 'P', 'Р': 'R', 'С': 'S', 'Т': 'T', 'У': 'U', 'Ф': 'F', 'Х': 'H', 'Ц': 'C',
            'Ч': 'Ch', 'Ш': 'Sh', 'Щ': 'Sh', 'Ъ': '', 'Ы': 'Y', 'Ь': '', 'Э': 'E', 'Ю': 'Yu',
            'Я': 'Ya',
            'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'yo', 'ж': 'zh',
            'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o',
            'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'c',
            'ч': 'ch', 'ш': 'sh', 'щ': 'sh', 'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu',
            'я': 'ya',
            // Ukrainian
            'Є': 'Ye', 'І': 'I', 'Ї': 'Yi', 'Ґ': 'G',
            'є': 'ye', 'і': 'i', 'ї': 'yi', 'ґ': 'g',
            // Czech
            'Č': 'C', 'Ď': 'D', 'Ě': 'E', 'Ň': 'N', 'Ř': 'R', 'Š': 'S', 'Ť': 'T', 'Ů': 'U',
            'Ž': 'Z',
            'č': 'c', 'ď': 'd', 'ě': 'e', 'ň': 'n', 'ř': 'r', 'š': 's', 'ť': 't', 'ů': 'u',
            'ž': 'z',
            // Polish
            'Ą': 'A', 'Ć': 'C', 'Ę': 'e', 'Ł': 'L', 'Ń': 'N', 'Ó': 'o', 'Ś': 'S', 'Ź': 'Z',
            'Ż': 'Z',
            'ą': 'a', 'ć': 'c', 'ę': 'e', 'ł': 'l', 'ń': 'n', 'ó': 'o', 'ś': 's', 'ź': 'z',
            'ż': 'z',
            // Latvian
            'Ā': 'A', 'Č': 'C', 'Ē': 'E', 'Ģ': 'G', 'Ī': 'i', 'Ķ': 'k', 'Ļ': 'L', 'Ņ': 'N',
            'Š': 'S', 'Ū': 'u', 'Ž': 'Z',
                    'ā': 'a', 'č': 'c', 'ē': 'e', 'ģ': 'g', 'ī': 'i', 'ķ': 'k', 'ļ': 'l', 'ņ': 'n',
            'š': 's', 'ū': 'u', 'ž': 'z'
        };

        // Make custom replacements
        for (var k in opt.replacements) {
            s = s.replace(RegExp(k, 'g'), opt.replacements[k]);
        }

        // Transliterate characters to ASCII
        if (opt.transliterate) {
            for (var k in char_map) {
                s = s.replace(RegExp(k, 'g'), char_map[k]);
            }
        }

        // Replace non-alphanumeric characters with our delimiter
        var alnum = (typeof (XRegExp) === 'undefined') ? RegExp('[^a-z0-9]+', 'ig') : XRegExp('[^\\p{L}\\p{N}]+', 'ig');
        s = s.replace(alnum, opt.delimiter);

        // Remove duplicate delimiters
        s = s.replace(RegExp('[' + opt.delimiter + ']{2,}', 'g'), opt.delimiter);

        // Truncate slug to max. characters
        s = s.substring(0, opt.limit);

        // Remove delimiter from ends
        s = s.replace(RegExp('(^' + opt.delimiter + '|' + opt.delimiter + '$)', 'g'), '');

        return opt.lowercase ? s.toLowerCase() : s;
    }
};

$(document).ready(Main.listeners);

var d = new Date();
var month = d.getMonth();
var day = d.getDate();
var year = d.getFullYear();

$('.datetime, #datetimelogs').datetimepicker({
    format: 'YYYY-MM-DD HH:mm:ss'
});
$('#createdTo, #dateTo').datetimepicker({
    format: 'YYYY-MM-DD HH:mm:ss',
    defaultDate: new Date(year, month, day, 23, 59)
});