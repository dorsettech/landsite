$(document).ready(function () {
    $('input.onoffswitch-checkbox').change(function () {
        var data = {
            column: $(this).attr('name'),
            id: $(this).attr('data-id')
        };
        $.ajax({
            url: window.location.pathname + '/toggleColumnValue',
            method: 'POST',
            data: data
        }).success(function (resp) {
            if (resp.status) {
                generateAlert('success', resp.message);
            } else {
                generateAlert('error', resp.message);
            }
        });
    });
});
