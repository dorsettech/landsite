var $image = $(".image-crop > img")

$($image).cropper({
    aspectRatio: aspectRatioView,
    preview: ".img-preview",
    done: function (data) {
        $(".img-preview").css("height", data.height / 2 + 'px');
        $(".img-preview").css("width", data.width / 2 + 'px');
    }
});

$("#save").click(function () {
    $("#image").val($image.cropper("getDataURL"));
    $(this).parent().submit();
});

$("#zoomIn").click(function () {
    $image.cropper("zoom", 0.1);
});

$("#zoomOut").click(function () {
    $image.cropper("zoom", -0.1);
});

$("#rotateLeft").click(function () {
    $image.cropper("rotate", 45);
});

$("#rotateRight").click(function () {
    $image.cropper("rotate", -45);
});

$("#setDrag").click(function () {
    $image.cropper("setDragMode", "crop");
});