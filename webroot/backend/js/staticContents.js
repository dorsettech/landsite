var staticContents = {};


staticContents.init = function () {
    var defaultValue = $('#staticContentsSelect option:selected').val();
    if (defaultValue.trim() !== '') {
        $('.staticContentValue-' + defaultValue).show();
    }
    $(document).on('change', '#staticContentsSelect', function () {
        staticContents.change($(this).val());
    });
};

staticContents.change = function (value) {
    $('.staticContent').hide();
    $('.staticContent').find('[name="customValue"]').prop('disabled', true);
    $('.staticContentValue-' + value).show();
    $('.staticContentValue-' + value).find('[name="customValue"]').prop('disabled', false);

};

$(document).ready(staticContents.init);