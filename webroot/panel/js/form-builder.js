var formBuilder = {
    row: null,
    option: null,
    currentOptionValue: {},
    currentOptionSelector: null,
    validators: {
        max_length: 'ex. 10',
        min_length: 'ex. 2',
        max_min_length: 'ex 2/10',
        decimal: '[0-9]+([\.,][0-9]+)?',
        phone: '[\+]\d{2}[\(]\d{2}[\)]\d{4}[\-]\d{4}'
    },
    mainEmails: [],
    ccEmails: [],
    bccEmails: []
};


formBuilder.init = function () {
    formBuilder.checkForm();
    formBuilder.events();
    formBuilder.listenRedirect($('#thank-you'));
    formBuilder.loadExistingEmails();
};
formBuilder.events = function () {
//add new field
    $(document).on('click', '.moveUp', formBuilder.moveFieldUp);
    $(document).on('click', '.moveDown', formBuilder.moveFieldDown);
    $(document).on('click', '#addField', formBuilder.addField);
    //remove current field
    $(document).on('click', '.remove', function () {
        $(this).parents('tr').remove();
        formBuilder.checkForm();
    });
    //show modal to create options for select
    $(document).on('change', '.optionsSelect', function () {
        if ($(this).val() === 'select' || $(this).val() === 'radio') {
            $(this).parent().children('.optionsButton').show();
        } else {
            $(this).parent().children('.optionsButton').hide();
        }
        $(this).parent().children('input[type="hidden"]').val('');
    });
    $(document).on('click', '.optionsButton', function () {
        $('.optionsBody').html('');
        formBuilder.currentOptionSelector = $(this).parent().children('input[type="hidden"]');
        var value = formBuilder.currentOptionSelector.val();
        if (value.trim() !== '') {
            formBuilder.currentOptionValue = value;
        }
        if (typeof formBuilder.currentOptionValue === 'string'
                && typeof JSON.parse(formBuilder.currentOptionValue) === 'object'
                && Object.keys(JSON.parse(formBuilder.currentOptionValue)).length > 0) {
            formBuilder.currentOptionValue = JSON.parse(formBuilder.currentOptionValue);
            formBuilder.buildExistingOption();
        }
    });
    //add new option to modal
    $(document).on('click', '#addOption', formBuilder.addOption);
    //save inserted options 
    $(document).on('click', '#saveOptions', formBuilder.saveOptions);
    //inserting rule into input depends on selected value
    $(document).on('change', '.validationSelect', function () {
        formBuilder.changeRule($(this));
    });
    //change name of field required to catch this field in controller by name
    $(document).on('change', '.inputName', function () {
        console.log($(this).val());
        var requiredField = $(this).parents('tr').find('.requiredField');
        requiredField.attr('name', 'required[' + $(this).val() + ']');
        requiredField.parent().children('input[type="hidden"]').attr('name', 'required[' + $(this).val() + ']');
    });
    $(document).on('click', '.addMainEmail,.addCCEmail,.addBCCEmail', function () {
        var input = $(this).parent().parent().find('input');
        formBuilder.addEmail(input.val(), $(this).data('email'), input);
    });

    $(document).on('click', '.emailElement', function () {
        formBuilder.removeEmail($(this).data('email'), $(this).data('selector'));
    });
    $(document).on('click', '#thank-you', function () {
        formBuilder.listenRedirect($(this));
    });
};

formBuilder.listenRedirect = function (element) {
    if (element.is(':checked')) {
        $('.redirect').parents('.form-group').show();
    } else {
        $('.redirect').parents('.form-group').hide();
    }
};

formBuilder.addEmail = function (value, selector, input) {
    if (formBuilder.validateEmail(value)) {
        formBuilder[selector].push(value);
        formBuilder.updateEmails(selector);
        input.val('').css({
            'border': '1px solid #e5e6e7'
        });
    } else {
        console.log('display error');
        input.css({
            'border': '1px solid red'
        });
    }
};
formBuilder.validateEmail = function (email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};
formBuilder.removeEmail = function (email, selector) {
    var index = formBuilder[selector].indexOf(email);
    if (index != -1) {
        formBuilder[selector].splice(index, 1);
    }
    formBuilder.updateEmails(selector);
};
formBuilder.updateEmails = function (selector) {
    var list = formBuilder[selector];
    var html = '';
    for (var i = 0; i < list.length; i++) {
        html += '<button type="button" class="btn btn-default btn-md emailElement" data-selector="' + selector + '" data-email="' + list[i] + '">' + list[i] + ' <i class="removeEmail fa fa-times text-danger"></i></button>';
    }
    $('.' + selector).html(html);
    $('.' + selector + 'Hidden').val(list);
};

formBuilder.loadExistingEmails = function () {
    var emails = ['mainEmails', 'ccEmails', 'bccEmails'];
    $.each(emails, function (index, value) {
        var arrayWithEmails = $('.' + value + 'Hidden').val();
        if (typeof arrayWithEmails !== 'undefined') {
            var email = arrayWithEmails.split(',');
            $.each(email, function (index, singleEmail) {
                if (singleEmail.trim() !== '') {
                    formBuilder[value].push(singleEmail);
                }
            });
        }
    });
};
formBuilder.addField = function () {
    if (typeof formBuilder.row[0] !== 'undefined') {
        $('.formBuilderBody').append(formBuilder.row[0].outerHTML);
    }
    formBuilder.checkForm();
};
formBuilder.addOption = function () {
    if (typeof formBuilder.option[0] !== 'undefined') {
        $('.optionsBody').append(formBuilder.option[0].outerHTML);
    }
};
formBuilder.checkForm = function () {
    if ($('.formBuilderTable tbody tr').length === 0) {
        $('.formBuilderTable').hide();
    } else {
        $('.formBuilderTable').show();
    }
};
formBuilder.saveOptions = function () {
    var key, value = '';
    formBuilder.currentOptionValue = {};
    $('.optionsBody tr').each(function () {
        $(this).find('input').each(function () {
            var id = $(this).attr('id');
            if (id === 'optionvalue') {
                key = $(this).val();
            } else if (id === 'optionname') {
                value = $(this).val();
            } else {
                key = value = '';
            }
        });
        formBuilder.currentOptionValue[key] = value;
    });
    formBuilder.currentOptionSelector.val(JSON.stringify(formBuilder.currentOptionValue));
    $('.optionsBody').html('');
    $('#optionModalDialog').modal('hide');
};
formBuilder.buildExistingOption = function () {
    for (var key in formBuilder.currentOptionValue) {
        var option = formBuilder.option.clone();
        option.find('input').each(function () {
            if ($(this).attr('id') === 'optionvalue') {
                $(this).val(key);
            } else if ($(this).attr('id') === 'optionname') {
                $(this).val(formBuilder.currentOptionValue[key]);
            }
        });
        $('.optionsBody').append(option.addClass('asd'));
    }
};
formBuilder.changeRule = function (validationSelect) {
    var value = validationSelect.val();
    var ruleInput = validationSelect.parents('tr').find('.rule');
    if (typeof ruleInput[0] !== 'undefined') {
        if (typeof formBuilder.validators[value] !== 'undefined') {
            ruleInput.val(formBuilder.validators[value]);
        } else {
            ruleInput.val('');
        }
    }
};
formBuilder.moveFieldUp = function () {
    var element = $(this).parents('tr');
    var prev = element.prev();
    element.insertBefore(prev);
};
formBuilder.moveFieldDown = function () {
    var element = $(this).parents('tr');
    var next = element.next();
    element.insertAfter(next);
};
$(document).ready(formBuilder.init);
$(window).on('load', function () {
    formBuilder.row = $('#row tbody > *').clone();
    formBuilder.option = $('#option tbody > *').clone();
});