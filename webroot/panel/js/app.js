/*!
  * Bootstrap v4.1.3 (https://getbootstrap.com/)
  * Copyright 2011-2018 The Bootstrap Authors (https://github.com/twbs/bootstrap/graphs/contributors)
  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
  */
(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('jquery')) :
  typeof define === 'function' && define.amd ? define(['exports', 'jquery'], factory) :
  (factory((global.bootstrap = {}),global.jQuery));
}(this, (function (exports,$) { 'use strict';

  $ = $ && $.hasOwnProperty('default') ? $['default'] : $;

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};
      var ownKeys = Object.keys(source);

      if (typeof Object.getOwnPropertySymbols === 'function') {
        ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) {
          return Object.getOwnPropertyDescriptor(source, sym).enumerable;
        }));
      }

      ownKeys.forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    }

    return target;
  }

  function _inheritsLoose(subClass, superClass) {
    subClass.prototype = Object.create(superClass.prototype);
    subClass.prototype.constructor = subClass;
    subClass.__proto__ = superClass;
  }

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.1.3): util.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  var Util = function ($$$1) {
    /**
     * ------------------------------------------------------------------------
     * Private TransitionEnd Helpers
     * ------------------------------------------------------------------------
     */
    var TRANSITION_END = 'transitionend';
    var MAX_UID = 1000000;
    var MILLISECONDS_MULTIPLIER = 1000; // Shoutout AngusCroll (https://goo.gl/pxwQGp)

    function toType(obj) {
      return {}.toString.call(obj).match(/\s([a-z]+)/i)[1].toLowerCase();
    }

    function getSpecialTransitionEndEvent() {
      return {
        bindType: TRANSITION_END,
        delegateType: TRANSITION_END,
        handle: function handle(event) {
          if ($$$1(event.target).is(this)) {
            return event.handleObj.handler.apply(this, arguments); // eslint-disable-line prefer-rest-params
          }

          return undefined; // eslint-disable-line no-undefined
        }
      };
    }

    function transitionEndEmulator(duration) {
      var _this = this;

      var called = false;
      $$$1(this).one(Util.TRANSITION_END, function () {
        called = true;
      });
      setTimeout(function () {
        if (!called) {
          Util.triggerTransitionEnd(_this);
        }
      }, duration);
      return this;
    }

    function setTransitionEndSupport() {
      $$$1.fn.emulateTransitionEnd = transitionEndEmulator;
      $$$1.event.special[Util.TRANSITION_END] = getSpecialTransitionEndEvent();
    }
    /**
     * --------------------------------------------------------------------------
     * Public Util Api
     * --------------------------------------------------------------------------
     */


    var Util = {
      TRANSITION_END: 'bsTransitionEnd',
      getUID: function getUID(prefix) {
        do {
          // eslint-disable-next-line no-bitwise
          prefix += ~~(Math.random() * MAX_UID); // "~~" acts like a faster Math.floor() here
        } while (document.getElementById(prefix));

        return prefix;
      },
      getSelectorFromElement: function getSelectorFromElement(element) {
        var selector = element.getAttribute('data-target');

        if (!selector || selector === '#') {
          selector = element.getAttribute('href') || '';
        }

        try {
          return document.querySelector(selector) ? selector : null;
        } catch (err) {
          return null;
        }
      },
      getTransitionDurationFromElement: function getTransitionDurationFromElement(element) {
        if (!element) {
          return 0;
        } // Get transition-duration of the element


        var transitionDuration = $$$1(element).css('transition-duration');
        var floatTransitionDuration = parseFloat(transitionDuration); // Return 0 if element or transition duration is not found

        if (!floatTransitionDuration) {
          return 0;
        } // If multiple durations are defined, take the first


        transitionDuration = transitionDuration.split(',')[0];
        return parseFloat(transitionDuration) * MILLISECONDS_MULTIPLIER;
      },
      reflow: function reflow(element) {
        return element.offsetHeight;
      },
      triggerTransitionEnd: function triggerTransitionEnd(element) {
        $$$1(element).trigger(TRANSITION_END);
      },
      // TODO: Remove in v5
      supportsTransitionEnd: function supportsTransitionEnd() {
        return Boolean(TRANSITION_END);
      },
      isElement: function isElement(obj) {
        return (obj[0] || obj).nodeType;
      },
      typeCheckConfig: function typeCheckConfig(componentName, config, configTypes) {
        for (var property in configTypes) {
          if (Object.prototype.hasOwnProperty.call(configTypes, property)) {
            var expectedTypes = configTypes[property];
            var value = config[property];
            var valueType = value && Util.isElement(value) ? 'element' : toType(value);

            if (!new RegExp(expectedTypes).test(valueType)) {
              throw new Error(componentName.toUpperCase() + ": " + ("Option \"" + property + "\" provided type \"" + valueType + "\" ") + ("but expected type \"" + expectedTypes + "\"."));
            }
          }
        }
      }
    };
    setTransitionEndSupport();
    return Util;
  }($);

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.1.3): alert.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  var Alert = function ($$$1) {
    /**
     * ------------------------------------------------------------------------
     * Constants
     * ------------------------------------------------------------------------
     */
    var NAME = 'alert';
    var VERSION = '4.1.3';
    var DATA_KEY = 'bs.alert';
    var EVENT_KEY = "." + DATA_KEY;
    var DATA_API_KEY = '.data-api';
    var JQUERY_NO_CONFLICT = $$$1.fn[NAME];
    var Selector = {
      DISMISS: '[data-dismiss="alert"]'
    };
    var Event = {
      CLOSE: "close" + EVENT_KEY,
      CLOSED: "closed" + EVENT_KEY,
      CLICK_DATA_API: "click" + EVENT_KEY + DATA_API_KEY
    };
    var ClassName = {
      ALERT: 'alert',
      FADE: 'fade',
      SHOW: 'show'
      /**
       * ------------------------------------------------------------------------
       * Class Definition
       * ------------------------------------------------------------------------
       */

    };

    var Alert =
    /*#__PURE__*/
    function () {
      function Alert(element) {
        this._element = element;
      } // Getters


      var _proto = Alert.prototype;

      // Public
      _proto.close = function close(element) {
        var rootElement = this._element;

        if (element) {
          rootElement = this._getRootElement(element);
        }

        var customEvent = this._triggerCloseEvent(rootElement);

        if (customEvent.isDefaultPrevented()) {
          return;
        }

        this._removeElement(rootElement);
      };

      _proto.dispose = function dispose() {
        $$$1.removeData(this._element, DATA_KEY);
        this._element = null;
      }; // Private


      _proto._getRootElement = function _getRootElement(element) {
        var selector = Util.getSelectorFromElement(element);
        var parent = false;

        if (selector) {
          parent = document.querySelector(selector);
        }

        if (!parent) {
          parent = $$$1(element).closest("." + ClassName.ALERT)[0];
        }

        return parent;
      };

      _proto._triggerCloseEvent = function _triggerCloseEvent(element) {
        var closeEvent = $$$1.Event(Event.CLOSE);
        $$$1(element).trigger(closeEvent);
        return closeEvent;
      };

      _proto._removeElement = function _removeElement(element) {
        var _this = this;

        $$$1(element).removeClass(ClassName.SHOW);

        if (!$$$1(element).hasClass(ClassName.FADE)) {
          this._destroyElement(element);

          return;
        }

        var transitionDuration = Util.getTransitionDurationFromElement(element);
        $$$1(element).one(Util.TRANSITION_END, function (event) {
          return _this._destroyElement(element, event);
        }).emulateTransitionEnd(transitionDuration);
      };

      _proto._destroyElement = function _destroyElement(element) {
        $$$1(element).detach().trigger(Event.CLOSED).remove();
      }; // Static


      Alert._jQueryInterface = function _jQueryInterface(config) {
        return this.each(function () {
          var $element = $$$1(this);
          var data = $element.data(DATA_KEY);

          if (!data) {
            data = new Alert(this);
            $element.data(DATA_KEY, data);
          }

          if (config === 'close') {
            data[config](this);
          }
        });
      };

      Alert._handleDismiss = function _handleDismiss(alertInstance) {
        return function (event) {
          if (event) {
            event.preventDefault();
          }

          alertInstance.close(this);
        };
      };

      _createClass(Alert, null, [{
        key: "VERSION",
        get: function get() {
          return VERSION;
        }
      }]);

      return Alert;
    }();
    /**
     * ------------------------------------------------------------------------
     * Data Api implementation
     * ------------------------------------------------------------------------
     */


    $$$1(document).on(Event.CLICK_DATA_API, Selector.DISMISS, Alert._handleDismiss(new Alert()));
    /**
     * ------------------------------------------------------------------------
     * jQuery
     * ------------------------------------------------------------------------
     */

    $$$1.fn[NAME] = Alert._jQueryInterface;
    $$$1.fn[NAME].Constructor = Alert;

    $$$1.fn[NAME].noConflict = function () {
      $$$1.fn[NAME] = JQUERY_NO_CONFLICT;
      return Alert._jQueryInterface;
    };

    return Alert;
  }($);

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.1.3): button.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  var Button = function ($$$1) {
    /**
     * ------------------------------------------------------------------------
     * Constants
     * ------------------------------------------------------------------------
     */
    var NAME = 'button';
    var VERSION = '4.1.3';
    var DATA_KEY = 'bs.button';
    var EVENT_KEY = "." + DATA_KEY;
    var DATA_API_KEY = '.data-api';
    var JQUERY_NO_CONFLICT = $$$1.fn[NAME];
    var ClassName = {
      ACTIVE: 'active',
      BUTTON: 'btn',
      FOCUS: 'focus'
    };
    var Selector = {
      DATA_TOGGLE_CARROT: '[data-toggle^="button"]',
      DATA_TOGGLE: '[data-toggle="buttons"]',
      INPUT: 'input',
      ACTIVE: '.active',
      BUTTON: '.btn'
    };
    var Event = {
      CLICK_DATA_API: "click" + EVENT_KEY + DATA_API_KEY,
      FOCUS_BLUR_DATA_API: "focus" + EVENT_KEY + DATA_API_KEY + " " + ("blur" + EVENT_KEY + DATA_API_KEY)
      /**
       * ------------------------------------------------------------------------
       * Class Definition
       * ------------------------------------------------------------------------
       */

    };

    var Button =
    /*#__PURE__*/
    function () {
      function Button(element) {
        this._element = element;
      } // Getters


      var _proto = Button.prototype;

      // Public
      _proto.toggle = function toggle() {
        var triggerChangeEvent = true;
        var addAriaPressed = true;
        var rootElement = $$$1(this._element).closest(Selector.DATA_TOGGLE)[0];

        if (rootElement) {
          var input = this._element.querySelector(Selector.INPUT);

          if (input) {
            if (input.type === 'radio') {
              if (input.checked && this._element.classList.contains(ClassName.ACTIVE)) {
                triggerChangeEvent = false;
              } else {
                var activeElement = rootElement.querySelector(Selector.ACTIVE);

                if (activeElement) {
                  $$$1(activeElement).removeClass(ClassName.ACTIVE);
                }
              }
            }

            if (triggerChangeEvent) {
              if (input.hasAttribute('disabled') || rootElement.hasAttribute('disabled') || input.classList.contains('disabled') || rootElement.classList.contains('disabled')) {
                return;
              }

              input.checked = !this._element.classList.contains(ClassName.ACTIVE);
              $$$1(input).trigger('change');
            }

            input.focus();
            addAriaPressed = false;
          }
        }

        if (addAriaPressed) {
          this._element.setAttribute('aria-pressed', !this._element.classList.contains(ClassName.ACTIVE));
        }

        if (triggerChangeEvent) {
          $$$1(this._element).toggleClass(ClassName.ACTIVE);
        }
      };

      _proto.dispose = function dispose() {
        $$$1.removeData(this._element, DATA_KEY);
        this._element = null;
      }; // Static


      Button._jQueryInterface = function _jQueryInterface(config) {
        return this.each(function () {
          var data = $$$1(this).data(DATA_KEY);

          if (!data) {
            data = new Button(this);
            $$$1(this).data(DATA_KEY, data);
          }

          if (config === 'toggle') {
            data[config]();
          }
        });
      };

      _createClass(Button, null, [{
        key: "VERSION",
        get: function get() {
          return VERSION;
        }
      }]);

      return Button;
    }();
    /**
     * ------------------------------------------------------------------------
     * Data Api implementation
     * ------------------------------------------------------------------------
     */


    $$$1(document).on(Event.CLICK_DATA_API, Selector.DATA_TOGGLE_CARROT, function (event) {
      event.preventDefault();
      var button = event.target;

      if (!$$$1(button).hasClass(ClassName.BUTTON)) {
        button = $$$1(button).closest(Selector.BUTTON);
      }

      Button._jQueryInterface.call($$$1(button), 'toggle');
    }).on(Event.FOCUS_BLUR_DATA_API, Selector.DATA_TOGGLE_CARROT, function (event) {
      var button = $$$1(event.target).closest(Selector.BUTTON)[0];
      $$$1(button).toggleClass(ClassName.FOCUS, /^focus(in)?$/.test(event.type));
    });
    /**
     * ------------------------------------------------------------------------
     * jQuery
     * ------------------------------------------------------------------------
     */

    $$$1.fn[NAME] = Button._jQueryInterface;
    $$$1.fn[NAME].Constructor = Button;

    $$$1.fn[NAME].noConflict = function () {
      $$$1.fn[NAME] = JQUERY_NO_CONFLICT;
      return Button._jQueryInterface;
    };

    return Button;
  }($);

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.1.3): carousel.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  var Carousel = function ($$$1) {
    /**
     * ------------------------------------------------------------------------
     * Constants
     * ------------------------------------------------------------------------
     */
    var NAME = 'carousel';
    var VERSION = '4.1.3';
    var DATA_KEY = 'bs.carousel';
    var EVENT_KEY = "." + DATA_KEY;
    var DATA_API_KEY = '.data-api';
    var JQUERY_NO_CONFLICT = $$$1.fn[NAME];
    var ARROW_LEFT_KEYCODE = 37; // KeyboardEvent.which value for left arrow key

    var ARROW_RIGHT_KEYCODE = 39; // KeyboardEvent.which value for right arrow key

    var TOUCHEVENT_COMPAT_WAIT = 500; // Time for mouse compat events to fire after touch

    var Default = {
      interval: 5000,
      keyboard: true,
      slide: false,
      pause: 'hover',
      wrap: true
    };
    var DefaultType = {
      interval: '(number|boolean)',
      keyboard: 'boolean',
      slide: '(boolean|string)',
      pause: '(string|boolean)',
      wrap: 'boolean'
    };
    var Direction = {
      NEXT: 'next',
      PREV: 'prev',
      LEFT: 'left',
      RIGHT: 'right'
    };
    var Event = {
      SLIDE: "slide" + EVENT_KEY,
      SLID: "slid" + EVENT_KEY,
      KEYDOWN: "keydown" + EVENT_KEY,
      MOUSEENTER: "mouseenter" + EVENT_KEY,
      MOUSELEAVE: "mouseleave" + EVENT_KEY,
      TOUCHEND: "touchend" + EVENT_KEY,
      LOAD_DATA_API: "load" + EVENT_KEY + DATA_API_KEY,
      CLICK_DATA_API: "click" + EVENT_KEY + DATA_API_KEY
    };
    var ClassName = {
      CAROUSEL: 'carousel',
      ACTIVE: 'active',
      SLIDE: 'slide',
      RIGHT: 'carousel-item-right',
      LEFT: 'carousel-item-left',
      NEXT: 'carousel-item-next',
      PREV: 'carousel-item-prev',
      ITEM: 'carousel-item'
    };
    var Selector = {
      ACTIVE: '.active',
      ACTIVE_ITEM: '.active.carousel-item',
      ITEM: '.carousel-item',
      NEXT_PREV: '.carousel-item-next, .carousel-item-prev',
      INDICATORS: '.carousel-indicators',
      DATA_SLIDE: '[data-slide], [data-slide-to]',
      DATA_RIDE: '[data-ride="carousel"]'
      /**
       * ------------------------------------------------------------------------
       * Class Definition
       * ------------------------------------------------------------------------
       */

    };

    var Carousel =
    /*#__PURE__*/
    function () {
      function Carousel(element, config) {
        this._items = null;
        this._interval = null;
        this._activeElement = null;
        this._isPaused = false;
        this._isSliding = false;
        this.touchTimeout = null;
        this._config = this._getConfig(config);
        this._element = $$$1(element)[0];
        this._indicatorsElement = this._element.querySelector(Selector.INDICATORS);

        this._addEventListeners();
      } // Getters


      var _proto = Carousel.prototype;

      // Public
      _proto.next = function next() {
        if (!this._isSliding) {
          this._slide(Direction.NEXT);
        }
      };

      _proto.nextWhenVisible = function nextWhenVisible() {
        // Don't call next when the page isn't visible
        // or the carousel or its parent isn't visible
        if (!document.hidden && $$$1(this._element).is(':visible') && $$$1(this._element).css('visibility') !== 'hidden') {
          this.next();
        }
      };

      _proto.prev = function prev() {
        if (!this._isSliding) {
          this._slide(Direction.PREV);
        }
      };

      _proto.pause = function pause(event) {
        if (!event) {
          this._isPaused = true;
        }

        if (this._element.querySelector(Selector.NEXT_PREV)) {
          Util.triggerTransitionEnd(this._element);
          this.cycle(true);
        }

        clearInterval(this._interval);
        this._interval = null;
      };

      _proto.cycle = function cycle(event) {
        if (!event) {
          this._isPaused = false;
        }

        if (this._interval) {
          clearInterval(this._interval);
          this._interval = null;
        }

        if (this._config.interval && !this._isPaused) {
          this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval);
        }
      };

      _proto.to = function to(index) {
        var _this = this;

        this._activeElement = this._element.querySelector(Selector.ACTIVE_ITEM);

        var activeIndex = this._getItemIndex(this._activeElement);

        if (index > this._items.length - 1 || index < 0) {
          return;
        }

        if (this._isSliding) {
          $$$1(this._element).one(Event.SLID, function () {
            return _this.to(index);
          });
          return;
        }

        if (activeIndex === index) {
          this.pause();
          this.cycle();
          return;
        }

        var direction = index > activeIndex ? Direction.NEXT : Direction.PREV;

        this._slide(direction, this._items[index]);
      };

      _proto.dispose = function dispose() {
        $$$1(this._element).off(EVENT_KEY);
        $$$1.removeData(this._element, DATA_KEY);
        this._items = null;
        this._config = null;
        this._element = null;
        this._interval = null;
        this._isPaused = null;
        this._isSliding = null;
        this._activeElement = null;
        this._indicatorsElement = null;
      }; // Private


      _proto._getConfig = function _getConfig(config) {
        config = _objectSpread({}, Default, config);
        Util.typeCheckConfig(NAME, config, DefaultType);
        return config;
      };

      _proto._addEventListeners = function _addEventListeners() {
        var _this2 = this;

        if (this._config.keyboard) {
          $$$1(this._element).on(Event.KEYDOWN, function (event) {
            return _this2._keydown(event);
          });
        }

        if (this._config.pause === 'hover') {
          $$$1(this._element).on(Event.MOUSEENTER, function (event) {
            return _this2.pause(event);
          }).on(Event.MOUSELEAVE, function (event) {
            return _this2.cycle(event);
          });

          if ('ontouchstart' in document.documentElement) {
            // If it's a touch-enabled device, mouseenter/leave are fired as
            // part of the mouse compatibility events on first tap - the carousel
            // would stop cycling until user tapped out of it;
            // here, we listen for touchend, explicitly pause the carousel
            // (as if it's the second time we tap on it, mouseenter compat event
            // is NOT fired) and after a timeout (to allow for mouse compatibility
            // events to fire) we explicitly restart cycling
            $$$1(this._element).on(Event.TOUCHEND, function () {
              _this2.pause();

              if (_this2.touchTimeout) {
                clearTimeout(_this2.touchTimeout);
              }

              _this2.touchTimeout = setTimeout(function (event) {
                return _this2.cycle(event);
              }, TOUCHEVENT_COMPAT_WAIT + _this2._config.interval);
            });
          }
        }
      };

      _proto._keydown = function _keydown(event) {
        if (/input|textarea/i.test(event.target.tagName)) {
          return;
        }

        switch (event.which) {
          case ARROW_LEFT_KEYCODE:
            event.preventDefault();
            this.prev();
            break;

          case ARROW_RIGHT_KEYCODE:
            event.preventDefault();
            this.next();
            break;

          default:
        }
      };

      _proto._getItemIndex = function _getItemIndex(element) {
        this._items = element && element.parentNode ? [].slice.call(element.parentNode.querySelectorAll(Selector.ITEM)) : [];
        return this._items.indexOf(element);
      };

      _proto._getItemByDirection = function _getItemByDirection(direction, activeElement) {
        var isNextDirection = direction === Direction.NEXT;
        var isPrevDirection = direction === Direction.PREV;

        var activeIndex = this._getItemIndex(activeElement);

        var lastItemIndex = this._items.length - 1;
        var isGoingToWrap = isPrevDirection && activeIndex === 0 || isNextDirection && activeIndex === lastItemIndex;

        if (isGoingToWrap && !this._config.wrap) {
          return activeElement;
        }

        var delta = direction === Direction.PREV ? -1 : 1;
        var itemIndex = (activeIndex + delta) % this._items.length;
        return itemIndex === -1 ? this._items[this._items.length - 1] : this._items[itemIndex];
      };

      _proto._triggerSlideEvent = function _triggerSlideEvent(relatedTarget, eventDirectionName) {
        var targetIndex = this._getItemIndex(relatedTarget);

        var fromIndex = this._getItemIndex(this._element.querySelector(Selector.ACTIVE_ITEM));

        var slideEvent = $$$1.Event(Event.SLIDE, {
          relatedTarget: relatedTarget,
          direction: eventDirectionName,
          from: fromIndex,
          to: targetIndex
        });
        $$$1(this._element).trigger(slideEvent);
        return slideEvent;
      };

      _proto._setActiveIndicatorElement = function _setActiveIndicatorElement(element) {
        if (this._indicatorsElement) {
          var indicators = [].slice.call(this._indicatorsElement.querySelectorAll(Selector.ACTIVE));
          $$$1(indicators).removeClass(ClassName.ACTIVE);

          var nextIndicator = this._indicatorsElement.children[this._getItemIndex(element)];

          if (nextIndicator) {
            $$$1(nextIndicator).addClass(ClassName.ACTIVE);
          }
        }
      };

      _proto._slide = function _slide(direction, element) {
        var _this3 = this;

        var activeElement = this._element.querySelector(Selector.ACTIVE_ITEM);

        var activeElementIndex = this._getItemIndex(activeElement);

        var nextElement = element || activeElement && this._getItemByDirection(direction, activeElement);

        var nextElementIndex = this._getItemIndex(nextElement);

        var isCycling = Boolean(this._interval);
        var directionalClassName;
        var orderClassName;
        var eventDirectionName;

        if (direction === Direction.NEXT) {
          directionalClassName = ClassName.LEFT;
          orderClassName = ClassName.NEXT;
          eventDirectionName = Direction.LEFT;
        } else {
          directionalClassName = ClassName.RIGHT;
          orderClassName = ClassName.PREV;
          eventDirectionName = Direction.RIGHT;
        }

        if (nextElement && $$$1(nextElement).hasClass(ClassName.ACTIVE)) {
          this._isSliding = false;
          return;
        }

        var slideEvent = this._triggerSlideEvent(nextElement, eventDirectionName);

        if (slideEvent.isDefaultPrevented()) {
          return;
        }

        if (!activeElement || !nextElement) {
          // Some weirdness is happening, so we bail
          return;
        }

        this._isSliding = true;

        if (isCycling) {
          this.pause();
        }

        this._setActiveIndicatorElement(nextElement);

        var slidEvent = $$$1.Event(Event.SLID, {
          relatedTarget: nextElement,
          direction: eventDirectionName,
          from: activeElementIndex,
          to: nextElementIndex
        });

        if ($$$1(this._element).hasClass(ClassName.SLIDE)) {
          $$$1(nextElement).addClass(orderClassName);
          Util.reflow(nextElement);
          $$$1(activeElement).addClass(directionalClassName);
          $$$1(nextElement).addClass(directionalClassName);
          var transitionDuration = Util.getTransitionDurationFromElement(activeElement);
          $$$1(activeElement).one(Util.TRANSITION_END, function () {
            $$$1(nextElement).removeClass(directionalClassName + " " + orderClassName).addClass(ClassName.ACTIVE);
            $$$1(activeElement).removeClass(ClassName.ACTIVE + " " + orderClassName + " " + directionalClassName);
            _this3._isSliding = false;
            setTimeout(function () {
              return $$$1(_this3._element).trigger(slidEvent);
            }, 0);
          }).emulateTransitionEnd(transitionDuration);
        } else {
          $$$1(activeElement).removeClass(ClassName.ACTIVE);
          $$$1(nextElement).addClass(ClassName.ACTIVE);
          this._isSliding = false;
          $$$1(this._element).trigger(slidEvent);
        }

        if (isCycling) {
          this.cycle();
        }
      }; // Static


      Carousel._jQueryInterface = function _jQueryInterface(config) {
        return this.each(function () {
          var data = $$$1(this).data(DATA_KEY);

          var _config = _objectSpread({}, Default, $$$1(this).data());

          if (typeof config === 'object') {
            _config = _objectSpread({}, _config, config);
          }

          var action = typeof config === 'string' ? config : _config.slide;

          if (!data) {
            data = new Carousel(this, _config);
            $$$1(this).data(DATA_KEY, data);
          }

          if (typeof config === 'number') {
            data.to(config);
          } else if (typeof action === 'string') {
            if (typeof data[action] === 'undefined') {
              throw new TypeError("No method named \"" + action + "\"");
            }

            data[action]();
          } else if (_config.interval) {
            data.pause();
            data.cycle();
          }
        });
      };

      Carousel._dataApiClickHandler = function _dataApiClickHandler(event) {
        var selector = Util.getSelectorFromElement(this);

        if (!selector) {
          return;
        }

        var target = $$$1(selector)[0];

        if (!target || !$$$1(target).hasClass(ClassName.CAROUSEL)) {
          return;
        }

        var config = _objectSpread({}, $$$1(target).data(), $$$1(this).data());

        var slideIndex = this.getAttribute('data-slide-to');

        if (slideIndex) {
          config.interval = false;
        }

        Carousel._jQueryInterface.call($$$1(target), config);

        if (slideIndex) {
          $$$1(target).data(DATA_KEY).to(slideIndex);
        }

        event.preventDefault();
      };

      _createClass(Carousel, null, [{
        key: "VERSION",
        get: function get() {
          return VERSION;
        }
      }, {
        key: "Default",
        get: function get() {
          return Default;
        }
      }]);

      return Carousel;
    }();
    /**
     * ------------------------------------------------------------------------
     * Data Api implementation
     * ------------------------------------------------------------------------
     */


    $$$1(document).on(Event.CLICK_DATA_API, Selector.DATA_SLIDE, Carousel._dataApiClickHandler);
    $$$1(window).on(Event.LOAD_DATA_API, function () {
      var carousels = [].slice.call(document.querySelectorAll(Selector.DATA_RIDE));

      for (var i = 0, len = carousels.length; i < len; i++) {
        var $carousel = $$$1(carousels[i]);

        Carousel._jQueryInterface.call($carousel, $carousel.data());
      }
    });
    /**
     * ------------------------------------------------------------------------
     * jQuery
     * ------------------------------------------------------------------------
     */

    $$$1.fn[NAME] = Carousel._jQueryInterface;
    $$$1.fn[NAME].Constructor = Carousel;

    $$$1.fn[NAME].noConflict = function () {
      $$$1.fn[NAME] = JQUERY_NO_CONFLICT;
      return Carousel._jQueryInterface;
    };

    return Carousel;
  }($);

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.1.3): collapse.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  var Collapse = function ($$$1) {
    /**
     * ------------------------------------------------------------------------
     * Constants
     * ------------------------------------------------------------------------
     */
    var NAME = 'collapse';
    var VERSION = '4.1.3';
    var DATA_KEY = 'bs.collapse';
    var EVENT_KEY = "." + DATA_KEY;
    var DATA_API_KEY = '.data-api';
    var JQUERY_NO_CONFLICT = $$$1.fn[NAME];
    var Default = {
      toggle: true,
      parent: ''
    };
    var DefaultType = {
      toggle: 'boolean',
      parent: '(string|element)'
    };
    var Event = {
      SHOW: "show" + EVENT_KEY,
      SHOWN: "shown" + EVENT_KEY,
      HIDE: "hide" + EVENT_KEY,
      HIDDEN: "hidden" + EVENT_KEY,
      CLICK_DATA_API: "click" + EVENT_KEY + DATA_API_KEY
    };
    var ClassName = {
      SHOW: 'show',
      COLLAPSE: 'collapse',
      COLLAPSING: 'collapsing',
      COLLAPSED: 'collapsed'
    };
    var Dimension = {
      WIDTH: 'width',
      HEIGHT: 'height'
    };
    var Selector = {
      ACTIVES: '.show, .collapsing',
      DATA_TOGGLE: '[data-toggle="collapse"]'
      /**
       * ------------------------------------------------------------------------
       * Class Definition
       * ------------------------------------------------------------------------
       */

    };

    var Collapse =
    /*#__PURE__*/
    function () {
      function Collapse(element, config) {
        this._isTransitioning = false;
        this._element = element;
        this._config = this._getConfig(config);
        this._triggerArray = $$$1.makeArray(document.querySelectorAll("[data-toggle=\"collapse\"][href=\"#" + element.id + "\"]," + ("[data-toggle=\"collapse\"][data-target=\"#" + element.id + "\"]")));
        var toggleList = [].slice.call(document.querySelectorAll(Selector.DATA_TOGGLE));

        for (var i = 0, len = toggleList.length; i < len; i++) {
          var elem = toggleList[i];
          var selector = Util.getSelectorFromElement(elem);
          var filterElement = [].slice.call(document.querySelectorAll(selector)).filter(function (foundElem) {
            return foundElem === element;
          });

          if (selector !== null && filterElement.length > 0) {
            this._selector = selector;

            this._triggerArray.push(elem);
          }
        }

        this._parent = this._config.parent ? this._getParent() : null;

        if (!this._config.parent) {
          this._addAriaAndCollapsedClass(this._element, this._triggerArray);
        }

        if (this._config.toggle) {
          this.toggle();
        }
      } // Getters


      var _proto = Collapse.prototype;

      // Public
      _proto.toggle = function toggle() {
        if ($$$1(this._element).hasClass(ClassName.SHOW)) {
          this.hide();
        } else {
          this.show();
        }
      };

      _proto.show = function show() {
        var _this = this;

        if (this._isTransitioning || $$$1(this._element).hasClass(ClassName.SHOW)) {
          return;
        }

        var actives;
        var activesData;

        if (this._parent) {
          actives = [].slice.call(this._parent.querySelectorAll(Selector.ACTIVES)).filter(function (elem) {
            return elem.getAttribute('data-parent') === _this._config.parent;
          });

          if (actives.length === 0) {
            actives = null;
          }
        }

        if (actives) {
          activesData = $$$1(actives).not(this._selector).data(DATA_KEY);

          if (activesData && activesData._isTransitioning) {
            return;
          }
        }

        var startEvent = $$$1.Event(Event.SHOW);
        $$$1(this._element).trigger(startEvent);

        if (startEvent.isDefaultPrevented()) {
          return;
        }

        if (actives) {
          Collapse._jQueryInterface.call($$$1(actives).not(this._selector), 'hide');

          if (!activesData) {
            $$$1(actives).data(DATA_KEY, null);
          }
        }

        var dimension = this._getDimension();

        $$$1(this._element).removeClass(ClassName.COLLAPSE).addClass(ClassName.COLLAPSING);
        this._element.style[dimension] = 0;

        if (this._triggerArray.length) {
          $$$1(this._triggerArray).removeClass(ClassName.COLLAPSED).attr('aria-expanded', true);
        }

        this.setTransitioning(true);

        var complete = function complete() {
          $$$1(_this._element).removeClass(ClassName.COLLAPSING).addClass(ClassName.COLLAPSE).addClass(ClassName.SHOW);
          _this._element.style[dimension] = '';

          _this.setTransitioning(false);

          $$$1(_this._element).trigger(Event.SHOWN);
        };

        var capitalizedDimension = dimension[0].toUpperCase() + dimension.slice(1);
        var scrollSize = "scroll" + capitalizedDimension;
        var transitionDuration = Util.getTransitionDurationFromElement(this._element);
        $$$1(this._element).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
        this._element.style[dimension] = this._element[scrollSize] + "px";
      };

      _proto.hide = function hide() {
        var _this2 = this;

        if (this._isTransitioning || !$$$1(this._element).hasClass(ClassName.SHOW)) {
          return;
        }

        var startEvent = $$$1.Event(Event.HIDE);
        $$$1(this._element).trigger(startEvent);

        if (startEvent.isDefaultPrevented()) {
          return;
        }

        var dimension = this._getDimension();

        this._element.style[dimension] = this._element.getBoundingClientRect()[dimension] + "px";
        Util.reflow(this._element);
        $$$1(this._element).addClass(ClassName.COLLAPSING).removeClass(ClassName.COLLAPSE).removeClass(ClassName.SHOW);
        var triggerArrayLength = this._triggerArray.length;

        if (triggerArrayLength > 0) {
          for (var i = 0; i < triggerArrayLength; i++) {
            var trigger = this._triggerArray[i];
            var selector = Util.getSelectorFromElement(trigger);

            if (selector !== null) {
              var $elem = $$$1([].slice.call(document.querySelectorAll(selector)));

              if (!$elem.hasClass(ClassName.SHOW)) {
                $$$1(trigger).addClass(ClassName.COLLAPSED).attr('aria-expanded', false);
              }
            }
          }
        }

        this.setTransitioning(true);

        var complete = function complete() {
          _this2.setTransitioning(false);

          $$$1(_this2._element).removeClass(ClassName.COLLAPSING).addClass(ClassName.COLLAPSE).trigger(Event.HIDDEN);
        };

        this._element.style[dimension] = '';
        var transitionDuration = Util.getTransitionDurationFromElement(this._element);
        $$$1(this._element).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
      };

      _proto.setTransitioning = function setTransitioning(isTransitioning) {
        this._isTransitioning = isTransitioning;
      };

      _proto.dispose = function dispose() {
        $$$1.removeData(this._element, DATA_KEY);
        this._config = null;
        this._parent = null;
        this._element = null;
        this._triggerArray = null;
        this._isTransitioning = null;
      }; // Private


      _proto._getConfig = function _getConfig(config) {
        config = _objectSpread({}, Default, config);
        config.toggle = Boolean(config.toggle); // Coerce string values

        Util.typeCheckConfig(NAME, config, DefaultType);
        return config;
      };

      _proto._getDimension = function _getDimension() {
        var hasWidth = $$$1(this._element).hasClass(Dimension.WIDTH);
        return hasWidth ? Dimension.WIDTH : Dimension.HEIGHT;
      };

      _proto._getParent = function _getParent() {
        var _this3 = this;

        var parent = null;

        if (Util.isElement(this._config.parent)) {
          parent = this._config.parent; // It's a jQuery object

          if (typeof this._config.parent.jquery !== 'undefined') {
            parent = this._config.parent[0];
          }
        } else {
          parent = document.querySelector(this._config.parent);
        }

        var selector = "[data-toggle=\"collapse\"][data-parent=\"" + this._config.parent + "\"]";
        var children = [].slice.call(parent.querySelectorAll(selector));
        $$$1(children).each(function (i, element) {
          _this3._addAriaAndCollapsedClass(Collapse._getTargetFromElement(element), [element]);
        });
        return parent;
      };

      _proto._addAriaAndCollapsedClass = function _addAriaAndCollapsedClass(element, triggerArray) {
        if (element) {
          var isOpen = $$$1(element).hasClass(ClassName.SHOW);

          if (triggerArray.length) {
            $$$1(triggerArray).toggleClass(ClassName.COLLAPSED, !isOpen).attr('aria-expanded', isOpen);
          }
        }
      }; // Static


      Collapse._getTargetFromElement = function _getTargetFromElement(element) {
        var selector = Util.getSelectorFromElement(element);
        return selector ? document.querySelector(selector) : null;
      };

      Collapse._jQueryInterface = function _jQueryInterface(config) {
        return this.each(function () {
          var $this = $$$1(this);
          var data = $this.data(DATA_KEY);

          var _config = _objectSpread({}, Default, $this.data(), typeof config === 'object' && config ? config : {});

          if (!data && _config.toggle && /show|hide/.test(config)) {
            _config.toggle = false;
          }

          if (!data) {
            data = new Collapse(this, _config);
            $this.data(DATA_KEY, data);
          }

          if (typeof config === 'string') {
            if (typeof data[config] === 'undefined') {
              throw new TypeError("No method named \"" + config + "\"");
            }

            data[config]();
          }
        });
      };

      _createClass(Collapse, null, [{
        key: "VERSION",
        get: function get() {
          return VERSION;
        }
      }, {
        key: "Default",
        get: function get() {
          return Default;
        }
      }]);

      return Collapse;
    }();
    /**
     * ------------------------------------------------------------------------
     * Data Api implementation
     * ------------------------------------------------------------------------
     */


    $$$1(document).on(Event.CLICK_DATA_API, Selector.DATA_TOGGLE, function (event) {
      // preventDefault only for <a> elements (which change the URL) not inside the collapsible element
      if (event.currentTarget.tagName === 'A') {
        event.preventDefault();
      }

      var $trigger = $$$1(this);
      var selector = Util.getSelectorFromElement(this);
      var selectors = [].slice.call(document.querySelectorAll(selector));
      $$$1(selectors).each(function () {
        var $target = $$$1(this);
        var data = $target.data(DATA_KEY);
        var config = data ? 'toggle' : $trigger.data();

        Collapse._jQueryInterface.call($target, config);
      });
    });
    /**
     * ------------------------------------------------------------------------
     * jQuery
     * ------------------------------------------------------------------------
     */

    $$$1.fn[NAME] = Collapse._jQueryInterface;
    $$$1.fn[NAME].Constructor = Collapse;

    $$$1.fn[NAME].noConflict = function () {
      $$$1.fn[NAME] = JQUERY_NO_CONFLICT;
      return Collapse._jQueryInterface;
    };

    return Collapse;
  }($);

  /**!
   * @fileOverview Kickass library to create and place poppers near their reference elements.
   * @version 1.14.3
   * @license
   * Copyright (c) 2016 Federico Zivolo and contributors
   *
   * Permission is hereby granted, free of charge, to any person obtaining a copy
   * of this software and associated documentation files (the "Software"), to deal
   * in the Software without restriction, including without limitation the rights
   * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   * copies of the Software, and to permit persons to whom the Software is
   * furnished to do so, subject to the following conditions:
   *
   * The above copyright notice and this permission notice shall be included in all
   * copies or substantial portions of the Software.
   *
   * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   * SOFTWARE.
   */
  var isBrowser = typeof window !== 'undefined' && typeof document !== 'undefined';

  var longerTimeoutBrowsers = ['Edge', 'Trident', 'Firefox'];
  var timeoutDuration = 0;
  for (var i = 0; i < longerTimeoutBrowsers.length; i += 1) {
    if (isBrowser && navigator.userAgent.indexOf(longerTimeoutBrowsers[i]) >= 0) {
      timeoutDuration = 1;
      break;
    }
  }

  function microtaskDebounce(fn) {
    var called = false;
    return function () {
      if (called) {
        return;
      }
      called = true;
      window.Promise.resolve().then(function () {
        called = false;
        fn();
      });
    };
  }

  function taskDebounce(fn) {
    var scheduled = false;
    return function () {
      if (!scheduled) {
        scheduled = true;
        setTimeout(function () {
          scheduled = false;
          fn();
        }, timeoutDuration);
      }
    };
  }

  var supportsMicroTasks = isBrowser && window.Promise;

  /**
  * Create a debounced version of a method, that's asynchronously deferred
  * but called in the minimum time possible.
  *
  * @method
  * @memberof Popper.Utils
  * @argument {Function} fn
  * @returns {Function}
  */
  var debounce = supportsMicroTasks ? microtaskDebounce : taskDebounce;

  /**
   * Check if the given variable is a function
   * @method
   * @memberof Popper.Utils
   * @argument {Any} functionToCheck - variable to check
   * @returns {Boolean} answer to: is a function?
   */
  function isFunction(functionToCheck) {
    var getType = {};
    return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
  }

  /**
   * Get CSS computed property of the given element
   * @method
   * @memberof Popper.Utils
   * @argument {Eement} element
   * @argument {String} property
   */
  function getStyleComputedProperty(element, property) {
    if (element.nodeType !== 1) {
      return [];
    }
    // NOTE: 1 DOM access here
    var css = getComputedStyle(element, null);
    return property ? css[property] : css;
  }

  /**
   * Returns the parentNode or the host of the element
   * @method
   * @memberof Popper.Utils
   * @argument {Element} element
   * @returns {Element} parent
   */
  function getParentNode(element) {
    if (element.nodeName === 'HTML') {
      return element;
    }
    return element.parentNode || element.host;
  }

  /**
   * Returns the scrolling parent of the given element
   * @method
   * @memberof Popper.Utils
   * @argument {Element} element
   * @returns {Element} scroll parent
   */
  function getScrollParent(element) {
    // Return body, `getScroll` will take care to get the correct `scrollTop` from it
    if (!element) {
      return document.body;
    }

    switch (element.nodeName) {
      case 'HTML':
      case 'BODY':
        return element.ownerDocument.body;
      case '#document':
        return element.body;
    }

    // Firefox want us to check `-x` and `-y` variations as well

    var _getStyleComputedProp = getStyleComputedProperty(element),
        overflow = _getStyleComputedProp.overflow,
        overflowX = _getStyleComputedProp.overflowX,
        overflowY = _getStyleComputedProp.overflowY;

    if (/(auto|scroll|overlay)/.test(overflow + overflowY + overflowX)) {
      return element;
    }

    return getScrollParent(getParentNode(element));
  }

  var isIE11 = isBrowser && !!(window.MSInputMethodContext && document.documentMode);
  var isIE10 = isBrowser && /MSIE 10/.test(navigator.userAgent);

  /**
   * Determines if the browser is Internet Explorer
   * @method
   * @memberof Popper.Utils
   * @param {Number} version to check
   * @returns {Boolean} isIE
   */
  function isIE(version) {
    if (version === 11) {
      return isIE11;
    }
    if (version === 10) {
      return isIE10;
    }
    return isIE11 || isIE10;
  }

  /**
   * Returns the offset parent of the given element
   * @method
   * @memberof Popper.Utils
   * @argument {Element} element
   * @returns {Element} offset parent
   */
  function getOffsetParent(element) {
    if (!element) {
      return document.documentElement;
    }

    var noOffsetParent = isIE(10) ? document.body : null;

    // NOTE: 1 DOM access here
    var offsetParent = element.offsetParent;
    // Skip hidden elements which don't have an offsetParent
    while (offsetParent === noOffsetParent && element.nextElementSibling) {
      offsetParent = (element = element.nextElementSibling).offsetParent;
    }

    var nodeName = offsetParent && offsetParent.nodeName;

    if (!nodeName || nodeName === 'BODY' || nodeName === 'HTML') {
      return element ? element.ownerDocument.documentElement : document.documentElement;
    }

    // .offsetParent will return the closest TD or TABLE in case
    // no offsetParent is present, I hate this job...
    if (['TD', 'TABLE'].indexOf(offsetParent.nodeName) !== -1 && getStyleComputedProperty(offsetParent, 'position') === 'static') {
      return getOffsetParent(offsetParent);
    }

    return offsetParent;
  }

  function isOffsetContainer(element) {
    var nodeName = element.nodeName;

    if (nodeName === 'BODY') {
      return false;
    }
    return nodeName === 'HTML' || getOffsetParent(element.firstElementChild) === element;
  }

  /**
   * Finds the root node (document, shadowDOM root) of the given element
   * @method
   * @memberof Popper.Utils
   * @argument {Element} node
   * @returns {Element} root node
   */
  function getRoot(node) {
    if (node.parentNode !== null) {
      return getRoot(node.parentNode);
    }

    return node;
  }

  /**
   * Finds the offset parent common to the two provided nodes
   * @method
   * @memberof Popper.Utils
   * @argument {Element} element1
   * @argument {Element} element2
   * @returns {Element} common offset parent
   */
  function findCommonOffsetParent(element1, element2) {
    // This check is needed to avoid errors in case one of the elements isn't defined for any reason
    if (!element1 || !element1.nodeType || !element2 || !element2.nodeType) {
      return document.documentElement;
    }

    // Here we make sure to give as "start" the element that comes first in the DOM
    var order = element1.compareDocumentPosition(element2) & Node.DOCUMENT_POSITION_FOLLOWING;
    var start = order ? element1 : element2;
    var end = order ? element2 : element1;

    // Get common ancestor container
    var range = document.createRange();
    range.setStart(start, 0);
    range.setEnd(end, 0);
    var commonAncestorContainer = range.commonAncestorContainer;

    // Both nodes are inside #document

    if (element1 !== commonAncestorContainer && element2 !== commonAncestorContainer || start.contains(end)) {
      if (isOffsetContainer(commonAncestorContainer)) {
        return commonAncestorContainer;
      }

      return getOffsetParent(commonAncestorContainer);
    }

    // one of the nodes is inside shadowDOM, find which one
    var element1root = getRoot(element1);
    if (element1root.host) {
      return findCommonOffsetParent(element1root.host, element2);
    } else {
      return findCommonOffsetParent(element1, getRoot(element2).host);
    }
  }

  /**
   * Gets the scroll value of the given element in the given side (top and left)
   * @method
   * @memberof Popper.Utils
   * @argument {Element} element
   * @argument {String} side `top` or `left`
   * @returns {number} amount of scrolled pixels
   */
  function getScroll(element) {
    var side = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'top';

    var upperSide = side === 'top' ? 'scrollTop' : 'scrollLeft';
    var nodeName = element.nodeName;

    if (nodeName === 'BODY' || nodeName === 'HTML') {
      var html = element.ownerDocument.documentElement;
      var scrollingElement = element.ownerDocument.scrollingElement || html;
      return scrollingElement[upperSide];
    }

    return element[upperSide];
  }

  /*
   * Sum or subtract the element scroll values (left and top) from a given rect object
   * @method
   * @memberof Popper.Utils
   * @param {Object} rect - Rect object you want to change
   * @param {HTMLElement} element - The element from the function reads the scroll values
   * @param {Boolean} subtract - set to true if you want to subtract the scroll values
   * @return {Object} rect - The modifier rect object
   */
  function includeScroll(rect, element) {
    var subtract = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

    var scrollTop = getScroll(element, 'top');
    var scrollLeft = getScroll(element, 'left');
    var modifier = subtract ? -1 : 1;
    rect.top += scrollTop * modifier;
    rect.bottom += scrollTop * modifier;
    rect.left += scrollLeft * modifier;
    rect.right += scrollLeft * modifier;
    return rect;
  }

  /*
   * Helper to detect borders of a given element
   * @method
   * @memberof Popper.Utils
   * @param {CSSStyleDeclaration} styles
   * Result of `getStyleComputedProperty` on the given element
   * @param {String} axis - `x` or `y`
   * @return {number} borders - The borders size of the given axis
   */

  function getBordersSize(styles, axis) {
    var sideA = axis === 'x' ? 'Left' : 'Top';
    var sideB = sideA === 'Left' ? 'Right' : 'Bottom';

    return parseFloat(styles['border' + sideA + 'Width'], 10) + parseFloat(styles['border' + sideB + 'Width'], 10);
  }

  function getSize(axis, body, html, computedStyle) {
    return Math.max(body['offset' + axis], body['scroll' + axis], html['client' + axis], html['offset' + axis], html['scroll' + axis], isIE(10) ? html['offset' + axis] + computedStyle['margin' + (axis === 'Height' ? 'Top' : 'Left')] + computedStyle['margin' + (axis === 'Height' ? 'Bottom' : 'Right')] : 0);
  }

  function getWindowSizes() {
    var body = document.body;
    var html = document.documentElement;
    var computedStyle = isIE(10) && getComputedStyle(html);

    return {
      height: getSize('Height', body, html, computedStyle),
      width: getSize('Width', body, html, computedStyle)
    };
  }

  var classCallCheck = function (instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  };

  var createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();





  var defineProperty = function (obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  };

  var _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  /**
   * Given element offsets, generate an output similar to getBoundingClientRect
   * @method
   * @memberof Popper.Utils
   * @argument {Object} offsets
   * @returns {Object} ClientRect like output
   */
  function getClientRect(offsets) {
    return _extends({}, offsets, {
      right: offsets.left + offsets.width,
      bottom: offsets.top + offsets.height
    });
  }

  /**
   * Get bounding client rect of given element
   * @method
   * @memberof Popper.Utils
   * @param {HTMLElement} element
   * @return {Object} client rect
   */
  function getBoundingClientRect(element) {
    var rect = {};

    // IE10 10 FIX: Please, don't ask, the element isn't
    // considered in DOM in some circumstances...
    // This isn't reproducible in IE10 compatibility mode of IE11
    try {
      if (isIE(10)) {
        rect = element.getBoundingClientRect();
        var scrollTop = getScroll(element, 'top');
        var scrollLeft = getScroll(element, 'left');
        rect.top += scrollTop;
        rect.left += scrollLeft;
        rect.bottom += scrollTop;
        rect.right += scrollLeft;
      } else {
        rect = element.getBoundingClientRect();
      }
    } catch (e) {}

    var result = {
      left: rect.left,
      top: rect.top,
      width: rect.right - rect.left,
      height: rect.bottom - rect.top
    };

    // subtract scrollbar size from sizes
    var sizes = element.nodeName === 'HTML' ? getWindowSizes() : {};
    var width = sizes.width || element.clientWidth || result.right - result.left;
    var height = sizes.height || element.clientHeight || result.bottom - result.top;

    var horizScrollbar = element.offsetWidth - width;
    var vertScrollbar = element.offsetHeight - height;

    // if an hypothetical scrollbar is detected, we must be sure it's not a `border`
    // we make this check conditional for performance reasons
    if (horizScrollbar || vertScrollbar) {
      var styles = getStyleComputedProperty(element);
      horizScrollbar -= getBordersSize(styles, 'x');
      vertScrollbar -= getBordersSize(styles, 'y');

      result.width -= horizScrollbar;
      result.height -= vertScrollbar;
    }

    return getClientRect(result);
  }

  function getOffsetRectRelativeToArbitraryNode(children, parent) {
    var fixedPosition = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

    var isIE10 = isIE(10);
    var isHTML = parent.nodeName === 'HTML';
    var childrenRect = getBoundingClientRect(children);
    var parentRect = getBoundingClientRect(parent);
    var scrollParent = getScrollParent(children);

    var styles = getStyleComputedProperty(parent);
    var borderTopWidth = parseFloat(styles.borderTopWidth, 10);
    var borderLeftWidth = parseFloat(styles.borderLeftWidth, 10);

    // In cases where the parent is fixed, we must ignore negative scroll in offset calc
    if (fixedPosition && parent.nodeName === 'HTML') {
      parentRect.top = Math.max(parentRect.top, 0);
      parentRect.left = Math.max(parentRect.left, 0);
    }
    var offsets = getClientRect({
      top: childrenRect.top - parentRect.top - borderTopWidth,
      left: childrenRect.left - parentRect.left - borderLeftWidth,
      width: childrenRect.width,
      height: childrenRect.height
    });
    offsets.marginTop = 0;
    offsets.marginLeft = 0;

    // Subtract margins of documentElement in case it's being used as parent
    // we do this only on HTML because it's the only element that behaves
    // differently when margins are applied to it. The margins are included in
    // the box of the documentElement, in the other cases not.
    if (!isIE10 && isHTML) {
      var marginTop = parseFloat(styles.marginTop, 10);
      var marginLeft = parseFloat(styles.marginLeft, 10);

      offsets.top -= borderTopWidth - marginTop;
      offsets.bottom -= borderTopWidth - marginTop;
      offsets.left -= borderLeftWidth - marginLeft;
      offsets.right -= borderLeftWidth - marginLeft;

      // Attach marginTop and marginLeft because in some circumstances we may need them
      offsets.marginTop = marginTop;
      offsets.marginLeft = marginLeft;
    }

    if (isIE10 && !fixedPosition ? parent.contains(scrollParent) : parent === scrollParent && scrollParent.nodeName !== 'BODY') {
      offsets = includeScroll(offsets, parent);
    }

    return offsets;
  }

  function getViewportOffsetRectRelativeToArtbitraryNode(element) {
    var excludeScroll = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

    var html = element.ownerDocument.documentElement;
    var relativeOffset = getOffsetRectRelativeToArbitraryNode(element, html);
    var width = Math.max(html.clientWidth, window.innerWidth || 0);
    var height = Math.max(html.clientHeight, window.innerHeight || 0);

    var scrollTop = !excludeScroll ? getScroll(html) : 0;
    var scrollLeft = !excludeScroll ? getScroll(html, 'left') : 0;

    var offset = {
      top: scrollTop - relativeOffset.top + relativeOffset.marginTop,
      left: scrollLeft - relativeOffset.left + relativeOffset.marginLeft,
      width: width,
      height: height
    };

    return getClientRect(offset);
  }

  /**
   * Check if the given element is fixed or is inside a fixed parent
   * @method
   * @memberof Popper.Utils
   * @argument {Element} element
   * @argument {Element} customContainer
   * @returns {Boolean} answer to "isFixed?"
   */
  function isFixed(element) {
    var nodeName = element.nodeName;
    if (nodeName === 'BODY' || nodeName === 'HTML') {
      return false;
    }
    if (getStyleComputedProperty(element, 'position') === 'fixed') {
      return true;
    }
    return isFixed(getParentNode(element));
  }

  /**
   * Finds the first parent of an element that has a transformed property defined
   * @method
   * @memberof Popper.Utils
   * @argument {Element} element
   * @returns {Element} first transformed parent or documentElement
   */

  function getFixedPositionOffsetParent(element) {
    // This check is needed to avoid errors in case one of the elements isn't defined for any reason
    if (!element || !element.parentElement || isIE()) {
      return document.documentElement;
    }
    var el = element.parentElement;
    while (el && getStyleComputedProperty(el, 'transform') === 'none') {
      el = el.parentElement;
    }
    return el || document.documentElement;
  }

  /**
   * Computed the boundaries limits and return them
   * @method
   * @memberof Popper.Utils
   * @param {HTMLElement} popper
   * @param {HTMLElement} reference
   * @param {number} padding
   * @param {HTMLElement} boundariesElement - Element used to define the boundaries
   * @param {Boolean} fixedPosition - Is in fixed position mode
   * @returns {Object} Coordinates of the boundaries
   */
  function getBoundaries(popper, reference, padding, boundariesElement) {
    var fixedPosition = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;

    // NOTE: 1 DOM access here

    var boundaries = { top: 0, left: 0 };
    var offsetParent = fixedPosition ? getFixedPositionOffsetParent(popper) : findCommonOffsetParent(popper, reference);

    // Handle viewport case
    if (boundariesElement === 'viewport') {
      boundaries = getViewportOffsetRectRelativeToArtbitraryNode(offsetParent, fixedPosition);
    } else {
      // Handle other cases based on DOM element used as boundaries
      var boundariesNode = void 0;
      if (boundariesElement === 'scrollParent') {
        boundariesNode = getScrollParent(getParentNode(reference));
        if (boundariesNode.nodeName === 'BODY') {
          boundariesNode = popper.ownerDocument.documentElement;
        }
      } else if (boundariesElement === 'window') {
        boundariesNode = popper.ownerDocument.documentElement;
      } else {
        boundariesNode = boundariesElement;
      }

      var offsets = getOffsetRectRelativeToArbitraryNode(boundariesNode, offsetParent, fixedPosition);

      // In case of HTML, we need a different computation
      if (boundariesNode.nodeName === 'HTML' && !isFixed(offsetParent)) {
        var _getWindowSizes = getWindowSizes(),
            height = _getWindowSizes.height,
            width = _getWindowSizes.width;

        boundaries.top += offsets.top - offsets.marginTop;
        boundaries.bottom = height + offsets.top;
        boundaries.left += offsets.left - offsets.marginLeft;
        boundaries.right = width + offsets.left;
      } else {
        // for all the other DOM elements, this one is good
        boundaries = offsets;
      }
    }

    // Add paddings
    boundaries.left += padding;
    boundaries.top += padding;
    boundaries.right -= padding;
    boundaries.bottom -= padding;

    return boundaries;
  }

  function getArea(_ref) {
    var width = _ref.width,
        height = _ref.height;

    return width * height;
  }

  /**
   * Utility used to transform the `auto` placement to the placement with more
   * available space.
   * @method
   * @memberof Popper.Utils
   * @argument {Object} data - The data object generated by update method
   * @argument {Object} options - Modifiers configuration and options
   * @returns {Object} The data object, properly modified
   */
  function computeAutoPlacement(placement, refRect, popper, reference, boundariesElement) {
    var padding = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : 0;

    if (placement.indexOf('auto') === -1) {
      return placement;
    }

    var boundaries = getBoundaries(popper, reference, padding, boundariesElement);

    var rects = {
      top: {
        width: boundaries.width,
        height: refRect.top - boundaries.top
      },
      right: {
        width: boundaries.right - refRect.right,
        height: boundaries.height
      },
      bottom: {
        width: boundaries.width,
        height: boundaries.bottom - refRect.bottom
      },
      left: {
        width: refRect.left - boundaries.left,
        height: boundaries.height
      }
    };

    var sortedAreas = Object.keys(rects).map(function (key) {
      return _extends({
        key: key
      }, rects[key], {
        area: getArea(rects[key])
      });
    }).sort(function (a, b) {
      return b.area - a.area;
    });

    var filteredAreas = sortedAreas.filter(function (_ref2) {
      var width = _ref2.width,
          height = _ref2.height;
      return width >= popper.clientWidth && height >= popper.clientHeight;
    });

    var computedPlacement = filteredAreas.length > 0 ? filteredAreas[0].key : sortedAreas[0].key;

    var variation = placement.split('-')[1];

    return computedPlacement + (variation ? '-' + variation : '');
  }

  /**
   * Get offsets to the reference element
   * @method
   * @memberof Popper.Utils
   * @param {Object} state
   * @param {Element} popper - the popper element
   * @param {Element} reference - the reference element (the popper will be relative to this)
   * @param {Element} fixedPosition - is in fixed position mode
   * @returns {Object} An object containing the offsets which will be applied to the popper
   */
  function getReferenceOffsets(state, popper, reference) {
    var fixedPosition = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

    var commonOffsetParent = fixedPosition ? getFixedPositionOffsetParent(popper) : findCommonOffsetParent(popper, reference);
    return getOffsetRectRelativeToArbitraryNode(reference, commonOffsetParent, fixedPosition);
  }

  /**
   * Get the outer sizes of the given element (offset size + margins)
   * @method
   * @memberof Popper.Utils
   * @argument {Element} element
   * @returns {Object} object containing width and height properties
   */
  function getOuterSizes(element) {
    var styles = getComputedStyle(element);
    var x = parseFloat(styles.marginTop) + parseFloat(styles.marginBottom);
    var y = parseFloat(styles.marginLeft) + parseFloat(styles.marginRight);
    var result = {
      width: element.offsetWidth + y,
      height: element.offsetHeight + x
    };
    return result;
  }

  /**
   * Get the opposite placement of the given one
   * @method
   * @memberof Popper.Utils
   * @argument {String} placement
   * @returns {String} flipped placement
   */
  function getOppositePlacement(placement) {
    var hash = { left: 'right', right: 'left', bottom: 'top', top: 'bottom' };
    return placement.replace(/left|right|bottom|top/g, function (matched) {
      return hash[matched];
    });
  }

  /**
   * Get offsets to the popper
   * @method
   * @memberof Popper.Utils
   * @param {Object} position - CSS position the Popper will get applied
   * @param {HTMLElement} popper - the popper element
   * @param {Object} referenceOffsets - the reference offsets (the popper will be relative to this)
   * @param {String} placement - one of the valid placement options
   * @returns {Object} popperOffsets - An object containing the offsets which will be applied to the popper
   */
  function getPopperOffsets(popper, referenceOffsets, placement) {
    placement = placement.split('-')[0];

    // Get popper node sizes
    var popperRect = getOuterSizes(popper);

    // Add position, width and height to our offsets object
    var popperOffsets = {
      width: popperRect.width,
      height: popperRect.height
    };

    // depending by the popper placement we have to compute its offsets slightly differently
    var isHoriz = ['right', 'left'].indexOf(placement) !== -1;
    var mainSide = isHoriz ? 'top' : 'left';
    var secondarySide = isHoriz ? 'left' : 'top';
    var measurement = isHoriz ? 'height' : 'width';
    var secondaryMeasurement = !isHoriz ? 'height' : 'width';

    popperOffsets[mainSide] = referenceOffsets[mainSide] + referenceOffsets[measurement] / 2 - popperRect[measurement] / 2;
    if (placement === secondarySide) {
      popperOffsets[secondarySide] = referenceOffsets[secondarySide] - popperRect[secondaryMeasurement];
    } else {
      popperOffsets[secondarySide] = referenceOffsets[getOppositePlacement(secondarySide)];
    }

    return popperOffsets;
  }

  /**
   * Mimics the `find` method of Array
   * @method
   * @memberof Popper.Utils
   * @argument {Array} arr
   * @argument prop
   * @argument value
   * @returns index or -1
   */
  function find(arr, check) {
    // use native find if supported
    if (Array.prototype.find) {
      return arr.find(check);
    }

    // use `filter` to obtain the same behavior of `find`
    return arr.filter(check)[0];
  }

  /**
   * Return the index of the matching object
   * @method
   * @memberof Popper.Utils
   * @argument {Array} arr
   * @argument prop
   * @argument value
   * @returns index or -1
   */
  function findIndex(arr, prop, value) {
    // use native findIndex if supported
    if (Array.prototype.findIndex) {
      return arr.findIndex(function (cur) {
        return cur[prop] === value;
      });
    }

    // use `find` + `indexOf` if `findIndex` isn't supported
    var match = find(arr, function (obj) {
      return obj[prop] === value;
    });
    return arr.indexOf(match);
  }

  /**
   * Loop trough the list of modifiers and run them in order,
   * each of them will then edit the data object.
   * @method
   * @memberof Popper.Utils
   * @param {dataObject} data
   * @param {Array} modifiers
   * @param {String} ends - Optional modifier name used as stopper
   * @returns {dataObject}
   */
  function runModifiers(modifiers, data, ends) {
    var modifiersToRun = ends === undefined ? modifiers : modifiers.slice(0, findIndex(modifiers, 'name', ends));

    modifiersToRun.forEach(function (modifier) {
      if (modifier['function']) {
        // eslint-disable-line dot-notation
        console.warn('`modifier.function` is deprecated, use `modifier.fn`!');
      }
      var fn = modifier['function'] || modifier.fn; // eslint-disable-line dot-notation
      if (modifier.enabled && isFunction(fn)) {
        // Add properties to offsets to make them a complete clientRect object
        // we do this before each modifier to make sure the previous one doesn't
        // mess with these values
        data.offsets.popper = getClientRect(data.offsets.popper);
        data.offsets.reference = getClientRect(data.offsets.reference);

        data = fn(data, modifier);
      }
    });

    return data;
  }

  /**
   * Updates the position of the popper, computing the new offsets and applying
   * the new style.<br />
   * Prefer `scheduleUpdate` over `update` because of performance reasons.
   * @method
   * @memberof Popper
   */
  function update() {
    // if popper is destroyed, don't perform any further update
    if (this.state.isDestroyed) {
      return;
    }

    var data = {
      instance: this,
      styles: {},
      arrowStyles: {},
      attributes: {},
      flipped: false,
      offsets: {}
    };

    // compute reference element offsets
    data.offsets.reference = getReferenceOffsets(this.state, this.popper, this.reference, this.options.positionFixed);

    // compute auto placement, store placement inside the data object,
    // modifiers will be able to edit `placement` if needed
    // and refer to originalPlacement to know the original value
    data.placement = computeAutoPlacement(this.options.placement, data.offsets.reference, this.popper, this.reference, this.options.modifiers.flip.boundariesElement, this.options.modifiers.flip.padding);

    // store the computed placement inside `originalPlacement`
    data.originalPlacement = data.placement;

    data.positionFixed = this.options.positionFixed;

    // compute the popper offsets
    data.offsets.popper = getPopperOffsets(this.popper, data.offsets.reference, data.placement);

    data.offsets.popper.position = this.options.positionFixed ? 'fixed' : 'absolute';

    // run the modifiers
    data = runModifiers(this.modifiers, data);

    // the first `update` will call `onCreate` callback
    // the other ones will call `onUpdate` callback
    if (!this.state.isCreated) {
      this.state.isCreated = true;
      this.options.onCreate(data);
    } else {
      this.options.onUpdate(data);
    }
  }

  /**
   * Helper used to know if the given modifier is enabled.
   * @method
   * @memberof Popper.Utils
   * @returns {Boolean}
   */
  function isModifierEnabled(modifiers, modifierName) {
    return modifiers.some(function (_ref) {
      var name = _ref.name,
          enabled = _ref.enabled;
      return enabled && name === modifierName;
    });
  }

  /**
   * Get the prefixed supported property name
   * @method
   * @memberof Popper.Utils
   * @argument {String} property (camelCase)
   * @returns {String} prefixed property (camelCase or PascalCase, depending on the vendor prefix)
   */
  function getSupportedPropertyName(property) {
    var prefixes = [false, 'ms', 'Webkit', 'Moz', 'O'];
    var upperProp = property.charAt(0).toUpperCase() + property.slice(1);

    for (var i = 0; i < prefixes.length; i++) {
      var prefix = prefixes[i];
      var toCheck = prefix ? '' + prefix + upperProp : property;
      if (typeof document.body.style[toCheck] !== 'undefined') {
        return toCheck;
      }
    }
    return null;
  }

  /**
   * Destroy the popper
   * @method
   * @memberof Popper
   */
  function destroy() {
    this.state.isDestroyed = true;

    // touch DOM only if `applyStyle` modifier is enabled
    if (isModifierEnabled(this.modifiers, 'applyStyle')) {
      this.popper.removeAttribute('x-placement');
      this.popper.style.position = '';
      this.popper.style.top = '';
      this.popper.style.left = '';
      this.popper.style.right = '';
      this.popper.style.bottom = '';
      this.popper.style.willChange = '';
      this.popper.style[getSupportedPropertyName('transform')] = '';
    }

    this.disableEventListeners();

    // remove the popper if user explicity asked for the deletion on destroy
    // do not use `remove` because IE11 doesn't support it
    if (this.options.removeOnDestroy) {
      this.popper.parentNode.removeChild(this.popper);
    }
    return this;
  }

  /**
   * Get the window associated with the element
   * @argument {Element} element
   * @returns {Window}
   */
  function getWindow(element) {
    var ownerDocument = element.ownerDocument;
    return ownerDocument ? ownerDocument.defaultView : window;
  }

  function attachToScrollParents(scrollParent, event, callback, scrollParents) {
    var isBody = scrollParent.nodeName === 'BODY';
    var target = isBody ? scrollParent.ownerDocument.defaultView : scrollParent;
    target.addEventListener(event, callback, { passive: true });

    if (!isBody) {
      attachToScrollParents(getScrollParent(target.parentNode), event, callback, scrollParents);
    }
    scrollParents.push(target);
  }

  /**
   * Setup needed event listeners used to update the popper position
   * @method
   * @memberof Popper.Utils
   * @private
   */
  function setupEventListeners(reference, options, state, updateBound) {
    // Resize event listener on window
    state.updateBound = updateBound;
    getWindow(reference).addEventListener('resize', state.updateBound, { passive: true });

    // Scroll event listener on scroll parents
    var scrollElement = getScrollParent(reference);
    attachToScrollParents(scrollElement, 'scroll', state.updateBound, state.scrollParents);
    state.scrollElement = scrollElement;
    state.eventsEnabled = true;

    return state;
  }

  /**
   * It will add resize/scroll events and start recalculating
   * position of the popper element when they are triggered.
   * @method
   * @memberof Popper
   */
  function enableEventListeners() {
    if (!this.state.eventsEnabled) {
      this.state = setupEventListeners(this.reference, this.options, this.state, this.scheduleUpdate);
    }
  }

  /**
   * Remove event listeners used to update the popper position
   * @method
   * @memberof Popper.Utils
   * @private
   */
  function removeEventListeners(reference, state) {
    // Remove resize event listener on window
    getWindow(reference).removeEventListener('resize', state.updateBound);

    // Remove scroll event listener on scroll parents
    state.scrollParents.forEach(function (target) {
      target.removeEventListener('scroll', state.updateBound);
    });

    // Reset state
    state.updateBound = null;
    state.scrollParents = [];
    state.scrollElement = null;
    state.eventsEnabled = false;
    return state;
  }

  /**
   * It will remove resize/scroll events and won't recalculate popper position
   * when they are triggered. It also won't trigger onUpdate callback anymore,
   * unless you call `update` method manually.
   * @method
   * @memberof Popper
   */
  function disableEventListeners() {
    if (this.state.eventsEnabled) {
      cancelAnimationFrame(this.scheduleUpdate);
      this.state = removeEventListeners(this.reference, this.state);
    }
  }

  /**
   * Tells if a given input is a number
   * @method
   * @memberof Popper.Utils
   * @param {*} input to check
   * @return {Boolean}
   */
  function isNumeric(n) {
    return n !== '' && !isNaN(parseFloat(n)) && isFinite(n);
  }

  /**
   * Set the style to the given popper
   * @method
   * @memberof Popper.Utils
   * @argument {Element} element - Element to apply the style to
   * @argument {Object} styles
   * Object with a list of properties and values which will be applied to the element
   */
  function setStyles(element, styles) {
    Object.keys(styles).forEach(function (prop) {
      var unit = '';
      // add unit if the value is numeric and is one of the following
      if (['width', 'height', 'top', 'right', 'bottom', 'left'].indexOf(prop) !== -1 && isNumeric(styles[prop])) {
        unit = 'px';
      }
      element.style[prop] = styles[prop] + unit;
    });
  }

  /**
   * Set the attributes to the given popper
   * @method
   * @memberof Popper.Utils
   * @argument {Element} element - Element to apply the attributes to
   * @argument {Object} styles
   * Object with a list of properties and values which will be applied to the element
   */
  function setAttributes(element, attributes) {
    Object.keys(attributes).forEach(function (prop) {
      var value = attributes[prop];
      if (value !== false) {
        element.setAttribute(prop, attributes[prop]);
      } else {
        element.removeAttribute(prop);
      }
    });
  }

  /**
   * @function
   * @memberof Modifiers
   * @argument {Object} data - The data object generated by `update` method
   * @argument {Object} data.styles - List of style properties - values to apply to popper element
   * @argument {Object} data.attributes - List of attribute properties - values to apply to popper element
   * @argument {Object} options - Modifiers configuration and options
   * @returns {Object} The same data object
   */
  function applyStyle(data) {
    // any property present in `data.styles` will be applied to the popper,
    // in this way we can make the 3rd party modifiers add custom styles to it
    // Be aware, modifiers could override the properties defined in the previous
    // lines of this modifier!
    setStyles(data.instance.popper, data.styles);

    // any property present in `data.attributes` will be applied to the popper,
    // they will be set as HTML attributes of the element
    setAttributes(data.instance.popper, data.attributes);

    // if arrowElement is defined and arrowStyles has some properties
    if (data.arrowElement && Object.keys(data.arrowStyles).length) {
      setStyles(data.arrowElement, data.arrowStyles);
    }

    return data;
  }

  /**
   * Set the x-placement attribute before everything else because it could be used
   * to add margins to the popper margins needs to be calculated to get the
   * correct popper offsets.
   * @method
   * @memberof Popper.modifiers
   * @param {HTMLElement} reference - The reference element used to position the popper
   * @param {HTMLElement} popper - The HTML element used as popper
   * @param {Object} options - Popper.js options
   */
  function applyStyleOnLoad(reference, popper, options, modifierOptions, state) {
    // compute reference element offsets
    var referenceOffsets = getReferenceOffsets(state, popper, reference, options.positionFixed);

    // compute auto placement, store placement inside the data object,
    // modifiers will be able to edit `placement` if needed
    // and refer to originalPlacement to know the original value
    var placement = computeAutoPlacement(options.placement, referenceOffsets, popper, reference, options.modifiers.flip.boundariesElement, options.modifiers.flip.padding);

    popper.setAttribute('x-placement', placement);

    // Apply `position` to popper before anything else because
    // without the position applied we can't guarantee correct computations
    setStyles(popper, { position: options.positionFixed ? 'fixed' : 'absolute' });

    return options;
  }

  /**
   * @function
   * @memberof Modifiers
   * @argument {Object} data - The data object generated by `update` method
   * @argument {Object} options - Modifiers configuration and options
   * @returns {Object} The data object, properly modified
   */
  function computeStyle(data, options) {
    var x = options.x,
        y = options.y;
    var popper = data.offsets.popper;

    // Remove this legacy support in Popper.js v2

    var legacyGpuAccelerationOption = find(data.instance.modifiers, function (modifier) {
      return modifier.name === 'applyStyle';
    }).gpuAcceleration;
    if (legacyGpuAccelerationOption !== undefined) {
      console.warn('WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!');
    }
    var gpuAcceleration = legacyGpuAccelerationOption !== undefined ? legacyGpuAccelerationOption : options.gpuAcceleration;

    var offsetParent = getOffsetParent(data.instance.popper);
    var offsetParentRect = getBoundingClientRect(offsetParent);

    // Styles
    var styles = {
      position: popper.position
    };

    // Avoid blurry text by using full pixel integers.
    // For pixel-perfect positioning, top/bottom prefers rounded
    // values, while left/right prefers floored values.
    var offsets = {
      left: Math.floor(popper.left),
      top: Math.round(popper.top),
      bottom: Math.round(popper.bottom),
      right: Math.floor(popper.right)
    };

    var sideA = x === 'bottom' ? 'top' : 'bottom';
    var sideB = y === 'right' ? 'left' : 'right';

    // if gpuAcceleration is set to `true` and transform is supported,
    //  we use `translate3d` to apply the position to the popper we
    // automatically use the supported prefixed version if needed
    var prefixedProperty = getSupportedPropertyName('transform');

    // now, let's make a step back and look at this code closely (wtf?)
    // If the content of the popper grows once it's been positioned, it
    // may happen that the popper gets misplaced because of the new content
    // overflowing its reference element
    // To avoid this problem, we provide two options (x and y), which allow
    // the consumer to define the offset origin.
    // If we position a popper on top of a reference element, we can set
    // `x` to `top` to make the popper grow towards its top instead of
    // its bottom.
    var left = void 0,
        top = void 0;
    if (sideA === 'bottom') {
      top = -offsetParentRect.height + offsets.bottom;
    } else {
      top = offsets.top;
    }
    if (sideB === 'right') {
      left = -offsetParentRect.width + offsets.right;
    } else {
      left = offsets.left;
    }
    if (gpuAcceleration && prefixedProperty) {
      styles[prefixedProperty] = 'translate3d(' + left + 'px, ' + top + 'px, 0)';
      styles[sideA] = 0;
      styles[sideB] = 0;
      styles.willChange = 'transform';
    } else {
      // othwerise, we use the standard `top`, `left`, `bottom` and `right` properties
      var invertTop = sideA === 'bottom' ? -1 : 1;
      var invertLeft = sideB === 'right' ? -1 : 1;
      styles[sideA] = top * invertTop;
      styles[sideB] = left * invertLeft;
      styles.willChange = sideA + ', ' + sideB;
    }

    // Attributes
    var attributes = {
      'x-placement': data.placement
    };

    // Update `data` attributes, styles and arrowStyles
    data.attributes = _extends({}, attributes, data.attributes);
    data.styles = _extends({}, styles, data.styles);
    data.arrowStyles = _extends({}, data.offsets.arrow, data.arrowStyles);

    return data;
  }

  /**
   * Helper used to know if the given modifier depends from another one.<br />
   * It checks if the needed modifier is listed and enabled.
   * @method
   * @memberof Popper.Utils
   * @param {Array} modifiers - list of modifiers
   * @param {String} requestingName - name of requesting modifier
   * @param {String} requestedName - name of requested modifier
   * @returns {Boolean}
   */
  function isModifierRequired(modifiers, requestingName, requestedName) {
    var requesting = find(modifiers, function (_ref) {
      var name = _ref.name;
      return name === requestingName;
    });

    var isRequired = !!requesting && modifiers.some(function (modifier) {
      return modifier.name === requestedName && modifier.enabled && modifier.order < requesting.order;
    });

    if (!isRequired) {
      var _requesting = '`' + requestingName + '`';
      var requested = '`' + requestedName + '`';
      console.warn(requested + ' modifier is required by ' + _requesting + ' modifier in order to work, be sure to include it before ' + _requesting + '!');
    }
    return isRequired;
  }

  /**
   * @function
   * @memberof Modifiers
   * @argument {Object} data - The data object generated by update method
   * @argument {Object} options - Modifiers configuration and options
   * @returns {Object} The data object, properly modified
   */
  function arrow(data, options) {
    var _data$offsets$arrow;

    // arrow depends on keepTogether in order to work
    if (!isModifierRequired(data.instance.modifiers, 'arrow', 'keepTogether')) {
      return data;
    }

    var arrowElement = options.element;

    // if arrowElement is a string, suppose it's a CSS selector
    if (typeof arrowElement === 'string') {
      arrowElement = data.instance.popper.querySelector(arrowElement);

      // if arrowElement is not found, don't run the modifier
      if (!arrowElement) {
        return data;
      }
    } else {
      // if the arrowElement isn't a query selector we must check that the
      // provided DOM node is child of its popper node
      if (!data.instance.popper.contains(arrowElement)) {
        console.warn('WARNING: `arrow.element` must be child of its popper element!');
        return data;
      }
    }

    var placement = data.placement.split('-')[0];
    var _data$offsets = data.offsets,
        popper = _data$offsets.popper,
        reference = _data$offsets.reference;

    var isVertical = ['left', 'right'].indexOf(placement) !== -1;

    var len = isVertical ? 'height' : 'width';
    var sideCapitalized = isVertical ? 'Top' : 'Left';
    var side = sideCapitalized.toLowerCase();
    var altSide = isVertical ? 'left' : 'top';
    var opSide = isVertical ? 'bottom' : 'right';
    var arrowElementSize = getOuterSizes(arrowElement)[len];

    //
    // extends keepTogether behavior making sure the popper and its
    // reference have enough pixels in conjuction
    //

    // top/left side
    if (reference[opSide] - arrowElementSize < popper[side]) {
      data.offsets.popper[side] -= popper[side] - (reference[opSide] - arrowElementSize);
    }
    // bottom/right side
    if (reference[side] + arrowElementSize > popper[opSide]) {
      data.offsets.popper[side] += reference[side] + arrowElementSize - popper[opSide];
    }
    data.offsets.popper = getClientRect(data.offsets.popper);

    // compute center of the popper
    var center = reference[side] + reference[len] / 2 - arrowElementSize / 2;

    // Compute the sideValue using the updated popper offsets
    // take popper margin in account because we don't have this info available
    var css = getStyleComputedProperty(data.instance.popper);
    var popperMarginSide = parseFloat(css['margin' + sideCapitalized], 10);
    var popperBorderSide = parseFloat(css['border' + sideCapitalized + 'Width'], 10);
    var sideValue = center - data.offsets.popper[side] - popperMarginSide - popperBorderSide;

    // prevent arrowElement from being placed not contiguously to its popper
    sideValue = Math.max(Math.min(popper[len] - arrowElementSize, sideValue), 0);

    data.arrowElement = arrowElement;
    data.offsets.arrow = (_data$offsets$arrow = {}, defineProperty(_data$offsets$arrow, side, Math.round(sideValue)), defineProperty(_data$offsets$arrow, altSide, ''), _data$offsets$arrow);

    return data;
  }

  /**
   * Get the opposite placement variation of the given one
   * @method
   * @memberof Popper.Utils
   * @argument {String} placement variation
   * @returns {String} flipped placement variation
   */
  function getOppositeVariation(variation) {
    if (variation === 'end') {
      return 'start';
    } else if (variation === 'start') {
      return 'end';
    }
    return variation;
  }

  /**
   * List of accepted placements to use as values of the `placement` option.<br />
   * Valid placements are:
   * - `auto`
   * - `top`
   * - `right`
   * - `bottom`
   * - `left`
   *
   * Each placement can have a variation from this list:
   * - `-start`
   * - `-end`
   *
   * Variations are interpreted easily if you think of them as the left to right
   * written languages. Horizontally (`top` and `bottom`), `start` is left and `end`
   * is right.<br />
   * Vertically (`left` and `right`), `start` is top and `end` is bottom.
   *
   * Some valid examples are:
   * - `top-end` (on top of reference, right aligned)
   * - `right-start` (on right of reference, top aligned)
   * - `bottom` (on bottom, centered)
   * - `auto-right` (on the side with more space available, alignment depends by placement)
   *
   * @static
   * @type {Array}
   * @enum {String}
   * @readonly
   * @method placements
   * @memberof Popper
   */
  var placements = ['auto-start', 'auto', 'auto-end', 'top-start', 'top', 'top-end', 'right-start', 'right', 'right-end', 'bottom-end', 'bottom', 'bottom-start', 'left-end', 'left', 'left-start'];

  // Get rid of `auto` `auto-start` and `auto-end`
  var validPlacements = placements.slice(3);

  /**
   * Given an initial placement, returns all the subsequent placements
   * clockwise (or counter-clockwise).
   *
   * @method
   * @memberof Popper.Utils
   * @argument {String} placement - A valid placement (it accepts variations)
   * @argument {Boolean} counter - Set to true to walk the placements counterclockwise
   * @returns {Array} placements including their variations
   */
  function clockwise(placement) {
    var counter = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

    var index = validPlacements.indexOf(placement);
    var arr = validPlacements.slice(index + 1).concat(validPlacements.slice(0, index));
    return counter ? arr.reverse() : arr;
  }

  var BEHAVIORS = {
    FLIP: 'flip',
    CLOCKWISE: 'clockwise',
    COUNTERCLOCKWISE: 'counterclockwise'
  };

  /**
   * @function
   * @memberof Modifiers
   * @argument {Object} data - The data object generated by update method
   * @argument {Object} options - Modifiers configuration and options
   * @returns {Object} The data object, properly modified
   */
  function flip(data, options) {
    // if `inner` modifier is enabled, we can't use the `flip` modifier
    if (isModifierEnabled(data.instance.modifiers, 'inner')) {
      return data;
    }

    if (data.flipped && data.placement === data.originalPlacement) {
      // seems like flip is trying to loop, probably there's not enough space on any of the flippable sides
      return data;
    }

    var boundaries = getBoundaries(data.instance.popper, data.instance.reference, options.padding, options.boundariesElement, data.positionFixed);

    var placement = data.placement.split('-')[0];
    var placementOpposite = getOppositePlacement(placement);
    var variation = data.placement.split('-')[1] || '';

    var flipOrder = [];

    switch (options.behavior) {
      case BEHAVIORS.FLIP:
        flipOrder = [placement, placementOpposite];
        break;
      case BEHAVIORS.CLOCKWISE:
        flipOrder = clockwise(placement);
        break;
      case BEHAVIORS.COUNTERCLOCKWISE:
        flipOrder = clockwise(placement, true);
        break;
      default:
        flipOrder = options.behavior;
    }

    flipOrder.forEach(function (step, index) {
      if (placement !== step || flipOrder.length === index + 1) {
        return data;
      }

      placement = data.placement.split('-')[0];
      placementOpposite = getOppositePlacement(placement);

      var popperOffsets = data.offsets.popper;
      var refOffsets = data.offsets.reference;

      // using floor because the reference offsets may contain decimals we are not going to consider here
      var floor = Math.floor;
      var overlapsRef = placement === 'left' && floor(popperOffsets.right) > floor(refOffsets.left) || placement === 'right' && floor(popperOffsets.left) < floor(refOffsets.right) || placement === 'top' && floor(popperOffsets.bottom) > floor(refOffsets.top) || placement === 'bottom' && floor(popperOffsets.top) < floor(refOffsets.bottom);

      var overflowsLeft = floor(popperOffsets.left) < floor(boundaries.left);
      var overflowsRight = floor(popperOffsets.right) > floor(boundaries.right);
      var overflowsTop = floor(popperOffsets.top) < floor(boundaries.top);
      var overflowsBottom = floor(popperOffsets.bottom) > floor(boundaries.bottom);

      var overflowsBoundaries = placement === 'left' && overflowsLeft || placement === 'right' && overflowsRight || placement === 'top' && overflowsTop || placement === 'bottom' && overflowsBottom;

      // flip the variation if required
      var isVertical = ['top', 'bottom'].indexOf(placement) !== -1;
      var flippedVariation = !!options.flipVariations && (isVertical && variation === 'start' && overflowsLeft || isVertical && variation === 'end' && overflowsRight || !isVertical && variation === 'start' && overflowsTop || !isVertical && variation === 'end' && overflowsBottom);

      if (overlapsRef || overflowsBoundaries || flippedVariation) {
        // this boolean to detect any flip loop
        data.flipped = true;

        if (overlapsRef || overflowsBoundaries) {
          placement = flipOrder[index + 1];
        }

        if (flippedVariation) {
          variation = getOppositeVariation(variation);
        }

        data.placement = placement + (variation ? '-' + variation : '');

        // this object contains `position`, we want to preserve it along with
        // any additional property we may add in the future
        data.offsets.popper = _extends({}, data.offsets.popper, getPopperOffsets(data.instance.popper, data.offsets.reference, data.placement));

        data = runModifiers(data.instance.modifiers, data, 'flip');
      }
    });
    return data;
  }

  /**
   * @function
   * @memberof Modifiers
   * @argument {Object} data - The data object generated by update method
   * @argument {Object} options - Modifiers configuration and options
   * @returns {Object} The data object, properly modified
   */
  function keepTogether(data) {
    var _data$offsets = data.offsets,
        popper = _data$offsets.popper,
        reference = _data$offsets.reference;

    var placement = data.placement.split('-')[0];
    var floor = Math.floor;
    var isVertical = ['top', 'bottom'].indexOf(placement) !== -1;
    var side = isVertical ? 'right' : 'bottom';
    var opSide = isVertical ? 'left' : 'top';
    var measurement = isVertical ? 'width' : 'height';

    if (popper[side] < floor(reference[opSide])) {
      data.offsets.popper[opSide] = floor(reference[opSide]) - popper[measurement];
    }
    if (popper[opSide] > floor(reference[side])) {
      data.offsets.popper[opSide] = floor(reference[side]);
    }

    return data;
  }

  /**
   * Converts a string containing value + unit into a px value number
   * @function
   * @memberof {modifiers~offset}
   * @private
   * @argument {String} str - Value + unit string
   * @argument {String} measurement - `height` or `width`
   * @argument {Object} popperOffsets
   * @argument {Object} referenceOffsets
   * @returns {Number|String}
   * Value in pixels, or original string if no values were extracted
   */
  function toValue(str, measurement, popperOffsets, referenceOffsets) {
    // separate value from unit
    var split = str.match(/((?:\-|\+)?\d*\.?\d*)(.*)/);
    var value = +split[1];
    var unit = split[2];

    // If it's not a number it's an operator, I guess
    if (!value) {
      return str;
    }

    if (unit.indexOf('%') === 0) {
      var element = void 0;
      switch (unit) {
        case '%p':
          element = popperOffsets;
          break;
        case '%':
        case '%r':
        default:
          element = referenceOffsets;
      }

      var rect = getClientRect(element);
      return rect[measurement] / 100 * value;
    } else if (unit === 'vh' || unit === 'vw') {
      // if is a vh or vw, we calculate the size based on the viewport
      var size = void 0;
      if (unit === 'vh') {
        size = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
      } else {
        size = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
      }
      return size / 100 * value;
    } else {
      // if is an explicit pixel unit, we get rid of the unit and keep the value
      // if is an implicit unit, it's px, and we return just the value
      return value;
    }
  }

  /**
   * Parse an `offset` string to extrapolate `x` and `y` numeric offsets.
   * @function
   * @memberof {modifiers~offset}
   * @private
   * @argument {String} offset
   * @argument {Object} popperOffsets
   * @argument {Object} referenceOffsets
   * @argument {String} basePlacement
   * @returns {Array} a two cells array with x and y offsets in numbers
   */
  function parseOffset(offset, popperOffsets, referenceOffsets, basePlacement) {
    var offsets = [0, 0];

    // Use height if placement is left or right and index is 0 otherwise use width
    // in this way the first offset will use an axis and the second one
    // will use the other one
    var useHeight = ['right', 'left'].indexOf(basePlacement) !== -1;

    // Split the offset string to obtain a list of values and operands
    // The regex addresses values with the plus or minus sign in front (+10, -20, etc)
    var fragments = offset.split(/(\+|\-)/).map(function (frag) {
      return frag.trim();
    });

    // Detect if the offset string contains a pair of values or a single one
    // they could be separated by comma or space
    var divider = fragments.indexOf(find(fragments, function (frag) {
      return frag.search(/,|\s/) !== -1;
    }));

    if (fragments[divider] && fragments[divider].indexOf(',') === -1) {
      console.warn('Offsets separated by white space(s) are deprecated, use a comma (,) instead.');
    }

    // If divider is found, we divide the list of values and operands to divide
    // them by ofset X and Y.
    var splitRegex = /\s*,\s*|\s+/;
    var ops = divider !== -1 ? [fragments.slice(0, divider).concat([fragments[divider].split(splitRegex)[0]]), [fragments[divider].split(splitRegex)[1]].concat(fragments.slice(divider + 1))] : [fragments];

    // Convert the values with units to absolute pixels to allow our computations
    ops = ops.map(function (op, index) {
      // Most of the units rely on the orientation of the popper
      var measurement = (index === 1 ? !useHeight : useHeight) ? 'height' : 'width';
      var mergeWithPrevious = false;
      return op
      // This aggregates any `+` or `-` sign that aren't considered operators
      // e.g.: 10 + +5 => [10, +, +5]
      .reduce(function (a, b) {
        if (a[a.length - 1] === '' && ['+', '-'].indexOf(b) !== -1) {
          a[a.length - 1] = b;
          mergeWithPrevious = true;
          return a;
        } else if (mergeWithPrevious) {
          a[a.length - 1] += b;
          mergeWithPrevious = false;
          return a;
        } else {
          return a.concat(b);
        }
      }, [])
      // Here we convert the string values into number values (in px)
      .map(function (str) {
        return toValue(str, measurement, popperOffsets, referenceOffsets);
      });
    });

    // Loop trough the offsets arrays and execute the operations
    ops.forEach(function (op, index) {
      op.forEach(function (frag, index2) {
        if (isNumeric(frag)) {
          offsets[index] += frag * (op[index2 - 1] === '-' ? -1 : 1);
        }
      });
    });
    return offsets;
  }

  /**
   * @function
   * @memberof Modifiers
   * @argument {Object} data - The data object generated by update method
   * @argument {Object} options - Modifiers configuration and options
   * @argument {Number|String} options.offset=0
   * The offset value as described in the modifier description
   * @returns {Object} The data object, properly modified
   */
  function offset(data, _ref) {
    var offset = _ref.offset;
    var placement = data.placement,
        _data$offsets = data.offsets,
        popper = _data$offsets.popper,
        reference = _data$offsets.reference;

    var basePlacement = placement.split('-')[0];

    var offsets = void 0;
    if (isNumeric(+offset)) {
      offsets = [+offset, 0];
    } else {
      offsets = parseOffset(offset, popper, reference, basePlacement);
    }

    if (basePlacement === 'left') {
      popper.top += offsets[0];
      popper.left -= offsets[1];
    } else if (basePlacement === 'right') {
      popper.top += offsets[0];
      popper.left += offsets[1];
    } else if (basePlacement === 'top') {
      popper.left += offsets[0];
      popper.top -= offsets[1];
    } else if (basePlacement === 'bottom') {
      popper.left += offsets[0];
      popper.top += offsets[1];
    }

    data.popper = popper;
    return data;
  }

  /**
   * @function
   * @memberof Modifiers
   * @argument {Object} data - The data object generated by `update` method
   * @argument {Object} options - Modifiers configuration and options
   * @returns {Object} The data object, properly modified
   */
  function preventOverflow(data, options) {
    var boundariesElement = options.boundariesElement || getOffsetParent(data.instance.popper);

    // If offsetParent is the reference element, we really want to
    // go one step up and use the next offsetParent as reference to
    // avoid to make this modifier completely useless and look like broken
    if (data.instance.reference === boundariesElement) {
      boundariesElement = getOffsetParent(boundariesElement);
    }

    // NOTE: DOM access here
    // resets the popper's position so that the document size can be calculated excluding
    // the size of the popper element itself
    var transformProp = getSupportedPropertyName('transform');
    var popperStyles = data.instance.popper.style; // assignment to help minification
    var top = popperStyles.top,
        left = popperStyles.left,
        transform = popperStyles[transformProp];

    popperStyles.top = '';
    popperStyles.left = '';
    popperStyles[transformProp] = '';

    var boundaries = getBoundaries(data.instance.popper, data.instance.reference, options.padding, boundariesElement, data.positionFixed);

    // NOTE: DOM access here
    // restores the original style properties after the offsets have been computed
    popperStyles.top = top;
    popperStyles.left = left;
    popperStyles[transformProp] = transform;

    options.boundaries = boundaries;

    var order = options.priority;
    var popper = data.offsets.popper;

    var check = {
      primary: function primary(placement) {
        var value = popper[placement];
        if (popper[placement] < boundaries[placement] && !options.escapeWithReference) {
          value = Math.max(popper[placement], boundaries[placement]);
        }
        return defineProperty({}, placement, value);
      },
      secondary: function secondary(placement) {
        var mainSide = placement === 'right' ? 'left' : 'top';
        var value = popper[mainSide];
        if (popper[placement] > boundaries[placement] && !options.escapeWithReference) {
          value = Math.min(popper[mainSide], boundaries[placement] - (placement === 'right' ? popper.width : popper.height));
        }
        return defineProperty({}, mainSide, value);
      }
    };

    order.forEach(function (placement) {
      var side = ['left', 'top'].indexOf(placement) !== -1 ? 'primary' : 'secondary';
      popper = _extends({}, popper, check[side](placement));
    });

    data.offsets.popper = popper;

    return data;
  }

  /**
   * @function
   * @memberof Modifiers
   * @argument {Object} data - The data object generated by `update` method
   * @argument {Object} options - Modifiers configuration and options
   * @returns {Object} The data object, properly modified
   */
  function shift(data) {
    var placement = data.placement;
    var basePlacement = placement.split('-')[0];
    var shiftvariation = placement.split('-')[1];

    // if shift shiftvariation is specified, run the modifier
    if (shiftvariation) {
      var _data$offsets = data.offsets,
          reference = _data$offsets.reference,
          popper = _data$offsets.popper;

      var isVertical = ['bottom', 'top'].indexOf(basePlacement) !== -1;
      var side = isVertical ? 'left' : 'top';
      var measurement = isVertical ? 'width' : 'height';

      var shiftOffsets = {
        start: defineProperty({}, side, reference[side]),
        end: defineProperty({}, side, reference[side] + reference[measurement] - popper[measurement])
      };

      data.offsets.popper = _extends({}, popper, shiftOffsets[shiftvariation]);
    }

    return data;
  }

  /**
   * @function
   * @memberof Modifiers
   * @argument {Object} data - The data object generated by update method
   * @argument {Object} options - Modifiers configuration and options
   * @returns {Object} The data object, properly modified
   */
  function hide(data) {
    if (!isModifierRequired(data.instance.modifiers, 'hide', 'preventOverflow')) {
      return data;
    }

    var refRect = data.offsets.reference;
    var bound = find(data.instance.modifiers, function (modifier) {
      return modifier.name === 'preventOverflow';
    }).boundaries;

    if (refRect.bottom < bound.top || refRect.left > bound.right || refRect.top > bound.bottom || refRect.right < bound.left) {
      // Avoid unnecessary DOM access if visibility hasn't changed
      if (data.hide === true) {
        return data;
      }

      data.hide = true;
      data.attributes['x-out-of-boundaries'] = '';
    } else {
      // Avoid unnecessary DOM access if visibility hasn't changed
      if (data.hide === false) {
        return data;
      }

      data.hide = false;
      data.attributes['x-out-of-boundaries'] = false;
    }

    return data;
  }

  /**
   * @function
   * @memberof Modifiers
   * @argument {Object} data - The data object generated by `update` method
   * @argument {Object} options - Modifiers configuration and options
   * @returns {Object} The data object, properly modified
   */
  function inner(data) {
    var placement = data.placement;
    var basePlacement = placement.split('-')[0];
    var _data$offsets = data.offsets,
        popper = _data$offsets.popper,
        reference = _data$offsets.reference;

    var isHoriz = ['left', 'right'].indexOf(basePlacement) !== -1;

    var subtractLength = ['top', 'left'].indexOf(basePlacement) === -1;

    popper[isHoriz ? 'left' : 'top'] = reference[basePlacement] - (subtractLength ? popper[isHoriz ? 'width' : 'height'] : 0);

    data.placement = getOppositePlacement(placement);
    data.offsets.popper = getClientRect(popper);

    return data;
  }

  /**
   * Modifier function, each modifier can have a function of this type assigned
   * to its `fn` property.<br />
   * These functions will be called on each update, this means that you must
   * make sure they are performant enough to avoid performance bottlenecks.
   *
   * @function ModifierFn
   * @argument {dataObject} data - The data object generated by `update` method
   * @argument {Object} options - Modifiers configuration and options
   * @returns {dataObject} The data object, properly modified
   */

  /**
   * Modifiers are plugins used to alter the behavior of your poppers.<br />
   * Popper.js uses a set of 9 modifiers to provide all the basic functionalities
   * needed by the library.
   *
   * Usually you don't want to override the `order`, `fn` and `onLoad` props.
   * All the other properties are configurations that could be tweaked.
   * @namespace modifiers
   */
  var modifiers = {
    /**
     * Modifier used to shift the popper on the start or end of its reference
     * element.<br />
     * It will read the variation of the `placement` property.<br />
     * It can be one either `-end` or `-start`.
     * @memberof modifiers
     * @inner
     */
    shift: {
      /** @prop {number} order=100 - Index used to define the order of execution */
      order: 100,
      /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
      enabled: true,
      /** @prop {ModifierFn} */
      fn: shift
    },

    /**
     * The `offset` modifier can shift your popper on both its axis.
     *
     * It accepts the following units:
     * - `px` or unitless, interpreted as pixels
     * - `%` or `%r`, percentage relative to the length of the reference element
     * - `%p`, percentage relative to the length of the popper element
     * - `vw`, CSS viewport width unit
     * - `vh`, CSS viewport height unit
     *
     * For length is intended the main axis relative to the placement of the popper.<br />
     * This means that if the placement is `top` or `bottom`, the length will be the
     * `width`. In case of `left` or `right`, it will be the height.
     *
     * You can provide a single value (as `Number` or `String`), or a pair of values
     * as `String` divided by a comma or one (or more) white spaces.<br />
     * The latter is a deprecated method because it leads to confusion and will be
     * removed in v2.<br />
     * Additionally, it accepts additions and subtractions between different units.
     * Note that multiplications and divisions aren't supported.
     *
     * Valid examples are:
     * ```
     * 10
     * '10%'
     * '10, 10'
     * '10%, 10'
     * '10 + 10%'
     * '10 - 5vh + 3%'
     * '-10px + 5vh, 5px - 6%'
     * ```
     * > **NB**: If you desire to apply offsets to your poppers in a way that may make them overlap
     * > with their reference element, unfortunately, you will have to disable the `flip` modifier.
     * > More on this [reading this issue](https://github.com/FezVrasta/popper.js/issues/373)
     *
     * @memberof modifiers
     * @inner
     */
    offset: {
      /** @prop {number} order=200 - Index used to define the order of execution */
      order: 200,
      /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
      enabled: true,
      /** @prop {ModifierFn} */
      fn: offset,
      /** @prop {Number|String} offset=0
       * The offset value as described in the modifier description
       */
      offset: 0
    },

    /**
     * Modifier used to prevent the popper from being positioned outside the boundary.
     *
     * An scenario exists where the reference itself is not within the boundaries.<br />
     * We can say it has "escaped the boundaries" — or just "escaped".<br />
     * In this case we need to decide whether the popper should either:
     *
     * - detach from the reference and remain "trapped" in the boundaries, or
     * - if it should ignore the boundary and "escape with its reference"
     *
     * When `escapeWithReference` is set to`true` and reference is completely
     * outside its boundaries, the popper will overflow (or completely leave)
     * the boundaries in order to remain attached to the edge of the reference.
     *
     * @memberof modifiers
     * @inner
     */
    preventOverflow: {
      /** @prop {number} order=300 - Index used to define the order of execution */
      order: 300,
      /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
      enabled: true,
      /** @prop {ModifierFn} */
      fn: preventOverflow,
      /**
       * @prop {Array} [priority=['left','right','top','bottom']]
       * Popper will try to prevent overflow following these priorities by default,
       * then, it could overflow on the left and on top of the `boundariesElement`
       */
      priority: ['left', 'right', 'top', 'bottom'],
      /**
       * @prop {number} padding=5
       * Amount of pixel used to define a minimum distance between the boundaries
       * and the popper this makes sure the popper has always a little padding
       * between the edges of its container
       */
      padding: 5,
      /**
       * @prop {String|HTMLElement} boundariesElement='scrollParent'
       * Boundaries used by the modifier, can be `scrollParent`, `window`,
       * `viewport` or any DOM element.
       */
      boundariesElement: 'scrollParent'
    },

    /**
     * Modifier used to make sure the reference and its popper stay near eachothers
     * without leaving any gap between the two. Expecially useful when the arrow is
     * enabled and you want to assure it to point to its reference element.
     * It cares only about the first axis, you can still have poppers with margin
     * between the popper and its reference element.
     * @memberof modifiers
     * @inner
     */
    keepTogether: {
      /** @prop {number} order=400 - Index used to define the order of execution */
      order: 400,
      /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
      enabled: true,
      /** @prop {ModifierFn} */
      fn: keepTogether
    },

    /**
     * This modifier is used to move the `arrowElement` of the popper to make
     * sure it is positioned between the reference element and its popper element.
     * It will read the outer size of the `arrowElement` node to detect how many
     * pixels of conjuction are needed.
     *
     * It has no effect if no `arrowElement` is provided.
     * @memberof modifiers
     * @inner
     */
    arrow: {
      /** @prop {number} order=500 - Index used to define the order of execution */
      order: 500,
      /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
      enabled: true,
      /** @prop {ModifierFn} */
      fn: arrow,
      /** @prop {String|HTMLElement} element='[x-arrow]' - Selector or node used as arrow */
      element: '[x-arrow]'
    },

    /**
     * Modifier used to flip the popper's placement when it starts to overlap its
     * reference element.
     *
     * Requires the `preventOverflow` modifier before it in order to work.
     *
     * **NOTE:** this modifier will interrupt the current update cycle and will
     * restart it if it detects the need to flip the placement.
     * @memberof modifiers
     * @inner
     */
    flip: {
      /** @prop {number} order=600 - Index used to define the order of execution */
      order: 600,
      /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
      enabled: true,
      /** @prop {ModifierFn} */
      fn: flip,
      /**
       * @prop {String|Array} behavior='flip'
       * The behavior used to change the popper's placement. It can be one of
       * `flip`, `clockwise`, `counterclockwise` or an array with a list of valid
       * placements (with optional variations).
       */
      behavior: 'flip',
      /**
       * @prop {number} padding=5
       * The popper will flip if it hits the edges of the `boundariesElement`
       */
      padding: 5,
      /**
       * @prop {String|HTMLElement} boundariesElement='viewport'
       * The element which will define the boundaries of the popper position,
       * the popper will never be placed outside of the defined boundaries
       * (except if keepTogether is enabled)
       */
      boundariesElement: 'viewport'
    },

    /**
     * Modifier used to make the popper flow toward the inner of the reference element.
     * By default, when this modifier is disabled, the popper will be placed outside
     * the reference element.
     * @memberof modifiers
     * @inner
     */
    inner: {
      /** @prop {number} order=700 - Index used to define the order of execution */
      order: 700,
      /** @prop {Boolean} enabled=false - Whether the modifier is enabled or not */
      enabled: false,
      /** @prop {ModifierFn} */
      fn: inner
    },

    /**
     * Modifier used to hide the popper when its reference element is outside of the
     * popper boundaries. It will set a `x-out-of-boundaries` attribute which can
     * be used to hide with a CSS selector the popper when its reference is
     * out of boundaries.
     *
     * Requires the `preventOverflow` modifier before it in order to work.
     * @memberof modifiers
     * @inner
     */
    hide: {
      /** @prop {number} order=800 - Index used to define the order of execution */
      order: 800,
      /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
      enabled: true,
      /** @prop {ModifierFn} */
      fn: hide
    },

    /**
     * Computes the style that will be applied to the popper element to gets
     * properly positioned.
     *
     * Note that this modifier will not touch the DOM, it just prepares the styles
     * so that `applyStyle` modifier can apply it. This separation is useful
     * in case you need to replace `applyStyle` with a custom implementation.
     *
     * This modifier has `850` as `order` value to maintain backward compatibility
     * with previous versions of Popper.js. Expect the modifiers ordering method
     * to change in future major versions of the library.
     *
     * @memberof modifiers
     * @inner
     */
    computeStyle: {
      /** @prop {number} order=850 - Index used to define the order of execution */
      order: 850,
      /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
      enabled: true,
      /** @prop {ModifierFn} */
      fn: computeStyle,
      /**
       * @prop {Boolean} gpuAcceleration=true
       * If true, it uses the CSS 3d transformation to position the popper.
       * Otherwise, it will use the `top` and `left` properties.
       */
      gpuAcceleration: true,
      /**
       * @prop {string} [x='bottom']
       * Where to anchor the X axis (`bottom` or `top`). AKA X offset origin.
       * Change this if your popper should grow in a direction different from `bottom`
       */
      x: 'bottom',
      /**
       * @prop {string} [x='left']
       * Where to anchor the Y axis (`left` or `right`). AKA Y offset origin.
       * Change this if your popper should grow in a direction different from `right`
       */
      y: 'right'
    },

    /**
     * Applies the computed styles to the popper element.
     *
     * All the DOM manipulations are limited to this modifier. This is useful in case
     * you want to integrate Popper.js inside a framework or view library and you
     * want to delegate all the DOM manipulations to it.
     *
     * Note that if you disable this modifier, you must make sure the popper element
     * has its position set to `absolute` before Popper.js can do its work!
     *
     * Just disable this modifier and define you own to achieve the desired effect.
     *
     * @memberof modifiers
     * @inner
     */
    applyStyle: {
      /** @prop {number} order=900 - Index used to define the order of execution */
      order: 900,
      /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
      enabled: true,
      /** @prop {ModifierFn} */
      fn: applyStyle,
      /** @prop {Function} */
      onLoad: applyStyleOnLoad,
      /**
       * @deprecated since version 1.10.0, the property moved to `computeStyle` modifier
       * @prop {Boolean} gpuAcceleration=true
       * If true, it uses the CSS 3d transformation to position the popper.
       * Otherwise, it will use the `top` and `left` properties.
       */
      gpuAcceleration: undefined
    }
  };

  /**
   * The `dataObject` is an object containing all the informations used by Popper.js
   * this object get passed to modifiers and to the `onCreate` and `onUpdate` callbacks.
   * @name dataObject
   * @property {Object} data.instance The Popper.js instance
   * @property {String} data.placement Placement applied to popper
   * @property {String} data.originalPlacement Placement originally defined on init
   * @property {Boolean} data.flipped True if popper has been flipped by flip modifier
   * @property {Boolean} data.hide True if the reference element is out of boundaries, useful to know when to hide the popper.
   * @property {HTMLElement} data.arrowElement Node used as arrow by arrow modifier
   * @property {Object} data.styles Any CSS property defined here will be applied to the popper, it expects the JavaScript nomenclature (eg. `marginBottom`)
   * @property {Object} data.arrowStyles Any CSS property defined here will be applied to the popper arrow, it expects the JavaScript nomenclature (eg. `marginBottom`)
   * @property {Object} data.boundaries Offsets of the popper boundaries
   * @property {Object} data.offsets The measurements of popper, reference and arrow elements.
   * @property {Object} data.offsets.popper `top`, `left`, `width`, `height` values
   * @property {Object} data.offsets.reference `top`, `left`, `width`, `height` values
   * @property {Object} data.offsets.arrow] `top` and `left` offsets, only one of them will be different from 0
   */

  /**
   * Default options provided to Popper.js constructor.<br />
   * These can be overriden using the `options` argument of Popper.js.<br />
   * To override an option, simply pass as 3rd argument an object with the same
   * structure of this object, example:
   * ```
   * new Popper(ref, pop, {
   *   modifiers: {
   *     preventOverflow: { enabled: false }
   *   }
   * })
   * ```
   * @type {Object}
   * @static
   * @memberof Popper
   */
  var Defaults = {
    /**
     * Popper's placement
     * @prop {Popper.placements} placement='bottom'
     */
    placement: 'bottom',

    /**
     * Set this to true if you want popper to position it self in 'fixed' mode
     * @prop {Boolean} positionFixed=false
     */
    positionFixed: false,

    /**
     * Whether events (resize, scroll) are initially enabled
     * @prop {Boolean} eventsEnabled=true
     */
    eventsEnabled: true,

    /**
     * Set to true if you want to automatically remove the popper when
     * you call the `destroy` method.
     * @prop {Boolean} removeOnDestroy=false
     */
    removeOnDestroy: false,

    /**
     * Callback called when the popper is created.<br />
     * By default, is set to no-op.<br />
     * Access Popper.js instance with `data.instance`.
     * @prop {onCreate}
     */
    onCreate: function onCreate() {},

    /**
     * Callback called when the popper is updated, this callback is not called
     * on the initialization/creation of the popper, but only on subsequent
     * updates.<br />
     * By default, is set to no-op.<br />
     * Access Popper.js instance with `data.instance`.
     * @prop {onUpdate}
     */
    onUpdate: function onUpdate() {},

    /**
     * List of modifiers used to modify the offsets before they are applied to the popper.
     * They provide most of the functionalities of Popper.js
     * @prop {modifiers}
     */
    modifiers: modifiers
  };

  /**
   * @callback onCreate
   * @param {dataObject} data
   */

  /**
   * @callback onUpdate
   * @param {dataObject} data
   */

  // Utils
  // Methods
  var Popper = function () {
    /**
     * Create a new Popper.js instance
     * @class Popper
     * @param {HTMLElement|referenceObject} reference - The reference element used to position the popper
     * @param {HTMLElement} popper - The HTML element used as popper.
     * @param {Object} options - Your custom options to override the ones defined in [Defaults](#defaults)
     * @return {Object} instance - The generated Popper.js instance
     */
    function Popper(reference, popper) {
      var _this = this;

      var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
      classCallCheck(this, Popper);

      this.scheduleUpdate = function () {
        return requestAnimationFrame(_this.update);
      };

      // make update() debounced, so that it only runs at most once-per-tick
      this.update = debounce(this.update.bind(this));

      // with {} we create a new object with the options inside it
      this.options = _extends({}, Popper.Defaults, options);

      // init state
      this.state = {
        isDestroyed: false,
        isCreated: false,
        scrollParents: []
      };

      // get reference and popper elements (allow jQuery wrappers)
      this.reference = reference && reference.jquery ? reference[0] : reference;
      this.popper = popper && popper.jquery ? popper[0] : popper;

      // Deep merge modifiers options
      this.options.modifiers = {};
      Object.keys(_extends({}, Popper.Defaults.modifiers, options.modifiers)).forEach(function (name) {
        _this.options.modifiers[name] = _extends({}, Popper.Defaults.modifiers[name] || {}, options.modifiers ? options.modifiers[name] : {});
      });

      // Refactoring modifiers' list (Object => Array)
      this.modifiers = Object.keys(this.options.modifiers).map(function (name) {
        return _extends({
          name: name
        }, _this.options.modifiers[name]);
      })
      // sort the modifiers by order
      .sort(function (a, b) {
        return a.order - b.order;
      });

      // modifiers have the ability to execute arbitrary code when Popper.js get inited
      // such code is executed in the same order of its modifier
      // they could add new properties to their options configuration
      // BE AWARE: don't add options to `options.modifiers.name` but to `modifierOptions`!
      this.modifiers.forEach(function (modifierOptions) {
        if (modifierOptions.enabled && isFunction(modifierOptions.onLoad)) {
          modifierOptions.onLoad(_this.reference, _this.popper, _this.options, modifierOptions, _this.state);
        }
      });

      // fire the first update to position the popper in the right place
      this.update();

      var eventsEnabled = this.options.eventsEnabled;
      if (eventsEnabled) {
        // setup event listeners, they will take care of update the position in specific situations
        this.enableEventListeners();
      }

      this.state.eventsEnabled = eventsEnabled;
    }

    // We can't use class properties because they don't get listed in the
    // class prototype and break stuff like Sinon stubs


    createClass(Popper, [{
      key: 'update',
      value: function update$$1() {
        return update.call(this);
      }
    }, {
      key: 'destroy',
      value: function destroy$$1() {
        return destroy.call(this);
      }
    }, {
      key: 'enableEventListeners',
      value: function enableEventListeners$$1() {
        return enableEventListeners.call(this);
      }
    }, {
      key: 'disableEventListeners',
      value: function disableEventListeners$$1() {
        return disableEventListeners.call(this);
      }

      /**
       * Schedule an update, it will run on the next UI update available
       * @method scheduleUpdate
       * @memberof Popper
       */


      /**
       * Collection of utilities useful when writing custom modifiers.
       * Starting from version 1.7, this method is available only if you
       * include `popper-utils.js` before `popper.js`.
       *
       * **DEPRECATION**: This way to access PopperUtils is deprecated
       * and will be removed in v2! Use the PopperUtils module directly instead.
       * Due to the high instability of the methods contained in Utils, we can't
       * guarantee them to follow semver. Use them at your own risk!
       * @static
       * @private
       * @type {Object}
       * @deprecated since version 1.8
       * @member Utils
       * @memberof Popper
       */

    }]);
    return Popper;
  }();

  /**
   * The `referenceObject` is an object that provides an interface compatible with Popper.js
   * and lets you use it as replacement of a real DOM node.<br />
   * You can use this method to position a popper relatively to a set of coordinates
   * in case you don't have a DOM node to use as reference.
   *
   * ```
   * new Popper(referenceObject, popperNode);
   * ```
   *
   * NB: This feature isn't supported in Internet Explorer 10
   * @name referenceObject
   * @property {Function} data.getBoundingClientRect
   * A function that returns a set of coordinates compatible with the native `getBoundingClientRect` method.
   * @property {number} data.clientWidth
   * An ES6 getter that will return the width of the virtual reference element.
   * @property {number} data.clientHeight
   * An ES6 getter that will return the height of the virtual reference element.
   */


  Popper.Utils = (typeof window !== 'undefined' ? window : global).PopperUtils;
  Popper.placements = placements;
  Popper.Defaults = Defaults;

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.1.3): dropdown.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  var Dropdown = function ($$$1) {
    /**
     * ------------------------------------------------------------------------
     * Constants
     * ------------------------------------------------------------------------
     */
    var NAME = 'dropdown';
    var VERSION = '4.1.3';
    var DATA_KEY = 'bs.dropdown';
    var EVENT_KEY = "." + DATA_KEY;
    var DATA_API_KEY = '.data-api';
    var JQUERY_NO_CONFLICT = $$$1.fn[NAME];
    var ESCAPE_KEYCODE = 27; // KeyboardEvent.which value for Escape (Esc) key

    var SPACE_KEYCODE = 32; // KeyboardEvent.which value for space key

    var TAB_KEYCODE = 9; // KeyboardEvent.which value for tab key

    var ARROW_UP_KEYCODE = 38; // KeyboardEvent.which value for up arrow key

    var ARROW_DOWN_KEYCODE = 40; // KeyboardEvent.which value for down arrow key

    var RIGHT_MOUSE_BUTTON_WHICH = 3; // MouseEvent.which value for the right button (assuming a right-handed mouse)

    var REGEXP_KEYDOWN = new RegExp(ARROW_UP_KEYCODE + "|" + ARROW_DOWN_KEYCODE + "|" + ESCAPE_KEYCODE);
    var Event = {
      HIDE: "hide" + EVENT_KEY,
      HIDDEN: "hidden" + EVENT_KEY,
      SHOW: "show" + EVENT_KEY,
      SHOWN: "shown" + EVENT_KEY,
      CLICK: "click" + EVENT_KEY,
      CLICK_DATA_API: "click" + EVENT_KEY + DATA_API_KEY,
      KEYDOWN_DATA_API: "keydown" + EVENT_KEY + DATA_API_KEY,
      KEYUP_DATA_API: "keyup" + EVENT_KEY + DATA_API_KEY
    };
    var ClassName = {
      DISABLED: 'disabled',
      SHOW: 'show',
      DROPUP: 'dropup',
      DROPRIGHT: 'dropright',
      DROPLEFT: 'dropleft',
      MENURIGHT: 'dropdown-menu-right',
      MENULEFT: 'dropdown-menu-left',
      POSITION_STATIC: 'position-static'
    };
    var Selector = {
      DATA_TOGGLE: '[data-toggle="dropdown"]',
      FORM_CHILD: '.dropdown form',
      MENU: '.dropdown-menu',
      NAVBAR_NAV: '.navbar-nav',
      VISIBLE_ITEMS: '.dropdown-menu .dropdown-item:not(.disabled):not(:disabled)'
    };
    var AttachmentMap = {
      TOP: 'top-start',
      TOPEND: 'top-end',
      BOTTOM: 'bottom-start',
      BOTTOMEND: 'bottom-end',
      RIGHT: 'right-start',
      RIGHTEND: 'right-end',
      LEFT: 'left-start',
      LEFTEND: 'left-end'
    };
    var Default = {
      offset: 0,
      flip: true,
      boundary: 'scrollParent',
      reference: 'toggle',
      display: 'dynamic'
    };
    var DefaultType = {
      offset: '(number|string|function)',
      flip: 'boolean',
      boundary: '(string|element)',
      reference: '(string|element)',
      display: 'string'
      /**
       * ------------------------------------------------------------------------
       * Class Definition
       * ------------------------------------------------------------------------
       */

    };

    var Dropdown =
    /*#__PURE__*/
    function () {
      function Dropdown(element, config) {
        this._element = element;
        this._popper = null;
        this._config = this._getConfig(config);
        this._menu = this._getMenuElement();
        this._inNavbar = this._detectNavbar();

        this._addEventListeners();
      } // Getters


      var _proto = Dropdown.prototype;

      // Public
      _proto.toggle = function toggle() {
        if (this._element.disabled || $$$1(this._element).hasClass(ClassName.DISABLED)) {
          return;
        }

        var parent = Dropdown._getParentFromElement(this._element);

        var isActive = $$$1(this._menu).hasClass(ClassName.SHOW);

        Dropdown._clearMenus();

        if (isActive) {
          return;
        }

        var relatedTarget = {
          relatedTarget: this._element
        };
        var showEvent = $$$1.Event(Event.SHOW, relatedTarget);
        $$$1(parent).trigger(showEvent);

        if (showEvent.isDefaultPrevented()) {
          return;
        } // Disable totally Popper.js for Dropdown in Navbar


        if (!this._inNavbar) {
          /**
           * Check for Popper dependency
           * Popper - https://popper.js.org
           */
          if (typeof Popper === 'undefined') {
            throw new TypeError('Bootstrap dropdown require Popper.js (https://popper.js.org)');
          }

          var referenceElement = this._element;

          if (this._config.reference === 'parent') {
            referenceElement = parent;
          } else if (Util.isElement(this._config.reference)) {
            referenceElement = this._config.reference; // Check if it's jQuery element

            if (typeof this._config.reference.jquery !== 'undefined') {
              referenceElement = this._config.reference[0];
            }
          } // If boundary is not `scrollParent`, then set position to `static`
          // to allow the menu to "escape" the scroll parent's boundaries
          // https://github.com/twbs/bootstrap/issues/24251


          if (this._config.boundary !== 'scrollParent') {
            $$$1(parent).addClass(ClassName.POSITION_STATIC);
          }

          this._popper = new Popper(referenceElement, this._menu, this._getPopperConfig());
        } // If this is a touch-enabled device we add extra
        // empty mouseover listeners to the body's immediate children;
        // only needed because of broken event delegation on iOS
        // https://www.quirksmode.org/blog/archives/2014/02/mouse_event_bub.html


        if ('ontouchstart' in document.documentElement && $$$1(parent).closest(Selector.NAVBAR_NAV).length === 0) {
          $$$1(document.body).children().on('mouseover', null, $$$1.noop);
        }

        this._element.focus();

        this._element.setAttribute('aria-expanded', true);

        $$$1(this._menu).toggleClass(ClassName.SHOW);
        $$$1(parent).toggleClass(ClassName.SHOW).trigger($$$1.Event(Event.SHOWN, relatedTarget));
      };

      _proto.dispose = function dispose() {
        $$$1.removeData(this._element, DATA_KEY);
        $$$1(this._element).off(EVENT_KEY);
        this._element = null;
        this._menu = null;

        if (this._popper !== null) {
          this._popper.destroy();

          this._popper = null;
        }
      };

      _proto.update = function update() {
        this._inNavbar = this._detectNavbar();

        if (this._popper !== null) {
          this._popper.scheduleUpdate();
        }
      }; // Private


      _proto._addEventListeners = function _addEventListeners() {
        var _this = this;

        $$$1(this._element).on(Event.CLICK, function (event) {
          event.preventDefault();
          event.stopPropagation();

          _this.toggle();
        });
      };

      _proto._getConfig = function _getConfig(config) {
        config = _objectSpread({}, this.constructor.Default, $$$1(this._element).data(), config);
        Util.typeCheckConfig(NAME, config, this.constructor.DefaultType);
        return config;
      };

      _proto._getMenuElement = function _getMenuElement() {
        if (!this._menu) {
          var parent = Dropdown._getParentFromElement(this._element);

          if (parent) {
            this._menu = parent.querySelector(Selector.MENU);
          }
        }

        return this._menu;
      };

      _proto._getPlacement = function _getPlacement() {
        var $parentDropdown = $$$1(this._element.parentNode);
        var placement = AttachmentMap.BOTTOM; // Handle dropup

        if ($parentDropdown.hasClass(ClassName.DROPUP)) {
          placement = AttachmentMap.TOP;

          if ($$$1(this._menu).hasClass(ClassName.MENURIGHT)) {
            placement = AttachmentMap.TOPEND;
          }
        } else if ($parentDropdown.hasClass(ClassName.DROPRIGHT)) {
          placement = AttachmentMap.RIGHT;
        } else if ($parentDropdown.hasClass(ClassName.DROPLEFT)) {
          placement = AttachmentMap.LEFT;
        } else if ($$$1(this._menu).hasClass(ClassName.MENURIGHT)) {
          placement = AttachmentMap.BOTTOMEND;
        }

        return placement;
      };

      _proto._detectNavbar = function _detectNavbar() {
        return $$$1(this._element).closest('.navbar').length > 0;
      };

      _proto._getPopperConfig = function _getPopperConfig() {
        var _this2 = this;

        var offsetConf = {};

        if (typeof this._config.offset === 'function') {
          offsetConf.fn = function (data) {
            data.offsets = _objectSpread({}, data.offsets, _this2._config.offset(data.offsets) || {});
            return data;
          };
        } else {
          offsetConf.offset = this._config.offset;
        }

        var popperConfig = {
          placement: this._getPlacement(),
          modifiers: {
            offset: offsetConf,
            flip: {
              enabled: this._config.flip
            },
            preventOverflow: {
              boundariesElement: this._config.boundary
            }
          } // Disable Popper.js if we have a static display

        };

        if (this._config.display === 'static') {
          popperConfig.modifiers.applyStyle = {
            enabled: false
          };
        }

        return popperConfig;
      }; // Static


      Dropdown._jQueryInterface = function _jQueryInterface(config) {
        return this.each(function () {
          var data = $$$1(this).data(DATA_KEY);

          var _config = typeof config === 'object' ? config : null;

          if (!data) {
            data = new Dropdown(this, _config);
            $$$1(this).data(DATA_KEY, data);
          }

          if (typeof config === 'string') {
            if (typeof data[config] === 'undefined') {
              throw new TypeError("No method named \"" + config + "\"");
            }

            data[config]();
          }
        });
      };

      Dropdown._clearMenus = function _clearMenus(event) {
        if (event && (event.which === RIGHT_MOUSE_BUTTON_WHICH || event.type === 'keyup' && event.which !== TAB_KEYCODE)) {
          return;
        }

        var toggles = [].slice.call(document.querySelectorAll(Selector.DATA_TOGGLE));

        for (var i = 0, len = toggles.length; i < len; i++) {
          var parent = Dropdown._getParentFromElement(toggles[i]);

          var context = $$$1(toggles[i]).data(DATA_KEY);
          var relatedTarget = {
            relatedTarget: toggles[i]
          };

          if (event && event.type === 'click') {
            relatedTarget.clickEvent = event;
          }

          if (!context) {
            continue;
          }

          var dropdownMenu = context._menu;

          if (!$$$1(parent).hasClass(ClassName.SHOW)) {
            continue;
          }

          if (event && (event.type === 'click' && /input|textarea/i.test(event.target.tagName) || event.type === 'keyup' && event.which === TAB_KEYCODE) && $$$1.contains(parent, event.target)) {
            continue;
          }

          var hideEvent = $$$1.Event(Event.HIDE, relatedTarget);
          $$$1(parent).trigger(hideEvent);

          if (hideEvent.isDefaultPrevented()) {
            continue;
          } // If this is a touch-enabled device we remove the extra
          // empty mouseover listeners we added for iOS support


          if ('ontouchstart' in document.documentElement) {
            $$$1(document.body).children().off('mouseover', null, $$$1.noop);
          }

          toggles[i].setAttribute('aria-expanded', 'false');
          $$$1(dropdownMenu).removeClass(ClassName.SHOW);
          $$$1(parent).removeClass(ClassName.SHOW).trigger($$$1.Event(Event.HIDDEN, relatedTarget));
        }
      };

      Dropdown._getParentFromElement = function _getParentFromElement(element) {
        var parent;
        var selector = Util.getSelectorFromElement(element);

        if (selector) {
          parent = document.querySelector(selector);
        }

        return parent || element.parentNode;
      }; // eslint-disable-next-line complexity


      Dropdown._dataApiKeydownHandler = function _dataApiKeydownHandler(event) {
        // If not input/textarea:
        //  - And not a key in REGEXP_KEYDOWN => not a dropdown command
        // If input/textarea:
        //  - If space key => not a dropdown command
        //  - If key is other than escape
        //    - If key is not up or down => not a dropdown command
        //    - If trigger inside the menu => not a dropdown command
        if (/input|textarea/i.test(event.target.tagName) ? event.which === SPACE_KEYCODE || event.which !== ESCAPE_KEYCODE && (event.which !== ARROW_DOWN_KEYCODE && event.which !== ARROW_UP_KEYCODE || $$$1(event.target).closest(Selector.MENU).length) : !REGEXP_KEYDOWN.test(event.which)) {
          return;
        }

        event.preventDefault();
        event.stopPropagation();

        if (this.disabled || $$$1(this).hasClass(ClassName.DISABLED)) {
          return;
        }

        var parent = Dropdown._getParentFromElement(this);

        var isActive = $$$1(parent).hasClass(ClassName.SHOW);

        if (!isActive && (event.which !== ESCAPE_KEYCODE || event.which !== SPACE_KEYCODE) || isActive && (event.which === ESCAPE_KEYCODE || event.which === SPACE_KEYCODE)) {
          if (event.which === ESCAPE_KEYCODE) {
            var toggle = parent.querySelector(Selector.DATA_TOGGLE);
            $$$1(toggle).trigger('focus');
          }

          $$$1(this).trigger('click');
          return;
        }

        var items = [].slice.call(parent.querySelectorAll(Selector.VISIBLE_ITEMS));

        if (items.length === 0) {
          return;
        }

        var index = items.indexOf(event.target);

        if (event.which === ARROW_UP_KEYCODE && index > 0) {
          // Up
          index--;
        }

        if (event.which === ARROW_DOWN_KEYCODE && index < items.length - 1) {
          // Down
          index++;
        }

        if (index < 0) {
          index = 0;
        }

        items[index].focus();
      };

      _createClass(Dropdown, null, [{
        key: "VERSION",
        get: function get() {
          return VERSION;
        }
      }, {
        key: "Default",
        get: function get() {
          return Default;
        }
      }, {
        key: "DefaultType",
        get: function get() {
          return DefaultType;
        }
      }]);

      return Dropdown;
    }();
    /**
     * ------------------------------------------------------------------------
     * Data Api implementation
     * ------------------------------------------------------------------------
     */


    $$$1(document).on(Event.KEYDOWN_DATA_API, Selector.DATA_TOGGLE, Dropdown._dataApiKeydownHandler).on(Event.KEYDOWN_DATA_API, Selector.MENU, Dropdown._dataApiKeydownHandler).on(Event.CLICK_DATA_API + " " + Event.KEYUP_DATA_API, Dropdown._clearMenus).on(Event.CLICK_DATA_API, Selector.DATA_TOGGLE, function (event) {
      event.preventDefault();
      event.stopPropagation();

      Dropdown._jQueryInterface.call($$$1(this), 'toggle');
    }).on(Event.CLICK_DATA_API, Selector.FORM_CHILD, function (e) {
      e.stopPropagation();
    });
    /**
     * ------------------------------------------------------------------------
     * jQuery
     * ------------------------------------------------------------------------
     */

    $$$1.fn[NAME] = Dropdown._jQueryInterface;
    $$$1.fn[NAME].Constructor = Dropdown;

    $$$1.fn[NAME].noConflict = function () {
      $$$1.fn[NAME] = JQUERY_NO_CONFLICT;
      return Dropdown._jQueryInterface;
    };

    return Dropdown;
  }($, Popper);

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.1.3): modal.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  var Modal = function ($$$1) {
    /**
     * ------------------------------------------------------------------------
     * Constants
     * ------------------------------------------------------------------------
     */
    var NAME = 'modal';
    var VERSION = '4.1.3';
    var DATA_KEY = 'bs.modal';
    var EVENT_KEY = "." + DATA_KEY;
    var DATA_API_KEY = '.data-api';
    var JQUERY_NO_CONFLICT = $$$1.fn[NAME];
    var ESCAPE_KEYCODE = 27; // KeyboardEvent.which value for Escape (Esc) key

    var Default = {
      backdrop: true,
      keyboard: true,
      focus: true,
      show: true
    };
    var DefaultType = {
      backdrop: '(boolean|string)',
      keyboard: 'boolean',
      focus: 'boolean',
      show: 'boolean'
    };
    var Event = {
      HIDE: "hide" + EVENT_KEY,
      HIDDEN: "hidden" + EVENT_KEY,
      SHOW: "show" + EVENT_KEY,
      SHOWN: "shown" + EVENT_KEY,
      FOCUSIN: "focusin" + EVENT_KEY,
      RESIZE: "resize" + EVENT_KEY,
      CLICK_DISMISS: "click.dismiss" + EVENT_KEY,
      KEYDOWN_DISMISS: "keydown.dismiss" + EVENT_KEY,
      MOUSEUP_DISMISS: "mouseup.dismiss" + EVENT_KEY,
      MOUSEDOWN_DISMISS: "mousedown.dismiss" + EVENT_KEY,
      CLICK_DATA_API: "click" + EVENT_KEY + DATA_API_KEY
    };
    var ClassName = {
      SCROLLBAR_MEASURER: 'modal-scrollbar-measure',
      BACKDROP: 'modal-backdrop',
      OPEN: 'modal-open',
      FADE: 'fade',
      SHOW: 'show'
    };
    var Selector = {
      DIALOG: '.modal-dialog',
      DATA_TOGGLE: '[data-toggle="modal"]',
      DATA_DISMISS: '[data-dismiss="modal"]',
      FIXED_CONTENT: '.fixed-top, .fixed-bottom, .is-fixed, .sticky-top',
      STICKY_CONTENT: '.sticky-top'
      /**
       * ------------------------------------------------------------------------
       * Class Definition
       * ------------------------------------------------------------------------
       */

    };

    var Modal =
    /*#__PURE__*/
    function () {
      function Modal(element, config) {
        this._config = this._getConfig(config);
        this._element = element;
        this._dialog = element.querySelector(Selector.DIALOG);
        this._backdrop = null;
        this._isShown = false;
        this._isBodyOverflowing = false;
        this._ignoreBackdropClick = false;
        this._scrollbarWidth = 0;
      } // Getters


      var _proto = Modal.prototype;

      // Public
      _proto.toggle = function toggle(relatedTarget) {
        return this._isShown ? this.hide() : this.show(relatedTarget);
      };

      _proto.show = function show(relatedTarget) {
        var _this = this;

        if (this._isTransitioning || this._isShown) {
          return;
        }

        if ($$$1(this._element).hasClass(ClassName.FADE)) {
          this._isTransitioning = true;
        }

        var showEvent = $$$1.Event(Event.SHOW, {
          relatedTarget: relatedTarget
        });
        $$$1(this._element).trigger(showEvent);

        if (this._isShown || showEvent.isDefaultPrevented()) {
          return;
        }

        this._isShown = true;

        this._checkScrollbar();

        this._setScrollbar();

        this._adjustDialog();

        $$$1(document.body).addClass(ClassName.OPEN);

        this._setEscapeEvent();

        this._setResizeEvent();

        $$$1(this._element).on(Event.CLICK_DISMISS, Selector.DATA_DISMISS, function (event) {
          return _this.hide(event);
        });
        $$$1(this._dialog).on(Event.MOUSEDOWN_DISMISS, function () {
          $$$1(_this._element).one(Event.MOUSEUP_DISMISS, function (event) {
            if ($$$1(event.target).is(_this._element)) {
              _this._ignoreBackdropClick = true;
            }
          });
        });

        this._showBackdrop(function () {
          return _this._showElement(relatedTarget);
        });
      };

      _proto.hide = function hide(event) {
        var _this2 = this;

        if (event) {
          event.preventDefault();
        }

        if (this._isTransitioning || !this._isShown) {
          return;
        }

        var hideEvent = $$$1.Event(Event.HIDE);
        $$$1(this._element).trigger(hideEvent);

        if (!this._isShown || hideEvent.isDefaultPrevented()) {
          return;
        }

        this._isShown = false;
        var transition = $$$1(this._element).hasClass(ClassName.FADE);

        if (transition) {
          this._isTransitioning = true;
        }

        this._setEscapeEvent();

        this._setResizeEvent();

        $$$1(document).off(Event.FOCUSIN);
        $$$1(this._element).removeClass(ClassName.SHOW);
        $$$1(this._element).off(Event.CLICK_DISMISS);
        $$$1(this._dialog).off(Event.MOUSEDOWN_DISMISS);

        if (transition) {
          var transitionDuration = Util.getTransitionDurationFromElement(this._element);
          $$$1(this._element).one(Util.TRANSITION_END, function (event) {
            return _this2._hideModal(event);
          }).emulateTransitionEnd(transitionDuration);
        } else {
          this._hideModal();
        }
      };

      _proto.dispose = function dispose() {
        $$$1.removeData(this._element, DATA_KEY);
        $$$1(window, document, this._element, this._backdrop).off(EVENT_KEY);
        this._config = null;
        this._element = null;
        this._dialog = null;
        this._backdrop = null;
        this._isShown = null;
        this._isBodyOverflowing = null;
        this._ignoreBackdropClick = null;
        this._scrollbarWidth = null;
      };

      _proto.handleUpdate = function handleUpdate() {
        this._adjustDialog();
      }; // Private


      _proto._getConfig = function _getConfig(config) {
        config = _objectSpread({}, Default, config);
        Util.typeCheckConfig(NAME, config, DefaultType);
        return config;
      };

      _proto._showElement = function _showElement(relatedTarget) {
        var _this3 = this;

        var transition = $$$1(this._element).hasClass(ClassName.FADE);

        if (!this._element.parentNode || this._element.parentNode.nodeType !== Node.ELEMENT_NODE) {
          // Don't move modal's DOM position
          document.body.appendChild(this._element);
        }

        this._element.style.display = 'block';

        this._element.removeAttribute('aria-hidden');

        this._element.scrollTop = 0;

        if (transition) {
          Util.reflow(this._element);
        }

        $$$1(this._element).addClass(ClassName.SHOW);

        if (this._config.focus) {
          this._enforceFocus();
        }

        var shownEvent = $$$1.Event(Event.SHOWN, {
          relatedTarget: relatedTarget
        });

        var transitionComplete = function transitionComplete() {
          if (_this3._config.focus) {
            _this3._element.focus();
          }

          _this3._isTransitioning = false;
          $$$1(_this3._element).trigger(shownEvent);
        };

        if (transition) {
          var transitionDuration = Util.getTransitionDurationFromElement(this._element);
          $$$1(this._dialog).one(Util.TRANSITION_END, transitionComplete).emulateTransitionEnd(transitionDuration);
        } else {
          transitionComplete();
        }
      };

      _proto._enforceFocus = function _enforceFocus() {
        var _this4 = this;

        $$$1(document).off(Event.FOCUSIN) // Guard against infinite focus loop
        .on(Event.FOCUSIN, function (event) {
          if (document !== event.target && _this4._element !== event.target && $$$1(_this4._element).has(event.target).length === 0) {
            _this4._element.focus();
          }
        });
      };

      _proto._setEscapeEvent = function _setEscapeEvent() {
        var _this5 = this;

        if (this._isShown && this._config.keyboard) {
          $$$1(this._element).on(Event.KEYDOWN_DISMISS, function (event) {
            if (event.which === ESCAPE_KEYCODE) {
              event.preventDefault();

              _this5.hide();
            }
          });
        } else if (!this._isShown) {
          $$$1(this._element).off(Event.KEYDOWN_DISMISS);
        }
      };

      _proto._setResizeEvent = function _setResizeEvent() {
        var _this6 = this;

        if (this._isShown) {
          $$$1(window).on(Event.RESIZE, function (event) {
            return _this6.handleUpdate(event);
          });
        } else {
          $$$1(window).off(Event.RESIZE);
        }
      };

      _proto._hideModal = function _hideModal() {
        var _this7 = this;

        this._element.style.display = 'none';

        this._element.setAttribute('aria-hidden', true);

        this._isTransitioning = false;

        this._showBackdrop(function () {
          $$$1(document.body).removeClass(ClassName.OPEN);

          _this7._resetAdjustments();

          _this7._resetScrollbar();

          $$$1(_this7._element).trigger(Event.HIDDEN);
        });
      };

      _proto._removeBackdrop = function _removeBackdrop() {
        if (this._backdrop) {
          $$$1(this._backdrop).remove();
          this._backdrop = null;
        }
      };

      _proto._showBackdrop = function _showBackdrop(callback) {
        var _this8 = this;

        var animate = $$$1(this._element).hasClass(ClassName.FADE) ? ClassName.FADE : '';

        if (this._isShown && this._config.backdrop) {
          this._backdrop = document.createElement('div');
          this._backdrop.className = ClassName.BACKDROP;

          if (animate) {
            this._backdrop.classList.add(animate);
          }

          $$$1(this._backdrop).appendTo(document.body);
          $$$1(this._element).on(Event.CLICK_DISMISS, function (event) {
            if (_this8._ignoreBackdropClick) {
              _this8._ignoreBackdropClick = false;
              return;
            }

            if (event.target !== event.currentTarget) {
              return;
            }

            if (_this8._config.backdrop === 'static') {
              _this8._element.focus();
            } else {
              _this8.hide();
            }
          });

          if (animate) {
            Util.reflow(this._backdrop);
          }

          $$$1(this._backdrop).addClass(ClassName.SHOW);

          if (!callback) {
            return;
          }

          if (!animate) {
            callback();
            return;
          }

          var backdropTransitionDuration = Util.getTransitionDurationFromElement(this._backdrop);
          $$$1(this._backdrop).one(Util.TRANSITION_END, callback).emulateTransitionEnd(backdropTransitionDuration);
        } else if (!this._isShown && this._backdrop) {
          $$$1(this._backdrop).removeClass(ClassName.SHOW);

          var callbackRemove = function callbackRemove() {
            _this8._removeBackdrop();

            if (callback) {
              callback();
            }
          };

          if ($$$1(this._element).hasClass(ClassName.FADE)) {
            var _backdropTransitionDuration = Util.getTransitionDurationFromElement(this._backdrop);

            $$$1(this._backdrop).one(Util.TRANSITION_END, callbackRemove).emulateTransitionEnd(_backdropTransitionDuration);
          } else {
            callbackRemove();
          }
        } else if (callback) {
          callback();
        }
      }; // ----------------------------------------------------------------------
      // the following methods are used to handle overflowing modals
      // todo (fat): these should probably be refactored out of modal.js
      // ----------------------------------------------------------------------


      _proto._adjustDialog = function _adjustDialog() {
        var isModalOverflowing = this._element.scrollHeight > document.documentElement.clientHeight;

        if (!this._isBodyOverflowing && isModalOverflowing) {
          this._element.style.paddingLeft = this._scrollbarWidth + "px";
        }

        if (this._isBodyOverflowing && !isModalOverflowing) {
          this._element.style.paddingRight = this._scrollbarWidth + "px";
        }
      };

      _proto._resetAdjustments = function _resetAdjustments() {
        this._element.style.paddingLeft = '';
        this._element.style.paddingRight = '';
      };

      _proto._checkScrollbar = function _checkScrollbar() {
        var rect = document.body.getBoundingClientRect();
        this._isBodyOverflowing = rect.left + rect.right < window.innerWidth;
        this._scrollbarWidth = this._getScrollbarWidth();
      };

      _proto._setScrollbar = function _setScrollbar() {
        var _this9 = this;

        if (this._isBodyOverflowing) {
          // Note: DOMNode.style.paddingRight returns the actual value or '' if not set
          //   while $(DOMNode).css('padding-right') returns the calculated value or 0 if not set
          var fixedContent = [].slice.call(document.querySelectorAll(Selector.FIXED_CONTENT));
          var stickyContent = [].slice.call(document.querySelectorAll(Selector.STICKY_CONTENT)); // Adjust fixed content padding

          $$$1(fixedContent).each(function (index, element) {
            var actualPadding = element.style.paddingRight;
            var calculatedPadding = $$$1(element).css('padding-right');
            $$$1(element).data('padding-right', actualPadding).css('padding-right', parseFloat(calculatedPadding) + _this9._scrollbarWidth + "px");
          }); // Adjust sticky content margin

          $$$1(stickyContent).each(function (index, element) {
            var actualMargin = element.style.marginRight;
            var calculatedMargin = $$$1(element).css('margin-right');
            $$$1(element).data('margin-right', actualMargin).css('margin-right', parseFloat(calculatedMargin) - _this9._scrollbarWidth + "px");
          }); // Adjust body padding

          var actualPadding = document.body.style.paddingRight;
          var calculatedPadding = $$$1(document.body).css('padding-right');
          $$$1(document.body).data('padding-right', actualPadding).css('padding-right', parseFloat(calculatedPadding) + this._scrollbarWidth + "px");
        }
      };

      _proto._resetScrollbar = function _resetScrollbar() {
        // Restore fixed content padding
        var fixedContent = [].slice.call(document.querySelectorAll(Selector.FIXED_CONTENT));
        $$$1(fixedContent).each(function (index, element) {
          var padding = $$$1(element).data('padding-right');
          $$$1(element).removeData('padding-right');
          element.style.paddingRight = padding ? padding : '';
        }); // Restore sticky content

        var elements = [].slice.call(document.querySelectorAll("" + Selector.STICKY_CONTENT));
        $$$1(elements).each(function (index, element) {
          var margin = $$$1(element).data('margin-right');

          if (typeof margin !== 'undefined') {
            $$$1(element).css('margin-right', margin).removeData('margin-right');
          }
        }); // Restore body padding

        var padding = $$$1(document.body).data('padding-right');
        $$$1(document.body).removeData('padding-right');
        document.body.style.paddingRight = padding ? padding : '';
      };

      _proto._getScrollbarWidth = function _getScrollbarWidth() {
        // thx d.walsh
        var scrollDiv = document.createElement('div');
        scrollDiv.className = ClassName.SCROLLBAR_MEASURER;
        document.body.appendChild(scrollDiv);
        var scrollbarWidth = scrollDiv.getBoundingClientRect().width - scrollDiv.clientWidth;
        document.body.removeChild(scrollDiv);
        return scrollbarWidth;
      }; // Static


      Modal._jQueryInterface = function _jQueryInterface(config, relatedTarget) {
        return this.each(function () {
          var data = $$$1(this).data(DATA_KEY);

          var _config = _objectSpread({}, Default, $$$1(this).data(), typeof config === 'object' && config ? config : {});

          if (!data) {
            data = new Modal(this, _config);
            $$$1(this).data(DATA_KEY, data);
          }

          if (typeof config === 'string') {
            if (typeof data[config] === 'undefined') {
              throw new TypeError("No method named \"" + config + "\"");
            }

            data[config](relatedTarget);
          } else if (_config.show) {
            data.show(relatedTarget);
          }
        });
      };

      _createClass(Modal, null, [{
        key: "VERSION",
        get: function get() {
          return VERSION;
        }
      }, {
        key: "Default",
        get: function get() {
          return Default;
        }
      }]);

      return Modal;
    }();
    /**
     * ------------------------------------------------------------------------
     * Data Api implementation
     * ------------------------------------------------------------------------
     */


    $$$1(document).on(Event.CLICK_DATA_API, Selector.DATA_TOGGLE, function (event) {
      var _this10 = this;

      var target;
      var selector = Util.getSelectorFromElement(this);

      if (selector) {
        target = document.querySelector(selector);
      }

      var config = $$$1(target).data(DATA_KEY) ? 'toggle' : _objectSpread({}, $$$1(target).data(), $$$1(this).data());

      if (this.tagName === 'A' || this.tagName === 'AREA') {
        event.preventDefault();
      }

      var $target = $$$1(target).one(Event.SHOW, function (showEvent) {
        if (showEvent.isDefaultPrevented()) {
          // Only register focus restorer if modal will actually get shown
          return;
        }

        $target.one(Event.HIDDEN, function () {
          if ($$$1(_this10).is(':visible')) {
            _this10.focus();
          }
        });
      });

      Modal._jQueryInterface.call($$$1(target), config, this);
    });
    /**
     * ------------------------------------------------------------------------
     * jQuery
     * ------------------------------------------------------------------------
     */

    $$$1.fn[NAME] = Modal._jQueryInterface;
    $$$1.fn[NAME].Constructor = Modal;

    $$$1.fn[NAME].noConflict = function () {
      $$$1.fn[NAME] = JQUERY_NO_CONFLICT;
      return Modal._jQueryInterface;
    };

    return Modal;
  }($);

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.1.3): tooltip.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  var Tooltip = function ($$$1) {
    /**
     * ------------------------------------------------------------------------
     * Constants
     * ------------------------------------------------------------------------
     */
    var NAME = 'tooltip';
    var VERSION = '4.1.3';
    var DATA_KEY = 'bs.tooltip';
    var EVENT_KEY = "." + DATA_KEY;
    var JQUERY_NO_CONFLICT = $$$1.fn[NAME];
    var CLASS_PREFIX = 'bs-tooltip';
    var BSCLS_PREFIX_REGEX = new RegExp("(^|\\s)" + CLASS_PREFIX + "\\S+", 'g');
    var DefaultType = {
      animation: 'boolean',
      template: 'string',
      title: '(string|element|function)',
      trigger: 'string',
      delay: '(number|object)',
      html: 'boolean',
      selector: '(string|boolean)',
      placement: '(string|function)',
      offset: '(number|string)',
      container: '(string|element|boolean)',
      fallbackPlacement: '(string|array)',
      boundary: '(string|element)'
    };
    var AttachmentMap = {
      AUTO: 'auto',
      TOP: 'top',
      RIGHT: 'right',
      BOTTOM: 'bottom',
      LEFT: 'left'
    };
    var Default = {
      animation: true,
      template: '<div class="tooltip" role="tooltip">' + '<div class="arrow"></div>' + '<div class="tooltip-inner"></div></div>',
      trigger: 'hover focus',
      title: '',
      delay: 0,
      html: false,
      selector: false,
      placement: 'top',
      offset: 0,
      container: false,
      fallbackPlacement: 'flip',
      boundary: 'scrollParent'
    };
    var HoverState = {
      SHOW: 'show',
      OUT: 'out'
    };
    var Event = {
      HIDE: "hide" + EVENT_KEY,
      HIDDEN: "hidden" + EVENT_KEY,
      SHOW: "show" + EVENT_KEY,
      SHOWN: "shown" + EVENT_KEY,
      INSERTED: "inserted" + EVENT_KEY,
      CLICK: "click" + EVENT_KEY,
      FOCUSIN: "focusin" + EVENT_KEY,
      FOCUSOUT: "focusout" + EVENT_KEY,
      MOUSEENTER: "mouseenter" + EVENT_KEY,
      MOUSELEAVE: "mouseleave" + EVENT_KEY
    };
    var ClassName = {
      FADE: 'fade',
      SHOW: 'show'
    };
    var Selector = {
      TOOLTIP: '.tooltip',
      TOOLTIP_INNER: '.tooltip-inner',
      ARROW: '.arrow'
    };
    var Trigger = {
      HOVER: 'hover',
      FOCUS: 'focus',
      CLICK: 'click',
      MANUAL: 'manual'
      /**
       * ------------------------------------------------------------------------
       * Class Definition
       * ------------------------------------------------------------------------
       */

    };

    var Tooltip =
    /*#__PURE__*/
    function () {
      function Tooltip(element, config) {
        /**
         * Check for Popper dependency
         * Popper - https://popper.js.org
         */
        if (typeof Popper === 'undefined') {
          throw new TypeError('Bootstrap tooltips require Popper.js (https://popper.js.org)');
        } // private


        this._isEnabled = true;
        this._timeout = 0;
        this._hoverState = '';
        this._activeTrigger = {};
        this._popper = null; // Protected

        this.element = element;
        this.config = this._getConfig(config);
        this.tip = null;

        this._setListeners();
      } // Getters


      var _proto = Tooltip.prototype;

      // Public
      _proto.enable = function enable() {
        this._isEnabled = true;
      };

      _proto.disable = function disable() {
        this._isEnabled = false;
      };

      _proto.toggleEnabled = function toggleEnabled() {
        this._isEnabled = !this._isEnabled;
      };

      _proto.toggle = function toggle(event) {
        if (!this._isEnabled) {
          return;
        }

        if (event) {
          var dataKey = this.constructor.DATA_KEY;
          var context = $$$1(event.currentTarget).data(dataKey);

          if (!context) {
            context = new this.constructor(event.currentTarget, this._getDelegateConfig());
            $$$1(event.currentTarget).data(dataKey, context);
          }

          context._activeTrigger.click = !context._activeTrigger.click;

          if (context._isWithActiveTrigger()) {
            context._enter(null, context);
          } else {
            context._leave(null, context);
          }
        } else {
          if ($$$1(this.getTipElement()).hasClass(ClassName.SHOW)) {
            this._leave(null, this);

            return;
          }

          this._enter(null, this);
        }
      };

      _proto.dispose = function dispose() {
        clearTimeout(this._timeout);
        $$$1.removeData(this.element, this.constructor.DATA_KEY);
        $$$1(this.element).off(this.constructor.EVENT_KEY);
        $$$1(this.element).closest('.modal').off('hide.bs.modal');

        if (this.tip) {
          $$$1(this.tip).remove();
        }

        this._isEnabled = null;
        this._timeout = null;
        this._hoverState = null;
        this._activeTrigger = null;

        if (this._popper !== null) {
          this._popper.destroy();
        }

        this._popper = null;
        this.element = null;
        this.config = null;
        this.tip = null;
      };

      _proto.show = function show() {
        var _this = this;

        if ($$$1(this.element).css('display') === 'none') {
          throw new Error('Please use show on visible elements');
        }

        var showEvent = $$$1.Event(this.constructor.Event.SHOW);

        if (this.isWithContent() && this._isEnabled) {
          $$$1(this.element).trigger(showEvent);
          var isInTheDom = $$$1.contains(this.element.ownerDocument.documentElement, this.element);

          if (showEvent.isDefaultPrevented() || !isInTheDom) {
            return;
          }

          var tip = this.getTipElement();
          var tipId = Util.getUID(this.constructor.NAME);
          tip.setAttribute('id', tipId);
          this.element.setAttribute('aria-describedby', tipId);
          this.setContent();

          if (this.config.animation) {
            $$$1(tip).addClass(ClassName.FADE);
          }

          var placement = typeof this.config.placement === 'function' ? this.config.placement.call(this, tip, this.element) : this.config.placement;

          var attachment = this._getAttachment(placement);

          this.addAttachmentClass(attachment);
          var container = this.config.container === false ? document.body : $$$1(document).find(this.config.container);
          $$$1(tip).data(this.constructor.DATA_KEY, this);

          if (!$$$1.contains(this.element.ownerDocument.documentElement, this.tip)) {
            $$$1(tip).appendTo(container);
          }

          $$$1(this.element).trigger(this.constructor.Event.INSERTED);
          this._popper = new Popper(this.element, tip, {
            placement: attachment,
            modifiers: {
              offset: {
                offset: this.config.offset
              },
              flip: {
                behavior: this.config.fallbackPlacement
              },
              arrow: {
                element: Selector.ARROW
              },
              preventOverflow: {
                boundariesElement: this.config.boundary
              }
            },
            onCreate: function onCreate(data) {
              if (data.originalPlacement !== data.placement) {
                _this._handlePopperPlacementChange(data);
              }
            },
            onUpdate: function onUpdate(data) {
              _this._handlePopperPlacementChange(data);
            }
          });
          $$$1(tip).addClass(ClassName.SHOW); // If this is a touch-enabled device we add extra
          // empty mouseover listeners to the body's immediate children;
          // only needed because of broken event delegation on iOS
          // https://www.quirksmode.org/blog/archives/2014/02/mouse_event_bub.html

          if ('ontouchstart' in document.documentElement) {
            $$$1(document.body).children().on('mouseover', null, $$$1.noop);
          }

          var complete = function complete() {
            if (_this.config.animation) {
              _this._fixTransition();
            }

            var prevHoverState = _this._hoverState;
            _this._hoverState = null;
            $$$1(_this.element).trigger(_this.constructor.Event.SHOWN);

            if (prevHoverState === HoverState.OUT) {
              _this._leave(null, _this);
            }
          };

          if ($$$1(this.tip).hasClass(ClassName.FADE)) {
            var transitionDuration = Util.getTransitionDurationFromElement(this.tip);
            $$$1(this.tip).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
          } else {
            complete();
          }
        }
      };

      _proto.hide = function hide(callback) {
        var _this2 = this;

        var tip = this.getTipElement();
        var hideEvent = $$$1.Event(this.constructor.Event.HIDE);

        var complete = function complete() {
          if (_this2._hoverState !== HoverState.SHOW && tip.parentNode) {
            tip.parentNode.removeChild(tip);
          }

          _this2._cleanTipClass();

          _this2.element.removeAttribute('aria-describedby');

          $$$1(_this2.element).trigger(_this2.constructor.Event.HIDDEN);

          if (_this2._popper !== null) {
            _this2._popper.destroy();
          }

          if (callback) {
            callback();
          }
        };

        $$$1(this.element).trigger(hideEvent);

        if (hideEvent.isDefaultPrevented()) {
          return;
        }

        $$$1(tip).removeClass(ClassName.SHOW); // If this is a touch-enabled device we remove the extra
        // empty mouseover listeners we added for iOS support

        if ('ontouchstart' in document.documentElement) {
          $$$1(document.body).children().off('mouseover', null, $$$1.noop);
        }

        this._activeTrigger[Trigger.CLICK] = false;
        this._activeTrigger[Trigger.FOCUS] = false;
        this._activeTrigger[Trigger.HOVER] = false;

        if ($$$1(this.tip).hasClass(ClassName.FADE)) {
          var transitionDuration = Util.getTransitionDurationFromElement(tip);
          $$$1(tip).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
        } else {
          complete();
        }

        this._hoverState = '';
      };

      _proto.update = function update() {
        if (this._popper !== null) {
          this._popper.scheduleUpdate();
        }
      }; // Protected


      _proto.isWithContent = function isWithContent() {
        return Boolean(this.getTitle());
      };

      _proto.addAttachmentClass = function addAttachmentClass(attachment) {
        $$$1(this.getTipElement()).addClass(CLASS_PREFIX + "-" + attachment);
      };

      _proto.getTipElement = function getTipElement() {
        this.tip = this.tip || $$$1(this.config.template)[0];
        return this.tip;
      };

      _proto.setContent = function setContent() {
        var tip = this.getTipElement();
        this.setElementContent($$$1(tip.querySelectorAll(Selector.TOOLTIP_INNER)), this.getTitle());
        $$$1(tip).removeClass(ClassName.FADE + " " + ClassName.SHOW);
      };

      _proto.setElementContent = function setElementContent($element, content) {
        var html = this.config.html;

        if (typeof content === 'object' && (content.nodeType || content.jquery)) {
          // Content is a DOM node or a jQuery
          if (html) {
            if (!$$$1(content).parent().is($element)) {
              $element.empty().append(content);
            }
          } else {
            $element.text($$$1(content).text());
          }
        } else {
          $element[html ? 'html' : 'text'](content);
        }
      };

      _proto.getTitle = function getTitle() {
        var title = this.element.getAttribute('data-original-title');

        if (!title) {
          title = typeof this.config.title === 'function' ? this.config.title.call(this.element) : this.config.title;
        }

        return title;
      }; // Private


      _proto._getAttachment = function _getAttachment(placement) {
        return AttachmentMap[placement.toUpperCase()];
      };

      _proto._setListeners = function _setListeners() {
        var _this3 = this;

        var triggers = this.config.trigger.split(' ');
        triggers.forEach(function (trigger) {
          if (trigger === 'click') {
            $$$1(_this3.element).on(_this3.constructor.Event.CLICK, _this3.config.selector, function (event) {
              return _this3.toggle(event);
            });
          } else if (trigger !== Trigger.MANUAL) {
            var eventIn = trigger === Trigger.HOVER ? _this3.constructor.Event.MOUSEENTER : _this3.constructor.Event.FOCUSIN;
            var eventOut = trigger === Trigger.HOVER ? _this3.constructor.Event.MOUSELEAVE : _this3.constructor.Event.FOCUSOUT;
            $$$1(_this3.element).on(eventIn, _this3.config.selector, function (event) {
              return _this3._enter(event);
            }).on(eventOut, _this3.config.selector, function (event) {
              return _this3._leave(event);
            });
          }

          $$$1(_this3.element).closest('.modal').on('hide.bs.modal', function () {
            return _this3.hide();
          });
        });

        if (this.config.selector) {
          this.config = _objectSpread({}, this.config, {
            trigger: 'manual',
            selector: ''
          });
        } else {
          this._fixTitle();
        }
      };

      _proto._fixTitle = function _fixTitle() {
        var titleType = typeof this.element.getAttribute('data-original-title');

        if (this.element.getAttribute('title') || titleType !== 'string') {
          this.element.setAttribute('data-original-title', this.element.getAttribute('title') || '');
          this.element.setAttribute('title', '');
        }
      };

      _proto._enter = function _enter(event, context) {
        var dataKey = this.constructor.DATA_KEY;
        context = context || $$$1(event.currentTarget).data(dataKey);

        if (!context) {
          context = new this.constructor(event.currentTarget, this._getDelegateConfig());
          $$$1(event.currentTarget).data(dataKey, context);
        }

        if (event) {
          context._activeTrigger[event.type === 'focusin' ? Trigger.FOCUS : Trigger.HOVER] = true;
        }

        if ($$$1(context.getTipElement()).hasClass(ClassName.SHOW) || context._hoverState === HoverState.SHOW) {
          context._hoverState = HoverState.SHOW;
          return;
        }

        clearTimeout(context._timeout);
        context._hoverState = HoverState.SHOW;

        if (!context.config.delay || !context.config.delay.show) {
          context.show();
          return;
        }

        context._timeout = setTimeout(function () {
          if (context._hoverState === HoverState.SHOW) {
            context.show();
          }
        }, context.config.delay.show);
      };

      _proto._leave = function _leave(event, context) {
        var dataKey = this.constructor.DATA_KEY;
        context = context || $$$1(event.currentTarget).data(dataKey);

        if (!context) {
          context = new this.constructor(event.currentTarget, this._getDelegateConfig());
          $$$1(event.currentTarget).data(dataKey, context);
        }

        if (event) {
          context._activeTrigger[event.type === 'focusout' ? Trigger.FOCUS : Trigger.HOVER] = false;
        }

        if (context._isWithActiveTrigger()) {
          return;
        }

        clearTimeout(context._timeout);
        context._hoverState = HoverState.OUT;

        if (!context.config.delay || !context.config.delay.hide) {
          context.hide();
          return;
        }

        context._timeout = setTimeout(function () {
          if (context._hoverState === HoverState.OUT) {
            context.hide();
          }
        }, context.config.delay.hide);
      };

      _proto._isWithActiveTrigger = function _isWithActiveTrigger() {
        for (var trigger in this._activeTrigger) {
          if (this._activeTrigger[trigger]) {
            return true;
          }
        }

        return false;
      };

      _proto._getConfig = function _getConfig(config) {
        config = _objectSpread({}, this.constructor.Default, $$$1(this.element).data(), typeof config === 'object' && config ? config : {});

        if (typeof config.delay === 'number') {
          config.delay = {
            show: config.delay,
            hide: config.delay
          };
        }

        if (typeof config.title === 'number') {
          config.title = config.title.toString();
        }

        if (typeof config.content === 'number') {
          config.content = config.content.toString();
        }

        Util.typeCheckConfig(NAME, config, this.constructor.DefaultType);
        return config;
      };

      _proto._getDelegateConfig = function _getDelegateConfig() {
        var config = {};

        if (this.config) {
          for (var key in this.config) {
            if (this.constructor.Default[key] !== this.config[key]) {
              config[key] = this.config[key];
            }
          }
        }

        return config;
      };

      _proto._cleanTipClass = function _cleanTipClass() {
        var $tip = $$$1(this.getTipElement());
        var tabClass = $tip.attr('class').match(BSCLS_PREFIX_REGEX);

        if (tabClass !== null && tabClass.length) {
          $tip.removeClass(tabClass.join(''));
        }
      };

      _proto._handlePopperPlacementChange = function _handlePopperPlacementChange(popperData) {
        var popperInstance = popperData.instance;
        this.tip = popperInstance.popper;

        this._cleanTipClass();

        this.addAttachmentClass(this._getAttachment(popperData.placement));
      };

      _proto._fixTransition = function _fixTransition() {
        var tip = this.getTipElement();
        var initConfigAnimation = this.config.animation;

        if (tip.getAttribute('x-placement') !== null) {
          return;
        }

        $$$1(tip).removeClass(ClassName.FADE);
        this.config.animation = false;
        this.hide();
        this.show();
        this.config.animation = initConfigAnimation;
      }; // Static


      Tooltip._jQueryInterface = function _jQueryInterface(config) {
        return this.each(function () {
          var data = $$$1(this).data(DATA_KEY);

          var _config = typeof config === 'object' && config;

          if (!data && /dispose|hide/.test(config)) {
            return;
          }

          if (!data) {
            data = new Tooltip(this, _config);
            $$$1(this).data(DATA_KEY, data);
          }

          if (typeof config === 'string') {
            if (typeof data[config] === 'undefined') {
              throw new TypeError("No method named \"" + config + "\"");
            }

            data[config]();
          }
        });
      };

      _createClass(Tooltip, null, [{
        key: "VERSION",
        get: function get() {
          return VERSION;
        }
      }, {
        key: "Default",
        get: function get() {
          return Default;
        }
      }, {
        key: "NAME",
        get: function get() {
          return NAME;
        }
      }, {
        key: "DATA_KEY",
        get: function get() {
          return DATA_KEY;
        }
      }, {
        key: "Event",
        get: function get() {
          return Event;
        }
      }, {
        key: "EVENT_KEY",
        get: function get() {
          return EVENT_KEY;
        }
      }, {
        key: "DefaultType",
        get: function get() {
          return DefaultType;
        }
      }]);

      return Tooltip;
    }();
    /**
     * ------------------------------------------------------------------------
     * jQuery
     * ------------------------------------------------------------------------
     */


    $$$1.fn[NAME] = Tooltip._jQueryInterface;
    $$$1.fn[NAME].Constructor = Tooltip;

    $$$1.fn[NAME].noConflict = function () {
      $$$1.fn[NAME] = JQUERY_NO_CONFLICT;
      return Tooltip._jQueryInterface;
    };

    return Tooltip;
  }($, Popper);

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.1.3): popover.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  var Popover = function ($$$1) {
    /**
     * ------------------------------------------------------------------------
     * Constants
     * ------------------------------------------------------------------------
     */
    var NAME = 'popover';
    var VERSION = '4.1.3';
    var DATA_KEY = 'bs.popover';
    var EVENT_KEY = "." + DATA_KEY;
    var JQUERY_NO_CONFLICT = $$$1.fn[NAME];
    var CLASS_PREFIX = 'bs-popover';
    var BSCLS_PREFIX_REGEX = new RegExp("(^|\\s)" + CLASS_PREFIX + "\\S+", 'g');

    var Default = _objectSpread({}, Tooltip.Default, {
      placement: 'right',
      trigger: 'click',
      content: '',
      template: '<div class="popover" role="tooltip">' + '<div class="arrow"></div>' + '<h3 class="popover-header"></h3>' + '<div class="popover-body"></div></div>'
    });

    var DefaultType = _objectSpread({}, Tooltip.DefaultType, {
      content: '(string|element|function)'
    });

    var ClassName = {
      FADE: 'fade',
      SHOW: 'show'
    };
    var Selector = {
      TITLE: '.popover-header',
      CONTENT: '.popover-body'
    };
    var Event = {
      HIDE: "hide" + EVENT_KEY,
      HIDDEN: "hidden" + EVENT_KEY,
      SHOW: "show" + EVENT_KEY,
      SHOWN: "shown" + EVENT_KEY,
      INSERTED: "inserted" + EVENT_KEY,
      CLICK: "click" + EVENT_KEY,
      FOCUSIN: "focusin" + EVENT_KEY,
      FOCUSOUT: "focusout" + EVENT_KEY,
      MOUSEENTER: "mouseenter" + EVENT_KEY,
      MOUSELEAVE: "mouseleave" + EVENT_KEY
      /**
       * ------------------------------------------------------------------------
       * Class Definition
       * ------------------------------------------------------------------------
       */

    };

    var Popover =
    /*#__PURE__*/
    function (_Tooltip) {
      _inheritsLoose(Popover, _Tooltip);

      function Popover() {
        return _Tooltip.apply(this, arguments) || this;
      }

      var _proto = Popover.prototype;

      // Overrides
      _proto.isWithContent = function isWithContent() {
        return this.getTitle() || this._getContent();
      };

      _proto.addAttachmentClass = function addAttachmentClass(attachment) {
        $$$1(this.getTipElement()).addClass(CLASS_PREFIX + "-" + attachment);
      };

      _proto.getTipElement = function getTipElement() {
        this.tip = this.tip || $$$1(this.config.template)[0];
        return this.tip;
      };

      _proto.setContent = function setContent() {
        var $tip = $$$1(this.getTipElement()); // We use append for html objects to maintain js events

        this.setElementContent($tip.find(Selector.TITLE), this.getTitle());

        var content = this._getContent();

        if (typeof content === 'function') {
          content = content.call(this.element);
        }

        this.setElementContent($tip.find(Selector.CONTENT), content);
        $tip.removeClass(ClassName.FADE + " " + ClassName.SHOW);
      }; // Private


      _proto._getContent = function _getContent() {
        return this.element.getAttribute('data-content') || this.config.content;
      };

      _proto._cleanTipClass = function _cleanTipClass() {
        var $tip = $$$1(this.getTipElement());
        var tabClass = $tip.attr('class').match(BSCLS_PREFIX_REGEX);

        if (tabClass !== null && tabClass.length > 0) {
          $tip.removeClass(tabClass.join(''));
        }
      }; // Static


      Popover._jQueryInterface = function _jQueryInterface(config) {
        return this.each(function () {
          var data = $$$1(this).data(DATA_KEY);

          var _config = typeof config === 'object' ? config : null;

          if (!data && /destroy|hide/.test(config)) {
            return;
          }

          if (!data) {
            data = new Popover(this, _config);
            $$$1(this).data(DATA_KEY, data);
          }

          if (typeof config === 'string') {
            if (typeof data[config] === 'undefined') {
              throw new TypeError("No method named \"" + config + "\"");
            }

            data[config]();
          }
        });
      };

      _createClass(Popover, null, [{
        key: "VERSION",
        // Getters
        get: function get() {
          return VERSION;
        }
      }, {
        key: "Default",
        get: function get() {
          return Default;
        }
      }, {
        key: "NAME",
        get: function get() {
          return NAME;
        }
      }, {
        key: "DATA_KEY",
        get: function get() {
          return DATA_KEY;
        }
      }, {
        key: "Event",
        get: function get() {
          return Event;
        }
      }, {
        key: "EVENT_KEY",
        get: function get() {
          return EVENT_KEY;
        }
      }, {
        key: "DefaultType",
        get: function get() {
          return DefaultType;
        }
      }]);

      return Popover;
    }(Tooltip);
    /**
     * ------------------------------------------------------------------------
     * jQuery
     * ------------------------------------------------------------------------
     */


    $$$1.fn[NAME] = Popover._jQueryInterface;
    $$$1.fn[NAME].Constructor = Popover;

    $$$1.fn[NAME].noConflict = function () {
      $$$1.fn[NAME] = JQUERY_NO_CONFLICT;
      return Popover._jQueryInterface;
    };

    return Popover;
  }($);

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.1.3): scrollspy.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  var ScrollSpy = function ($$$1) {
    /**
     * ------------------------------------------------------------------------
     * Constants
     * ------------------------------------------------------------------------
     */
    var NAME = 'scrollspy';
    var VERSION = '4.1.3';
    var DATA_KEY = 'bs.scrollspy';
    var EVENT_KEY = "." + DATA_KEY;
    var DATA_API_KEY = '.data-api';
    var JQUERY_NO_CONFLICT = $$$1.fn[NAME];
    var Default = {
      offset: 10,
      method: 'auto',
      target: ''
    };
    var DefaultType = {
      offset: 'number',
      method: 'string',
      target: '(string|element)'
    };
    var Event = {
      ACTIVATE: "activate" + EVENT_KEY,
      SCROLL: "scroll" + EVENT_KEY,
      LOAD_DATA_API: "load" + EVENT_KEY + DATA_API_KEY
    };
    var ClassName = {
      DROPDOWN_ITEM: 'dropdown-item',
      DROPDOWN_MENU: 'dropdown-menu',
      ACTIVE: 'active'
    };
    var Selector = {
      DATA_SPY: '[data-spy="scroll"]',
      ACTIVE: '.active',
      NAV_LIST_GROUP: '.nav, .list-group',
      NAV_LINKS: '.nav-link',
      NAV_ITEMS: '.nav-item',
      LIST_ITEMS: '.list-group-item',
      DROPDOWN: '.dropdown',
      DROPDOWN_ITEMS: '.dropdown-item',
      DROPDOWN_TOGGLE: '.dropdown-toggle'
    };
    var OffsetMethod = {
      OFFSET: 'offset',
      POSITION: 'position'
      /**
       * ------------------------------------------------------------------------
       * Class Definition
       * ------------------------------------------------------------------------
       */

    };

    var ScrollSpy =
    /*#__PURE__*/
    function () {
      function ScrollSpy(element, config) {
        var _this = this;

        this._element = element;
        this._scrollElement = element.tagName === 'BODY' ? window : element;
        this._config = this._getConfig(config);
        this._selector = this._config.target + " " + Selector.NAV_LINKS + "," + (this._config.target + " " + Selector.LIST_ITEMS + ",") + (this._config.target + " " + Selector.DROPDOWN_ITEMS);
        this._offsets = [];
        this._targets = [];
        this._activeTarget = null;
        this._scrollHeight = 0;
        $$$1(this._scrollElement).on(Event.SCROLL, function (event) {
          return _this._process(event);
        });
        this.refresh();

        this._process();
      } // Getters


      var _proto = ScrollSpy.prototype;

      // Public
      _proto.refresh = function refresh() {
        var _this2 = this;

        var autoMethod = this._scrollElement === this._scrollElement.window ? OffsetMethod.OFFSET : OffsetMethod.POSITION;
        var offsetMethod = this._config.method === 'auto' ? autoMethod : this._config.method;
        var offsetBase = offsetMethod === OffsetMethod.POSITION ? this._getScrollTop() : 0;
        this._offsets = [];
        this._targets = [];
        this._scrollHeight = this._getScrollHeight();
        var targets = [].slice.call(document.querySelectorAll(this._selector));
        targets.map(function (element) {
          var target;
          var targetSelector = Util.getSelectorFromElement(element);

          if (targetSelector) {
            target = document.querySelector(targetSelector);
          }

          if (target) {
            var targetBCR = target.getBoundingClientRect();

            if (targetBCR.width || targetBCR.height) {
              // TODO (fat): remove sketch reliance on jQuery position/offset
              return [$$$1(target)[offsetMethod]().top + offsetBase, targetSelector];
            }
          }

          return null;
        }).filter(function (item) {
          return item;
        }).sort(function (a, b) {
          return a[0] - b[0];
        }).forEach(function (item) {
          _this2._offsets.push(item[0]);

          _this2._targets.push(item[1]);
        });
      };

      _proto.dispose = function dispose() {
        $$$1.removeData(this._element, DATA_KEY);
        $$$1(this._scrollElement).off(EVENT_KEY);
        this._element = null;
        this._scrollElement = null;
        this._config = null;
        this._selector = null;
        this._offsets = null;
        this._targets = null;
        this._activeTarget = null;
        this._scrollHeight = null;
      }; // Private


      _proto._getConfig = function _getConfig(config) {
        config = _objectSpread({}, Default, typeof config === 'object' && config ? config : {});

        if (typeof config.target !== 'string') {
          var id = $$$1(config.target).attr('id');

          if (!id) {
            id = Util.getUID(NAME);
            $$$1(config.target).attr('id', id);
          }

          config.target = "#" + id;
        }

        Util.typeCheckConfig(NAME, config, DefaultType);
        return config;
      };

      _proto._getScrollTop = function _getScrollTop() {
        return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop;
      };

      _proto._getScrollHeight = function _getScrollHeight() {
        return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight);
      };

      _proto._getOffsetHeight = function _getOffsetHeight() {
        return this._scrollElement === window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height;
      };

      _proto._process = function _process() {
        var scrollTop = this._getScrollTop() + this._config.offset;

        var scrollHeight = this._getScrollHeight();

        var maxScroll = this._config.offset + scrollHeight - this._getOffsetHeight();

        if (this._scrollHeight !== scrollHeight) {
          this.refresh();
        }

        if (scrollTop >= maxScroll) {
          var target = this._targets[this._targets.length - 1];

          if (this._activeTarget !== target) {
            this._activate(target);
          }

          return;
        }

        if (this._activeTarget && scrollTop < this._offsets[0] && this._offsets[0] > 0) {
          this._activeTarget = null;

          this._clear();

          return;
        }

        var offsetLength = this._offsets.length;

        for (var i = offsetLength; i--;) {
          var isActiveTarget = this._activeTarget !== this._targets[i] && scrollTop >= this._offsets[i] && (typeof this._offsets[i + 1] === 'undefined' || scrollTop < this._offsets[i + 1]);

          if (isActiveTarget) {
            this._activate(this._targets[i]);
          }
        }
      };

      _proto._activate = function _activate(target) {
        this._activeTarget = target;

        this._clear();

        var queries = this._selector.split(','); // eslint-disable-next-line arrow-body-style


        queries = queries.map(function (selector) {
          return selector + "[data-target=\"" + target + "\"]," + (selector + "[href=\"" + target + "\"]");
        });
        var $link = $$$1([].slice.call(document.querySelectorAll(queries.join(','))));

        if ($link.hasClass(ClassName.DROPDOWN_ITEM)) {
          $link.closest(Selector.DROPDOWN).find(Selector.DROPDOWN_TOGGLE).addClass(ClassName.ACTIVE);
          $link.addClass(ClassName.ACTIVE);
        } else {
          // Set triggered link as active
          $link.addClass(ClassName.ACTIVE); // Set triggered links parents as active
          // With both <ul> and <nav> markup a parent is the previous sibling of any nav ancestor

          $link.parents(Selector.NAV_LIST_GROUP).prev(Selector.NAV_LINKS + ", " + Selector.LIST_ITEMS).addClass(ClassName.ACTIVE); // Handle special case when .nav-link is inside .nav-item

          $link.parents(Selector.NAV_LIST_GROUP).prev(Selector.NAV_ITEMS).children(Selector.NAV_LINKS).addClass(ClassName.ACTIVE);
        }

        $$$1(this._scrollElement).trigger(Event.ACTIVATE, {
          relatedTarget: target
        });
      };

      _proto._clear = function _clear() {
        var nodes = [].slice.call(document.querySelectorAll(this._selector));
        $$$1(nodes).filter(Selector.ACTIVE).removeClass(ClassName.ACTIVE);
      }; // Static


      ScrollSpy._jQueryInterface = function _jQueryInterface(config) {
        return this.each(function () {
          var data = $$$1(this).data(DATA_KEY);

          var _config = typeof config === 'object' && config;

          if (!data) {
            data = new ScrollSpy(this, _config);
            $$$1(this).data(DATA_KEY, data);
          }

          if (typeof config === 'string') {
            if (typeof data[config] === 'undefined') {
              throw new TypeError("No method named \"" + config + "\"");
            }

            data[config]();
          }
        });
      };

      _createClass(ScrollSpy, null, [{
        key: "VERSION",
        get: function get() {
          return VERSION;
        }
      }, {
        key: "Default",
        get: function get() {
          return Default;
        }
      }]);

      return ScrollSpy;
    }();
    /**
     * ------------------------------------------------------------------------
     * Data Api implementation
     * ------------------------------------------------------------------------
     */


    $$$1(window).on(Event.LOAD_DATA_API, function () {
      var scrollSpys = [].slice.call(document.querySelectorAll(Selector.DATA_SPY));
      var scrollSpysLength = scrollSpys.length;

      for (var i = scrollSpysLength; i--;) {
        var $spy = $$$1(scrollSpys[i]);

        ScrollSpy._jQueryInterface.call($spy, $spy.data());
      }
    });
    /**
     * ------------------------------------------------------------------------
     * jQuery
     * ------------------------------------------------------------------------
     */

    $$$1.fn[NAME] = ScrollSpy._jQueryInterface;
    $$$1.fn[NAME].Constructor = ScrollSpy;

    $$$1.fn[NAME].noConflict = function () {
      $$$1.fn[NAME] = JQUERY_NO_CONFLICT;
      return ScrollSpy._jQueryInterface;
    };

    return ScrollSpy;
  }($);

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.1.3): tab.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  var Tab = function ($$$1) {
    /**
     * ------------------------------------------------------------------------
     * Constants
     * ------------------------------------------------------------------------
     */
    var NAME = 'tab';
    var VERSION = '4.1.3';
    var DATA_KEY = 'bs.tab';
    var EVENT_KEY = "." + DATA_KEY;
    var DATA_API_KEY = '.data-api';
    var JQUERY_NO_CONFLICT = $$$1.fn[NAME];
    var Event = {
      HIDE: "hide" + EVENT_KEY,
      HIDDEN: "hidden" + EVENT_KEY,
      SHOW: "show" + EVENT_KEY,
      SHOWN: "shown" + EVENT_KEY,
      CLICK_DATA_API: "click" + EVENT_KEY + DATA_API_KEY
    };
    var ClassName = {
      DROPDOWN_MENU: 'dropdown-menu',
      ACTIVE: 'active',
      DISABLED: 'disabled',
      FADE: 'fade',
      SHOW: 'show'
    };
    var Selector = {
      DROPDOWN: '.dropdown',
      NAV_LIST_GROUP: '.nav, .list-group',
      ACTIVE: '.active',
      ACTIVE_UL: '> li > .active',
      DATA_TOGGLE: '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]',
      DROPDOWN_TOGGLE: '.dropdown-toggle',
      DROPDOWN_ACTIVE_CHILD: '> .dropdown-menu .active'
      /**
       * ------------------------------------------------------------------------
       * Class Definition
       * ------------------------------------------------------------------------
       */

    };

    var Tab =
    /*#__PURE__*/
    function () {
      function Tab(element) {
        this._element = element;
      } // Getters


      var _proto = Tab.prototype;

      // Public
      _proto.show = function show() {
        var _this = this;

        if (this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && $$$1(this._element).hasClass(ClassName.ACTIVE) || $$$1(this._element).hasClass(ClassName.DISABLED)) {
          return;
        }

        var target;
        var previous;
        var listElement = $$$1(this._element).closest(Selector.NAV_LIST_GROUP)[0];
        var selector = Util.getSelectorFromElement(this._element);

        if (listElement) {
          var itemSelector = listElement.nodeName === 'UL' ? Selector.ACTIVE_UL : Selector.ACTIVE;
          previous = $$$1.makeArray($$$1(listElement).find(itemSelector));
          previous = previous[previous.length - 1];
        }

        var hideEvent = $$$1.Event(Event.HIDE, {
          relatedTarget: this._element
        });
        var showEvent = $$$1.Event(Event.SHOW, {
          relatedTarget: previous
        });

        if (previous) {
          $$$1(previous).trigger(hideEvent);
        }

        $$$1(this._element).trigger(showEvent);

        if (showEvent.isDefaultPrevented() || hideEvent.isDefaultPrevented()) {
          return;
        }

        if (selector) {
          target = document.querySelector(selector);
        }

        this._activate(this._element, listElement);

        var complete = function complete() {
          var hiddenEvent = $$$1.Event(Event.HIDDEN, {
            relatedTarget: _this._element
          });
          var shownEvent = $$$1.Event(Event.SHOWN, {
            relatedTarget: previous
          });
          $$$1(previous).trigger(hiddenEvent);
          $$$1(_this._element).trigger(shownEvent);
        };

        if (target) {
          this._activate(target, target.parentNode, complete);
        } else {
          complete();
        }
      };

      _proto.dispose = function dispose() {
        $$$1.removeData(this._element, DATA_KEY);
        this._element = null;
      }; // Private


      _proto._activate = function _activate(element, container, callback) {
        var _this2 = this;

        var activeElements;

        if (container.nodeName === 'UL') {
          activeElements = $$$1(container).find(Selector.ACTIVE_UL);
        } else {
          activeElements = $$$1(container).children(Selector.ACTIVE);
        }

        var active = activeElements[0];
        var isTransitioning = callback && active && $$$1(active).hasClass(ClassName.FADE);

        var complete = function complete() {
          return _this2._transitionComplete(element, active, callback);
        };

        if (active && isTransitioning) {
          var transitionDuration = Util.getTransitionDurationFromElement(active);
          $$$1(active).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
        } else {
          complete();
        }
      };

      _proto._transitionComplete = function _transitionComplete(element, active, callback) {
        if (active) {
          $$$1(active).removeClass(ClassName.SHOW + " " + ClassName.ACTIVE);
          var dropdownChild = $$$1(active.parentNode).find(Selector.DROPDOWN_ACTIVE_CHILD)[0];

          if (dropdownChild) {
            $$$1(dropdownChild).removeClass(ClassName.ACTIVE);
          }

          if (active.getAttribute('role') === 'tab') {
            active.setAttribute('aria-selected', false);
          }
        }

        $$$1(element).addClass(ClassName.ACTIVE);

        if (element.getAttribute('role') === 'tab') {
          element.setAttribute('aria-selected', true);
        }

        Util.reflow(element);
        $$$1(element).addClass(ClassName.SHOW);

        if (element.parentNode && $$$1(element.parentNode).hasClass(ClassName.DROPDOWN_MENU)) {
          var dropdownElement = $$$1(element).closest(Selector.DROPDOWN)[0];

          if (dropdownElement) {
            var dropdownToggleList = [].slice.call(dropdownElement.querySelectorAll(Selector.DROPDOWN_TOGGLE));
            $$$1(dropdownToggleList).addClass(ClassName.ACTIVE);
          }

          element.setAttribute('aria-expanded', true);
        }

        if (callback) {
          callback();
        }
      }; // Static


      Tab._jQueryInterface = function _jQueryInterface(config) {
        return this.each(function () {
          var $this = $$$1(this);
          var data = $this.data(DATA_KEY);

          if (!data) {
            data = new Tab(this);
            $this.data(DATA_KEY, data);
          }

          if (typeof config === 'string') {
            if (typeof data[config] === 'undefined') {
              throw new TypeError("No method named \"" + config + "\"");
            }

            data[config]();
          }
        });
      };

      _createClass(Tab, null, [{
        key: "VERSION",
        get: function get() {
          return VERSION;
        }
      }]);

      return Tab;
    }();
    /**
     * ------------------------------------------------------------------------
     * Data Api implementation
     * ------------------------------------------------------------------------
     */


    $$$1(document).on(Event.CLICK_DATA_API, Selector.DATA_TOGGLE, function (event) {
      event.preventDefault();

      Tab._jQueryInterface.call($$$1(this), 'show');
    });
    /**
     * ------------------------------------------------------------------------
     * jQuery
     * ------------------------------------------------------------------------
     */

    $$$1.fn[NAME] = Tab._jQueryInterface;
    $$$1.fn[NAME].Constructor = Tab;

    $$$1.fn[NAME].noConflict = function () {
      $$$1.fn[NAME] = JQUERY_NO_CONFLICT;
      return Tab._jQueryInterface;
    };

    return Tab;
  }($);

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.1.3): index.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  (function ($$$1) {
    if (typeof $$$1 === 'undefined') {
      throw new TypeError('Bootstrap\'s JavaScript requires jQuery. jQuery must be included before Bootstrap\'s JavaScript.');
    }

    var version = $$$1.fn.jquery.split(' ')[0].split('.');
    var minMajor = 1;
    var ltMajor = 2;
    var minMinor = 9;
    var minPatch = 1;
    var maxMajor = 4;

    if (version[0] < ltMajor && version[1] < minMinor || version[0] === minMajor && version[1] === minMinor && version[2] < minPatch || version[0] >= maxMajor) {
      throw new Error('Bootstrap\'s JavaScript requires at least jQuery v1.9.1 but less than v4.0.0');
    }
  })($);

  exports.Util = Util;
  exports.Alert = Alert;
  exports.Button = Button;
  exports.Carousel = Carousel;
  exports.Collapse = Collapse;
  exports.Dropdown = Dropdown;
  exports.Modal = Modal;
  exports.Popover = Popover;
  exports.Scrollspy = ScrollSpy;
  exports.Tab = Tab;
  exports.Tooltip = Tooltip;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=bootstrap.bundle.js.map

/*
Template Name: Color Admin - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Version: 4.2.0
Author: Sean Ngu
Website: http://www.seantheme.com/color-admin-v4.2/admin/
*/

var FONT_COLOR  = '#616161';
var FONT_FAMILY = 'Roboto,"Helvetica Neue",Helvetica,Arial,sans-serif';
var FONT_WEIGHT	= '600';
var FONT_SIZE   = '12px';

var COLOR_BLUE               = '#2196F3';
var COLOR_BLUE_LIGHTER       = '#64B5F6';
var COLOR_BLUE_DARKER        = '#1976D2';
var COLOR_BLUE_TRANSPARENT_1 = 'rgba(33, 150, 243, 0.1)';
var COLOR_BLUE_TRANSPARENT_2 = 'rgba(33, 150, 243, 0.2)';
var COLOR_BLUE_TRANSPARENT_3 = 'rgba(33, 150, 243, 0.3)';
var COLOR_BLUE_TRANSPARENT_4 = 'rgba(33, 150, 243, 0.4)';
var COLOR_BLUE_TRANSPARENT_5 = 'rgba(33, 150, 243, 0.5)';
var COLOR_BLUE_TRANSPARENT_6 = 'rgba(33, 150, 243, 0.6)';
var COLOR_BLUE_TRANSPARENT_7 = 'rgba(33, 150, 243, 0.7)';
var COLOR_BLUE_TRANSPARENT_8 = 'rgba(33, 150, 243, 0.8)';
var COLOR_BLUE_TRANSPARENT_9 = 'rgba(33, 150, 243, 0.9)';

var COLOR_AQUA               = '#00BCD4';
var COLOR_AQUA_LIGHTER       = '#4DD0E1';
var COLOR_AQUA_DARKER        = '#0097A7';
var COLOR_AQUA_TRANSPARENT_1 = 'rgba(0, 188, 212, 0.1)';
var COLOR_AQUA_TRANSPARENT_2 = 'rgba(0, 188, 212, 0.2)';
var COLOR_AQUA_TRANSPARENT_3 = 'rgba(0, 188, 212, 0.3)';
var COLOR_AQUA_TRANSPARENT_4 = 'rgba(0, 188, 212, 0.4)';
var COLOR_AQUA_TRANSPARENT_5 = 'rgba(0, 188, 212, 0.5)';
var COLOR_AQUA_TRANSPARENT_6 = 'rgba(0, 188, 212, 0.6)';
var COLOR_AQUA_TRANSPARENT_7 = 'rgba(0, 188, 212, 0.7)';
var COLOR_AQUA_TRANSPARENT_8 = 'rgba(0, 188, 212, 0.8)';
var COLOR_AQUA_TRANSPARENT_9 = 'rgba(0, 188, 212, 0.9)';
	
var COLOR_GREEN	              = '#009688';
var COLOR_GREEN_LIGHTER	      = '#4DB6AC';
var COLOR_GREEN_DARKER	      = '#00796B';
var COLOR_GREEN_TRANSPARENT_1 = 'rgba(0, 150, 136, 0.1)';
var COLOR_GREEN_TRANSPARENT_2 = 'rgba(0, 150, 136, 0.2)';
var COLOR_GREEN_TRANSPARENT_3 = 'rgba(0, 150, 136, 0.3)';
var COLOR_GREEN_TRANSPARENT_4 = 'rgba(0, 150, 136, 0.4)';
var COLOR_GREEN_TRANSPARENT_5 = 'rgba(0, 150, 136, 0.5)';
var COLOR_GREEN_TRANSPARENT_6 = 'rgba(0, 150, 136, 0.6)';
var COLOR_GREEN_TRANSPARENT_7 = 'rgba(0, 150, 136, 0.7)';
var COLOR_GREEN_TRANSPARENT_8 = 'rgba(0, 150, 136, 0.8)';
var COLOR_GREEN_TRANSPARENT_9 = 'rgba(0, 150, 136, 0.9)';

var COLOR_YELLOW               = '#FFEB3B';
var COLOR_YELLOW_LIGHTER       = '#FFF176';
var COLOR_YELLOW_DARKER        = '#FBC02D';
var COLOR_YELLOW_TRANSPARENT_1 = 'rgba(255, 235, 59, 0.1)';
var COLOR_YELLOW_TRANSPARENT_2 = 'rgba(255, 235, 59, 0.2)';
var COLOR_YELLOW_TRANSPARENT_3 = 'rgba(255, 235, 59, 0.3)';
var COLOR_YELLOW_TRANSPARENT_4 = 'rgba(255, 235, 59, 0.4)';
var COLOR_YELLOW_TRANSPARENT_5 = 'rgba(255, 235, 59, 0.5)';
var COLOR_YELLOW_TRANSPARENT_6 = 'rgba(255, 235, 59, 0.6)';
var COLOR_YELLOW_TRANSPARENT_7 = 'rgba(255, 235, 59, 0.7)';
var COLOR_YELLOW_TRANSPARENT_8 = 'rgba(255, 235, 59, 0.8)';
var COLOR_YELLOW_TRANSPARENT_9 = 'rgba(255, 235, 59, 0.9)';
	
var COLOR_ORANGE               = '#FF9800';
var COLOR_ORANGE_LIGHTER       = '#FFB74D';
var COLOR_ORANGE_DARKER        = '#F57C00';
var COLOR_ORANGE_TRANSPARENT_1 = 'rgba(255, 152, 0, 0.1)';
var COLOR_ORANGE_TRANSPARENT_2 = 'rgba(255, 152, 0, 0.2)';
var COLOR_ORANGE_TRANSPARENT_3 = 'rgba(255, 152, 0, 0.3)';
var COLOR_ORANGE_TRANSPARENT_4 = 'rgba(255, 152, 0, 0.4)';
var COLOR_ORANGE_TRANSPARENT_5 = 'rgba(255, 152, 0, 0.5)';
var COLOR_ORANGE_TRANSPARENT_6 = 'rgba(255, 152, 0, 0.6)';
var COLOR_ORANGE_TRANSPARENT_7 = 'rgba(255, 152, 0, 0.7)';
var COLOR_ORANGE_TRANSPARENT_8 = 'rgba(255, 152, 0, 0.8)';
var COLOR_ORANGE_TRANSPARENT_9 = 'rgba(255, 152, 0, 0.9)';
	
var COLOR_PURPLE               = '#673AB7';
var COLOR_PURPLE_LIGHTER       = '#9575CD';
var COLOR_PURPLE_DARKER        = '#673AB7';
var COLOR_PURPLE_TRANSPARENT_1 = 'rgba(103, 58, 183, 0.1)';
var COLOR_PURPLE_TRANSPARENT_2 = 'rgba(103, 58, 183, 0.2)';
var COLOR_PURPLE_TRANSPARENT_3 = 'rgba(103, 58, 183, 0.3)';
var COLOR_PURPLE_TRANSPARENT_4 = 'rgba(103, 58, 183, 0.4)';
var COLOR_PURPLE_TRANSPARENT_5 = 'rgba(103, 58, 183, 0.5)';
var COLOR_PURPLE_TRANSPARENT_6 = 'rgba(103, 58, 183, 0.6)';
var COLOR_PURPLE_TRANSPARENT_7 = 'rgba(103, 58, 183, 0.7)';
var COLOR_PURPLE_TRANSPARENT_8 = 'rgba(103, 58, 183, 0.8)';
var COLOR_PURPLE_TRANSPARENT_9 = 'rgba(103, 58, 183, 0.9)';

var COLOR_RED               = '#F44336';
var COLOR_RED_LIGHTER       = '#E57373';
var COLOR_RED_DARKER        = '#D32F2F';
var COLOR_RED_TRANSPARENT_1 = 'rgba(244, 67, 54, 0.1)';
var COLOR_RED_TRANSPARENT_2 = 'rgba(244, 67, 54, 0.2)';
var COLOR_RED_TRANSPARENT_3 = 'rgba(244, 67, 54, 0.3)';
var COLOR_RED_TRANSPARENT_4 = 'rgba(244, 67, 54, 0.4)';
var COLOR_RED_TRANSPARENT_5 = 'rgba(244, 67, 54, 0.5)';
var COLOR_RED_TRANSPARENT_6 = 'rgba(244, 67, 54, 0.6)';
var COLOR_RED_TRANSPARENT_7 = 'rgba(244, 67, 54, 0.7)';
var COLOR_RED_TRANSPARENT_8 = 'rgba(244, 67, 54, 0.8)';
var COLOR_RED_TRANSPARENT_9 = 'rgba(244, 67, 54, 0.9)';
	
var COLOR_GREY               = '#9E9E9E';
var COLOR_GREY_LIGHTER       = '#E0E0E0';
var COLOR_GREY_DARKER        = '#616161';
var COLOR_GREY_TRANSPARENT_1 = 'rgba(158, 158, 158, 0.1)';
var COLOR_GREY_TRANSPARENT_2 = 'rgba(158, 158, 158, 0.2)';
var COLOR_GREY_TRANSPARENT_3 = 'rgba(158, 158, 158, 0.3)';
var COLOR_GREY_TRANSPARENT_4 = 'rgba(158, 158, 158, 0.4)';
var COLOR_GREY_TRANSPARENT_5 = 'rgba(158, 158, 158, 0.5)';
var COLOR_GREY_TRANSPARENT_6 = 'rgba(158, 158, 158, 0.6)';
var COLOR_GREY_TRANSPARENT_7 = 'rgba(158, 158, 158, 0.7)';
var COLOR_GREY_TRANSPARENT_8 = 'rgba(158, 158, 158, 0.8)';
var COLOR_GREY_TRANSPARENT_9 = 'rgba(158, 158, 158, 0.9)';
	
var COLOR_SILVER               = '#EEEEEE';
var COLOR_SILVER_LIGHTER       = '#FAFAFA';
var COLOR_SILVER_DARKER        = '#9E9E9E';
var COLOR_SILVER_TRANSPARENT_1 = 'rgba(238, 238, 238, 0.1)';
var COLOR_SILVER_TRANSPARENT_2 = 'rgba(238, 238, 238, 0.2)';
var COLOR_SILVER_TRANSPARENT_3 = 'rgba(238, 238, 238, 0.3)';
var COLOR_SILVER_TRANSPARENT_4 = 'rgba(238, 238, 238, 0.4)';
var COLOR_SILVER_TRANSPARENT_5 = 'rgba(238, 238, 238, 0.5)';
var COLOR_SILVER_TRANSPARENT_6 = 'rgba(238, 238, 238, 0.6)';
var COLOR_SILVER_TRANSPARENT_7 = 'rgba(238, 238, 238, 0.7)';
var COLOR_SILVER_TRANSPARENT_8 = 'rgba(238, 238, 238, 0.8)';
var COLOR_SILVER_TRANSPARENT_9 = 'rgba(238, 238, 238, 0.9)';

var COLOR_BLACK               = '#212121';
var COLOR_BLACK_LIGHTER       = '#616161';
var COLOR_BLACK_DARKER	      = '#000';
var COLOR_BLACK_TRANSPARENT_1 = 'rgba(33, 33, 33, 0.1)';
var COLOR_BLACK_TRANSPARENT_2 = 'rgba(33, 33, 33, 0.2)';
var COLOR_BLACK_TRANSPARENT_3 = 'rgba(33, 33, 33, 0.3)';
var COLOR_BLACK_TRANSPARENT_4 = 'rgba(33, 33, 33, 0.4)';
var COLOR_BLACK_TRANSPARENT_5 = 'rgba(33, 33, 33, 0.5)';
var COLOR_BLACK_TRANSPARENT_6 = 'rgba(33, 33, 33, 0.6)';
var COLOR_BLACK_TRANSPARENT_7 = 'rgba(33, 33, 33, 0.7)';
var COLOR_BLACK_TRANSPARENT_8 = 'rgba(33, 33, 33, 0.8)';
var COLOR_BLACK_TRANSPARENT_9 = 'rgba(33, 33, 33, 0.9)';

var COLOR_WHITE                = '#FFFFFF';
var COLOR_WHITE_TRANSPARENT_1  = 'rgba(255, 255, 255, 0.1)';
var COLOR_WHITE_TRANSPARENT_2  = 'rgba(255, 255, 255, 0.2)';
var COLOR_WHITE_TRANSPARENT_3  = 'rgba(255, 255, 255, 0.3)';
var COLOR_WHITE_TRANSPARENT_4  = 'rgba(255, 255, 255, 0.4)';
var COLOR_WHITE_TRANSPARENT_5  = 'rgba(255, 255, 255, 0.5)';
var COLOR_WHITE_TRANSPARENT_6  = 'rgba(255, 255, 255, 0.6)';
var COLOR_WHITE_TRANSPARENT_7  = 'rgba(255, 255, 255, 0.7)';
var COLOR_WHITE_TRANSPARENT_8  = 'rgba(255, 255, 255, 0.8)';
var COLOR_WHITE_TRANSPARENT_9  = 'rgba(255, 255, 255, 0.9)';
/*
Template Name: Color Admin - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Version: 4.2.0
Author: Sean Ngu
Website: http://www.seantheme.com/color-admin-v4.2/admin/
	----------------------------
	APPS CONTENT TABLE
	----------------------------

	<!-- ======== GLOBAL SCRIPT SETTING ======== -->
	01. Handle Scrollbar
	02. Handle Sidebar - Menu
	03. Handle Sidebar - Mobile View Toggle
	04. Handle Sidebar - Minify / Expand
	05. Handle Page Load - Fade in
	06. Handle Panel - Remove / Reload / Collapse / Expand
	07. Handle Panel - Draggable
	08. Handle Tooltip & Popover Activation
	09. Handle Scroll to Top Button Activation

	<!-- ======== Added in V1.2 ======== -->
	10. Handle Theme & Page Structure Configuration - added in V1.2
	11. Handle Theme Panel Expand - added in V1.2
	12. Handle After Page Load Add Class Function - added in V1.2

	<!-- ======== Added in V1.5 ======== -->
	13. Handle Save Panel Position Function - added in V1.5
	14. Handle Draggable Panel Local Storage Function - added in V1.5
	15. Handle Reset Local Storage - added in V1.5

	<!-- ======== Added in V1.6 ======== -->
	16. Handle IE Full Height Page Compatibility - added in V1.6
	17. Handle Unlimited Nav Tabs - added in V1.6

	<!-- ======== Added in V1.9 ======== -->
	18. Handle Top Menu - Unlimited Top Menu Render - added in V1.9
	19. Handle Top Menu - Sub Menu Toggle - added in V1.9
	20. Handle Top Menu - Mobile Sub Menu Toggle - added in V1.9
	21. Handle Top Menu - Mobile Top Menu Toggle - added in V1.9
	22. Handle Clear Sidebar Selection & Hide Mobile Menu - added in V1.9

	<!-- ======== Added in V4.0 ======== -->
	23. Handle Check Bootstrap Version - added in V4.0
	24. Handle Page Scroll Class - added in V4.0
	25. Handle Toggle Navbar Profile - added in V4.0
	26. Handle Sidebar Scroll Memory - added in V4.0
	27. Handle Sidebar Minify Sub Menu - added in V4.0
	28. Handle Ajax Mode - added in V4.0
	29. Handle Float Navbar Search - added in V4.0

	<!-- ======== APPLICATION SETTING ======== -->
	Application Controller
*/



/* 01. Handle Scrollbar
------------------------------------------------ */
var handleSlimScroll = function() {
	"use strict";
	$.when($('[data-scrollbar=true]').each( function() {
		generateSlimScroll($(this));
	})).done(function() {
		$('[data-scrollbar="true"]').mouseover();
	});
};
var generateSlimScroll = function(element) {
	if ($(element).attr('data-init')) {
		return;
	}
	var dataHeight = $(element).attr('data-height');
	    dataHeight = (!dataHeight) ? $(element).height() : dataHeight;

	var scrollBarOption = {
		height: dataHeight
	};
	if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		$(element).css('height', dataHeight);
		$(element).css('overflow-x','scroll');
	} else {
		$(element).slimScroll(scrollBarOption);
	}
	$(element).attr('data-init', true);
	$('.slimScrollBar').hide();
};


/* 02. Handle Sidebar - Menu
------------------------------------------------ */
var handleSidebarMenu = function() {
    "use strict";
    
	var expandTime = ($('.sidebar').attr('data-disable-slide-animation')) ? 0 : 250;
	$('.sidebar .nav > .has-sub > a').click(function() {
		var target = $(this).next('.sub-menu');
		var otherMenu = $('.sidebar .nav > li.has-sub > .sub-menu').not(target);

		if ($('.page-sidebar-minified').length === 0) {
			$(otherMenu).closest('li').addClass('closing');
			$(otherMenu).slideUp(expandTime, function() {
				$(otherMenu).closest('li').addClass('closed').removeClass('expand closing');
			});
			if ($(target).is(':visible')) {
				$(target).closest('li').addClass('closing').removeClass('expand');
			} else {
				$(target).closest('li').addClass('expanding').removeClass('closed');
			}
			$(target).slideToggle(expandTime, function() {
				var targetLi = $(this).closest('li');
				if (!$(target).is(':visible')) {
					$(targetLi).addClass('closed');
					$(targetLi).removeClass('expand');
				} else {
					$(targetLi).addClass('expand');
					$(targetLi).removeClass('closed');
				}
				$(targetLi).removeClass('expanding closing');
			});
		}
	});
	$('.sidebar .nav > .has-sub .sub-menu li.has-sub > a').click(function() {
		if ($('.page-sidebar-minified').length === 0) {
			var target = $(this).next('.sub-menu');
			if ($(target).is(':visible')) {
				$(target).closest('li').addClass('closing').removeClass('expand');
			} else {
				$(target).closest('li').addClass('expanding').removeClass('closed');
			}
			$(target).slideToggle(expandTime, function() {
				var targetLi = $(this).closest('li');
				if (!$(target).is(':visible')) {
					$(targetLi).addClass('closed');
					$(targetLi).removeClass('expand');
				} else {
					$(targetLi).addClass('expand');
					$(targetLi).removeClass('closed');
				}
				$(targetLi).removeClass('expanding closing');
			});
		}
	});
};


/* 03. Handle Sidebar - Mobile View Toggle
------------------------------------------------ */
var handleMobileSidebarToggle = function() {
	var sidebarProgress = false;
	
	$('.sidebar').bind('click touchstart', function(e) {
		if ($(e.target).closest('.sidebar').length !== 0) {
			sidebarProgress = true;
		} else {
			sidebarProgress = false;
			e.stopPropagation();
		}
	});

	$(document).bind('click touchstart', function(e) {
		if ($(e.target).closest('.sidebar').length === 0) {
			sidebarProgress = false;
		}
		if ($(e.target).closest('#float-sub-menu').length !== 0) {
			sidebarProgress = true;
		}

		if (!e.isPropagationStopped() && sidebarProgress !== true) {
			if ($('#page-container').hasClass('page-sidebar-toggled')) {
				sidebarProgress = true;
				$('#page-container').removeClass('page-sidebar-toggled');
			}
			if ($(window).width() <= 767) {
				if ($('#page-container').hasClass('page-right-sidebar-toggled')) {
					sidebarProgress = true;
					$('#page-container').removeClass('page-right-sidebar-toggled');
				}
			}
		}
	});
    
	$('[data-click=right-sidebar-toggled]').click(function(e) {
		e.stopPropagation();
		var targetContainer = '#page-container';
		var targetClass = 'page-right-sidebar-collapsed';
		targetClass = ($(window).width() < 979) ? 'page-right-sidebar-toggled' : targetClass;
		if ($(targetContainer).hasClass(targetClass)) {
			$(targetContainer).removeClass(targetClass);
		} else if (sidebarProgress !== true) {
			$(targetContainer).addClass(targetClass);
		} else {
			sidebarProgress = false;
		}
		if ($(window).width() < 480) {
			$('#page-container').removeClass('page-sidebar-toggled');
		}
		$(window).trigger('resize');
	});
    
	$('[data-click=sidebar-toggled]').click(function(e) {
		e.stopPropagation();
		var sidebarClass = 'page-sidebar-toggled';
		var targetContainer = '#page-container';

		if ($(targetContainer).hasClass(sidebarClass)) {
			$(targetContainer).removeClass(sidebarClass);
		} else if (sidebarProgress !== true) {
			$(targetContainer).addClass(sidebarClass);
		} else {
			sidebarProgress = false;
		}
		if ($(window).width() < 480) {
			$('#page-container').removeClass('page-right-sidebar-toggled');
		}
	});
};


/* 04. Handle Sidebar - Minify / Expand
------------------------------------------------ */
var handleSidebarMinify = function() {
	$(document).on('click', '[data-click=sidebar-minify]', function(e) {
		e.preventDefault();
		var sidebarClass = 'page-sidebar-minified';
		var targetContainer = '#page-container';

		if ($(targetContainer).hasClass(sidebarClass)) {
			$(targetContainer).removeClass(sidebarClass);
		} else {
			$(targetContainer).addClass(sidebarClass);

			if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
				$('#sidebar [data-scrollbar="true"]').css('margin-top','0');
				$('#sidebar [data-scrollbar="true"]').css('overflow-x', 'scroll');
			}
		}
		$(window).trigger('resize');
	});
};


/* 05. Handle Page Load - Fade in
------------------------------------------------ */
var handlePageContentView = function() {
	"use strict";

	var hideClass = '';
	var showClass = '';
	var removeClass = '';
	var bootstrapVersion = handleCheckBootstrapVersion();

	if (bootstrapVersion >= 3 && bootstrapVersion < 4) {
		hideClass = 'hide';
		showClass = 'in';
	} else if (bootstrapVersion >= 4 && bootstrapVersion < 5) {
		hideClass = 'd-none';
		showClass = 'show';
	}
	$(window).on('load', function() {
		$.when($('#page-loader').addClass(hideClass)).done(function() {
			$('#page-container').addClass(showClass);
		});
	});
};


/* 06. Handle Panel - Remove / Reload / Collapse / Expand
------------------------------------------------ */
var panelActionRunning = false;
var handlePanelAction = function() {
	"use strict";

	if (panelActionRunning) {
		return false;
	}
	panelActionRunning = true;

	// remove
	$(document).on('hover', '[data-click=panel-remove]', function(e) {
		if (!$(this).attr('data-init')) {
			$(this).tooltip({
				title: 'Remove',
				placement: 'bottom',
				trigger: 'hover',
				container: 'body'
			});
			$(this).tooltip('show');
			$(this).attr('data-init', true);
		}
	});
	$(document).on('click', '[data-click=panel-remove]', function(e) {
		e.preventDefault();
		var bootstrapVersion = handleCheckBootstrapVersion();

		if (bootstrapVersion >= 4 && bootstrapVersion < 5) {
			$(this).tooltip('dispose');
		} else {
			$(this).tooltip('destroy');
		}
		$(this).closest('.panel').remove();
	});

	// collapse
	$(document).on('hover', '[data-click=panel-collapse]', function(e) {
		if (!$(this).attr('data-init')) {
			$(this).tooltip({
				title: 'Collapse / Expand',
				placement: 'bottom',
				trigger: 'hover',
				container: 'body'
			});
			$(this).tooltip('show');
			$(this).attr('data-init', true);
		}
	});
	$(document).on('click', '[data-click=panel-collapse]', function(e) {
		e.preventDefault();
		$(this).closest('.panel').find('.panel-body').slideToggle();
	});

	// reload
	$(document).on('hover', '[data-click=panel-reload]', function(e) {
		if (!$(this).attr('data-init')) {
			$(this).tooltip({
				title: 'Reload',
				placement: 'bottom',
				trigger: 'hover',
				container: 'body'
			});
			$(this).tooltip('show');
			$(this).attr('data-init', true);
		}
	});
	$(document).on('click', '[data-click=panel-reload]', function(e) {
		e.preventDefault();
		var target = $(this).closest('.panel');
		if (!$(target).hasClass('panel-loading')) {
			var targetBody = $(target).find('.panel-body');
			var spinnerHtml = '<div class="panel-loader"><span class="spinner-small"></span></div>';
			$(target).addClass('panel-loading');
			$(targetBody).prepend(spinnerHtml);
			setTimeout(function() {
				$(target).removeClass('panel-loading');
				$(target).find('.panel-loader').remove();
			}, 2000);
		}
	});

	// expand
	$(document).on('hover', '[data-click=panel-expand]', function(e) {
		if (!$(this).attr('data-init')) {
			$(this).tooltip({
				title: 'Expand / Compress',
				placement: 'bottom',
				trigger: 'hover',
				container: 'body'
			});
			$(this).tooltip('show');
			$(this).attr('data-init', true);
		}
	});
	$(document).on('click', '[data-click=panel-expand]', function(e) {
		e.preventDefault();
		var target = $(this).closest('.panel');
		var targetBody = $(target).find('.panel-body');
		var targetTop = 40;
		if ($(targetBody).length !== 0) {
			var targetOffsetTop = $(target).offset().top;
			var targetBodyOffsetTop = $(targetBody).offset().top;
			targetTop = targetBodyOffsetTop - targetOffsetTop;
		}

		if ($('body').hasClass('panel-expand') && $(target).hasClass('panel-expand')) {
			$('body, .panel').removeClass('panel-expand');
			$('.panel').removeAttr('style');
			$(targetBody).removeAttr('style');
		} else {
			$('body').addClass('panel-expand');
			$(this).closest('.panel').addClass('panel-expand');

			if ($(targetBody).length !== 0 && targetTop != 40) {
				var finalHeight = 40;
				$(target).find(' > *').each(function() {
					var targetClass = $(this).attr('class');

					if (targetClass != 'panel-heading' && targetClass != 'panel-body') {
						finalHeight += $(this).height() + 30;
					}
				});
				if (finalHeight != 40) {
					$(targetBody).css('top', finalHeight + 'px');
				}
			}
		}
		$(window).trigger('resize');
	});
};


/* 07. Handle Panel - Draggable
------------------------------------------------ */
var handleDraggablePanel = function() {
	"use strict";
	var target = $('.panel:not([data-sortable="false"])').parent('[class*=col]');
	var targetHandle = '.panel-heading';
	var connectedTarget = '.row > [class*=col]';

	$(target).sortable({
		handle: targetHandle,
		connectWith: connectedTarget,
		stop: function(event, ui) {
			ui.item.find('.panel-title').append('<i class="fa fa-refresh fa-spin m-l-5" data-id="title-spinner"></i>');
			handleSavePanelPosition(ui.item);
		}
	});
};


/* 08. Handle Tooltip & Popover Activation
------------------------------------------------ */
var handelTooltipPopoverActivation = function() {
	"use strict";
	if ($('[data-toggle="tooltip"]').length !== 0) {
		$('[data-toggle=tooltip]').tooltip();
	}
	if ($('[data-toggle="popover"]').length !== 0) {
		$('[data-toggle=popover]').popover();
	}
};


/* 09. Handle Scroll to Top Button Activation
------------------------------------------------ */
var handleScrollToTopButton = function() {
	"use strict";
	var bootstrapVersion = handleCheckBootstrapVersion();
	var showClass = '';

	if (bootstrapVersion >= 3 && bootstrapVersion < 4) {
		showClass = 'in';
	} else if (bootstrapVersion >= 4 && bootstrapVersion < 5) {
		showClass = 'show';
	}
	$(document).scroll( function() {
		var totalScroll = $(document).scrollTop();

		if (totalScroll >= 200) {
			$('[data-click=scroll-top]').addClass(showClass);
		} else {
			$('[data-click=scroll-top]').removeClass(showClass);
		}
	});

	$('[data-click=scroll-top]').click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $("body").offset().top
		}, 500);
	});
};


/* 10. Handle Theme & Page Structure Configuration - added in V1.2
------------------------------------------------ */
var handleThemePageStructureControl = function() {

	// THEME - theme selection
	$(document).on('click', '.theme-list [data-theme]', function() {
		var cssFileSrc = $(this).attr('data-theme-file');
		$('#theme').attr('href', cssFileSrc);
		$('.theme-list [data-theme]').not(this).closest('li').removeClass('active');
		$(this).closest('li').addClass('active');
		Cookies.set('theme', $(this).attr('data-theme'));
	});

	// COOKIE - theme selection
	if (Cookies && Cookies.get('theme')) {
		if ($('.theme-list [data-theme]').length !== 0) {
			$('.theme-list [data-theme="'+ Cookies.get('theme') +'"]').trigger('click');
		}
	}

	// HEADER - header styling selection
	$(document).on('change', '.theme-panel [name=header-styling]', function() {
		var targetClassAdd = ($(this).val() == 1) ? 'navbar-default' : 'navbar-inverse';
		var targetClassRemove = ($(this).val() == 1) ? 'navbar-inverse' : 'navbar-default';
		$('#header').removeClass(targetClassRemove).addClass(targetClassAdd);
		Cookies.set('header-styling', $(this).val());
	});

	// COOKIE - header styling selection
	if (Cookies && Cookies.get('header-styling')) {
		var targetElm = '.theme-panel [name="header-styling"]';
		if ($(targetElm).length !== 0) {
			$(targetElm + ' option[value="'+ Cookies.get('header-styling') +'"]').prop('selected', true);
			$(targetElm).trigger('change');
		}
	}

	// SIDEBAR - sidebar styling selection
	$(document).on('change', '.theme-panel [name=sidebar-styling]', function() {
		if ($(this).val() == 2) {
			$('#sidebar').addClass('sidebar-grid');
			Cookies.set('sidebar-styling', $(this).val());
		} else {
			$('#sidebar').removeClass('sidebar-grid');
			Cookies.set('sidebar-styling', $(this).val());
		}
	});

	// COOKIE - sidebar styling selection
	if (Cookies && Cookies.get('sidebar-styling')) {
		var targetElm = '.theme-panel [name="sidebar-styling"]';
		if ($(targetElm).length !== 0) {
			$(targetElm + ' option[value="'+ Cookies.get('sidebar-styling') +'"]').prop('selected', true);
			$(targetElm).trigger('change');
		}
	}

	// CONTENT - content gradient selection
	$(document).on('change', '.theme-panel [name=content-gradient]', function() {
		if ($(this).val() == 2) {
			$('#page-container').addClass('gradient-enabled');
			Cookies.set('content-gradient', $(this).val());
		} else {
			$('#page-container').removeClass('gradient-enabled');
			Cookies.set('content-gradient', $(this).val());
		}
	});

	// COOKIE - content gradient selection
	if (Cookies && Cookies.get('content-gradient')) {
		var targetElm = '.theme-panel [name="content-gradient"]';
		if ($(targetElm).length !== 0) {
			$(targetElm + ' option[value="'+ Cookies.get('content-gradient') +'"]').prop('selected', true);
			$(targetElm).trigger('change');
		}
	}

	// CONTENT - content styling selection
	$(document).on('change', '.theme-panel [name=content-styling]', function() {
		if ($(this).val() == 2) {
			$('body').addClass('flat-black');
			Cookies.set('content-styling', $(this).val());
		} else {
			$('body').removeClass('flat-black');
			Cookies.set('content-styling', $(this).val());
		}
	});

	// COOKIE - content styling selection
	if (Cookies && Cookies.get('content-styling')) {
		var targetElm = '.theme-panel [name="content-styling"]';
		if ($(targetElm).length !== 0) {
			$(targetElm + ' option[value="'+ Cookies.get('content-styling') +'"]').prop('selected', true);
			$(targetElm).trigger('change');
		}
	}

	// DIRECTION - direction selection
	$(document).on('change', '.theme-panel [name=direction]', function() {
		if ($(this).val() == 2) {
			$('body').addClass('rtl-mode');
			Cookies.set('direction', $(this).val());
		} else {
			$('body').removeClass('rtl-mode');
			Cookies.set('direction', $(this).val());
		}
	});

	// COOKIE - direction selection
	if (Cookies && Cookies.get('direction')) {
		var targetElm = '.theme-panel [name="direction"]';
		if ($(targetElm).length !== 0) {
			$(targetElm + ' option[value="'+ Cookies.get('direction') +'"]').prop('selected', true);
			$(targetElm).trigger('change');
		}
	}
  
  // SIDEBAR - sidebar fixed selection
	$(document).on('change', '.theme-panel [name=sidebar-fixed]', function() {
		if ($(this).val() == 1) {
			if ($('.theme-panel [name=header-fixed]').val() == 2) {
				alert('Default Header with Fixed Sidebar option is not supported. Proceed with Fixed Header with Fixed Sidebar.');
				$('.theme-panel [name=header-fixed] option[value="1"]').prop('selected', true);
				$('#page-container').addClass('page-header-fixed');
			}
			$('#page-container').addClass('page-sidebar-fixed');
			if (!$('#page-container').hasClass('page-sidebar-minified')) {
				generateSlimScroll($('.sidebar [data-scrollbar="true"]'));
			}
		} else {
			$('#page-container').removeClass('page-sidebar-fixed');
			if ($('.sidebar .slimScrollDiv').length !== 0) {
				if ($(window).width() <= 979) {
					$('.sidebar').each(function() {
						if (!($('#page-container').hasClass('page-with-two-sidebar') && $(this).hasClass('sidebar-right'))) {
							$(this).find('.slimScrollBar').remove();
							$(this).find('.slimScrollRail').remove();
							$(this).find('[data-scrollbar="true"]').removeAttr('style');
							var targetElement = $(this).find('[data-scrollbar="true"]').parent();
							var targetHtml = $(targetElement).html();
							$(targetElement).replaceWith(targetHtml);
						}
					});
				} else if ($(window).width() > 979) {
					$('.sidebar [data-scrollbar="true"]').slimScroll({destroy: true});
					$('.sidebar [data-scrollbar="true"]').removeAttr('style');
					$('.sidebar [data-scrollbar="true"]').removeAttr('data-init');
				}
			}
			if ($('#page-container .sidebar-bg').length === 0) {
				$('#page-container').append('<div class="sidebar-bg"></div>');
			}
		}
	});
    
	// HEADER - fixed or default
	$(document).on('change', '.theme-panel [name=header-fixed]', function() {
		if ($(this).val() == 1) {
			$('#header').addClass('navbar-fixed-top');
			$('#page-container').addClass('page-header-fixed');
			Cookies.set('header-fixed', true);
		} else {
			if ($('.theme-panel [name=sidebar-fixed]').val() == 1) {
				alert('Default Header with Fixed Sidebar option is not supported. Proceed with Default Header with Default Sidebar.');
				$('.theme-panel [name=sidebar-fixed] option[value="2"]').prop('selected', true);
				$('.theme-panel [name=sidebar-fixed]').trigger('change');
				if ($('#page-container .sidebar-bg').length === 0) {
					$('#page-container').append('<div class="sidebar-bg"></div>');
				}
			}
			$('#header').removeClass('navbar-fixed-top');
			$('#page-container').removeClass('page-header-fixed');
			Cookies.set('header-fixed', false);
		}
	});
};


/* 11. Handle Theme Panel Expand - added in V1.2
------------------------------------------------ */
var handleThemePanelExpand = function() {
	$(document).on('click', '[data-click="theme-panel-expand"]', function() {
		var targetContainer = '.theme-panel';
		var targetClass = 'active';
		if ($(targetContainer).hasClass(targetClass)) {
			$(targetContainer).removeClass(targetClass);
		} else {
			$(targetContainer).addClass(targetClass);
		}
	});
};


/* 12. Handle After Page Load Add Class Function - added in V1.2
------------------------------------------------ */
var handleAfterPageLoadAddClass = function() {
	if ($('[data-pageload-addclass]').length !== 0) {
		$(window).on('load', function() {
			$('[data-pageload-addclass]').each(function() {
				var targetClass = $(this).attr('data-pageload-addclass');
				$(this).addClass(targetClass);
			});
		});
	}
};


/* 13. Handle Save Panel Position Function - added in V1.5
------------------------------------------------ */
var handleSavePanelPosition = function(element) {
	"use strict";
	if ($('.ui-sortable').length !== 0) {
		var newValue = [];
		var index = 0;
		$.when($('.ui-sortable').each(function() {
			var panelSortableElement = $(this).find('[data-sortable-id]');
			if (panelSortableElement.length !== 0) {
				var columnValue = [];
				$(panelSortableElement).each(function() {
					var targetSortId = $(this).attr('data-sortable-id');
					columnValue.push({id: targetSortId});
				});
				newValue.push(columnValue);
			} else {
				newValue.push([]);
			}
			index++;
		})).done(function() {
			var targetPage = window.location.href;
			    targetPage = targetPage.split('?');
			    targetPage = targetPage[0];
			localStorage.setItem(targetPage, JSON.stringify(newValue));
			$(element).find('[data-id="title-spinner"]').delay(500).fadeOut(500, function() {
				$(this).remove();
			});
		});
	}
};


/* 14. Handle Draggable Panel Local Storage Function - added in V1.5
------------------------------------------------ */
var handleLocalStorage = function() {
	"use strict";
	if (typeof(Storage) !== 'undefined' && typeof(localStorage) !== 'undefined') {
		var targetPage = window.location.href;
		    targetPage = targetPage.split('?');
		    targetPage = targetPage[0];
		var panelPositionData = localStorage.getItem(targetPage);

		if (panelPositionData) {
			panelPositionData = JSON.parse(panelPositionData);
			var i = 0;
			$.when($('.panel:not([data-sortable="false"])').parent('[class*="col-"]').each(function() {
				var storageData = panelPositionData[i]; 
				var targetColumn = $(this);
				if (storageData) {
					$.each(storageData, function(index, data) {
						var targetId = $('[data-sortable-id="'+ data.id +'"]').not('[data-init="true"]');
						if ($(targetId).length !== 0) {
							var targetHtml = $(targetId).clone();
							$(targetId).remove();
							$(targetColumn).append(targetHtml);
							$('[data-sortable-id="'+ data.id +'"]').attr('data-init','true');
						}
					});
				}
				i++;
			})).done(function() {
				window.dispatchEvent(new CustomEvent('localstorage-position-loaded'));
			});
		}
	} else {
		alert('Your browser is not supported with the local storage'); 
	}
};


/* 15. Handle Reset Local Storage - added in V1.5
------------------------------------------------ */
var handleResetLocalStorage = function() {
	"use strict";
	$(document).on('click', '[data-click=reset-local-storage]', function(e) {
		e.preventDefault();

		var targetModalHtml = ''+
		'<div class="modal fade" data-modal-id="reset-local-storage-confirmation">'+
		'    <div class="modal-dialog">'+
		'        <div class="modal-content">'+
		'            <div class="modal-header">'+
		'                <h4 class="modal-title"><i class="fa fa-redo m-r-5"></i> Reset Local Storage Confirmation</h4>'+
		'                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
		'            </div>'+
		'            <div class="modal-body">'+
		'                <div class="alert alert-info m-b-0">Would you like to RESET all your saved widgets and clear Local Storage?</div>'+
		'            </div>'+
		'            <div class="modal-footer">'+
		'                <a href="javascript:;" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times"></i> No</a>'+
		'                <a href="javascript:;" class="btn btn-sm btn-inverse" data-click="confirm-reset-local-storage"><i class="fa fa-check"></i> Yes</a>'+
		'            </div>'+
		'        </div>'+
		'    </div>'+
		'</div>';

		$('body').append(targetModalHtml);
		$('[data-modal-id="reset-local-storage-confirmation"]').modal('show');
	});
	$(document).on('hidden.bs.modal', '[data-modal-id="reset-local-storage-confirmation"]', function(e) {
		$('[data-modal-id="reset-local-storage-confirmation"]').remove();
	});
	$(document).on('click', '[data-click=confirm-reset-local-storage]', function(e) {
		e.preventDefault();
		var localStorageName = window.location.href;
		localStorageName = localStorageName.split('?');
		localStorageName = localStorageName[0];
		localStorage.removeItem(localStorageName);

		location.reload();
	});
};


/* 16. Handle IE Full Height Page Compatibility - added in V1.6
------------------------------------------------ */
var handleIEFullHeightContent = function() {
	var userAgent = window.navigator.userAgent;
	var msie = userAgent.indexOf("MSIE ");

	if (msie > 0) {
		$('.vertical-box-row [data-scrollbar="true"][data-height="100%"]').each(function() {
			var targetRow = $(this).closest('.vertical-box-row');
			var targetHeight = $(targetRow).height();
			$(targetRow).find('.vertical-box-cell').height(targetHeight);
		});
	}
};


/* 17. Handle Unlimited Nav Tabs - added in V1.6
------------------------------------------------ */
var handleUnlimitedTabsRender = function() {
    
	// function handle tab overflow scroll width 
	function handleTabOverflowScrollWidth(obj, animationSpeed) {
		var targetElm = 'li.active';

		if ($(obj).find('li').first().hasClass('nav-item')) {
			targetElm = $(obj).find('.nav-item .active').closest('li');
		}
		var targetCss = ($('body').hasClass('rtl-mode')) ? 'margin-right' : 'margin-left';
		var marginLeft = parseInt($(obj).css(targetCss));  
		var viewWidth = $(obj).width();
		var prevWidth = $(obj).find(targetElm).width();
		var speed = (animationSpeed > -1) ? animationSpeed : 150;
		var fullWidth = 0;

		$(obj).find(targetElm).prevAll().each(function() {
			prevWidth += $(this).width();
		});

		$(obj).find('li').each(function() {
			fullWidth += $(this).width();
		});

		if (prevWidth >= viewWidth) {
			var finalScrollWidth = prevWidth - viewWidth;
			if (fullWidth != prevWidth) {
				finalScrollWidth += 40;
			}
			if ($('body').hasClass('rtl-mode')) {
				$(obj).find('.nav.nav-tabs').animate({ marginRight: '-' + finalScrollWidth + 'px'}, speed);
			} else {
				$(obj).find('.nav.nav-tabs').animate({ marginLeft: '-' + finalScrollWidth + 'px'}, speed);
			}
		}

		if (prevWidth != fullWidth && fullWidth >= viewWidth) {
			$(obj).addClass('overflow-right');
		} else {
			$(obj).removeClass('overflow-right');
		}

		if (prevWidth >= viewWidth && fullWidth >= viewWidth) {
			$(obj).addClass('overflow-left');
		} else {
			$(obj).removeClass('overflow-left');
		}
	}
    
	// function handle tab button action - next / prev
	function handleTabButtonAction(element, direction) {
		var obj = $(element).closest('.tab-overflow');
		var targetCss = ($('body').hasClass('rtl-mode')) ? 'margin-right' : 'margin-left';
		var marginLeft = parseInt($(obj).find('.nav.nav-tabs').css(targetCss));  
		var containerWidth = $(obj).width();
		var totalWidth = 0;
		var finalScrollWidth = 0;

		$(obj).find('li').each(function() {
			if (!$(this).hasClass('next-button') && !$(this).hasClass('prev-button')) {
				totalWidth += $(this).width();
			}
		});

		switch (direction) {
			case 'next':
				var widthLeft = totalWidth + marginLeft - containerWidth;
				if (widthLeft <= containerWidth) {
					finalScrollWidth = widthLeft - marginLeft;
					setTimeout(function() {
						$(obj).removeClass('overflow-right');
					}, 150);
				} else {
					finalScrollWidth = containerWidth - marginLeft - 80;
				}

				if (finalScrollWidth !== 0) {
					if (!$('body').hasClass('rtl-mode')) {
						$(obj).find('.nav.nav-tabs').animate({ marginLeft: '-' + finalScrollWidth + 'px'}, 150, function() {
							$(obj).addClass('overflow-left');
						});
					} else {
						$(obj).find('.nav.nav-tabs').animate({ marginRight: '-' + finalScrollWidth + 'px'}, 150, function() {
							$(obj).addClass('overflow-left');
						});
					}
				}
			break;
		case 'prev':
			var widthLeft = -marginLeft;

			if (widthLeft <= containerWidth) {
				$(obj).removeClass('overflow-left');
				finalScrollWidth = 0;
			} else {
				finalScrollWidth = widthLeft - containerWidth + 80;
			}
			if (!$('body').hasClass('rtl-mode')) {
				$(obj).find('.nav.nav-tabs').animate({ marginLeft: '-' + finalScrollWidth + 'px'}, 150, function() {
					$(obj).addClass('overflow-right');
				});
			} else {
				$(obj).find('.nav.nav-tabs').animate({ marginRight: '-' + finalScrollWidth + 'px'}, 150, function() {
					$(obj).addClass('overflow-right');
				});
			}
			break;
		}
	}

	// handle page load active tab focus
	function handlePageLoadTabFocus() {
		$('.tab-overflow').each(function() {
			handleTabOverflowScrollWidth(this, 0);
		});
	}

	// handle tab next button click action
	$('[data-click="next-tab"]').click(function(e) {
		e.preventDefault();
		handleTabButtonAction(this,'next');
	});

	// handle tab prev button click action
	$('[data-click="prev-tab"]').click(function(e) {
		e.preventDefault();
		handleTabButtonAction(this,'prev');
	});

	// handle unlimited tabs responsive setting
	$(window).resize(function() {
		$('.tab-overflow .nav.nav-tabs').removeAttr('style');
		handlePageLoadTabFocus();
	});

	handlePageLoadTabFocus();
};


/* 18. Handle Top Menu - Unlimited Top Menu Render - added in V1.9
------------------------------------------------ */
var handleUnlimitedTopMenuRender = function() {
	"use strict";
	// function handle menu button action - next / prev
	function handleMenuButtonAction(element, direction) {
		var obj = $(element).closest('.nav');
		var targetCss = ($('body').hasClass('rtl-mode')) ? 'margin-right' : 'margin-left';
		var marginLeft = parseInt($(obj).css(targetCss));  
		var containerWidth = $('.top-menu').width() - 88;
		var totalWidth = 0;
		var finalScrollWidth = 0;

		$(obj).find('li').each(function() {
			if (!$(this).hasClass('menu-control')) {
				totalWidth += $(this).width();
			}
		});

		switch (direction) {
			case 'next':
				var widthLeft = totalWidth + marginLeft - containerWidth;
				if (widthLeft <= containerWidth) {
					finalScrollWidth = widthLeft - marginLeft + 128;
					setTimeout(function() {
						$(obj).find('.menu-control.menu-control-right').removeClass('show');
					}, 150);
				} else {
					finalScrollWidth = containerWidth - marginLeft - 128;
				}

				if (finalScrollWidth !== 0) {
					if (!$('body').hasClass('rtl-mode')) { 
						$(obj).animate({ marginLeft: '-' + finalScrollWidth + 'px'}, 150, function() {
							$(obj).find('.menu-control.menu-control-left').addClass('show');
						});
					} else {
						$(obj).animate({ marginRight: '-' + finalScrollWidth + 'px'}, 150, function() {
							$(obj).find('.menu-control.menu-control-left').addClass('show');
						});
					}
				}
				break;
			case 'prev':
				var widthLeft = -marginLeft;

				if (widthLeft <= containerWidth) {
					$(obj).find('.menu-control.menu-control-left').removeClass('show');
					finalScrollWidth = 0;
				} else {
					finalScrollWidth = widthLeft - containerWidth + 88;
				}
				if (!$('body').hasClass('rtl-mode')) { 
					$(obj).animate({ marginLeft: '-' + finalScrollWidth + 'px'}, 150, function() {
						$(obj).find('.menu-control.menu-control-right').addClass('show');
					});
				} else {
					$(obj).animate({ marginRight: '-' + finalScrollWidth + 'px'}, 150, function() {
						$(obj).find('.menu-control.menu-control-right').addClass('show');
					});
				}
				break;
			}
	}

	// handle page load active menu focus
	function handlePageLoadMenuFocus() {
		var targetMenu = $('.top-menu .nav');
		var targetList = $('.top-menu .nav > li');
		var targetActiveList = $('.top-menu .nav > li.active');
		var targetContainer = $('.top-menu');
		var targetCss = ($('body').hasClass('rtl-mode')) ? 'margin-right' : 'margin-left';
		var marginLeft = parseInt($(targetMenu).css(targetCss));  
		var viewWidth = $(targetContainer).width() - 128;
		var prevWidth = $('.top-menu .nav > li.active').width();
		var speed = 0;
		var fullWidth = 0;

		$(targetActiveList).prevAll().each(function() {
			prevWidth += $(this).width();
		});

		$(targetList).each(function() {
			if (!$(this).hasClass('menu-control')) {
				fullWidth += $(this).width();
			}
		});

		if (prevWidth >= viewWidth) {
			var finalScrollWidth = prevWidth - viewWidth + 128;
			if (!$('body').hasClass('rtl-mode')) { 
				$(targetMenu).animate({ marginLeft: '-' + finalScrollWidth + 'px'}, speed);
			} else {
				$(targetMenu).animate({ marginRight: '-' + finalScrollWidth + 'px'}, speed);
			}
		}

		if (prevWidth != fullWidth && fullWidth >= viewWidth) {
			$(targetMenu).find('.menu-control.menu-control-right').addClass('show');
		} else {
			$(targetMenu).find('.menu-control.menu-control-right').removeClass('show');
		}

		if (prevWidth >= viewWidth && fullWidth >= viewWidth) {
			$(targetMenu).find('.menu-control.menu-control-left').addClass('show');
		} else {
			$(targetMenu).find('.menu-control.menu-control-left').removeClass('show');
		}
	}

	// handle menu next button click action
	$('[data-click="next-menu"]').click(function(e) {
		e.preventDefault();
		handleMenuButtonAction(this,'next');
	});

	// handle menu prev button click action
	$('[data-click="prev-menu"]').click(function(e) {
		e.preventDefault();
		handleMenuButtonAction(this,'prev');
	});

	// handle unlimited menu responsive setting
	$(window).resize(function() {
		$('.top-menu .nav').removeAttr('style');
		handlePageLoadMenuFocus();
	});

	handlePageLoadMenuFocus();
};


/* 19. Handle Top Menu - Sub Menu Toggle - added in V1.9
------------------------------------------------ */
var handleTopMenuSubMenu = function() {
	"use strict";
	$('.top-menu .sub-menu .has-sub > a').click(function() {
		var target = $(this).closest('li').find('.sub-menu').first();
		var otherMenu = $(this).closest('ul').find('.sub-menu').not(target);
		$(otherMenu).not(target).slideUp(250, function() {
			$(this).closest('li').removeClass('expand');
		});
		$(target).slideToggle(250, function() {
			var targetLi = $(this).closest('li');
			if ($(targetLi).hasClass('expand')) {
				$(targetLi).removeClass('expand');
			} else {
				$(targetLi).addClass('expand');
			}
		});
	});
};


/* 20. Handle Top Menu - Mobile Sub Menu Toggle - added in V1.9
------------------------------------------------ */
var handleMobileTopMenuSubMenu = function() {
	"use strict";
	$('.top-menu .nav > li.has-sub > a').click(function() {
		if ($(window).width() <= 767) {
			var target = $(this).closest('li').find('.sub-menu').first();
			var otherMenu = $(this).closest('ul').find('.sub-menu').not(target);
			$(otherMenu).not(target).slideUp(250, function() {
				$(this).closest('li').removeClass('expand');
			});
			$(target).slideToggle(250, function() {
				var targetLi = $(this).closest('li');
				if ($(targetLi).hasClass('expand')) {
					$(targetLi).removeClass('expand');
				} else {
					$(targetLi).addClass('expand');
				}
			});
		}
	});
};


/* 21. Handle Top Menu - Mobile Top Menu Toggle - added in V1.9
------------------------------------------------ */
var handleTopMenuMobileToggle = function() {
	"use strict";
	$('[data-click="top-menu-toggled"]').click(function() {
		$('.top-menu').slideToggle(250);
	});
};


/* 22. Handle Clear Sidebar Selection & Hide Mobile Menu - added in V1.9
------------------------------------------------ */
var handleClearSidebarSelection = function() {
	$('.sidebar .nav > li, .sidebar .nav .sub-menu').removeClass('expand').removeAttr('style');
};
var handleClearSidebarMobileSelection = function() {
	$('#page-container').removeClass('page-sidebar-toggled');
};


/* 23. Handle Check Bootstrap Version - added in V4.0
------------------------------------------------ */
var handleCheckBootstrapVersion = function() {
	return parseInt($.fn.tooltip.Constructor.VERSION);
};


/* 24. Handle Page Scroll Class - added in V4.0
------------------------------------------------ */
var handleCheckScrollClass = function() {
	if ($(window).scrollTop() > 0) {
		$('#page-container').addClass('has-scroll');
	} else {
		$('#page-container').removeClass('has-scroll');
	}
};
var handlePageScrollClass = function() {
	$(window).on('scroll', function() {
		handleCheckScrollClass();
	});
	handleCheckScrollClass();
};


/* 25. Handle Toggle Navbar Profile - added in V4.0
------------------------------------------------ */
var handleToggleNavProfile = function() {
	var expandTime = ($('.sidebar').attr('data-disable-slide-animation')) ? 0 : 250;

	$('[data-toggle="nav-profile"]').click(function(e) {
		e.preventDefault();

		var targetLi = $(this).closest('li');
		var targetProfile = $('.sidebar .nav.nav-profile');
		var targetClass = 'active';
		var targetExpandingClass = 'expanding';
		var targetExpandClass = 'expand';
		var targetClosingClass = 'closing';
		var targetClosedClass = 'closed';

		if ($(targetProfile).is(':visible')) {
			$(targetLi).removeClass(targetClass);
			$(targetProfile).removeClass(targetClosingClass);
		} else {
			$(targetLi).addClass(targetClass);
			$(targetProfile).addClass(targetExpandingClass);
		}
		$(targetProfile).slideToggle(expandTime, function() {
			if (!$(targetProfile).is(':visible')) {
				$(targetProfile).addClass(targetClosedClass);
				$(targetProfile).removeClass(targetExpandClass);
			} else {
				$(targetProfile).addClass(targetExpandClass);
				$(targetProfile).removeClass(targetClosedClass);
			}
			$(targetProfile).removeClass(targetExpandingClass + ' ' + targetClosingClass);
		});
	});
};


/* 26. Handle Sidebar Scroll Memory - added in V4.0
------------------------------------------------ */
var handleSidebarScrollMemory = function() {
	if (!(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))) {
		$('.sidebar [data-scrollbar="true"]').slimScroll().bind('slimscrolling', function(e, pos) {
			localStorage.setItem('sidebarScrollPosition', pos + 'px');
		});
	
		var defaultScroll = localStorage.getItem('sidebarScrollPosition');
		if (defaultScroll) {
			$('.sidebar [data-scrollbar="true"]').slimScroll({ scrollTo: defaultScroll });
		}
	}
};


/* 27. Handle Sidebar Minify Sub Menu - added in V4.0
------------------------------------------------ */
var floatSubMenuTimeout;
var targetFloatMenu;
var handleMouseoverFloatSubMenu = function(elm) {
	clearTimeout(floatSubMenuTimeout);
};
var handleMouseoutFloatSubMenu = function(elm) {
	floatSubMenuTimeout = setTimeout(function() {
		$('#float-sub-menu').remove();
	}, 150);
};
var handleSidebarMinifyFloatMenu = function() {
	$(document).on('click', '#float-sub-menu li.has-sub > a', function(e) {
		var target = $(this).next('.sub-menu');
		var targetLi = $(target).closest('li');
		var close = false;
		var expand = false;
		if ($(target).is(':visible')) {
			$(targetLi).addClass('closing');
			close = true;
		} else {
			$(targetLi).addClass('expanding');
			expand = true;
		}
		$(target).slideToggle({
			duration: 250,
			progress: function() {
				var targetMenu = $('#float-sub-menu');
				var targetHeight = $(targetMenu).height();
				var targetOffset = $(targetMenu).offset();
				var targetOriTop = $(targetMenu).attr('data-offset-top');
				var targetMenuTop = $(targetMenu).attr('data-menu-offset-top');
				var targetTop 	 = targetOffset.top - $(window).scrollTop();
				var windowHeight = $(window).height();
				if (close) {
					if (targetTop > targetOriTop) {
						targetTop = (targetTop > targetOriTop) ? targetOriTop : targetTop;
						$('#float-sub-menu').css({ 'top': targetTop + 'px', 'bottom': 'auto' });
						$('#float-sub-menu-arrow').css({ 'top': '20px', 'bottom': 'auto' });
						$('#float-sub-menu-line').css({ 'top': '20px', 'bottom': 'auto' });
					}
				}
				if (expand) {
					if ((windowHeight - targetTop) < targetHeight) {
						var arrowBottom = (windowHeight - targetMenuTop) - 22;
						$('#float-sub-menu').css({ 'top': 'auto', 'bottom': 0 });
						$('#float-sub-menu-arrow').css({ 'top': 'auto', 'bottom': arrowBottom + 'px' });
						$('#float-sub-menu-line').css({ 'top': '20px', 'bottom': arrowBottom + 'px' });
					}
				}
			},
			complete: function() {
				if ($(target).is(':visible')) {
					$(targetLi).addClass('expand');
				} else {
					$(targetLi).addClass('closed');
				}
				$(targetLi).removeClass('closing expanding');
			}
		});
	});
	$('.sidebar .nav > li.has-sub > a').hover(function() {
		if ($('#page-container').hasClass('page-sidebar-minified')) {
			clearTimeout(floatSubMenuTimeout);

			var targetMenu = $(this).closest('li').find('.sub-menu').first();
			if (targetFloatMenu == this && $('#float-sub-menu').length !== 0) {
				return;
			} else {
				targetFloatMenu = this;
			}
			var targetMenuHtml = $(targetMenu).html();
			if (targetMenuHtml) {
				var sidebarOffset = $('#sidebar').offset();
				var sidebarX = (!$('#page-container').hasClass('page-with-right-sidebar')) ? (sidebarOffset.left + 60) : ($(window).width() - sidebarOffset.left);
				var targetHeight = $(targetMenu).height();
				var targetOffset = $(this).offset();
				var targetTop = targetOffset.top - $(window).scrollTop();
				var targetLeft = (!$('#page-container').hasClass('page-with-right-sidebar')) ? sidebarX : 'auto';
				var targetRight = (!$('#page-container').hasClass('page-with-right-sidebar')) ? 'auto' : sidebarX;
				var windowHeight = $(window).height();
	
				if ($('#float-sub-menu').length === 0) {
					targetMenuHtml = ''+
					'<div class="float-sub-menu-container" id="float-sub-menu" data-offset-top="'+ targetTop +'" data-menu-offset-top="'+ targetTop +'" onmouseover="handleMouseoverFloatSubMenu(this)" onmouseout="handleMouseoutFloatSubMenu(this)">'+
					'	<div class="float-sub-menu-arrow" id="float-sub-menu-arrow"></div>'+
					'	<div class="float-sub-menu-line" id="float-sub-menu-line"></div>'+
					'	<ul class="float-sub-menu">'+ targetMenuHtml + '</ul>'+
					'</div>';
					$('#page-container').append(targetMenuHtml);
				} else {
					$('#float-sub-menu').attr('data-offset-top', targetTop);
					$('#float-sub-menu').attr('data-menu-offset-top', targetTop);
					$('.float-sub-menu').html(targetMenuHtml);
				}
				
				if ((windowHeight - targetTop) > targetHeight) {
					$('#float-sub-menu').css({
						'top': targetTop,
						'left': targetLeft,
						'bottom': 'auto',
						'right': targetRight
					});
					$('#float-sub-menu-arrow').css({ 'top': '20px', 'bottom': 'auto' });
					$('#float-sub-menu-line').css({ 'top': '20px', 'bottom': 'auto' });
				} else {
					$('#float-sub-menu').css({
						'bottom': 0,
						'top': 'auto',
						'left': targetLeft,
						'right': targetRight
					});
					var arrowBottom = (windowHeight - targetTop) - 21;
					$('#float-sub-menu-arrow').css({ 'top': 'auto', 'bottom': arrowBottom + 'px' });
					$('#float-sub-menu-line').css({ 'top': '20px', 'bottom': arrowBottom + 'px' });
				}
			} else {
				$('#float-sub-menu').remove();
				targetFloatMenu = '';
			}
		}
	}, function() {
		if ($('#page-container').hasClass('page-sidebar-minified')) {
			floatSubMenuTimeout = setTimeout(function() {
				$('#float-sub-menu').remove();
				targetFloatMenu = '';
			}, 250);
		}
	});
};


/* 28. Handle Ajax Mode - added in V4.0
------------------------------------------------ */
var CLEAR_OPTION = '';
var handleAjaxMode = function(setting) {
	var emptyHtml = (setting.emptyHtml) ?  setting.emptyHtml : '<div class="p-t-40 p-b-40 text-center f-s-20 content"><i class="fa fa-warning fa-lg text-muted m-r-5"></i> <span class="f-w-600 text-inverse">Error 404! Page not found.</span></div>';
	var defaultUrl = (setting.ajaxDefaultUrl) ? setting.ajaxDefaultUrl : '';
	    defaultUrl = (window.location.hash) ? window.location.hash : defaultUrl;
	
	if (defaultUrl === '') {
		$('#content').html(emptyHtml);
	} else {
		renderAjax(defaultUrl, '', true);
	}
    
	function clearElement() {
		$('.jvectormap-label, .jvector-label, .AutoFill_border ,#gritter-notice-wrapper, .ui-autocomplete, .colorpicker, .FixedHeader_Header, .FixedHeader_Cloned .lightboxOverlay, .lightbox, .introjs-hints, .nvtooltip, #float-sub-menu').remove();
		if ($.fn.DataTable) {
			$('.dataTable').DataTable().destroy();
		}
		if ($('#page-container').hasClass('page-sidebar-toggled')) {
			$('#page-container').removeClass('page-sidebar-toggled');
		}
	}
	
	function checkSidebarActive(url) {
		var targetElm = '#sidebar [data-toggle="ajax"][href="'+ url +'"]';
		if ($(targetElm).length !== 0) {
			$('#sidebar li').removeClass('active');
			$(targetElm).closest('li').addClass('active');
			$(targetElm).parents().addClass('active');
		}
	}
	
	function checkPushState(url) {
		var targetUrl = url.replace('#','');
		var targetUserAgent = window.navigator.userAgent;
		var isIE = targetUserAgent.indexOf('MSIE ');
	
		if (isIE && (isIE > 0 && isIE < 9)) {
			window.location.href = targetUrl;
		} else {
			history.pushState('', '', '#' + targetUrl);
		}
	}
	
	function checkClearOption() {
		if (CLEAR_OPTION) {
			App.clearPageOption(CLEAR_OPTION);
			CLEAR_OPTION = '';
		}
	}
	
	function checkLoading(load) {
		if (!load) {
			if ($('#page-content-loader').length === 0) {
				$('body').addClass('page-content-loading');
				$('#content').append('<div id="page-content-loader"><span class="spinner"></span></div>');
			}
		} else {
			$('#page-content-loader').remove();
			$('body').removeClass('page-content-loading');
		}
	}
	
	function renderAjax(url, elm, disablePushState) {
		Pace.restart();
		
		checkLoading(false);
		clearElement();
		checkSidebarActive(url);
		checkClearOption();
		if (!disablePushState) {
			checkPushState(url);
		}
    
		var targetContainer= '#content';
		var targetUrl 	   = url.replace('#','');
		var targetType 	   = (setting.ajaxType) ? setting.ajaxType : 'GET';
		var targetDataType = (setting.ajaxDataType) ? setting.ajaxDataType : 'html';
		if (elm) {
			targetDataType = ($(elm).attr('data-type')) ? $(elm).attr('data-type') : targetDataType;
			targetDataDataType = ($(elm).attr('data-data-type')) ? $(elm).attr('data-data-type') : targetDataType;
		}
		
		$.ajax({
			url: targetUrl,
			type: targetType,
			dataType: targetDataType,
			success: function(data) {
				$(targetContainer).html(data);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				$(targetContainer).html(emptyHtml);
			}
		}).done(function() {
			checkLoading(true);
			$('html, body').animate({ scrollTop: 0 }, 0);
			App.initComponent();
		});
	}
	
	$(window).on('hashchange', function() {
		if (window.location.hash) {
			renderAjax(window.location.hash, '', true);
		}
	});
	
	$(document).on('click', '[data-toggle="ajax"]', function(e) {
		e.preventDefault();
		renderAjax($(this).attr('href'), this);
	});
};
var handleSetPageOption = function(option) {
	if (option.pageContentFullHeight) {
		$('#page-container').addClass('page-content-full-height');
	}
	if (option.pageSidebarLight) {
		$('#page-container').addClass('page-with-light-sidebar');
	}
	if (option.pageSidebarRight) {
		$('#page-container').addClass('page-with-right-sidebar');
	}
	if (option.pageSidebarWide) {
		$('#page-container').addClass('page-with-wide-sidebar');
	}
	if (option.pageSidebarMinified) {
		$('#page-container').addClass('page-sidebar-minified');
	}
	if (option.pageSidebarTransparent) {
		$('#sidebar').addClass('sidebar-transparent');
	}
	if (option.pageContentFullWidth) {
		$('#content').addClass('content-full-width');
	}
	if (option.pageContentInverseMode) {
		$('#content').addClass('content-inverse-mode');
	}
	if (option.pageBoxedLayout) {
		$('body').addClass('boxed-layout');
	}
	if (option.clearOptionOnLeave) {
		CLEAR_OPTION = option;
	}
};
var handleClearPageOption = function(option) {
	if (option.pageContentFullHeight) {
		$('#page-container').removeClass('page-content-full-height');
	}
	if (option.pageSidebarLight) {
		$('#page-container').removeClass('page-with-light-sidebar');
	}
	if (option.pageSidebarRight) {
		$('#page-container').removeClass('page-with-right-sidebar');
	}
	if (option.pageSidebarWide) {
		$('#page-container').removeClass('page-with-wide-sidebar');
	}
	if (option.pageSidebarMinified) {
		$('#page-container').removeClass('page-sidebar-minified');
	}
	if (option.pageSidebarTransparent) {
		$('#sidebar').removeClass('sidebar-transparent');
	}
	if (option.pageContentFullWidth) {
		$('#content').removeClass('content-full-width');
	}
	if (option.pageContentInverseMode) {
		$('#content').removeClass('content-inverse-mode');
	}
	if (option.pageBoxedLayout) {
		$('body').removeClass('boxed-layout');
	}
};


/* 29. Handle Float Navbar Search - added in V4.0
------------------------------------------------ */
var handleToggleNavbarSearch = function() {
	$('[data-toggle="navbar-search"]').click(function(e) {
		e.preventDefault();
		$('.header').addClass('header-search-toggled');
	});

	$('[data-dismiss="navbar-search"]').click(function(e) {
		e.preventDefault();
		$('.header').removeClass('header-search-toggled');
                $(this).closest('form').find('input[type="text"]').val('');
	});
};


/* Application Controller
------------------------------------------------ */
var App = function () {
	"use strict";
	
	var setting;
	
	return {
		//main function
		init: function (option) {
			if (option) {
				setting = option;
			}
			this.initLocalStorage();
			this.initSidebar();
			this.initTopMenu();
			this.initComponent();
			this.initThemePanel();
			this.initPageLoad();
			$(window).trigger('load');

			if (setting && setting.ajaxMode) {
				this.initAjax();
			}
		},
		initSidebar: function() {
			handleSidebarMenu();
			handleMobileSidebarToggle();
			handleSidebarMinify();
			handleSidebarMinifyFloatMenu();
			handleToggleNavProfile();
			handleToggleNavbarSearch();
			
			if (!setting || (setting && !setting.disableSidebarScrollMemory)) {
				handleSidebarScrollMemory();
			}
		},
		initSidebarSelection: function() {
			handleClearSidebarSelection();
		},
		initSidebarMobileSelection: function() {
			handleClearSidebarMobileSelection();
		},
		initTopMenu: function() {
			handleUnlimitedTopMenuRender();
			handleTopMenuSubMenu();
			handleMobileTopMenuSubMenu();
			handleTopMenuMobileToggle();
		},
		initPageLoad: function() {
			handlePageContentView();
		},
		initComponent: function() {
			if (!setting || (setting && !setting.disableDraggablePanel)) {
				handleDraggablePanel();
			}
			handleIEFullHeightContent();
			handleSlimScroll();
			handleUnlimitedTabsRender();
			handlePanelAction();
			handelTooltipPopoverActivation();
			handleScrollToTopButton();
			handleAfterPageLoadAddClass();
			handlePageScrollClass();
		},
		initLocalStorage: function() {
			handleLocalStorage();
		},
		initThemePanel: function() {
			handleThemePageStructureControl();
			handleThemePanelExpand();
			handleResetLocalStorage();
		},
		initAjax: function() {
			handleAjaxMode(setting);
			$.ajaxSetup({
				cache: true
			});
		},
		setPageTitle: function(pageTitle) {
			document.title = pageTitle;
		},
		setPageOption: function(option) {
			handleSetPageOption(option);
		},
		clearPageOption: function(option) {
			handleClearPageOption(option);
		},
		restartGlobalFunction: function() {
			this.initLocalStorage();
			this.initTopMenu();
			this.initComponent();
		},
		scrollTop: function() {
			$('html, body').animate({
				scrollTop: $('body').offset().top
			}, 0);
		}
  };
}();

$(document).ready(function() {
    App.init();
});
var Csrf = {
    token: null,
    tokenName: 'csrfToken',
    init: function () {
        Csrf.setToken();
    },
    setToken: function () {
        Csrf.setTokenFromCookie();
        if (Csrf.token == null) {
            Csrf.setTokenFromForm();
        }
    },
    setTokenFromCookie: function () {
        var cookieToken = Csrf.getCookie(Csrf.tokenName);
        if (cookieToken != '' && cookieToken != 'undefined') {
            Csrf.token = cookieToken;
            Csrf.addToAjaxSetup();
        }
    },
    getCookie: function (cookieName) {
        var name = cookieName + "=",
            decodedCookie = decodeURIComponent(document.cookie),
            ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }

        return "";
    },
    setTokenFromForm: function () {
        csrfInput = $(document).find('input[name="_' + Csrf.tokenName + '"]');
        if (csrfInput.length > 0) {
            Csrf.token = csrfInput.val();
            Csrf.addToAjaxSetup();
        }
    },
    addToAjaxSetup: function () {
        $.ajaxSetup({
            headers : Csrf.getHeaders()
        });
    },
    getHeaders: function() {
        return {
            'X-CSRF-Token': Csrf.token
        }
    }
};

$(document).ready(function () {
    Csrf.init();
});

/**
 * Translate
 *
 * @type object
 */
var Translate = {
    SeoAnalyzer: {
        rowSaved: 'Row updated',
        rowNotSaved: 'Row not saved',
        ogImageUploaded: 'OG images uploaded',
        ogImageNotUploaded: 'OG images not uploaded'
    },
    select2: {
        placeholder: "-- Please choose --"
    },
    propertyAttributes: {
        noAttributes: 'There are no additional options for selected property type.',
        pleaseSelect: 'Please choose property type to display additional options.'
    }
};

var Loading = {
    create: function (obj) {
        obj.append('<div class="loading"><i class="fas fa-circle-notch fa-spin fa-3x"></i></div>');
    },
    remove: function (obj) {
        obj.find('.loading').remove();
    }
}

/**
 * Notifications
 *
 * @description Default notifications used in administration panel. 
 * 
 * Color Admin uses Gritter as a default notifications, however it's quite old as it has been created in 2009 (last 
 * update in 2012). That's why it has been removed from plugins and replaced with Noty
 * @see https://github.com/needim/noty Github
 * @see https://ned.im/noty/ Website
 * @see https://ned.im/noty/#/options Options
 * 
 * 
 * @type object
 */
var Notifications = {
    enabled: false,
    config: {
        layout: 'topRight',
        progressBar: true,
        theme: 'metroui'    // specific CSS theme file needs to be included
    },
    init: function () {
        if (typeof Noty === 'function') {
            Notifications.enabled = true;
        } else {
            this.devError();
        }
    },
    devError: function () {
        console.error('Notifications:', 'Please include Noty plugin.');
    },
    create: function (type, text, options) {
        if (this.enabled) {
            options = options || {};
            var timeout = (typeof options.timeout !== 'undefined' && options.timeout !== null) ? options.timeout : 3000;
            
            var config = $.extend({}, this.config);
            config = $.extend(config, {
                type: type,
                text: text,
                timeout: timeout,
            });

            if (typeof options.buttons === 'object') {
                config = $.extend(config, {buttons: options.buttons});
            }
            if (typeof options.callbacks === 'object') {
                config = $.extend(config, {callbacks: options.callbacks});
            }

            var noty = new Noty(config);
            noty.show();
            
            return noty;
        }
    },
    closeAll: function() {
        if (this.enabled) {
            Noty.closeAll();
        }
    }
}

function generateAlert(type, text) {
    Notifications.init();
    Notifications.create(type, text);
}

$(document).ready(function () {
    Notifications.init();
});
/**
 * Swal 
 * 
 * @type object
 * @@see https://sweetalert.js.org/docs/
 */
var Swal = {
    enabled: false,
    class: '.confirm',
    init: function () {
        if (typeof swal === 'function') {
            Swal.enabled = true;
        } else {
            this.devError();
        }

        Swal.listeners();
    },
    devError: function () {
        console.error('Swal:', 'Please include swal plugin.');
    },
    listeners: function () {
        if (Swal.enabled) {
            $(document).on('click', 'a' + Swal.class, function () {
                Swal.elementClick($(this));
            });
        }
    },
    elementClick: function (obj) {
        var confirm = obj.data('confirm');
        var title = obj.data('message');
        var message = obj.data('text');
        var formName = obj.data('form');
        var icon = obj.data('icon');

        if (confirm === 1 && title != '' && formName != '') {
            Swal.submitConfirmation(title, message, formName, icon);
        }
    },
    submitConfirmation: function (title, text, formName, icon) {
        if (icon == undefined) {
            icon = 'error';
        }
        if (Swal.enabled) {
            swal({
                title: title,
                text: text,
                icon: icon,
                buttons: true,
                dangerMode: true,
            }).then(function (confirmed) {
                var form = $('form[name="' + formName + '"]');
                if (confirmed && typeof form !== 'undefined') {
                    form.submit();
                }
            });
        }
    }
}

$(document).ready(function () {
    Swal.init();
});


//var FormBuilder = {
//    row: null,
//    option: null,
//    currentOptionValue: {},
//    currentOptionSelector: null,
//    validators: {
//        max_length: 'ex. 10',
//        min_length: 'ex. 2',
//        max_min_length: 'ex 2/10',
//        decimal: '[0-9]+([\.,][0-9]+)?',
//        phone: '[\+]\d{2}[\(]\d{2}[\)]\d{4}[\-]\d{4}'
//    },
//    mainEmails: [],
//    ccEmails: [],
//    bccEmails: [],
//    init: function () {
//        FormBuilder.checkForm();
//        FormBuilder.events();
//        FormBuilder.listenRedirect($('#thank-you'));
//        FormBuilder.loadExistingEmails();
//    },
//    events: function () {
////add new field
//        $(document).on('click', '.moveUp', FormBuilder.moveFieldUp());
//        $(document).on('click', '.moveDown', FormBuilder.moveFieldDown());
//        $(document).on('click', '#addField', FormBuilder.addField());
//        //remove current field
//        $(document).on('click', '.remove', function () {
//            $(this).parents('tr').remove();
//            FormBuilder.checkForm();
//        });
//        //show modal to create options for select
//        $(document).on('change', '.optionsSelect', function () {
//            if ($(this).val() === 'select' || $(this).val() === 'radio') {
//                $(this).parent().children('.optionsButton').show();
//            } else {
//                $(this).parent().children('.optionsButton').hide();
//            }
//            $(this).parent().children('input[type="hidden"]').val('');
//        });
//        $(document).on('click', '.optionsButton', function () {
//            $('.optionsBody').html('');
//            FormBuilder.currentOptionSelector = $(this).parent().children('input[type="hidden"]');
//            var value = FormBuilder.currentOptionSelector.val();
//            if (value.trim() !== '') {
//                FormBuilder.currentOptionValue = value;
//            }
//            if (typeof FormBuilder.currentOptionValue === 'string'
//                    && typeof JSON.parse(FormBuilder.currentOptionValue) === 'object'
//                    && Object.keys(JSON.parse(FormBuilder.currentOptionValue)).length > 0) {
//                FormBuilder.currentOptionValue = JSON.parse(FormBuilder.currentOptionValue);
//                FormBuilder.buildExistingOption();
//            }
//        });
//        //add new option to modal
//        $(document).on('click', '#addOption', FormBuilder.addOption());
//        //save inserted options 
//        $(document).on('click', '#saveOptions', FormBuilder.saveOptions());
//        //inserting rule into input depends on selected value
//        $(document).on('change', '.validationSelect', function () {
//            FormBuilder.changeRule($(this));
//        });
//        //change name of field required to catch this field in controller by name
//        $(document).on('change', '.inputName', function () {
//            console.log($(this).val());
//            var requiredField = $(this).parents('tr').find('.requiredField');
//            requiredField.attr('name', 'required[' + $(this).val() + ']');
//            requiredField.parent().children('input[type="hidden"]').attr('name', 'required[' + $(this).val() + ']');
//        });
//        $(document).on('click', '.addMainEmail,.addCCEmail,.addBCCEmail', function () {
//            var input = $(this).parent().parent().find('input');
//            FormBuilder.addEmail(input.val(), $(this).data('email'), input);
//        });
//
//        $(document).on('click', '.emailElement', function () {
//            FormBuilder.removeEmail($(this).data('email'), $(this).data('selector'));
//        });
//        $(document).on('click', '#thank-you', function () {
//            FormBuilder.listenRedirect($(this));
//        });
//    },
//
//    listenRedirect: function (element) {
//        if (element.is(':checked')) {
//            $('.redirect').parents('.form-group').show();
//        } else {
//            $('.redirect').parents('.form-group').hide();
//        }
//    },
//
//    addEmail: function (value, selector, input) {
//        if (FormBuilder.validateEmail(value)) {
//            FormBuilder[selector].push(value);
//            FormBuilder.updateEmails(selector);
//            input.val('').css({
//                'border': '1px solid #e5e6e7'
//            });
//        } else {
//            console.log('display error');
//            input.css({
//                'border': '1px solid red'
//            });
//        }
//    },
//    validateEmail: function (email) {
//        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//        return re.test(email);
//    },
//    removeEmail: function (email, selector) {
//        var index = FormBuilder[selector].indexOf(email);
//        if (index != -1) {
//            FormBuilder[selector].splice(index, 1);
//        }
//        FormBuilder.updateEmails(selector);
//    },
//    updateEmails: function (selector) {
//        var list = FormBuilder[selector];
//        var html = '';
//        for (var i = 0; i < list.length; i++) {
//            html += '<button type="button" class="btn btn-default btn-md emailElement" data-selector="' + selector + '" data-email="' + list[i] + '">' + list[i] + ' <i class="removeEmail fa fa-close text-danger"></i></button>';
//        }
//        $('.' + selector).html(html);
//        $('.' + selector + 'Hidden').val(list);
//    },
//
//    loadExistingEmails: function () {
//        var emails = ['mainEmails', 'ccEmails', 'bccEmails'];
//        $.each(emails, function (index, value) {
//            var arrayWithEmails = $('.' + value + 'Hidden').val();
//            if (typeof arrayWithEmails !== 'undefined') {
//                var email = arrayWithEmails.split(',');
//                $.each(email, function (index, singleEmail) {
//                    if (singleEmail.trim() !== '') {
//                        FormBuilder[value].push(singleEmail);
//                    }
//                });
//            }
//        });
//    },
//    addField: function () {
//        if (typeof FormBuilder.row[0] !== 'undefined') {
//            $('.formBuilderBody').append(FormBuilder.row[0].outerHTML);
//        }
//        FormBuilder.checkForm();
//    },
//    addOption: function () {
//        if (typeof FormBuilder.option[0] !== 'undefined') {
//            $('.optionsBody').append(FormBuilder.option[0].outerHTML);
//        }
//    },
//    checkForm: function () {
//        if ($('.formBuilderTable tbody tr').length === 0) {
//            $('.formBuilderTable').hide();
//        } else {
//            $('.formBuilderTable').show();
//        }
//    },
//    saveOptions: function () {
//        var key, value = '';
//        FormBuilder.currentOptionValue = {};
//        $('.optionsBody tr').each(function () {
//            $(this).find('input').each(function () {
//                var id = $(this).attr('id');
//                if (id === 'optionvalue') {
//                    key = $(this).val();
//                } else if (id === 'optionname') {
//                    value = $(this).val();
//                } else {
//                    key = value = '';
//                }
//            });
//            FormBuilder.currentOptionValue[key] = value;
//        });
//        FormBuilder.currentOptionSelector.val(JSON.stringify(formBuilder.currentOptionValue));
//        $('.optionsBody').html('');
//        $('#optionModalDialog').modal('hide');
//    },
//    buildExistingOption: function () {
//        for (var key in FormBuilder.currentOptionValue) {
//            var option = FormBuilder.option.clone();
//            option.find('input').each(function () {
//                if ($(this).attr('id') === 'optionvalue') {
//                    $(this).val(key);
//                } else if ($(this).attr('id') === 'optionname') {
//                    $(this).val(FormBuilder.currentOptionValue[key]);
//                }
//            });
//            $('.optionsBody').append(option.addClass('asd'));
//        }
//    },
//    changeRule: function (validationSelect) {
//        var value = validationSelect.val();
//        var ruleInput = validationSelect.parents('tr').find('.rule');
//        if (typeof ruleInput[0] !== 'undefined') {
//            if (typeof FormBuilder.validators[value] !== 'undefined') {
//                ruleInput.val(FormBuilder.validators[value]);
//            } else {
//                ruleInput.val('');
//            }
//        }
//    },
//    moveFieldUp: function () {
//        var element = $(this).parents('tr');
//        var prev = element.prev();
//        element.insertBefore(prev);
//    },
//    moveFieldDown: function () {
//        var element = $(this).parents('tr');
//        var next = element.next();
//        element.insertAfter(next);
//    }
//};
//$(document).ready(function () {
//    FormBuilder.init();
//});
//$(window).load(function () {
//    FormBuilder.row = $('#row tbody > *').clone();
//    FormBuilder.option = $('#option tbody > *').clone();
//});
//sessionlifetime, userEmail, adminPrefix set in head.ctp
var LockScreen = {
    enabled: false,
    lifeTime: null,
    userEmail: null,
    lockScreenId: 'lock-screen',
    progressBarId: 'session-time-bar',
    lockScreenFormId: 'locked-screen-form',
    lockScreenObj: null,
    progressBarObj: null,
    percent: 100,
    currentPercent: null,
    percentWarning: 30,
    submitTries: 0,
    init: function () {
        LockScreen.setLifeTime();
        LockScreen.setUserEmail();
        LockScreen.setObj('lockScreenObj', LockScreen.lockScreenId);
        LockScreen.setObj('progressBarObj', LockScreen.progressBarId);
        LockScreen.setEnabled();
        LockScreen.setCurrentPercent(LockScreen.percent);

        var interval = LockScreen.lifeTime / LockScreen.percent * 1000;

        LockScreen.loop();
        setInterval(function () {
            LockScreen.loop();
        }, interval);

        LockScreen.listeners();
    },
    listeners: function () {
        if (LockScreen.enabled) {
            $('#' + LockScreen.lockScreenFormId).submit(function (e) {
                e.preventDefault();
                LockScreen.submitScreenLock($(this));
            });
        }
    },
    setObj: function (name, id) {
        var obj = $('#' + id);

        if (obj.length > 0) {
            LockScreen[name] = obj;
        }
    },
    setLifeTime: function () {
        if (typeof sessionlifetime !== 'undefined') {
            LockScreen.lifeTime = sessionlifetime;
        }
    },
    setUserEmail: function () {
        if (typeof userEmail !== 'undefined') {
            LockScreen.userEmail = userEmail;
        }
    },
    setEnabled: function () {
        if (LockScreen.lifeTime !== null && LockScreen.userEmail !== null && typeof LockScreen.lockScreenObj === 'object' && typeof LockScreen.progressBarObj === 'object') {
            LockScreen.enabled = true;
        }
    },
    setCurrentPercent: function (percent) {
        LockScreen.currentPercent = percent;
    },
    setProgress: function (width) {
        $('#' + LockScreen.progressBarId).css('width', width + '%');
    },
    loop: function () {
        if (LockScreen.enabled) {
            LockScreen.setProgress(LockScreen.currentPercent);
            LockScreen.currentPercent--;
            LockScreen.checkPercent();
        }
    },
    checkPercent: function () {
        if (LockScreen.currentPercent === LockScreen.percentWarning) {
            LockScreen.displayWarningNotification();
        } else if (LockScreen.currentPercent === 0) {
            LockScreen.clearNotification();
            LockScreen.displayScreenLock();
        }
    },
    resetPercent: function () {
        LockScreen.currentPercent = LockScreen.percent;
    },
    displayWarningNotification: function () {
        warningNotification = Notifications.create('warning', 'Your session is about to expire...', {
            timeout: false,
            buttons: [
                Noty.button('Click here to renew your session', 'btn btn-primary', function () {
                    LockScreen.refreshSession(warningNotification.options.type);
                    warningNotification.close();
                }, {id: 'noty-btn-refresh', 'data-status': 'ok'}),
            ],
        });
    },
    refreshSession: function (type) {
        var data = {};
        if (type === "error") {
            data.message = "Redirect";
        } else if (type === "warning") {
            data.message = "Renew";
        }
        $.ajax({
            url: adminPrefix + '/users/refreshOnAlertClick',
            method: 'POST',
            data: data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRF-Token", Csrf.token);
            }
        }).done(function (resp) {
            if (resp.status === true) {
                LockScreen.resetPercent();
            }
        });
    },
    clearNotification: function () {
        Notifications.closeAll();
    },
    displayScreenLock: function () {
        LockScreen.lockScreenObj.removeClass('invisible').removeClass('hid').addClass('vis');
    },
    hideScreenLock: function () {
        LockScreen.lockScreenObj.removeClass('vis').addClass('hid').addClass('invisible');
    },
    submitScreenLock: function (obj) {
        var formData = obj.serializeArray();
        obj.find('input[type="password"]').val('');
        formData = {
            email: LockScreen.userEmail,
            password: formData[2]['value']
        };

        $.ajax({
            url: adminPrefix + '/users/login',
            method: 'POST',
            data: formData,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRF-Token", Csrf.token);
            }
        }).done(function (resp) {
            if (resp.status === true) {
                LockScreen.hideScreenLock()
                LockScreen.resetPercent();
                LockScreen.submitTries = 0;
            } else {
                LockScreen.submitTries++;
                if (LockScreen.submitTries > 2) {
                    window.location.replace(adminPrefix + "/users/login");
                } else {
                    errorNotification = Notifications.create('error', 'Incorrect data, please try again.');
                }
            }
        });
    }
}

$(document).ready(function () {
    LockScreen.init();
});

var Switcher = {
    class: '.switcher',
    init: function () {
        if ($(Switcher.class).length > 0) {
            Switcher.listener();
        }
    },
    listener: function () {
        $(Switcher.class + ' input[type=checkbox]').change(function () {
            Switcher.updateValue($(this));
        });
    },
    updateValue: function (obj) {
        var data = {
            id: obj.attr('data-id'),
            model: obj.attr('data-model'),
            column: obj.attr('name')
        };

        var path = obj.attr('data-path');
        if (typeof path == 'undefined' || path == '') {
            path = window.location.pathname
        }

        $.ajax({
            url: path + '/toggle-column-value.json',
            method: 'POST',
            data: data
        }).done(function (resp) {
            if (resp.status) {
                Notifications.create('success', resp.message);
            } else {
                Notifications.create('error', resp.message);
            }
        });
    }
}

$(document).ready(function () {
    Switcher.init();
});
/**
 * Select2
 * 
 * Initialize by adding class 'select2' (default) to the select attribute.
 * 
 * There's additional attributes which can be set:
 * <ul>
 *  <li>`data-type` - controller which will be requesting for available list. It has to have getList() method. 
 *  (there's a GetListTrait for this)</li>
 *  <li>`min-length` - default set to 2, override it if needed by setting this attribute to any number</li>
 * </ul>
 *
 * @type object
 * @see https://select2.org
 */

var Select = {
    enabled: false,
    class: '.select2',
    data: {},
    init: function () {
        if (typeof jQuery.fn.select2 === 'function') {
            this.enabled = true;
        }

        if ($(this.class).length > 0 && this.enabled) {
            this.initSelects();
        } else if ($(this.class).length > 0 && !this.enabled) {
            this.devError();
        }
    },
    devError: function () {
        console.error('Select2:', 'Please include select2 plugin.');
    },
    initSelects: function () {
        $.each($(this.class), function (index, element) {
            var obj = $(element);
            Select.initSelect2(obj);
        });
    },
    defalutOptions: function () {
        var _this = this;
        return {
            placeholder: Translate.select2.placeholder,
            allowClear: true,
            escapeMarkup: function (markup) {
                return markup;
            },
            maximumSelectionLength: 20,
            minimumInputLength: 2,
            ajax: {
                dataType: 'json',
                method: "POST",
                delay: 1000,
                data: function (params) {
                    _this.data.keyword = params.term;
                    return _this.data;
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: true
            }
        };
    },
    initSelect2: function (obj) {
        var options = $.extend({}, this.defalutOptions());
        var dataType = obj.attr('data-type');
        var minLength = obj.attr('min-length');
        var model = dataType || '';

        if (minLength >= 0) {
            options.minimumInputLength = minLength;
        }

        if (typeof dataType != 'undefined') {
            options.ajax.url = adminPrefix + '/' + model + '/getList.json';
        } else {
            delete options.minimumInputLength;
        }

        if (typeof options.ajax.url == 'undefined') {
            delete options.ajax;
        }

        obj.select2(options);
    }
};

$(document).ready(function () {
    Select.init();
});

/**
 * X-Editable
 *
 * @type object
 */
var XEditable = {
    class: {
        editable: '.x-editable',
        appendClass: '.input-group-btn',
        save: '.save',
        cancel: '.cancel',
        showHide: '.showHide',
        rankingList: '.list-unstyled',
        limitFrom: '.limitFrom'
    },
    init: function () {
        this.initDropzone();
        return this;
    },
    listener: function () {
        var _this = this;
        $(this.class.editable).siblings(this.class.appendClass).find('.actions ' + this.class.save).on('click', function (e) {
            $(this).parent().parent().attr('readonly', true);
            _this.saveData($(this).parent().parent().siblings(_this.class.editable));
        });
        $(this.class.editable).siblings(this.class.appendClass).find('.actions ' + this.class.cancel).on('click', function () {
            $(this).parent().parent().siblings(_this.class.editable).attr('readonly', true);
            $(this).parent().hide();
        });
        $(this.class.editable).on('click', function () {
            $(this).attr('readonly', false);
            $(this).siblings(_this.class.appendClass).find('.actions ' + _this.class.save).attr('disabled', false);
            $(this).siblings(_this.class.appendClass).find('.actions').show();
        });
        $(this.class.showHide).on('click', function () {
            $(this).parent().siblings(_this.class.rankingList).toggleClass(function () {
                if ($(this).parent().is(".hide")) {
                    return "show";
                }
                return "hide";
            });
            $(this).toggleClass('fa-chevron-up', 'fa-chevron-down');
        });
        $(this.class.editable).on('keyup', function () {
            $(this).siblings('.input-group-addon').find(_this.class.limitFrom).text($(this).val().length);
        });
        return _this;
    },
    initDropzone: function () {
        var _this = this;
        if ($(".dropzone-seo-analyser").length > 0) {
            $(".dropzone-seo-analyser").each(function (index, item) {
                var handler = $(item).attr('id'),
                    myDropzone = handler + "_" + index,
                    obj = $('#' + handler),
                    myDropzone = new Dropzone('#' + handler, {
                        url: adminPrefix + "/seo-analyser/upload",
                        method: 'post',
                        parallelUploads: 1,
                        maxFilesize: 1,
                        maxFiles: 1,
                        paramName: "og_image",
                        acceptedFiles: 'image/*',
                        headers: Csrf.getHeaders(),
                        autoDiscover: false
                    });
                myDropzone.on("success", function (file, resp) {
                    if (resp.status == 'success') {
                        var id = $(obj).attr('data-id');
                        _this.refreshRaport(resp, id);
                        $("#og_image_" + id).val(resp.og_image);
                    }
                });
                myDropzone.on("complete", function (file) {
                    obj.attr('disabled', false);
                    if (file.dataURL != undefined) {
                        var id = obj.attr('data-id');
                        $('#og_image_thumb_' + id).attr('src', file.dataURL);
                        generateAlert('success', Translate.SeoAnalyzer.ogImageUploaded);
                    } else {
                        generateAlert('error', Translate.SeoAnalyzer.ogImageNotUploaded);
                    }
                });
                myDropzone.on('sending', function (file, xhr, formData) {
                    obj.attr('disabled', true);
                    formData.append('id', obj.attr('data-id'));
                    formData.append('model', obj.attr('data-model'));
                    formData.append('field', obj.attr('data-name'));
                });
            });
        }
    },
    saveData: function (obj) {
        var _this = this;
        $.ajax({
            url: adminPrefix + '/seo-analyser/editable',
            method: 'PUT',
            data: {
                id: $(obj).attr('data-id'),
                field: $(obj).attr('data-name'),
                value: $(obj).val(),
                model: $(obj).attr('data-model')
            }
        }).done(function (resp) {
            $(obj).attr('readonly', true);
            if (resp.status == 'success') {
                generateAlert('success', Translate.SeoAnalyzer.rowSaved);
                $(obj).siblings(_this.class.appendClass).find('.actions').hide();
                _this.refreshRaport(resp, $(obj).attr('data-id'));
            } else {
                generateAlert('error', Translate.SeoAnalyzer.rowNotSaved);
            }
        });
    },
    refreshRaport: function (resp, id) {
        var o = $("#sa-" + id),
            tmp = '';

        o.removeClass('panel-danger').removeClass('panel-success').removeClass('panel-warning');
        o.addClass('panel-' + resp.data.statusLevel);
        o.find('.from').text(resp.data.from);
        o.find('.to').text(resp.data.to)
        o.find("input[name='" + resp.field + "']").val(resp.value);

        for (var key in resp.data.report) {
            tmp += '<li class="text-' + key + '">' + resp.data.report[key].join('<br />') + '</li>';
        }
        o.find('.ranking-list').html(tmp);
    }
};

$(document).ready(function () {
    XEditable.init().listener();
});

var Datetime = {
    enabled: false,
    domElements: {
        standard: '.datetime, #datetimelogs',
        time: '.time',
        timeFrom: '.time-from',
        timeTo: '.time-to',
        endDay: '#createdTo, #dateTo'
    },
    format: {
        datetime: 'YYYY-MM-DD HH:mm:ss',
        time: 'HH:mm'
    },
    
    init: function () {
        if (typeof jQuery.fn.datetimepicker === 'function') {
            this.enabled = true;
        }
        
        if (this.enabled) {
            this.elements();
        } else {
            this.devError();
        }
    },
    devError: function () {
        console.error('Datetimepicker:', 'Please include datetimepicker plugin.');
    },
    elements: function () {
        var date = new Date();
        var month = date.getMonth();
        var day = date.getDate();
        var year = date.getFullYear();
        
        if ($(this.domElements.standard).length > 0) {
            $(this.domElements.standard).datetimepicker({
                format: this.format.datetime
            });
        }
        if ($(this.domElements.time).length > 0) {
            $(this.domElements.time).datetimepicker({
                format: this.format.time,
                defaultDate: new Date(year, month, day, 07, 00)
            });
        }
        if ($(this.domElements.timeFrom).length > 0) {
            $(this.domElements.timeFrom).datetimepicker({
                format: this.format.time,
                defaultDate: new Date(year, month, day, 08, 00)
            });
        }
        if ($(this.domElements.timeTo).length > 0) {
            $(this.domElements.timeTo).datetimepicker({
                format: this.format.time,
                defaultDate: new Date(year, month, day, 16, 00)
            });
        }
        if ($(this.domElements.endDay).length > 0) {
            $(this.domElements.endDay).datetimepicker({
                format: this.format.datetime,
                defaultDate: new Date(year, month, day, 23, 59)
            });
        }
    }
};

$(document).ready(function () {
    Datetime.init();
});
var DatePicker = {
    enabled: false,
    domElements: {
        standard: '.date'
    },
    format: 'YYYY-MM-DD',
    init: function () {
        if (typeof jQuery.fn.datetimepicker === 'function') {
            this.enabled = true;
        }
        
        if (this.enabled) {
            this.elements();
        } else {
            this.devError();
        }
    },
    devError: function () {
        console.error('Datepicker:', 'Please include datetimepicker plugin.');
    },
    elements: function () {
        if ($(this.domElements.standard).length > 0) {
            $(this.domElements.standard).datetimepicker({
                format: this.format
            });
        }
    }
};

$(document).ready(function () {
    DatePicker.init();
});
/**
 * Magnific Popup
 *
 * @type object
 * @see http://dimsemenov.com/plugins/magnific-popup/documentation.html
 */

var MagnificPopup = {
    enabled: false,
    classImage: '.magnific-image',
    classGallery: '.magnific-gallery',
    init: function () {
        var magnificImageAvailable = $(this.classImage).length > 0;
        var magnificGalleryAvailable = $(this.classGallery).length > 0;

        if (typeof jQuery.fn.magnificPopup === 'function') {
            this.enabled = true;
        }
        
        if (this.enabled) {
            if (magnificImageAvailable) {
                $(this.classImage).magnificPopup({type: 'image'});
            }
            if (magnificGalleryAvailable) {
                $(this.classGallery).magnificPopup({
                    delegate: 'div.widget-card-box > a',
                    type: 'image',
                    gallery: {
                        enabled: true
                    }
                });
            }
        } else if (magnificImageAvailable || magnificGalleryAvailable) {
            this.devError();
        }
    },
    devError: function () {
        console.error('Magnific Popup:', 'Please include Magnific Popup plugin.');
    }
};

$(document).ready(function () {
    MagnificPopup.init();
});
var CodeEditor = {
    enabled: false,
    class: '.code-mirror',
    options: {
        lineNumbers: true,
        mode: 'css'
    },
    hiddenElements: [],
    hiddenElementsInterval: 400,
    instances: [],
    init: function () {
        if ($(this.class).length > 0) {
            this.initialize($(this.class));
            this.checkHiddenElements();
        }
    },
    devError: function () {
        console.error('CodeMirror:', 'Please include CodeMirror plugin.');
    },
    initialize: function (object) {
        if (typeof CodeMirror === 'function') {
            this.enabled = true;
        }
        
        if (!this.enabled) {
            this.devError();
        }

        if (this.enabled && object.length == 1) {
            this.fromTextArea(object[0], this.options);
        } else if (this.enabled && object.length > 1) {
            $.each(object, function(i, textarea) {
                CodeEditor.fromTextArea(textarea, CodeEditor.options);
            });
        }
    },
    fromTextArea: function (element, options) {
        if (this.isElementVisible(element)) {
            var object = $(element);
            if (object.data('readonly')) {
                options['readOnly'] = object.data('readonly');
            }
            if (object.data('mode')) {
                options['mode'] = object.data('mode');
            }
            var codeMirrorInstance = CodeMirror.fromTextArea(element, options);
            this.instances[$(element).attr('id')] = codeMirrorInstance;
            
            return codeMirrorInstance;
        } else {
            this.hiddenElements.push(element);
        }
    },
    isElementVisible: function (element) {
        return $(element).is(':visible');
    },
    checkHiddenElements: function () {
        setInterval(function () {
            if (CodeEditor.hiddenElements.length > 0) {
                $.each(CodeEditor.hiddenElements, function (i, element) {
                    if (CodeEditor.isElementVisible(element)) {
                        CodeEditor.fromTextArea(element, CodeEditor.options);
                        delete CodeEditor.hiddenElements[i];
                    }
                });
            }
        }, this.hiddenElementsInterval);
    }
};

$(document).ready(function () {
    CodeEditor.init();
});
var PageFiles = {
    enabled: false,
    class: '.page-files',
    init: function () {
        if ($(this.class).length > 0) {
            this.enabled = true;
        }

        if (this.enabled) {
            this.listeners();
            this.sortable();
        }
    },
    listeners: function () {
        $(document).on('click', this.class + ' .status', function () {
            var id = $(this).data('id');
            PageFiles.switchStatus(id);
        });
    },
    sortable: function () {
        $(this.class).sortable({
            scroll: true,
            tolerance: "pointer",
            revert: false,
            deactivate: function (event, ui) {
                var tab = [];
                var position = 0;
                var pageId = $(this).data('page-id');

                $(this).find('.widget-card-box').each(function () {
                    tab[position] = $(this).data('id');
                    position++;
                });
                PageFiles.saveGalleryOrder(pageId, tab);
            }
        });
        $(this.class).disableSelection();
    },
    saveGalleryOrder: function (pageId, tab) {
        $.ajax({
            type: "POST",
            url: adminPrefix + "/files/saveGalleryFotoOrder",
            data: {'data': tab, 'page_id': pageId},
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success') {
                    Notifications.create('success', 'Gallery order has been updated.');
                } else {
                    Notifications.create('error', 'Gallery order has not been updated.');
                }
            }, error: function () {
                Notifications.create('error', 'An error occured while updating order of files');
            }
        });
    },
    switchStatus: function (id) {
        $.ajax({
            type: "POST",
            url: adminPrefix + "/files/switchStatus",
            data: {'id': id},
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success') {
                    var icon = $("#file-" + id + " .status > i").removeClass('fa-check').removeClass('fa-times');
                    var switchClass = (response.data.active == 1 ? 'fa-check' : 'fa-times');
                    icon.addClass(switchClass);

                    if (response.data.active) {
                        Notifications.create('success', 'File #' + id + ' has been activated');
                    } else {
                        Notifications.create('success', 'File #' + id + ' has been deactivated');
                    }
                }
            }, error: function () {
                Notifications.create('error', 'An error occured while changing active state of file #' + id);
            }
        });
    }
};

$(document).ready(function () {
    PageFiles.init();
});
var CustomCSS = {
    enabled: false,
    codeMirrorCurrent: null,
    codeMirrorVersion: null,
    cssCurrent: 'css-current',
    cssVersion: 'css-version',
    cssDiffView: 'diff',
    cssDiff: 'css-diff',
    init: function () {
        if ($('.' + this.cssCurrent).length > 0 && $('.' + this.cssVersion).length > 0) {
            this.enabled = true;
        }
        if (this.enabled) {
            if (CodeEditor.instances.length > 0 && CodeEditor.instances[this.cssCurrent] instanceof CodeMirror) {
                this.codeMirrorCurrent = CodeEditor.instances[this.cssCurrent];
            }
            if (CodeEditor.instances.length > 0 && CodeEditor.instances[this.cssVersion] instanceof CodeMirror) {
                this.codeMirrorVersion = CodeEditor.instances[this.cssVersion];
            }

            this.listeners();
            this.activeDiffWrapper();
        }
    },
    listeners: function () {
        if (this.codeMirrorCurrent !== null && this.codeMirrorVersion !== null) {
            this.codeMirrorCurrent.on('change', function () {
                CustomCSS.activeDiffWrapper(CustomCSS.codeMirrorCurrent, CustomCSS.codeMirrorVersion);
            });
            this.codeMirrorVersion.on('change', function () {
                CustomCSS.activeDiffWrapper(CustomCSS.codeMirrorCurrent, CustomCSS.codeMirrorVersion);
            });
        }
    },
    activeDiffWrapper: function (primary, secondary) {
        if (primary instanceof CodeMirror && secondary instanceof CodeMirror) {
            $(".diffWrapper").prettyTextDiff({
                originalContent: primary.getValue(),
                changedContent: this.convertString(secondary.getValue()),
                diffContainer: '.' + CustomCSS.cssDiffView
            });
        } else {
            $(".diffWrapper").prettyTextDiff({
                originalContent: $('#' + this.cssCurrent).val() || '/* custom.css */',
                changedContent: $('#' + this.cssVersion).val() || '/* custom.css */',
                diffContainer: '.' + CustomCSS.cssDiffView
            });
        }
        var diff = $('.' + CustomCSS.cssDiffView).html();
        if (diff.length <= 0) {
            diff = '/* custom.css */';
        }
        $('.' + CustomCSS.cssDiff).val(diff);
    },
    convertString: function (string) {
        var string = string.replace(/<span>/g, '');
        string = string.replace(/<\/span>/g, '');
        string = string.replace(/<ins>/g, '');
        string = string.replace(/<\/ins>/g, '');
        string = string.replace(/<del>/g, '');
        string = string.replace(/<\/del>/g, '');
        string = string.replace(/\\r\\n/g, '');
        string = string.replace(/&gt;/g, '>');
        string = string.replace(/&lt;/g, '<');
        string = string.replace(/<br\s*\/?>/mg, "\n");
        return string;
    }
};

$(document).ready(function () {
    CustomCSS.init();
});



var Cropp = {
    enabled: false,
    cropper: null,
    submit: false,
    class: {
        imageSource: '.img-crop',
        imagePreview: '.img-preview',
        form: '.form-crop',
        inputImage: '.input-image',
        btnSave: '.btn-save',
        btnZoomIn: '.btn-zoom-in',
        btnZoomOut: '.btn-zoom-out',
        btnRotateLeft: '.btn-rotate-left',
        btnRotateRight: '.btn-rotate-right',
        btnRatio: '.btn-ratio',
    },
    rotateDegree: 15,
    options: {
        preview: '.img-preview',
        viewMode: 2,
    },
    init: function () {
        if (typeof Cropper === 'function') {
            this.enabled = true;
        }

        var image = $(this.class.imageSource);
        if (image.length > 0) {
            this.create(image[0], this.options);
        }

        this.listeners();
    },
    listeners: function () {
        if (this.enabled) {
            $(document).on('click', this.class.btnSave, function (event) {
                var croppedImage = Cropp.cropper.getCroppedCanvas().toDataURL();
                $(Cropp.class.inputImage).val(croppedImage);                
                $(Cropp.class.form).submit();
            });
            $(document).on('click', this.class.btnZoomIn, function () {
                Cropp.cropper.zoom(0.1);
            });
            $(document).on('click', this.class.btnZoomOut, function () {
                Cropp.cropper.zoom(-0.1);
            });
            $(document).on('click', this.class.btnRotateLeft, function () {
                Cropp.cropper.rotate(-Cropp.rotateDegree);
            });
            $(document).on('click', this.class.btnRotateRight, function () {
                Cropp.cropper.rotate(Cropp.rotateDegree);
            });
            $(document).on('click', this.class.btnRatio, function () {
                var ratio = $(this).data('ratio');
                Cropp.cropper.setAspectRatio(ratio);
                $(Cropp.class.btnRatio).removeClass('active');
                $(this).addClass('active');
            });
        }
    },
    create: function (image, options) {
        if (this.enabled) {
            this.cropper = new Cropper(image, options);
            this.scalePreviewImage(image);
            
            if (typeof aspectRatioView != 'undefined') {
                Cropp.cropper.setAspectRatio(aspectRatioView);
            }
        }
    },
    scalePreviewImage: function (image) {
        image.addEventListener('crop', function (event) {
            var maxWidth = $(Cropp.class.imagePreview).width();
            var width = event.detail.width / 2;
            var height = event.detail.height / 2;
            var ratio = maxWidth * 100 / width / 100;

            width = width * ratio;
            height = height * ratio;

            $(Cropp.class.imagePreview).css("width", width + 'px');
            $(Cropp.class.imagePreview).css("height", height + 'px');
            $(Cropp.class.imagePreview).css("min-height", height + 'px');
        });
    }
};

$(document).ready(function () {
    Cropp.init();
});
var AppDashboard = {
    id: {
        websiteAnalytics: '#website-analytics',
        detailAnalytics: '#detail-analytics',
        plotTooltip: 'plot-tooltip',
    },
    colors: {
        users: COLOR_GREEN_DARKER,
        newUsers: COLOR_GREEN_LIGHTER,
        sessions: COLOR_BLUE_LIGHTER,
        avgSessionDuration: COLOR_BLUE_DARKER,
        bounces: COLOR_ORANGE_DARKER,
        bounceRate: COLOR_ORANGE_LIGHTER,
        pageviews: COLOR_PURPLE_LIGHTER,
    },
    sparklineOptions: {
        width: '100%',
        height: '23px',
        fillColor: 'transparent',
        type: 'line',
        lineWidth: 2,
        spotRadius: '4',
        lineColor: COLOR_BLUE,
        highlightLineColor: COLOR_BLUE,
        highlightSpotColor: COLOR_BLUE,
        spotColor: false,
        minSpotColor: false,
        maxSpotColor: false
    },
    init: function () {
        this.handleWebsiteAnalytics();
        this.handleDetailAnalytics();
    },
    getWebsiteAnalyticsData: function () {
        return $.ajax({
            url: adminPrefix + '/dashboard/get-website-analytics-data.json',
            method: 'GET'
        });
    },
    getDetailAnalyticsData: function () {
        return $.ajax({
            url: adminPrefix + '/dashboard/get-detail-analytics-data.json',
            method: 'GET'
        });
    },
    handleWebsiteAnalytics: function () {
        var websiteAnalytics = $(AppDashboard.id.websiteAnalytics);
        if (typeof $.plot == 'function' && websiteAnalytics.length === 1) {
            Loading.create(websiteAnalytics);
            $.when(this.getWebsiteAnalyticsData()).then(function (results, textStatus, jqXHR) {
                if (textStatus == 'success') {
                    Loading.remove(websiteAnalytics);
                    AppDashboard.plotInit(websiteAnalytics, results);
                }
            });
        }
    },
    plotInit: function (obj, results) {
        var details = this.formatData(results);
        var previousPoint = null;

        $.plot(obj, details.data, details.config);
        obj.bind('plothover', function (event, pos, item) {
            $("#x").text(pos.x.toFixed(2));
            $("#y").text(pos.y.toFixed(2));
            if (item) {
                if (previousPoint !== item.dataIndex) {
                    previousPoint = item.dataIndex;
                    $('#' + AppDashboard.id.plotTooltip).remove();
                    var y = item.datapoint[1].toFixed(2);

                    var content = item.series.label + " " + y;
                    AppDashboard.showTooltip(item.pageX, item.pageY, content);
                }
            } else {
                $('#' + AppDashboard.id.plotTooltip).remove();
                previousPoint = null;
            }
            event.preventDefault();
        });
    },
    formatData: function (data) {
        var details = {data: [], config: {}};
        var xLabel = [];
        var yMax = 20;

        $.each(data.results, function (index, array) {
            details.data.push({
                data: array.data,
                label: array.title,
                color: AppDashboard.colors[index],
                lines: {show: true, fill: false, lineWidth: 2},
                points: {show: true, radius: 3, fillColor: COLOR_WHITE},
                shadowSize: 0
            });

            if (xLabel.length == 0) {
                xLabel = array.xLabel;
            }
            if (array.maxValue > yMax) {
                yMax = parseInt(array.maxValue) + (parseInt(array.maxValue) * 0.1);
                yMax = Math.ceil(yMax / 100) * 100;
            }
        });

        details.config = {
            xaxis: {ticks: xLabel, tickDecimals: 0, tickColor: COLOR_BLACK_TRANSPARENT_2},
            yaxis: {ticks: 10, tickColor: COLOR_BLACK_TRANSPARENT_2, min: 0, max: yMax},
            grid: {
                hoverable: true,
                clickable: true,
                tickColor: COLOR_BLACK_TRANSPARENT_2,
                borderWidth: 1,
                backgroundColor: 'transparent',
                borderColor: COLOR_BLACK_TRANSPARENT_2
            },
            legend: {
                labelBoxBorderColor: COLOR_BLACK_TRANSPARENT_2,
                margin: 10,
                noColumns: 1,
                show: true
            }
        };

        return details;
    },
    showTooltip: function (x, y, contents) {
        $('<div id="' + AppDashboard.id.plotTooltip + '" class="flot-tooltip">' + contents + '</div>').css({
            top: y - 45,
            left: x - 55
        }).appendTo("body").fadeIn(200);
    },
    handleDetailAnalytics: function () {
        var detailAnalytics = $(AppDashboard.id.detailAnalytics);
        if (detailAnalytics.length === 1) {
            Loading.create(detailAnalytics);
            $.when(this.getDetailAnalyticsData()).then(function (results, textStatus, jqXHR) {
                if (textStatus == 'success') {
                    AppDashboard.renderDetails(detailAnalytics, results);
                }
            });
        }
    },
    renderDetails: function (obj, results) {
        $.when(this.detailHtml(results)).then(function (html) {
            Loading.remove(obj);
            obj.append(html);
            $.each(results.results, function (index, array) {
                AppDashboard.sparklineInit(index, array.data)
            });
        });
    },
    detailHtml: function (data) {
        var html = '';
        html += '<div class="table-responsive">';
        html += '<table class="table table-valign-middle">';
        html += '<thead><tr>';
        html += '<th>Source</th>';
        html += '<th>Total</th>';
        html += '<th>Trend</th>';
        html += '</tr></thead>';
        html += '<tbody>';

        $.each(data.results, function (index, array) {
            html += '<tr>';
            html += '<td><label class="label" style="background: ' + AppDashboard.colors[index] + '; color: #ffffff">' + array.title + '</label></td>';
            switch (array.type) {
                case 'PERCENT':
                    html += '<td>' + Math.round(array.value * 100) / 100 + '%</td>';
                    break;
                case 'TIME':
                    html += '<td>' + Math.round(array.value * 100) / 100 + 's</td>';
                    break;
                default:
                    html += '<td>' + array.value + '</td>';
                    break;
            }
            html += '<td><div id="sparkline-' + index + '" class="sparkline-chart"></div></td>';
            html += '</tr>';
        });

        html += '</tbody>';
        html += '</table>';
        html += '</div>';

        return html;
    },
    sparklineInit: function (sparklineId, data) {
        var options = $.extend({}, this.sparklineOptions);
        var sparklineObj = $('#sparkline-' + sparklineId);

        if (sparklineObj.length == 1) {
            var countWidth = sparklineObj.width();
            if (countWidth >= 200) {
                options.width = '200px';
            } else {
                options.width = '100%';
            }

            var color = AppDashboard.colors[sparklineId];

            if (color != 'undefined') {
                options.lineColor = color;
                options.highlightLineColor = color;
                options.highlightSpotColor = color;
            }

            sparklineObj.sparkline(data, options);
        }
    }
};

$(document).ready(function () {
    AppDashboard.init();
});
var Attributes = {
    classes: {
        type: '.property-type',
        attributes: '.attributes'
    },
    init: function () {
        if ($(Attributes.classes.type).length > 0 && $(Attributes.classes.attributes).length > 0) {
            this.listeners();
        }
    },
    listeners: function () {
        $(document).on('change', this.classes.type, function(event) {
            Attributes.get($(this));
        });
    },
    get: function(obj) {
        var typeId = obj.val();
        
        Loading.create($(Attributes.classes.attributes));
        $.when(this.getData(typeId)).then(function (results) {
            if (results.status == 'success') {
                Attributes.parseData(results.data, typeId);
            } else {
                if (results.message != '') {
                    Notifications.create('error', results.message);
                }
                Loading.remove($(Attributes.classes.attributes));
            }
        });
    },
    getData: function (typeId) {
        return $.ajax({
            url: adminPrefix + '/property-attributes/get-by-type/' + typeId + '.json',
            method: 'GET'
        });
    },
    parseData: function (data, typeId) {
        var html = '';
        $(this.classes.attributes).html(html);
        $.each(data, function (index, details) {
            html += Attributes.generateHtml(index, details);
        });
        
        if (html == '') {
            html = '<input type="hidden" name="property_attributes[]" class="form-control" id="property-attributes-id" value="">';
            if (typeId == '') {
                html += '<p class="alert alert-info">' + Translate.propertyAttributes.pleaseSelect + '</p>';
            } else {
                html += '<p class="alert alert-warning">' + Translate.propertyAttributes.noAttributes + '</p>';
            }
        }
        
        $(this.classes.attributes).html(html);
        Loading.remove($(Attributes.classes.attributes));
    },
    generateHtml: function (index, details) {
        var html = '';
        
        html += '<input type="hidden" name="property_attributes[' + index + '][id]" class="form-control" id="property-attributes-' + index + '-id" value="' + details.id + '">';
        html += '<div class="checkbox checkbox-css">';
        html += '<input type="hidden" name="property_attributes[' + index + '][_joinData][attribute_value]" class="form-control" value="0">';
        html += '<input type="checkbox" name="property_attributes[' + index + '][_joinData][attribute_value]" value="1" id="property-attributes-' + index + '-joindata-attribute-value">';
        html += '<label class="form-check-label" for="property-attributes-' + index + '-joindata-attribute-value">' + details.name + '</label>';
        html += '</div>';
        
        return html;
    }
}

$(document).ready(function() {
    Attributes.init();
});
var Contain = {
    enabled: false,
    maxRows: null,
    classes: {
        contains: '.contains',
        containRow: '.row',
        containFormGroup: '.form-group',
        containAdd: '.add-contain',
        containRemove: '.remove-contain',
        btnSuccess: '.btn-success',
        btnWarning: '.btn-warning',
        icoPlus: '.fa-plus',
        icoTimes: '.fa-times'
    },
    init: function () {
        if ($(this.classes.contains).length > 0) {
            this.enabled = true;
        }
        if (this.enabled) {
            this.setMaxRows();
            this.listeners();
            this.disableFirstFormButton();
        }
    },
    listeners: function () {
        $(this.classes.containAdd).on('click', function () {
            Contain.addContain();
        });
        $(document).on('click', this.classes.containRemove, function () {
            Contain.removeContain($(this));
        });
    },
    setMaxRows: function () {
        this.maxRows = $(this.classes.contains).data('max-rows');

        if (typeof this.maxRows == 'undefined') {
            this.maxRows = null;
        }
    },
    getRows: function () {
        return $(this.classes.contains + ' ' + this.classes.containRow).length;
    },
    addContain: function () {
        if (this.maxRows == null || this.getRows() < this.maxRows) {
            var contains = $(this.classes.contains + ' ' + this.classes.containRow);
            var containsCount = contains.length;
            var clonedRow = contains.first().clone(true);
            clonedRow.find('input').attr('value', '');
            clonedRow.find(this.classes.containRemove).attr('disabled', false);

            clonedRow = clonedRow
                    .prop('outerHTML')
                    .replace(/\[\d\]/g, '[' + containsCount + ']')
                    .replace(this.getClassName(this.classes.icoPlus), this.getClassName(this.classes.icoTimes))
                    .replace(this.getClassName(this.classes.btnSuccess), this.getClassName(this.classes.btnWarning))
                    .replace(this.getClassName(this.classes.containAdd), this.getClassName(this.classes.containRemove));

            $(this.classes.contains).append(clonedRow);
        } else {
            Notifications.create('warning', 'Max no. rows limit is set to ' + this.maxRows)
        }
    },
    removeContain: function (obj) {
        if ($(this.classes.contains + ' ' + this.classes.containFormGroup).length > 1) {
            obj.closest(this.classes.containRow).remove();
        }
    },
    disableFirstFormButton: function () {
        $(this.classes.contains + ' ' + this.classes.containRow).first().find(this.classes.containRemove).attr('disabled', true);
    },
    getClassName: function (className) {
        return className.replace('.', '');
    }
};

$(document).ready(function () {
    Contain.init();
});
var Drop = {
    enabled: false,
    class: '.dropzone',
    config: {
        maxFiles: 10,
        maxFilesize: 10,
        paramName: 'file',
    },
    init: function () {
        if (typeof Dropzone === 'function') {
            this.enabled = true;
            
            Dropzone.autoDiscover = false;
        }
        
        if (this.enabled && $(this.class).length > 0) {
            this.setOptions();
            this.listeners();
        }
    },
    listeners: function () {
        Dropzone.instances[0].on('queuecomplete', function () {
            Drop.refreshPage();
        });
    },
    getOptions: function () {
        var config = $.extend({}, this.config);
        var element = $(this.class);
        var paramName = element.attr('data-param-name') || this.config.paramName;
        
        config.paramName = paramName;
        
        return config;
    },
    setOptions: function () {
        Dropzone.instances[0].options = $.extend(Dropzone.instances[0].options, this.getOptions());
    },
    refreshPage: function () {
        location.reload();
    }
}

$(document).ready(function () {
    Drop.init();
});
var ACL = {
    classes: {
        checkInput: '.acl-check',
        toggleModule: '.acl-toggle-module',
        toggleAll: '.acl-toggle-all',
        toggleIndex: '.acl-toggle-index',
        toggleAdd: '.acl-toggle-add',
        toggleEdit: '.acl-toggle-edit',
        toggleDelete: '.acl-toggle-delete',
    },
    init: function () {
        this.listeners();
    },
    listeners: function () {
        $(document).on('click', this.classes.toggleModule, function () {
            ACL.toggleModule(this);
        });
        $(document).on('change', this.classes.checkInput, function () {
            var obj = $(this);
            if (typeof obj.data('related') != 'undefined') {
                ACL.toggleRelated(obj);
            }
        });
        $(document).on('click', this.classes.toggleAll, function () {
            ACL.toggleSpecific($(this), 'all');
        });
        $(document).on('click', this.classes.toggleIndex, function () {
            ACL.toggleSpecific($(this), 'index');
        });
        $(document).on('click', this.classes.toggleAdd, function () {
            ACL.toggleSpecific($(this), 'add');
        });
        $(document).on('click', this.classes.toggleEdit, function () {
            ACL.toggleSpecific($(this), 'edit');
        });
        $(document).on('click', this.classes.toggleDelete, function () {
            ACL.toggleSpecific($(this), 'delete');
        });
    },
    toggleModule: function (element) {
        $(element).parent().parent().find('input:checkbox').not(element).prop('checked', element.checked);
    },
    toggleRelated: function (obj) {
        var related = JSON.parse(atob(obj.data('related')));
        var name = obj.attr('name').split('[');
        var checked = obj.prop('checked');

        $.each(related, function (k, v) {
            var inputName = name[0] + '[' + v + ']';
            $("input[name='" + inputName + "']").prop('checked', checked);
        });
    },
    toggleSpecific: function (obj, type) {
        var value = obj.data('value');
        var element = this.classes.checkInput;
        if (type != 'all') {
            element = element + '-' + type;
        }

        $(element).prop('checked', value);
        $(element).trigger('change');
        $('.acl-toggle-' + type).data('value', !value);
    }
};

$(document).ready(function () {
    ACL.init();
})


$(document).on('click', '.approve-reject-button', function () {
    var id = $(this).data('id');
    $('input[type=radio][name=state]').prop('checked', false);
    if (!$('.reject-reason').hasClass('d-none')) {
        $('.reject-reason').addClass('d-none');
    }
    $('.approve-reject-modal, .article-id').val(id);
    $('input[type=radio][name=state]').change(function () {
        if (this.value === 'REJECTED') {
            $('.reject-reason').removeClass('d-none');
        } else {
            $('.reject-reason').addClass('d-none');
        }
    });
});

$(document).on('click', '.approve-reject-member-button', function () {
    var id = $(this).data('id');
    
    $('input[type=radio][name=state]').prop('checked', false);
    if (!$('.reject-reason').hasClass('d-none')) {
        $('.reject-reason').addClass('d-none');
    }
    $('.approve-reject-member-modal, .user-id').val(id);
    $('input[type=radio][name=state]').change(function () {
        if (this.value === 'REJECTED') {
            $('.reject-reason').removeClass('d-none');
            $('#reject-reason').prop('required', true);
        } else {
            $('.reject-reason').addClass('d-none');
            $('#reject-reason').prop('required', false);
        }
    });
});

$(document).on('click', '.approve-reject-property-button', function () {
    var id = $(this).data('id');
    $('input[type=radio][name=state]').prop('checked', false);
    if (!$('.reject-reason').hasClass('d-none')) {
        $('.reject-reason').addClass('d-none');
    }
    $('.approve-reject-property-modal, .property-id').val(id);
    $('input[type=radio][name=state]').change(function () {
        if (this.value === 'REJECTED') {
            $('.reject-reason').removeClass('d-none');
        } else {
            $('.reject-reason').addClass('d-none');
        }
    });
});

$(document).on('click', '.approve-reject-service-button', function () {
    var id = $(this).data('id');
    $('input[type=radio][name=state]').prop('checked', false);
    if (!$('.reject-reason').hasClass('d-none')) {
        $('.reject-reason').addClass('d-none');
    }
    $('.approve-reject-service-modal, .service-id').val(id);
    $('input[type=radio][name=state]').change(function () {
        if (this.value === 'REJECTED') {
            $('.reject-reason').removeClass('d-none');
        } else {
            $('.reject-reason').addClass('d-none');
        }
    });
});

/**
 * eConnect4u
 */
var Generate = {
    enabled: false,
    length: 10,
    characters: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
    classes: {
        button: '.generate',
        target: '.code'
    },
    init: function () {
        if ($(this.classes.button).length > 0) {
            this.enabled = true;
        }

        if (this.enabled) {
            this.listeners();
        }
    },
    listeners: function () {
        $(document).on('click', this.classes.button, function () {
            Generate.setCode($(this));
        });
    },
    generateCode: function (length, characters) {
        var result = '';
        length = length || this.length
        characters = characters || this.characters;
        var charactersLength = characters.length;

        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

        return result;
    },
    setCode: function (obj) {
        length = obj.data('length') || this.classes.length;
        characters = obj.data('characters') || this.classes.characters;
        target = obj.data('target') || this.classes.target;

        var code = this.generateCode(length, characters);

        $(target).val(code);
    }
}

$(document).ready(function () {
    Generate.init();
});



var Main = {
    originalOutput: '',
    currentInstance: null,
    init: function () {
        Main.listeners();
        
        Main.dragAndDropMenu();
        //autoload
        Main.activateTabs();
        // output initial seriali1zed data
        if ($('#menu-page').length > 0) {
            Main.updateOutput($('#menu-page').data('output', $('.menu-page-output')));
        }
    },
    listeners: function () {
        $(document)
                .on('change', '.select-menu', Main.updateSelectPage)                        // Pages/add.ctp
                .on('change', '.select-pagetype', Main.checkRedirectionType)                // Pages/add.ctp
                .on('click', '#savePageButton', Main.handleRequiredFields)                  // Pages/editcontents.ctp
                .on('click', '.btn-details-toggle', Main.detailsToggle)                     // Menus/view.ctp
//                .on('click', '#openMediaLibrary', Main.openMediaLibrary)
                .on('keyup', "#name", Main.updateUrlname)                                   // global
                .on('keyup', "#title", Main.updateUrlname)                                  // global
                .on('keyup', "#company", Main.updateUrlname);                               // global
    },
    detailsToggle: function (event) {
        var target = $('.btn-details-toggle'),
                targetText = target.find('span'),
                content = $('.page-contents'),
                text = target.hasClass('active') ? target.data('show') : target.data('hide');

        targetText.html(text);
        content.toggleClass('d-none');
        target.toggleClass('active');
    },
    handleRequiredFields: function () {
        var error = 0,
                tab = null;
        $(':input[required]').each(function () {
            var $this = $(this),
                    value = $this.val();
            if (value.trim() === '') {
                error = 1;
                tab = $this.closest('.tab-pane').attr('id');
                $this.css('border', '1px solid red').attr('placeholder', 'This field is required');
                return false;
            }
        });
        if (error) {
            $('.nav-tabs a[href="#' + tab + '"]').tab('show');
            return false;
        }
    },
    updateSelectPage: function (event) {
        var target = $(event.currentTarget),
                value = target.val(),
                model = target.attr('data-model');

        //change parent element (select)
        Main.getSelectEntitiesByAjax(model, value);
    },
    checkRedirectionType: function (event) {
        var target = $(event.currentTarget),
                value = target.val(),
                pageRedirection = $('.page-redirection');

        // display redirection tab on select page change
        if (value === '4202ef115ebede37eb22297113f5fb32') {  // md5('Redirect')
            pageRedirection.removeClass('hidden');
        } else {
            pageRedirection.addClass('hidden');
        }
    },
    activateTabs: function () {
        var url = document.location.toString();
        if (url.match('#')) {
            $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
        }
        $('.nav-tabs a').on('shown.bs.tab', function (e) {
            // window.location.hash = e.target.hash;
            var currentID = $('.tab-pane.active').find('textarea').attr('id');
            Main.currentInstance = currentID;
        });

        //set active ckeditor
        var currentID = $('.tab-pane.active').find('textarea').attr('id');
        Main.currentInstance = currentID;
    },
    dragAndDropMenu: function () {
        if ($('#menu-page').length > 0) {
            $('#menu-page').nestable({
                handleClass: 'dd-handle-custom',
                group: 1
            }).on('change', Main.updateOutput);
        }
    },
    //update Menu page
    updateOutput: function (e) {
        var list = e.length ? e : $(e.target),
                output = list.data('output');
        if (window.JSON) {
            page = window.JSON.stringify(list.nestable('serialize')); //, null, 2));
            if (typeof output !== 'undefined') {
                output.val(page);
            }
            // set the original page json string for comparing
            if (Main.originalOutput.length === 0) {
                Main.originalOutput = page;
            }
            // enable/disable update page button
            pageUpdateForm = $('page-update-form');
            pageUpdateButton = $('.pages-update');

            if (page !== Main.originalOutput) {
                pageUpdateButton
                        .removeClass('disabled')
                        .removeClass('btn-default')
                        .addClass('btn-primary')
                        .removeAttr('disabled');
            } else {
                pageUpdateButton
                        .addClass('disabled')
                        .addClass('btn-default')
                        .removeClass('btn-primary')
                        .attr('disabled', 'disabled');
            }
        } else {
            output.val('JSON browser support required.');
        }
    },
    getSelectEntitiesByAjax: function (controller, id) {
        $.ajax({
            type: 'POST',
            url: adminPrefix + '/' + controller + '/ajax-list',
            data: {'id': id},
            success: function (data) {
                selectEntities = $('select.' + controller + '');
                selected = selectEntities.find(":selected");
                var slct = new Array();
                for (s = 0; s < selected.length; s++) {
                    slct[selected[s]['value']] = selected[s]['text'];
                }
                for (i = 0; i < selectEntities.length; i++) {
                    selectEntities[i].options.length = 1;
                    for (c = 0; c < data.length; c++) {
                        option = data[c];
                        if (typeof (slct[option.value]) != 'undefined') {
                            selectEntities[i].options.add(new Option(option.text, option.value, true, true));
                        } else {
                            selectEntities[i].options.add(new Option(option.text, option.value));
                        }
                    }
                }
            }
        });
    },
//    openMediaLibrary: function () {
//        var iframe = $("<iframe id='fm-iframe' class='fm-modal'/>").attr({
//            src: 'panel/fileBrowser' + // Change it to wherever  Filemanager is stored.
//                    '?CKEditorFuncNum=' + CKEDITOR.instances[event.editor.name]._.filebrowserFn +
//                    '&CKEditorCleanUpFuncNum=' + cleanUpFuncRef +
//                    '&langCode=en' +
//                    '&CKEditor=' + event.editor.name
//        });
//
//        $("body").append(iframe);
//        $("body").css("overflow-y", "hidden");  // Get rid of possible scrollbars in containing document
//    },
    updateUrlname: function (event) {
        if ($("#urlname").length == 1) {
            $("#urlname").val(Main.slug($(this).val()));
        }
        if ($("#slug").length == 1) {
            $("#slug").val(Main.slug($(this).val()));
        }
    },
    slug: function (s, opt) {
        s = String(s);
        opt = Object(opt);

        var defaults = {
            'delimiter': '-',
            'limit': undefined,
            'lowercase': true,
            'replacements': {},
            'transliterate': (typeof (XRegExp) === 'undefined') ? true : false
        };

        // Merge options
        for (var k in defaults) {
            if (!opt.hasOwnProperty(k)) {
                opt[k] = defaults[k];
            }
        }

        var char_map = {
            // Latin
            'À': 'A', 'Á': 'A', 'Â': 'A', 'Ã': 'A', 'Ä': 'A', 'Å': 'A', 'Æ': 'AE', 'Ç': 'C',
            'È': 'E', 'É': 'E', 'Ê': 'E', 'Ë': 'E', 'Ì': 'I', 'Í': 'I', 'Î': 'I', 'Ï': 'I',
            'Ð': 'D', 'Ñ': 'N', 'Ò': 'O', 'Ó': 'O', 'Ô': 'O', 'Õ': 'O', 'Ö': 'O', 'Ő': 'O',
            'Ø': 'O', 'Ù': 'U', 'Ú': 'U', 'Û': 'U', 'Ü': 'U', 'Ű': 'U', 'Ý': 'Y', 'Þ': 'TH',
            'ß': 'ss',
            'à': 'a', 'á': 'a', 'â': 'a', 'ã': 'a', 'ä': 'a', 'å': 'a', 'æ': 'ae', 'ç': 'c',
            'è': 'e', 'é': 'e', 'ê': 'e', 'ë': 'e', 'ì': 'i', 'í': 'i', 'î': 'i', 'ï': 'i',
            'ð': 'd', 'ñ': 'n', 'ò': 'o', 'ó': 'o', 'ô': 'o', 'õ': 'o', 'ö': 'o', 'ő': 'o',
            'ø': 'o', 'ù': 'u', 'ú': 'u', 'û': 'u', 'ü': 'u', 'ű': 'u', 'ý': 'y', 'þ': 'th',
            'ÿ': 'y',
            // Latin symbols
            '©': '(c)',
            // Greek
            'Α': 'A', 'Β': 'B', 'Γ': 'G', 'Δ': 'D', 'Ε': 'E', 'Ζ': 'Z', 'Η': 'H', 'Θ': '8',
            'Ι': 'I', 'Κ': 'K', 'Λ': 'L', 'Μ': 'M', 'Ν': 'N', 'Ξ': '3', 'Ο': 'O', 'Π': 'P',
            'Ρ': 'R', 'Σ': 'S', 'Τ': 'T', 'Υ': 'Y', 'Φ': 'F', 'Χ': 'X', 'Ψ': 'PS', 'Ω': 'W',
            'Ά': 'A', 'Έ': 'E', 'Ί': 'I', 'Ό': 'O', 'Ύ': 'Y', 'Ή': 'H', 'Ώ': 'W', 'Ϊ': 'I',
            'Ϋ': 'Y',
            'α': 'a', 'β': 'b', 'γ': 'g', 'δ': 'd', 'ε': 'e', 'ζ': 'z', 'η': 'h', 'θ': '8',
            'ι': 'i', 'κ': 'k', 'λ': 'l', 'μ': 'm', 'ν': 'n', 'ξ': '3', 'ο': 'o', 'π': 'p',
            'ρ': 'r', 'σ': 's', 'τ': 't', 'υ': 'y', 'φ': 'f', 'χ': 'x', 'ψ': 'ps', 'ω': 'w',
            'ά': 'a', 'έ': 'e', 'ί': 'i', 'ό': 'o', 'ύ': 'y', 'ή': 'h', 'ώ': 'w', 'ς': 's',
            'ϊ': 'i', 'ΰ': 'y', 'ϋ': 'y', 'ΐ': 'i',
            // Turkish
            'Ş': 'S', 'İ': 'I', 'Ç': 'C', 'Ü': 'U', 'Ö': 'O', 'Ğ': 'G',
            'ş': 's', 'ı': 'i', 'ç': 'c', 'ü': 'u', 'ö': 'o', 'ğ': 'g',
            // Russian
            'А': 'A', 'Б': 'B', 'В': 'V', 'Г': 'G', 'Д': 'D', 'Е': 'E', 'Ё': 'Yo', 'Ж': 'Zh',
            'З': 'Z', 'И': 'I', 'Й': 'J', 'К': 'K', 'Л': 'L', 'М': 'M', 'Н': 'N', 'О': 'O',
            'П': 'P', 'Р': 'R', 'С': 'S', 'Т': 'T', 'У': 'U', 'Ф': 'F', 'Х': 'H', 'Ц': 'C',
            'Ч': 'Ch', 'Ш': 'Sh', 'Щ': 'Sh', 'Ъ': '', 'Ы': 'Y', 'Ь': '', 'Э': 'E', 'Ю': 'Yu',
            'Я': 'Ya',
            'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'yo', 'ж': 'zh',
            'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o',
            'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'c',
            'ч': 'ch', 'ш': 'sh', 'щ': 'sh', 'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu',
            'я': 'ya',
            // Ukrainian
            'Є': 'Ye', 'І': 'I', 'Ї': 'Yi', 'Ґ': 'G',
            'є': 'ye', 'і': 'i', 'ї': 'yi', 'ґ': 'g',
            // Czech
            'Č': 'C', 'Ď': 'D', 'Ě': 'E', 'Ň': 'N', 'Ř': 'R', 'Š': 'S', 'Ť': 'T', 'Ů': 'U',
            'Ž': 'Z',
            'č': 'c', 'ď': 'd', 'ě': 'e', 'ň': 'n', 'ř': 'r', 'š': 's', 'ť': 't', 'ů': 'u',
            'ž': 'z',
            // Polish
            'Ą': 'A', 'Ć': 'C', 'Ę': 'e', 'Ł': 'L', 'Ń': 'N', 'Ó': 'o', 'Ś': 'S', 'Ź': 'Z',
            'Ż': 'Z',
            'ą': 'a', 'ć': 'c', 'ę': 'e', 'ł': 'l', 'ń': 'n', 'ó': 'o', 'ś': 's', 'ź': 'z',
            'ż': 'z',
            // Latvian
            'Ā': 'A', 'Č': 'C', 'Ē': 'E', 'Ģ': 'G', 'Ī': 'i', 'Ķ': 'k', 'Ļ': 'L', 'Ņ': 'N',
            'Š': 'S', 'Ū': 'u', 'Ž': 'Z',
            'ā': 'a', 'č': 'c', 'ē': 'e', 'ģ': 'g', 'ī': 'i', 'ķ': 'k', 'ļ': 'l', 'ņ': 'n',
            'š': 's', 'ū': 'u', 'ž': 'z'
        };

        // Make custom replacements
        for (var k in opt.replacements) {
            s = s.replace(RegExp(k, 'g'), opt.replacements[k]);
        }

        // Transliterate characters to ASCII
        if (opt.transliterate) {
            for (var k in char_map) {
                s = s.replace(RegExp(k, 'g'), char_map[k]);
            }
        }

        // Replace non-alphanumeric characters with our delimiter
        var alnum = (typeof (XRegExp) === 'undefined') ? RegExp('[^a-z0-9]+', 'ig') : XRegExp('[^\\p{L}\\p{N}]+', 'ig');
        s = s.replace(alnum, opt.delimiter);

        // Remove duplicate delimiters
        s = s.replace(RegExp('[' + opt.delimiter + ']{2,}', 'g'), opt.delimiter);

        // Truncate slug to max. characters
        s = s.substring(0, opt.limit);

        // Remove delimiter from ends
        s = s.replace(RegExp('(^' + opt.delimiter + '|' + opt.delimiter + '$)', 'g'), '');

        return opt.lowercase ? s.toLowerCase() : s;
    }
};

$(document).ready(function () {
    Main.init();
});

/**
 * eConnect4u
 */

var Discounts = {
    enabled: false,
    classes: {
        type: '.discount-type',
        amount: '.discount-amount'
    },
    init: function () {
        if ($(this.classes.type).length > 0) {
            this.enabled = true;
        }

        if (this.enabled) {
            Discounts.setByType(Discounts.getType($(this.classes.type)));
            this.listeners();
        }
    },
    listeners: function () {
        $(document).on('change', this.classes.type, function () {
            Discounts.setByType(Discounts.getType($(this)));
        });
    },
    getType: function (obj) {
        return obj.val();
    },
    setByType: function (type) {
        this.resetAmountField();
        
        switch (type) {
            case 'FIXEDSUM':
                this.setFixedSumAmountField();
                break;
            case 'FREE': 
                this.setFreeAmountField();
                break;
            case 'PERCENTAGE':
                this.setPercentageAmountField();
                break;
        }
    },
    resetAmountField: function() {
        $(this.classes.amount).attr('min', 0);
        $(this.classes.amount).attr('max', '');
        $(this.classes.amount).attr('step', '1');
        $(this.classes.amount).attr('readonly', false);
    },
    setFixedSumAmountField: function () {
        $(this.classes.amount).attr('step', '0.01');
    },
    setFreeAmountField: function () {
        $(this.classes.amount).val('');
        $(this.classes.amount).attr('readonly', true);
    },
    setPercentageAmountField: function () {
        $(this.classes.amount).attr('max', 100);
    },
}


$(document).ready(function () {
    Discounts.init();
});

$(document).ready(function () {
    var
        $form = $('#report-settings'),
        $button = $('#report-submit');
    $form.submit(function(e) {
        e.preventDefault();
        $button.prop('disabled', true).children('.s').removeClass('hide').next('span').text('Working ...');
        $.post('/' + adminPrefix + '/reports/generate', $form.serialize(), function(data) {
            $button.prop('disabled', false).children('.s').addClass('hide').next('span').text('Generate and download');
            if (data.success) {
                Notifications.create('success', data.msg);
                location.href = '/' + adminPrefix + data.data.url + '?' + Math.random() * 10e15;
            } else {
                Notifications.create('error', data.msg);
            }
        }, 'json');
    });
});
