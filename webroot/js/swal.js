var swalMessage = {};

swalMessage.display = function (message, type) {
    swal({html: true,title:message, type: type});
};

swalMessage.dialog = function (title, action) {
    swalMessage({
        title: title,
        html: true,
        type: "warning",
        showCancelButton: true,
        cancelButtonText: 'Cancel',
        confirmButtonColor: "#ec4758",
        confirmButtonText: "Yes!",
        closeOnConfirm: false
    }, function () {
        var form = $('form[name="' + action + '"]');
        if (typeof form !== 'undefined') {
            form.submit();
        }
    });
};


