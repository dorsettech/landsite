var formBuilderListener = {
    files: {}
};

formBuilderListener.init = function () {
    formBuilderListener.handleSubmit();
};

formBuilderListener.handleSubmit = function () {
    $(document).on('submit', '.formBuilder', function (event) {
        event.preventDefault();
        var form = new FormData($(this)[0]);
        formBuilderListener.sendForm(form);
    });
};

formBuilderListener.sendForm = function (form) {
    $.ajax({
        url: '/forms_data/save',
        type: 'post',
        data: form,
        cache: false,
        dataType: 'json',
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        beforeSend: function () {
            $('.formBuilderLoading').html('<i class="fa fa-spinner fa-pulse"></i> ');
        },
        success: function (data) {
            if (data.status) {
                if (data.status === 200) {
                    swalMessage.display(data.message, 'success');
                    $('.formBuilder')[0].reset();
                } else if (data.status === 500) {
                    swalMessage.display(data.message, 'error');
                }else if(data.status === 301){
                    window.location.href = data.url;
                }
            } else {
                swalMessage.display('An error occurred. Try again', 'error');
            }
        },
        error: function (requestObject, error, errorThrown) {
            console.log('request: ' + requestObject);
            console.log('Error: ' + error);
            console.log('error thrown: ' + errorThrown);
            swalMessage.display('Form has\'t been send. Try again.', 'error');
        },
        complete: function () {
            $('.formBuilderLoading').find('.fa-spinner').remove();
        }
    });
};


$(document).ready(formBuilderListener.init);