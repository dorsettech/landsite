<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-12-10)
 * @version 1.0
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Filesystem\File;
use Cake\I18n\I18n;
use stdClass;

if (!defined('AJAX_STATUS_OK')) {
    define('AJAX_STATUS_OK', 'success');
}
if (!defined('AJAX_STATUS_FAIL')) {
    define('AJAX_STATUS_FAIL', 'fail');
}
if (!defined('AJAX_STATUS_ERROR')) {
    define('AJAX_STATUS_ERROR', 'error');
}

/**
 * Application Controller
 */
class AppController extends Controller
{
    /**
     * Default layout
     *
     * @var string
     */
    public $layoutDefault = 'default';

    /**
     * Layout exception
     *
     * @var string
     */
    public $layoutExtension = 'ctp';

    /*
     * Service name
     *
     * @var string
     */
    public $service = '';

    /**
     * Application version
     *
     * @var string
     */
    public $version = '';

    /**
     * Redirection flag
     *
     * @var boolean
     */
    private $redirect = false;

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Languages');
        $this->session = $this->request->getSession();
        $this->setLocale();
        $this->fetchConfigTable();
        $this->checkRedirections();
        $this->setVersion();
        $this->setViewVars();
    }

    /**
     * Set redirection
     *
     * @param mixed $data Redirect data.
     *
     * @return void
     */
    public function setRedirect($data)
    {
        $this->redirect = $data;
    }

    /**
     * Get redirection
     *
     * @return boolean
     */
    public function getRedirect()
    {
        return $this->redirect;
    }

    /**
     * Check redirections
     *
     * @return void
     */
    public function checkRedirections()
    {
        $url = $this->getRequest()->getRequestTarget();
        if ($url) {
            $this->loadModel('Redirections');
            $redirection = $this->Redirections->find('all')->where(['redirection_from' => $url])->first();
            if (!empty($redirection)) {
                $this->redirect($redirection->redirect_to, 301);
                $this->setRedirect(true);
            }
        }
    }

    /**
     * Set app version
     *
     * Sets application version from VERSION.txt file located in main app directory
     *
     * @return void
     */
    public function setVersion(): void
    {
        $versionFile = new File(ROOT . DS . 'VERSION.txt');

        $this->version = $versionFile->read();
    }

    /**
     * Set view vars
     *
     * @return void
     */
    public function setViewVars()
    {
        $this->service = !empty($_service = $this->getDbConf('service')) ? $_service : $this->getConf('Website.service');
        $this->set('_service', $this->service);
        $this->set('_copy', (!empty($_copy = $this->getDbConf('copy')) ? $_copy : $this->getConf('Website.copy')));
        $this->set('_projectUrl', ($this->getConf('Website.url')));
        $this->set('_membersUrl', ($this->getConf('Website.members_url')));
        $this->set('_version', $this->version);
    }

    /**
     * Load configuration values from the 'Static contents' (Layout variables)
     *
     * @return void
     */
    private function fetchConfigTable()
    {
        $this->loadModel('StaticContentGroups');
        $config = $this->StaticContentGroups->getConfigurationValues()->toArray();
        if (isset($config[0]) && isset($config[0]['static_contents']) && !empty($config[0]['static_contents'])) {
            foreach ($config[0]['static_contents'] as $value) {
                Configure::write('dbconf.' . $value->var_name, $value->value);
            }
        }
    }

    /**
     * Get db conf
     *
     * @param string|null $string Config section name.
     *
     * @return mixed
     */
    public function getDbConf($string)
    {
        return $this->getConf('dbconf.' . $string);
    }

    /**
     *
     * Get config
     *
     * @param string|null $string Key to read.
     *
     * @return mixed
     */
    public function getConf($string)
    {
        return Configure::read($string);
    }

    /**
     * Set layout
     *
     * @param string|null $layout Layout name.
     *
     * @return void
     */
    public function setLayout($layout = 'default')
    {
        $this->viewBuilder()->setLayout($layout);
    }

    /**
     * Set locale
     *
     * @return void
     */
    protected function setLocale()
    {
        if ($this->session->check('lang')) {
            I18n::setLocale($this->session->read('lang'));
        } else {
            $this->setDefaultLanguage();
        }

        $this->set('_languages', $this->Languages->getAvalibleLanguages());
    }

    /**
     * Set default language
     *
     * @return void
     */
    public function setDefaultLanguage()
    {
        $locale = $this->Languages->getDefaultLanguage();
        if (!!$locale && !$this->session->check('lang')) {
            $this->session->write('lang', $locale->locale);
        }
        $this->setLocale();
    }

    /**
     * Array to object converter
     *
     * @param array|null $array   Array to conversion.
     * @param array|null $options Additional options.
     *
     * @return stdClass
     */
    public function arrayToObject($array, $options)
    {
        $object = new stdClass();

        $emptyValues = (isset($options['emptyValues'])) ? $options['emptyValues'] : false;
        $defaultValues = (isset($options['defaultValues'])) ? $options['defaultValues'] : false;

        if (!empty($array) && is_array($array)) {
            foreach ($array as $key => $value) {
                if ($emptyValues) {
                    $object->$value = '';
                } elseif ($defaultValues) {
                    $object->$value = isset($defaultValues[$value]) ? $defaultValues[$value] : '';
                } else {
                    $object->$key = $value;
                }
            }
        }

        return $object;
    }

    /**
     * Get possible file size and types
     *
     * @param string|null $type File type.
     *
     * @return array
     */
    public function getPossibleFileSizeAndTypes($type)
    {
        $configValue = $this->getDbConf('available_' . $type . '_types');
        $imageIndicator['available_' . $type . '_types'] = ($configValue !== "" && $configValue !== null) ? explode('|', $configValue) : '';
        $imageIndicator[$type . '_max_size'] = $this->getDbConf($type . '_max_size') / 1024 / 1024; // in MB
        if ($type === 'image') {
            $imageIndicator['image_max_width'] = $this->getDbConf('image_max_width');
            $imageIndicator['image_max_width'] = $this->getDbConf('image_max_height');
        }

        return $imageIndicator;
    }

    /**
     * Remove file
     *
     * @param integer|string $file File id or file path.
     *
     * @return void
     */
    public function removeFile($file)
    {
        if (is_numeric($file)) {
            $this->loadModel('files');
            $entity = $this->files->get($file);
            $file = $entity->path;
            $this->files->delete($entity);
        }
        unlink($file);
    }

    /**
     * Change column value
     *
     * @return string
     */
    public function changeColumnValue()
    {
        $this->autoRender = false;
        $response = [
            'status' => false,
            'message' => __('Cannot update field.'),
            'value' => ''
        ];

        $model = $this->request->getParam('controller');
        $this->loadModel($model);
        $data = $this->request->getData();

        $entity = $this->{$model}->get($data['id']);
        if ($entity) {
            $entity = $this->{$model}->patchEntity($entity, $data);
            if ($this->{$model}->save($entity)) {
                $response = [
                    'status' => true,
                    'message' => __('Update field OK'),
                    'value' => (is_array($data) ? array_pop($data) : '')
                ];
            }
        }
        return $this->response->withType('application/json')
                              ->withStringBody(json_encode($response));
    }
}
