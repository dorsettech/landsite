<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-08)
 * @version 1.0
 */

namespace App\Controller;

use Cake\Event\Event;

/**
 * Error Handling Controller
 */
class ErrorController extends AppController
{
    /**
     * Initialization hook method.
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->setLayout('error');
        $this->loadModel('staticContents');
        $this->setStatics();
        $this->setMenus();
    }

    /**
     * Before render callback.
     *
     * @param Event $event Event.
     *
     * @return void
     */
    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);

        $this->viewBuilder()->setTemplatePath('Error');
    }

    /**
     * Set statics
     *
     * @return void
     */
    public function setStatics()
    {
        $this->loadModel('staticContents');
        $contents = $this->staticContents->find('all');
        $data = [];
        foreach ($contents as $content) {
            $data[$content->var_name] = $content->value;
        }

        $this->set('statics', $data);
    }

    /**
     * Set menus
     *
     * @return void
     */
    public function setMenus()
    {
        $this->loadModel('Menus');
        $menus = $this->Menus->find('all');
        $parsedMenus = [];
        foreach ($menus as $menu) {
            $parsedMenus[$menu->urlname] = $this->Menus->getPages($menu->urlname, 'all');
        }

        $this->set('menus', $parsedMenus);
    }
}
