<?php /** @noinspection CurlSslServerSpoofingInspection */

namespace App\Controller\Api;

use App\Mime;
use App\Model\Entity\PropertyMedia;
use App\Model\Enum\AdType;
use App\Model\Enum\PropertyPriceQualifier;
use App\Model\Enum\PropertyPurpose;
use App\Model\Enum\PropertyStatus;
use App\Model\Enum\Rightmove\BranchChannel;
use App\Model\Enum\Rightmove\CommonUseClass as CommonUseClassRightmove;
use App\Model\Enum\Rightmove\MediaType as MediaTypeRightmove;
use App\Model\Enum\Rightmove\PropertyPriceQualifier as PropertyPriceQualifierRightmove;
use App\Model\Enum\Rightmove\PropertyStatus as PropertyStatusRightmove;
use App\Model\Enum\Rightmove\PropertyType as PropertyTypeRightmove;
use App\Model\Enum\Rightmove\RentFrequency;
use App\Model\Table\PropertiesTable;
use App\Model\Table\PropertyMediaTable;
use App\RequestType;
use App\Settings;
use App\Strings;
use Cake\Core\Configure;
use Cake\I18n\Time;
use DateTime;
use Exception;

/**
 * API Property Controller
 *
 * @property PropertiesTable    $Properties
 * @property PropertyMediaTable $PropertyMedia
 *
 * @package App\Controller\Api
 */
class PropertyController extends ApiAppController
{

    public const MSG_REQ_FIELD = 'The required data is missing: %s';

    private $maxFileSize;
    private $mimeAllowed;
    /**
     * @var array|null
     */
    private $validity;

    /**
     * @return void
     * @throws Exception Inherited.
     */
    public function initialize(): void
    {
        parent::initialize();

        Configure::load('website');

        $this->maxFileSize = Configure::read('Media.max_image_size');
        $this->mimeAllowed = Configure::read('Media.mime_types_allowed');

        $this->validity = Settings::getGroup('validity');

        $this->loadModel('Properties');
        $this->loadModel('PropertyMedia');

        if ($this->PropertyMedia->hasBehavior('Upload')) {
            $this->PropertyMedia->removeBehavior('Upload');
        }
        if ($this->PropertyMedia->hasBehavior('Log')) {
            $this->PropertyMedia->removeBehavior('Log');
        }
        if ($this->Properties->hasBehavior('Log')) {
            $this->Properties->removeBehavior('Log');
        }
    }

    /**
     * @return mixed
     * @throws Exception Inherited.
     * @api  POST /v1/property/getbranchpropertylist
     */
    public function getbranchpropertylist()
    {
        $this->allowMethod(RequestType::POST);
        $rawInput = $this->Data->all();
        if (isset($rawInput['getBranchPropertyListForm'])) {
            $input = $rawInput['getBranchPropertyListForm'];
        } elseif (isset($rawInput['root'])) {
            $input = $rawInput['root'];
        } else {
            $input = $rawInput;
        }

        $checkResult = $this->checkMandatoryFieldsForGetBranch($input);
        if ($checkResult !== true) {
            return $this->fail([], $checkResult);
        }

        $user = $this->Auth->getUser();

        $isSale = isset($input['branch']['channel']) && (int)$input['branch']['channel'] === BranchChannel::SALES;

        $properties = $this->Properties->find('all', [
            'conditions' => [
                'user_id' => $user->get('id'),
                'purpose' => $isSale ? PropertyPurpose::SALE : PropertyPurpose::RENT
            ],
            'fields' => [
                'id', 'reference'
            ]
        ]);

        $this->success([
            'branch' => $input['branch'],
            'property' => $properties
        ]);

        $this->dumpLog($rawInput);
    }

    /**
     * @param array $input Input data.
     *
     * @return boolean|string
     */
    private function checkMandatoryFieldsForGetBranch(array $input)
    {
        if (!isset($input['branch'])) {
            return sprintf(self::MSG_REQ_FIELD, 'branch');
        }
        if (!isset($input['branch']['channel'])) {
            return sprintf(self::MSG_REQ_FIELD, 'branch/channel');
        }
        return true;
    }

    /**
     * @return mixed
     * @throws Exception Inherited.
     * @api  POST /v1/property/removeproperty
     */
    public function removeproperty()
    {
        $this->allowMethod(RequestType::POST);
        $rawInput = $this->Data->all();
        if (isset($rawInput['removePropertyForm'])) {
            $input = $rawInput['removePropertyForm'];
        } elseif (isset($rawInput['root'])) {
            $input = $rawInput['root'];
        } else {
            $input = $rawInput;
        }

        $user = $this->Auth->getUser();

        $checkResult = $this->checkMandatoryFieldsForRemoveProperty($input);
        if ($checkResult !== true) {
            return $this->fail([], $checkResult);
        }

        $isSale = (int)$input['branch']['channel'] === BranchChannel::SALES;

        $property = $this->Properties->find('all', [
            'conditions' => [
                'user_id' => $user->get('id'),
                'purpose' => $isSale ? PropertyPurpose::SALE : PropertyPurpose::RENT,
                'reference' => $input['property']['agent_ref']
            ]
        ])->first();

        if (!$property) {
            return $this->fail([], 'The Property does not exists was removed.');
        }

        $result = $this->Properties->delete($property);

        $property = [
            'agent_ref' => $property->get('reference'),
            'id' => $property->get('id')
        ];

        if ($result) {
            $this->success([
                'property' => $property
            ]);
        } else {
            $this->fail([
                'property' => $property
            ], 'The Property cannot be removed.');
        }

        $this->dumpLog($rawInput);
    }

    /**
     * @param array $input Input data.
     *
     * @return boolean|string
     */
    private function checkMandatoryFieldsForRemoveProperty(array $input)
    {
        if (!isset($input['branch'])) {
            return sprintf(self::MSG_REQ_FIELD, 'branch');
        }
        if (!isset($input['branch']['channel'])) {
            return sprintf(self::MSG_REQ_FIELD, 'branch/channel');
        }
        if (!isset($input['property'])) {
            return sprintf(self::MSG_REQ_FIELD, 'property');
        }
        if (!isset($input['property']['agent_ref'])) {
            return sprintf(self::MSG_REQ_FIELD, 'property/agent_ref');
        }
        return true;
    }

    /**
     * Add new property to the system.
     *
     * @return mixed
     * @throws Exception Inherited.
     * @api  POST /v1/property/sendpropertydetails
     */
    public function sendpropertydetails()
    {
        $this->allowMethod(RequestType::POST);

        $rawInput = $this->Data->all();

        if (isset($rawInput['addUpdatePropertyForm'])) {
            $input = $rawInput['addUpdatePropertyForm'];
        } elseif (isset($rawInput['root'])) {
            $input = $rawInput['root'];
        } else {
            $input = $rawInput;
        }
        $user = $this->Auth->getUser();

        $checkResult = $this->checkMandatoryFieldsForSendProperty($input);
        if ($checkResult !== true) {
            return $this->fail([], $checkResult);
        }

        $isSale = (int)$input['branch']['channel'] === BranchChannel::SALES;

        $title = PropertyTypeRightmove::getNamesList()[$input['property']['property_type']] . ($isSale ? ' for sale' : ' to let') . ' in ' . $input['property']['address']['town'];

        $property = [
            'reference' => $input['property']['agent_ref'] ?? null,
            'user_id' => $user->get('id'),
            'title' => $title,
            'slug' => Strings::utf8Deaccent($title, '-'),
            'purpose' => $isSale ? PropertyPurpose::SALE : PropertyPurpose::RENT,
            'status' => $input['property']['published'] ? PropertyStatus::PUBLISHED : PropertyStatus::DRAFT,
            'type_id' => PropertyTypeRightmove::toTheLandsiteValue($input['property']['property_type']),
            'under_offer' => PropertyStatusRightmove::toTheLandsiteValue($input['property']['status']),
            'location' => $input['property']['address']['display_address'],
            'town' => $input['property']['address']['town'],
            'postcode' => $input['property']['address']['postcode_1'] . $input['property']['address']['postcode_2'],
            'headline' => Strings::wordSubstr(Strings::strip($input['property']['details']['summary']), 997),
            'description' => mb_strlen($input['property']['details']['description']) > 32000 ?
                mb_substr($input['property']['details']['description'], 0, 31997) . '...' :
                $input['property']['details']['description']
        ];

        if (empty($property['reference'])) {
            $isNew = true;
            $entity = $this->Properties->newEntity();
        } else {
            $entity = $this->Properties->find()->where([
                'reference' => $property['reference'],
                'user_id' => $user->get('id')
            ])->orderDesc('id')->first();

            if ($entity) {
                $isNew = false;
            } else {
                $isNew = true;
                $entity = $this->Properties->newEntity();
            }
        }

        if ($isNew) {
            $property['publish_date'] = new Time();
            $expiryDateTime = new Time();
            $expiryDateTime->add(new \DateInterval("P{$this->validity['property-' . strtolower(AdType::STANDARD) . '-days']}D"));
            $property['ad_expiry_date'] = $expiryDateTime;
        }

        if (isset($input['property']['address']['latitude'], $input['property']['address']['longitude'])) {
            $property['lat'] = $input['property']['address']['latitude'];
            $property['lng'] = $input['property']['address']['longitude'];
        } else {
            $latLng = $this->getLatLng($property['postcode']);
            $property['lat'] = $latLng['lat'];
            $property['lng'] = $latLng['lng'];
        }

        if ($isSale) {
            $property['price_rent'] = 0;
            $property['price_sale'] = $input['property']['price_information']['price'];
            $property['price_sale_qualifier'] = isset($input['property']['price_information']['price_qualifier']) ? PropertyPriceQualifierRightmove::toTheLandsiteValue($input['property']['price_information']['price_qualifier'], $property['purpose']) : PropertyPriceQualifier::DEFAULT;
        } else {
            $property['price_sale'] = 0;
            $property['price_rent'] = $input['property']['price_information']['price'];
            $property['price_rent_qualifier'] = isset($input['property']['price_information']['price_qualifier']) ? PropertyPriceQualifierRightmove::toTheLandsiteValue($input['property']['price_information']['price_qualifier'], $property['purpose']) : PropertyPriceQualifier::DEFAULT;
            if (isset($input['property']['price_information']['rent_frequency'])) {
                $property['price_rent_per'] = RentFrequency::toTheLandsiteValue($input['property']['price_information']['rent_frequency']);
            }
        }

        if (isset($input['property']['price_information']['auction'])) {
            $property['auction'] = $input['property']['price_information']['auction'] === 'true' || $input['property']['price_information']['auction'] === 1;
        }

        if (isset($input['property']['details']['comm_use_class'])) {
            $property['comm_use_class'] = [];
            foreach ($input['property']['details']['comm_use_class'] as $classId) {
                $property['comm_use_class'][] = CommonUseClassRightmove::toTheLandsiteValue($classId);
            }
        }

        $medias = [];
        if (isset($input['property']['media'])) {
            foreach ($input['property']['media'] as $media) {
                $mediaType = MediaTypeRightmove::toTheLandsiteValue($media['media_type']);
                if (!$mediaType || empty($media['media_url'])) {
                    continue;
                }
                $medias[] = [
                    'type' => $mediaType,
                    'url' => trim($media['media_url']),
                    'last_update' => isset($media['media_update_date']) ? DateTime::createFromFormat('d-m-Y H:i:s', $media['media_update_date']) : null,
                    'position' => $media['sort_order'] ?? 0
                ];
            }
        }

        $this->Properties->patchEntity($entity, $property);
        $result = $this->Properties->save($entity);

        if ($result) {
            $propertyId = $entity->get('id');
            $fileDir = MEMBERS_PROPERTY_FILES . $user->get('uid') . DS;

            if (!file_exists($fileDir)) {
                mkdir($fileDir, 0777, true);
            }

            if ($isNew) {
                foreach ($medias as $media) {
                    $media['result'] = $this->downloadFileFrom($media['url']);
                    if ($media['result']['success']) {
                        $fileName = str_replace('.', '', uniqid($user->get('id') . '-', true)) . '.' . Mime::mimeToExtension($media['result']['mime']);
                        if (file_put_contents($fileDir . $fileName, $media['result']['data'])) {
                            $this->PropertyMedia->save($this->PropertyMedia->newEntity([
                                'property_id' => $propertyId,
                                'type' => $media['type'],
                                'source' => $fileName,
                                'extra' => $media['result']['file'],
                                'url' => $media['url'],
                                'position' => $media['position'],
                                'modified' => $media['last_update'] instanceof DateTime ? $media['last_update'] : new DateTime()
                            ]));
                        }
                    }
                }
            } else {
                $urls = array_map(static function ($item) {
                    return $item['url'];
                }, $medias);

                // remove unused first
                $this->PropertyMedia->find()->where([
                    'property_id' => $propertyId
                ])->each(static function (PropertyMedia $entity) use ($urls, $fileDir) {
                    if (!in_array($entity->get('url'), $urls, true)) {
                        $source = $entity->get('source');
                        if ($this->PropertyMedia->delete($entity)) {
                            @unlink($fileDir . $source);
                        }
                    }
                });

                // check the rest of medias
                foreach ($medias as $media) {
                    $file = $this->PropertyMedia->find()->where([
                        'property_id' => $propertyId,
                        'url' => $media['url']
                    ])->first();
                    if ($file) {
                        // check whether changed and update
                        if ($media['last_update'] instanceof DateTime && $file->get('modified')->getTimestamp() < $media['last_update']->getTimestamp()) {
                            $media['result'] = $this->downloadFileFrom($media['url']);
                            if ($media['result']['success']) {
                                $fileName = str_replace('.', '', uniqid($user->get('id') . '-', true)) . '.' . Mime::mimeToExtension($media['result']['mime']);
                                if (file_put_contents($fileDir . $fileName, $media['result']['data'])) {
                                    $oldSource = $file->get('source');
                                    $file->set('source', $fileName);
                                    $file->set('position', $media['position']);
                                    $file->set('extra', $media['result']['file']);
                                    $file->set('modified', $media['last_update']);
                                    if ($this->PropertyMedia->save($file)) {
                                        @unlink($fileDir . $oldSource);
                                    }
                                }
                            }
                        }
                    } else {
                        // add new here
                        $media['result'] = $this->downloadFileFrom($media['url']);

                        if ($media['result']['success']) {
                            $fileName = str_replace('.', '', uniqid($user->get('id') . '-', true)) . '.' . Mime::mimeToExtension($media['result']['mime']);
                            if (file_put_contents($fileDir . $fileName, $media['result']['data'])) {
                                $this->PropertyMedia->save($this->PropertyMedia->newEntity([
                                    'property_id' => $propertyId,
                                    'type' => $media['type'],
                                    'source' => $fileName,
                                    'extra' => $media['result']['file'],
                                    'url' => $media['url'],
                                    'position' => $media['position'],
                                    'modified' => $media['last_update'] instanceof DateTime ? $media['last_update'] : new DateTime()
                                ]));
                            }
                        }
                    }
                }
            }

            $this->success([
                'property' => [
                    'agent_ref' => $property['reference'],
                    'id' => $propertyId
                ]
            ], 'Property object has been successfully save.');
        } else {
            $this->fail([
                'property' => [
                    'agent_ref' => $property['reference'],
                    'id' => $entity->get('id')
                ]
            ], $entity->getErrorsMessages(null, PHP_EOL, true));
        }

        $this->dumpLog($rawInput);
    }

    /**
     * @param array $input Input data.
     *
     * @return boolean|string
     */
    private function checkMandatoryFieldsForSendProperty(array $input)
    {
        if (!isset($input['branch'])) {
            return sprintf(self::MSG_REQ_FIELD, 'branch');
        }
        if (!isset($input['branch']['channel'])) {
            return sprintf(self::MSG_REQ_FIELD, 'branch/channel');
        }
        if (!isset($input['property'])) {
            return sprintf(self::MSG_REQ_FIELD, 'property');
        }
        if (!isset($input['property']['property_type'])) {
            return sprintf(self::MSG_REQ_FIELD, 'property/property_type');
        }
        if (!isset($input['property']['address'])) {
            return sprintf(self::MSG_REQ_FIELD, 'property/address');
        }
        if (!isset($input['property']['published'])) {
            return sprintf(self::MSG_REQ_FIELD, 'property/published');
        }
        if (!isset($input['property']['status'])) {
            return sprintf(self::MSG_REQ_FIELD, 'property/status');
        }
        if (!isset($input['property']['address']['town'])) {
            return sprintf(self::MSG_REQ_FIELD, 'property/address/town');
        }
        if (!isset($input['property']['address']['display_address'])) {
            return sprintf(self::MSG_REQ_FIELD, 'property/address/display_address');
        }
        if (!isset($input['property']['address']['postcode_1'])) {
            return sprintf(self::MSG_REQ_FIELD, 'property/address/postcode_1');
        }
        if (!isset($input['property']['address']['postcode_2'])) {
            return sprintf(self::MSG_REQ_FIELD, 'property/address/postcode_2');
        }
        if (!isset($input['property']['details'])) {
            return sprintf(self::MSG_REQ_FIELD, 'property/details');
        }
        if (!isset($input['property']['details']['summary'])) {
            return sprintf(self::MSG_REQ_FIELD, 'property/details/summary');
        }
        if (!isset($input['property']['details']['description'])) {
            return sprintf(self::MSG_REQ_FIELD, 'property/details/description');
        }
        if (!isset($input['property']['price_information'])) {
            return sprintf(self::MSG_REQ_FIELD, 'property/price_information');
        }
        if (!isset($input['property']['price_information']['price'])) {
            return sprintf(self::MSG_REQ_FIELD, 'property/price_information/price');
        }

        return true;
    }

    /**
     * @param string $postcode UK postcode.
     *
     * @return array
     */
    protected function getLatLng(string $postcode): array
    {
        $h = curl_init();
        curl_setopt($h, CURLOPT_URL, 'https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($postcode) . '&key=' . Settings::get('google-api.geocoding'));
        curl_setopt($h, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($h);
        if (!$data) {
            return [];
        }
        $data = json_decode($data, true);
        if (!$data || !isset($data['results'][0]['geometry']['location'])) {
            return ['lat' => null, 'lng' => null];
        }
        return ['lat' => $data['results'][0]['geometry']['location']['lat'], 'lng' => $data['results'][0]['geometry']['location']['lng']];
    }

    /**
     * @param string $url Media url.
     *
     * @return array
     */
    public function downloadFileFrom(string $url): array
    {
        if (!filter_var($url, FILTER_VALIDATE_URL) || parse_url($url) === false) {
            return [
                'success' => false,
                'message' => 'Bad media URL'
            ];
        }

        $h = curl_init($url);
        curl_setopt($h, CURLOPT_NOBODY, 1);
        curl_setopt($h, CURLOPT_RETURNTRANSFER, 0);
        curl_setopt($h, CURLOPT_HEADER, 0);
        curl_setopt($h, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($h, CURLOPT_MAXREDIRS, 5);
        curl_exec($h);
        $size = curl_getinfo($h, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
        $mime = curl_getinfo($h, CURLINFO_CONTENT_TYPE);
        if ($mime && strpos($mime, ';') !== false) {
            $mime = explode(';', $mime);
            $mime = $mime[0];
        }
        curl_close($h);

        if ($size < 0) {
            return [
                'success' => false,
                'msg' => 'The media URL is not valid. File size is equal or less than 0.'
            ];
        }

        if ($size > $this->maxFileSize) {
            return [
                'success' => false,
                'msg' => __('Maximum allowed size is {0} bytes', $this->maxFileSize)
            ];
        }

        if (empty($mime)) {
            return [
                'success' => false,
                'msg' => 'The media URL is not valid. Server did not return proper MIME type.'
            ];
        }

        if (!in_array($mime, array_merge($this->mimeAllowed['image'], $this->mimeAllowed['document']), true)) {
            return [
                'success' => false,
                'msg' => __('File type not allowed. Acceptable types: JPEG, PNG, WEBP, DOC, DOCX, PDF')
            ];
        }

        $h = curl_init($url);
        curl_setopt($h, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($h, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($h, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)');
        curl_setopt($h, CURLOPT_FAILONERROR, true);
        curl_setopt($h, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($h, CURLOPT_AUTOREFERER, true);
        curl_setopt($h, CURLOPT_TIMEOUT, 10);
        curl_setopt($h, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($h, CURLOPT_SSL_VERIFYPEER, false);
        $fileName = basename($url);
        curl_setopt($h, CURLOPT_HEADERFUNCTION, static function ($h, $header) use (&$fileName) {
            if (preg_match('/Content-Disposition: .*filename=([^ ]+)/', $header, $matches)) {
                $fileName = $matches[1];
            }
            return strlen($header);
        });

        $data = curl_exec($h);
        $code = curl_getinfo($h, CURLINFO_HTTP_CODE);
        curl_close($h);

        if ($code === 200) {
            return [
                'success' => true,
                'data' => $data,
                'file' => $fileName,
                'mime' => $mime
            ];
        }

        return [
            'success' => false,
            'msg' => curl_error($h)
        ];
    }
}
