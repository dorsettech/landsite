<?php

namespace App\Controller\Api;

use App\Application;
use App\RequestType;
use Cake\Core\Configure;
use Cake\Validation\Validation;
use Exception;

/**
 * Primary API methods.
 *
 * @author  Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
class ApiController extends ApiAppController
{
    /**
     * Initialize controller.
     *
     * @return void
     * @throws Exception Inherited exception.
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->Auth->allow(['version', 'login', 'token', 'hello']);
    }

    /**
     * Return API (system) version and build numbers
     *
     * @return void
     * @throws Exception Method exception.
     * @api GET /v1/version
     */
    public function version(): void
    {
        $this->allowMethod(RequestType::GET);

        $this->success([
            'Api' => [
                'production' => Application::isProduction(),
                'version' => $this->getApiVersion(),
                'build' => $this->getBuildVersion(),
                'revision' => $this->getRevisionString(true)
            ]
        ]);
    }

    /**
     * Heartbeat method to check that API works.
     *
     * @return void
     * @throws Exception Inherited exception.
     * @api GET /v1/hello
     */
    public function hello(): void
    {
        $this->allowMethod(RequestType::GET);

        $this->success([], 'hi');
    }

    /**
     * Authorization of the customer via login and password
     *
     * @return null
     * @throws Exception Inherited exception.
     * @api POST /v1/login
     */
    public function login()
    {
        $this->allowMethod(RequestType::POST);

        $login = $this->Data->get('email');
        $data = [
            'User' => [
                'email' => $login
            ]
        ];

        if (!Validation::email($login)) {
            return $this->fail($data, __('Please provide a valid login'));
        }

        $user = $this->Auth->identify();

        if ($user) {
            if (!$user['active']) {
                return $this->fail($data, __('Your account was disabled or not yet approved.'));
            }

            $this->Auth->setUser($user);

            $data['User'] = array_merge($data['User'], $user, [
                'image_url' => empty($user['image']) ? '' : Configure::read('Website.members_url') . $user['image_path']
            ]);

            unset($data['User']['id'], $data['User']['group_id'], $data['User']['image'], $data['User']['active'], $data['User']['image_path']);

            $data['Token'] = $this->Auth->authenticationProvider()->generateToken($user);

            return $this->success($data, __('You have successfully logged in.'));
        }

        return $this->fail($data, __('Incorrect credentials or API access disabled'));
    }

    /**
     * Regenerate JWT authorization token
     *
     * @return null
     * @throws Exception Inherited exception.
     * @api GET /v1/token
     */
    public function token()
    {
        $this->allowMethod(RequestType::GET);

        $user = $this->Auth->identify();

        if (!$user) {
            return $this->fail([], 'You are not authorized');
        }

        return $this->success([
            'Token' => $this->Auth->authenticationProvider()->generateToken($user)
        ]);
    }
}
