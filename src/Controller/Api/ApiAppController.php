<?php

namespace App\Controller\Api;

use App\Controller\Component\AuthComponent;
use App\Controller\Members\AppController;
use App\Mime;
use App\Model\Entity\Group;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Http\Response;
use Cake\Utility\Xml;
use DateTime;
use Exception;
use ReflectionException;
use RuntimeException;

/**
 * Base class for API inheritance.
 *
 * Authorized API controller - all controllers that inherit it,
 * needs to be authorized by client first.
 *
 * Communication standard used: JSON Web Token
 *
 * @property AuthComponent Auth
 * @package App\Controller
 * @author  Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
class ApiAppController extends AppController
{

    /**
     * Request/response timestamp field format
     */
    public const TS_FORMAT = 'd-m-Y H:i:s.u';

    /**
     * @var string Required unique ID.
     */
    public $requestId;

    /**
     * @var DateTime Request timestamp.
     */
    public $requestTs;

    /**
     * @var DateTime Response timestamp.
     */
    public $responseTs;

    /**
     * @var array Array of warnings.
     *
     * Warning:
     *  - Warning_Code (required)
     *  - Warning _Description (required)
     *  - Warning _Value (optional)
     */
    public $warnings = [];

    /**
     * @var array Array of errors.
     *
     * Error:
     *
     * - Error_Code (required)
     * - Error_Description (required)
     * - Error_Value (optional)
     */
    public $errors = [];


    /**
     * @return void
     * @throws Exception Inherited exception.
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Auth', [
            'className' => 'Auth',
            'storage' => 'Memory',
            'authenticate' => [
                'Jwt' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ],
                    'finder' => [
                        'auth' => [
                            'group_id' => Group::MEMBERS,
                            'api' => true
                        ]
                    ],
                    'userModel' => 'Users',
                    'key' => Configure::read('application.secret'),
                    'parameter' => 'token',
                    'loginAction' => 'login',
                    'tokenExpiration' => 2419200
                ]
            ],
            'checkAuthIn' => 'Controller.startup',
            'unauthorizedRedirect' => false,
            'loginAction' => false
        ]);
    }

    /**
     * @param Event $event Life cycle event.
     *
     * @return Response|void|null
     */
    public function afterFilter(Event $event)
    {
        parent::afterFilter($event);

        if (!$this->Auth->isAuthenticated()) {
            return;
        }
    }

    /**
     * @param Event $event Life cycle event.
     *
     * @return \Cake\Network\Response|void|null
     * @throws ReflectionException Inherited exception.
     */
    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);

        if ($this->request->accepts() === []) {
            $this->viewBuilder()->setLayout('ajax');
            $this->viewBuilder()->setTemplate('\Api\error');
            $this->set('message', 'No valid response type defined. We support JSON or XML format. Accept header required.');
        }
    }

    /**
     * @param Event $event Life cycle event.
     *
     * @return Response|void|null
     * @throws Exception Inherited exception.
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->requestId = str_replace('.', '-', uniqid('req-', true));
        $this->requestTs = new DateTime();
    }

    /**
     * Build success API response.
     *
     * @param array  $data    Data array.
     * @param string $message Message text.
     *
     * @return null
     * @throws Exception General exception.
     */
    public function success(array $data = [], string $message = '')
    {
        return $this->buildResponse(true, $message, $data);
    }

    /**
     * Build general API response. Response structure is based on rightmove.co.uk api (compatibility)
     *
     * @param boolean $success Success flag.
     * @param string  $message Message text.
     * @param array   $data    Response data.
     *
     * @return null
     * @throws Exception General exception.
     */
    protected function buildResponse(bool $success, string $message, array $data)
    {
        $this->responseTs = new DateTime();

        $response = array_merge([
            'request_id' => $this->requestId,
            'message' => $message,
            'success' => $success,
            'request_timestamp' => $this->requestTs->format(self::TS_FORMAT),
            'response_timestamp' => $this->responseTs->format(self::TS_FORMAT)
        ], $data);

        if ($this->warnings) {
            $response['warnings'] = $this->warnings;
        }

        if ($this->errors) {
            $response['errors'] = $this->errors;
        }

        $this->set($response);

        return null;
    }

    /**
     * Build failure API response.
     *
     * @param array  $data    Data array.
     * @param string $message Message text.
     *
     * @return null
     * @throws Exception General exception.
     */
    public function fail(array $data = [], string $message = '')
    {
        return $this->buildResponse(false, $message, $data);
    }

    /**
     * @param array $rawInput Raw input data.
     *
     * @return void
     */
    public function dumpLog(array $rawInput): void
    {
        $ext = Mime::mimeToExtension($this->request->contentType());
        $logFileName = $this->requestId . ".$ext";

        if ($ext === 'json') {
            $data = json_encode($rawInput);
        } else if ($ext === 'xml') {
            $data = Xml::build($rawInput)->saveXML();
        } else {
            $data = serialize($rawInput);
        }

        $path = API_LOGS_V1 . date('Y-m-d') . DS;

        if (!file_exists($path) && !mkdir($path, 0777, true) && !is_dir($path)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $path));
        }

        file_put_contents($path . $logFileName, $data);
    }
}
