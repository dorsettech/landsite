<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-08)
 * @version 1.0
 */

namespace App\Controller\Panel;

/**
 * PageTypes Controller
 *
 * @description Page types module.
 */
class PageTypesController extends PanelController
{
    /**
     * Index method
     *
     * @description Page types list
     * @return void
     */
    public function index()
    {
        $pageTypes = $this->paginate($this->PageTypes);

        $this->set(compact('pageTypes'));
        $this->set('_serialize', ['pageTypes']);
    }

    /**
     * Add method
     *
     * @description Create page type
     * @return Response|void
     */
    public function add()
    {
        $pageType = $this->PageTypes->newEntity();
        if ($this->request->is('post')) {
            $pageType = $this->PageTypes->patchEntity($pageType, $this->request->getData());
            if ($this->PageTypes->save($pageType)) {
                $this->Flash->success(__('The page type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The page type could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('pageType'));
    }

    /**
     * Edit method
     *
     * @description Update page type
     * @param integer|null $id Page Type id.
     * @return Response|void
     */
    public function edit($id = null)
    {
        $pageType = $this->PageTypes->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pageType = $this->PageTypes->patchEntity($pageType, $this->request->getData());
            if ($this->PageTypes->save($pageType)) {
                $this->Flash->success(__('The page type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The page type could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('pageType'));
    }

    /**
     * Delete method
     *
     * @description Delete page type
     * @param integer|null $id Page Type id.
     * @return Response
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pageType = $this->PageTypes->get($id);
        if ($this->PageTypes->delete($pageType)) {
            $this->Flash->success(__('The page type has been deleted.'));
        } else {
            $this->Flash->error(__('The page type could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
