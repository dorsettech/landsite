<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-08)
 * @version 1.0
 */

namespace App\Controller\Panel;

use App\Traits\SwitcherTrait;

/**
 * Forms Controller
 *
 * @description Forms module.
 */
class FormsController extends PanelController
{
    use SwitcherTrait;

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->set('_removeAddButton', true);
        $this->set('_removeSearch', true);
    }

    /**
     * Index method
     *
     * @description Forms list
     * @return void
     */
    public function index()
    {
        $this->set('forms', $this->paginate($this->Forms));
        $this->set('_serialize', ['forms']);
    }

    /**
     * Add method
     *
     * @description Create form
     * @return Response|void
     */
    public function add()
    {
        $form = $this->Forms->newEntity();
        if ($this->request->is('post')) {
            $patch = $this->getPatchData($this->request->getData());
            $form = $this->Forms->patchEntity($form, $patch);
            if ($this->Forms->save($form)) {
                $this->Flash->success(__('The form has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The form could not be saved. Please, try again.'));
            }
        }
        $types = $this->getAvailableTypes();
        $validations = $this->getAvailableValidators();
        $this->set(compact('form', 'types', 'validations'));
        $this->set('_serialize', ['form']);
    }

    /**
     * Edit method
     *
     * @description Update form
     * @param string|null $id Form id.
     * @return Response|void
     */
    public function edit($id = null)
    {
        if (!is_numeric($id)) {
            $this->redirect(['action' => 'index']);
        }

        $form = $this->Forms->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $patch = $this->getPatchData($this->request->getData());
            $form = $this->Forms->patchEntity($form, $patch);
            if ($this->Forms->save($form)) {
                $this->Flash->success(__('The form has been saved.'));
                return $this->redirect(['action' => 'edit', $form->id]);
            } else {
                $this->Flash->error(__('The form could not be saved. Please, try again.'));
            }
        }

        $data = [json_decode($form['data'])];
        $types = $this->getAvailableTypes();
        $validations = $this->getAvailableValidators();
        $emails = json_decode($form->emails);
        $this->set(compact('form', 'data', 'types', 'validations', 'emails'));
        $this->set('_serialize', ['form']);
    }

    /**
     * Delete method
     *
     * @description Deleted form
     * @param string|null $id Form id.
     * @return Response
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $form = $this->Forms->get($id);
        if ($this->Forms->delete($form)) {
            $this->Flash->success(__('The form has been deleted.'));
        } else {
            $this->Flash->error(__('The form could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Generate data
     *
     * @param array $data Data to analyze.
     * @return false|string
     */
    protected function generateData(array $data = [])
    {
        $length = isset($data['input_name']) ? count($data['input_name']) : 0;
        $response = [];
        for ($i = 0; $i < $length; $i++) {
            $response[isset($data['input_name'][$i]) ? $data['input_name'][$i] : $i] = [
                'label' => isset($data['label'][$i]) ? $data['label'][$i] : '',
                'validation_text' => isset($data['validation_text'][$i]) ? $data['validation_text'][$i] : '',
                'placeholder' => isset($data['placeholder'][$i]) ? $data['placeholder'][$i] : '',
                'class' => isset($data['class'][$i]) ? $data['class'][$i] : '',
                'type' => isset($data['type'][$i]) ? $data['type'][$i] : 'text',
                'value' => isset($data['value'][$i]) ? $data['value'][$i] : '',
                'validation' => isset($data['validation'][$i]) ? $data['validation'][$i] : '',
                'rule' => isset($data['rule'][$i]) ? $data['rule'][$i] : '',
                'required' => isset($data['required'][$data['input_name'][$i]]) ? $data['required'][$data['input_name'][$i]] : false
            ];
            if ($response[isset($data['input_name'][$i]) ? $data['input_name'][$i] : $i]['type'] === 'select' || $response[isset($data['input_name'][$i]) ? $data['input_name'][$i] : $i]['type'] === 'radio') {
                $response[isset($data['input_name'][$i]) ? $data['input_name'][$i] : $i]['options'] = isset($data['options'][$i]) ? json_decode($data['options'][$i]) : [];
            }
        }

        return json_encode($response);
    }

    /**
     * Get patch data
     *
     * @param array $data Data to path.
     * @return array
     */
    protected function getPatchData(array $data = [])
    {
        $patch = [];
        $patch['name'] = isset($data['name']) ? $data['name'] : '';
        $patch['alias'] = isset($data['alias']) ? $data['alias'] : '';
        $patch['html_class'] = isset($data['html_class']) ? $data['html_class'] : '';
        $patch['active'] = isset($data['active']) ? $data['active'] : '';
        $patch['send_email'] = isset($data['send_email']) ? $data['send_email'] : false;
        $patch['thank_you'] = isset($data['thank_you']) ? $data['thank_you'] : false;
        $patch['redirect_url'] = isset($data['redirect_url']) ? $data['redirect_url'] : '';
        $patch['data'] = $this->generateData($data);
        $patch['emails'] = $this->generateEmailsAsJson($data);

        return $patch;
    }

    /**
     * Generate emails as json
     *
     * @param array|null $data Data to check.
     * @return false|string
     */
    protected function generateEmailsAsJson($data)
    {
        $emails = [];
        $emails['main'] = isset($data['mainEmails']) && trim($data['mainEmails']) !== '' ? explode(',', $data['mainEmails']) : '';
        $emails['cc'] = isset($data['ccEmails']) && trim($data['ccEmails']) !== '' ? explode(',', $data['ccEmails']) : '';
        $emails['bcc'] = isset($data['bccEmails']) && trim($data['bccEmails']) !== '' ? explode(',', $data['bccEmails']) : '';

        return json_encode($emails);
    }

    /**
     * Get available types
     *
     * @return array
     */
    protected function getAvailableTypes()
    {
        return [
            'text' => 'Text',
            'textarea' => 'Textarea',
            'checkbox' => 'Checkbox',
            'radio' => 'Radio',
            'select' => 'Select',
            'file_document' => 'File (pdf, doc)',
            'image' => 'Image',
            'number' => 'Number',
            'email' => 'Email',
            'date' => 'Date',
            'dateTime' => 'DateTime',
            'submit' => 'Submit button'
        ];
    }

    /**
     * Get available validators
     *
     * @return array
     */
    protected function getAvailableValidators()
    {
        return [
            'none' => 'None',
            'max_length' => 'Max length',
            'min_length' => 'Min length',
            'max_min_length' => 'Max/Min length',
            'decimal' => 'Decimal Number',
            'phone' => 'Phone number'
        ];
    }
}
