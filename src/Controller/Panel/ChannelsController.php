<?php

namespace App\Controller\Panel;

use App\Controller\Panel\PanelController;
use App\Model\Table\ChannelsTable;
use App\Traits\GetListTrait;
use App\Traits\SwitcherTrait;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Response;

/**
 * Channels Controller
 *
 * @property ChannelsTable $Channels
 */
class ChannelsController extends PanelController
{
    /**
     * Switcher trait
     */
    use SwitcherTrait;

    /**
     * Get list trait
     */
    use GetListTrait;

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
    }

    /**
     * Index method
     *
     * @return Response|void
     */
    public function index()
    {
        $this->paginate = [
            'order' => ['Channels.position' => 'ASC', 'Channels.created' => 'DESC', 'Channels.id' => 'DESC']
        ];

        $channels = $this->paginate($this->Channels);

        $this->set(compact('channels'));
    }

    /**
     * Add method
     *
     * @return Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $channel = $this->Channels->newEntity();

        if ($this->request->is('post')) {
            $channel = $this->Channels->patchEntity($channel, $this->request->getData());

            if ($this->Channels->save($channel)) {
                $this->Flash->success(__('Channel has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Channel could not be saved. Please, try again.'));
        }

        $this->set(compact('channel'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Channel id.
     * @return Response|null Redirects on successful edit, renders view otherwise.
     * @throws RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $channel = $this->Channels->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $channel = $this->Channels->patchEntity($channel, $this->request->getData());

            if ($this->Channels->save($channel)) {
                $this->Flash->success(__('Channel has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Channel could not be saved. Please, try again.'));
        }

        $this->set(compact('channel'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Channel id.
     * @return Response|null Redirects to index.
     * @throws RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $channel = $this->Channels->get($id);

        if ($this->Channels->delete($channel)) {
            $this->Flash->success(__('Channel has been deleted.'));
        } else {
            $this->Flash->error(__('Channel could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
