<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-08)
 * @version 1.0
 */

namespace App\Controller\Panel;

use App\Traits\SwitcherTrait;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Network\Response;
use Cake\ORM\Query;

/**
 * Pages Controller
 *
 * @description Pages module.
 * @property \App\Model\Table\PagesTable $Pages
 */
class PagesController extends PanelController
{
    use SwitcherTrait;

    /**
     * Page path separator
     *
     * @var string
     */
    protected $pathSeparator = '.';

    /**
     * Errors array
     *
     * @var array
     */
    private $errors = [];

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('FileUpload', ['directory' => 'contents', 'mime' => ['image/jpeg', 'image/png']]);
    }

    /**
     * Before filter event
     *
     * @param Event $event Event.
     *
     * @return void
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $parentId = $this->request->getData('parent_id');
        if ($this->request->is(['post', 'put']) && isset($parentId) && empty($parentId)) {
            $this->request->withData('parent_id', null);
        }
    }

    /**
     * Index
     *
     * @description Pages list
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Menus', 'ParentPages', 'PageTypes'],
            'order' => [
                'Pages.menu_id' => 'ASC',
                'Pages.in_menu' => 'DESC',
                'Pages.ip+0 ASC',
                'Pages.position' => 'ASC'
            ]
        ];
        $pages = $this->paginate($this->Pages);
        $this->set(compact('pages'));
        $this->set('_serialize', ['pages']);
    }

    /**
     * Add method
     *
     * @description Create page
     * @param string|null $menuUrlname Urlname of a menu.
     * @return Response|void
     */
    public function add($menuUrlname = null)
    {
        $this->setLayout('clean');

        $menuId = '';
        if ($menuUrlname !== null) {
            $this->loadModel('Menus');
            $menu   = $this->Menus->getMenuByUrlname($menuUrlname);
            $menuId = $menu->id;
        } else {
            $menuId = 1;
        }

        $parentPages = $this->Pages->getParentPagesList($menuId);
        $page        = $this->Pages->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $page = $this->Pages->patchEntity($page, $data);

            if ($this->Pages->save($page)) {
                $this->msgSuccess(__('Page has been added.'));
                $this->recoverPage($page, '');

                $this->loadModel('Menus');
                $currentMenu = $this->Menus->getMenuById($page->menu_id);

                return $this->redirect(['prefix' => $this->adminPrefix, 'controller' => 'Pages', 'action' => 'edit-contents', $page->id]);
            } else {
                $this->msgError(__('Page could not be added.'));
            }
        }
        $this->request = $this->request->withData('position', 999);
        $this->request = $this->request->withData('active', 1);
        $this->request = $this->request->withData('seo_priority', 0.8);
        $this->request = $this->request->withData('in_menu', 1);
        $this->request = $this->request->withData('layout', 'default');
        $menus         = $this->Pages->Menus->getMenusList();
        $currentMenu   = isset($menu) ? $menu->id : '';
        $layouts       = $this->Pages->getLayoutsList();
        $types         = $this->Pages->PageTypes->find('list')->toArray();
        if (!$layouts) {
            $this->msgError(__('No templates!'));
        }
        $this->set(compact('page', 'menus', 'currentMenu', 'parentPages', 'layouts', 'types'));
        $this->set('_serialize', ['page']);
    }

    /**
     * Edit method
     *
     * @description Page edit (redirects to editContents)
     * @related editContents
     * @param integer|null $id Page id.
     * @return Response
     */
    public function edit($id = null)
    {
        if (!is_numeric($id)) {
            return $this->redirect(['action' => 'index']);
        }

        $this->redirect(['prefix' => $this->adminPrefix, 'controller' => 'Pages', 'action' => 'edit-contents', $id]);
    }

    /**
     * Edit contents
     *
     * @description Update page with contents
     * @param integer|null $id Row id.
     * @return Response|void
     */
    public function editContents($id = null)
    {
        $this->setLayout('clean');

        $page      = $this->Pages->getPageDetails($id);
        $pageOldIp = $page->ip;
        if ($this->request->is(['post', 'patch', 'put'])) {
            $data = $this->getRequest()->getData();
            foreach ($page->contents as $key => $content) {
                $contentKeys[$content->content_name] = $key;
            }

            if (isset($data['contents'])) {
                foreach ($data['contents'] as $key => $content) {
                    $pageContent = (isset($contentKeys[$content['content_name']]) && isset($page->contents[$contentKeys[$content['content_name']]])) ? $page->contents[$contentKeys[$content['content_name']]] : null;
                    if (isset($content['file_image'], $content['file_image']['name']) && !empty($content['file_image']['name'])) {
                        $data['contents'][$key]['file'] = $this->FileUpload->upload($content['file_image']);
                        if (!empty($data['contents'][$key]['file'])) {
                            $uploadedFiles[] = $data['contents'][$key]['file'];
                        }
                    }
                    if (isset($content['file_image2'], $content['file_image2']['name']) && !empty($content['file_image2']['name'])) {
                        $data['contents'][$key]['file2'] = $this->FileUpload->upload($content['file_image2']);
                        if (!empty($data['contents'][$key]['file2'])) {
                            $uploadedFiles[] = $data['contents'][$key]['file2'];
                        }
                    }
                    if (isset($content['file_image3'], $content['file_image3']['name']) && !empty($content['file_image3']['name'])) {
                        $data['contents'][$key]['file3'] = $this->FileUpload->upload($content['file_image3']);
                        if (!empty($data['contents'][$key]['file3'])) {
                            $uploadedFiles[] = $data['contents'][$key]['file3'];
                        }
                    }
                    if (isset($content['file_image4'], $content['file_image4']['name']) && !empty($content['file_image4']['name'])) {
                        $data['contents'][$key]['file4'] = $this->FileUpload->upload($content['file_image4']);
                        if (!empty($data['contents'][$key]['file4'])) {
                            $uploadedFiles[] = $data['contents'][$key]['file4'];
                        }
                    }
                    if (isset($content['file_image5'], $content['file_image5']['name']) && !empty($content['file_image5']['name'])) {
                        $data['contents'][$key]['file5'] = $this->FileUpload->upload($content['file_image5']);
                        if (!empty($data['contents'][$key]['file5'])) {
                            $uploadedFiles[] = $data['contents'][$key]['file5'];
                        }
                    }
                    if (is_object($pageContent) && $pageContent->has('id') && is_numeric($pageContent->id)) {
                        $data['contents'][$key]['id'] = $pageContent->id;
                    }
                    if (isset($content['remove_image']) && $content['remove_image']) {
                        $data['contents'][$key]['file'] = null;
                    }
                }
            }

            $page = $this->Pages->patchEntity($page, $data);

            if (!empty($page->upload_errors)) {
                $page->setErrors($page->upload_errors);
            }

            if ($this->Pages->save($page)) {
                $this->msgSuccess(__('Page saved.'));
                $this->recoverPage($page, $pageOldIp);

                $this->loadModel('Menus');
                $currentMenu = $this->Menus->getMenuById($page->menu_id);

                $close = $this->request->getData('close');
                if (isset($close) && $close) {
                    return $this->redirect(['prefix' => $this->adminPrefix, 'controller' => 'Menus', 'action' => 'view', $currentMenu->urlname]);
                }

                return $this->redirect($this->request->self);
            } else {
                $this->msgError(__('Page not saved.'));
                if (!empty($uploadedFiles)) {
                    foreach ($uploadedFiles as $key => $filePath) {
                        $this->FileUpload->removeFile($filePath);
                    }
                }
            }
        }

        $menus          = $this->Pages->Menus->getMenusList();
        $types          = $this->Pages->PageTypes->find('list')->order(['PageTypes.title' => 'ASC'])->toArray();
        $parentPages    = $this->Pages->getParentPagesList($page->menu_id);
        $layouts        = $this->Pages->getLayoutsList();
        $layoutContents = $this->Pages->getLayoutContentsList($page->layout);
        $layoutForms    = $this->Pages->generateFormContents($layoutContents);
        $contentsLayout = $this->Pages->getContentsLayout($page->contents);
        $emptyContent   = $this->arrayToObject($this->Pages->Contents->schema()->columns(), ['defaultValues' => ['visible' => true, 'position' => 999]]);

        $slider      = $this->editFiles($page->id, 1);
        $galleries   = $this->editFiles($page->id, 2);
        $attachments = $this->editFiles($page->id, 3);
        if (!$layouts) {
            $this->msgError(__('No templates!'));
        }
        $this->setHeading('title', $page->name);
        $this->setHeading('subtitle', __('Edit'));
        $this->set(compact('page', 'menus', 'parentPages', 'types', 'layouts', 'layoutContents', 'contentsLayout', 'emptyContent', 'galleries', 'attachments', 'layoutForms', 'slider'));
        $this->set('_serialize', ['page']);
    }

    /**
     * Recover pages page tree
     *
     * @param object      $page      Page object.
     * @param string|null $pageOldIp Old page ip.
     * @return void
     */
    private function recoverPage(object $page, $pageOldIp = '')
    {
        $errors = [];
        $this->Pages->recover();

        $data = $this->request->getData();
        if (empty($page->parent_id) || $page->parent_id == 0) {
            $data['ip'] = $page->id;
        } else {
            $parent = $this->Pages->get($page->parent_id);
            $data['ip'] = $parent->ip . '.' . $page->id;
        }

        $data['depth'] = substr_count($data['ip'], '.');
        $page = $this->Pages->patchEntity($page, $data);

        if (!$this->Pages->save($page, ['associated' => false])) {
            $errors[] = __('An error occured while updating page.');
        }

        if (!empty($pageOldIp) && $pageOldIp != $page->ip) {
            $updatePages = $this->Pages->find('all')->where(['Pages.ip LIKE ' => $pageOldIp . '.%']);
            $errors = [];
            foreach ($updatePages as $uPage) {
                $updateData['id'] = $uPage->id;
                $updateData['ip'] = $page->ip . '.' . str_replace($pageOldIp . '.', '', $uPage->ip);
                $updateData['depth'] = substr_count($updateData['ip'], '.');

                $uPage = $this->Pages->patchEntity($uPage, $updateData, ['validate' => false, 'associated' => false]);

                if (!$this->Pages->save($uPage)) {
                    $errors[] = __('Page {0} has not been updated.', $uPage->name);
                }
            }
        }

        if (!empty($errors)) {
            $this->msgError(implode("<br>", $errors));
        }
    }

    /**
     * Delete method
     *
     * @description Delete page
     * @param integer|null $id Page id.
     * @return Response
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $page = $this->Pages->get($id);
        if ($this->Pages->delete($page)) {
            $this->msgSuccess(__('Page removed.'));
        } else {
            $this->msgError(__('Page not removed.'));
        }

        $this->loadModel('Menus');
        $currentMenu = $this->Menus->getMenuById($page->menu_id);

        return $this->redirect(['prefix' => $this->adminPrefix, 'controller' => 'Menus', 'action' => 'view', $currentMenu->urlname]);
    }

    /**
     * Clone page
     *
     * @description Clone page
     * @param integer|null $id Page id.
     * @return Response
     */
    public function clonePage($id = null)
    {
        $suffix = $this->generateSuffix();
        $page = $this->Pages->get($id, ['contain' => ['Contents']]);
        $urlname = $page->urlname;
        $name = $page->name;
        $newPage = $this->Pages->newEntity();
        $newPage = $this->Pages->patchEntity($newPage, $page->toArray());
        $newPage['name'] = $name . '-' . $suffix;
        $newPage['urlname'] = $urlname . '-' . $suffix;
        if ($this->Pages->save($newPage)) {
            $this->msgSuccess(__('Page has been cloned.'));
        } else {
            $this->msgError(__('Page could not be cloned.'));
        }
        $this->recoverPage($newPage);
        return $this->redirect(['action' => 'editcontents', $newPage->id, '#' => 'page']);
    }

    /**
     * Generate suffix
     *
     * @return string
     */
    private function generateSuffix()
    {
        $time = new Time();
        return substr(md5($time), 0, 5);
    }

    /**
     * Update page
     *
     * @description Update page
     * @return Response|null
     */
    public function updatePage()
    {
        if ($this->request->is(['post'])) {
            $page = $this->request->getData('page');
            if (isset($page)) {
                $page = json_decode($page);
                $this->renderPage($page);

                if (!empty($this->errors)) {
                    $this->msgError(implode('<br>', $this->errors));
                } else {
                    $this->msgSuccess(__('Menu structure updated.'));
                }
                $this->Pages->recover();
            }
        }

        return $this->redirect($this->referer());
    }

    /**
     * Render page
     *
     * @param array|null   $page     Page data.
     * @param integer|null $parentId Parent id.
     * @param string|null  $path     Path.
     * @param integer|null $depth    Depth.
     * @return void
     */
    protected function renderPage($page, $parentId = null, $path = '', $depth = 0)
    {
        $position = 1;
        foreach ($page as $element) {
            $currentPath = $path . (!empty($path) ? $this->pathSeparator : '') . $element->id;

            $update = $this->updateElement($element->id, $position, $parentId, $currentPath, $depth);
            if ($update !== true) {
                $this->errors[] = $update;
            }

            if (!empty($element->children)) {
                $this->renderPage($element->children, $element->id, $currentPath, $depth + 1);
            }
            $position++;
        }
    }

    /**
     * Update element
     *
     * @param integer|null $id       Id.
     * @param integer|null $position Position.
     * @param integer|null $parentId Parent id.
     * @param string|null  $path     Path.
     * @param integer|null $depth    Depth.
     * @return string
     */
    protected function updateElement($id = 0, $position = 0, $parentId = null, $path = '', $depth = 0)
    {
        $element = $this->Pages->get($id);
        $element->parent_id = $parentId;
        $element->position = $position;
        $element->ip = $path;
        $element->depth = $depth;

        if (!$this->Pages->save($element)) {
            return __("Error during the proces of updating page {0}", $element['name']);
        }

        return true;
    }

    /**
     * Update page
     *
     * @description Update element field
     * @param integer|null $id    Page id.
     * @param string|null  $field Field name.
     * @param string|null  $value Field value.
     * @return Response
     */
    public function updateElementField($id = 0, $field = '', $value = '')
    {
        $this->request->allowMethod(['post', 'patch', 'put']);

        $page = $this->Pages->get($id);
        $pageOldIp = $page->ip;

        switch ($field) {
            case 'menu':
                $data['menu_id'] = $value;
                break;
            case 'page':
                $data['parent_id'] = $value;
                break;
            case 'layout':
                $data['layout'] = $value;
                break;
            case 'type':
                $data['type'] = $value;
                break;
            default:
                $data[$field] = $value;
                break;
        }

        if ($field == 'type' && $value != md5('Redirect')) {
            $data['redirection'] = '';
            $data['target_blank'] = 0;
        }
        $page = $this->Pages->patchEntity($page, $data);

        if (empty($page['errors'])) {
            if ($this->Pages->save($page)) {
                if ($field == 'page') {
                    $this->recoverPage($page, $pageOldIp);
                }
                $this->msgSuccess(__("Page {0} updated.", $page->name));
            } else {
                $this->msgError(__("Page {0} not updated.", $page->name));
            }
        } else {
            $this->msgError(__('Error during the proces of updating page {0}', h(implode(' ', $page['errors']))));
        }

        return $this->redirect($this->referer());
    }

    /**
     * Get list of pages by requested menu ID
     *
     * @description Get pages list (ajax)
     * @return Response
     */
    public function ajaxList()
    {
        $output = [];
        if ($this->request->is('ajax')) {
            $data = $this->request->getData();
            if (!empty($data['id']) && is_numeric($data['id'])) {
                $pages = $this->Pages->getParentPagesList($data['id'])->toArray();
                foreach ($pages as $pageId => $page) {
                    $output[] = ['value' => $pageId, 'text' => $page];
                }
            }
        }

        return $this->response->withType('application/json')
                              ->withStringBody(json_encode($output));
    }

    /**
     * Change redirection from
     *
     * @description Change redirection from
     * @param integer|null $currentId Row id.
     * @param integer|null $deletedId Row id.
     * @return Response|void
     */
    public function changeRedirectionFrom($currentId = 0, $deletedId = null)
    {
        if ($this->request->is('post')) {
            $deletedPage = ($deletedId) ? $this->Pages->find()->where(['id' => $deletedId])->first() : null;
            $currentPage = $this->Pages->find()->where(['id' => $currentId])->first();
            if ($deletedPage !== null) {
                $deletedPage->deleted_redirection = $this->Pages->generateURL($currentPage->id);
                if ($this->Pages->save($deletedPage)) {
                    $this->msgSuccess(__("Page {0} updated.", $currentPage->name));
                }
            } elseif ($deletedId === null) {
                $deletedPages = $this->Pages->find()->where(['deleted_redirection' => $this->Pages->generateURL($currentPage->id)]);
                foreach ($deletedPages as $page) {
                    $page->deleted_redirection = null;
                    $this->Pages->save($page);
                }
                $this->msgSuccess(__("Page {0} updated.", $currentPage->name));
            }
        }

        $this->redirect($this->referer());
    }

    /**
     * Edit files
     *
     * @description Edit files
     * @param integer|null $pageId Page id.
     * @param string|null  $type   Page type.
     * @return Query
     */
    public function editFiles($pageId, $type)
    {
        $this->loadModel('PageFiles');
        return $this->paginate($this->PageFiles->find('all')->where(['type' => $type, 'page_id' => $pageId])->order('position'), ['limit' => 50]);
    }
}
