<?php

namespace App\Controller\Panel;

use App\Controller\Panel\PanelController;
use App\Model\Table\CategoriesTable;
use App\Traits\GetListTrait;
use App\Traits\SwitcherTrait;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Response;

/**
 * Categories Controller
 *
 * @property CategoriesTable $Categories
 */
class CategoriesController extends PanelController
{
    /**
     * Switcher trait
     */
    use SwitcherTrait;

    /**
     * Get list trait
     */
    use GetListTrait;

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
    }

    /**
     * Index method
     *
     * @return Response|void
     */
    public function index()
    {
        $this->paginate = [
            'order' => ['Categories.position' => 'ASC', 'Categories.created' => 'DESC', 'Categories.id' => 'DESC']
        ];

        $categories = $this->paginate($this->Categories);

        $this->set(compact('categories'));
    }

    /**
     * Add method
     *
     * @return Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $category = $this->Categories->newEntity();

        if ($this->request->is('post')) {
            $category = $this->Categories->patchEntity($category, $this->request->getData());

            if ($this->Categories->save($category)) {
                $this->Flash->success(__('Category has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Category could not be saved. Please, try again.'));
        }
        
        $this->set(compact('category'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Category id.
     * @return Response|null Redirects on successful edit, renders view otherwise.
     * @throws RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $category = $this->Categories->get($id);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $category = $this->Categories->patchEntity($category, $this->request->getData());
            
            if ($this->Categories->save($category)) {
                $this->Flash->success(__('Category has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Category could not be saved. Please, try again.'));
        }
        
        $this->set(compact('category'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Category id.
     * @return Response|null Redirects to index.
     * @throws RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $category = $this->Categories->get($id);
        
        if ($this->Categories->delete($category)) {
            $this->Flash->success(__('Category has been deleted.'));
        } else {
            $this->Flash->error(__('Category could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
