<?php

/**
 * @author  Stefan
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-08)
 * @version 1.0
 */

namespace App\Controller\Panel;

use App\Library\ACL;
use Cake\Http\Response;

/**
 * Groups Controller
 *
 * @description Groups module.
 * @property \App\Model\Table\GroupsTable $Groups
 */
class GroupsController extends PanelController
{
    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadModel('Groups');
    }

    /**
     * Display list of groups
     *
     * @description Groups list
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ParentGroups'],
            'order' => ['id' => 'DESC']
        ];
        $this->set('groups', $this->paginate($this->Groups));
        $this->set('_serialize', ['groups']);
    }

    /**
     * Add a group
     *
     * @description Create group
     * @return Response|void
     */
    public function add()
    {
        $group = $this->Groups->newEntity();
        $groups = $this->Groups->getGroupsList($this->user['group_id']);

        if ($this->request->is('post')) {
            $group = $this->Groups->patchEntity($group, $this->request->getData());
            if ($this->Groups->save($group)) {
                $this->msgSuccess(__('Group has been added.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->msgError(__('Group not added.'));
            }
        }

        $this->set(compact('group', 'groups'));
        $this->set('_serialize', ['group', 'groups']);
    }

    /**
     * Edit a group
     *
     * @description Update group
     * @param integer|null $id Group id.
     * @return Response|void
     */
    public function edit($id = null)
    {
        if (!is_numeric($id)) {
            return $this->redirect(['action' => 'index']);
        }
        $group = $this->Groups->get($id, [
            'contain' => ['ParentGroups']
        ]);
        $groups = $this->Groups->getGroupsList($this->user['group_id']);

        $this->redirectIfNotAllowed($group->id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $group = $this->Groups->patchEntity($group, $this->request->data);
            if ($this->Groups->save($group)) {
                $this->msgSuccess(__('Group saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->msgError(__('Group has not been saved.'));
            }
        }

        $this->set(compact('group', 'groups'));
        $this->set('_serialize', ['group', 'groups']);
    }

    /**
     * Delete a group
     *
     * @description Delete group
     * @param integer|null $id Group id.
     * @return Response
     */
    public function delete($id = null)
    {
        if (!is_numeric($id)) {
            return $this->redirect(['action' => 'index']);
        }
        $this->request->allowMethod(['post', 'delete']);
        $group = $this->Groups->get($id);

        $this->redirectIfNotAllowed($group->id);

        if ($this->Groups->delete($group)) {
            $this->msgSuccess(__('Group removed.'));
        } else {
            $this->msgError(__('Group has not been removed.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Permissions configuration method
     *
     * @description ACL Configuration
     * @param string|null $id Group id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     */
    public function permissions($id)
    {
        $this->setLayout('clean');

        $group = $this->Groups->get($id);
        $ACL = new ACL();

        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $data = $this->getRequest()->getData();
            $close = !empty($data['close']);
            unset($data['close']);
            $group = $this->Groups->patchEntity($group, ['permissions' => $data]);
            if ($this->Groups->save($group)) {
                $this->msgSuccess(__('The ACL settings has been saved.'));

                if ($close) {
                    return $this->redirect(['action' => 'index']);
                }
                return $this->redirect(['action' => 'permissions', $id]);
            } else {
                $this->msgSuccess(__('ACL settings could not be saved. Please, try again.'));
            }
        }

        $this->setHeading('title', $group->name);

        $enableClose = true;

        $resources = $ACL->getResources();
        $this->set(compact('group', 'resources', 'enableClose'));
    }
}
