<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-08)
 * @version 1.0
 */

namespace App\Controller\Panel;

use App\Model\Entity\StaticContent;

/**
 * StaticContents Controller
 *
 * @description Layout variables module.
 */
class StaticContentsController extends PanelController
{
    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        $this->setHeading('title', 'Layout Settings');
        $this->loadComponent('FileUpload', ['directory' => 'contents', 'mime' => ['image/*', 'application/pdf', 'application/msword']]);
    }

    /**
     * Add
     *
     * @description Create layout variable
     * @param integer|null $group_id Group id.
     * @return Response|void
     */
    public function add($group_id = null)
    {
        if (!$this->Auth->user('is_root') || !is_numeric($group_id)) {
            return $this->redirect(['action' => 'index']);
        }

        $staticContent = $this->StaticContents->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['static_group_id'] = $group_id;
            if ($staticContent->types[$data['type']] == 'file') {
                $data['value'] = $this->FileUpload->upload($data['value']);
            }
            $staticContent = $this->StaticContents->patchEntity($staticContent, $data);
            if ($this->StaticContents->save($staticContent)) {
                $this->Flash->success(__('The static content has been saved.'));
                return $this->redirect(['controller' => 'StaticContentGroups', 'action' => 'view', $group_id]);
            } else {
                $this->Flash->error(__('The static content could not be saved. Please, try again.'));
            }
        }

        $type = $this->request->getQuery('type');
        if (!empty($type)) {
            $staticContent->type = $type;
        }
        
        $staticContentGroup = $this->StaticContents->StaticContentGroups->get($group_id)->toArray();
        $this->setHeading('subtitle', __('Add Variable'));
        $this->setHeading('panel', __('Add Variable to Group: {0}', h($staticContentGroup['name'])));
        $this->set(compact('staticContent', 'staticContentGroup', 'type'));
    }

    /**
     * Edit method
     *
     * @description Update layout variable
     * @param integer|null $id Static Content id.
     * @return Response|void
     */
    public function edit($id = null)
    {
        $staticContent = $this->StaticContents->get($id, ['contain' => ['StaticContentGroups']]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            if (isset($data['type']) && !empty($data['value']['name']) && ($data['type'] === StaticContent::TYPE_FILE || $staticContent->type === StaticContent::TYPE_FILE)) {
                $data['value'] = $this->FileUpload->upload($data['value']);
            } elseif ($staticContent->type === StaticContent::TYPE_FILE) {
                $data['value'] = $staticContent->value;
            }

            $staticContent = $this->StaticContents->patchEntity($staticContent, $data);
            if ($this->StaticContents->save($staticContent)) {
                $this->Flash->success(__('The layout variable has been saved.'));
                return $this->redirect(['controller' => 'StaticContentGroups', 'action' => 'view', $staticContent->static_group_id]);
            } else {
                $this->Flash->error(__('The layout variable could not be saved. Please, try again.'));
            }
        }
        
        $type = $this->request->getQuery('type');
        if (!empty($type)) {
            $staticContent->type = $type;
        }

        $this->setHeading('subtitle', __('Edit Variable'));
        $this->setHeading('panel', __('Edit Variable from Group: {0}', h($staticContent->static_content_group->name)));
        $this->set(compact('staticContent', 'type'));
        $this->set('_serialize', ['staticContent']);
    }

    /**
     * Delete method
     *
     * @description Delete layout variable
     * @param integer|null $id Static Content id.
     * @return Response|null.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $staticContent = $this->StaticContents->get($id);
        if ($this->StaticContents->delete($staticContent)) {
            $this->Flash->success(__('The static content has been deleted.'));
        } else {
            $this->Flash->error(__('The static content could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'StaticContentGroups', 'action' => 'view', $staticContent->static_group_id]);
    }
}
