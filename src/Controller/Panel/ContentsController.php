<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\Controller\Panel;

use Cake\Http\Response;

/**
 * Contents Controller
 *
 * @description Contents module.
 */
class ContentsController extends PanelController
{
    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        $this->set('_removeAddButton', true);
    }

    /**
     * Index method
     *
     * @description All contents list.
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Pages']
        ];
        $contents = $this->paginate($this->Contents);

        $this->set(compact('contents'));
        $this->set('_serialize', ['contents']);
    }

    /**
     * Delete method
     *
     * @description Delete content.
     * @param string|null $id Page id.
     * @return Response
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $content = $this->Contents->get($id);
        if ($this->Contents->save($content)) {
            $this->msgSuccess(__('Content has been removed.'));
        } else {
            $this->msgError(__('Content not removed.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
