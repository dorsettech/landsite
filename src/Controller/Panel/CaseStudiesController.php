<?php

namespace App\Controller\Panel;

use App\Controller\Panel\PanelController;
use App\Model\Enum\ArticleStatus;
use App\Model\Enum\ArticleType;
use App\Model\Table\ArticlesTable;
use App\Traits\SwitcherTrait;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Event\Event;
use Cake\Http\Response;
use Cake\Utility\Inflector;
use App\Model\Enum\State;
use Cake\Mailer\MailerAwareTrait;

/**
 * Insights Controller
 *
 * @property ArticlesTable $Articles
 *
 */
class CaseStudiesController extends PanelController
{

    use MailerAwareTrait;
    use SwitcherTrait;
    /**
     * Article type
     *
     * @var string
     */
    protected $articleType = null;

    /**
     * Article name for flash messages and view use
     *
     * @var string
     */
    protected $articleName = 'Article';

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadModel('Articles');

        $this->articleType = ArticleType::CASE_STUDY;
        $this->articleName = Inflector::singularize($this->name);

        $this->set('articleName', $this->articleName);
    }

    /**
     * Before render event
     *
     * @param Event $event Event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);
        $this->set('_removeSearch', false);
    }

    /**
     * Index method
     *
     * @return Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users' => ['Services'], 'Categories'],
            'order' => ['Articles.created' => 'DESC', 'Articles.id' => 'DESC']
        ];
        $articles       = $this->paginate($this->Articles->getByType($this->articleType));

        $this->set(compact('articles'));
        $this->render('/Panel/Articles/index');
    }

    /**
     * Add method
     *
     * @return Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $article = $this->Articles->newEntity();
        if ($this->request->is('post')) {
            $data         = $this->request->getData();
            $data['type'] = $this->articleType;
            $article      = $this->Articles->patchEntity($article, $data);
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('{0} has been saved.', $this->articleName));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('{0} could not be saved. Please, try again.', $this->articleName));
        }
        $users           = $this->Articles->Users->getUsersListWithServiceNames(['group_id' => 6]);
        $categories      = $this->Articles->Categories->find('list', ['limit' => 200]);
        $channels      = $this->Articles->Channels->find('list', ['limit' => 200]);
        $articleStatuses = ArticleStatus::getNamesList();

        $this->set(compact('article', 'users', 'categories', 'channels', 'articleStatuses'));
        $this->render('/Panel/Articles/add');
    }

    /**
     * Edit method
     *
     * @param string|null $id Article id.
     * @return Response|null Redirects on successful edit, renders view otherwise.
     * @throws RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $article = $this->Articles->get($id, ['conditions' => ['Articles.type' => $this->articleType]]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $article = $this->Articles->patchEntity($article, $this->request->getData());
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('{0} has been saved.', $this->articleName));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('{0} could not be saved. Please, try again.', $this->articleName));
        }
        $users           = $this->Articles->Users->getUsersListWithServiceNames(['group_id' => 6]);
        $categories      = $this->Articles->Categories->find('list', ['limit' => 200]);
        $channels      = $this->Articles->Channels->find('list', ['limit' => 200]);
        $articleStatuses = ArticleStatus::getNamesList();
        $this->set(compact('article', 'users', 'categories', 'channels', 'articleStatuses'));
        $this->render('/Panel/Articles/edit');
    }

    /**
     * Delete method
     *
     * @param string|null $id Article id.
     * @return Response|null Redirects to index.
     * @throws RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $article = $this->Articles->get($id, ['conditions' => ['Articles.type' => $this->articleType]]);
        if ($this->Articles->delete($article)) {
            $this->Flash->success(__('{0} has been deleted.', $this->articleName));
        } else {
            $this->Flash->error(__('{0} could not be deleted. Please, try again.', $this->articleName));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Approve or reject article
     *
     * @return Response|null Redirects to index.
     */
    public function approveOrReject()
    {
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if (!empty($data['id'])) {
                $article = $this->Articles->get($data['id'], ['contain' => ['Users']]);
                $article = $this->Articles->patchEntity($article, $data);
                if ($this->Articles->save($article)) {
					/*
					// NR 2019-09-06 - Stop 'approve/reject' email from being sent
                    if ($article->state === State::APPROVED) {
                        $this->getMailer('Articles')->send('caseStudyApprove', [$article]);
                    } else if ($article->state === State::REJECTED) {
                        $this->getMailer('Articles')->send('caseStudyReject', [$article]);
                    }
					*/
                    $this->Flash->success(__('{0} has been updated.', $this->articleName));
                } else {
                    $this->Flash->error(__('{0} could not be updated. Please, try again.', $this->articleName));
                }
            }
        }
        return $this->redirect(['action' => 'index']);
    }

}
