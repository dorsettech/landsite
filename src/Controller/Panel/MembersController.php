<?php
/**
 * @author  Stefan <marcin@econnect4u.pl>
 * @date (2019-02-21)
 * @version 1.0
 */

namespace App\Controller\Panel;

use App\Model\Entity\Group;
use App\Model\Table\CategoriesTable;
use App\Model\Table\UsersTable;
use App\Traits\SwitcherTrait;
use Cake\Http\Response;
use App\Mailer\Members\AccountMailer;
use Cake\Mailer\MailerAwareTrait;

/**
 * MembersController
 *
 * @property UsersTable $Users
 * @property CategoriesTable $Categories
 * @description Members module
 */
class MembersController extends PanelController
{

    /**
     * Switcher trait
     */
    use SwitcherTrait;

    /**
     * MailerAware trait
     */
    use MailerAwareTrait;

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->modelAlias = 'Users';
    }

    /**
     * Members list
     *
     * @description Members group list
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'order' => ['Users.created' => 'DESC', 'Users.id' => 'DESC']
        ];

        $users = $this->paginate($this->Users->getUsers(['Users.group_id' => Group::MEMBERS]));

        $this->set('users', $users);
        $this->set('_serialize', 'users');
    }

    /**
     * Add member
     *
     * @description Add member
     * @return Response|void
     */
    public function add()
    {
        if (!$this->Auth->user('is_root') && !$this->Auth->user('is_admin')) {
            $this->redirect(['controller' => 'Members', 'action' => 'index']);
        }

        $user = $this->Users->newEntity();
        if ($this->getRequest()->is('post')) {
            $data             = $this->getRequest()->getData();
            $data['group_id'] = Group::MEMBERS;

            $options = [];
            if (isset($data['user_detail'], $data['user_detail']['use_billing_details']) && $data['user_detail']['use_billing_details'] == 0) {
                $options = [
                    'associated' => ['UserDetails', 'UserBillingAddresses' => ['validate' => 'mandatory']]
                ];
            }

            $user = $this->Users->patchEntity($user, $data, $options);
            if ($this->Users->save($user)) {
                $this->msgSuccess(__('Member has been added.'));

                return $this->redirect(['controller' => 'Members', 'action' => 'index']);
            } else {
                $this->msgError(__('Member has not been added, try again.'));
            }
        }

        $this->set(compact('user'));
    }

    /**
     * Edit member details
     *
     * @description Edit member
     * @param integer|null $id User id.
     * @return Response|void
     */
    public function edit($id = null)
    {
        if (!is_numeric($id)) {
            return $this->redirect(['controller' => 'Members', 'action' => 'index']);
        }

        $user = $this->Users->get($id, [
            'contain' => ['UserDetails', 'UserBillingAddresses']
        ]);

        if ($this->user['id'] != $user->id) {
            $this->redirectIfNotAllowed($user['group_id']);
        }

        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $data             = $this->getRequest()->getData();
            $data['group_id'] = Group::MEMBERS;

            if ($data['password'] == '') {
                unset($data['password']);
            }

            if (!empty($data['remove_image']) && $data['remove_image'] == true) {
                $data['image'] = null;
            }

            $options = [];
            if (isset($data['user_detail'], $data['user_detail']['use_billing_details']) && $data['user_detail']['use_billing_details'] == 0) {
                $options = [
                    'associated' => ['UserDetails', 'UserBillingAddresses' => ['validate' => 'mandatory']]
                ];
            }

            $user = $this->Users->patchEntity($user, $data, $options);

            if ($this->Users->save($user)) {
                $this->msgSuccess(__("Member {0} has been updated.", $user->full_name));

                return $this->redirect(['controller' => 'Members', 'action' => 'index']);
            } else {
                $this->msgError(__("Member {0} has not been updated.", $user->full_name));
            }
        }

        $this->set(compact('user'));
    }

    /**
     * Mark user as deleted
     *
     * @description Delete member
     * @param integer|null $id User id.
     * @return Response
     */
    public function delete($id = null)
    {
        if (!is_numeric($id)) {
            return $this->redirect(['controller' => 'Members', 'action' => 'index']);
        }

        $user = $this->Users->get($id, [
            'conditions' => ['Users.group_id' => Group::MEMBERS]
        ]);
        $this->redirectIfNotAllowed($user['group_id']);

        if ($this->getRequest()->is(['post', 'delete'])) {
            if ($this->Users->delete($user)) {
                $this->msgSuccess(__("Member {0} has been removed.", $user->full_name));
            } else {
                $this->msgError(__("Member {0} has not been removed.", $user->full_name));
            }
        }

        return $this->redirect($this->getRequest()->referer());
    }

    /**
     * View member details
     *
     * @description View member details
     * @param integer|null $id User id.
     * @return Response
     */
    public function view($id = null)
    {
        if (!is_numeric($id)) {
            return $this->redirect(['controller' => 'Members', 'action' => 'index']);
        }

        $this->setLayout('clean');

        $user = $this->Users->getMemberWithDetails($id);

        $this->loadModel('Categories');
        $relatedCategories = $this->Categories->relatedCategoriesList(explode(',', $user->user_detail->pref_insights_list));

        $this->setHeading('title', $user->full_name);
        $this->setHeading('subtitle', '#' . $user->id);

        $this->set(compact('user', 'relatedCategories'));
    }

    /**
     * Get list
     *
     * @return void Returns members list as a json object.
     */
    public function getList()
    {
        $this->getRequest()->allowMethod(['ajax']);

        $data = $this->getRequest()->getData();

        $conditions = [
            'Users.group_id' => Group::MEMBERS
        ];

        if (!empty($data['keyword'])) {
            $conditions['OR'] = [
                'Users.first_name LIKE ' => '%' . $data['keyword'] . '%',
                'Users.last_name LIKE '  => '%' . $data['keyword'] . '%',
                'Users.email LIKE '      => '%' . $data['keyword'] . '%',
            ];
        }

        $list = $this->Users->getUsersList($conditions);

        $data = [];
        foreach ($list as $id => $field) {
            $data[] = [
                'id'   => $id,
                'text' => $field . ' #' . $id
            ];
        }

        $this->set('data', $data);
        $this->set('_serialize', 'data');
    }

    /**
     * Approve or reject member
     *
     * @return Response|null Redirects to index.
     */
    public function approveOrReject()
    {
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if (!empty($data['id'])) {
                $user = $this->Users->get($data['id']);
                if ($data['state'] === 'APPROVED') {
                    $user->active = true;
                } else if ($data['state'] === 'REJECTED') {
                    $user->active        = false;
                    $user->reject_reason = $data['reject_reason'];
                }
                if ($this->Users->save($user)) {
                    if ($user->active === true) {
                        $this->getMailer('Members/Account')->send('approve', [$user]);
                        $this->Flash->success(__('{0} has been updated.', $user->email));
                    } else if ($user->active === false && !empty($user->reject_reason)) {
                        $this->getMailer('Members/Account')->send('reject', [$user]);
                        $this->Users->delete($user);
                        $this->Flash->success(__('{0} has been rejected.', $user->email));
                    }
                } else {
                    $this->Flash->error(__('{0} could not be updated. Please, try again.', $user->email));
                }
            }
        }
        return $this->redirect(['action' => 'index']);
    }
}
