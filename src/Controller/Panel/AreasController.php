<?php

namespace App\Controller\Panel;

use App\Controller\Panel\PanelController;
use App\Model\Table\AreasTable;
use App\Traits\GetListTrait;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Response;

/**
 * Areas Controller
 *
 * @property AreasTable $Areas

 */
class AreasController extends PanelController
{
    use GetListTrait;
    
    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->setHeading('title', __('Areas We Cover'));
    }

    /**
     * Index method
     *
     * @return Response|void
     */
    public function index()
    {
        $this->paginate = [
            'order' => ['name' => 'ASC']
        ];
        $areas = $this->paginate($this->Areas);

        $this->set(compact('areas'));
    }

    /**
     * Add method
     *
     * @return Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $area = $this->Areas->newEntity();
        
        if ($this->request->is('post')) {
            $area = $this->Areas->patchEntity($area, $this->request->getData());
            
            if ($this->Areas->save($area)) {
                $this->Flash->success(__('Area has been added.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Area could not be added. Please, try again.'));
        }
        
        $this->set(compact('area'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Area id.
     * @return Response|null Redirects on successful edit, renders view otherwise.
     * @throws RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $area = $this->Areas->get($id);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $area = $this->Areas->patchEntity($area, $this->request->getData());
            
            if ($this->Areas->save($area)) {
                $this->Flash->success(__('Area has been updated.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Area could not be updated. Please, try again.'));
        }
        
        $this->set(compact('area'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Area id.
     * @return Response|null Redirects to index.
     * @throws RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $area = $this->Areas->get($id);
        
        if ($this->Areas->delete($area)) {
            $this->Flash->success(__('Area has been deleted.'));
        } else {
            $this->Flash->error(__('Area could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
