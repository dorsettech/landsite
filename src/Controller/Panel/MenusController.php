<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-08)
 * @version 1.0
 */

namespace App\Controller\Panel;

use App\Traits\SwitcherTrait;
use Cake\Network\Response;

/**
 * Menus Controller
 *
 * @description Menus module.
 */
class MenusController extends PanelController
{

    use SwitcherTrait;
    /**
     * Initialize
     *
     * @return void
     *
     */
    public function initialize()
    {
        parent::initialize();
        $this->set('_removeSearch', true);
    }

    /**
     * Index method
     *
     * @description Menu list
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'order' => ['Menus.id' => 'ASC', 'Menus.position+0' => 'ASC']
        ];
        $menus = $this->paginate($this->Menus);

        $this->set(compact('menus'));
        $this->set('_serialize', ['menus']);
    }

    /**
     * View specific menu page
     *
     * @description View menu pages tree
     * @param string|type $urlname Urlname.
     * @return Response|void
     */
    public function view($urlname = '')
    {
        if (empty($urlname)) {
            $this->redirect(['prefix' => $this->adminPrefix, 'Controller' => $this->adminDashboard]);
        }

        $this->loadModel('Pages');
        $layouts = $this->Pages->getLayoutsWithDetails();
        $menu = $this->Menus->getMenuPage($urlname);

        if (!empty($menu)) {
            $menuName = $menu['name'];
            $this->set(compact('menu', 'layouts', 'contents', 'urlname'));
            $this->set('_serialize', ['menu']);
        } else {
            $this->redirect(['action' => 'index']);
        }
    }

    /**
     * Add method
     *
     * @description Create menu
     * @return Response|void
     */
    public function add()
    {
        $menu = $this->Menus->newEntity();
        if ($this->request->is('post')) {
            $this->request->withData('position', 99);
            $menu = $this->Menus->patchEntity($menu, $this->request->getData());
            if ($this->Menus->save($menu)) {
                $this->msgSuccess(__('Menu added.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->msgError(__('Menu can not be added.'));
            }
        }

        $this->set(compact('menu', 'languagesList'));
    }

    /**
     * Edit method
     *
     * @description Update menu
     * @param integer|null $id Menu id.
     * @return Response|void
     */
    public function edit($id = null)
    {
        if (!is_numeric($id)) {
            return $this->redirect(['action' => 'index']);
        }
        $menu = $this->Menus->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $menu = $this->Menus->patchEntity($menu, $this->request->getData());
            if ($this->Menus->save($menu)) {
                $this->msgSuccess(__('Menu saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->msgError(__('Menu not saved.'));
            }
        }
        $this->set(compact('menu', 'languagesList'));
    }

    /**
     * Delete method
     *
     * @description Delete menu
     * @param integer|null $id Menu id.
     * @return Response|void
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $menu = $this->Menus->get($id);
        if ($this->Menus->delete($menu)) {
            $this->msgSuccess(__('Menu removed.'));
        } else {
            $this->msgError(__('Menu not removed.'));
        }

        return $this->redirect($this->request->referer());
    }
}
