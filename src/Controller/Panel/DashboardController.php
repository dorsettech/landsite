<?php
/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */
namespace App\Controller\Panel;

use App\Library\GoogleAnalyticsReporting\Analytics;
use App\Model\Entity\Group;
use Cake\Core\Configure;
use Cake\Core\Exception\Exception;
use Cake\ORM\Table;

/**
 * DashboardController
 *
 * @description Dashboard module
 */
class DashboardController extends PanelController
{
    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadModel('Pages');
        $this->loadModel('FormsData');
        $this->loadModel('Logs');
    }

    /**
     * Dashboard defalut view
     *
     * @description Dashboard
     * @return void
     */
    public function index()
    {
        $this->groupRedirect();
        $this->setGoogleAnalyticsData();

        $dashboard = [];
        $this->setLayout('dashboard');

        $dashboardContent = $this->getDashboardPanels();
        foreach ($dashboardContent as $key => $value) {
            if (!($this->{$key} instanceof Table)) {
                continue;
            }
            $dashboard[$key] = $this->{$key}->find('all', [
                    'contain' => $value['contain'],
                    'order'   => [$key . '.' . $value['order']],
                ])->limit(5);

            $this->set($key, $dashboard[$key]);
        }
        $this->setServerSettingsData();
    }

    /**
     * Group redirect
     *
     * Use for group redirections if needed.
     *
     * @return \Cake\Http\Response|void
     */
    private function groupRedirect()
    {
        $user = $this->Auth->getUser();
        if (!empty($user['group_id'])) {
            switch ($user['group_id']) {
                case Group::MODERATOR:
                    return $this->redirect(['controller' => 'News', 'action' => 'index']);
                    break;
            }
        }
    }

    /**
     * Get dashboard panels
     *
     * @return array
     */
    protected function getDashboardPanels()
    {
        return [
            'Pages'     => [
                'order'   => 'modified DESC',
                'contain' => ['Menus']
            ],
            'FormsData' => [
                'order'   => 'created DESC',
                'contain' => ['Forms']
            ],
            'Logs'      => [
                'order'   => 'created DESC',
                'contain' => ['Users' => ['Groups']]
            ]
        ];
    }

    /**
     * Set Google Analytics Data
     *
     * @return void Sets the Google Analytics API data as an array.
     */
    protected function setGoogleAnalyticsData(): void
    {
        $this->helpers['GoogleAnalyticsReporting'] = null;

        $googleAnalyticsReporting = new Analytics([
            'users'              => 'Visitors',
            'newUsers'           => 'New Visitors',
            'avgSessionDuration' => 'Avg. Session Duration',
            'bounceRate'         => 'Bounce Rate',
        ]);
        $analyticsApi             = $googleAnalyticsReporting->getAnalytics();
        $analyticsTimeSpan        = $googleAnalyticsReporting->getTimeSpanInDays();

        $this->set(compact('analyticsApi', 'analyticsTimeSpan'));
    }

    /**
     * Set data for ServerSettings Widget.
     *
     * @return void
     */
    private function setServerSettingsData(): void
    {
        $settings = [];
        $keys     = [
            'memory_limit',
            'upload_max_filesize',
            'post_max_size',
            'max_execution_time',
            'max_input_time',
            'max_input_vars',
        ];

        try {
            Configure::load('website');
            foreach ($keys as $key) {
                $projectSetting = $this->getConf('ServerSettings.' . $key) ?: __('not set');

                $settings[$key] = [
                    $projectSetting,
                    ini_get($key) ?: __('not set'),
                    floatval(ini_get($key)) >= floatval($projectSetting),
                ];
            }
        } catch (Exception $ex) {
            $settings['error'] = __("You need to have a website.php file set to use this widget");
        }


        $this->set('serverSettings', $settings);
    }

    /**
     * Get website analytics data
     *
     * @description Website analytics chart data (ajax)
     * @return void Returns website analytics data for Ajax request (plot chart usage).
     */
    public function getWebsiteAnalyticsData(): void
    {
        $this->request->allowMethod('ajax');

        $results = [];

        $googleAnalyticsReporting = new Analytics([
            'users'     => 'Users',
            'pageviews' => 'Page Views',
        ]);
        $results                  = $googleAnalyticsReporting->getAnalyticsForChart('plot');

        $this->set(compact('results'));
        $this->set('_serialize', ['results']);
    }

    /**
     * Get detail analytics data
     *
     * @description Detail charts data (ajax)
     * @return void Returns detail analytics data for Ajax request (sparkline charts usage).
     */
    public function getDetailAnalyticsData(): void
    {
        $this->request->allowMethod('ajax');

        $results = [];

        $googleAnalyticsReporting = new Analytics([
            'users'              => 'Users',
            'newUsers'           => 'New Users',
            'sessions'           => 'Sessions',
            'avgSessionDuration' => 'Avg. Session Duration',
            'bounces'            => 'Bounces',
            'bounceRate'         => 'Bounce Rate',
        ]);
        $results                  = $googleAnalyticsReporting->getAnalyticsForChart('sparkline');

        $this->set(compact('results'));
        $this->set('_serialize', ['results']);
    }
}
