<?php

namespace App\Controller\Panel;

use App\Controller\Panel\PanelController;
use App\Model\Entity\Group;
use App\Model\Enum\DiscountType;
use App\Model\Table\DiscountsTable;
use App\Traits\SwitcherTrait;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Response;

/**
 * Discounts Controller
 *
 * @property DiscountsTable $Discounts

 */
class DiscountsController extends PanelController
{

    use SwitcherTrait;

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
    }

    /**
     * Index method
     *
     * @return Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];

        $discounts      = $this->paginate($this->Discounts->getAll());

        $discounts = $discounts->toArray();
        if (!empty($discounts['all'])) {
            $discounts = $discounts['all'];
        }

        $this->set(compact('discounts'));
    }

    /**
     * Add method
     *
     * @return Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $discount = $this->Discounts->newEntity();

        if ($this->request->is('post')) {
            $discount = $this->Discounts->patchEntity($discount, $this->request->getData());

            if ($this->Discounts->save($discount)) {
                $this->Flash->success(__('Discount has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Discount could not be saved. Please, try again.'));
        }

        $users         = $this->Discounts->Users->getUsersList(['Users.group_id' => Group::MEMBERS]);
        $discountTypes = DiscountType::getNamesList();

        $this->set(compact('discount', 'users', 'discountTypes'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Discount id.
     * @return Response|null Redirects on successful edit, renders view otherwise.
     * @throws RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $discount = $this->Discounts->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $discount = $this->Discounts->patchEntity($discount, $this->request->getData());
            
            if ($this->Discounts->save($discount)) {
                $this->Flash->success(__('Discount has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Discount could not be saved. Please, try again.'));
        }

        $users = $this->Discounts->Users->getUsersList(['Users.id' => $discount->user_id, 'Users.group_id' => Group::MEMBERS]);
        $discountTypes = DiscountType::getNamesList();

        $this->set(compact('discount', 'users', 'discountTypes'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Discount id.
     * @return Response|null Redirects to index.
     * @throws RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        
        $discount = $this->Discounts->get($id);

        if ($this->Discounts->delete($discount)) {
            $this->Flash->success(__('Discount has been deleted.'));
        } else {
            $this->Flash->error(__('Discount could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
