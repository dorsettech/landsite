<?php

namespace App\Controller\Panel;

use App\Controller\Panel\PanelController;
use App\Model\Table\ServiceMediaTable;
use App\Model\Table\ServicesTable;
use App\Traits\GetType;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Response;

/**
 * ServiceMedia Controller
 *
 * @property ServiceMediaTable $ServiceMedia
 * @property ServicesTable $Services
 */
class ServiceMediaController extends PanelController
{

    use GetType;

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadModel('Services');
    }

    /**
     * Index method
     *
     * @param integer|null $serviceId Service id.
     * @return Response|void
     * @throws RecordNotFoundException When record not found.
     */
    public function list($serviceId = null): void
    {
        $service = $this->Services->get($serviceId);

        $this->paginate = [
            'order' => ['position' => 'ASC', 'created' => 'ASC']
        ];
        $serviceMedia = $this->paginate($this->ServiceMedia->getServiceMedia($service->id));

        $this->setHeading('title', $service->company);
        $this->setHeading('subtitle', __('Media'));
        $this->setHeading('link_close', ['controller' => 'Services', 'action' => 'index']);

        $this->set(compact('serviceMedia', 'service'));
    }

    /**
     * Add method
     *
     * Ajax only as it's used in Dropzone available on list template.
     *
     * @param integer|null $serviceId Service id.
     * @return void
     * @throws RecordNotFoundException When record not found.
     */
    public function add($serviceId = null): void
    {
        $this->getRequest()->allowMethod(['ajax']);

        $service = $this->Services->get($serviceId);

        $serviceMedia = $this->ServiceMedia->newEntity();

        if ($this->getRequest()->is('ajax')) {
            $data               = $this->request->getData();
            $data['service_id'] = $service->id;

            if (!empty($data['source']['type'])) {
                Configure::load('website');
                $media        = Configure::read('Media');
                $data['type'] = $this->mediaTypeFromMime($data['source']['type'], $media['mime_types_allowed']);
            }

            $serviceMedia = $this->ServiceMedia->patchEntity($serviceMedia, $data);
            if ($this->ServiceMedia->save($serviceMedia)) {
                $this->Flash->success(__('Service media has been added.'));
            } else {
                $this->setResponse($this->getResponse()->withStatus(406));
                $serviceMedia = $serviceMedia->getErrorsMessages();

                $this->Flash->error(__('Service media could not be added. Please, try again.'));
            }
        }

        $this->set('serviceMedia', $serviceMedia);
        $this->set('_serialize', 'serviceMedia');
    }

    /**
     * Edit method
     *
     * @param integer|null $serviceId Service id.
     * @param integer|null $id        Service Media id.
     * @return Response|null Redirects on successful edit, renders view otherwise.
     * @throws RecordNotFoundException When record not found.
     */
    public function edit($serviceId = null, $id = null)
    {
        $serviceMedia = $this->ServiceMedia->get($id, ['conditions' => ['service_id' => $serviceId], 'contain' => ['Services']]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $serviceMedia = $this->ServiceMedia->patchEntity($serviceMedia, $this->request->getData());

            if ($this->ServiceMedia->save($serviceMedia)) {
                $this->Flash->success(__('Service media has been updated.'));

                return $this->redirect(['action' => 'list', $serviceId]);
            }
            $this->Flash->error(__('Service media could not be updated. Please, try again.'));
        }

        $this->setHeading('title', $serviceMedia->service->company);
        $this->setHeading('subtitle', __('Media Edit'));
        $this->setHeading('link_close', ['action' => 'list', $serviceId]);

        $this->set(compact('serviceMedia'));
    }

    /**
     * Delete method
     *
     * @param integer|null $serviceId Service id.
     * @param integer|null $id        Service Media id.
     * @return Response|null Redirects to index.
     * @throws RecordNotFoundException When record not found.
     */
    public function delete($serviceId = null, $id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $serviceMedia = $this->ServiceMedia->get($id, ['conditions' => ['service_id' => $serviceId]]);

        if ($this->ServiceMedia->delete($serviceMedia)) {
            $this->Flash->success(__('Service media has been deleted.'));
        } else {
            $this->Flash->error(__('Service media could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'list', $serviceId]);
    }
}
