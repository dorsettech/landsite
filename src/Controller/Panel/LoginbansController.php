<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-08)
 * @version 1.0
 */

namespace App\Controller\Panel;

/**
 * Loginbans Controller
 *
 * @description Login bans module
 */
class LoginbansController extends PanelController
{
    /**
     * Index method
     *
     * @description Login bans list
     * @return void
     */
    public function index()
    {
        $loginbans = $this->paginate($this->Loginbans, ['order' => ['id DESC']]);

        $this->set(compact('loginbans'));
        $this->set('_serialize', ['loginbans']);
    }

    /**
     * Add method
     *
     * @description Create login ban
     * @return Response|void
     */
    public function add()
    {
        $loginban = $this->Loginbans->newEntity();
        if ($this->request->is('post')) {
            $loginban = $this->Loginbans->patchEntity($loginban, $this->request->getData());
            if ($this->Loginbans->save($loginban)) {
                $this->Flash->success(__('The loginban has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The loginban could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('loginban'));
        $this->set('_serialize', ['loginban']);
    }

    /**
     * Edit method
     *
     * @description Update login ban
     * @param integer|null $id Loginban id.
     * @return Response|void
     */
    public function edit($id = null)
    {
        $loginban = $this->Loginbans->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $loginban = $this->Loginbans->patchEntity($loginban, $this->request->getData());
            if ($this->Loginbans->save($loginban)) {
                $this->Flash->success(__('The loginban has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The loginban could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('loginban'));
        $this->set('_serialize', ['loginban']);
    }

    /**
     * Delete method
     *
     * @description Delete login ban
     * @param string|null $id Loginban id.
     * @return Response
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $loginban = $this->Loginbans->get($id);
        if ($this->Loginbans->delete($loginban)) {
            $this->Flash->success(__('The loginban has been deleted.'));
        } else {
            $this->Flash->error(__('The loginban could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
