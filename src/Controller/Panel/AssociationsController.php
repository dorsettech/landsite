<?php

namespace App\Controller\Panel;

use App\Controller\Panel\PanelController;
use App\Model\Enum\CredentialType;
use App\Model\Table\CredentialsTable;
use App\Traits\GetListTrait;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Response;
use Cake\Utility\Inflector;

/**
 * Associations Controller
 *
 * @property CredentialsTable $Credentials
 */
class AssociationsController extends PanelController
{
    use GetListTrait;

    /**
     * Credential type
     *
     * @var string
     */
    protected $credentialType = null;

    /**
     * Credential name for flash messages and view use
     *
     * @var string
     */
    protected $credentialName = 'Credential';

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadModel('Credentials');

        $this->credentialType = CredentialType::ASS;
        $this->credentialName = Inflector::singularize($this->name);

        $this->set('credentialName', $this->credentialName);
    }

    /**
     * Index method
     *
     * @return Response|void
     */
    public function index()
    {
        $this->paginate = [
            'order' => ['Credentials.created' => 'DESC', 'Credentials.id' => 'DESC']
        ];

        $credentials = $this->paginate($this->Credentials->getByType($this->credentialType));

        $this->set(compact('credentials'));

        $this->render('/Panel/Credentials/index');
    }

    /**
     * Add method
     *
     * @return Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $credential = $this->Credentials->newEntity();

        if ($this->request->is('post')) {
            $data         = $this->request->getData();
            $data['type'] = $this->credentialType;
            $credential   = $this->Credentials->patchEntity($credential, $data);

            if ($this->Credentials->save($credential)) {
                $this->Flash->success(__('{0} has been added.', $this->credentialName));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('{0} could not be added. Please, try again.', $this->credentialName));
        }

        $this->set(compact('credential'));

        $this->render('/Panel/Credentials/add');
    }

    /**
     * Edit method
     *
     * @param string|null $id Credential id.
     * @return Response|null Redirects on successful edit, renders view otherwise.
     * @throws RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $credential = $this->Credentials->get($id, ['conditions' => ['Credentials.type' => $this->credentialType]]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $credential = $this->Credentials->patchEntity($credential, $this->request->getData());

            if ($this->Credentials->save($credential)) {
                $this->Flash->success(__('{0} has been updated.', $this->credentialName));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('{0} could not be updated. Please, try again.', $this->credentialName));
        }

        $this->set(compact('credential'));

        $this->render('/Panel/Credentials/edit');
    }

    /**
     * Delete method
     *
     * @param string|null $id Credential id.
     * @return Response|null Redirects to index.
     * @throws RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $credential = $this->Credentials->get($id, ['conditions' => ['Credentials.type' => $this->credentialType]]);

        if ($this->Credentials->delete($credential)) {
            $this->Flash->success(__('{0} has been deleted.', $this->credentialName));
        } else {
            $this->Flash->error(__('{0} could not be deleted. Please, try again.', $this->credentialName));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Get list
     *
     * @param integer $limit Limit results.
     * @return void
     */
    public function getList(int $limit = 200)
    {
        $this->getRequest()->allowMethod(['ajax']);

        $data    = $this->getRequest()->getData();
        $keyword = (!empty($data['keyword'])) ? $data['keyword'] : '';

        $list = $this->Credentials->getListByType(['Credentials.name LIKE' => '%' . $keyword . '%', 'Credentials.type' => $this->credentialType], $limit);

        $data = $this->convertList($list);

        $this->set('data', $data);
        $this->set('_serialize', 'data');
    }
}
