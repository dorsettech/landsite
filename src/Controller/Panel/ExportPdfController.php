<?php

namespace App\Controller\Panel;

use App\Model\Entity\Payment;
use App\Model\Enum\PaymentType;
use Cake\Filesystem\Folder;
use Cake\View\Helper\NumberHelper;
use Cake\View\View;

/**
 * ExportPdf Controller
 *
 * @author  Dzidek <marcin.nierobis@eConnect4u.pl>
 * @date    date(2019-04-24)
 * @version 1.0
 * @todo dzidek psr-2
 */
class ExportPdfController extends PanelController
{
    /**
     * Pdf instance
     *
     * @var null
     */
    protected $pdf = null;

    /**
     * Project name
     *
     * @var string
     */
    private $projectName = '';

    /**
     * Invoice number
     *
     * @var string
     */
    private $invoiceNumber = '';

    /**
     * Number helper
     *
     * @var null
     */
    private $numberHelper = null;

    /**
     * initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        try {
            $this->loadModel('Payments');
            $this->loadModel('Properties');
            $this->loadModel('Services');
            $this->projectName = $this->getConf('Website.service');
            $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
            $fontDirs = $defaultConfig['fontDir'];
            $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
            $fontData = $defaultFontConfig['fontdata'];
            $this->pdf = new \Mpdf\Mpdf([
                'fontDir' => array_merge($fontDirs, [
                    WWW_ROOT . '/pdf/font',
                ]),
                'fontdata' => $fontData + [
                        'raleway' => [
                            'R' => 'Raleway-Light.ttf',
                            'I' => 'Raleway-Italic.ttf',
                            'B' => 'Raleway-Bold.ttf',
                        ]
                    ],
                'default_font' => 'raleway',
                'mode' => 'utf-8',
                'orientation' => 'P',
                'format' => 'A4',
                'mgl' => 10,
                'mgr' => 10,
                'mgt' => 10,
                'mgb' => 10,
                'default_font_size' => 12
            ]);
            $this->pdf->SetImportUse();
            $this->pdf->allow_charset_conversion = true;
            $this->pdf->SetAuthor($this->projectName);
            $this->pdf->SetAutoPageBreak(true);

            $this->autoRender = false;
            $this->numberHelper = new NumberHelper(new View());
        } catch (\Exception $e) {
            exit($e->getMessage());
        }
    }

    /**
     * Export payment invoice
     *
     * @param integer $payment_id Payment id.
     * @param boolean $download   Download file.
     *
     * @return string|void
     */
    public function invoice(int $payment_id = 0, bool $download = true)
    {
        $payment = $this->Payments->get($payment_id, ['contain' => ['Users' => ['Services', 'UserDetails']]]);
        $payment->paid_object = $this->getPaidObject($payment);
        if (!empty($payment)) {
            $this->invoiceNumber = $payment->created->format('Ymdhis') . '-' . $payment->id;
            $fileName = $this->invoiceNumber . '.pdf';
            $this->pdf->SetTitle($fileName);
            $this->pdf->SetSubject(__('Invoice') . ' ' . $fileName);

            $backgroundPage = $this->pdf->SetSourceFile(WWW_ROOT . '/pdf/background/invoice_template.pdf');
            $template = $this->pdf->ImportPage($backgroundPage);
            $this->pdf->UseTemplate($template);
            $this->createInvoicePage($payment, 'invoice');
            $path = WWW_ROOT . 'files/invoices/' . $payment->user_id;
            $dir = new Folder();
            $dir->create($path);
            $this->pdf->Output($path . '/' . $fileName, 'F');
            $payment->invoice = $fileName;
            $this->Payments->save($payment);
            if ($download) {
                $this->pdf->Output($fileName, 'I');
            } else {
                return $fileName;
            }
        }
    }

    /**
     * Get paid object for invoice.
     *
     * @param Payment $payment Payment entity.
     *
     * @return object
     */
    private function getPaidObject(Payment $payment)
    {
        if ($payment->type === PaymentType::PROPERTY) {
            $this->Properties->removeBehavior('SoftDelete');
            return $this->Properties->get($payment->object_id);
        } elseif ($payment->type === PaymentType::SERVICE) {
            return $this->Services->get($payment->object_id);
        }
    }

    /**
     * Render one page invoice from one order
     *
     * @param Payment $payment Payment entity.
     * @param string  $type    Document type.
     *
     * @return void
     */
    private function createInvoicePage(Payment $payment, string $type = 'invoice'): void
    {
        $border = '2px solid #E0E2E4;';
        $style = '<style>'
            . '.w-100{width:100%}'
            . '.font-normal{font-weight:normal}'
            . '.font-bold{font-weight:bold}'
            . '.order-number{text-align:right; font-size:16px; font-weight:bold;}'
            . '.purchase-number{text-align:right; font-size:14px; font-weight:bold;}'
            . '.invoice-title{text-align:left; margin-left:-15px; margin-top:30px; font-weight:bold; font-size:24px;}'
            . '.invoice-number{text-align:left; margin-left:-15px; font-weight:bold; font-size:14px;}'
            . '.order-title{text-align:left; margin-left:-15px; padding-top:60px; font-weight:bold; font-size:24px;}'
            . '.order-subtitle{text-align:left; margin-left:-15px; margin-top:20px; font-weight:bold; font-size:16px;}'
            . '.order-address{text-align:left; margin-left:-15px; margin-top:10px; font-size:12px;}'
            . '.order-dates-table{width: 100%; margin-left:-15px; border-top:' . $border . ' ; border-bottom:' . $border . '}'
            . '.order-dates-table tr td{width:33%; padding:10px 0;}'
            . '.order-items-table{width: 100%; margin-left:-15px; margin-top:20px; border-bottom:' . $border . '}'
            . '.order-items-table tr th{font-weight:bold}'
            . '.order-summary-table{width: 100%; margin-left:-15px; border-bottom:' . $border . '}'
            . '.order-total-table{width: 100%; margin-left:-15px; margin-top:10px; padding-bottom:10px; border-bottom: ' . $border . '}'
            . '</style>';

        $html = $style;
        $meta = json_decode($payment->meta);
        $payload = json_decode($payment->payload);

        if ($type === 'invoice') {
            if ($meta->type === 'ORDER') {
                $html .= $this->generateInvoiceTitle($payment->id, $payment->token_id);
            } else {
                $html .= $this->generateInvoiceTitle($payment->id);
            }
        } elseif ($type === 'order') {
            if ($meta->type === 'ORDER') {
                $html .= $this->generateOrderTitle($payment->id, $payment->token_id);
            } else {
                $html .= $this->generateOrderTitle($payment->id);
            }
        }
        $payment_name = $payment->user->full_name;
        if ($payment->user->has('service')) {
            $payment_name .= ' - ' . $payment->user->service->company;
        }
        $html .= '<div class="w-100 order-subtitle">' . $payment_name . '</div>';
        if ($meta->type === 'STRIPE') {
            $html .= $this->generateAddressForStripePayment($payload->billing_details->address);
        } elseif ($meta->type === 'ORDER') {
            $html .= $this->generateAddressForOrderPayment($meta->billing_details);
        }

        $html .= '<table class="order-dates-table">'
            . '<tr>'
            . '<td><span class="font-bold">Order Date: </span>' . $payment->created->format('d/m/Y') . '</td>'
            . '<td><span class="font-bold">Start Date: </span>' . $payment->paid_object->created->format('d/m/Y') . '</td>'
            . '<td><span class="font-bold">End Date: </span>' . $payment->paid_object->created->add(new \DateInterval('P30D'))->format('d/m/Y') . '</td>'
            . '</tr>'
            . '</table>';

        $basket = $payload->metadata->basket;
        if (!empty($basket)) {
            $basket = json_decode($basket);
        }

        foreach ($basket as $item) {
            $vat = $item->price * ($payment->vat_rate / 100);
            $itemsHtml = '<tr>'
                . '<td style="width:50%; padding-top:5px; text-align:left;">' . $item->name . '</td>'
                . '<td style="width:20%; padding-top:5px; text-align:center;">1</td>'
                . '<td style="width:20%; padding-top:5px; text-align:center;">&nbsp;</td>'
                . '<td style="width:10%; padding-top:5px; text-align:center;">' . $this->numberHelper->currency($item->price, 'GBP') . '</td>'
                . '</tr>';
        }
        $html .= '<table class="order-items-table">'
            . '<tr>'
            . '<th style="width:50%; text-align:left;">Item</td>'
            . '<th style="width:20%;">Qty</td>'
            . '<th style="width:20%;">&nbsp;</td>'
            . '<th style="width:10%;">Subtotal</td>'
            . '</tr>'
            . $itemsHtml
            . '</table>';

		$discountHtml = '';
		if ($payment->discount > 0) {
			$discountHtml .= '<tr>'
						   . '<td style="width:70%; text-align:right;"></td>'
						   . '<td style="width:20%; text-align:center;"><span class="font-bold">Discount</span></td>'
						   . '<td style="width:10%; text-align:center;">' . $this->numberHelper->currency($payment->discount, 'GBP') . '</td>'
						   . '</tr>';
		}
		$subTotal = $payment->amount / (1 + $payment->vat_rate / 100);
		$vatTotal = $payment->amount - $subTotal;
        $html .= '<table class="order-summary-table">'
            . $discountHtml
            . '<tr>'
            . '<td style="width:70%; text-align:right;"></td>'
            . '<td style="width:20%; text-align:center;"><span class="font-bold">Subtotal</span></td>'
            . '<td style="width:10%; text-align:center;">' . $this->numberHelper->currency($subTotal, 'GBP') . '</td>'
            . '</tr>'
            . '<tr>'
            . '<td style="width:70%; padding-top:5px; text-align:right;"></td>'
            . '<td style="width:20%; padding-top:5px; text-align:center;"><span class="font-bold">VAT</span></td>'
            . '<td style="width:10%; padding-top:5px; text-align:center;">' . $this->numberHelper->currency(/*$payment->vat*/ $vatTotal, 'GBP') . '</td>'
            . '</tr>'
            . '</table>';

        $html .= '<table class="order-total-table">'
            . '<tr>'
            . '<td style="width:70%; text-align:right;"></td>'
            . '<td style="width:20%; text-align:center;"><span class="font-bold">Total</span></td>'
            . '<td style="width:10%; text-align:center;">' . $this->numberHelper->currency($payment->amount, 'GBP') . '</td>'
            . '</tr>'
            . '</table>';

        $this->pdf->WriteHTML($html);
    }

    /**
     * Generate invoice title
     *
     * @param integer $order_number   Order number.
     * @param string  $purchase_order Purchase order number.
     *
     * @return string
     */
    private function generateInvoiceTitle(int $order_number = 0, string $purchase_order = '')
    {
        $return = '<div class="w-100 order-number">Order No. <span class="font-normal">' . $order_number . '</span></div>';
        if (!empty($purchase_order)) {
            $return .= '<div class="w-100 purchase-number">PO number <span class="font-normal">' . $purchase_order . '</span></div>';
        }
        $return .= '<div class="w-100 invoice-title">Invoice</div>
            <div class="w-100 invoice-number">Invoice No. <span class="font-normal">' . $this->invoiceNumber . '</span></div>';
        return $return;
    }

    /**
     * Generate order title
     *
     * @param integer $order_number   Order number.
     * @param string  $purchase_order Purchase order number.
     *
     * @return string
     */
    private function generateOrderTitle(int $order_number = 0, string $purchase_order = '')
    {
        $return = '';
        if (!empty($purchase_order)) {
            $return .= '<div class="w-100 purchase-number">PO number <span class="font-normal">' . $purchase_order . '</span></div>';
        }
        $return .= '<div class="w-100 order-title">Order</div>
            <div class="w-100 invoice-number">Order No. <span class="font-normal">' . $order_number . '</span></div>';
        return $return;
    }

    /**
     * Generate address base on stripe data.
     *
     * @param \stdClass $address Address data.
     *
     * @return string
     */
    private function generateAddressForStripePayment(\stdClass $address)
    {
        $html = '<p class="w-100 order-address">';
        if (!empty($address->line1)) {
            $html .= $address->line1 . '<br>';
        }
        if (!empty($address->line2)) {
            $html .= $address->line2 . '<br>';
        }
        if (!empty($address->city)) {
            $html .= $address->city . '<br>';
        }
        if (!empty($address->county)) {
            $html .= $address->county . '<br>';
        }
        if (!empty($address->state)) {
            $html .= $address->state . '<br>';
        }
        if (!empty($address->postal_code)) {
            $html .= $address->postal_code . '<br>';
        }
        if (!empty($address->country)) {
            $html .= $address->country . '<br>';
        }
        $html .= '</p>';

        return $html;
    }

    /**
     * Generate address base on order data.
     *
     * @param \stdClass $address Address data.
     *
     * @return string
     */
    private function generateAddressForOrderPayment(\stdClass $address)
    {
        $html = '<p class="w-100 order-address">';
        if (!empty($address->address)) {
            $html .= $address->address . '<br>';
        }
        if (!empty($address->postcode)) {
            $html .= $address->postcode . '<br>';
        }
        $html .= '</p>';

        return $html;
    }

    /**
     * Export payment invoice
     *
     * @param integer $payment_id Payment id.
     *
     * @return void
     */
    public function order(int $payment_id = 0)
    {
        $payment = $this->Payments->get($payment_id, ['contain' => ['Users' => ['Services', 'UserDetails']]]);
        $payment->paid_object = $this->getPaidObject($payment);
        if (!empty($payment)) {
            $fileName = $payment->created->format('Ymdhis') . '-' . $payment->id . '.pdf';
            $this->pdf->SetTitle($fileName);
            $this->pdf->SetSubject(__('Order') . ' ' . $fileName);
            $backgroundPage = $this->pdf->SetSourceFile(WWW_ROOT . '/pdf/background/invoice_template.pdf');
            $template = $this->pdf->ImportPage($backgroundPage);
            $this->pdf->UseTemplate($template);
            $this->createInvoicePage($payment, 'order');
            $this->pdf->Output($fileName, 'I');
        }
    }
}
