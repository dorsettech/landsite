<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\Controller\Panel;

use Cake\Filesystem\File;

/**
 * CustomCss
 *
 * @description Custom CSS module.
 */
class CustomCssController extends PanelController
{
    /**
     * Css dir location
     *
     * @var string
     */
    public $cssDir = WWW_ROOT . 'css' . DS;

    /**
     * Css file name
     *
     * @var string
     */
    public $cssFile = 'custom.css';

    /**
     * Css custom name
     *
     * @var string
     */
    public $customCssName = 'custom_css';

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadModel('Logs');
    }

    /**
     * Index
     *
     * @description Current custom CSS
     * @return Response|void
     */
    public function index()
    {
        $_removeAddButton = true;
        $_removeSearch = true;

        $content = $this->readCssFromFile();
        $versions = $this->getCssRestoredVersionList();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $oldContent = $content;
            $con = $this->request->getData('content');
            if (isset($con)) {
                $content = $con;
            }

            if ($this->saveCssToFile($content)) {
                $this->msgSuccess(__('Custom CSS has been saved.'));
                $this->saveVersion($oldContent);
                return $this->redirect(['action' => 'index']);
            } else {
                $this->msgError(__('Couldn\'t write changes to {0} file. Please check file or folder permissions.', $this->cssFile));
            }
        }

        $this->setHeading('title', 'Custom CSS');
        $this->setHeading('subtitle', '');

        $this->set(compact('content', 'versions', '_removeAddButton', '_removeSearch'));
        $this->set('_serialize', ['content']);
    }

    /**
     * Edit restored Css file
     *
     * @description Display CSS version details
     * @param integer|null $id Row id.
     * @return Response|void
     */
    public function edit($id = 0)
    {
        if ($id > 0) {
            $version = $this->Logs->find()
                                  ->select(['id', 'entity', 'created'])
                                  ->where(['id' => $id])
                                  ->first();

            if (!is_null($version)) {
                if ($this->request->is('post')) {
                    $content = "";
                    $con = $this->request->getData('content');
                    if (isset($con)) {
                        $content = $con;
                    }

                    if ($this->saveCssToFile($content)) {
                        $this->msgSuccess(__('Custom CSS has been saved.'));
                        $this->saveVersion($content);
                        return $this->redirect(['action' => 'edit', 'id' => $id]);
                    } else {
                        $this->msgError(__('Couldn\'t write changes to {0} file. Please check file or folder permissions.', $this->cssFile));
                    }
                }
                $content = $this->readCssFromFile();
                $restored = $this->convertContent($version->entity);
                $this->set(compact('content', 'restored', 'id', 'version'));
            } else {
                return $this->redirect(['action' => 'index']);
            }
        } else {
            return $this->redirect(['action' => 'index']);
        }
    }

    /**
     * Read css file
     *
     * @return File
     */
    protected function readCssFile()
    {
        return new File($this->cssDir . $this->cssFile, true);
    }

    /**
     * Read css from file
     *
     * @return false|string
     */
    protected function readCssFromFile()
    {
        $cssFile = $this->readCssFile();

        return $cssFile->read();
    }

    /**
     * Save css to file
     *
     * @param string|null $content Content.
     *
     * @return boolean
     */
    protected function saveCssToFile($content = '')
    {
        $cssFile = $this->readCssFile();
        $saved = $cssFile->write($content);
        $cssFile->close();

        return $saved;
    }

    /**
     * Save version
     *
     * @param string|null $content Content.
     *
     * @return mixed
     */
    protected function saveVersion($content = '')
    {
        $data = [
            'user_id' => isset($this->user['id']) ? $this->user['id'] : null,
            'ip' => $this->request->clientIp(),
            'host' => gethostbyaddr($this->request->clientIp()),
            'agent' => $this->request->getHeader('User-Agent'),
            'content' => __('{0} made changes in CustomCss file.', $this->user['first_name'] . ' ' . $this->user['last_name']),
            'url' => $this->request->getRequestTarget(),
            'entity' => serialize($content),
            'foreign_key' => '',
            'model' => 'CustomCss',
            'locale' => ''
        ];

        $version = $this->Logs->newEntity($data);
        return $this->Logs->save($version);
    }

    /**
     * Restore Css to Custom CSS
     *
     * @description Restore specific CSS version
     * @return Response
     */
    public function restore()
    {
        $param = $this->request->getParam('pass');
        if (is_array($param)) {
            if (isset($param[0])) {
                $id = (int)$param[0];
                $versions = $this->getCssRestoredVersionList($id)->toArray();
                if (isset($versions[0]) && !empty($versions[0]) && $versions[0]->id == $id) {
                    $content = $this->convertContent($versions[0]->entity);
                    if ($this->saveCssToFile($content)) {
                        $this->saveVersion($content);
                    }
                }
            }
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Css Restored List
     *
     * @param integer|null $versionId Version id.
     *
     * @return array|object
     */
    private function getCssRestoredVersionList($versionId = null)
    {
        $versions = $this->Logs->find('all')
                               ->select(['id', 'created', 'entity'])
                               ->where(['model' => 'CustomCss'])
                               ->where(function ($exp) {
                                   return $exp->isNotNull('entity');
                               })
                               ->order(['created' => 'DESC']);
        if (!empty($versionId) && is_numeric($versionId)) {
            $versions->where(['id' => $versionId])->first();
        }

        return $versions;
    }

    /**
     * Convert data
     *
     * @param string|null $content Content.
     *
     * @return string
     */
    private function convertContent($content = '')
    {
        return str_replace(["\"", "\\r\\n", "\\r", "\\"], ["", "\n", "\n", ""], $content);
    }
}
