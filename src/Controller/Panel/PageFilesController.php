<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-08)
 * @version 1.0
 */

namespace App\Controller\Panel;

use App\Model\Entity\PageFile;

/**
 * PageFiles Controller
 *
 * @description Page files module.
 */
class PageFilesController extends PanelController
{
    /**
     * Edit method
     *
     * @description Update page file
     * @param integer|null $id Training Image id.
     * @return Response|void
     */
    public function edit($id = null)
    {
        $pageFile = $this->PageFiles->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pageFile = $this->PageFiles->patchEntity($pageFile, $this->request->getData());
            if ($this->PageFiles->save($pageFile)) {
                $this->loadModel('Logs');
                $this->Logs->addLog($this->request, __("File has been saved."));
                $this->msgSuccess(__('File has been saved.'));
                return $this->redirect(['controller' => 'Pages', 'action' => 'edit-contents', $pageFile->page_id]);
            } else {
                $this->msgError(__('Error while saving file.'));
            }
        }
        $this->set(compact('pageFile'));
    }

    /**
     * Delete method
     *
     * @description Delete page file
     * @param integer|null $id Training Image id.
     * @return Response
     */
    public function delete($id = null)
    {
        $pageFile = $this->PageFiles->get($id);
        if ($this->PageFiles->delete($pageFile)) {
            $this->removeFile($pageFile->path);
            $this->loadModel('Logs');
            $this->Logs->addLog($this->request, __("File has been removed."));
            $this->msgSuccess(__('File has been removed.'));
        } else {
            $this->msgError(__('Error while removing file.'));
        }

        return $this->redirect($this->referer());
    }

    /**
     * Add files to page
     *
     * @description Upload files to page
     * @param integer|null $pageId Page id.
     * @param string|null  $type   Type.
     * @return Response
     */
    public function addFilesToPage($pageId = 0, $type = '')
    {
        $this->loadComponent('FileUpload', $this->getUploadComponentSettings($type));
        $this->render(false);
        if ($this->request->is(['put', 'patch', 'post'])) {
            $data = $this->request->getData();
            if (isset($data['images']) && is_array($data['images'])) {
                $entities = [];
                foreach ($data['images'] as $file) {
                    if (isset($file['name']) && !empty($file['name'])) {
                        $filePath = $this->FileUpload->upload($file);
                        if (!empty($filePath)) {
                            $entities[] = $this->PageFiles->newEntity([
                                                                          'page_id' => $pageId,
                                                                          'type' => $type,
                                                                          'path' => $filePath,
                                                                          'name' => $file['name'],
                                                                          'active' => 1,
                                                                          'position' => 99
                                                                      ]);
                        }
                    }
                }
                if (!empty($entities) && $this->PageFiles->saveMany($entities)) {
                    $this->loadModel('Logs');
                    $this->Logs->addLog($this->request, __("Files have been added."));
                    $this->msgSuccess(__("Files have been added."));
                } else {
                    $this->msgError(__('There was error while saving files.'));
                }

                return $this->redirect($this->referer());
            }
        }
    }

    /**
     * Get upload component settings
     *
     * @param integer|null $type Gallery type.
     *
     * @return array
     */
    protected function getUploadComponentSettings($type = 0)
    {
        switch ($type) {
            case PageFile::SLIDER:
                return ['directory' => 'pages_sliders', 'mime' => 'image/*', 'field' => 'path'];
                break;
            case PageFile::GALLERY:
                return ['directory' => 'pages_galleries', 'mime' => 'image/*', 'field' => 'path'];
                break;
            case PageFile::DOCUMENTS:
                return ['directory' => 'pages_documents', 'mime' => ['application/pdf', 'application/msword'], 'field' => 'path'];
                break;
        }
    }
}
