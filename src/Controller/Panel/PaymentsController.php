<?php

namespace App\Controller\Panel;

use App\Controller\Panel\PanelController;

/**
 * Payments Controller
 *
 * @property \App\Model\Table\PaymentsTable $Payments

 */
class PaymentsController extends PanelController
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users' => ['UserDetails', 'Services', 'Properties']]
        ];
        $payments = $this->paginate($this->Payments);

        $this->set(compact('payments'));
    }
}
