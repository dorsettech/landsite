<?php

namespace App\Controller\Panel;

use App\Model\Entity\Group;
use App\Model\Enum\AdType;
use App\Model\Enum\CommonUseClass;
use App\Model\Enum\PropertyPriceQualifier;
use App\Model\Enum\PropertyPriceRentPer;
use App\Model\Enum\PropertyPriceSalePer;
use App\Model\Enum\PropertyPurpose;
use App\Model\Enum\PropertyStatus;
use App\Model\Enum\State;
use App\Model\Table\PropertiesTable;
use App\Model\Table\PropertyAttributesTable;
use App\Traits\SwitcherTrait;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Response;
use Cake\Mailer\MailerAwareTrait;

/**
 * Properties Controller
 *
 * @property PropertiesTable         $Properties
 * @property PropertyAttributesTable $PropertyAttributes
 * @todo psr-2 @stefan
 */
class PropertiesController extends PanelController
{
    use SwitcherTrait;
    use MailerAwareTrait;

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
    }

    /**
     * Index method
     *
     * @return Response|void
     */
    public function index()
    {
        if ($this->Properties->behaviors()->has('SoftDelete')) {
            $this->Properties->removeBehavior('SoftDelete');
        }

        $this->paginate = [
            'order' => ['Properties.modified' => 'DESC', 'Properties.id' => 'DESC']
        ];

        $properties = $this->paginate($this->Properties->getProperties());

        $this->set(compact('properties'));
    }

    /**
     * Add method
     *
     * @return Response|null Redirects on successful add, renders view otherwise.
     * @throws \ReflectionException
     */
    public function add()
    {
        $property = $this->Properties->newEntity();

        if ($this->request->is('post')) {
            $property = $this->Properties->patchEntity($property, $this->request->getData());

            if ($this->Properties->save($property)) {
                $this->Flash->success(__('Property has been aded.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Property could not be added. Please, try again.'));
        }

        $propertyTypes = $this->Properties->PropertyTypes->find('list', ['limit' => 200]);
        $users = $this->Properties->Users->getUsersList(['Users.group_id' => Group::MEMBERS], 5000);
        $propertyAttributes = [];
        $purposes = PropertyPurpose::getNamesList();
        $statuses = PropertyStatus::getNamesList();
        $adTypes = AdType::getNamesList();
        $priceRentTypes = PropertyPriceRentPer::getSelectList();
        $priceSaleTypes = PropertyPriceSalePer::getSelectList();
        $priceSaleQualifiers = PropertyPriceQualifier::getNamesList(PropertyPurpose::SALE);
        $priceRentQualifiers = PropertyPriceQualifier::getNamesList(PropertyPurpose::RENT);
        $commonUseClass = CommonUseClass::getNamesList();

        $this->set(compact('property', 'propertyTypes', 'users', 'propertyAttributes', 'purposes', 'statuses', 'adTypes', 'priceRentTypes', 'priceSaleTypes', 'priceSaleQualifiers', 'priceRentQualifiers', 'commonUseClass'));
    }

    /**
     * Edit method
     *
     * @param integer|null $id Property id.
     *
     * @return Response|null Redirects on successful edit, renders view otherwise.
     * @throws RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $property = $this->Properties->get($id, [
            'contain' => ['PropertyAttributes']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $property = $this->Properties->patchEntity($property, $this->request->getData(), ['associated' => ['PropertyAttributes._joinData']]);

            if ($this->Properties->save($property)) {
                $this->Flash->success(__('Property has been updated.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Property could not be updated. Please, try again.'));
        }

        $propertyTypes = $this->Properties->PropertyTypes->find('list', ['limit' => 200]);
        $users = $this->Properties->Users->getUsersList(['Users.group_id' => Group::MEMBERS], 5000);
        $propertyAttributes = $this->Properties->PropertyAttributes->getByType($property->type_id);
        $purposes = PropertyPurpose::getNamesList();
        $statuses = PropertyStatus::getNamesList();
        $adTypes = AdType::getNamesList();
        $priceRentTypes = PropertyPriceRentPer::getSelectList();
        $priceSaleTypes = PropertyPriceSalePer::getSelectList();
        $priceSaleQualifiers = PropertyPriceQualifier::getNamesList(PropertyPurpose::SALE);
        $priceRentQualifiers = PropertyPriceQualifier::getNamesList(PropertyPurpose::RENT);
        $commonUseClass = CommonUseClass::getNamesList();

        $this->set(compact('property', 'propertyTypes', 'users', 'propertyAttributes', 'purposes', 'statuses', 'adTypes', 'priceRentTypes', 'priceSaleTypes', 'priceSaleQualifiers', 'priceRentQualifiers', 'commonUseClass'));
    }

    /**
     * Delete method
     *
     * @param integer|null $id Property id.
     *
     * @return Response|null Redirects to index.
     * @throws RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $property = $this->Properties->get($id);

        if ($this->Properties->delete($property)) {
            $this->Flash->success(__('Property has been deleted.'));
        } else {
            $this->Flash->error(__('Property could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * View
     *
     * @param integer|null $id Service id.
     *
     * @return void Renders view.
     * @throws RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->setLayout('clean');

        if ($this->Properties->behaviors()->has('SoftDelete')) {
            $this->Properties->removeBehavior('SoftDelete');
        }

        $property = $this->Properties->getProperty($id);

        $this->setHeading('title', $property->title);
        $this->setHeading('subtitle', __('Property') . ($property->under_offer ? ' <span class="badge badge-info">' . _('Under Offer') . '</span>' : ''));

        $this->set('property', $property);
    }

    /**
     * Approve or reject
     *
     * @return Response|null Redirects to index.
     */
    public function approveOrReject()
    {
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if (!empty($data['id'])) {
                try {
                    $property = $this->Properties->get($data['id'], ['contain' => ['Users']]);
                } catch (RecordNotFoundException $exception) {
                    $this->Flash->error(__('The property does not exists or is deleted. Action cannot be done.'));
                    return $this->redirect($this->getRequest()->referer());
                }

                $property = $this->Properties->patchEntity($property, $data);

                if ($this->Properties->save($property)) {
                    /*
                     * Waiting For Approval notification has been disabled
                     * @see LS-413
                     */
                    /*
                    if ($property->state === State::APPROVED) {
                         $this->getMailer('Properties')->send('propertyApprove', [$property]);
                    }
                    */
                    if ($property->state === State::REJECTED) {
                        $this->getMailer('Properties')->send('propertyReject', [$property]);
                    }
                    $this->Flash->success(__('{0} has been updated.', $property->title));
                } else {
                    $this->Flash->error(__('{0} could not be updated. Please, try again.', $property->title));
                }
            }
        }
        return $this->redirect($this->getRequest()->referer());
    }
}
