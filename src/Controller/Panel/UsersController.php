<?php
/**
 * @author  Stefan
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-08)
 * @version 1.0
 */

namespace App\Controller\Panel;

use App\Model\Entity\Group;
use App\Model\Entity\User;
use App\Traits\SwitcherTrait;
use Cake\Core\Configure;
use Cake\Http\Response;
use Cake\I18n\Time;
use Cake\Mailer\MailerAwareTrait;
use Cake\Utility\Text;

/**
 * UsersController
 *
 * @description Users module.
 */
class UsersController extends PanelController
{

    /**
     * Switcher trait
     */
    use SwitcherTrait;

    /**
     * MailerAware trait
     */
    use MailerAwareTrait;
    
    /**
     * Bad logins count
     *
     * @var integer
     */
    public $badLogins = 3;

    /**
     * Ban time in seconds
     *
     * @var integer
     */
    public $banTime = 900;

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadModel('Users');
        $this->loadModel('Logs');
        $this->loadModel('Loginbans');
        $this->Loginbans->setBanTime($this->banTime);

        $this->set('_image_indicators', $this->getPossibleFileSizeAndTypes('image'));
    }

    /**
     * Users list
     *
     * @description Users list
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'order' => ['Users.created' => 'DESC', 'Users.id' => 'DESC']
        ];

        $users = $this->paginate($this->Users->getUsers(['Users.group_id !=' => Group::MEMBERS]));

        $this->set('users', $users);
        $this->set('_serialize', 'users');
    }

    /**
     * Add user
     *
     * @description Add user
     * @return Response|void
     */
    public function add()
    {
        if (!$this->Auth->user('is_root') && !$this->Auth->user('is_admin')) {
            $this->redirect(['controller' => 'Users', 'action' => 'index']);
        }

        $user = $this->Users->newEntity();
        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData();

            if (!$this->Auth->user('is_root') && !$this->Auth->user('is_admin')) {
                $data['group_id'] = $user['group_id'];
                $data['active']   = $user['active'];
            }

            $options = [];
            if (isset($data['user_detail'], $data['user_detail']['use_billing_details']) && $data['user_detail']['use_billing_details'] == 0) {
                $options = [
                    'associated' => ['UserDetails', 'UserBillingAddresses' => ['validate' => 'mandatory']]
                ];
            }

            $user = $this->Users->patchEntity($user, $data, $options);
            if ($this->Users->save($user)) {
                $this->msgSuccess(__('User has been added.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'index']);
            } else {
                $this->msgError(__('User has not been added, try again.'));
            }
        }

        $groups = $this->Users->Groups->getGroupsList($this->user['group_id']);
        $this->set(compact('user', 'groups'));
    }

    /**
     * Edit user details
     *
     * @description Edit user
     * @param integer|null $id User id.
     * @return Response|void
     */
    public function edit($id = null)
    {
        if ($id == null) {
            $id = (isset($this->user['id'])) ? $this->user['id'] : null;
        }
        if (!is_numeric($id)) {
            return $this->redirect(['controller' => 'Users', 'action' => 'index']);
        }

        $user = $this->Users->get($id, [
            'contain'    => ['UserDetails', 'UserBillingAddresses']
        ]);

        if ($this->user['id'] != $user->id) {
            $this->redirectIfNotAllowed($user['group_id']);
        }

        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $data = $this->getRequest()->getData();
            if ($data['password'] == '') {
                unset($data['password']);
            }
            if (!$this->Auth->user('is_root') && !$this->Auth->user('is_admin')) {
                $data['group_id'] = $user['group_id'];
                $data['active']   = $user['active'];
            }
            if (!empty($data['remove_image']) && $data['remove_image'] == true) {
                $data['image'] = null;
            }

            $options = [];
            if (isset($data['user_detail'], $data['user_detail']['use_billing_details']) && $data['user_detail']['use_billing_details'] == 0) {
                $options = [
                    'associated' => ['UserDetails', 'UserBillingAddresses' => ['validate' => 'mandatory']]
                ];
            }

            $user = $this->Users->patchEntity($user, $data, $options);

            if ($this->Users->save($user)) {
                if ($this->user['id'] == $user->id) {
                    $this->msgSuccess(__("Profile has been updated.", $user->full_name));
                    return $this->redirect(['controller' => 'Users', 'action' => 'edit']);
                }

                $this->msgSuccess(__("User {0} has been updated.", $user->full_name));
                return $this->redirect(['controller' => 'Users', 'action' => 'index']);
            } else {
                $this->msgError(__("User {0} has not been updated.", $user->full_name));
            }
        }

        $groups = $this->Users->Groups->getGroupsList($this->user['group_id']);
        $this->set(compact('user', 'groups'));
    }

    /**
     * Mark user as deleted
     *
     * @description Delete user
     * @param integer|null $id User id.
     * @return Response
     */
    public function delete($id = null)
    {
        if (!is_numeric($id)) {
            return $this->redirect(['controller' => 'Users', 'action' => 'index']);
        }

        $user = $this->Users->get($id);
        $this->redirectIfNotAllowed($user['group_id']);

        if ($this->getRequest()->is(['post', 'delete'])) {
            $user->deleted = true;
            if ($this->Users->save($user)) {
                $this->msgSuccess(__("User {0} has been removed.", $user->full_name));
            } else {
                $this->msgError(__("User {0} has not been removed.", $user->full_name));
            }
        }

        return $this->redirect($this->getRequest()->referer());
    }

    /**
     * Panel login action
     *
     * @description Sign in
     * @return Response
     */
    public function login()
    {
        if ($this->Auth->isAuthorized()) {
            $this->redirect(['controller' => $this->adminDashboard, 'action' => 'index']);
        }

        if ($this->getRequest()->is(['ajax'])) {
            $this->getRequest()->allowMethod(['post']);
            return $this->unlockScreen();
        }

        $banIp = $this->Loginbans->checkIpBan($this->getRequest()->clientIp());

        if ($banIp) {
            $this->msgError(__('Sorry, but you can\'t access this website.'));
        }

        if (!$banIp && $this->getRequest()->is('post')) {
            $user = $this->Auth->identify();

            if ($user && !$this->isTokenValid($user)) {
                $this->Auth->setUser($user);
                $this->tokenGenerate($user);
                $this->Logs->addLog($this->getRequest(), __('Token generated for user.'));

                return $this->redirect(['action' => 'token']);
            } elseif ($user && $this->isTokenValid($user)) {
                $this->Auth->setUser($user);
                $this->Logs->addLog($this->getRequest(), __('User logged in.'));

                return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
            } else {
                $badLogin = $this->badLogin();
                if ($badLogin) {
                    $this->msgError(__('Sorry, but you can\'t access website at this time.'));
                } else {
                    $this->msgError(__('Incorrect data, please try again.'));
                }
            }
        }

        $this->setLoginVariables();
    }

    /**
     * Administration logout action
     *
     * @description Sign out
     * @return Response|void
     */
    public function logout()
    {
        $this->Logs->addLog($this->getRequest(), 'User logged out.');
        $this->Auth->logout();
        $this->session->destroy();
        $this->msgSuccess(__('Logged out.'));
        $this->redirect(['action' => 'login']);
    }

    /**
     * Register bad login
     *
     * @return Time
     */
    protected function badLogin()
    {
        $remoteAddr = $this->getRequest()->clientIp();

        $badLoginCount = $this->Loginbans->getBanCount($remoteAddr);

        $banExpires = null;
        if (($badLoginCount + 1) >= $this->badLogins) {
            $banExpires = new Time("+" . $this->banTime . "seconds");
        }

        $data = [
            'user'        => $this->getRequest()->getData('email'),
            'remote_addr' => $remoteAddr,
            'ban_expires' => $banExpires
        ];

        $this->Loginbans->saveBadLogin($data);

        return $banExpires;
    }

    /**
     * Unlock screen
     *
     * @description Unlock screen
     * @return Response
     */
    public function unlockScreen()
    {
        $data = $this->getRequest()->getData();
        if (!isset($data['email'])) {
            return $this->redirect(['action' => 'index']);
        }

        if ($this->getRequest()->allowMethod('post')) {
            if ($data['email'] !== null) {
                $user = $this->Auth->identify();
                if ($user) {
                    $this->Auth->setUser($user);
                    $response = ['status' => true];
                } else {
                    $response = ['status' => false];
                }

                return $this->response->withType('application/json')->withStringBody(json_encode($response));
            }
        }

        return $this->response;
    }

    /**
     * Refresh on alert click
     *
     * @description Refresh on alert click (ajax)
     * @return Response
     */
    public function refreshOnAlertClick()
    {
        $data = $this->getRequest()->getData();
        $this->getRequest()->allowMethod('POST');
        if ($data['message'] === "Renew") {
            $session  = $this->getRequest()->getSession();
            $session->renew();
            $response = ['status' => true];
        } elseif ($data['message'] === "Redirect") {
            $session  = $this->getRequest()->getSession();
            $session->renew();
            $response = ['status' => false];
        }

        return $this->response->withType('application/json')->withStringBody(json_encode($response));
    }

    /**
     * Token
     *
     * @description Token confirmation page
     * @return Response|null
     */
    public function token()
    {
        if ($this->isTokenAuthorized($this->user)) {
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }

        $this->setLayout('login');
        $this->loadModel('Users');

        if ($this->getRequest()->is(['post'])) {
            $data = $this->getRequest()->getData();

            $user = $this->Users->get($this->user['id']);

            if ($user->token == $data['token']) {
                $now                    = new Time();
                $user->token_authorized = $now->format('Y-m-d H:i:s');

                if ($this->Users->save($user)) {
                    $this->Auth->setUser($user);
                    $this->msgSuccess(__('Thank you for updating token.'));

                    return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
                } else {
                    $this->msgError(__('Token could not be updated. Please try again.'));
                }
            } else {
                $this->msgError(__('Token is not valid. Please try again.'));
            }
        }

        $this->setLoginVariables();
    }

    /**
     * Generate token
     *
     * @description Generate token
     * @return Response|null
     */
    public function generateToken()
    {
        if (!$this->isTokenValid($this->user)) {
            $this->tokenGenerate($this->user);
        }

        return $this->redirect($this->getRequest()->referer());
    }

    /**
     * Token update
     *
     * @param User $user User entity.
     * @return User
     */
    protected function tokenUpdate(User $user)
    {
        Configure::load('website', 'default');
        $configToken = Configure::read('Token');

        $user->token            = substr(Text::uuid(), 0, 8);
        $user->token_authorized = null;
        $user->token_expires    = new Time('+' . $configToken['days_valid'] . ' days');

        return $user;
    }

    /**
     * Token generate
     *
     * @param array $user User array.
     * @return boolean
     */
    protected function tokenGenerate(array $user)
    {
        $this->loadModel('Users');
        $userEntity = $this->Users->get($user['id']);
        $userEntity = $this->tokenUpdate($userEntity);

        if ($this->Users->save($userEntity)) {
            $expires = new Time($userEntity->token_expires);

            $this->getMailer('Users')->send('token', [$userEntity, $expires->nice()]);

            $this->msgSuccess(__('A new token has been generated. Please check your mailbox to get it.'));

            return true;
        } else {
            $this->msgError(__('An error occured while generating token.'));
        }

        return false;
    }

    /**
     * Set login variables
     *
     * @return void
     */
    protected function setLoginVariables(): void
    {
        $this->loadModel('StaticContents');
        $loginTitle  = $this->StaticContents->getValue('login_title');
        $loginSlogan = $this->StaticContents->getValue('login_slogan');

        $this->set(compact(['loginTitle', 'loginSlogan']));
    }
}
