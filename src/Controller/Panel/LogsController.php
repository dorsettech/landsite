<?php
/**
 * @author  Zordon (dawid@econnect4u.pl)
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-08)
 * @version 1.0
 */

namespace App\Controller\Panel;

use Cake\Http\Response;
use Cake\I18n\I18n;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Query;

/**
 * Logs Controller
 *
 * @description Logs module.
 */
class LogsController extends PanelController
{
    /**
     * Paginate config
     *
     * @var array
     */
    public $paginate = [
        'limit' => 20,
        'order' => [
            'Logs.id' => 'desc'
        ]
    ];

    /**
     * Url list
     *
     * @var array
     */
    public $urls = [
        '/panel/files/upload-files',
        '/panel/dashboard/login',
        '/panel/dashboard/logout'
    ];

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadModel('Pages');
        $this->loadModel('Contents');
        $this->set('_removeAddButton', true);
    }

    /**
     * Index Method
     *
     * @description Logs list
     * @return Response|void
     */
    public function index()
    {
        $createdFrom = $this->request->getQuery('createdFrom');
        $createdTo   = $this->request->getQuery('createdTo');
        if ($this->request->getQuery('date_from') || $this->request->getQuery('date_to')) {
            if ($this->delete() === true) {
                $this->Flash->success(__('Chosen records has been deleted'));
            } else {
                $this->Flash->error(__('Something went wrong'));
            }
            return $this->redirect(['action' => 'index']);
        }
        if (!empty($this->request->getQuery('search'))) {
            $logs = $this->filter();
            if ($logs === false) {
                $this->Flash->error(__('You picked wrong date range.'));
                return $this->redirect(['action' => 'index']);
            }
        } else {
            $logs = $this->Logs->find('all')->contain('Users')->order('Logs.created DESC');
        }

        $this->set('logs', $this->paginate($logs));
        $this->set('urls', $this->urls);
        $this->set(compact(['createdFrom', 'createdTo']));
    }

    /**
     * User logs view method
     *
     * @description View specific user logs
     * @param integer|null $userId User's id.
     * @return Response|void
     */
    public function viewUser($userId = 0)
    {
        if (!is_int(intval($userId))) {
            return $this->redirect(['action' => 'index']);
        }

        if (isset($this->request->getQuery['search'])) {
            $logs = $this->filter($userId);
            if ($logs === false) {
                $this->Flash->error('You picked wrong date range.');
                return $this->redirect(['action' => 'viewUser', $userId]);
            }
        } else {
            $logs = $this->Logs->find('all')
                    ->where(['user_id' => $userId])
                    ->contain('Users')->order('Logs.created DESC');
        }

        $this->set(compact('userId'));
        $this->set('logs', $this->paginate($logs));
    }

    /**
     * Show versions
     *
     * @description Show specific entry changes
     * @param string|null  $model Model name.
     * @param integer|null $id    Key id.
     * @return Response|void
     */
    public function showVersions($model = null, $id = null)
    {
        if (!isset($model, $id)) {
            return $this->redirect(['action' => 'index']);
        }

        $versions = $this->Logs->find('all')->where(['foreign_key' => $id, 'locale' => I18n::getLocale(), 'model' => $model])->order(['id' => 'ASC']);
        if ($versions === null) {
            return $this->redirect(['action' => 'index']);
        }
        $this->set('versions', $this->paginate($versions));
    }

    /**
     * Compare versions
     *
     * @description Compare versions
     * @param integer|null $versionId Version id.
     * @return Response|void
     */
    public function compareVersions($versionId = 0)
    {
        if (!isset($versionId)) {
            return $this->redirect(['action' => 'index']);
        }
        $version        = $this->Logs->find('all')->where(['id' => $versionId])->first();
        $this->loadModel($version->model);
        $currentVersion = $this->{$version->model}->find('all')->where([$version->model.'.id' => $version->foreign_key])->first();
        $oldVersion     = $version->entity;
        $this->set(compact('currentVersion', 'oldVersion', 'versionId'));
        $this->set('model', $version->model);
    }

    /**
     * RevertVersion
     *
     * @description Revert version
     * @param integer|null $id Row id.
     * @return Response|void
     * @throws NotFoundException NotFoundException.
     */
    public function revertVersion($id = 0)
    {
        $this->autoRender = false;
        $version          = $this->Logs->get($id);

        if (!$version) {
            throw new NotFoundException();
        }
        $this->loadModel($version->model);
        $entity = $this->{$version->model}->find('all')->where([$version->model.'.id' => $version->foreign_key])->first();

        $options = [];
        if ($version->model === "Pages") {
            $options = ['associated' => ['Contents']];
        }

        if (!empty($entity)) {
            $revertedEntity = $this->{$version->model}->patchEntity($entity, $version->entity->toArray(), $options);
        } else {
            $revertedEntity = $this->{$version->model}->newEntity($version->entity->toArray(), $options);
        }

        if ($this->{$version->model}->save($revertedEntity)) {
            $this->Flash->success(__('Data restored successfully.'));
        } else {
            $this->Flash->error(__('Data could not be restored.'));
        }

        $this->redirect($this->request->referer());
    }

    /**
     * Truncate
     *
     * @description Clear all logs
     * @return string
     */
    public function truncate()
    {
        $databaseConf = $this->Logs->connection()->config()['name'];

        $connection = ConnectionManager::get($databaseConf);
        $connection->execute('TRUNCATE TABLE `logs`');
        $this->Flash->success(__('All logs has been removed.'));

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Delete
     *
     * @description Delete logs
     * @return boolean
     */
    public function delete()
    {
        $date_from = $this->request->getQuery('date_from');
        $date_to   = $this->request->getQuery('date_to');
        if ($date_from >= $date_to && $date_to === "") {
            return false;
        } else {
            if (($date_from !== null && $date_to !== null) && ($date_from !== '' && $date_to !== '')) {
                $this->Logs->deleteAll(['Logs.created >=' => $date_from, 'Logs.created <=' => $date_to]);
            } else if ($date_from !== null && $date_to === "") {
                $this->Logs->deleteAll(['Logs.created >=' => $date_from]);
            } else if ($date_from === "" && $date_to !== null) {
                $this->Logs->deleteAll(['Logs.created <=' => $date_to]);
            }

            return true;
        }
    }

    /**
     * Filter
     *
     * @description Filter logs
     * @param integer|null $userId User id.
     * @return boolean|Query
     */
    public function filter($userId = null)
    {
        $createdFrom = $this->request->getQuery('createdFrom');
        $createdTo   = $this->request->getQuery('createdTo');
        if ($createdFrom >= $createdTo && $createdTo === "") {
            return false;
        }

        $logs = $this->Logs->find('all')->contain('Users');
        if (($createdFrom !== null && $createdTo !== null) && ($createdFrom !== '' && $createdTo !== '')) {
            $logs->where(['Logs.created >=' => $createdFrom, 'Logs.created <=' => $createdTo]);
        } else if ($createdFrom !== null && $createdTo === "") {
            $logs->where(['Logs.created >=' => $createdFrom]);
        } else if ($createdFrom === "" && $createdTo !== null) {
            $logs->where(['Logs.created <=' => $createdTo]);
        }
        if ($userId) {
            $logs->where(['user_id' => $userId])->order('Logs.created DESC');
        }

        return $logs;
    }

    /**
     * Save contents
     *
     * @description Save entity contents (@todo check if used)
     * @param object|null $revertedEntity Entity.
     * @return void
     */
    public function saveContents($revertedEntity)
    {
        if (isset($revertedEntity->contents)) {
            foreach ($revertedEntity->contents as $key => $content) {
                $entity = $this->Contents->get($content->id);
                $entity = $this->Contents->patchEntity($content, $entity->toArray());
                $this->Contents->save($entity);
            }
        }
    }
}
