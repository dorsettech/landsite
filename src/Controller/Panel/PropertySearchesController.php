<?php
/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */

namespace App\Controller\Panel;

use App\Traits\SwitcherTrait;

/**
 * PropertySearchesController
 *
 * @description Property search module
 */
class PropertySearchesController extends PanelController
{

    use SwitcherTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate   = [
            'contain' => ['Users', 'PropertyTypes'],
            'order'   => ['PropertySearches.created' => 'DESC']
        ];
        $propertySearches = $this->paginate($this->PropertySearches);

        $this->set(compact('propertySearches'));
    }

    /**
     * Delete property search
     *
     * @description Delete property search
     * @param integer|null $id User id.
     * @return Response
     */
    public function delete($id = null)
    {
        if (!is_numeric($id)) {
            return $this->redirect(['controller' => 'Members', 'action' => 'index']);
        }

        $propertySearch = $this->PropertySearches->get($id);

        if ($this->getRequest()->is(['post', 'delete'])) {
            if ($this->PropertySearches->delete($propertySearch)) {
                $this->msgSuccess(__("Property search #{0} has been removed.", $propertySearch->id));
            } else {
                $this->msgSuccess(__("Property search #{0} has not been removed.", $propertySearch->id));
            }
        }

        return $this->redirect($this->getRequest()->referer());
    }
}
