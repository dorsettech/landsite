<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-08)
 * @version 1.0
 */

namespace App\Controller\Panel;

use App\Model\Entity\StaticContent;
use Cake\Http\Response;

/**
 * StaticContentGroups Controller
 *
 * @description Layout variable groups module.
 *
 */
class StaticContentGroupsController extends PanelController
{
    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        $this->set('breadcrumb_controller', 'Layout Settings');
        $this->loadComponent('FileUpload', ['directory' => 'contents', 'mime' => ['image/*', 'application/pdf', 'application/msword']]);
        $this->loadModel('Logs');

        $this->setHeading('title', 'Layout Settings');
    }

    /**
     * Index
     *
     * @description Variable groups list
     * @return Response|void
     */
    public function index()
    {
        if (!$this->Auth->user('is_root')) {
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }
        $this->paginate = [
            'order' => ['name' => 'ASC']
        ];
        $staticContentGroups = $this->paginate($this->StaticContentGroups);
        $this->setHeading('panel', __('Layout Settings Group List'));
        $this->setHeading('title_singular', __('Layout Settings Group'));
        $this->set(compact('staticContentGroups'));
    }

    /**
     * Add method
     *
     * @description Create variable group
     * @return Response|void
     */
    public function add()
    {
        if (!$this->Auth->user('is_root')) {
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }
        $staticContentGroup = $this->StaticContentGroups->newEntity();
        if ($this->request->is('post')) {
            $staticContentGroup = $this->StaticContentGroups->patchEntity($staticContentGroup, $this->request->data);
            if ($this->StaticContentGroups->save($staticContentGroup)) {
                $this->msgSuccess(__('Layout variable group has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->msgError(__('Layout variable group could not be saved. Please, try again.'));
            }
        }
        $this->setHeading('panel', __('Add Layout Settings Group'));
        $this->set(compact('staticContentGroup'));
    }

    /**
     * View
     *
     * @description Variables group list
     * @param integer|null $id Row id.
     * @return Response|null
     */
    public function view($id = null)
    {
        if (!is_numeric($id)) {
            return $this->redirect(['action' => 'index']);
        }
        
        $group = $this->StaticContentGroups->get($id, ['contain' => ['StaticContents']]);

        $this->setHeading('subtitle', __('Group: {0}', h($group->name)));
        $this->setHeading('panel', h($group->name));
        $this->set('group', $group);
        $this->set('_removeSearch', true);
    }

    /**
     * Edit
     *
     * @description Update group variables at once
     * @param integer|null $id Row id.
     * @return Response|void
     */
    public function editGroup($id = null)
    {
        if (!is_numeric($id)) {
            return $this->redirect(['action' => 'index']);
        }

        $group = $this->StaticContentGroups->get($id, ['contain' => ['StaticContents']]);

        if (empty($group['static_contents'])) {
            $this->msgError(__('Layout settings group {0} doesn\'t have any variables added yet. Please add at least one variable first.', h($group->name)));

            return $this->redirect(['controller' => 'StaticContents', 'action' => 'add', $group->id]);
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $entities = [];
            $data = $this->request->getData();
            foreach ($group->static_contents as &$content) {
                if ($content->type === StaticContent::TYPE_FILE && !empty($data['content'][$content->id]['value']['name'])) {
                    $value = $this->FileUpload->upload($data['content'][$content->id]['value']);
                } elseif ($content->type === StaticContent::TYPE_FILE) {
                    $value = $content->value;
                } else {
                    $value = $data['content'][$content->id]['value'];
                }
                $entities[] = $this->StaticContentGroups->StaticContents->patchEntity($content, ['value' => $value]);
            }
            if ($this->StaticContentGroups->StaticContents->saveMany($entities)) {
                $this->msgSuccess(__('Group has been saved succesfully.'));
            } else {
                $this->msgError(__('Group could not be saved.'));
            }

            return $this->redirect(['action' => 'view', $id]);
        }

        $this->set('group', $group);
    }

    /**
     * Edit method
     *
     * @description Edit variable group
     * @param integer|null $id Static Content Group id.
     * @return Response|void
     */
    public function edit($id = null)
    {
        if (!$this->Auth->user('is_root')) {
            return $this->redirect(['action' => 'index']);
        }

        $staticContentGroup = $this->StaticContentGroups->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $staticContentGroup = $this->StaticContentGroups->patchEntity($staticContentGroup, $this->request->data);
            if ($this->StaticContentGroups->save($staticContentGroup)) {
                $this->msgSuccess(__('Layout variable group has been saved.'));

                return $this->redirect(['action' => 'view', $staticContentGroup->id]);
            } else {
                $this->msgError(__('Layout variable group could not be saved. Please, try again.'));
            }
        }
        $this->setHeading('panel', __('Edit Layout Settings Group'));
        $this->set(compact('staticContentGroup'));
    }

    /**
     * Delete method
     *
     * @description Delete variable group
     * @param integer|null $id Static Content Group id.
     * @return Response
     */
    public function delete($id = null)
    {
        if (!$this->Auth->user('is_root')) {
            return $this->redirect(['action' => 'index']);
        }

        $this->request->allowMethod(['post', 'delete']);
        $staticContentGroup = $this->StaticContentGroups->get($id);

        if (is_numeric($this->StaticContentGroups->StaticContents->deleteAll(['static_group_id' => $staticContentGroup->id]))) {
            if ($this->StaticContentGroups->delete($staticContentGroup)) {
                $this->msgSuccess(__('Layout variable group has been deleted.'));
            } else {
                $this->msgError(__('Layout variable group could not be deleted. Please, try again.'));
            }
        } else {
            $this->msgError(__('An error occured while removing layout variables. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
