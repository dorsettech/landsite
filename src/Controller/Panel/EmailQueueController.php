<?php

namespace App\Controller\Panel;

use App\Controller\Panel\PanelController;

/**
 * EmailQueue Controller
 *
 * @property \App\Model\Table\EmailQueueTable $EmailQueue

 */
class EmailQueueController extends PanelController
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $emailQueue = $this->EmailQueue->findByDate($this->getRequest()->getQuery());
        $emailQueue = $this->paginate($emailQueue);

        $this->set(compact('emailQueue'));
    }
}
