<?php

namespace App\Controller\Panel;

use App\Model\Table\SettingsTable;
use Cake\Http\Response;

/**
 * SettingsController
 *
 * @property SettingsTable $Settings
 */
class SettingsController extends PanelController
{
    /**
     * Initialize
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();
    }

    /**
     * Setting prices update
     *
     * @return Response
     */
    public function prices()
    {
        $settings = $this->Settings->getGroup('prices');

        if ($this->getRequest()->is(['post', 'patch', 'put'])) {
            $data = $this->getRequest()->getData('services');

            $settings = $this->Settings->patchEntities($settings, $data, ['validate' => 'panel']);

            if ($this->Settings->saveMany($settings)) {
                $this->msgSuccess(__('Prices has been updated.'));

                return $this->redirect(['action' => 'prices']);
            }

            $this->msgError(__('An error occured while updating prices.'));
        }

        $this->setHeading('title', __('Set Your Prices'));
        $this->setHeading('subtitle', __('Settings'));

        $this->set(compact('settings'));
    }
}
