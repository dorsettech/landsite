<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-08)
 * @version 1.0
 */

namespace App\Controller\Panel;

use Cake\ORM\TableRegistry;
use Cake\Filesystem\File;

/**
 * Class GalleryController
 *
 * @description Gallery module.
 */
class GalleryController extends PanelController
{
    /**
     * Manage gallery
     *
     * @description Manage gallery
     * @param string|null  $model      Model name.
     * @param integer|null $entityId   Id.
     * @param string|null  $field      Field name.
     * @param string|null  $controller Controller.
     * @param string|null  $action     Action name.
     * @param integer|null $editId     Row id.
     * @return Response|void
     */
    public function manage($model, $entityId, $field, $controller = null, $action = null, $editId = null)
    {
        $data = $this->getGalleryData($model, $entityId, $field);
        $files = $data['gallery'];
        if ($this->request->is('post')) {
            $images = $this->request->getData('images');
            if (is_array($images) && !empty($images)) {
                $this->loadComponent('FileUpload', ['directory' => strtolower($model)]);
                if (!is_array($files)) {
                    $galleryItems = [];
                } else {
                    $galleryItems = $files;
                }
                foreach ($this->request->getData('images') as $key => $row) {
                    $upload = $this->FileUpload->upload($row);
                    if ($upload) {
                        $galleryItems[] = [
                            'path' => $this->FileUpload->upload($row),
                            'alt' => '',
                            'title' => ''
                        ];
                    }
                }
                $patchedEntity = $data['model']->patchEntity($data['entity'], [$field => $galleryItems]);
                if ($data['model']->save($patchedEntity)) {
                    $this->Flash->success(__('Images added to gallery'));
                } else {
                    $this->Flash->error(__('Images couldnt be added gallery'));
                }
            } else {
                $this->Flash->error(__('Please select some images to upload.'));
            }

            return $this->redirect($this->referer());
        }

        $this->set(compact('model', 'entityId', 'field', 'files', 'controller', 'action', 'editId'));
    }

    /**
     * Get single image data
     *
     * @description Get single image data (ajax)
     * @related updateSingleImage, updateGalleryOrder
     * @return void
     */
    public function getSingleImageData()
    {
        $galleryData = $this->getGalleryData($this->request->getQuery('model'), $this->request->getQuery('entityId'), $this->request->getQuery('field'), $this->request->getQuery('key'));
        $data = $galleryData['image'];
        $this->set(compact('data'));
        $this->set('_serialize', 'data');
    }

    /**
     * Update single image
     *
     * @description Update single image data (ajax)
     * @related getSingleImageData, updateGalleryOrder
     * @return void
     */
    public function updateSingleImage()
    {
        $this->request->allowMethod('post');
        $requestData = $this->request->getData();
        $galleryData = $this->getGalleryData($requestData['model'], $requestData['entityId'], $requestData['field']);
        $galleryData['gallery'][$requestData['key']]['title'] = $requestData['data']['title'];
        $galleryData['gallery'][$requestData['key']]['alt'] = $requestData['data']['alt'];
        $patchedEntity = $galleryData['model']->patchEntity($galleryData['entity'], [$requestData['field'] => $galleryData['gallery']]);
        if ($galleryData['model']->save($patchedEntity)) {
            $data = ['message' => __('Image updated successfuly.'), 'status' => 200];
        } else {
            $data = ['message' => __('There was error while trying to update image.'), 'status' => 500];
        }
        $this->set(compact('data'));
        $this->set('_serialize', 'data');
    }

    /**
     * Update gallery order
     *
     * @description Update gallery order (ajax)
     * @related getSingleImageData, updateSingleImage
     * @return void
     */
    public function updateGalleryOrder()
    {
        $this->request->allowMethod('post');
        $requestData = $this->request->getData();
        $galleryData = $this->getGalleryData($requestData['model'], $requestData['entityId'], $requestData['field']);

        $newData = [];
        foreach ($requestData['data'] as $oldKey => $newKey) {
            $newData[$newKey] = $galleryData['gallery'][$oldKey];
        }
        ksort($newData);
        $patchedEntity = $galleryData['model']->patchEntity($galleryData['entity'], [$requestData['field'] => $newData]);
        if ($galleryData['model']->save($patchedEntity)) {
            $data = ['message' => __('Positions for gallery have been updated.'), 'status' => 200];
        } else {
            $data = ['message' => __('There was error while trying to update gallery order.'), 'status' => 500];
        }
        $this->set(compact('data'));
        $this->set('_serialize', 'data');
    }

    /**
     * Delete single gallery image
     *
     * @description Delete gallery image
     * @param string|null  $modelName Model name.
     * @param integer|null $entityId  Entity id.
     * @param string|null  $field     Field name.
     * @param string|full  $imageKey  Image key.
     * @return void
     */
    public function deleteSingleGalleryImage($modelName, $entityId, $field, $imageKey)
    {
        $this->request->allowMethod('delete');
        $galleryData = $this->getGalleryData($modelName, $entityId, $field, $imageKey);
        $file = new File($galleryData['image']['path']);
        $file->delete();
        unset($galleryData['gallery'][$imageKey]);
        $patchedEntity = $galleryData['model']->patchEntity($galleryData['entity'], [$field => $galleryData['gallery']]);
        if ($galleryData['model']->save($patchedEntity)) {
            $data = ['message' => __('Image removed successfuly.'), 'status' => 200];
        } else {
            $data = ['message' => __('There was error while trying to remove image.'), 'status' => 500];
        }
        $this->set(compact('data'));
        $this->set('_serialize', 'data');
    }

    /**
     * Get gallery data
     *
     * @param string|null  $modelName Model name.
     * @param integer|null $entityId  Entity id.
     * @param string|null  $field     Field name.
     * @param string|full  $imageKey  Image key.
     *
     * @return array
     */
    private function getGalleryData($modelName, $entityId, $field, $imageKey = null)
    {
        $data['model'] = TableRegistry::getTableLocator()->get($modelName);
        if ($entityId) {
            $data['entity'] = $data['model']->get($entityId);
            $data['gallery'] = $data['entity']->{$field};
        }
        if (is_numeric($imageKey)) {
            $data['image'] = $data['gallery'][$imageKey];
        }

        return $data;
    }
}
