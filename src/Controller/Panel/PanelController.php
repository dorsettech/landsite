<?php
/**
 * @author  Stefan
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-08)
 * @version 1.0
 */

namespace App\Controller\Panel;

use App\Controller\AppController;
use App\Library\ACL;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Utility\Inflector;

/**
 * PanelController
 */
class PanelController extends AppController
{
    /**
     * Helpers list
     *
     * @var array
     */
    public $helpers = [
        'Common',
        'Transform',
        'Page',
        'Menu',
        'Images',
        'Urlname'
    ];

    /**
     * Components list
     *
     * @var array
     */
    public $components = ['FormBuilder'];

    /**
     * Logged user details
     *
     * @var array
     */
    protected $user = [];

    /**
     * Session object
     *
     * @var object
     */
    public $session = null;

    /**
     * Default admin prefix
     * It should be replaced with variable from config file
     *
     * @var string
     */
    public $adminPrefix = 'panel';

    /**
     * Default dashboard url
     * It should be replaced with variable from config file
     *
     * @var string
     */
    public $adminDashboard = 'Dashboard';

    /**
     * Group related vars
     *
     * @var int
     */
    public $rootGroupId      = 5;
    public $adminGroupId     = 4;
    public $moderatorGroupId = 3;
    public $memberGroupId    = 2;
    public $guestGroupId     = 1;

    /**
     * Layout related
     *
     * @var string
     */
    public $breadcrumb = [];

    /**
     * Heading array
     *
     * Used for display page heading and panel title
     *
     * @var array
     */
    public $heading = [];

    /**
     * Meta details
     *
     * @var array
     */
    public $meta = [];

    /**
     * Model alias
     *
     * @var string
     */
    protected $modelAlias = '';

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');

        $this->setSession();
        $this->setModelVariables();
        $this->initHeading();

        $authOptions = [
            'authorize'    => ['Controller'],
            'loginAction'  => [
                'plugin'     => null,
                'prefix'     => $this->adminPrefix,
                'controller' => 'Users',
                'action'     => 'login'
            ],
            'authError'    => __('You don\'t have access to this page.'),
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ]
                ]
            ],
            'storage'      => 'SessionEx'
        ];

        $this->loadComponent('Auth', $authOptions);
        $layout = $this->Auth->user() ? 'default' : 'login';
        $this->setLayout($layout);
        $this->setSessionTimeout();
        $this->setAllowedGroups();
    }

    /**
     * Set session
     *
     * @return void
     */
    private function setSession(): void
    {
        $this->session = $this->getRequest()->getSession();
    }

    /**
     * Set model layer variables
     *
     * @return void
     */
    private function setModelVariables(): void
    {
        $loaded           = $this->loadModel($this->modelClass);
        $this->modelAlias = $loaded->getAlias();

        $this->{$this->modelAlias}->prefix = $this->request->getParam('prefix');
    }

    /**
     * Set session life time variable
     *
     * @return void
     */
    public function setSessionTimeout()
    {
        $sessionTimeout = $this->getConf('Session.timeout');

        if ($sessionTimeout === null) {
            $sessionTimeout = ini_get('session.gc_maxlifetime');
        } elseif (is_numeric($sessionTimeout)) {
            $sessionTimeout = $sessionTimeout * 60;
        }

        Configure::write('Session', [
            'defaults' => 'php',
            'ini'      => ['session.cookie_lifetime' => $sessionTimeout]
        ]);

        $this->set('_session_lifetime', $sessionTimeout);
    }

    /**
     * Has access
     *
     * @param array|null $permissions Permissions array.
     * @return boolean
     */
    private function hasAccess($permissions = [])
    {
        if ($this->Auth->user('is_root')) {
            return ACL::ROOT_ACCESS;
        }
        
        if (empty($permissions) || !isset($permissions[$this->getRequest()->getParam('controller')][$this->getRequest()->getParam('action')])) {
            return ACL::HAS_ACCESS;
        }

        return $permissions[$this->getRequest()->getParam('controller')][$this->getRequest()->getParam('action')];
    }

    /**
     * Before render event
     *
     * @param Event $event Event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);

        $this->tokenRedirect();
        $this->setPanelVariables();
        $this->setUnreadMessages();
        $this->removeSearch();
    }

    /**
     * Token redirect
     *
     * @return mixed Redirects to token page if token has not be authorized.
     */
    protected function tokenRedirect()
    {
        if ($this->Auth->user() && !$this->isTokenAuthorized($this->user) && $this->getRequest()->getParam('action') != 'token') {
            return $this->redirect(['controller' => 'Users', 'action' => 'token']);
        }
    }

    /**
     * Is token authorized
     *
     * @param array $user User array.
     * @return boolean
     */
    protected function isTokenAuthorized(array $user)
    {
        return (!empty($user['token_authorized']) && !empty($user['token_expires']) && $user['token_authorized'] < $user['token_expires']);
    }

    /**
     * Is token expired
     *
     * @param array $user User array.
     * @return boolean
     */
    protected function isTokenExpired(array $user)
    {
        return (!empty($user['token_expires']) && $user['token_expires'] < (new Time()));
    }

    /**
     * Is token valid
     *
     * @param array $user User array.
     * @return boolean
     */
    protected function isTokenValid(array $user = [])
    {
        if (!empty($user['token']) && $this->isTokenAuthorized($user) && !$this->isTokenExpired($user)) {
            return true;
        }

        return false;
    }

    /**
     * Set panel variables
     *
     * @return void
     */
    public function setPanelVariables()
    {
        $this->adminPrefix    = $this->getConf('Website.admin_prefix');
        $this->adminDashboard = $this->getConf('Website.admin_panel');

        $this->setMeta(['title' => $this->getConf('Website.service') . ' - ' . $this->heading['title'] . ' - ' . $this->heading['subtitle']]);

        $this->set('_admin_prefix', $this->adminPrefix);
        $this->set('_admin_panel', $this->adminDashboard);
        $this->set('sidebar_menu', $this->getSidebarMenu());
        $this->set('heading', $this->heading);
        $this->set('meta', $this->meta);
        $this->set('_filter_keyword', $this->getRequest()->getQuery('filter_keyword'));
    }

    /**
     * Check if user is authorized
     *
     * @param array|null $user Logged user data.
     * @return boolean
     */
    public function isAuthorized($user = null)
    {
        if (!isset($user['group_id'])) {
            $group_id = 1;
            $this->set('_user', null);
        } else {
            if (isset($user['active']) && !$user['active']) {
                $this->msgError(__('Account is not active.'));
                return false;
            }

            $this->loadModel('Users');
            $user = $this->Users->get($this->Auth->user('id'), [
                'contain' => ['Groups'],
                'filter' => false
            ]);
            if (isset($user['group']) && !empty($user['group'])) {
                $user['is_admin'] = ($user['group']->id == $this->adminGroupId);
                $user['is_root']  = ($user['group']->id == $this->rootGroupId);
            }

            $this->set('_user', $user);
            $this->Auth->setUser($user->toArray());
            $this->user = $user->toArray();

            $group_id = (!empty($group_id) && is_numeric($group_id)) ? $group_id : $user['group_id'];
        }

        if (!isset($group_id)) {
            return false;
        }
        $this->groupId = $group_id;

        $prefix     = $this->request->getParam('prefix', '');
        $controller = $this->request->getParam('controller');
        $action     = $this->request->getParam('action');

        $this->set(compact('prefix', 'controller', 'action'));

        $hasAccess = $this->hasAccess($user['group']['permissions']);

        $this->set([
            '_permissions' => $user['group']['permissions']
        ]);

        if ($hasAccess) {
            $this->Auth->allow();
        } else {
            $this->Auth->deny();
        }

        return $hasAccess;
    }

    /**
     * Show a message using Flash->success(). If no message is set, then function uses a default and generic 'action has been successful' value.
     *
     * @param string|null $message Message to be shown.
     * @return void
     */
    public function msgSuccess($message = '')
    {
        if (empty($message)) {
            $message = __('Action has been successful.');
        }
        $this->Flash->success($message);
    }

    /**
     * Show a message using Flash->error(). If no message is set, then function uses a default and generic 'an error occurred' value.
     *
     * @param string|null $message Message to be shown.
     * @return void
     */
    public function msgError($message = '')
    {
        if (empty($message)) {
            $message = __('An error occurred.');
        }
        $this->Flash->error($message);
    }

    /**
     * Set unread messages
     *
     * @return void
     */
    public function setUnreadMessages()
    {
        $this->loadModel('FormsData');
        $unreadMessages = $this->FormsData->find('all', [
            'contain'    => ['Forms'],
            'conditions' => [
                'FormsData.view' => false,
            ],
            'order'      => ['FormsData.created' => 'DESC'],
            'limit'      => 10
        ]);

        $this->set('unreadMessages', $unreadMessages);
    }

    /**
     * Set allowed groups
     *
     * @return void
     */
    protected function setAllowedGroups()
    {
        if ($this->Auth->user()) {
            $this->loadModel('Groups');
            $this->allowedGroups = $this->Groups->getAllowedGroupIds($this->Auth->user('group_id'));

            $this->set('_allowedGroups', $this->allowedGroups);
        }
    }

    /**
     * Redirect if not allowed
     *
     * @param integer|null $userGroupId Group id.
     * @return mixed
     */
    protected function redirectIfNotAllowed($userGroupId = 0)
    {
        if (!($this->Auth->user('is_root') || $this->Auth->user('is_admin')) || !in_array($this->Auth->user('group_id'), $this->allowedGroups) || !in_array($userGroupId, $this->allowedGroups)) {
            $this->msgError(__('You are not allowed to access that resource.'));
            return $this->redirect(['action' => 'index']);
        }
    }

    /**
     * Get sidebar menu from configuration file
     *
     * @return array
     */
    protected function getSidebarMenu()
    {
        Configure::load('sidebar');

        return $this->getConf('Sidebar', []);
    }

    /**
     * Remove search
     *
     * @return void Sets _removeSearch variable to true if Filter behavior has been added to modelClass
     */
    protected function removeSearch()
    {
        $removeSearch = true;

        if ($this->request->getParam('action') === 'index' && (!empty($this->modelAlias) && $this->{$this->modelAlias}->behaviors()->has('Filter'))) {
            $removeSearch = false;
        }

        $this->set('_removeSearch', $removeSearch);
    }

    /**
     * Set meta
     *
     * @param array $array Meta details array - available keys: title, keywords, description.
     * @return void
     */
    protected function setMeta(array $array = []): void
    {
        $this->meta['title']       = (!empty($array['title'])) ? $array['title'] : __('Panel administration');
        $this->meta['keywords']    = (!empty($array['keywords'])) ? $array['keywords'] : '';
        $this->meta['description'] = (!empty($array['description'])) ? $array['description'] : '';
    }

    /**
     * Init page heading
     *
     * @param array $options Additional options to set, possible array keys: title, subtitle, panel.
     * @return void Sets the heading array.
     */
    protected function initHeading(array $options = []): void
    {
        $controller = $this->request->getParam('controller');
        $action     = $this->request->getParam('action');

        $title = (!empty($options['title'])) ? $options['title'] : Inflector::humanize(Inflector::delimit($controller));
        if (empty($title)) {
            $title = '';
        }
        $titleSingular = Inflector::singularize($title);

        switch ($action) {
            case 'index':
                $subtitle = __('List');
                $panel    = $title . ' ' . $subtitle;
                break;
            default:
                $subtitle = ucwords($action);
                $panel    = $subtitle . ' ' . $titleSingular;
                break;
        }

        $subtitle = (!empty($options['subtitle'])) ? $options['subtitle'] : $subtitle;
        $panel    = (!empty($options['panel'])) ? $options['panel'] : $panel;

        $this->heading = [
            'controller'     => $controller,
            'action'         => $action,
            'title'          => $title,
            'title_singular' => $titleSingular,
            'subtitle'       => $subtitle,
            'panel'          => $panel,
            'link_add'       => ['action' => 'add'],
            'link_close'     => ['action' => 'index']
        ];
    }

    /**
     * Set specific heading option
     *
     * @param string $key   Heading key.
     * @param mixed  $value Heading key value.
     * @return void
     */
    protected function setHeading(string $key, $value): void
    {
        $this->heading[$key] = $value;
    }
}
