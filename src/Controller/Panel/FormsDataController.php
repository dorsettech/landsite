<?php
/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-08)
 * @version 1.0
 */

namespace App\Controller\Panel;

use Cake\Http\Response;

/**
 * Class FormsDataController
 *
 * @description Form messages module.
 */
class FormsDataController extends PanelController
{
    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Forms');
        $this->loadModel('FormsData');
    }

    /**
     * Index
     *
     * @description Redirect to forms list if no action given
     * @return Response|void
     */
    public function index()
    {
        $this->redirect(['controller' => 'Forms', 'action' => 'index']);
    }

    /**
     *
     * List view
     *
     * @description Messages list
     * @param integer|null $idForm Form id.
     * @return Response|void
     */
    public function listView($idForm = null)
    {
        if (!is_numeric($idForm)) {
            return $this->redirect(['controller' => 'Forms', 'action' => 'index']);
        }

        $to   = date('Y-m-d H:i:s');
        $from = date("Y-m-d H:i:s", strtotime("-6 months", strtotime($to)));

        $conditions = ['FormsData.created <=' => $to, 'FormsData.created >=' => $from];
        if ($this->request->is('post')) {
            $conditions = [];
            if (!empty($this->request->data['from'])) {
                $from                               = $this->request->data['from'];
                $conditions['FormsData.created >='] = $from;
            }
            if (!empty($this->request->data['to'])) {
                $to                                 = $this->request->data['to'];
                $conditions['FormsData.created <='] = $to;
            }
        }
        $form       = $this->Forms->getForm($idForm);
        $formFields = json_decode($this->Forms->getForm($idForm)->data);
        $formsData  = $this->getForm($idForm, $conditions);
        foreach ($formsData as $data) {
            $data->values = $this->takeProperValuesForListing($formFields, $data);
        }
        $this->set(compact('formFields', 'formsData', 'form', 'to', 'from'));
    }

    /**
     * List to Csv
     *
     * @description List to CSV (@todo check if needed)
     * @return Response|void
     */
    public function listToCsv()
    {
        $formDataAfter = [];
        $formData      = $this->request->getData();

        $options = [];
        if (!empty($formData['form_id'])) {
            $options['conditions']['Forms.id'] = $formData['form_id'];
        }
        if (!empty($formData['from'])) {
            $options['matching']['FormsData.created >='] = $formData['from'];
        }
        if (!empty($formData['to'])) {
            $options['matching']['FormsData.created <='] = $formData['to'];
        }

        $forms = $this->Forms->getFormsWithData($options);
        foreach ($forms as $form) {
            foreach ($form->forms_data as $data) {
                $formDataAfter[$form->id.$data->id] = [
                    'form_name' => $form->name,
                    'date' => $data->created,
                ];
                $formDataAfter[$form->id.$data->id] = array_merge($formDataAfter[$form->id.$data->id], (array) json_decode($data->data));
            }
        }

        $this->response->withDownload((!empty($formData['form_id']) ? $form->name : 'All_Forms').'_'.'FormsExport.csv');

        $_serialize = 'FormDataAfter';
        $this->set(compact('FormDataAfter', '_serialize'));
        $this->viewBuilder()->setClassName('CsvView.Csv');
    }

    /**
     * View
     *
     * @description View message details
     * @param integer|null $id Row id.
     * @return Response|void
     */
    public function view($id = 0)
    {
        if (!is_numeric($id)) {
            $this->redirect($this->referer());
        }

        $this->setLayout('clean');

        $formsData = $this->FormsData->get($id, [
            'contain' => ['FormDataFiles', 'Forms']
        ]);

        if (!$formsData->view) {
            $formsData->view = true;
            if (!$this->FormsData->save($formsData)) {
                $this->msgError(__('An error occured while marking message as read.'));
            }
        }

        $this->set(compact('formsData'));
    }

    /**
     * Get form
     *
     * @param integer|null $id         Form's id.
     * @param array        $conditions Conditions.
     * @return mixed
     */
    protected function getForm($id, array $conditions)
    {
        return $this->FormsData->find('all', [
                'conditions' => [
                    'FormsData.id_forms' => $id,
                    $conditions
                ],
                'order' => 'FormsData.created DESC',
        ]);
    }

    /**
     * Delete
     *
     * @description Delete message
     * @param integer|null $id Form id.
     * @return Response|void
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $formsData = $this->FormsData->get($id);
        if ($this->FormsData->delete($formsData)) {
            $this->msgSuccess(__('Form message removed.'));
        } else {
            $this->msgError(__('Form message not removed.'));
        }

        return $this->redirect($this->request->referer());
    }

    /**
     * Export Messages
     *
     * @description Export messages from specific form
     * @return void
     */
    public function exportMessages()
    {
        if ($this->request->is('post')) {
            $conditions = [];
            $request    = $this->request->data;
            if (!empty($request['from'])) {
                $conditions['FormsData.created >='] = $request['from'];
            }
            if (!empty($request['to'])) {
                $conditions['FormsData.created <='] = $request['to'];
            }
            if (!empty($request['form_id'])) {
                $form       = $this->Forms->getForm($request['form_id']);
                $formFields = json_decode($this->Forms->getForm($request['form_id'])->data);
                $formsData  = $this->getForm($request['form_id'], $conditions);
            }
            foreach ($formsData as $data) {
                $data->values = $this->takeProperValuesForListing($formFields, $data);
            }
            $filename = $form->name.' '.$request['from'].'-'.$request['to'].'.csv';
            $this->response->download($filename);
            $this->viewBuilder()->layout('ajax');
            $this->set(compact('formFields', 'formsData'));
        }
    }

    /**
     * Export all forms
     *
     * @description Export messages from all forms
     * @return void
     */
    public function exportAllForms()
    {
        if ($this->request->is('post')) {
            $conditions = [];
            $request    = $this->request->data;
            if (!empty($request['from'])) {
                $conditions['FormsData.created >='] = $request['from'];
            }
            if (!empty($request['to'])) {
                $conditions['FormsData.created <='] = $request['to'];
            }
            $forms = $this->Forms->find('all')->select(['id', 'name'])->toArray();
            foreach ($forms as $form) {
                $formFields = json_decode($this->Forms->getForm($form->id)->data);
                $formsData  = $this->getForm($form->id, $conditions)->toArray();
                foreach ($formsData as $data) {
                    $data->values = $this->takeProperValuesForListing($formFields, $data);
                }
                $form->fields = $formFields;
                $form->data   = $formsData;
            }
            $filename = 'All forms '.$request['from'].'-'.$request['to'].'.csv';
            $this->response->download($filename);
            $this->viewBuilder()->layout('ajax');
            $this->set(compact('forms'));
        }
    }

    /**
     * Take proper values for listing
     *
     * @param mixed $formFields Form fields.
     * @param mixed $data       Data.
     * @return array
     */
    protected function takeProperValuesForListing($formFields, $data)
    {
        $array = [];
        foreach ($formFields as $fieldName => $fieldOptions) {
            if ($fieldName !== 'send' && $fieldName !== 'submit') {
                $array[$fieldName] = '';
            }
            foreach (json_decode($data->data) as $dataFieldName => $dataFieldValue) {
                foreach ($fieldOptions as $k => $v) {
                    if ($v === $dataFieldName || $fieldName === $dataFieldName) {
                        $array[$fieldName] = $dataFieldValue;
                    }
                }
            }
        }
        if (empty($array['receive_news'])) {
            $array['receive_news'] = __('Yes');
        }
        return $array;
    }

    /**
     * Recent messages
     *
     * @description Recent messages
     * @return void
     */
    public function recentMessages()
    {
        $messages = $this->paginate($this->FormsData->getFormsData());
        $this->set(compact('messages'));
    }
}
