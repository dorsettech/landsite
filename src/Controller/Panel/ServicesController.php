<?php

namespace App\Controller\Panel;

use App\Model\Entity\Group;
use App\Model\Enum\AdType;
use App\Model\Enum\ClickType;
use App\Model\Enum\CredentialType;
use App\Model\Enum\OpeningDayType;
use App\Model\Enum\ServiceStatus;
use App\Model\Enum\State;
use App\Model\Table\PropertiesTable;
use App\Model\Table\ServicesTable;
use App\Traits\SwitcherTrait;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Response;
use Cake\Mailer\MailerAwareTrait;

/**
 * Services Controller
 *
 * @property ServicesTable $Services
 * @property PropertiesTable $Properties
 */
class ServicesController extends PanelController
{
    use SwitcherTrait;
    use MailerAwareTrait;

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadModel('Properties');

        $this->setHeading('title', __('Professional Services'));
    }

    /**
     * Index method
     *
     * @return Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Categories', 'Users'],
            'order'   => ['Services.created' => 'DESC', 'Services.id' => 'DESC']
        ];

        $services = $this->paginate($this->Services);

        $this->set(compact('services'));
    }

    /**
     * Add method
     *
     * @return Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $service = $this->Services->newEntity();

        if ($this->request->is('post')) {
            $service = $this->Services->patchEntity($service, $this->request->getData(), [
                'associated' => [
                    'Areas', 'Accreditations', 'Associations', 'Products', 'ServiceContacts' => ['validate' => 'services']
                ]
            ]);

            if ($this->Services->save($service)) {
                $this->Flash->success(__('The service has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The service could not be saved. Please, try again.'));
        }

        $categories     = $this->Services->Categories->find('list');
        $users          = $this->Services->Users->getUsersList(['Users.group_id' => Group::MEMBERS]);
        $openingDays    = OpeningDayType::getNamesList();
        $statuses       = ServiceStatus::getNamesList();
        $ads            = AdType::getNamesList();
        $areas          = $this->Services->Areas->find('list', ['order' => ['name' => 'ASC']]);
        $accreditations = $this->Services->Credentials->getListByType(['Credentials.type' => CredentialType::ACC]);
        $associations   = $this->Services->Credentials->getListByType(['Credentials.type' => CredentialType::ASS]);
        $products       = $this->Services->Products->find('list', ['order' => ['name' => 'ASC']]);

        $this->set(compact('service', 'categories', 'users', 'openingDays', 'statuses', 'ads', 'areas', 'accreditations', 'associations', 'products'));
    }

    /**
     * Edit method
     *
     * @param integer|null $id Service id.
     * @return Response|null Redirects on successful edit, renders view otherwise.
     * @throws RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $service = $this->Services->get($id, [
            'contain' => ['Areas', 'Accreditations', 'Associations', 'Products', 'ServiceContacts']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data    = $this->request->getData();

            $openingHours = !empty($service->get('opening_hours')) ? json_decode($service->get('opening_hours'), true) : [];
            $openingHoursFirst = [
                'from' => $data['opening_time_from'],
                'to' => $data['opening_time_to'],
                'days' => $data['opening_days']
            ];
            if (isset($openingHours[0])) {
                $openingHours[0] = $openingHoursFirst;
            } else {
                $openingHours[] = $openingHoursFirst;
            }

            $data['opening_hours'] = json_encode($openingHours);

            $service = $this->Services->patchEntity($service, $data, [
                'associated' => [
                    'Areas', 'Accreditations', 'Associations', 'Products', 'ServiceContacts' => ['validate' => 'services']
                ]
            ]);

            if ($this->Services->save($service)) {
                $this->Flash->success(__('The service has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The service could not be saved. Please, try again.'));
        }

        if ($service->get('opening_hours')) {
            $hours = json_decode($service->get('opening_hours'), true);
            if (isset($hours[0])) {
                $service->set('opening_days', $hours[0]['days']);
                $service->set('opening_time_from', $hours[0]['from']);
                $service->set('opening_time_to', $hours[0]['to']);
            }
        }

        $categories     = $this->Services->Categories->find('list');
        $users          = $this->Services->Users->getUsersList(['Users.group_id' => Group::MEMBERS]);
        $openingDays    = OpeningDayType::getNamesList();
        $statuses       = ServiceStatus::getNamesList();
        $ads            = AdType::getNamesList();
        $areas          = $this->Services->Areas->find('list', ['order' => ['name' => 'ASC']]);
        $accreditations = $this->Services->Credentials->getListByType(['Credentials.type' => CredentialType::ACC]);
        $associations   = $this->Services->Credentials->getListByType(['Credentials.type' => CredentialType::ASS]);
        $products       = $this->Services->Products->find('list', ['order' => ['name' => 'ASC']]);

        $this->set(compact('service', 'categories', 'users', 'openingDays', 'statuses', 'ads', 'areas', 'accreditations', 'associations', 'products'));
    }

    /**
     * Delete method
     *
     * @param integer|null $id Service id.
     * @return Response|null Redirects to index.
     * @throws RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $service = $this->Services->get($id);

        if ($this->Services->delete($service)) {
            $this->Flash->success(__('The service has been deleted.'));
        } else {
            $this->Flash->error(__('The service could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * View
     *
     * @param integer|null $id Service id.
     * @return void Renders view.
     * @throws RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->setLayout('clean');

        $service = $this->Services->getDetails($id);

        $clicks = $this->Services->ServiceClicks->find()->where(['service_id' => $service->id, 'element' => ClickType::WEBSITE_URL]);
        $clicks->select(['count' =>$clicks->func()->count('id')]);

        $clicks2 = $this->Services->ServiceClicks->find()->where(['service_id' => $service->id, 'element' => ClickType::CALL_URL]);
        $clicks2->select(['count' =>$clicks2->func()->count('id')]);

        $websiteClicks = $clicks->first()->count;
        $phoneClicks = $clicks2->first()->count;

        $query = $this->Properties->find();
        $propertiesViews = $this->Properties->query()->select(['s' => $query->func()->sum('views')])->where(['user_id' => $service->get('user_id')])->extract('s')->first();

        $this->setHeading('title', $service->company);
        $this->setHeading('subtitle', __('Professional Service'));
        $this->setHeading('panel', __('{0} Details', $service->company));

        $this->set('service', $service);
        $this->set('websiteClicks', $websiteClicks);
        $this->set('phoneClicks', $phoneClicks);
        $this->set('propertiesViews', $propertiesViews);
    }

    /**
     * Approve or reject
     *
     * @return Response|null Redirects to index.
     */
    public function approveOrReject()
    {
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if (!empty($data['id'])) {
                $service = $this->Services->get($data['id'], ['contain' => ['Users']]);
                $service = $this->Services->patchEntity($service, $data);

                if ($this->Services->save($service)) {
                    if ($service->state === State::APPROVED) {
                        $this->getMailer('Services')->send('serviceApprove', [$service]);
                    } else if ($service->state === State::REJECTED) {
                        $this->getMailer('Services')->send('serviceReject', [$service]);
                    }
                    $this->Flash->success(__('{0} has been updated.', $service->company));
                } else {
                    $this->Flash->error(__('{0} could not be updated. Please, try again.', $service->company));
                }
            }
        }
        return $this->redirect($this->getRequest()->referer());
    }
}
