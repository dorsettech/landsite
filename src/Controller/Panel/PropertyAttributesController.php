<?php

namespace App\Controller\Panel;

use App\Controller\Panel\PanelController;
use App\Model\Table\PropertyAttributesTable;
use App\Traits\SwitcherTrait;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Response;

/**
 * PropertyAttributes Controller
 *
 * @property PropertyAttributesTable $PropertyAttributes
 */
class PropertyAttributesController extends PanelController
{

    use SwitcherTrait;

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
    }

    /**
     * Index method
     *
     * @return Response|void
     */
    public function index()
    {
        $this->paginate     = [
            'contain' => ['PropertyTypes'],
            'order'   => ['PropertyAttributes.position' => 'ASC', 'PropertyAttributes.created' => 'DESC', 'PropertyAttributes.id' => 'DESC']
        ];
        $propertyAttributes = $this->paginate($this->PropertyAttributes);

        $this->set(compact('propertyAttributes'));
    }

    /**
     * Add method
     *
     * @return Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $propertyAttribute = $this->PropertyAttributes->newEntity();

        if ($this->request->is('post')) {
            $propertyAttribute = $this->PropertyAttributes->patchEntity($propertyAttribute, $this->request->getData());

            if ($this->PropertyAttributes->save($propertyAttribute)) {
                $this->Flash->success(__('Property Attribute has been added.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Property Attribute could not be added. Please, try again.'));
        }

        $propertyTypes = $this->PropertyAttributes->PropertyTypes->find('list', ['limit' => 200]);

        $this->set(compact('propertyAttribute', 'propertyTypes'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Property Attribute id.
     * @return Response|null Redirects on successful edit, renders view otherwise.
     * @throws RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $propertyAttribute = $this->PropertyAttributes->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $propertyAttribute = $this->PropertyAttributes->patchEntity($propertyAttribute, $this->request->getData());

            if ($this->PropertyAttributes->save($propertyAttribute)) {
                $this->Flash->success(__('Property Attribute has been updated.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Property Attribute could not be updated. Please, try again.'));
        }

        $propertyTypes = $this->PropertyAttributes->PropertyTypes->find('list', ['limit' => 200]);

        $this->set(compact('propertyAttribute', 'propertyTypes'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Property Attribute id.
     * @return Response|null Redirects to index.
     * @throws RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $propertyAttribute = $this->PropertyAttributes->get($id);

        if ($this->PropertyAttributes->delete($propertyAttribute)) {
            $this->Flash->success(__('Property Attribute has been deleted.'));
        } else {
            $this->Flash->error(__('Property Attribute could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Get by type
     *
     * @param integer|null $typeId Attribute type id.
     * @return void
     */
    public function getByType($typeId = null)
    {
        $this->getRequest()->allowMethod(['ajax', 'get']);

        $status  = AJAX_STATUS_FAIL;
        $message = '';

        $data = [];
        if (is_numeric($typeId)) {
            $data   = $this->PropertyAttributes->getByType($typeId);
            $status = AJAX_STATUS_OK;
        } elseif (empty($typeId)) {
            $status = AJAX_STATUS_OK;
        } else {
            $status  = AJAX_STATUS_ERROR;
            $message = __('An error occured while getting additional options');
        }

        $attributes = [
            'status'  => $status,
            'data'    => $data,
            'message' => $message
        ];

        $this->set('attributes', $attributes);
        $this->set('_serialize', 'attributes');
    }
}
