<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-08)
 * @version 1.0
 */

namespace App\Controller\Panel;

use App\Traits\SwitcherTrait;
use Cake\Http\Response;

/**
 * Languages Controller
 *
 * @description Languages module.
 *
 */
class LanguagesController extends PanelController
{
    use SwitcherTrait;
    
    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
    }

    /**
     * Index method
     *
     * @description Languages list
     * @related toggle, setLanguage
     * @return void
     */
    public function index()
    {
        $languages = $this->paginate($this->Languages->find('all'));
        $this->set(compact('languages'));
        $this->set('_serialize', ['languages']);
    }

    /**
     * Add method
     *
     * @description Create language
     * @return Response|void
     */
    public function add()
    {
        $language = $this->Languages->newEntity();
        if ($this->request->is('post')) {
            $language = $this->Languages->patchEntity($language, $this->request->getData());
            if ($this->Languages->save($language)) {
                $red = $this->request->getData('redirect');
                if (isset($red)) {
                    $this->session->write('lang', $language->locale);
                    $this->redirect(['controller' => 'Reindexing', 'action' => 'reindexI18n']);
                }
                $this->msgSuccess(__('Language added.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->msgError(__('Language not added.'));
            }
        }
        $this->request->withData('redirect', true);
        $this->set(compact('language'));
        $this->set('_serialize', ['language']);
    }

    /**
     * Edit method
     *
     * @description Update language
     * @param string|integer $id Language id.
     *
     * @return Response|void
     */
    public function edit($id = null)
    {
        if (!is_numeric($id)) {
            return $this->redirect(['action' => 'index']);
        }
        $language = $this->Languages->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $language = $this->Languages->patchEntity($language, $this->request->data);
            if ($this->Languages->save($language)) {
                $this->msgSuccess(__('Language saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->msgError(__('Languaged not updated.'));
            }
        }
        $this->set(compact('language'));
        $this->set('_serialize', ['language']);
    }

    /**
     * Delete method
     *
     * @description Delete language
     * @param integer|null $id Language id.
     *
     * @return Response
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $language = $this->Languages->get($id);
        if ($this->Languages->delete($language)) {
            $this->msgSuccess(__('Language removed.'));
        } else {
            $this->msgError(__('Language not removed.'));
        }

        return $this->redirect($this->request->referer());
    }

    /**
     * Toggle boolean value
     *
     * @description Toogle boolean value
     * @param integer|null $id     Row id.
     * @param string|null  $column Column name.
     *
     * @return Response
     */
    public function toggle($id = null, $column = null)
    {
        if (!is_numeric($id) || empty($column)) {
            return $this->redirect(['action' => 'index']);
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $language = $this->Languages->get($id);
            $language->$column = !$language->$column;
            if ($this->Languages->save($language)) {
                if ($language->$column) {
                    $this->msgSuccess(__("Settings {0} for language {1} turned on.", $column, $language->name));
                } else {
                    $this->msgSuccess(__("Settings {0} for language {1} turned off.", $column, $language->name));
                }
            } else {
                $this->msgError(__("Language {0} updated.", $language->namenot));
            }
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Handle lang change
     *
     * @description Set session language
     * @param string|null $lang Language vale.
     *
     * @return Response
     */
    public function setLanguage($lang = '')
    {
        if (isset($lang) && !empty($lang) && $this->Languages->hasLanguage($lang)) {
            $this->session->write('lang', $lang);
            $this->msgSuccess(__('Language updated.'));
        } else {
            $this->msgError(__('Language not updated.'));
        }

        return $this->redirect($this->referer());
    }
}
