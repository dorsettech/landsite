<?php
/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */

namespace App\Controller\Panel;

use App\Traits\SwitcherTrait;
use Cake\Http\Response;

/**
 * ServiceSearchesController
 *
 * @description Service search module
 */
class ServiceSearchesController extends PanelController
{

    use SwitcherTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate  = [
            'contain' => ['Users', 'Categories'],
            'order'   => ['ServiceSearches.created' => 'DESC']
        ];
        $serviceSearches = $this->paginate($this->ServiceSearches);

        $this->set(compact('serviceSearches'));
    }

    /**
     * Delete service search
     *
     * @description Delete service search
     * @param integer|null $id User id.
     * @return Response
     */
    public function delete($id = null)
    {
        if (!is_numeric($id)) {
            return $this->redirect(['controller' => 'Members', 'action' => 'index']);
        }

        $serviceSearch = $this->ServiceSearches->get($id);

        if ($this->getRequest()->is(['post', 'delete'])) {
            if ($this->ServiceSearches->delete($serviceSearch)) {
                $this->msgSuccess(__("Business search #{0} has been removed.", $serviceSearch->id));
            } else {
                $this->msgSuccess(__("Business search #{0} has not been removed.", $serviceSearch->id));
            }
        }

        return $this->redirect($this->getRequest()->referer());
    }
}
