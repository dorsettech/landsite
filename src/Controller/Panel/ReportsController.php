<?php

namespace App\Controller\Panel;

use App\Model\Enum\ReportType;
use App\Model\Table\PaymentsTable;
use App\Model\Table\PropertiesTable;
use App\Model\Table\ServicesTable;
use App\Model\Table\UsersTable;
use Cake\Http\Response;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use ReflectionException;

/**
 * Reports Controller
 *
 * @property PaymentsTable   $Payments
 * @property UsersTable      $Users
 * @property ServicesTable   $Services
 * @property PropertiesTable $Properties
 */
class ReportsController extends PanelController
{

    public function initialize(): void
    {
        parent::initialize();

        $this->loadModel('Payments');
        $this->loadModel('Users');
        $this->loadModel('Services');
        $this->loadModel('Properties');
    }

    /**
     * Index method
     *
     * @return Response|void
     * @throws ReflectionException
     */
    public function index(): void
    {
        $this->heading['subtitle'] = 'Select and generate';
        $this->heading['panel'] = 'Report settings';
        $this->set('_removeAddButton', true);
        $this->set('reports', ReportType::getNamesList());
    }

    public function generate(): Response
    {
        $data = $this->request->getData();

        try {
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $dt = date('Y-m-d H-i-s');
            switch ((int)$data['type']) {
                case ReportType::PAYMENTS_LIST:
                    $sheet->setCellValue('A1', 'Member ID');
                    $sheet->setCellValue('B1', 'User');
                    $sheet->setCellValue('C1', 'Invoice #');
                    $sheet->setCellValue('D1', 'Order #');
                    $sheet->setCellValue('E1', 'Email');
                    $sheet->setCellValue('F1', 'Phone');
                    $sheet->setCellValue('G1', 'Company');
                    $sheet->setCellValue('H1', 'Item');
                    $sheet->setCellValue('I1', 'Amount (excluding VAT)');
                    $sheet->setCellValue('J1', 'VAT');
                    $sheet->setCellValue('K1', 'Total amount');
                    $this->Payments->find('all', [
                        'contain' => ['Users.UserDetails'],
                        'conditions' => [
                            'Payments.created >=' => "{$data['from']} 00:00:00",
                            'Payments.created <=' => "{$data['to']} 23:59:59"
                        ]
                    ])->each(static function ($payment, $row) use (&$sheet) {
                        $row += 2;
                        $sheet->setCellValueByColumnAndRow(1, $row, $payment->get('user_id'));
                        $sheet->setCellValueByColumnAndRow(2, $row, $payment->get('user')->get('full_name'));
                        $sheet->setCellValueByColumnAndRow(3, $row, basename($payment->get('invoice'), '.pdf'));
                        $sheet->setCellValueByColumnAndRow(4, $row, $payment->get('id'));
                        $sheet->setCellValueByColumnAndRow(5, $row, $payment->get('user')->get('email'));
                        $sheet->setCellValueByColumnAndRow(6, $row, $payment->get('user')->get('phone'));
                        $sheet->setCellValueByColumnAndRow(7, $row, $payment->get('user')->get('user_detail')->get('company'));
                        $sheet->setCellValueByColumnAndRow(8, $row, $payment->get('title'));
                        $sheet->setCellValueByColumnAndRow(9, $row, $payment->get('net'));
                        $sheet->setCellValueByColumnAndRow(10, $row, $payment->get('vat'));
                        $sheet->setCellValueByColumnAndRow(11, $row, $payment->get('amount'));
                    });
                    $fileName = "$dt Payments List Report.xlsx";
                    break;
                case ReportType::LIST_OF_ACCOUNT_NAMES_AND_EMAILS:
                    $sheet->setCellValue('A1', 'Member ID');
                    $sheet->setCellValue('B1', 'First Name/Last Name');
                    $sheet->setCellValue('C1', 'Email');
                    $sheet->setCellValue('D1', 'Company');
                    $sheet->setCellValue('E1', 'Contact 1 name');
                    $sheet->setCellValue('F1', 'Contact 1 email');
                    $sheet->setCellValue('G1', 'Contact 2 name');
                    $sheet->setCellValue('H1', 'Contact 2 email');
                    $sheet->setCellValue('I1', 'Contact 3 name');
                    $sheet->setCellValue('J1', 'Contact 3 email');
                    $sheet->setCellValue('K1', 'Contact 4 name');
                    $sheet->setCellValue('L1', 'Contact 4 email');
                    $sheet->setCellValue('M1', 'Contact 5 name');
                    $sheet->setCellValue('N1', 'Contact 5 email');
                    $this->Users->find('all', [
                        'contain' => ['Services.ServiceContacts'],
                        'conditions' => [
                            'Users.created >=' => "{$data['from']} 00:00:00",
                            'Users.created <=' => "{$data['to']} 23:59:59"
                        ]
                    ])->each(static function ($user, $row) use (&$sheet) {
                        $row += 2;
                        $sheet->setCellValueByColumnAndRow(1, $row, $user->get('id'));
                        $sheet->setCellValueByColumnAndRow(2, $row, $user->get('full_name'));
                        $sheet->setCellValueByColumnAndRow(3, $row, $user->get('email'));
                        if ($user->get('service')) {
                            $sheet->setCellValueByColumnAndRow(4, $row, $user->get('service')->get('company'));
                            if ($user->get('service')->get('service_contacts')) {

                                foreach ($user->get('service')->get('service_contacts') as $index => $contact) {
                                    if ($index === 4) {
                                        break;
                                    }
                                    $o = ($index * 2) - $index;
                                    $sheet->setCellValueByColumnAndRow(5 + $index + $o, $row, $contact->get('name'));
                                    $sheet->setCellValueByColumnAndRow(6 + $index + $o, $row, $contact->get('email'));
                                }
                            }
                        }
                    });
                    $fileName = "$dt List of accounts names and emails.xlsx";
                    break;
                case ReportType::LIST_OF_BUSINESS_PROFILES:
                    $sheet->setCellValue('A1', 'Business ID');
                    $sheet->setCellValue('B1', 'Company');
                    $sheet->setCellValue('C1', 'Business Category');
                    $sheet->setCellValue('D1', 'Postcode');
                    $sheet->setCellValue('E1', 'Member ID');
                    $sheet->setCellValue('F1', 'Member Name');
                    $sheet->setCellValue('G1', 'Member email');
                    $sheet->setCellValue('H1', 'Member phone number');
                    $sheet->setCellValue('I1', 'Business ad type');
                    $sheet->setCellValue('J1', 'Date created');
                    $sheet->setCellValue('K1', 'Expiry date');
                    $this->Services->find('all', [
                        'contain' => ['Categories', 'Users'],
                        'conditions' => [
                            'Services.created >=' => "{$data['from']} 00:00:00",
                            'Services.created <=' => "{$data['to']} 23:59:59"
                        ]
                    ])->each(static function ($service, $row) use (&$sheet) {
                        $row += 2;
                        $sheet->setCellValueByColumnAndRow(1, $row, $service->get('id'));
                        $sheet->setCellValueByColumnAndRow(2, $row, $service->get('company'));
                        $sheet->setCellValueByColumnAndRow(3, $row, $service->get('category')->get('name'));
                        $sheet->setCellValueByColumnAndRow(4, $row, $service->get('postcode'));
                        $sheet->setCellValueByColumnAndRow(5, $row, $service->get('user_id'));
                        $sheet->setCellValueByColumnAndRow(6, $row, $service->get('user')->get('full_name'));
                        $sheet->setCellValueByColumnAndRow(7, $row, $service->get('user')->get('email'));
                        $sheet->setCellValueByColumnAndRow(8, $row, $service->get('user')->get('phone'));
                        $sheet->setCellValueByColumnAndRow(9, $row, $service->get('ad_type'));
                        $sheet->setCellValueByColumnAndRow(10, $row, $service->get('created')->format('d.m.Y'));
                        $sheet->setCellValueByColumnAndRow(11, $row, $service->get('ad_expiry_date') ? $service->get('ad_expiry_date')->format('d.m.Y') : '');
                    });
                    $fileName = "$dt List of business profiles.xlsx";
                    break;
                case ReportType::LIST_OF_PROPERTIES:
                    $sheet->setCellValue('A1', 'Property ID');
                    $sheet->setCellValue('B1', 'Company');
                    $sheet->setCellValue('C1', 'Purpose');
                    $sheet->setCellValue('D1', 'Type');
                    $sheet->setCellValue('E1', 'Status');
                    $sheet->setCellValue('F1', 'Member Name');
                    $sheet->setCellValue('G1', 'Member email');
                    $sheet->setCellValue('H1', 'Member phone number');
                    $sheet->setCellValue('I1', 'Property ad type');
                    $sheet->setCellValue('J1', 'Date created');
                    $sheet->setCellValue('K1', 'Expiry date');
                    $this->Properties->find('all', [
                        'contain' => ['PropertyTypes', 'Users.UserDetails'],
                        'conditions' => [
                            'Properties.created >=' => "{$data['from']} 00:00:00",
                            'Properties.created <=' => "{$data['to']} 23:59:59"
                        ]
                    ])->each(static function ($property, $row) use (&$sheet) {
                        $row += 2;
                        $sheet->setCellValueByColumnAndRow(1, $row, $property->get('id'));
                        $sheet->setCellValueByColumnAndRow(2, $row, $property->get('user')->get('user_detail')->get('company'));
                        $sheet->setCellValueByColumnAndRow(3, $row, $property->get('purpose'));
                        $sheet->setCellValueByColumnAndRow(4, $row, $property->get('property_type') ? $property->get('property_type')->get('name') : '');
                        $sheet->setCellValueByColumnAndRow(5, $row, $property->get('status'));
                        $sheet->setCellValueByColumnAndRow(6, $row, $property->get('user')->get('full_name'));
                        $sheet->setCellValueByColumnAndRow(7, $row, $property->get('user')->get('email'));
                        $sheet->setCellValueByColumnAndRow(8, $row, $property->get('user')->get('phone'));
                        $sheet->setCellValueByColumnAndRow(9, $row, $property->get('ad_type'));
                        $sheet->setCellValueByColumnAndRow(10, $row, $property->get('created')->format('d.m.Y'));
                        $sheet->setCellValueByColumnAndRow(11, $row, $property->get('ad_expiry_date') ? $property->get('ad_expiry_date')->format('d.m.Y') : '');
                    });
                    $fileName = "$dt List of properties.xlsx";
                    break;
                default:
                    return $this->result(false, 'Report type not supported');
            }

            $writer = new Xlsx($spreadsheet);
            $writer->save(TMP . $fileName);

            return $this->result(true, 'Report successfully generated. Starting download...', [
                'url' => '/reports/download/' . base64_encode($fileName)
            ]);
        } catch (Exception $e) {
            return $this->result(false, $e->getMessage());
        }
    }

    /**
     * @param bool   $success
     * @param string $msg
     * @param array  $data
     *
     * @return Response
     */
    private function result(bool $success, string $msg = '', $data = []): Response
    {
        return $this->response->withType('application/json')->withStringBody(json_encode([
            'success' => $success,
            'msg' => $msg,
            'data' => $data
        ]));
    }

    public function download($fileName)
    {
        $fileName = base64_decode($fileName);
        $filePath = TMP . $fileName;
        if (!file_exists($filePath)) {
            exit;
        }
        return $this->response->withFile($filePath)->withDownload($fileName)->withLength(filesize($filePath));
    }
}
