<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\Controller\Panel;

/**
 * Cropper Controller
 *
 * @description Cropper module.
 */
class CropperController extends PanelController
{
    /**
     * Session variable
     *
     * @var string
     */
    private $sessionVariable = 'crop.referer';

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->setReferer();
    }

    /**
     * Get referer
     *
     * @return mixed
     */
    protected function getReferer()
    {
        return $this->session->read($this->sessionVariable);
    }

    /**
     * Set referer
     *
     * @return void
     */
    protected function setReferer()
    {
        $referer = $this->getReferer();

        if (!preg_match("#cropper#", $this->request->referer()) && $this->referer() != '/' && $this->referer() != $referer) {
            $referer = $this->referer();
        }

        $this->session->write($this->sessionVariable, $referer);

        $this->set('_referer', $referer);
    }

    /**
     * Index
     *
     * @description Redirect back if no action given
     * @return Response
     */
    public function index()
    {
        return $this->redirect($this->referer());
    }

    /**
     * Crop
     *
     * @description Crop image
     * @param string|null  $model       Model name.
     * @param string|null  $field       Field name.
     * @param integer|null $id          Id.
     * @param string|null  $aspectRatio Flag ratio.
     * @return void
     */
    public function crop($model, $field, $id, $aspectRatio = 'NaN')
    {
        $this->loadModel($model);
        $image = $this->{$model}->get($id);

        if ($this->request->is('post')) {
            $image->{$field} = $this->createFile($image[$field], $this->request->getData('image'));
            $this->{$model}->save($image);
            if ($this->{$model}->save($image)) {
                $this->Flash->success(__('Image has been saved.'));
            } else {
                $this->Flash->error(__('Image could not be saved. Please, try again.'));
            }
        }

        $this->set('path', $image[$field]);
        $this->set('aspectRatio', $aspectRatio);
        $this->set('restoreUrl', ['action' => 'restore', $this->request->pass[0], $this->request->pass[1], $this->request->pass[2]]);
    }

    /**
     * Restore
     *
     * @description Restore original image
     * @param string|null  $model Model name.
     * @param string|null  $field Field name.
     * @param integer|null $id    Id.
     * @return Response|null
     */
    public function restore($model, $field, $id)
    {
        $this->loadModel($model);
        $image = $this->{$model}->get($id);

        $image->{$field} = str_replace('crop_', '', $image->{$field});

        if ($this->{$model}->save($image)) {
            $this->Flash->success(__('Image has been restored.'));
            return $this->redirect(['action' => 'crop', $model, $field, $id]);
        } else {
            $this->Flash->error(__('Image could not be restored. Please, try again.'));
        }
    }

    /**
     * Create file
     *
     * @param string $path        Source file path.
     * @param string $base64Image Base64 encoded image.
     * @return boolean|string     Returns new file path or false if fail.
     */
    protected function createFile(string $path, string $base64Image)
    {
        $exp = explode('/', $path);
        $filename = end($exp);
        array_pop($exp);
        $newPath = implode('/', $exp) . '/crop_' . $filename;

        list($type, $data) = explode(';', $base64Image);
        list(, $data) = explode(',', $base64Image);
        $data = base64_decode($data);

        if (file_put_contents($newPath, $data)) {
            return $newPath;
        }

        return false;
    }

    /**
     * Crop file
     *
     * @description Crop file from path
     * @param string $aspectRatio Aspect ratio to set.
     * @return Response|null
     */
    public function cropFile(string $aspectRatio = 'NaN')
    {
        $path = urldecode(trim($this->getRequest()->getQuery('path'), "/"));

        if ($this->request->is('post')) {
            $croppedFile = $this->createFile($path, $this->request->getData('image'));

            if ($croppedFile) {
                $this->Flash->success(__('New cropped image has been created.'));
                $this->Flash->info(__('Please remember to remove unncessary files.'));

                return $this->redirect(['action' => 'cropFile', 'path' => urlencode($croppedFile)]);
            } else {
                $this->Flash->error(__('Image could not be cropped. Please, try again.'));
            }
        }

        $this->set('path', $path);
        $this->set('aspectRatio', $aspectRatio);
        $this->set('restoreUrl', null);

        $this->render('crop');
    }
}
