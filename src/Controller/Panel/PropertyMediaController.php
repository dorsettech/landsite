<?php

namespace App\Controller\Panel;

use App\Controller\Panel\PanelController;
use App\Model\Table\PropertyMediaTable;
use App\Model\Table\PropertiesTable;
use App\Traits\GetType;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Response;

/**
 * PropertyMedia Controller
 *
 * @property PropertyMediaTable $PropertyMedia
 * @property PropertiesTable $Properties
 */
class PropertyMediaController extends PanelController
{
    use GetType;

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadModel('Properties');
    }

    /**
     * Index method
     *
     * @param integer|null $propertyId Property id.
     * @return Response|void
     * @throws RecordNotFoundException When record not found.
     */
    public function list($propertyId = null): void
    {
        $property = $this->Properties->get($propertyId);

        $this->paginate = [
            'order' => ['position' => 'ASC', 'created' => 'ASC']
        ];
        $propertyMedia = $this->paginate($this->PropertyMedia->getPropertyMedia($property->id));

        $this->setHeading('title', $property->title);
        $this->setHeading('subtitle', __('Media'));
        $this->setHeading('link_close', ['controller' => 'Properties', 'action' => 'index']);

        $this->set(compact('propertyMedia', 'property'));
    }

    /**
     * Add method
     *
     * Ajax only as it's used in Dropzone available on list template.
     *
     * @param integer|null $propertyId Property id.
     * @return void
     * @throws RecordNotFoundException When record not found.
     */
    public function add($propertyId = null): void
    {
        $this->getRequest()->allowMethod(['ajax']);

        $property = $this->Properties->get($propertyId);

        $propertyMedia = $this->PropertyMedia->newEntity();

        if ($this->getRequest()->is('ajax')) {
            $data               = $this->request->getData();
            $data['property_id'] = $property->id;

            if (!empty($data['source']['type'])) {
                Configure::load('website');
                $media        = Configure::read('Media');
                $data['type'] = $this->mediaTypeFromMime($data['source']['type'], $media['mime_types_allowed']);
            }

            $propertyMedia = $this->PropertyMedia->patchEntity($propertyMedia, $data);
            if ($this->PropertyMedia->save($propertyMedia)) {
                $this->Flash->success(__('Property media has been added.'));
            } else {
                $this->setResponse($this->getResponse()->withStatus(406));
                $propertyMedia = $propertyMedia->getErrorsMessages();

                $this->Flash->error(__('Property media could not be added. Please, try again.'));
            }
        }

        $this->set('propertyMedia', $propertyMedia);
        $this->set('_serialize', 'propertyMedia');
    }

    /**
     * Edit method
     *
     * @param integer|null $propertyId Property id.
     * @param integer|null $id         Property Media id.
     * @return Response|null Redirects on successful edit, renders view otherwise.
     * @throws RecordNotFoundException When record not found.
     */
    public function edit($propertyId = null, $id = null)
    {
        $propertyMedia = $this->PropertyMedia->get($id, ['conditions' => ['property_id' => $propertyId], 'contain' => ['Properties']]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $propertyMedia = $this->PropertyMedia->patchEntity($propertyMedia, $this->request->getData());

            if ($this->PropertyMedia->save($propertyMedia)) {
                $this->Flash->success(__('Property media has been updated.'));

                return $this->redirect(['action' => 'list', $propertyId]);
            }
            $this->Flash->error(__('Property media could not be updated. Please, try again.'));
        }

        $this->setHeading('title', $propertyMedia->property->title);
        $this->setHeading('subtitle', __('Media Edit'));
        $this->setHeading('link_close', ['action' => 'list', $propertyId]);

        $this->set(compact('propertyMedia'));
    }

    /**
     * Delete method
     *
     * @param integer|null $propertyId Property id.
     * @param integer|null $id         Property Media id.
     * @return Response|null Redirects to index.
     * @throws RecordNotFoundException When record not found.
     */
    public function delete($propertyId = null, $id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $propertyMedia = $this->PropertyMedia->get($id, ['conditions' => ['property_id' => $propertyId]]);

        if ($this->PropertyMedia->delete($propertyMedia)) {
            $this->Flash->success(__('Property media has been deleted.'));
        } else {
            $this->Flash->error(__('Property media could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'list', $propertyId]);
    }
}
