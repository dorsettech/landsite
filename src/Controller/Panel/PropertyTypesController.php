<?php

namespace App\Controller\Panel;

use App\Controller\Panel\PanelController;
use App\Model\Table\PropertyTypesTable;
use App\Traits\SwitcherTrait;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Response;

/**
 * PropertyTypes Controller
 *
 * @property PropertyTypesTable $PropertyTypes

 */
class PropertyTypesController extends PanelController
{

    use SwitcherTrait;

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
    }

    /**
     * Index method
     *
     * @return Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ParentPropertyTypes'],
            'order' => ['PropertyTypes.position' => 'ASC', 'PropertyTypes.created' => 'DESC', 'PropertyTypes.id' => 'DESC']
        ];
        $propertyTypes  = $this->paginate($this->PropertyTypes);

        $this->set(compact('propertyTypes'));
    }

    /**
     * Add method
     *
     * @return Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $propertyType = $this->PropertyTypes->newEntity();
        
        if ($this->request->is('post')) {
            $propertyType = $this->PropertyTypes->patchEntity($propertyType, $this->request->getData());
            
            if ($this->PropertyTypes->save($propertyType)) {
                $this->Flash->success(__('Property Type has been added.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Property Type could not be saved. Please, try again.'));
        }
        
        $parentPropertyTypes = $this->PropertyTypes->ParentPropertyTypes->find('treeList', ['limit' => 200]);

        $this->set(compact('propertyType', 'parentPropertyTypes'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Property Type id.
     * @return Response|null Redirects on successful edit, renders view otherwise.
     * @throws RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $propertyType = $this->PropertyTypes->get($id);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $propertyType = $this->PropertyTypes->patchEntity($propertyType, $this->request->getData());
            
            if ($this->PropertyTypes->save($propertyType)) {
                $this->Flash->success(__('Property Type has been updated.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Property Type could not be saved. Please, try again.'));
        }

        $parentPropertyTypes = $this->PropertyTypes->ParentPropertyTypes->find('treeList', ['limit' => 200]);
        
        $this->set(compact('propertyType', 'parentPropertyTypes'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Property Type id.
     * @return Response|null Redirects to index.
     * @throws RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $propertyType = $this->PropertyTypes->get($id);

        if ($this->PropertyTypes->delete($propertyType)) {
            $this->Flash->success(__('Property Type has been deleted.'));
        } else {
            $this->Flash->error(__('Property Type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
