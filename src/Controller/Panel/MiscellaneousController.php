<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @author  Bartosz Skrajniak <bartosz.skrajniak@econnect4u.pl>
 * @date (2018-11-13)
 * @version 1.0
 */

namespace App\Controller\Panel;

use Cake\Filesystem\File;

/**
 * Class MiscellaneousController
 *
 * @description Miscellaneous module.
 */
class MiscellaneousController extends PanelController
{
    /**
     * Sitemap
     *
     * @description Sitemap upload
     * @return mixed
     */
    public function sitemap()
    {
        if ($this->request->is('post')) {
            $upload = new \upload($this->request->getData('sitemap'));
            $upload->allowed = ['text/xml', 'application/xml'];
            $upload->no_script = false;
            $upload->file_new_name_body = 'sitemap';
            @unlink(WWW_ROOT . 'sitemap.xml');
            $upload->process(WWW_ROOT);
            if ($upload->processed) {
                $this->Flash->success(__('Sitemap uploaded successfully.'));

                return $this->redirect(['action' => 'sitemap']);
            } else {
                $this->Flash->error(__('Sitemap could not be uploaded: {0}', $upload->error));
            }
        }

        $sitemap = false;
        $file = new File(WWW_ROOT . 'sitemap.xml');
        if ($file->exists()) {
            $sitemap = $file->read();
        }
        $this->set(compact('sitemap'));
    }
}
