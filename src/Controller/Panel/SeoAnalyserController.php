<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2019-01-03)
 * @version 1.0
 */

namespace App\Controller\Panel;

use Cake\Core\Configure;
use Cake\Http\Response;
use Cake\Utility\Inflector;
use Cake\Utility\Text;

/**
 * Seo analyser Controller
 *
 * @description SEO Analyser module.
 */
class SeoAnalyserController extends PanelController
{
    /**
     * Initialize
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();
        Configure::load('website');
        $this->loadModel('Pages');
    }

    /**
     * Index
     *
     * @description SEO Analyser
     * @return void
     */
    public function index(): void
    {
        $this->setLayout('clean');

        $limits = Configure::readOrFail('SeoAnalyzer.limit');
        $pages = $this->paginate($this->Pages->find('all'));

        /* @todo this switch is not softwared, if on system will be added new elements (etc blog, products ... ) this select should be softwared */
        $options = [
            'Pages' => 'Pages'
        ];

        $data = $pages;
        $this->set(compact('data', 'limits', 'options'));
    }

    /**
     * Editable
     *
     * @description Edit specific SEO field (ajax)
     * @return Response
     */
    public function editable(): Response
    {
        $this->autoRender = false;
        $this->setLayout(false);

        $this->request->allowMethod('PUT');
        $data = $this->getRequest()->getData();

        $id = (int)$data['id'];
        $model = Inflector::pluralize($data['model']);
        $field = $data['field'];
        $value = $data['value'];
        if (in_array($field, ['urlname', 'slug'])) {
            $value = Text::slug($value);
        }
        $this->loadModel($model);
        $record = $this->{$model}->get($id);
        $response['data'] = [];
        if ($record) {
            $record->{$field} = $value;
            if ($this->{$model}->save($record)) {
                $response['data'] = $record->seo_analyzer['internal_ranking'];
                $response['value'] = $value;
                $response['field'] = $field;
                $response['status'] = AJAX_STATUS_OK;
            } else {
                $response['status'] = AJAX_STATUS_FAIL;
            }
        } else {
            $response['status'] = AJAX_STATUS_ERROR;
        }

        return $this->response->withType('application/json')
                              ->withStringBody(json_encode($response));
    }

    /**
     * Upload og_image fields (at this moment)
     *
     * @description Upload og_image (ajax)
     * @return Response
     */
    public function upload(): Response
    {
        $this->autoRender = false;
        $this->setLayout(false);

        $this->request->allowMethod('POST');
        $data = $this->getRequest()->getData();

        $id = (int)$data['id'];
        $model = Inflector::pluralize($data['model']);

        $this->loadModel($model);
        $record = $this->{$model}->get($id);
        $response['data'] = [];
        if ($record) {
            $record = $this->{$model}->patchEntity($record, $data);
            if ($this->{$model}->save($record)) {
                $field = $data['field'];
                $response[$field] = $record->{$field};
                $response['data'] = $record->seo_analyzer['internal_ranking'];
                $response['status'] = AJAX_STATUS_OK;
            } else {
                $response['status'] = AJAX_STATUS_FAIL;
            }
        } else {
            $response['status'] = AJAX_STATUS_ERROR;
        }

        return $this->response->withType('application/json')
                              ->withStringBody(json_encode($response));
    }
}
