<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-08)
 * @version 1.0
 */

namespace App\Controller\Panel;

/**
 * Redirections Controller
 *
 * @description Redirections module
 */
class RedirectionsController extends PanelController
{
    /**
     * Index method
     *
     * @description Redirections list
     * @return void
     */
    public function index()
    {
        $redirections = $this->paginate($this->Redirections);
        $this->set(compact('redirections'));
        $this->set('_serialize', ['redirections']);
    }

    /**
     * Add method
     *
     * @description Create redirection
     * @return Response|void
     */
    public function add()
    {
        $redirection = $this->Redirections->newEntity();
        if ($this->request->is('post')) {
            $redirection = $this->Redirections->patchEntity($redirection, $this->request->data);
            if ($this->Redirections->save($redirection)) {
                $this->Flash->success(__('The redirection has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The redirection could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('redirection'));
    }

    /**
     * Edit method
     *
     * @description Update redirection
     * @param integer|null $id Redirection id.
     * @return Response|void
     */
    public function edit($id = null)
    {
        $redirection = $this->Redirections->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $redirection = $this->Redirections->patchEntity($redirection, $this->request->getData());
            if ($this->Redirections->save($redirection)) {
                $this->Flash->success(__('The redirection has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The redirection could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('redirection'));
    }

    /**
     * Delete method
     *
     * @description Delete redirection
     * @param integer|null $id Redirection id.
     * @return Response
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $redirection = $this->Redirections->get($id);
        if ($this->Redirections->delete($redirection)) {
            $this->Flash->success(__('The redirection has been deleted.'));
        } else {
            $this->Flash->error(__('The redirection could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
