<?php

namespace App\Controller\Panel;

use App\Controller\Panel\PanelController;
use App\Model\Table\ProductsTable;
use App\Traits\GetListTrait;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Response;

/**
 * Products Controller
 *
 * @property ProductsTable $Products

 */
class ProductsController extends PanelController
{
    use GetListTrait;

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->setHeading('title', __('Products and Services'));
    }

    /**
     * Index method
     *
     * @return Response|void
     */
    public function index()
    {
        $this->paginate = [
            'order' => ['Products.created' => 'DESC']
        ];
        $products = $this->paginate($this->Products);

        $this->set(compact('products'));
    }

    /**
     * Add method
     *
     * @return Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $product = $this->Products->newEntity();
        
        if ($this->request->is('post')) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            
            if ($this->Products->save($product)) {
                $this->Flash->success(__('Product or Service has been added.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Product or Service could not be added. Please, try again.'));
        }
        
        $this->set(compact('product'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Product id.
     * @return Response|null Redirects on successful edit, renders view otherwise.
     * @throws RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $product = $this->Products->get($id);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            
            if ($this->Products->save($product)) {
                $this->Flash->success(__('Product or Service has been updated.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Product or Service could not be updated. Please, try again.'));
        }
        
        $this->set(compact('product'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Product id.
     * @return Response|null Redirects to index.
     * @throws RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $product = $this->Products->get($id);
        
        if ($this->Products->delete($product)) {
            $this->Flash->success(__('Product or Service has been deleted.'));
        } else {
            $this->Flash->error(__('Product or Service could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
