<?php

namespace App\Controller\Members;

use App\Mime;
use App\Model\Entity\Service;
use App\Model\Entity\ServiceMedia;
use App\Model\Enum\ClickType;
use App\Model\Enum\MediaType;
use App\Model\Enum\ServiceStatus;
use App\Model\Enum\State;
use App\Model\Table\AreasTable;
use App\Model\Table\CategoriesTable;
use App\Model\Table\CredentialsTable;
use App\Model\Table\DiscountsTable;
use App\Model\Table\DiscountUsesTable;
use App\Model\Table\ProductsTable;
use App\Model\Table\ServiceClicksTable;
use App\Model\Table\ServiceEnquiriesTable;
use App\Model\Table\ServiceMediaTable;
use App\Model\Table\ServicesTable;
use App\Model\Table\ServiceViewsTable;
use App\Model\Table\UserDetailsTable;
use App\Model\Table\UserSavedTable;
use App\RequestType;
use App\Settings;
use App\Traits\DiscountValidator;
use Cake\Core\Configure;
use Cake\Http\Response;
use Cake\I18n\Time;
use Cake\ORM\Query;
use DateInterval;
use DatePeriod;
use DateTime;

/**
 * Services Controller
 *
 * @property ServicesTable         $Services
 * @property CategoriesTable       $Categories
 * @property ProductsTable         $Products
 * @property AreasTable            $Areas
 * @property CredentialsTable      $Credentials
 * @property UserDetailsTable      $UserDetails
 * @property ServiceMediaTable     $ServiceMedia
 * @property UserSavedTable        $UserSaved
 * @property ServiceEnquiriesTable $ServiceEnquiries
 * @property ServiceViewsTable     $ServiceViews
 * @property ServiceClicksTable    $ServiceClicks
 * @property DiscountsTable        $Discounts
 * @property DiscountUsesTable     $DiscountUses
 * @package App\Controller\Members
 */
class ServicesController extends PaymentController
{
    use DiscountValidator;

    private $tempFilePrefix;
    private $maxFileSize;
    private $mimeAllowed;

    /**
     * @return void
     * @throws \Exception General exception.
     */
    public function initialize(): void
    {
        parent::initialize();

        Configure::load('website');

        $this->tempFilePrefix = Configure::read('Media.upload_tmp_prefix');
        $this->maxFileSize = Configure::read('Media.max_image_size');
        $this->mimeAllowed = Configure::read('Media.mime_types_allowed.image');

        $this->loadModel('UserDetails');
        $this->loadModel('ServiceMedia');
        $this->loadModel('Categories');
        $this->loadModel('Products');
        $this->loadModel('Areas');
        $this->loadModel('Credentials');
        $this->loadModel('UserSaved');
        $this->loadModel('ServiceEnquiries');
        $this->loadModel('ServiceViews');
        $this->loadModel('ServiceClicks');
        $this->loadModel('Discounts');
        $this->loadModel('DiscountUses');
    }

    /**
     * @return mixed
     * @throws \Exception General exception.
     */
    public function latest(): Response
    {
        $this->allowMethod(RequestType::GET);

        $userId = $this->Auth->user('id');

        return $this->asListResponse($this->Services->find('all', [
            'fields' => [
                'id', 'uid', 'company', 'description_short', 'image' => 'Users.image', 'user_uid' => 'Users.uid', 'slug'
            ],
            'contain' => [
                'Users'
            ],
            'conditions' => [
                'status' => ServiceStatus::PUBLISHED,
                'user_id !=' => $userId
            ],
            'order' => [
                'publish_date' => 'DESC'
            ],
            'limit' => 6
        ]));
    }

    /**
     * @return Response
     * @throws \Exception General exception.
     */
    public function business(): Response
    {
        $this->allowMethod(RequestType::POST);

        $user = $this->Auth->getUser();

        $service = $this->Services->find('all', [
            'conditions' => [
                'user_id' => $user->id
            ],
            'contain' => [
                'ServiceProducts.Products',
                'ServiceAreas.Areas',
                'ServiceMedia',
                'ServiceContacts',
                'ServiceCredentials.Credentials'
            ]
        ])->first();

        if ($service === null) {
            $details = $this->UserDetails->find()->where(['user_id' => $user->id])->first();
            $service = $this->Services->newEntity(array_merge(['user_id' => $user->id], $details ? [
                'company' => $details->get('company'),
                'location' => $details->get('location'),
                'postcode' => $details->get('postcode')
            ] : []));
        }

        $categories = $this->Categories->find('all', [
            'fields' => [
                'id', 'name', 'description', 'image', 'is_recommended'
            ],
            'order' => [
                'position' => 'ASC',
                'name' => 'ASC'
            ]
        ]);

        $products = $this->Products->find('all', [
            'fields' => [
                'id', 'name', 'description'
            ],
            'order' => [
                'name' => 'ASC'
            ]
        ]);

        $areas = $this->Areas->find('all', [
            'fields' => [
                'id', 'name'
            ],
            'order' => [
                'name' => 'ASC'
            ]
        ]);

        $credentials = $this->Credentials->find('all', [
            'fields' => [
                'id', 'name', 'type'
            ],
            'order' => [
                'name' => 'ASC'
            ]
        ]);

        return $this->asJsonResponse([
            'service' => $service->toArray(),
            'categories' => $categories,
            'products' => $products,
            'areas' => $areas,
            'credentials' => $credentials
        ]);
    }

    /**
     * Save draft
     *
     * @return Response
     */
    public function draft(): Response
    {
        $data = $this->Data->all();

        $this->unsetSensitiveData($data);

        if ($this->Services->ServiceMedia->hasBehavior('Upload')) {
            $this->Services->ServiceMedia->removeBehavior('Upload');
        }

        $data['user_id'] = $this->Auth->user('id');

        $entity = $this->Services->patchEntity(empty($data['id']) ?
            $this->Services->newEntity() :
            $this->Services->get($data['id'], [
                'contain' => [
                    'ServiceProducts.Products',
                    'ServiceAreas.Areas',
                    'ServiceMedia',
                    'ServiceContacts',
                    'ServiceCredentials.Credentials'
                ]
            ]), $data);

        $result = $this->Services->save($entity, ['associated' => [
            'ServiceProducts',
            'ServiceAreas',
            'ServiceMedia',
            'ServiceContacts',
            'ServiceCredentials'
        ]]);

        if ($result) {
            $this->renameTemporaryFiles($entity);
        }

        $medias = $this->ServiceMedia->find()->where([
            'service_id' => $entity->id
        ]);

        return $this->asMessageResponse($result, $result ? __('Draft successfully saved') : $entity->getErrorsMessages(), [
            'media' => $medias,
            'id' => $entity->id
        ]);
    }

    /**
     * @param array $data Request data.
     *
     * @return void
     */
    private function unsetSensitiveData(array &$data): void
    {
        unset(
            $data['ad_basket'],
            $data['ad_cost'],
            $data['ad_expiry_date'],
            $data['ad_extend'],
            $data['ad_payment'],
            $data['ad_total_cost'],
            $data['ad_type'],
            $data['ad_plan_type'],
            $data['deleted'],
            $data['publish_date'],
            $data['status'],
            $data['visibility']
        );
    }

    /**
     * Rename temporary file names (to keep tidiness)
     *
     * @param Service $entity Service entity.
     *
     * @return void
     */
    private function renameTemporaryFiles(Service $entity): void
    {
        $user = $this->Auth->getUser();
        $filePath = MEMBERS_SERVICE_FILES . $user->get('uid') . DS;
        $this->ServiceMedia->find()->where([
            'service_id' => $entity->id,
            'type' => MediaType::IMAGE
        ])->each(function (ServiceMedia $media) use ($filePath) {
            if (strpos($media->get('source'), $this->tempFilePrefix) !== 0) {
                return;
            }
            $destFileName = str_replace($this->tempFilePrefix, '', $media->get('source'));
            if (rename($filePath . $media->get('source'), $filePath . $destFileName)) {
                $media->set('source', $destFileName);
                $this->ServiceMedia->save($media);
            }
        });
    }

    /**
     * @return mixed
     * @throws \Exception General exception.
     */
    public function stats()
    {
        $this->allowMethod(RequestType::GET);

        $user = $this->Auth->getUser();

        $service = $this->Services->find('all', [
            'conditions' => [
                'user_id' => $user->id
            ]
        ])->first();

        if (!$service) {
            return $this->asJsonResponse();
        }

        $oneMonthIntervalInverted = new DateInterval('P1M');
        $dateTimeStart = new DateTime();
        $dateTimeStart->modify('first day of this month');
        $dateTimeStart->setTime(0, 0, 0);
        $dateTimeStart->sub(new DateInterval('P11M'));
        $datePeriod = new DatePeriod($dateTimeStart, $oneMonthIntervalInverted, 11);
        $stats = [];
        foreach ($datePeriod as $key => $item) {
            $label = $item->format('M, y');
            if (!isset($stats[$key])) {
                $stats[$key] = [
                    'label' => $label,
                    'views' => 0,
                    'enquiries' => 0,
                    'clicks' => 0,
                    'calls' => 0
                ];
            }

            $stats[$key]['views'] = (int)$this->ServiceViews->query()->select([
                'value' => 'COUNT(*)'
            ])->where([
                'service_id' => $service->id,
                'YEAR(created)' => $item->format('Y'),
                'MONTH(created)' => $item->format('n')
            ])->group('DATE_FORMAT(created,"%Y-%m")')->extract('value')->first();

            $stats[$key]['enquiries'] = (int)$this->ServiceEnquiries->query()->select([
                'value' => 'COUNT(*)'
            ])->where([
                'service_id' => $service->id,
                'YEAR(created)' => $item->format('Y'),
                'MONTH(created)' => $item->format('n')
            ])->group('DATE_FORMAT(created,"%Y-%m")')->extract('value')->first();

            $stats[$key]['clicks'] = (int)$this->ServiceClicks->query()->select([
                'value' => 'COUNT(*)'
            ])->where([
                'service_id' => $service->id,
                'element' => ClickType::WEBSITE_URL,
                'YEAR(created)' => $item->format('Y'),
                'MONTH(created)' => $item->format('n')
            ])->group('DATE_FORMAT(created,"%Y-%m")')->extract('value')->first();

            $stats[$key]['calls'] = (int)$this->ServiceClicks->query()->select([
                'value' => 'COUNT(*)'
            ])->where([
                'service_id' => $service->id,
                'element' => ClickType::CALL_URL,
                'YEAR(created)' => $item->format('Y'),
                'MONTH(created)' => $item->format('n')
            ])->group('DATE_FORMAT(created,"%Y-%m")')->extract('value')->first();
        }

        return $this->asJsonResponse($stats);
    }

    /**
     * Submit/Update service
     *
     * @return Response
     * @throws \Exception General exception.
     */
    public function update(): Response
    {
        $data = $this->Data->all();
        $user = $this->Auth->getUser();
        $userId = $user->id;

        if (!isset($data['ad_extend'])) {
            $data['ad_extend'] = false;
        }

        unset($data['publish_date']);
        $data['status'] = ServiceStatus::PUBLISHED;
        $data['state'] = State::PENDING;
        $data['user_id'] = $userId;

        if ($this->Services->ServiceMedia->hasBehavior('Upload')) {
            $this->Services->ServiceMedia->removeBehavior('Upload');
        }

        $validity = Settings::getGroup('validity');

        $entity = $this->Services->get($data['id'], [
            'contain' => [
                'ServiceProducts.Products',
                'ServiceAreas.Areas',
                'ServiceMedia',
                'ServiceContacts',
                'ServiceCredentials.Credentials'
            ]
        ]);

        if ($entity->get('publish_date') === null) {
            $data['publish_date'] = new Time();
        }

        $adType = strtolower($data['ad_type']);
        $adPlanType = strtolower($data['ad_plan_type']);

        if ($entity->get('visibility')) {
            if ($entity->get('ad_expiry_date') === null || (
                    $entity->get('status') === ServiceStatus::EXPIRED && !empty($data['ad_type']) && ($data['ad_cost'] > 0 || !empty($data['ad_discount']))
            )) {
                $expiryDateTime = new Time();
                $expiryDateTime->add(new \DateInterval("P{$validity[$adPlanType . '-' . $adType . '-days']}D"));
                $data['ad_expiry_date'] = $expiryDateTime;
            } elseif ($data['ad_extend']) {
                $expiryDateTime = new Time($entity->get('ad_expiry_date'));
                $expiryDateTime->add(new \DateInterval("P{$validity[strtolower($entity->get('ad_plan_type')) . '-' . strtolower($entity->get('ad_type')) . '-days']}D"));
                $data['ad_expiry_date'] = $expiryDateTime;
            } else {
                unset($data['ad_expiry_date']);
            }
        } else {
            if ($data['ad_expiry_date'] === null && !empty($data['ad_type'])) {
                $expiryDateTime = new Time();
                $expiryDateTime->add(new \DateInterval("P{$validity[$adPlanType . '-' . $adType . '-days']}D"));
                $data['ad_expiry_date'] = $expiryDateTime;
            } else {
                unset($data['ad_expiry_date']);
            }
        }

        $entity = $this->Services->patchEntity($entity, $data);

        $hasToPay = $data['ad_cost'] > 0 && !empty($data['ad_basket']) && !empty($data['ad_payment']);

        // temporary disable item visibility until payment will be done or change the state
        $entity->set('visibility', !$hasToPay);

        $adBasket = null;
        if (!empty($data['ad_basket'])) {
            if (is_array($data['ad_basket'])) {
                $adBasket = json_encode($data['ad_basket']);
            } else {
                $adBasket = $data['ad_basket'];
            }
        }
        $entity->set('ad_basket', $adBasket);

        $result = $this->Services->save($entity, ['associated' => [
            'ServiceProducts',
            'ServiceAreas',
            'ServiceMedia',
            'ServiceContacts',
            'ServiceCredentials'
        ]]);

        $medias = $this->ServiceMedia->find()->where([
            'service_id' => $entity->id
        ])->order(['position' => 'ASC']);

        if ($result) {
            $this->renameTemporaryFiles($entity);
            $this->Services->dispatchEvent('Dashboard.update', [$userId]);

            $discount = null;
            if (!empty($data['ad_discount_code'])) {
                $discount = $this->checkCodeValidity($data['ad_discount_code'], $userId);
                if (!$discount['success']) {
                    return $this->asMessageResponse(false, $discount['msg'], [
                        'media' => $medias
                    ]);
                }
            }

            $details = $this->UserDetails->find('all', [
                'conditions' => [
                    'user_id' => $userId
                ]
            ])->first();

            // update user details company name
            if ($details) {
                $details->set('company', $entity->get('company'));
                $this->UserDetails->save($details);
            }

            if (!$hasToPay) {
                if ($discount) {
                    $this->DiscountUses->save($this->DiscountUses->newEntity([
                        'discount_id' => $discount['discount']->get('id'),
                        'user_id' => $userId
                    ]));
                }
                return $this->asMessageResponse(true, __('Your changes have been saved'), [
                    'media' => $medias
                ]);
            }

            try {
                if ($discount) {
                    $discount = $discount['discount'];
                    $discount->set('price', $data['ad_discount']['price']);
                }

                $paid = $this->charge($data['ad_payment']['type'], $user, $entity, $discount, $data['ad_vat']);

                if ($paid['result'] === true) {
                    $entity->set('visibility', true);
                    $entity->set('ad_basket', null);
                    $entity->set('ad_total_cost', $entity->get('ad_total_cost') + $entity->get('ad_cost'));
                    $this->Services->save($entity);
                }

                return $this->asMessageResponse($paid['result'], $paid['message'], [
                    'media' => $medias
                ]);
            } catch (\BadMethodCallException $exception) {
                return $this->asMessageResponse(false, $exception->getMessage());
            }
        }

        return $this->asMessageResponse(false, $entity->getErrorsMessages(), [
            'media' => $medias
        ]);
    }

    /**
     * @return mixed
     * @throws \Exception General exception.
     */
    public function images()
    {
        $this->allowMethod(RequestType::POST);

        $files = $this->Data->all();

        $results = [];
        foreach ($files as $file) {
            $results[] = $this->processUpload($file);
        }

        $result = array_filter($results, function ($item) {
            return $item['success'] === true;
        });

        if (count($result) === 0) {
            $overall = false;
        } else {
            $overall = count($result) === count($files);
        }

        return $this->asMessageResponse($overall, $overall ? __('File(s) uploaded successfully.') : __('Upload failed on one or more files.'), ['results' => $results]);
    }

    /**
     * @param array $image Image array.
     *
     * @return array|null
     */
    protected function processUpload(array $image): ?array
    {
        $user = $this->Auth->getUser();

        if ($image['size'] > $this->maxFileSize) {
            return ['success' => false, 'msg' => __('Maximum allowed size is {0} bytes', $this->maxFileSize), 'image' => $image['name']];
        }

        if (!in_array($image['type'], $this->mimeAllowed, true)) {
            return ['success' => false, 'msg' => __('File type not allowed. Acceptable types: JPEG, PNG, WEBP'), 'image' => $image['name']];
        }

        $fileName = str_replace('.', '', uniqid($this->tempFilePrefix . $user->get('id') . '-', true)) . '.' . Mime::mimeToExtension($image['type']);
        $fileDir = MEMBERS_SERVICE_FILES . $user->get('uid') . DS;
        $filePath = $fileDir . $fileName;

        if (!file_exists($fileDir) && !mkdir($fileDir, 0777, true) && !is_dir($fileDir)) {
            return ['success' => false, 'msg' => sprintf('Directory "%s" was not created', $fileDir), 'image' => $image['name']];
        }

        $result = false;
        if (is_uploaded_file($image['tmp_name'])) {
            $result = move_uploaded_file($image['tmp_name'], $filePath);
        }

        if ($result) {
            return ['success' => true, 'image' => $image['name'], 'filename' => $fileName];
        } else {
            return ['success' => false, 'msg' => __('An unexpected error occurred.'), 'image' => $image['name']];
        }
    }

    /**
     * @return mixed
     * @throws \Exception General exception.
     */
    public function saved()
    {
        $this->allowMethod(RequestType::GET);

        $userId = $this->Auth->user('id');
        $page = $this->Data->asInt('page');
        $perPage = $this->Data->asInt('per_page');

        $conditions = [
            'UserSaved.user_id' => $userId,
            'service_id IS NOT' => null,
            'property_id IS' => null
        ];

        $total = $this->UserSaved->find('all', [
            'conditions' => $conditions
        ])->count();

        $order = [
            'UserSaved.created' => 'DESC'
        ];

        $records = $this->UserSaved->find('all', [
            'conditions' => $conditions,
            'order' => $order,
            'contain' => [
                'Services.Categories' => static function (Query $q) {
                    return $q->select(['name']);
                },
                'Services.Users' => static function (Query $q) {
                    return $q->select(['uid', 'image']);
                }
            ]
        ])->page($page, $perPage);

        return $this->asJsonResponse([
            'records' => $records,
            'pagination' => [
                'total' => $total,
                'per_page' => $perPage,
                'current_page' => $page,
                'last_page' => ceil($total / $perPage),
                'from' => ($page - 1) * $perPage,
                'to' => $page * $perPage
            ]
        ]);
    }

    /**
     * @return Response
     * @throws \Exception General exception.
     */
    public function deleteSaved(): Response
    {
        $this->allowMethod(RequestType::DELETE);

        $user = $this->Auth->getUser();
        $id = $this->Data->asInt('id');

        $entity = $this->UserSaved->find('all', [
            'conditions' => [
                'id' => $id,
                'user_id' => $user->id
            ]
        ])->first();

        if (!$entity) {
            return $this->asMessageResponse(false, __('Record does not exists'));
        }

        $result = $this->UserSaved->delete($entity);

        return $this->asMessageResponse($result, $result ? __('Record successfully deleted') : $entity->getErrorsMessages());
    }

    /**
     * @return mixed
     * @throws \Exception General exception.
     */
    public function enquiries()
    {
        $this->allowMethod(RequestType::GET);

        $userId = $this->Auth->user('id');
        $id = $this->Data->asInt('id');
        $sort = $this->Data->get('sort');
        $page = $this->Data->asInt('page');
        $perPage = $this->Data->asInt('per_page');

        if (!empty($id)) {
            $service = $this->Services->get($id);

            if ($userId !== $service->get('user_id')) {
                return $this->asJsonResponse();
            }

            $conditions = [
                'service_id' => $id
            ];
        } else {
            $conditions = [
                'Services.user_id' => $userId
            ];
        }

        $total = $this->ServiceEnquiries->find('all', [
            'contain' => [
                'Services', 'Users'
            ],
            'conditions' => $conditions
        ])->count();

        $order = [];

        if (!empty($sort)) {
            [$f, $o] = explode('|', $sort);
            $order = [
                $f => $o
            ];
        }

        $records = $this->ServiceEnquiries->find('all', [
            'fields' => [
                'id', 'ServiceEnquiries.created', 'full_name', 'message', 'phone', 'enquiry_title' => 'Services.company', 'email' => 'Users.email'
            ],
            'contain' => [
                'Services', 'Users'
            ],
            'conditions' => $conditions,
            'order' => $order
        ])->page($page, $perPage);

        return $this->asJsonResponse([
            'records' => $records,
            'pagination' => [
                'total' => $total,
                'per_page' => $perPage,
                'current_page' => $page,
                'last_page' => ceil($total / $perPage),
                'from' => ($page - 1) * $perPage,
                'to' => $page * $perPage
            ]
        ]);
    }
}
