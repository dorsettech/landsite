<?php

namespace App\Controller\Members;

use App\Model\Table\CategoriesTable;
use App\RequestType;
use App\Traits\JsonResponse;
use Cake\Http\Response;

/**
 * Categories Controller
 *
 * @property CategoriesTable $Categories
 * @package App\Controller\Members
 */
class CategoriesController extends AuthController
{
    use JsonResponse;

    public function initialize(): void
    {
        parent::initialize();

        $this->Auth->allow(['index']);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function index(): Response
    {
        $this->allowMethod(RequestType::GET);

        return $this->asListResponse($this->Categories->find('all', [
            'fields' => [
                'id', 'name', 'description', 'image', 'is_recommended'
            ],
            'order' => [
                'position' => 'ASC',
                'name' => 'ASC'
            ]
        ]));
    }
}
