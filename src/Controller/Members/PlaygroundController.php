<?php

namespace App\Controller\Members;


use App\Traits\PlainResponse;

/**
 * DashboardController
 *
 * @description Dashboard module
 */
class PlaygroundController extends AppController
{

    use PlainResponse;

    public function index(): void
    {
        echo uniqid('', false);
        exit;
    }


}
