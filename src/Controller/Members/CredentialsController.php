<?php

namespace App\Controller\Members;

use App\Model\Table\CredentialsTable;
use App\RequestType;
use App\Traits\JsonResponse;
use Cake\Http\Response;

/**
 * Credentials Controller
 *
 * @property CredentialsTable $Credentials
 * @package App\Controller\Members
 */
class CredentialsController extends AuthController
{
    use JsonResponse;

    public function initialize(): void
    {
        parent::initialize();

        $this->Auth->allow(['index']);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function index(): Response
    {
        $this->allowMethod(RequestType::GET);

        return $this->asListResponse($this->Credentials->find('all', [
            'fields' => [
                'id', 'name', 'type'
            ],
            'order' => [
                'name' => 'ASC'
            ]
        ]));
    }
}
