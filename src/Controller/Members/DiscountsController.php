<?php

namespace App\Controller\Members;

use App\Model\Table\DiscountsTable;
use App\Model\Table\DiscountUsesTable;
use App\RequestType;
use App\Traits\DiscountValidator;
use App\Traits\JsonResponse;

/**
 * Discounts Controller
 *
 * @property DiscountsTable    $Discounts
 * @property DiscountUsesTable $DiscountUses
 * @package App\Controller\Members
 */
class DiscountsController extends AuthController
{
    use JsonResponse, DiscountValidator;

    /**
     * @return void
     * @throws \Exception Inherited.
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadModel('Discounts');
        $this->loadModel('DiscountUses');
    }

    /**
     * @return mixed
     * @throws \Exception Inherited.
     */
    public function verify()
    {
        $this->allowMethod(RequestType::GET);

        $user = $this->Auth->getUser();
        $code = $this->Data->get('code');

        $result = $this->checkCodeValidity($code, $user->get('id'));

        if ($result['success']) {
            return $this->asMessageResponse(true, $result['msg'], ['discount' => $result['discount']]);
        } else {
            return $this->asMessageResponse(false, $result['msg']);
        }
    }
}
