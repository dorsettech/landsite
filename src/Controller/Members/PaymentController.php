<?php

namespace App\Controller\Members;

use App\Mailer\Members\PaymentMailer;
use App\Model\Entity\Discount;
use App\Model\Entity\ExtendedEntity;
use App\Model\Entity\Property;
use App\Model\Entity\Service;
use App\Model\Entity\User;
use App\Model\Enum\PaymentMethod;
use App\Model\Enum\PaymentType;
use App\Model\Table\DiscountsTable;
use App\Model\Table\DiscountUsesTable;
use App\Model\Table\PaymentsTable;
use App\Model\Table\UserDetailsTable;
use App\RequestType;
use App\ResponseType;
use App\Settings;
use App\Traits\JsonResponse;
use BadMethodCallException;
use Cake\Event\Event;
use Cake\Http\Response;
use Cake\Mailer\Mailer;
use Cake\Mailer\MailerAwareTrait;
use Exception;
use Stripe\ApiResource;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Error\SignatureVerification;
use Stripe\Stripe;
use Stripe\StripeObject;
use Stripe\Webhook;
use UnexpectedValueException;

/**
 * Payment Controller
 *
 * @property UserDetailsTable  $UserDetails
 * @property PaymentsTable     $Payments
 * @property DiscountsTable    $Discounts
 * @property DiscountUsesTable $DiscountUses
 * @package App\Controller\Members
 */
class PaymentController extends AuthController
{
    use JsonResponse;
    use MailerAwareTrait;

    /**
     * @var array Stripe configuration
     */
    private $stripe;

    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @var string Last payment error
     */
    private $lastError;

    /**
     * @return void
     * @throws Exception Inherited.
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->stripe = Settings::getGroup('stripe');
        Stripe::setApiKey($this->stripe['secret-key']);

        $this->loadModel('UserDetails');
        $this->loadModel('Payments');
        $this->loadModel('Discounts');
        $this->loadModel('DiscountUses');

        $this->mailer = $this->getMailer(PaymentMailer::class);

        $this->Auth->allow(['webhook', 'invoice']);
    }

    /**
     * Disable CSRF for stripe web hooks
     *
     * @param Event $event Event.
     *
     * @return Response|null
     */
    public function beforeFilter(Event $event): ?Response
    {
        if ($this->request->action === 'webhook') {
            if (isset($this->Security)) {
                $this->getEventManager()->off($this->Security);
            }
            if (isset($this->Csrf)) {
                $this->getEventManager()->off($this->Csrf);
            }
        }

        return parent::beforeFilter($event);
    }

    /**
     * Webhook handling for Stripe Payments API.
     *
     * @link https://stripe.com/docs/webhooks
     * @link https://stripe.com/docs/api#event_types
     * @return Response
     */
    public function webhook(): Response
    {
        $payload = $this->Data->input();
        $signature = env('HTTP_STRIPE_SIGNATURE');

        $event = null;

        try {
            $event = Webhook::constructEvent($payload, $signature, $this->stripe['secret-webhook']);
        } catch (UnexpectedValueException $e) {
            return $this->response->withStatus(400, 'Invalid payload');
        } catch (SignatureVerification $e) {
            return $this->response->withStatus(400, 'Invalid signature');
        }

        if (!$event || !$event->id) {
            return $this->response->withStatus(400);
        }

        /*
         * @todo Handle the event here
         */

        return $this->response->withStatus(200);
    }

    /**
     * @param string $thumb Invoice thumb.
     *
     * @return Response
     * @throws Exception Inherited.
     */
    public function invoice(string $thumb): ?Response
    {
        $this->allowMethod(RequestType::GET);
        $this->setResponseType(ResponseType::NONE);

        $payment = $this->Payments->query()->where(['MD5(invoice) =' => $thumb])->first();
        if (!$payment || empty($payment->get('invoice'))) {
            return $this->response->withStringBody('Invoice does not exists')->withType('text/plain');
        }

        return $this->response->withFile($payment->get('invoice_path'));
    }

    /**
     * @param string         $method   Payment method.
     * @param User           $user     User entity.
     * @param ExtendedEntity $entity   Related payment object entity.
     *
     * @param Discount|null  $discount Discount entity with price value set.
     *
     * @param float          $vat VAT value (payment vat)
     *
     * @return array
     */
    protected function charge(string $method, User $user, ExtendedEntity $entity, Discount $discount = null, float $vat = 0): ?array
    {
        switch ($method) {
            case PaymentMethod::STRIPE:
                $result = $this->stripeCharge($user, $entity, $discount, $vat);
                break;
            case PaymentMethod::PURCHASE_ORDER_NUMBER:
                $result = $this->purchaseOrderCharge($user, $entity, $discount, $vat);
                break;
            default:
                throw new BadMethodCallException(__('This payment method is not supported'));
        }

        if (!$result['result']) {
            $this->mailer->send('fail', [$user]);
        }

        return $result;
    }

    /**
     * Charge an user by Stripe gateway
     *
     * @param User           $user     User entity.
     * @param ExtendedEntity $entity   Related payment object entity.
     * @param Discount|null  $discount Discount entity with price value set.
     *
     * @param float          $vat VAT value (payment vat)
     *
     * @return array
     */
    protected function stripeCharge(User $user, ExtendedEntity $entity, Discount $discount = null, float $vat = 0): array
    {
        $payment = $entity->get('ad_payment');
        $customer = $this->stripeCustomerFindAndUpdateOrCreate($user, $payment);

        if ($customer === false) {
            return ['result' => false, 'message' => $this->lastError];
        }

        $paymentTitle = $this->getPaymentTitle($entity);

        if (!$paymentTitle['result']) {
            return $paymentTitle;
        }

        $vatRate = Settings::get('prices.vat');

        $charge = Charge::create([
            'amount' => $entity->get('ad_cost') * 100,
            'currency' => $this->stripe['currency'],
            'description' => $paymentTitle['title'],
            'customer' => $customer['id'],
            'metadata' => [
                'id' => $entity->get('id'),
                'entity' => $paymentTitle['object'],
                'expiry' => $entity->get('ad_expiry_date')->getTimestamp(),
                'plan' => $entity->get('ad_type'),
                'user_id' => $entity->get('user_id'),
                'basket' => $entity->get('ad_basket'),
                'vat_rate' => $vatRate
            ]
        ]);

        if ($charge['paid']) {
            $this->Payments->save($this->Payments->newEntity([
                'user_id' => $entity->get('user_id'),
                'title' => $paymentTitle['title'],
                'amount' => $entity->get('ad_cost'),
                'amount_pos' => $entity->get('ad_cost') * 100,
                'vat' => $vat,
                'vat_rate' => $vatRate,
                'currency' => $charge['currency'],
                'type' => $paymentTitle['object'],
                'token_id' => $charge['source']['id'],
                'charge_id' => $charge['id'],
                'object_id' => $entity->get('id'),
                'payload' => json_encode($charge),
                'meta' => json_encode($entity->get('ad_payment')),
                'discount_id' => $discount ? $discount->get('id') : null,
                'discount' => $discount ? $discount->get('price') : 0
            ]));

            if ($discount) {
                $this->DiscountUses->save($this->DiscountUses->newEntity([
                    'discount_id' => $discount->get('id'),
                    'user_id' => $entity->get('user_id')
                ]));
            }

            return [
                'result' => true,
                'message' => $charge['outcome']['seller_message'],
                'obj' => $charge
            ];
        }

        return [
            'result' => false,
            'message' => !empty($charge['outcome']['seller_message']) ? $charge['outcome']['seller_message'] : __('Error occurred on Stripe payments gate'),
            'obj' => $charge
        ];
    }

    /**
     * Find, update and return the Stripe customer object or create it if needed
     *
     * @param User  $user    User entity.
     * @param array $payment Ad payment array.
     *
     * @return boolean|ApiResource|StripeObject
     */
    protected function stripeCustomerFindAndUpdateOrCreate(User $user, array $payment)
    {
        $userDetails = $this->UserDetails->find()->where(['user_id' => $user->id])->first();
        if ($userDetails) {
            $customerId = $userDetails->get('stripe_customer_id');
        }

        if (empty($customerId)) {
            try {
                $customer = Customer::create([
                    'description' => $payment['billing_details']['first_name'] . ' ' . $payment['billing_details']['last_name'],
                    'email' => $payment['billing_details']['email'],
                    'source' => $payment['token']['id'],
                    'metadata' => [
                        'id' => $user->id,
                        'uid' => $user->get('uid')
                    ]
                ]);

                $this->UserDetails->save($this->UserDetails->patchEntity($userDetails, ['stripe_customer_id' => $customer['id']], ['validate' => false]));
            } catch (Exception $error) {
                $this->lastError = $error->getMessage();
                return false;
            }
        } else {
            try {
                $customer = Customer::retrieve($customerId);

                if (isset($customer['deleted']) && $customer['deleted']) {
                    $this->lastError = __('The customer does not exists anymore on Stripe. Please call to us to verify state of your account.');
                    return false;
                }

                $customer = Customer::update($customer['id'], [
                    'description' => $payment['billing_details']['first_name'] . ' ' . $payment['billing_details']['last_name'],
                    'email' => $payment['billing_details']['email'],
                    'source' => $payment['token']['id']
                ]);
            } catch (Exception $error) {
                $this->lastError = $error->getMessage();
                return false;
            }
        }

        return $customer;
    }

    /**
     * Return payment title (description) and object type.
     *
     * @param ExtendedEntity $entity Payment object entity.
     *
     * @return array
     */
    private function getPaymentTitle(ExtendedEntity $entity): array
    {
        if ($entity instanceof Property) {
            $object = PaymentType::PROPERTY;
        } elseif ($entity instanceof Service) {
            $object = PaymentType::SERVICE;
        } else {
            return ['result' => false, 'message' => 'Object type not supported'];
        }

        return [
            'result' => true,
            'title' => ucfirst(strtolower($object)) . ' (' . $entity->get('ad_type') . ', exp. ' . $entity->get('ad_expiry_date')->nice() . ')',
            'object' => $object
        ];
    }

    /**
     * Payment option - purchase by order number.
     *
     * @param User           $user     User entity.
     * @param ExtendedEntity $entity   Related payment object entity.
     * @param Discount|null  $discount Discount entity with price value set.
     *
     * @param float          $vat VAT value (payment vat)
     *
     * @return array
     */
    protected function purchaseOrderCharge(User $user, ExtendedEntity $entity, Discount $discount = null, float $vat = 0): array
    {
        $payment = $entity->get('ad_payment');
        $paymentTitle = $this->getPaymentTitle($entity);

        if (!$paymentTitle['result']) {
            return $paymentTitle;
        }

        if ($this->Payments->query()->where([
            'token_id' => $payment['token']['id'],
            'refunded' => false
        ])->count()) {
            return [
                'result' => false,
                'message' => __('This order number has been used.'),
                'obj' => null
            ];
        }

        $vatRate = Settings::get('prices.vat');

        $payload = [
            'amount' => $entity->get('ad_cost') * 100,
            'currency' => $this->stripe['currency'],
            'description' => $paymentTitle['title'],
            'metadata' => [
                'id' => $entity->get('id'),
                'entity' => $paymentTitle['object'],
                'expiry' => $entity->get('ad_expiry_date')->getTimestamp(),
                'plan' => $entity->get('ad_type'),
                'user_id' => $user->get('id'),
                'basket' => $entity->get('ad_basket'),
                'vat_rate' => $vatRate
            ]
        ];

        $this->Payments->save($this->Payments->newEntity([
            'user_id' => $entity->get('user_id'),
            'title' => $paymentTitle['title'],
            'amount' => $entity->get('ad_cost'),
            'amount_pos' => $entity->get('ad_cost') * 100,
            'vat' => $vat,
            'vat_rate' => $vatRate,
            'currency' => $this->stripe['currency'],
            'type' => $paymentTitle['object'],
            'token_id' => $payment['token']['id'],
            'charge_id' => null,
            'object_id' => $entity->get('id'),
            'payload' => json_encode($payload),
            'meta' => json_encode($entity->get('ad_payment')),
            'discount_id' => $discount ? $discount->get('id') : null,
            'discount' => $discount ? $discount->get('price') : 0
        ]));

        if ($discount) {
            $this->DiscountUses->save($this->DiscountUses->newEntity([
                'discount_id' => $discount->get('id'),
                'user_id' => $entity->get('user_id')
            ]));
        }

        return [
            'result' => true,
            'message' => 'Payment complete.',
            'obj' => null
        ];
    }
}
