<?php

namespace App\Controller\Members;

use App\Controller\Component\AuthComponent;
use App\Model\Entity\Group;

/**
 * Authorized controller - all controllers which inherits it,
 * they are needs to be authorized by user first.
 *
 * @property AuthComponent Auth
 * @package App\Controller
 * @author Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
class AuthController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Auth', [
            'className' => 'Auth',
            'loginAction' => [
                'controller' => 'login',
                'action' => 'index',
                'plugin' => null,
                'prefix' => 'members'
            ],
            'loginRedirect' => [
                'controller' => 'Dashboard',
                'action' => 'index',
                'plugin' => null,
                'prefix' => 'members'
            ],
            'logoutRedirect' => [
                'controller' => 'login',
                'action' => 'index',
                'plugin' => null,
                'prefix' => 'members'
            ],
            'authError' => __('You don\'t have access to this page.'),
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ],
                    'userModel' => 'Users',
                    'finder' => [
                        'auth' => [
                            'group_id' => Group::MEMBERS
                        ]
                    ]
                ]
            ],
            'storage' => 'SessionEx',
            'checkAuthIn' => 'Controller.startup'
        ]);
    }
}
