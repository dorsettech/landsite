<?php

namespace App\Controller\Members;

use App\Model\Table\StaticContentsTable;
use App\Settings;
use App\Utils\JavaScriptPacker;
use Cake\Http\Response;
use Exception;

/**
 * Statics content wrapper for JS
 *
 * @property StaticContentsTable $StaticContents
 * @package App\Controller
 * @author  Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
class StaticsController extends AppController
{

    public const ACCOUNT_CREATION = 0b0001;
    public const MEMBER_DASHBOARD = 0b0010;
    public const MARKETING_PREFERENCES = 0b0100;

    /**
     * @return void
     * @throws Exception Inherited.
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadModel('StaticContents');

        $this->autoRender = false;
    }

    /**
     * @param string $flag Static flag.
     *
     * @return Response
     */
    public function index(string $flag): Response
    {
        $names = $this->getGroups(bindec($flag));

        $map = [];

        if (count($names)) {
            $map = $this->StaticContents->find()->innerJoinWith('StaticContentGroups')->where([
                'StaticContentGroups.name IN' => $names
            ])->select(['var_name', 'value'])->combine('var_name', 'value')->toArray();
        }

        $prices = Settings::getGroup('prices');

        $settings = array_combine(array_map(static function ($key) {
            return 'settings-prices-' . $key;
        }, array_keys($prices)), array_values($prices));

        $map = array_merge($map, $settings);

        $wrapper = 'window.__statics = (function () {
                    "use strict";
                    let d = ' . json_encode($map) . ', 
                        func = function() {
                            let args = arguments,
                                label = args[0];
            
                            if (typeof d[label] !== "undefined") {
                                if (args.length === 1) return d[label];
            
                                return d[label].replace(/{(\d+)}/g, function (match, number) {
                                    number = parseInt(number) + 1;
                                    return typeof args[number] != "undefined"
                                        ? args[number]
                                        : match
                                        ;
                                });
                            }
            
                            return String("[ %s ]").replace("%s", label);
                        };                    
                    
                    return {
                        n: function () {
                            return func.apply(this, arguments)                        
                        }                    
                    }
                }).call(window);';

        $packer = new JavaScriptPacker($wrapper, 0, true, false, true);

        return $this->response
            ->withStringBody($packer->pack())
            ->withType('application/javascript');
    }

    /**
     * @param float|integer $flags Dec flag.
     *
     * @return array
     */
    protected function getGroups($flags): array
    {
        $groups = [];
        if ($flags & self::ACCOUNT_CREATION) {
            $groups[] = 'Account Creation';
        }
        if ($flags & self::MEMBER_DASHBOARD) {
            $groups[] = 'Member Dashboard';
        }
        if ($flags & self::MARKETING_PREFERENCES) {
            $groups[] = 'Marketing Preferences';
        }

        return $groups;
    }
}
