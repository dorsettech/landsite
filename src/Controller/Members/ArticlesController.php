<?php

namespace App\Controller\Members;

use App\Mime;
use App\Model\Entity\Article;
use App\Model\Enum\State;
use App\Model\Enum\ArticleStatus;
use App\Model\Enum\ArticleType;
use App\Model\Table\ArticlesTable;
use App\Model\Table\ServicesTable;
use App\Model\Table\UserDetailsTable;
use App\RequestType;
use App\Traits\JsonResponse;
use Cake\Core\Configure;
use Cake\Http\Response;
use Cake\ORM\Query;

/**
 * Insights Controller
 *
 * @property ArticlesTable    $Articles
 * @property ServicesTable    $Services
 * @property UserDetailsTable $UserDetails
 * @package App\Controller\Members
 */
class ArticlesController extends AuthController
{
    use JsonResponse;

    private $tempFilePrefix;
    private $maxFileSize;
    private $mimeAllowed;

    /**
     * @return void
     * @throws \Exception General exception.
     */
    public function initialize(): void
    {
        parent::initialize();

        Configure::load('website');

        $this->tempFilePrefix = Configure::read('Media.upload_tmp_prefix');
        $this->maxFileSize = Configure::read('Media.max_image_size');
        $this->mimeAllowed = Configure::read('Media.mime_types_allowed.image');

        $this->loadModel('Services');
        $this->loadModel('UserDetails');
    }

    /**
     * @return Response
     * @throws \Exception General exception.
     */
    public function index(): Response
    {
        $this->allowMethod(RequestType::GET);

        $userId = $this->Auth->user('id');
        $type = $this->Data->get('type');
        $sort = $this->Data->get('sort');
        $page = $this->Data->asInt('page');
        $perPage = $this->Data->asInt('per_page');

        $conditions = [
            'type' => $type,
            'author_id' => $userId
        ];

        $filters = $this->Data->asArray('filters');

        $status = $filters['status'] ?? '';
        $publishDate = $filters['publish_date'] ?? '';
        $created = $filters['created'] ?? '';
        $modified = $filters['modified'] ?? '';

        switch ($status) {
            case ArticleStatus::DRAFT:
                $conditions['status'] = ArticleStatus::DRAFT;
                break;
            case ArticleStatus::PUBLISHED:
                $conditions['status'] = ArticleStatus::PUBLISHED;
                $conditions['state'] = State::APPROVED;
                break;
            case State::PENDING:
                $conditions['state'] = State::PENDING;
                break;
            case State::REJECTED:
                $conditions['state'] = State::REJECTED;
                break;
        }

        if (!empty($publishDate)) {
            $conditions['DATE(publish_date)'] = $publishDate;
        }

        if (!empty($created)) {
            $conditions['DATE(created)'] = $created;
        }

        if (!empty($modified)) {
            $conditions['DATE(modified)'] = $modified;
        }

        $total = $this->Articles->find('all', [
            'conditions' => $conditions
        ])->count();

        $order = [
            'created' => 'DESC'
        ];

        if (!empty($sort)) {
            [$f, $o] = explode('|', $sort);
            $order = [
                $f => $o
            ];
        }

        $records = $this->Articles->find('all', [
            'conditions' => $conditions,
            'order' => $order
        ])->page($page, $perPage);

        return $this->asJsonResponse([
            'records' => $records,
            'pagination' => [
                'total' => $total,
                'per_page' => $perPage,
                'current_page' => $page,
                'last_page' => ceil($total / $perPage),
                'from' => ($page - 1) * $perPage,
                'to' => $page * $perPage
            ]
        ]);
    }

    /**
     * @return Response
     * @throws \Exception General exception.
     */
    public function article(): Response
    {
        $this->allowMethod(RequestType::GET);

        $userId = $this->Auth->user('id');
        $id = $this->Data->asInt('id');

        $entity = $entity = $this->Articles->find('all', [
            'conditions' => [
                'id' => $id
            ]
        ])->first();

        if (!$entity) {
            return $this->asMessageResponse(false, __('Record does not exists'));
        }

        if ($entity->get('author_id') !== $userId) {
            return $this->asMessageResponse(false, __('You do not have permission to edit this Article'));
        }

        return $this->asJsonResponse($entity->toArray());
    }

    /**
     * @return Response
     * @throws \Exception General exception.
     */
    public function visibility(): Response
    {
        $this->allowMethod(RequestType::PUT);

        $userId = $this->Auth->user('id');
        $id = $this->Data->asInt('id');
        $visibility = $this->Data->get('visibility');

        $entity = $this->Articles->find('all', [
            'conditions' => [
                'id' => $id
            ]
        ])->first();

        if (!$entity) {
            return $this->asMessageResponse(false, __('Record does not exists'));
        }

        if ($entity->get('author_id') !== $userId) {
            return $this->asMessageResponse(false, __('You do not have permission to this action'));
        }

        $entity->set('visibility', $visibility ? 1 : 0);
        $result = $this->Articles->save($entity);

        return $this->asMessageResponse($result, $result ? __('Your changes have been saved') : $entity->getErrorsMessages());
    }

    /**
     * @return Response
     * @throws \Exception General exception.
     */
    public function delete(): Response
    {
        $this->allowMethod(RequestType::DELETE);

        $user = $this->Auth->getUser();
        $id = $this->Data->asInt('id');

        $entity = $this->Articles->find('all', [
            'conditions' => [
                'id' => $id
            ]
        ])->first();

        if (!$entity) {
            return $this->asMessageResponse(false, __('Record does not exists'));
        }

        if ($entity->get('author_id') !== $user->id) {
            return $this->asMessageResponse(false, __('Only author of the Article can delete it'));
        }

        $result = $this->Articles->delete($entity);

        return $this->asMessageResponse($result, $result ? __('Record successfully deleted') : $entity->getErrorsMessages());
    }

    /**
     * @return mixed
     * @throws \Exception General exception.
     */
    public function image()
    {
        $this->allowMethod(RequestType::POST);

        $user = $this->Auth->getUser();

        $fileKey = 'image';

        $image = $this->Data->get($fileKey);

        if ($image['size'] > $this->maxFileSize) {
            return $this->asMessageResponse(false, __('Maximum allowed size is {0} bytes', $this->maxFileSize));
        }

        if (!in_array($image['type'], $this->mimeAllowed, true)) {
            return $this->asMessageResponse(false, __('File type not allowed. Acceptable types: JPEG, PNG, WEBP'));
        }

        $fileDir = MEMBERS_ARTICLE_FILES . $user->get('uid') . DS;
        if (!file_exists($fileDir) && !mkdir($fileDir, 0777, true) && !is_dir($fileDir)) {
            return $this->asMessageResponse(false, sprintf('Directory "%s" was not created', $fileDir));
        }

        $fileName = str_replace('.', '', uniqid($this->tempFilePrefix . $user->get('id') . '-', true)) . '.' . Mime::mimeToExtension($image['type']);
        $filePath = MEMBERS_ARTICLE_FILES . $user->get('uid') . DS . $fileName;

        $result = false;
        if (is_uploaded_file($image['tmp_name'])) {
            $result = move_uploaded_file($image['tmp_name'], $filePath);
        }

        return $this->asMessageResponse($result, $result ? __('File uploaded successfully.') : __('Upload failed. Unexpected server error occurred.'), [$fileKey => $fileName]);
    }

    /**
     * Save draft
     *
     * @return Response
     */
    public function draft(): Response
    {
        $userId = $this->Auth->user('id');
        $data = $this->Data->all();

        $service = $this->Services->find('all', [
            'fields' => ['category_id'],
            'conditions' => [
                'user_id' => $userId
            ]
        ])->first();

        if (!$service) {
            return $this->asMessageResponse(false, __('Before you add an Article you have to publish your business profile'));
        }

        $data['author_id'] = $userId;

        if (!isset($data['status'])) {
            $data['status'] = ArticleStatus::DRAFT;
        } elseif (!($data['status'] === ArticleStatus::PUBLISHED && $data['state'] === State::PENDING)) {
            $data['status'] = ArticleStatus::DRAFT;
            $data['state'] = null;
        }

        if (empty($data['category_id'])) {
            $data['category_id'] = $service->get('category_id');
        }

        if ($this->Articles->hasBehavior('Upload')) {
            $this->Articles->removeBehavior('Upload');
        }

        $entity = $this->Articles->patchEntity(empty($data['id']) ? $this->Articles->newEntity() : $this->Articles->get($data['id']), $data);

        $result = $this->Articles->save($entity);

        if ($result && $this->renameTemporaryFile($entity)) {
            $entity = $this->Articles->get($entity->id);
        }

        return $this->asMessageResponse($result, $result ? __('Draft successfully saved') : $entity->getErrorsMessages(), [
            'image' => $entity->image,
            'id' => $entity->id
        ]);
    }

    /**
     * Rename temporary file name (to keep tidiness)
     *
     * @param Article $entity Article entity.
     *
     * @return boolean|Article
     */
    private function renameTemporaryFile(Article $entity)
    {
        $user = $this->Auth->getUser();
        $filePath = MEMBERS_ARTICLE_FILES . $user->get('uid') . DS;

        if ($entity->get('image') === null || strpos($entity->get('image'), $this->tempFilePrefix) !== 0) {
            return false;
        }
        $destFileName = str_replace($this->tempFilePrefix, '', $entity->get('image'));
        if (rename($filePath . $entity->get('image'), $filePath . $destFileName)) {
            $entity->set('image', $destFileName);
            return $this->Articles->save($entity);
        }

        return false;
    }

    /**
     * Submit/Update service
     *
     * @return Response
     * @throws \Exception General exception.
     */
    public function update(): Response
    {
        $userId = $this->Auth->user('id');
        $data = $this->Data->all();

        $service = $this->Services->find('all', [
            'fields' => ['category_id'],
            'conditions' => [
                'user_id' => $userId
            ]
        ])->first();

        if (!$service && empty($data['category_id'])) {
            return $this->asMessageResponse(false, __('Before you add an Article you have to publish your business profile'));
        }

        $data['author_id'] = $userId;
        $data['status'] = ArticleStatus::PUBLISHED;
        $data['state'] = State::PENDING;

        if (empty($data['category_id'])) {
            $data['category_id'] = $service->get('category_id');
        }

        if ($this->Articles->hasBehavior('Upload')) {
            $this->Articles->removeBehavior('Upload');
        }

        $entity = $this->Articles->patchEntity(empty($data['id']) ? $this->Articles->newEntity() : $this->Articles->get($data['id']), $data);

        $result = $this->Articles->save($entity);

        if ($result && $this->renameTemporaryFile($entity)) {
            $entity = $this->Articles->get($entity->id);
        }

        return $this->asMessageResponse($result, $result ? __('Article has been successfully saved.It needs to be accepted by our team before publication.') : $entity->getErrorsMessages(), [
            'image' => $entity->image,
            'id' => $entity->id
        ]);
    }

    /**
     * Return recent news (insights) or case studies
     *
     * @return Response
     * @throws \ReflectionException General exception.
     */
    public function recent(): Response
    {
        $userId = $this->Auth->user(['id']);
        $type = $this->Data->get('type');
        $page = $this->Data->asInt('page');
        $perPage = $this->Data->asInt('per_page');

        $preferred = $this->UserDetails->find('all', [
            'conditions' => [
                'user_id' => $userId
            ],
            'fields' => ['pref_insights_list']
        ])->extract('pref_insights_list')->first();

        $preferred = explode(',', $preferred);

        $conditions = [
            'author_id !=' => $userId,
            'type' => ArticleType::hasName($type) ? $type : ArticleType::INSIGHT,
            'Articles.visibility' => true,
            'Articles.status' => ArticleStatus::PUBLISHED,
            'Articles.state' => State::APPROVED,
            'Articles.category_id IN' => count($preferred) ? $preferred : [0]
        ];

        $total = $this->Articles->find('all', [
            'conditions' => $conditions
        ])->count();

        $order = [
            'Articles.publish_date' => 'DESC'
        ];

        $records = $this->Articles->find('all', [
            'conditions' => $conditions,
            'order' => $order,
            'contain' => [
                'Posters.UserDetails' => static function (Query $q) {
                    return $q->select(['company']);
                },
                'Posters.Services' => static function (Query $q) {
                    return $q->select(['id', 'slug', 'company']);
                }
            ]
        ])->page($page, $perPage);

        return $this->asJsonResponse([
            'records' => $records,
            'pagination' => [
                'total' => $total,
                'per_page' => $perPage,
                'current_page' => $page,
                'last_page' => ceil($total / $perPage),
                'from' => ($page - 1) * $perPage,
                'to' => $page * $perPage
            ]
        ]);
    }
}
