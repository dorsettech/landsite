<?php

namespace App\Controller\Members;

use App\Mime;
use App\Model\Entity\Property;
use App\Model\Entity\PropertyMedia;
use App\Model\Enum\MediaType;
use App\Model\Enum\PropertyStatus;
use App\Model\Enum\State;
use App\Model\Table\DiscountsTable;
use App\Model\Table\DiscountUsesTable;
use App\Model\Table\PropertiesTable;
use App\Model\Table\PropertyEnquiriesTable;
use App\Model\Table\PropertyMediaTable;
use App\Model\Table\PropertyViewsTable;
use App\Model\Table\UserSavedTable;
use App\RequestType;
use App\Settings;
use App\Traits\DiscountValidator;
use Cake\Core\Configure;
use Cake\Http\Response;
use Cake\I18n\Time;
use DateInterval;
use DatePeriod;
use DateTime;

/**
 * Properties Controller
 *
 * @property PropertiesTable        $Properties
 * @property PropertyMediaTable     $PropertyMedia
 * @property PropertyViewsTable     $PropertyViews
 * @property PropertyEnquiriesTable $PropertyEnquiries
 * @property DiscountsTable         $Discounts
 * @property DiscountUsesTable      $DiscountUses
 * @property UserSavedTable         $UserSaved
 * @package App\Controller\Members
 */
class PropertiesController extends PaymentController
{

    use DiscountValidator;

    private $tempFilePrefix;
    private $maxFileSize;
    private $mimeAllowed;

    /**
     * Initialize controller
     *
     * @return void
     * @throws \Exception General exception.
     */
    public function initialize(): void
    {
        parent::initialize();

        Configure::load('website');

        $this->tempFilePrefix = Configure::read('Media.upload_tmp_prefix');
        $this->maxFileSize = Configure::read('Media.max_image_size');
        $this->mimeAllowed = Configure::read('Media.mime_types_allowed');

        $this->loadModel('PropertyMedia');
        $this->loadModel('PropertyViews');
        $this->loadModel('PropertyEnquiries');
        $this->loadModel('UserSaved');
        $this->loadModel('Discounts');
        $this->loadModel('DiscountUses');

        $this->Auth->allow(['stats']);
    }

    /**
     * @return Response
     * @throws \Exception Method exception.
     */
    public function index(): Response
    {
        $this->allowMethod(RequestType::GET);

        $userId = $this->Auth->user('id');
        $page = $this->Data->asInt('page');
        $perPage = $this->Data->asInt('per_page');

        $conditions = [
            'user_id' => $userId
        ];

        $filters = $this->Data->asArray('filters');

        $keywords = $filters['keywords'] ?? '';

        if (!empty($keywords)) {
            $conditions['title LIKE'] = "%$keywords%";
        }

        $total = $this->Properties->find('all', [
            'conditions' => $conditions
        ])->count();

        $order = [
            'modified' => 'DESC',
            'id' => 'DESC'
        ];

        $records = $this->Properties->find('all', [
            'conditions' => $conditions,
            'order' => $order,
            'contain' => [
                'PropertyMedia',
                'PropertiesPropertyAttributes'
            ]
        ])->page($page, $perPage);

        return $this->asJsonResponse([
            'records' => $records,
            'pagination' => [
                'total' => $total,
                'per_page' => $perPage,
                'current_page' => $page,
                'last_page' => ceil($total / $perPage),
                'from' => ($page - 1) * $perPage,
                'to' => $page * $perPage
            ]
        ]);
    }

    /**
     * @return Response
     * @throws \Exception Method Exception.
     */
    public function property(): Response
    {
        $this->allowMethod(RequestType::GET);

        $userId = $this->Auth->user('id');
        $id = $this->Data->asInt('id');

        $entity = $entity = $this->Properties->find('all', [
            'conditions' => [
                'id' => $id
            ],
            'contain' => [
                'PropertyMedia',
                'PropertiesPropertyAttributes'
            ]
        ])->first();

        if (!$entity) {
            return $this->asMessageResponse(false, __('Record does not exists'));
        }

        if ($entity->get('user_id') !== $userId) {
            return $this->asMessageResponse(false, __('You do not have permission to edit this Property'));
        }

        return $this->asJsonResponse($entity->toArray());
    }

    /**
     * Save draft
     *
     * @return Response
     */
    public function draft(): Response
    {
        $data = $this->Data->all();

        $this->unsetSensitiveData($data);

        if ($this->Properties->PropertyMedia->hasBehavior('Upload')) {
            $this->Properties->PropertyMedia->removeBehavior('Upload');
        }

        $data['user_id'] = $this->Auth->user('id');
        $data['status'] = PropertyStatus::DRAFT;

        $entity = $this->Properties->patchEntity(empty($data['id']) ?
            $this->Properties->newEntity() :
            $this->Properties->get($data['id'], [
                'contain' => [
                    'PropertyMedia',
                    'PropertiesPropertyAttributes'
                ]
            ]), $data);

        $result = $this->Properties->save($entity, ['associated' => [
            'PropertyMedia',
            'PropertiesPropertyAttributes'
        ]]);

        if ($result) {
            $this->renameTemporaryFiles($entity);
        }

        $medias = $this->PropertyMedia->find()->where([
            'property_id' => $entity->id
        ]);

        return $this->asMessageResponse($result, $result ? __('Draft successfully saved') : $entity->getErrorsMessages(), [
            'media' => $medias,
            'id' => $entity->id
        ]);
    }

    /**
     * @param array $data Request data.
     *
     * @return void
     */
    private function unsetSensitiveData(array &$data): void
    {
        unset(
            $data['ad_basket'],
            $data['ad_cost'],
            $data['ad_expiry_date'],
            $data['ad_extend'],
            $data['ad_payment'],
            $data['ad_total_cost'],
            $data['ad_type'],
            $data['deleted'],
            $data['publish_date'],
            $data['status'],
            $data['visibility']
        );
    }

    /**
     * Rename temporary file names (to keep tidiness)
     *
     * @param Property $entity Property entity.
     *
     * @return void
     */
    private function renameTemporaryFiles(Property $entity): void
    {
        $user = $this->Auth->getUser();
        $filePath = MEMBERS_PROPERTY_FILES . $user->get('uid') . DS;
        $this->PropertyMedia->find()->where([
            'property_id' => $entity->id,
            'type !=' => MediaType::VIDEO
        ])->each(function (PropertyMedia $media) use ($filePath) {
            if (strpos($media->get('source'), $this->tempFilePrefix) !== 0) {
                return;
            }
            $destFileName = str_replace($this->tempFilePrefix, '', $media->get('source'));
            if (rename($filePath . $media->get('source'), $filePath . $destFileName)) {
                $media->set('source', $destFileName);
                $this->PropertyMedia->save($media);
            }
        });
    }


    /**
     * Submit/Update property
     *
     * @return Response
     * @throws \Exception ORM exceptions.
     */
    public function update(): Response
    {
        $data = $this->Data->all();
        $user = $this->Auth->getUser();
        $userId = $user->id;

        if (!isset($data['ad_extend'])) {
            $data['ad_extend'] = false;
        }

        unset($data['publish_date']);
        $data['status'] = PropertyStatus::PUBLISHED;
        $data['state'] = State::PENDING;
        $data['user_id'] = $userId;

        if ($this->Properties->PropertyMedia->hasBehavior('Upload')) {
            $this->Properties->PropertyMedia->removeBehavior('Upload');
        }

        $validity = Settings::getGroup('validity');

        $entity = $this->Properties->get($data['id'], [
            'contain' => [
                'PropertyMedia',
                'PropertiesPropertyAttributes'
            ]
        ]);

        if ($entity->get('publish_date') === null) {
            $data['publish_date'] = new Time();
        }

        if ($entity->get('visibility')) {
            if ($entity->get('ad_expiry_date') === null || (
                    $entity->get('status') === PropertyStatus::EXPIRED && !empty($data['ad_type']) && ($data['ad_cost'] > 0 || !empty($data['ad_discount']))
                )) {
                $expiryDateTime = new Time();
                $expiryDateTime->add(new \DateInterval("P{$validity['property-' . strtolower($data['ad_type']) . '-days']}D"));
                $data['ad_expiry_date'] = $expiryDateTime;
            } elseif ($data['ad_extend']) {
                $expiryDateTime = new Time($entity->get('ad_expiry_date'));
                $expiryDateTime->add(new \DateInterval("P{$validity['property-' . strtolower($entity->get('ad_type')) . '-days']}D"));
                $data['ad_expiry_date'] = $expiryDateTime;
            } else {
                unset($data['ad_expiry_date']);
            }
        } else {
            if ($data['ad_expiry_date'] === null && !empty($data['ad_type'])) {
                $expiryDateTime = new Time();
                $expiryDateTime->add(new \DateInterval("P{$validity['property-' . strtolower($data['ad_type']) . '-days']}D"));
                $data['ad_expiry_date'] = $expiryDateTime;
            } else {
                unset($data['ad_expiry_date']);
            }
        }

        $entity = $this->Properties->patchEntity($entity, $data);

        $hasToPay = $data['ad_cost'] > 0 && !empty($data['ad_basket']) && !empty($data['ad_payment']);

        // temporary disable item visibility until payment will be done or change the state
        $entity->set('visibility', !$hasToPay);

        $adBasket = null;
        if (!empty($data['ad_basket'])) {
            if (is_array($data['ad_basket'])) {
                $adBasket = json_encode($data['ad_basket']);
            } else {
                $adBasket = $data['ad_basket'];
            }
        }
        $entity->set('ad_basket', $adBasket);

        $result = $this->Properties->save($entity, ['associated' => [
            'PropertyMedia',
            'PropertiesPropertyAttributes'
        ]]);

        $medias = $this->PropertyMedia->find()->where([
            'property_id' => $entity->id
        ])->order(['position' => 'ASC']);

        if ($result) {
            $this->renameTemporaryFiles($entity);
            $this->Properties->dispatchEvent('Dashboard.update', [$userId]);

            $discount = null;
            if (!empty($data['ad_discount_code'])) {
                $discount = $this->checkCodeValidity($data['ad_discount_code'], $userId);
                if (!$discount['success']) {
                    return $this->asMessageResponse(false, $discount['msg'], [
                        'media' => $medias
                    ]);
                }
            }

            if (!$hasToPay) {
                if ($discount) {
                    $this->DiscountUses->save($this->DiscountUses->newEntity([
                        'discount_id' => $discount['discount']->get('id'),
                        'user_id' => $userId
                    ]));
                }
                return $this->asMessageResponse(true, __('Your changes have been saved'), [
                    'media' => $medias
                ]);
            }

            try {
                if ($discount) {
                    $discount = $discount['discount'];
                    $discount->set('price', $data['ad_discount']['price']);
                }

                $paid = $this->charge($data['ad_payment']['type'], $user, $entity, $discount, $data['ad_vat']);

                if ($paid['result'] === true) {
                    $entity->set('visibility', true);
                    $entity->set('ad_basket', null);
                    $entity->set('ad_total_cost', $entity->get('ad_total_cost') + $entity->get('ad_cost'));
                    $this->Properties->save($entity);
                }

                return $this->asMessageResponse($paid['result'], $paid['message'], [
                    'media' => $medias
                ]);
            } catch (\BadMethodCallException $exception) {
                return $this->asMessageResponse(false, $exception->getMessage());
            }
        }

        return $this->asMessageResponse(false, $entity->getErrorsMessages(), [
            'media' => $medias
        ]);
    }

    /**
     * @return mixed
     * @throws \Exception See - files.
     */
    public function images()
    {
        return $this->files('image');
    }

    /**
     * @param string $type Possible value: image, document.
     *
     * @return mixed
     * @throws \Exception General exception.
     */
    protected function files(string $type)
    {
        $this->allowMethod(RequestType::POST);

        $files = $this->Data->all();

        $results = [];
        foreach ($files as $file) {
            $results[] = $this->processUpload($file, $type);
        }

        $result = array_filter($results, static function ($item) {
            return $item['success'] === true;
        });

        if (count($result) === 0) {
            $overall = false;
        } else {
            $overall = count($result) === count($files);
        }

        return $this->asMessageResponse($overall, $overall ? __('File(s) uploaded successfully.') : __('Upload failed on one or more files.'), ['results' => $results]);
    }

    /**
     * @param array  $file File details.
     * @param string $type Possible value: image, document.
     *
     * @return array|null
     */
    protected function processUpload(array $file, string $type): ?array
    {
        $user = $this->Auth->getUser();

        if ($file['size'] > $this->maxFileSize) {
            return ['success' => false, 'msg' => __('Maximum allowed size is {0} bytes', $this->maxFileSize), $type => $file['name']];
        }

        if (!in_array($file['type'], $this->mimeAllowed[$type], true)) {
            return ['success' => false, 'msg' => __('File type not allowed. Acceptable types: JPEG, PNG, WEBP, DOC, DOCX, PDF'), $type => $file['name']];
        }

        $fileName = str_replace('.', '', uniqid($this->tempFilePrefix . $user->get('id') . '-', true)) . '.' . Mime::mimeToExtension($file['type']);
        $fileDir = MEMBERS_PROPERTY_FILES . $user->get('uid') . DS;
        $filePath = $fileDir . $fileName;

        if (!file_exists($fileDir) && !mkdir($fileDir, 0777, true) && !is_dir($fileDir)) {
            return ['success' => false, 'msg' => sprintf('Directory "%s" was not created', $fileDir), $type => $file['name']];
        }

        $result = false;
        if (is_uploaded_file($file['tmp_name'])) {
            $result = move_uploaded_file($file['tmp_name'], $filePath);
        }

        if ($result) {
            return ['success' => true, $type => $file['name'], 'filename' => $fileName];
        }

        return ['success' => false, 'msg' => __('An unexpected error occurred.'), $type => $file['name']];
    }

    /**
     * @return mixed
     * @throws \Exception General exception.
     */
    public function documents()
    {
        return $this->files('document');
    }

    /**
     * @return Response
     * @throws \Exception General exception.
     */
    public function delete(): Response
    {
        $this->allowMethod(RequestType::DELETE);

        $userId = $this->Auth->user('id');
        $id = $this->Data->asInt('id');
        $reason = $this->Data->get('deletion_reason');

        $entity = $this->Properties->find('all', [
            'conditions' => [
                'id' => $id
            ]
        ])->first();

        if (!$entity) {
            return $this->asMessageResponse(false, __('Record does not exists'));
        }

        if ($entity->get('user_id') !== $userId) {
            return $this->asMessageResponse(false, __('Only creator of the Property can delete it'));
        }

        if (!empty($reason)) {
            $entity->set('deletion_reason', $reason);
            $this->Properties->save($entity);
        }

        $result = $this->Properties->delete($entity);

        if ($result) {
            $this->Properties->dispatchEvent('Dashboard.update', [$userId]);
        }

        return $this->asMessageResponse($result, $result ? __('Record successfully deleted') : $entity->getErrorsMessages());
    }

    /**
     * @return mixed
     * @throws \Exception General exception.
     */
    public function stats()
    {
        $this->allowMethod(RequestType::GET);

        $user = $this->Auth->getUser();
        $id = $this->Data->asInt('id');

        $property = $this->Properties->get($id);

        if ($user->id !== $property->get('user_id')) {
            return $this->asJsonResponse();
        }

        $oneMonthIntervalInverted = new DateInterval('P1M');
        $dateTimeStart = new DateTime();
        $dateTimeStart->modify('first day of this month');
        $dateTimeStart->setTime(0, 0, 0);
        $dateTimeStart->sub(new DateInterval('P11M'));
        $datePeriod = new DatePeriod($dateTimeStart, $oneMonthIntervalInverted, 11);
        $stats = [];
        foreach ($datePeriod as $key => $item) {
            $label = $item->format('M, y');
            if (!isset($stats[$key])) {
                $stats[$key] = [
                    'label' => $label,
                    'views' => 0,
                    'enquiries' => 0
                ];
            }

            $stats[$key]['views'] = (int)$this->PropertyViews->query()->select([
                'value' => 'COUNT(*)'
            ])->where([
                'property_id' => $id,
                'YEAR(created)' => $item->format('Y'),
                'MONTH(created)' => $item->format('n')
            ])->group('DATE_FORMAT(created,"%Y-%m")')->extract('value')->first();

            $stats[$key]['enquiries'] = (int)$this->PropertyEnquiries->query()->select([
                'value' => 'COUNT(*)'
            ])->where([
                'property_id' => $id,
                'YEAR(created)' => $item->format('Y'),
                'MONTH(created)' => $item->format('n')
            ])->group('DATE_FORMAT(created,"%Y-%m")')->extract('value')->first();
        }

        return $this->asJsonResponse($stats);
    }

    /**
     * @return mixed
     * @throws \Exception General exception.
     */
    public function enquiries()
    {
        $this->allowMethod(RequestType::GET);

        $userId = $this->Auth->user('id');
        $id = $this->Data->asInt('id');
        $sort = $this->Data->get('sort');
        $page = $this->Data->asInt('page');
        $perPage = $this->Data->asInt('per_page');

        if (!empty($id)) {
            $property = $this->Properties->get($id);

            if ($userId !== $property->get('user_id')) {
                return $this->asJsonResponse();
            }

            $conditions = [
                'property_id' => $id
            ];
        } else {
            $conditions = [
                'Properties.user_id' => $userId
            ];
        }

        $total = $this->PropertyEnquiries->find('all', [
            'contain' => [
                'Properties', 'Users'
            ],
            'conditions' => $conditions
        ])->count();

        $order = [];

        if (!empty($sort)) {
            [$f, $o] = explode('|', $sort);
            $order = [
                $f => $o
            ];
        }

        $records = $this->PropertyEnquiries->find('all', [
            'fields' => [
                'id', 'PropertyEnquiries.created', 'full_name', 'message', 'phone', 'title', 'enquiry_title' => 'Properties.title', 'email' => 'Users.email'
            ],
            'contain' => [
                'Properties', 'Users'
            ],
            'conditions' => $conditions,
            'order' => $order
        ])->page($page, $perPage);

        return $this->asJsonResponse([
            'records' => $records,
            'pagination' => [
                'total' => $total,
                'per_page' => $perPage,
                'current_page' => $page,
                'last_page' => ceil($total / $perPage),
                'from' => ($page - 1) * $perPage,
                'to' => $page * $perPage
            ]
        ]);
    }

    /**
     * @return mixed
     * @throws \Exception General exception.
     */
    public function saved()
    {
        $this->allowMethod(RequestType::GET);

        $userId = $this->Auth->user('id');
        $page = $this->Data->asInt('page');
        $perPage = $this->Data->asInt('per_page');

        $conditions = [
            'UserSaved.user_id' => $userId,
            'property_id IS NOT' => null,
            'service_id IS' => null
        ];

        $total = $this->UserSaved->find('all', [
            'conditions' => $conditions
        ])->count();

        $order = [
            'UserSaved.created' => 'DESC'
        ];

        $records = $this->UserSaved->find('all', [
            'conditions' => $conditions,
            'order' => $order,
            'contain' => [
                'Properties.PropertyMedia'
            ]
        ])->page($page, $perPage);

        return $this->asJsonResponse([
            'records' => $records,
            'pagination' => [
                'total' => $total,
                'per_page' => $perPage,
                'current_page' => $page,
                'last_page' => ceil($total / $perPage),
                'from' => ($page - 1) * $perPage,
                'to' => $page * $perPage
            ]
        ]);
    }

    /**
     * @return Response
     * @throws \Exception General exception.
     */
    public function deleteSaved(): Response
    {
        $this->allowMethod(RequestType::DELETE);

        $user = $this->Auth->getUser();
        $id = $this->Data->asInt('id');

        $entity = $this->UserSaved->find('all', [
            'conditions' => [
                'id' => $id,
                'user_id' => $user->id
            ]
        ])->first();

        if (!$entity) {
            return $this->asMessageResponse(false, __('Record does not exists'));
        }

        $result = $this->UserSaved->delete($entity);

        return $this->asMessageResponse($result, $result ? __('Record successfully deleted') : $entity->getErrorsMessages());
    }
}
