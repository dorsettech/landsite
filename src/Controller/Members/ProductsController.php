<?php

namespace App\Controller\Members;

use App\Model\Table\ProductsTable;
use App\RequestType;
use App\Traits\JsonResponse;
use Cake\Http\Response;

/**
 * Products Controller
 *
 * @property ProductsTable $Products
 * @package App\Controller\Members
 */
class ProductsController extends AuthController
{
    use JsonResponse;

    /**
     * @return mixed
     * @throws \Exception
     */
    public function index(): Response
    {
        $this->allowMethod(RequestType::GET);

        return $this->asListResponse($this->Products->find('all', [
            'fields' => [
                'id', 'name', 'description'
            ],
            'order' => [
                'name' => 'ASC'
            ]
        ]));
    }
}
