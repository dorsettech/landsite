<?php

namespace App\Controller\Members;

use App\Model\Enum\CommonUseClass;
use App\Model\Table\PropertyTypesTable;
use App\RequestType;
use App\Traits\JsonResponse;
use Cake\Http\Response;

/**
 * Property Types Controller
 *
 * @property PropertyTypesTable $PropertyTypes
 * @package App\Controller\Members
 */
class PropertyOptionsController extends AuthController
{
    use JsonResponse;

    /**
     * @return void
     * @throws \Exception Inherited exception.
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadModel('PropertyTypes');

        $this->Auth->allow(['index']);
    }

    /**
     * Return active property types and related attributes
     *
     * @return mixed
     * @throws \Exception Inherited exception.
     */
    public function index(): Response
    {
        $this->allowMethod(RequestType::GET);

        return $this->asJsonResponse([
            'types' => $this->PropertyTypes->find('threaded', [
                'fields' => [
                    'id', 'parent_id', 'lft', 'rght', 'label' => 'PropertyTypes.name'
                ],
                'conditions' => [
                    'PropertyTypes.visibility' => true
                ],
                'contain' => [
                    'PropertyAttributes' => [
                        'fields' => [
                            'id', 'name', 'description', 'image', 'type_id'
                        ]
                    ]
                ],
                'order' => [
                    'PropertyTypes.position' => 'ASC'
                ]
            ]),
            'classes' => array_map(function ($id, $label) {
                return [
                    'id' => $id,
                    'label' => $label
                ];
            }, CommonUseClass::getConstList(), CommonUseClass::getNamesList())
        ]);
    }
}
