<?php

namespace App\Controller\Members;

use App\Application;
use App\Controller\Component\DataComponent;
use App\Http\Exception\MethodNotAllowedException;
use App\Mime;
use App\ResponseType;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\I18n\I18n;
use Cake\Network\Response;
use Exception;
use ReflectionException;

/**
 * @property DataComponent Data
 * @property ResponseType  responseType
 */
class AppController extends Controller
{
    /**
     * Cached build version
     */
    private static $buildVersion;

    /**
     * Major.Minor member area version
     */
    public $VERSION = '1.0';

    /**
     * Major.Minor api version
     */
    public $API_VERSION = '1.0';

    /**
     * @return void
     * @throws Exception Inherited.
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Data');
    }

    /**
     * Event used for reinitialization Data component to fix unexpected behaviour (especially when CORS enabled).
     *
     * @return mixed
     * @throws ReflectionException Inherited.
     */
    public function invokeAction()
    {
        $this->Data->reinitialize();
        return parent::invokeAction();
    }

    /**
     * Before render callback.
     *
     * @param Event $event Life cycle event.
     *
     * @return Response|null|void
     * @throws ReflectionException Inherited.
     */
    public function beforeRender(Event $event)
    {
        if (isset($this->responseType) && $this->responseType !== '') {
            $this->RequestHandler->renderAs($this, $this->responseType);
            $this->response->withType($this->responseType);

            if (in_array($this->responseType, array_values(ResponseType::getConstList()), true)) {
                $this->viewBuilder()->setClassName(ucfirst($this->responseType));
            }
        }

        if ($this->response->getType() === Mime::extensionToMime('html')) {
            $this->set([
                'appLanguage' => str_replace('_', '-', I18n::getLocale()),
                'appDescription' => 'The Land Site - member area',
                'appKeywords' => 'properties,businesses,insights,case studies',
                'appAuthor' => 'Bespoke 4 Business',
                'appName' => 'The Land Site',
                'appCompany' => 'The Land Site',
                'isProduction' => Application::isProduction(),
                'appVersion' => $this->getApplicationVersion(),
                'appBuildVersion' => $this->getBuildVersion(),
                'appRevisionString' => $this->getRevisionString(),
                'csrfToken' => $this->request->getParam('_csrfToken')
            ]);
        }

        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->getType(), [Mime::extensionToMime('json'), Mime::extensionToMime('xml')], true)
        ) {
            $this->set('_serialize', true);
        }
    }

    /**
     * Get application version string.
     *
     * @return string
     */
    protected function getApplicationVersion(): string
    {
        return $this->VERSION;
    }

    /**
     * Get build version string.
     *
     * @return string
     */
    protected function getBuildVersion(): string
    {
        if (!empty(self::$buildVersion)) {
            return self::$buildVersion;
        }
        $path = ROOT . DS . 'VERSION-MEMBERS';
        if (!file_exists($path)) {
            return '';
        }
        $content = file_get_contents($path);
        $version = explode(' ', $content);
        self::$buildVersion = isset($version[1]) ? trim($version[1]) : '';
        return self::$buildVersion;
    }

    /**
     * Get full revision string of the app or API.
     *
     * @param boolean $api Provide API version or the app.
     *
     * @return string
     */
    protected function getRevisionString(bool $api = false): string
    {
        $version = $api ? $this->getApiVersion() : $this->getApplicationVersion();
        $build = $this->getBuildVersion();

        if ($build === '') {
            return $version;
        }

        return trim($version . '.' . substr($build, strpos($build, '-') + 1));
    }

    /**
     * Get API version string.
     *
     * @return string
     */
    protected function getApiVersion(): string
    {
        return $this->API_VERSION;
    }

    /**
     * Return response type if was set.
     *
     * @return ResponseType|string
     */
    protected function getResponseType()
    {
        return $this->responseType ?? '';
    }

    /**
     * Extended and aliased request object method.
     *
     * @param array|string $methods Methods.
     *
     * @return boolean
     * @throws Exception Inherited.
     * @see ServerRequest::allowMethod()
     */
    protected function allowMethod($methods): bool
    {
        $methods = (array)$methods;
        foreach ($methods as $method) {
            if ($this->request->is($method)) {
                return true;
            }
        }
        $allowed = strtoupper(implode(', ', $methods));
        $responseType = (isset($this->responseType) && !empty($this->responseType)) ? $this->responseType : '';
        $exceptionClassName = 'App\\Http\\Exception\\' . ucfirst($responseType) . 'MethodNotAllowedException';
        if (class_exists($exceptionClassName)) {
            $e = new $exceptionClassName(null, 401, $allowed);
        } else {
            $e = new MethodNotAllowedException();
        }
        $e->responseHeader('Allow', $allowed);
        /** @var Exception $e */
        throw $e;
    }

    /**
     * Show "upgrade browser" page.
     *
     * @return void
     */
    public function upgradeBrowser(): void
    {
        $this->setResponseType(ResponseType::NONE);
        $this->viewBuilder()->setTemplate('upgrade-browser');
    }

    /**
     * Set response type (format ie. json, xml, html, plain)
     *
     * @param string $responseType Response type.
     *
     * @return boolean|object
     */
    protected function setResponseType(string $responseType)
    {
        $this->responseType = $responseType;
        return $this->responseType;
    }
}
