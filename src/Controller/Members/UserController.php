<?php
namespace App\Controller\Members;

use App\Mailer\Members\AccountMailer;
use App\Mailer\Members\PasswordMailer;
use App\Mime;
use App\Model\Entity\Group;
use App\Model\Entity\User;
use App\Model\Entity\UserDetail;
use App\Model\Table\ServicesTable;
use App\Model\Table\UserBillingAddressesTable;
use App\Model\Table\UserDetailsTable;
use App\Model\Table\UsersTable;
use App\Model\Table\UserTokensTable;
use App\Model\Table\EmailQueueMembersTable;
use App\RequestType;
use App\ResponseType;
use App\Traits\JsonResponse;
use Cake\Core\Configure;
use Cake\Http\Response;
use Cake\I18n\Time;
use Cake\Mailer\MailerAwareTrait;
use Cake\Utility\Text;
use Cake\Validation\Validation;



/**
 * Login Controller
 *
 * @package App\Controller\Members
 * @property UsersTable                $Users
 * @property UserDetailsTable          $UserDetails
 * @property UserTokensTable           $UserTokens
 * @property ServicesTable             $Services
 * @property UserBillingAddressesTable $UserBillingAddresses
 * @property EmailQueueMembersTable    $EmailQueueMembers
 *
 * @author  Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
class UserController extends AuthController
{
    use JsonResponse;
    use MailerAwareTrait;

    private $tempFilePrefix;
    private $maxFileSize;
    private $mimeAllowed;

    /**
     * @return void
     * @throws \Exception General exception.
     */
    public function initialize(): void
    {
        parent::initialize();

        Configure::load('website');

        $this->tempFilePrefix = Configure::read('Media.upload_tmp_prefix');
        $this->maxFileSize = Configure::read('Media.max_image_size');
        $this->mimeAllowed = Configure::read('Media.mime_types_allowed.image');

        $this->loadModel('Users');
        $this->loadModel('UserDetails');
        $this->loadModel('UserTokens');
        $this->loadModel('UserBillingAddresses');
        $this->loadModel('Services');
        $this->loadModel('EmailQueueMembers');

        $this->Auth->allow(['index', 'register', 'login', 'logout', 'validateEmail', 'password', 'verifyEmail', 'resendVerificationEmails', 'unsubscribe']);
    }

    /**
     * @return void
     */
    public function index(): void
    {
        $this->setResponseType(ResponseType::NONE);

        if ($this->Auth->isAuthenticated()) {
            $this->redirect($this->Auth->redirectUrl());
        }

        $this->viewBuilder()->setLayout('default');
    }

    /*
     * Create a forum user
     *
     * @param array containing username, password and email
     *
     * @return (int) newly created user id (in forum system)
     * */
    public function forum_add($details){

        global $db, $phpbb_container;

        $group_name = 'REGISTERED';
        $sql = 'SELECT group_id
        FROM ' . GROUPS_TABLE . "
        WHERE group_name = '" . $db->sql_escape($group_name) . "'
            AND group_type = " . GROUP_SPECIAL;

        $result = $db->sql_query($sql);
        $row = $db->sql_fetchrow($result);
        $group_id = $row['group_id'];

        $user_actkey = md5(rand(0, 100) . time());
        $user_actkey = substr($user_actkey, 0, rand(6, 10));
        $timezone = '0';
        $language = 'en';
        $user_type = USER_NORMAL;
        $reg_time = !empty($details['reg_time']) ? $details['reg_time'] : time();

        $passwords_manager = $phpbb_container->get('passwords.manager');

        $user_row = array(
            'username'                => $details['username'],
            'user_password'            => $passwords_manager->hash($details['password']),
            'user_email'            => $details['email'],
            'group_id'                => (int) $group_id,
            'user_timezone'            => (float) $timezone,
            'user_lang'                => $language,
            'user_type'                => $user_type,
            'user_actkey'            => $user_actkey,
            'user_regdate'            => $reg_time,
        );

        // Register user...
        return user_add($user_row);
    }

    /*
     * Update a forum user
     *
     * @param array containing fields to update
     *
     * @return bool
     * */
    public function forum_update($details) {
        global $db, $phpbb_container;

        $fields_allowed = array('username', 'user_email', 'user_password', 'user_regdate', 'group_id');
        $passwords_manager = $phpbb_container->get('passwords.manager');
        $set = array();
        $result = false;

        foreach ($details as $field => $value) {
            if(in_array($field, $fields_allowed) && $field != 'user_id') {
                if($field == 'password') {
                    $set[] = $field . " = '" . $passwords_manager->hash($value) . "'";
                } else if(gettype($value) == 'integer') {
                    $set[] = $field . " = " . $db->sql_escape($value) . "";
                } else {
                    $set[] = $field . " = '" . $db->sql_escape($value) . "'";
                }
            }
        }

        if(!empty($set) && !empty($details['user_id'])) {
            $sql = 'UPDATE ' . USERS_TABLE . " SET ".implode(', ', $set)."
            WHERE user_id = " . $db->sql_escape($details['user_id']) . " LIMIT 1";
            $result = $db->sql_query($sql);
        }

        return $result;

    }

    /*
     * Log the user into the forum
     *
     * @param user array, password
     *
     * @return bool
     * */
    public function forum_login($user, $password) {
        global $auth, $db, $phpbb_container;

        $passwords_manager = $phpbb_container->get('passwords.manager');

        // do they have a forum account?
        if(empty($user['phpbbId'])) {

            // this is an old user account without an associated forum account, so create one...
            $forumId = $this->forum_add([
                'username' => $this->generateForumUsername($user['id']),
                'email' => $user['email'],
                'password' => $password,
                'reg_time' => strtotime($user['created'])
            ]);

            // store their forum user id in the main user table for cross reference
            $this->Users->query()->update()->set(['phpbbId' => $forumId])->where(['id' => $user['id']])->execute();

            $user['phpbbId'] = $user['id'];

        }

        // reset password for forum in case it has been changed since last login (we use main users system to authenticate, not the forum)
        $sql = 'UPDATE ' . USERS_TABLE . ' SET user_password = \'' . $passwords_manager->hash($password) . '\'
			WHERE user_id = ' . $db->sql_escape($user['phpbbId']);
        $result = $db->sql_query($sql);

        // grab username so we can log the forum user in
        $sql = 'SELECT username
			FROM ' . USERS_TABLE . '
			WHERE user_id = ' . $db->sql_escape($user['phpbbId']);
        $result = $db->sql_query($sql);
        $row = $db->sql_fetchrow($result);
        $db->sql_freeresult($result);

        return $auth->login($row['username'], $password, false);

    }

    /**
     * @return Response|null
     * @throws \Exception General exception.
     */
    public function unsubscribe(string $email = '')
    {
        $this->allowMethod([RequestType::GET, RequestType::POST, RequestType::PATCH]);

        $this->setResponseType(ResponseType::NONE);
        $this->viewBuilder()->setTemplate('unsubscribe');

        if ($this->request->is(RequestType::GET))
		{
            $email = trim($this->Data->get('email'));

            if (!empty($email) && Validation::email($email))
			{
				$this->EmailQueueMembers->deleteAll(
					['email' => $email]
				);
            }

		}

    }

    /**
     * @param string $token Password reset token.
     *
     * @return Response|null
     * @throws \Exception General exception.
     */
    public function password(string $token = '')
    {
        $this->allowMethod([RequestType::GET, RequestType::POST, RequestType::PATCH]);

        if ($this->request->is(RequestType::GET)) {
            $this->setResponseType(ResponseType::NONE);

            if ($this->Auth->isAuthenticated()) {
                $this->redirect($this->Auth->redirectUrl());
            }

            if (!empty($token)) {
                $token = $this->UserTokens->find('all', [
                    'conditions' => [
                        'token' => $token,
                        'used IS' => null,
                        'TIME_TO_SEC(TIMEDIFF(NOW(), created)) <' => 1800 // 30 min
                    ]
                ])->first();

                $this->set('token', $token ? $token->get('token') : '');
                $this->viewBuilder()->setTemplate('reset-password');
            } else {
                $this->viewBuilder()->setTemplate('forgot-password');
            }
        } elseif ($this->request->is(RequestType::POST)) {
            $email = trim($this->Data->get('email'));
            $host = $this->request->host();

            if (!Validation::email($email)) {
                return $this->asMessageResponse(false, __('Please enter a valid e-mail address'));
            }

            $user = $this->Users->find('all', [
                'conditions' => [
                    'email' => $email,
                    'group_id' => Group::MEMBERS
                ]
            ])->first();

            if (!$user) {
                return $this->asMessageResponse(false, __('Cannot find provided e-mail address'));
            }

            $token = $this->randToken();

            if (!$this->UserTokens->save($this->UserTokens->newEntity(['user_id' => $user->id, 'token' => $token]))) {
                return $this->asMessageResponse(false, __('Upss! Something goes wrong'));
            }

            $this->getMailer(PasswordMailer::class)->send('token', [$user, $token, $host]);

            return $this->asMessageResponse(true, "Check your inbox. We've sent you an email with a link to reset your password.");
        } else {
            $data = $this->Data->all();

            $token = $this->UserTokens->find('all', [
                'conditions' => [
                    'token' => $data['token'],
                    'used IS' => null,
                    'TIME_TO_SEC(TIMEDIFF(NOW(), created)) <' => 2700 // 30 min + 15 min handicap
                ]
            ])->first();

            if (!$token) {
                return $this->asMessageResponse(false, __('Sorry, but your reset password token is invalid or has expired.'));
            }

            $user = $this->Users->get($token->get('user_id'));
            $user->set('password', $data['password']);
            $user->set('confirm_password', $data['confirm_password']);

            if ($this->Users->save($user)) {
                $token->set('used', new \DateTime());
                $this->UserTokens->save($token);
                return $this->asMessageResponse(true, 'Your new password has been successfully updated.');
            }

            return $this->asMessageResponse(false, 'Please check your data and try again');
        }
    }

    /**
     * @return mixed
     */
    public function randToken()
    {
        return str_replace('-', '', Text::uuid());
    }

    /**
     * @return void
     * @throws \Exception General exception.
     */
    public function register(): void
    {
        $this->allowMethod([RequestType::GET, RequestType::POST]);

        if ($this->request->is(RequestType::GET)) {
            $this->setResponseType(ResponseType::NONE);

            if ($this->Auth->isAuthenticated()) {
                $this->redirect($this->Auth->redirectUrl());
            }

            $this->viewBuilder()->setTemplate('register');
        } else {
            $data = $this->Data->all();

            if (!Validation::email($data['email'])) {
                $this->asMessage(false, __('Please enter a valid e-mail address'));
                return;
            }

            $result = $this->Users->getConnection()->transactional(function () use ($data) {

                $now = new Time();
                $expires = new Time();
                $expires->addYears(30);

                $user = new User([
                    'group_id' => Group::MEMBERS,
                    'image' => null,
                    'email' => $data['email'],
                    'password' => $data['password'],
                    'first_name' => $data['first_name'],
                    'last_name' => $data['last_name'],
                    'job_role' => 'Member',
                    'phone' => null,
                    'token' => null,
                    'token_authorized' => $now,
                    'token_expires' => $expires,
                    /**
                     * Email after registration is always not verified
                     */
                    'email_verified' => false,
                    /**
                     * Member account is auto approved
                     *
                     * @see LS-422
                     */
                    'active' => true
                ]);


                if ($this->Users->save($user, ['atomic' => false])) {

                    // add user as a forum user too
                    $forumId = $this->forum_add([
                        'username' => $this->generateForumUsername($user->get('id')),
                        'email' => $data['email'],
                        'password' => $data['password'],
                    ]);

                    // store their forum user id in the main user table for cross reference
                    $this->Users->query()->update()->set(['phpbbId' => $forumId])->where(['id' => $user->get('id')])->execute();

                    $location = strtolower($data['location']) === 'all' ? null : $data['locationText'];

                    $userDetails = new UserDetail([
                        'user_id' => $user->get('id'),
                        'company' => $data['company'],
                        'location' => $location,
                        'pref_buying' => $data['buying'],
                        'pref_selling' => $data['selling'],
                        'pref_professional' => $data['professional'],
                        'pref_insights' => $data['insights'],
                        'pref_insights_list' => implode(',', $data['insights_list'])
                    ]);

                    if ($this->UserDetails->save($userDetails, ['atomic' => false])) {
                        /*
                         * Auto-login disabled due to account approval needed first
                         *
                        $userAuth = $this->Auth->identify();
                        if ($userAuth) {
                            $this->Auth->setUser($userAuth);
                        }
                        */

                        $token = $this->randToken();
                        $host = $this->request->host();

                        $this->UserTokens->save($this->UserTokens->newEntity(['user_id' => $user->get('id'), 'token' => $token]));

                        /**
                         * Send welcome mail to user
                         */
                        $this->getMailer(AccountMailer::class)->send('register', [$user, $token, $host]);

                        return true;
                    }
                }

                return false;
            });


            $this->asMessage($result, $result ? __('Congratulations. Please now check your email to verify your email address. If you do not receive this email, please check your spam or junk folder.') : __('An unexpected error occurred. Please try again.'));
        }
    }

    /**
     * @return String
     */
    public function generateForumUsername($id)
    {
        $seed = '123456789' . (string)$id;
        $array = str_split($seed);
        shuffle($array);
        return 'Member' . implode($array);
    }

    /**
     * @return Response
     * @throws \Exception General exception.
     */
    public function login(): Response
    {
        $this->allowMethod(RequestType::POST);

        $login = $this->Data->get('email');
        $password = $this->Data->get('password');

        if (!Validation::email($login)) {
            return $this->asMessageResponse(false, __('Please enter a valid username'));
        }

        $user = $this->Auth->identify();

        if ($user) {
            if (!$user['active']) {
                return $this->asMessageResponse(false, __('Your account was disabled or not yet approved.'));
            }
            if (!$user['email_verified']) {
                return $this->asMessageResponse(false, __('Please verify your email address before sign-in.'));
            }
            $this->Auth->setUser($user);

            $this->forum_login($user, $password);

            return $this->asMessageResponse(true, __('You have successfully logged in. Redirecting ...'));
        }

        return $this->asMessageResponse(false, __('Incorrect login or password'));
    }

    /**
     * @return Response
     */
    public function logout(): Response
    {
        global $user;

        $user->session_kill();
        $user->session_begin();

        return $this->redirect($this->Auth->logout());
    }

    /**
     * @param string $token Email verification token.
     *
     * @return mixed
     * @throws \Exception Inherited exception.
     */
    public function verifyEmail(string $token = '')
    {
        $this->allowMethod([RequestType::GET, RequestType::POST]);

        if ($this->request->is(RequestType::GET)) {
            $this->setResponseType(ResponseType::NONE);
            $this->viewBuilder()->setTemplate('verify-email');

            if (strtolower($token) === 'start') {
                $this->set('resend', 'true');
                $this->set('message', '');
                return null;
            }

            $userToken = $this->UserTokens->find('all', [
                'conditions' => [
                    'token' => $token,
                    'used IS' => null
                ]
            ])->first();

            if (!$userToken) {
                $this->set('resend', 'true');
                $this->set('message', 'Your email verification token is invalid.');
                return null;
            }

            $user = $this->Users->get($userToken->get('user_id'));
            $user->set('email_verified', true);

            if ($this->Users->save($user)) {
                $userToken->set('used', new \DateTime());
                $this->UserTokens->save($userToken);
                $this->set('resend', 'false');
                $this->set('message', 'Your email address has been successfully verified. Thank you.');

                /**
                 * Send notification to staff
                 */
                $this->getMailer(AccountMailer::class)->send('adminNotification', [$user]);
            }
        }

        if ($this->request->is(RequestType::POST)) {
            $email = trim($this->Data->get('email'));
            $host = $this->request->host();

            if (!Validation::email($email)) {
                return $this->asMessageResponse(false, __('Please enter a valid e-mail address'));
            }

            $user = $this->Users->find('all', [
                'conditions' => [
                    'email' => $email,
                    'group_id' => Group::MEMBERS
                ]
            ])->first();

            if (!$user) {
                return $this->asMessageResponse(false, __('Cannot find provided e-mail address'));
            }

            $token = $this->randToken();

            if (!$this->UserTokens->save($this->UserTokens->newEntity(['user_id' => $user->id, 'token' => $token]))) {
                return $this->asMessageResponse(false, __('Upss! Something goes wrong'));
            }

            $this->getMailer(AccountMailer::class)->send('verify', [$user, $token, $host]);

            return $this->asMessageResponse(true, "Check your inbox. We've sent you an email with a link to verify your address.");
        }
    }

    /**
     * @return Response
     * @throws \Exception General exception.
     */
    public function validateEmail()
    {
        $this->allowMethod(RequestType::POST);

        $email = $this->Data->get('email');

        if (!Validation::email($email)) {
            return $this->asMessageResponse(false, __('Please enter a valid e-mail address'));
        }

        $result = !$this->Users->exists(['email' => $email]);

        return $this->asMessageResponse($result, $result === false ? __('This e-mail address is already taken') : '');
    }

    /**
     * @return mixed
     * @throws \Exception General exception.
     */
    public function image()
    {
        $this->allowMethod(RequestType::POST);

        $user = $this->Auth->getUser();

        $fileKey = 'image';

        $image = $this->Data->get($fileKey);

        if ($image['size'] > $this->maxFileSize) {
            return $this->asMessageResponse(false, __('Maximum allowed size is {0} bytes', $this->maxFileSize));
        }

        if (!in_array($image['type'], $this->mimeAllowed, true)) {
            return $this->asMessageResponse(false, __('File type not allowed. Acceptable types: JPEG, PNG, WEBP'));
        }

        $fileDir = MEMBERS_LOGO . $user->get('uid') . DS;
        if (!file_exists($fileDir) && !mkdir($fileDir, 0777, true) && !is_dir($fileDir)) {
            return $this->asMessageResponse(false, sprintf('Directory "%s" was not created', $fileDir));
        }

        $fileName = str_replace('.', '', uniqid($this->tempFilePrefix . $user->get('id') . '-', true)) . '.' . Mime::mimeToExtension($image['type']);
        $filePath = MEMBERS_LOGO . $user->get('uid') . DS . $fileName;

        $result = false;
        if (is_uploaded_file($image['tmp_name'])) {
            $result = move_uploaded_file($image['tmp_name'], $filePath);
        }

        return $this->asMessageResponse($result, $result ? __('File uploaded successfully.') : __('Upload failed. Unexpected server error occurred.'), [$fileKey => $fileName]);
    }

    /**
     * @return Response
     * @throws \Exception General exception.
     */
    public function update()
    {
        global $db, $phpbb_container;

        $this->allowMethod(RequestType::POST);

        $data = $this->Data->all();
        $authUser = $this->Auth->getUser();

        if ($data['id'] !== $authUser->id) {
            return $this->asMessageResponse(false, __('An unexpected error occurred. Please try again.'));
        }

        if (is_string($data['image']) && strpos($data['image'], $this->tempFilePrefix) === 0) {
            $destFileName = str_replace($this->tempFilePrefix, '', $data['image']);
            $data['image'] = rename(
                MEMBERS_LOGO . $authUser->get('uid') . DS . $data['image'],
                MEMBERS_LOGO . $authUser->get('uid') . DS . $destFileName
            ) ? $destFileName : null;
        }

        $user = $this->Users->get($data['id']);

        $errors = '';
        $result = $this->Users->getConnection()->transactional(function () use ($data, $user, &$errors) {

            if ($this->Users->hasBehavior('Upload')) {
                $this->Users->removeBehavior('Upload');
            }

            $user = $this->Users->patchEntity($user, $data);

            if ($user->hasErrors()) {
                $errors = $user->getErrorsMessages(null, BR);
                return false;
            }

            if ($this->Users->save($user, ['atomic' => false])) {
                if ($data['image'] !== null) {
                    $this->Users->setImageFor($user, $data['image']);
                }

                // reset email for forum user
                if(!empty($data['email'])) {
                    $this_user = $this->Users->get($data['id']);
                    $this->forum_update(['user_id' => $this_user['phpbbId'], 'user_email' => $data['email']]);
                }

                $details = $this->UserDetails->find()->where(['user_id' => $data['id']])->first();

                if (!$details) {
                    $details = $this->UserDetails->newEntity(['user_id' => $data['id']]);
                }

                $data['details']['use_billing_details'] = !$data['details']['use_billing_details'];

                if (!empty($data['details']['pref_insights_list']) && is_array($data['details']['pref_insights_list'])) {
                    $data['details']['pref_insights_list'] = implode(',', $data['details']['pref_insights_list']);
                }

                /**
                 * Change the profile completion flag to TRUE. That's mean the user finish
                 * the details update first time and can using all system parts.
                 */
                $data['details']['profile_completion'] = true;

                $details = $this->UserDetails->patchEntity($details, $data['details']);

                if ($details->hasErrors()) {
                    $errors = $details->getErrorsMessages(null, BR);
                    return false;
                }

                if ($this->UserDetails->save($details, ['atomic' => false])) {
                    // update business profile company name
                    if (!empty($data['details']['company'])) {
                        $service = $this->Services->find('all', [
                            'conditions' => [
                                'user_id' => $data['id']
                            ]
                        ])->first();

                        if ($service) {
                            $service->set('company', $data['details']['company']);
                            $service->set('slug', Text::slug($data['details']['company']));
                            $this->Services->save($service);
                        }
                    }

                    $billing = $this->UserBillingAddresses->find()->where([
                        'user_id' => $data['id'],
                        'is_default' => true
                    ])->first();

                    if (!$billing) {
                        $billing = $this->UserBillingAddresses->newEntity(['user_id' => $data['id'], 'is_default' => true]);
                    }

                    $billing = $this->UserBillingAddresses->patchEntity($billing, array_merge(
                        $data['billing_address'],
                        ['is_default' => true]
                    ));

                    if ($billing->hasErrors()) {
                        $errors = $billing->getErrorsMessages(null, BR);
                        return false;
                    }

                    if ($this->UserBillingAddresses->save($billing, ['atomic' => false])) {
                        return true;
                    }

                    return true;
                }
            }
            return false;
        });

        if ($result) {
            return $this->asMessageResponse(true, __('Your account details have been successfully updated'), [
                'image' => $data['image']
            ]);
        }

        $errors = $errors ?: __('An unexpected error occurred. Please try again.');
        return $this->asMessageResponse(false, $errors);
    }

    /**
     * Script to re-send verification emails to all unverified accounts
     */
    public function resendVerificationEmails(): void
    {
        set_time_limit(0);
        $this->setResponseType(ResponseType::NONE);

        $i = 0;
        $this->Users->find('all', [
            'conditions' => [
                'email_verified' => false,
                'group_id' => Group::MEMBERS
            ]
        ])->leftJoin(['ut' => 'user_tokens'], ['ut.user_id = Users.id AND ut.used IS null'])->select([
            'Users.id', 'first_name', 'email', 'token' => 'ut.token', 'used' => 'ut.used'
        ])->group('Users.id')->each(function ($user) use (&$i) {
            if ($user->token === null) {
                $user->token = $this->randToken();

                if (!$this->UserTokens->save($this->UserTokens->newEntity(['user_id' => $user->id, 'token' => $user->token]))) {
                    return false;
                }
            }
            $host = $this->request->host();
            if ($this->getMailer(AccountMailer::class)->send('verify', [$user, $user->token, $host])) {
                $i++;
            }
        });

        echo "$i verification emails were sent";
        exit;
    }
}
