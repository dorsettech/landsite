<?php

namespace App\Controller\Members;

use App\Model\Table\ChannelsTable;
use App\RequestType;
use App\Traits\JsonResponse;
use Cake\Http\Response;

/**
 * Channels Controller
 *
 * @property ChannelsTable $Channels
 * @package App\Controller\Members
 */
class ChannelsController extends AuthController
{
    use JsonResponse;

    public function initialize(): void
    {
        parent::initialize();

        $this->Auth->allow(['index']);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function index(): Response
    {
        $this->allowMethod(RequestType::GET);

        return $this->asListResponse($this->Channels->find('all', [
            'fields' => [
                'id', 'name', 'description', 'image', 'active'
            ],
            'order' => [
                'position' => 'ASC',
                'name' => 'ASC'
            ]
        ]));
    }
}
