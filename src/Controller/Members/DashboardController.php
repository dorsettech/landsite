<?php

namespace App\Controller\Members;

use App\Model\Entity\Group;
use App\Model\Enum\ClickType;
use App\Model\Enum\DashboardType;
use App\Model\Table\PropertiesTable;
use App\Model\Table\ServiceClicksTable;
use App\Model\Table\ServicesTable;
use App\Model\Table\UserBillingAddressesTable;
use App\Model\Table\UserDetailsTable;
use App\RequestType;
use App\ResponseType;
use App\Settings;
use App\Traits\JsonResponse;
use Cake\Core\Configure;
use Cake\Http\Response;

/**
 * DashboardController
 *
 * @property UserDetailsTable          $UserDetails
 * @property UserBillingAddressesTable $UserBillingAddresses
 * @property PropertiesTable           $Properties
 * @property ServicesTable             $Services
 * @property ServiceClicksTable        $ServiceClicks
 * @description Dashboard module
 */
class DashboardController extends AuthController
{
    use JsonResponse;

    /**
     * @return void
     * @throws \Exception Inherited.
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadModel('UserDetails');
        $this->loadModel('UserBillingAddresses');
        $this->loadModel('Properties');
        $this->loadModel('Services');
        $this->loadModel('ServiceClicks');
    }

    /**
     * Dashboard view
     *
     * @return void
     */
    public function index(): void
    {
        $user = $this->Auth->getUser();

        if ($user->get('group_id') !== Group::MEMBERS) {
            if (in_array($user->get('group_id'), [
                Group::ROOT,
                Group::ADMINISTRATOR,
                Group::MODERATOR
            ], true)) {
                $this->redirect('//' . $this->request->domain() . '/' . Configure::read('Website.admin_prefix'));
            } else {
                $this->redirect('//' . $this->request->domain());
            }
            return;
        }

        $this->setResponseType(ResponseType::NONE);

        $this->set('config', [
            'user' => $this->getUser(),
            'settings' => $this->getSettings()
        ]);

        $this->viewBuilder()->setLayout('default');
    }

    /**
     * Redirect to the forum (For handling links from the frontend)
     *
     * @return void
     */
    public function forumredirect(): void
    {
        $this->redirect('//' . $this->request->host() . '/#/forum');
    }

    /**
     * @return array
     */
    private function getUser(): array
    {
        $user = $this->Auth->getUser();
        $details = $this->UserDetails->find()->where(['user_id' => $user->id])->first();
        $billing = $this->UserBillingAddresses->find()->where([
            'user_id' => $user->id,
            'is_default' => true
        ])->first();

        $hasService = $this->Services->query()->where([ 'user_id' => $user->id])->count() > 0;

        return [
            'id' => $user->id,
            'uid' => $user->uid,
            'email' => $user->get('email'),
            'phone' => $user->get('phone'),
            'image' => $user->getImageRaw(),
            'first_name' => $user->get('first_name'),
            'last_name' => $user->get('last_name'),
            'full_name' => $user->get('full_name'),
            'details' => $details ? [
                'profile_completion' => $details->get('profile_completion'),
                'company' => $details->get('company'),
                'address' => $details->get('address'),
                'postcode' => $details->get('postcode'),
                'use_billing_details' => !$details->get('use_billing_details'),
                'marketing_agreement_1' => $details->get('marketing_agreement_1'),
                'marketing_agreement_2' => $details->get('marketing_agreement_2'),
                'marketing_agreement_3' => $details->get('marketing_agreement_3'),
                'pref_insights_list' => is_string($details->get('pref_insights_list')) ? explode(',', $details->get('pref_insights_list')) : [],
                'dashboard_type' => $details->get('dashboard_type')
            ] : [
                'user_id' => $user->id
            ],
            'billing_address' => $billing ? [
                'first_name' => $billing->get('first_name'),
                'last_name' => $billing->get('last_name'),
                'email' => $billing->get('email'),
                'phone' => $billing->get('phone'),
                'company' => $billing->get('company'),
                'postcode' => $billing->get('postcode'),
                'address' => $billing->get('address')
            ] : [
                'user_id' => $user->id
            ],
            'hasService' => $hasService
        ];
    }

    /**
     * @return array
     */
    private function getSettings(): array
    {
        $stripe = Settings::getGroup('stripe');

        /*
         * Secrets only for backend side
         */
        unset($stripe['secret-webhook'], $stripe['secret-key']);

        return [
            'api' => [
                'crafty-click' => Settings::get('crafty-clicks.access-token'),
                'google-map' => Settings::get('google-api.maps'),
                'stripe' => $stripe
            ],
            // alias
            'currency' => [
                'symbol' => $stripe['currency-symbol'],
                'name' => $stripe['currency']
            ],
            'plans' => Settings::getGroup('plans'),
            'prices' => Settings::getGroup('prices'),
            'validity' => Settings::getGroup('validity')
        ];
    }

    /**
     * @return Response
     * @throws \Exception Inherited.
     */
    public function refresh(): Response
    {
        $this->allowMethod(RequestType::POST);

        return $this->asJsonResponse([
            'config' => [
                'user' => $this->getUser(),
                'settings' => $this->getSettings()
            ]
        ]);
    }

    /**
     * @return Response
     * @throws \Exception Inherited.
     */
    public function stats(): Response
    {
        $this->allowMethod(RequestType::GET);

        $type = $this->Data->get('type');
        $userId = $this->Auth->user('id');

        $counters = [];

        if ($type === DashboardType::SELLER || DashboardType::SELLER_PROFESSIONAL) {
            $query = $this->Properties->find();
            $counters['properties'] = $query->where(['user_id' => $userId])->count();
            $counters['properties_enquiries'] = $this->Properties->query()->select(['s' => $query->func()->sum('enquiries')])->where(['user_id' => $userId])->extract('s')->first();
            $counters['properties_views'] = $this->Properties->query()->select(['s' => $query->func()->sum('views')])->where(['user_id' => $userId])->extract('s')->first();
        }

        if ($type === DashboardType::PROFESSIONAL || DashboardType::SELLER_PROFESSIONAL) {
            $query = $this->Services->find();
            $counters['service_enquiries'] = $this->Services->query()->select(['s' => $query->func()->sum('enquiries')])->where(['user_id' => $userId])->extract('s')->first();
            $counters['service_views'] = $this->Services->query()->select(['s' => $query->func()->sum('views')])->where(['user_id' => $userId])->extract('s')->first();
            $serviceId = $this->Services->query()->select(['id'])->where(['user_id' => $userId])->extract('id')->first();
            $counters['service_website_clicks'] = $serviceId ? (int) $this->ServiceClicks->query()->select([
                'value' => 'COUNT(*)'
            ])->where([
                'service_id' => $serviceId,
                'element' => ClickType::WEBSITE_URL
            ])->extract('value')->first() : 0;
            $counters['business'] = $query->select(['id', 'slug'])->where(['user_id' => $userId])->first();
        }

        return $this->asJsonResponse($counters);
    }
}
