<?php

namespace App\Controller\Members;

use App\Model\Enum\ServiceStatus;
use App\Model\Table\AreasTable;
use App\Model\Table\ServicesTable;
use App\RequestType;
use App\Traits\JsonResponse;
use Cake\Http\Response;

/**
 * Areas Controller
 *
 * @property AreasTable $Areas
 * @package App\Controller\Members
 */
class AreasController extends AuthController
{
    use JsonResponse;

    public function initialize(): void
    {
        parent::initialize();

        $this->Auth->allow(['index']);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function index(): Response
    {
        $this->allowMethod(RequestType::GET);

        return $this->asListResponse($this->Areas->find('all', [
            'fields' => [
                'id', 'name'
            ],
            'order' => [
                'name' => 'ASC'
            ]
        ]));
    }
}
