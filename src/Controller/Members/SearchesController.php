<?php

namespace App\Controller\Members;

use App\Model\Table\PropertyAttributesTable;
use App\Model\Table\PropertySearchesTable;
use App\Model\Table\ServiceSearchesTable;
use App\RequestType;
use App\Traits\JsonResponse;
use Cake\Http\Response;

/**
 * Searches Controller
 *
 * @property PropertySearchesTable   $PropertySearches
 * @property PropertyAttributesTable $PropertyAttributes
 * @property ServiceSearchesTable    $ServiceSearches
 * @package App\Controller\Members
 */
class SearchesController extends AuthController
{
    use JsonResponse;

    /**
     * @return void
     * @throws \Exception General exception.
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadModel('PropertySearches');
        $this->loadModel('ServiceSearches');
        $this->loadModel('PropertyAttributes');
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $userId = $this->Auth->user('id');
        $perPage = $this->Data->asInt('per_page');
        $page = $this->Data->asInt('page');
        $sort = $this->Data->get('sort');
        $type = $this->Data->get('type');
        $modelClass = $this->getModelClass($type);
        $table = $this->{$modelClass};

        $conditions = [
            'user_id' => $userId
        ];

        if ($type === 'SERVICE') {
            $contain = [
                'Categories'
            ];
        } else {
            $attributes = $this->PropertyAttributes->find()->select(['id', 'name'])->combine('id', 'name')->toArray();
            $contain = [
                'PropertyTypes'
            ];
        }

        $total = $table->find('all', [
            'conditions' => $conditions
        ])->count();

        $order = [
            "$modelClass.created" => 'DESC'
        ];

        if (!empty($sort)) {
            [$f, $o] = explode('|', $sort);
            $order = [
                $f => $o
            ];
        }

        $records = $table->find('all', [
            'conditions' => $conditions,
            'contain' => $contain,
            'order' => $order
        ])->page($page, $perPage)->toArray();

        if ($type === 'PROPERTY') {
            foreach ($records as &$record) {
                $record['attrs'] = [];
                if (empty($record['attributes'])) {
                    continue;
                }
                $attrs = explode(',', $record['attributes']);
                foreach ($attrs as $attrId) {
                    $record['attrs'][] = [
                        'id' => $attrId,
                        'name' => $attributes[$attrId]
                    ];
                }
            }
        }

        return $this->asJsonResponse([
            'records' => $records,
            'pagination' => [
                'total' => $total,
                'per_page' => $perPage,
                'current_page' => $page,
                'last_page' => ceil($total / $perPage),
                'from' => ($page - 1) * $perPage,
                'to' => $page * $perPage
            ]
        ]);
    }

    /**
     *
     * @param string $type Model type.
     *
     * @return string
     */
    private function getModelClass(string $type): string
    {
        $modelClass = ucfirst(strtolower($type)) . 'Searches';
        $this->loadModel($modelClass);
        return $modelClass;
    }

    /**
     * @return Response
     * @throws \Exception General exception.
     */
    public function alerts(): Response
    {
        $this->allowMethod(RequestType::PUT);

        $userId = $this->Auth->user('id');
        $id = $this->Data->asInt('id');
        $alerts = $this->Data->get('alerts');
        $type = $this->Data->get('type');
        $modelClass = $this->getModelClass($type);
        $table = $this->{$modelClass};

        $entity = $table->find('all', [
            'conditions' => [
                'id' => $id
            ]
        ])->first();

        if (!$entity) {
            return $this->asMessageResponse(false, __('Record does not exists'));
        }

        if ($entity->get('user_id') !== $userId) {
            return $this->asMessageResponse(false, __('You do not have permission to this action'));
        }

        $entity->set('alerts', $alerts ? 1 : 0);
        $result = $table->save($entity);

        return $this->asMessageResponse($result, $result ? __('Your changes have been saved') : $entity->getErrorsMessages());
    }

    /**
     * @return Response
     * @throws \Exception General exception.
     */
    public function delete(): Response
    {
        $this->allowMethod(RequestType::DELETE);

        $user = $this->Auth->getUser();
        $id = $this->Data->asInt('id');
        $type = $this->Data->get('type');
        $modelClass = $this->getModelClass($type);
        $table = $this->{$modelClass};

        $entity = $table->find('all', [
            'conditions' => [
                'id' => $id
            ]
        ])->first();

        if (!$entity) {
            return $this->asMessageResponse(false, __('Record does not exists'));
        }

        if ($entity->get('user_id') !== $user->id) {
            return $this->asMessageResponse(false, __('Only owner can delete the selected record'));
        }

        $result = $table->delete($entity);

        return $this->asMessageResponse($result, $result ? __('Record successfully deleted') : $entity->getErrorsMessages());
    }
}
