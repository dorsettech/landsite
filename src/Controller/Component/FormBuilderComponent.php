<?php

/**
 * @author Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-06)
 * @version 1.0
 */

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Routing\Router;
use Cake\Utility\Hash;

/**
 * Class FormBuilderComponent
 * @todo psr-2 @dawid.katarzynski
 */
class FormBuilderComponent extends Component
{
    /**
     * App object
     * @var null
     */
    private $app = null;

    /**
     * Form group flag
     *
     * @var boolean
     */
    private $formGroup = false;

    /**
     * Init
     *
     * @param object $app App object.
     *
     * @return void
     */
    public function init($app)
    {
        $this->app = $app;
        $this->app->loadModel('Forms');
    }

    /**
     * Generate forms base on body name
     *
     * @param string|null $body Body name.
     *
     * @return string
     */
    public function generateForms($body = '')
    {
        return $this->getForms($body);
    }

    /**
     * Get all forms in body of document
     *
     * @param string|null $body Body content.
     * @return string
     */
    public function getForms($body = '')
    {
        $forms = $list = [];
        if (preg_match_all("#form\.([a-zA-Z0-9_-]+).field.([a-zA-Z0-9_-]*)#", $body, $fields)) {
            if (!empty($fields) && isset($fields[1], $fields[2])) {
                for ($i = 0; $i < count($fields[1]); $i++) {
                    if (isset($fields[1][$i], $fields[2][$i])) {
                        $list[$i] = [
                            'form' => $fields[1][$i],
                            'field' => $fields[2][$i]
                        ];
                    }
                }
            }
        }

        if (preg_match_all("#form\.([a-zA-Z0-9._-]+)#", $body, $fields)) {
            if (!empty($fields) && isset($fields[1])) {
                foreach ($fields[1] as $key => $formElement) {
                    $forms = Hash::insert($forms, $formElement);
                }
            }
        }

        return $this->replaceFields($forms, $body);
    }

    /**
     * Replace form fields
     *
     * @param array       $forms Available forms.
     * @param string|null $body  Body content to parse.
     *
     * @return string
     */
    public function replaceFields(array $forms = [], $body = '')
    {
        foreach ($forms as $formName => $fields) {
            $currentForm = $this->getCurrentForm($formName);
            if (is_array($fields)) {
                foreach ($fields as $name => $values) {
                    if (!empty($values) && is_array($values)) {
                        $body = $this->parseFields($values, $formName, $currentForm, $body);
                    } else {
                        $body = $this->parseOtherFields($name, $formName, $currentForm, $body);
                    }
                }
            }
        }

        return $body;
    }

    /**
     * Replaced start/end fields of the forms
     *
     * @param object|null $field    Field name.
     * @param string|null $formName Form name
     * @param array|false $form     Current form data.
     * @param string|null $body     Body.
     * @return string
     */
    public function parseOtherFields($field, $formName, $form, $body)
    {
        $replace = '';
        switch ($field) {
            case ('start') :
                if ($form && $form->active) {
                    $route = Router::url(['controller' => 'FormsData', 'action' => 'save',]);
                    $replace = '<form action="' . $route . '" method="post" class="' . $form->html_class . ' formBuilder" enctype="multipart/form-data">';
                    $replace .= '<input type="hidden" name="form_name" value="' . $form->name . '" />';
                }
                $body = str_replace('{form.' . $formName . '.start}', $replace, $body);
                break;
            case ('end') :
                if ($form && $form->active) {
                    $replace = '</form>';
                }
                $body = str_replace('{form.' . $formName . '.end}', $replace, $body);
                break;
        }
        return $body;
    }

    /**
     * Parses form fields
     *
     * @param array       $fields   Array of field names.
     * @param string|null $formName Form name.
     * @param array|false $form     Current form data.
     * @param string|null $body     Body.
     *
     * @return string
     */
    public function parseFields($fields, $formName, $form, $body)
    {
        foreach ($fields as $field => $value) {
            $replace = '';
            if ($form && $form->active) {
                $data = json_decode($form->data);
                if (property_exists($data, $field)) {
                    $replace = $this->generateField($data->{$field}, $field);
                }
            }
            $body = str_replace('{form.' . $formName . '.field.' . $field . '}', $replace, $body);
        }

        return $body;
    }

    /**
     * Get current form data
     *
     * @param string|null $form Form name.
     *
     * @return array|false
     */
    public function getCurrentForm($form)
    {
        $query = $this->app->Forms->find('all', [
            'conditions' => [
                'Forms.alias' => $form
            ]
        ]);

        return !empty($query->toArray()) ? $query->toArray()[0] : false;
    }

    /**
     * Generate field
     *
     * @param object|null $field Filed object.
     * @param string|null $name Name.
     *
     * @return string
     */
    public function generateField($field, $name)
    {
        $element = '';
        $id = $name . time();

        $field->label = trim($field->label);
        $emptyLabel = empty($field->label);
        $this->formGroup = !$emptyLabel;
        $addLabel = (!$emptyLabel) ? '<label for="' . $id . '"' . ($field->required === '1' ? ' class="required"' : '') . '>' . $field->label . "</label>" : '';

        switch ($field->type) {
            case 'text':
                $element .= $addLabel;
                $element .= "<input id='$id' value='$field->value' class='$field->class' placeholder='$field->placeholder' type='text' name='$name' " . ($field->required === '1' ? 'required="true"' : '') . " " . $this->generateValidator($field->validation, $field->rule) . "/>";
                break;
            case 'textarea':
                $element .= $addLabel;
                $element .= "<textarea id='$id' class='$field->class' placeholder='$field->placeholder' name='$name' " . ($field->required === '1' ? 'required="true"' : '') . " " . $this->generateValidator($field->validation, $field->rule) . ">$field->value</textarea>";
                break;
            case 'checkbox':
                $element .= '<label for="' . $id . '">';
                $element .= "<input id='$id' value='$field->value' type='checkbox' name='$name' " . ($field->required === '1' ? 'required="true"' : '') . " />";
                $element .= !$emptyLabel ? $field->label : '';
                $element .= '</label>';
                $this->formGroup = false;
                break;
            case 'radio':
                if (isset($field->options) && !empty($field->options)) {
                    foreach ($field->options as $value => $label) {
                        $element .= '<label for="' . $id . '">';
                        $element .= "<input id='$id' value='$value' type='radio' name='$name' " . ($field->required === '1' ? 'required="true"' : '') . " />";
                        $element .= trim($label !== '') ? $label : '' . '</label>';
                        $element .= '</label>';
                    }
                }
                $this->formGroup = false;
                break;
            case 'select':
                $multiple = '';
                if (isset($field->multiple) && $field->multiple) {
                    $multiple = 'multiple';
                }
                $element .= $addLabel;
                $element .= "<select $multiple class='$field->class' name='$name' " . ($field->required === '1' ? 'required="true"' : '') . ">";
                if (isset($field->placeholder) && !empty($field->placeholder)) {
                    $element .= "<option selected disabled>$field->placeholder</option>";
                }
                if (isset($field->options) && !empty($field->options)) {
                    foreach ($field->options as $value => $name) {
                        $element .= "<option value='$value'>$name</option>";
                    }
                }
                $element .= '</select>';
                break;
            case ($field->type === 'file_document' || $field->type === 'image'):
                $element .= $addLabel;
                $element .= "<input id='$id' type='file' name='" . $field->type . "_" . $name . "' " . ($field->required === '1' ? 'required="true"' : '') . " />";
                break;
            case 'number':
                $element .= $addLabel;
                $element .= "<input id='$id' value='$field->value' class='$field->class' placeholder='$field->placeholder' type='number' name='$name' " . ($field->required === '1' ? 'required="true"' : '') . " " . $this->generateValidator($field->validation, $field->rule) . "/>";
                break;
            case 'email':
                $element .= $addLabel;
                $element .= "<input id='$id' value='$field->value' class='$field->class' placeholder='$field->placeholder' type='email' name='$name' " . ($field->required === '1' ? 'required="true"' : '') . " " . $this->generateValidator($field->validation, $field->rule) . "/>";
                break;
            case ($field->type === 'date' || $field->type === 'dateTime'):
                $element .= $addLabel;
                $element .= "<input id='$id' format='yyyy-mm-dd' value='$field->value' class='$field->class datepicker' placeholder='$field->placeholder' type='text' name='$name' " . ($field->required === '1' ? 'required="true"' : '') . " " . $this->generateValidator($field->validation, $field->rule) . "/>";
                break;
            case 'submit':
                $element .= "<button class='$field->class' type='submit'><span class='formBuilderLoading'></span>$field->label</button>";
                $this->formGroup = false;
                break;
        }

        if ($this->formGroup) {
            $element = $this->addFormGroupToElement($element);
        }

        return $element;
    }

    /**
     * Add form group to element
     *
     * @param string|null $element Element.
     *
     * @return string
     */
    public function addFormGroupToElement($element = '')
    {
        return '<div class="form-group">' . $element . '</div>';
    }

    /**
     * Generate validator
     *
     * @param string|null $validation Validation.
     * @param mixed $rule Rule type.
     *
     * @return string
     */
    public function generateValidator($validation, $rule)
    {
        $validator = '';
        switch ($validation) {
            case 'max_length':
                $length = intval($rule);
                $rule = is_numeric($length) ? $length : false;
                if ($rule) {
                    $validator = "maxlength='$rule'";
                }
                break;
            case 'min_length':
                $length = intval($rule);
                $rule = is_numeric($length) ? $length : false;
                if ($rule) {
                    $validator = "minlength='$rule'";
                }
                break;
            case 'max_min_length':
                $length = explode('/', $rule);
                $min = isset($length[0]) ? intval($length[0]) : false;
                $max = isset($length[1]) ? intval($length[1]) : false;
                if (is_numeric($min) && is_numeric($max)) {
                    $validator = "minlength='$min' maxlength='$max'";
                }
                break;
            case 'decimal':
                $validator = "pattern='$rule'";
                break;
            case 'phone':
                $validator = "pattern='$rule'";
                break;
        }

        return $validator;
    }
}
