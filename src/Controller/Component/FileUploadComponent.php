<?php

/**
 * @author Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-06)
 * @version 1.0
 */

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Filesystem\File;
use Cake\ORM\TableRegistry;
use upload;

/**
 * Class FileUploadComponent
 */
class FileUploadComponent extends Component
{
    /**
     * Components list
     *
     * @var array
     */
    public $components = ['Flash'];

    /**
     * upload object
     *
     * @var null
     */
    private $upload = null;

    /**
     * Default config
     *
     * @var array
     */
    protected $_defaultConfig = [
        'mime' => 'image/*',
        'name' => '',
        'directory' => 'media',
        'max_width' => 2000,
        'max_height' => 2000,
        'max_file_size' => 8388608
    ];

    /**
     * Initialize
     *
     * @param array $config Additional config.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setConfigFromDb();
    }

    /**
     * Upload method
     *
     * @param object $file File.
     *
     * @return string
     */
    public function upload($file)
    {
        $this->setFileName($file);
        $this->upload = new upload($file);
        $this->upload->allowed = $this->_config['mime'];
        if ($this->upload->uploaded) {
            $this->setFileSettings();
            !file_exists('files/' . $this->_config['directory']) ? mkdir('files/' . $this->_config['directory'], 777, true) : '';
            $this->upload->process('files/' . $this->_config['directory'] . '/');
            if ($this->upload->processed) {
                return 'files/' . $this->_config['directory'] . '/' . $this->upload->file_dst_name;
            } else {
                return $this->Flash->error(__('Error while uploading file: {0}', $this->upload->error));
            }
        } else {
            return $this->Flash->error(__('Error while uploading file: {0}', $this->upload->error));
        }
    }

    /**
     * Set config from DB
     *
     * @return void
     */
    private function setConfigFromDb()
    {
        $maxWidth = $this->getDbConfigValue('image_max_width');
        $maxHeight = $this->getDbConfigValue('image_max_height');
        $maxSize = $this->getDbConfigValue('file_max_size');
        if (!empty($maxWidth)) {
            $this->_config['max_width'] = $maxWidth;
        }
        if (!empty($maxHeight)) {
            $this->_config['max_width'] = $maxHeight;
        }
        if (!empty($maxSize)) {
            $this->_config['max_file_size'] = $maxSize;
        }
    }

    /**
     * Get DB config value
     *
     * @param string|null $name Var name.
     * @return mixed
     */
    private function getDbConfigValue($name = '')
    {
        $staticContents = TableRegistry::getTableLocator()->get('StaticContents');
        return $staticContents->find('all', ['conditions' => ['var_name' => $name]])->select('value')->enableHydration(false)->first()['value'];
    }

    /**
     * Set file name
     *
     * @param array $file File config.
     *
     * @return void
     */
    public function setFileName(array $file = [])
    {
        if (isset($file['name']) && !empty($file['name'])) {
            $this->_config['name'] = md5($file['name'] . time());
        } else {
            $this->_config['name'] = md5(time());
        }
    }

    /**
     * Set file settings
     *
     * @return void
     */
    public function setFileSettings()
    {
        if ($this->upload->file_is_image) {
            if ($this->upload->image_src_x > $this->_config['max_width'] || $this->upload->image_src_y > $this->_config['max_height']) {
                $this->upload->image_resize = true;
                $this->upload->image_ratio = true;
                $this->upload->image_x = $this->_config['max_width'];
                $this->upload->image_y = $this->_config['max_height'];
            }
        }
        $this->upload->file_max_size = $this->_config['max_file_size'];
    }

    /**
     * Removes file
     *
     * @param string|null $filePath Path to file.
     * @return bool
     */
    public function removeFile($filePath)
    {
        $file = new File($filePath);

        return $file->delete();
    }
}
