<?php

namespace App\Controller\Component;

use App\Arrays;
use App\Strings;
use ArrayAccess;
use Cake\Controller\Component;

/**
 * Universal Request Data component.
 *
 * It is replacement for getters build in ServerRequest.
 * Provide universal methods to get data regardless of
 * request type.
 *
 * @package App\Controller\Component
 * @author  Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
class DataComponent extends Component implements ArrayAccess
{

    private $data;

    /**
     * @return string
     */
    public function __toString()
    {
        /** @noinspection ForgottenDebugOutputInspection */
        return (string)var_export($this->data);
    }

    /**
     * @param mixed $offset Offset.
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->_object[$offset]);
    }

    /**
     * @param mixed $offset Offset.
     * @param mixed $value  Value.
     *
     * @return boolean|void
     */
    public function offsetSet($offset, $value)
    {
        return $this->__set($offset, $value);
    }

    /**
     * @param mixed $offset Offset.
     *
     * @return boolean|mixed
     */
    public function offsetGet($offset)
    {
        return $this->__get($offset);
    }

    /**
     * @param mixed $key Key.
     *
     * @return boolean|mixed
     */
    public function __get($key)
    {
        if ($this->has($key)) {
            return $this->data[$key];
        }

        return false;
    }

    /**
     * @param mixed $key   Key.
     * @param mixed $value Value.
     *
     * @return boolean
     */
    public function __set($key, $value)
    {
        if (!$this->has($key)) {
            $this->data[$key] = $value;
            return true;
        }

        return false;
    }

    /**
     * @param mixed $key Key.
     *
     * @return boolean
     */
    public function has($key)
    {
        return isset($this->data[$key]);
    }

    /**
     * @param mixed $offset Offset.
     *
     * @return boolean|void
     */
    public function offsetUnset($offset)
    {
        return $this->__remove($offset);
    }

    /**
     * @param mixed $key Key.
     *
     * @return boolean
     */
    public function __remove($key)
    {
        if ($this->has($key)) {
            unset($this->data[$key]);
            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function all()
    {
        return $this->data;
    }

    /**
     * @param mixed $key   Key.
     * @param mixed $value Value.
     *
     * @return boolean
     */
    public function set($key, $value)
    {
        $result = $this->__set($key, $value);
        if ($result === false) {
            return Arrays::set($this->data, $key, $value);
        }

        return $result;
    }

    /**
     * @param mixed $key      Key.
     * @param mixed $newValue New value.
     *
     * @return boolean
     */
    public function overwrite($key, $newValue)
    {
        return $this->__overwrite($key, $newValue);
    }

    /**
     * @param mixed $key      Key.
     * @param mixed $newValue New value.
     *
     * @return boolean
     */
    public function __overwrite($key, $newValue)
    {
        if ($this->has($key)) {
            $this->data[$key] = $newValue;
            return true;
        }

        return false;
    }

    /**
     * @param mixed $key      Key.
     * @param mixed $newValue New value.
     *
     * @return boolean
     */
    public function force($key, $newValue)
    {
        if ($this->has($key)) {
            return $this->__overwrite($key, $newValue);
        }

        return $this->__set($key, $newValue);
    }

    /**
     * @param mixed $key Key.
     *
     * @return boolean
     */
    public function remove($key)
    {
        return $this->__remove($key);
    }

    /**
     * @param mixed $key Key.
     *
     * @return boolean|float|integer|mixed|string|null
     */
    public function asboolean($key)
    {
        return $this->toType($key, 'booleanean');
    }

    /**
     * @param mixed $key  Key.
     * @param mixed $type Value type.
     *
     * @return boolean|float|integer|mixed|string|null
     */
    private function toType($key, $type)
    {
        $value = $this->get($key);
        if (is_null($value)) {
            return $value;
        }

        switch ($type) {
            case 'booleanean':
                if (in_array(strtolower($value), ['true', 'false'])) {
                    return (strtolower($value) === 'true');
                }
                return (boolean)$value;
            case 'integer':
                return (int)$value;
            case 'float':
                return (float)trim(str_replace(',', '.', $value));
            case 'array':
                return Strings::jsonDecode($value, true);
            case 'object':
                return Strings::jsonDecode($value);
            case 'string':
            default:
                return (string)$value;
        }
    }

    /**
     * @param mixed $key     Key.
     * @param mixed $default Default value.
     *
     * @return boolean|mixed|null
     */
    public function get($key, $default = null)
    {
        $value = $this->__get($key);
        if ($value === false) {
            $value = Arrays::get($this->data, $key, $default);
        }

        return ($value === false) ? $default : $value;
    }

    /**
     * @param mixed $key Key.
     *
     * @return boolean|float|integer|mixed|string|null
     */
    public function asString($key)
    {
        return $this->toType($key, 'string');
    }

    /**
     * @param mixed $key Key.
     *
     * @return boolean|float|integer|mixed|string|null
     */
    public function asArray($key)
    {
        return $this->toType($key, 'array');
    }

    /**
     * @param mixed $key Key.
     *
     * @return boolean|float|integer|mixed|string|null
     */
    public function asObject($key)
    {
        return $this->toType($key, 'object');
    }

    /**
     * @param mixed $key Key.
     *
     * @return boolean|float|integer|mixed|string|null
     */
    public function asInt($key)
    {
        return $this->toType($key, 'integer');
    }

    /**
     * @param mixed $key Key.
     *
     * @return boolean|float|integer|mixed|string|null
     */
    public function asFloat($key)
    {
        return $this->toType($key, 'float');
    }

    /**
     * @param array $config Configuration array.
     *
     * @return void
     */
    public function initialize(array $config): void
    {
        $this->reinitialize();
    }

    /**
     * Reinitialization of the data.
     *
     * @return void
     */
    public function reinitialize(): void
    {
        $this->data = $this->getController()->request->getData();
        if (!$this->data) {
            $this->data = $this->getController()->request->getQueryParams();
        }
    }

    /**
     * @param mixed $callback Callback method.
     * @param mixed ...$args  Arguments.
     *
     * @return string
     */
    public function input($callback = null, ...$args)
    {
        switch ($callback) {
            case 'json':
                $callback = 'json_decode';
                break;
            case 'xml':
                $callback = 'Xml::build';
                break;
        }
        return $this->getController()->request->input($callback, $args);
    }
}
