<?php

namespace App\Controller\Component;

use App\Model\Entity\User;
use Cake\Event\Event;
use Cake\Http\Response;
use Cake\ORM\Locator\TableLocator;

/**
 * Authorization component.
 *
 * @package App\Controller\Component
 * @author Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
class AuthComponent extends \Cake\Controller\Component\AuthComponent
{

    /**
     * Returns the URL to redirect back to or / if not possible.
     *
     * This method takes the referrer into account if the
     * request is not of type GET.
     *
     * @return string
     */
    protected function _getUrlToRedirectBackTo()
    {
        $urlToRedirectBackTo = '//' .  $this->request->host() . $this->request->getRequestTarget();
        if (!$this->request->is('get')) {
            $urlToRedirectBackTo = $this->request->referer(true);
        }

        return $urlToRedirectBackTo;
    }

    /**
     * Required for CORS requests
     * @param Event $event
     * @return \Cake\Http\Response|null
     * @throws \ReflectionException
     */
    public function authCheck(Event $event): ?Response
    {
        if ($this->request->is('options')) {
            return null;
        }

        return parent::authCheck($event);
    }

    public function isAuthenticated(): bool
    {

        $user = $this->user();

        if (empty($user)) {
            return false;
        }

        if ($user['id'] === 0) {
            return false;
        }

        return true;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {

        $user = $this->user();

        if ($user === null) {
            return new User(['id' => 0]);
        }

        return (new TableLocator)->get('Users')->get($user['id']);
    }
}
