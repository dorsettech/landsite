<?php
/**
 * Based on orginal 'prosty javascriptowy sposob na zabezpieczenie formow przed automatycznymi robotami gdy nie ma
 * targetowanego ataku' AKA DragonCaptcha by Dragon <lukasz.dragan@econnect4u.pl>
 *
 * Installation:
 * Copy CaptchaComponent and CaptchaHelper to proper locations.
 *
 * Usage:
 * 0. Enabling Captcha
 *  - (default) Configuration: Project.captcha needs to be set to `true` or
 *  - `captcha` key with boolean value needs to be passed to configuration array when loading component
 * 1. Load this component in controller, it can be FrontController or AppController (depends of the usage)
 *  - CaptchaHelper will be loaded with component
 * 2. Check if captcha is valid before sending form by executing this method in Controller: $this->Captcha->isValid()
 * 3. There are two uses of this captcha - standard forms and FormBuilder (CMS4)
 * 3.1. Standard forms:
 * 3.1.1 Use `$this->Captcha->formStart()` instead of `this->Form->start()` - same variables applies
 * 3.1.2 Use `$this->Captcha->formEnd()` instead of `this->Form->end()` - it will add script to the `scriptBottom` (make
 *       sure that the script has been declared right before closing `body` tag: <?= $this->fetch('scriptBottom') ?>)
 * 3.2 FormBuilder:
 * 3.2.1 Use `$this->Captcha->replaceStart('FormName')` instead of `{form.FormName.start}` - pass just the FormName
 * 3.2.2 Use `$this->Captcha->formEnd()` instead of `{form.FormName.end}` - it will add script to the `scriptBottom`
 *       (make sure that the script has been declared right before closing `body` tag: <?= $this->fetch('scriptBottom')
 *       ?>), it will also changes the onSubmit event from FormBuilder JS.
 *
 * @author Dzidek <marcin.nierobis@econnect4u.pl>
 * @date date(2018-05-07)
 * @version 1.3
 */
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;

/**
 * CaptchaComponent
 */
class CaptchaComponent extends Component
{
    /**
     * Additional helpers to load
     *
     * @var array
     */
    public $helpers = ['Captcha'];

    /**
     * Components list
     *
     * @var array
     */
    public $components = [];

    /**
     * Current controller
     *
     * @var object
     */
    protected $controller = null;

    /**
     * CaptchaOn
     *
     * @var boolean true (on)/false (off)
     */
    protected $captchaIsEnabled = true;

    /**
     * Captcha variables array
     *
     * @var array
     */
    protected $captcha = [];

    /**
     * Captcha name
     *
     * @var string
     */
    protected $captchaName = null;

    /**
     * Captcha session prefix
     *
     * @var string
     */
    protected $sessionPrefix = 'hostumx_';

    /**
     * Initialize
     *
     * @param array $config Config array.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $isEnabled = isset($config['captcha']) ? $config['captcha'] : Configure::read('Project.captcha');

        $this->setCaptcha($isEnabled);
        $this->setController();
        $this->setCaptchaName();
        $this->setVariables();
    }

    /**
     * Before render event
     *
     * @param Event $event Cake event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        $this->setSessionValue($this->captcha['value']);
    }

    /**
     * Set controller
     *
     * @return void
     */
    protected function setController()
    {
        $this->controller = $this->getController();
    }

    /**
     * Set captcha
     *
     * @param boolean $boolean Captcha value.
     * @return void
     */
    protected function setCaptcha($boolean)
    {
        $this->captchaIsEnabled = $boolean;
    }

    /**
     * Set captcha name
     * @return void
     */
    protected function setCaptchaName()
    {
        if ($this->captchaIsEnabled) {
            $oldCaptchaName = $this->request->session()->read('Captcha.recentName');
            $captchaName    = str_replace('/', '-', substr($this->request->getRequestTarget(), 1));

            if ($captchaName != $oldCaptchaName && !empty($this->request->getData())) {
                $captchaName = $oldCaptchaName;
            }

            $this->captchaName = $this->request->is('get') ? $captchaName : $oldCaptchaName;
        }
    }

    /**
     * Set captcha session value
     *
     * @param string $value String to compare.
     * @return void
     */
    protected function setSessionValue($value)
    {
        if (!$this->request->is('ajax')) {
            if ($this->captchaName !== '404') {
                $session = $this->request->session();
                $session->write('Captcha.recentName', $this->captchaName);
                $session->write('Captcha.' . $this->sessionPrefix . $this->captchaName, $value);
            }
        }
    }

    /**
     * Get captcha session value
     * @return type
     */
    public function getSessionValue()
    {
        return $this->request->session()->read('Captcha.' . $this->sessionPrefix . $this->captchaName);
    }

    /**
     * Is enabled
     *
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->captchaIsEnabled;
    }

    /**
     * Set captcha variables
     * @return void Sets variables to view.
     */
    protected function setVariables()
    {
        if ($this->captchaIsEnabled) {
            $this->captcha = [
                'name'  => sha1(date('dDjzmYaH') . 's6A2wgVa5eS11hrZ56e' . $this->request->host() . $this->captchaName),
                'value' => sha1(date('dDjzmYaH') . '2A53qZy20q45Yzy74yLq' . $this->request->host() . $this->captchaName),
                'form'  => 'x' . md5(rand() . '4a53c28Za09aXf'),
                'var1'  => 'x' . md5(rand() . 'af2dgaa9s'),
                'var2'  => 'x' . md5(rand() . '62dgaaX9s'),
                'var3'  => 'x' . md5(rand() . 'af2d8ca9s'),
                'var4'  => 'x' . md5(rand() . 'af8dfaO9s'),
                'var5'  => 'x' . md5(rand() . 'asddfA2h0'),
            ];
        }

        $formsModel = TableRegistry::get('Forms');
        $forms      = [];
        if ($formsModel instanceof \App\Model\Table\FormsTable) {
            $formsNames = Hash::combine($formsModel->find('all')->toArray(), '{n}.name', '{n}');
            $formsAliases = Hash::combine($formsModel->find('all')->toArray(), '{n}.alias', '{n}');
            $forms = array_merge($formsNames, $formsAliases);
        }

        $this->controller->set('captcha', $this->captcha);
        $this->controller->set('forms', $forms);
    }

    /**
     * Is valid
     * Checks if sent data matching generated strings
     *
     * @return boolean
     */
    public function isValid()
    {
        $isValid = true;

        if ($this->captchaIsEnabled) {
            $formData = $this->request->getData();

            $captchaSessionValue = $this->getSessionValue();

            if (empty($captchaSessionValue) || !isset($formData[$this->captcha['name']])) {
                $isValid = false;
            } else if ($this->captcha['value'] != $formData[$this->captcha['name']] || $captchaSessionValue != $formData[$this->captcha['name']]) {
                $isValid = false;
            }
        }

        return $isValid;
    }
}
