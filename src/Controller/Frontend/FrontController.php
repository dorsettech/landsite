<?php

/**
 * @author Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-06)
 * @version 1.0
 */

namespace App\Controller\Frontend;

use App\Controller\AppController;
use App\View\Helper\UrlnameHelper;
use App\Model\Entity\Group;

/**
 * Class FrontController
 */
class FrontController extends AppController
{
    /**
     * Meta tag keys
     *
     * @var array
     */
    protected $metaTags = [
        'title' => '',
        'keywords' => '',
        'description' => ''
    ];

    /**
     * Helper list
     *
     * @var array
     */
    public $helpers = [
        'Images',
        'Urlname'
    ];

    /**
     * Components list
     *
     * @var array
     */
    public $components = [
        'FormBuilder'
    ];

    /**
     * Authorisation configuration
     *
     * @var array
     */
    protected $auth_config;

    /**
     * Page object
     *
     * @var null
     */
    private $page = null;

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        // Set the default config for authorisation.
        $this->auth_config = [
            'loginAction' => [
                'controller' => 'login',
                'action' => 'index',
                'plugin' => null,
                'prefix' => 'members'
            ],
            'logoutRedirect' => [
                'controller' => 'login',
                'action' => 'index',
                'plugin' => null,
                'prefix' => 'members'
            ],
            'authError' => false,
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ],
                    'userModel' => 'Users',
                    'finder' => [
                        'auth' => [
                            'group_id' => Group::MEMBERS
                        ]
                    ]
                ]
            ],
            'storage' => 'SessionEx',
            'checkAuthIn' => 'Controller.startup'
        ];

        $this->loadModel('Pages');
        $this->loadModel('PageFiles');
        $this->loadModel('Menus');
        $this->loadModel('Contents');
        $this->loadComponent('Flash');
        $this->loadComponent('Captcha', ['captcha' => true]);
        $this->fetchStaticContents();
        $this->setPageElements();
        $this->setMenus();
        $this->setBreadcrumbs();
        $this->setRecentInsights();
        $this->setRecentCaseStudies();
        $this->setChannels();

        // Check if authorised
        if(!$this->authCheck()) {
            // Hide this controller behind the member login
            $this->loadComponent('Auth', $this->auth_config);
        }

    }

    /**
     * Check if authorised
     *
     * @return boolean
     */
    public function authCheck() {

        // HACK We shouldn't really hard-code this
        if($this->getPage() != null && $this->getPage()->urlname == 'news-and-case-studies') {
            return false;
        }

        if(isset($this->allowPublic) && $this->allowPublic == false) {
            return false;
        }

        return true;

    }

    /**
     * Set menus
     *
     * @return void
     */
    public function setMenus()
    {
        $parsedMenus = [];
        $menus = $this->Menus->find('all')->where(['is_visible' => true])->order(['position' => 'ASC']);
        foreach ($menus as $menu) {
            $parsedMenus[$menu->urlname] = $this->Menus->getPages($menu->urlname, 'all');
        }
        $this->set('menus', $parsedMenus);
    }

    /**
     * Set page
     *
     * @param object|null $page Page object.
     *
     * @return void
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * Get page object
     *
     * @return object
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set page elements
     *
     * @return void
     */
    public function setPageElements()
    {
        $requestedUrl = explode('/', ltrim($this->getRequest()->getPath(), '/'));
        $page = $this->getRequest()->getAttribute('here') === '/' ? $this->Pages->getIndexPage() : $this->Pages->getSubpage($requestedUrl);
        if (!empty($page)) {
            $page = $this->PageFiles->parseFiles($page);
            $this->setPage($page);
            $this->setMetaTags($page);
            $contents = new \stdClass();
            if (!empty($page->contents)) {
                foreach ($page->contents as $content) {
                    $contents->{$content->content_name} = $content;
                }
            }
            $this->set(compact('page', 'contents'));
        }
    }

    /**
     *
     * Set meta tags
     *
     * @param object|null $data Page data.
     *
     * @return void
     */
    protected function setMetaTags($data)
    {
        $page = $this->getPage();

        if (!empty($data->seo_title)) {
            $meta_tags['title'] = $data->seo_title;
        } elseif (!empty($page->name)) {
            $meta_tags['title'] = $page->name;
        } else {
            $meta_tags['title'] = '';
        }
        if (!empty($data->og_title)) {
            $meta_tags['og_title'] = $data->og_title;
        } elseif (!empty($page->name)) {
            $meta_tags['og_title'] = $page->name;
        } else {
            $meta_tags['og_title'] = '';
        }
        if (!empty($data->seo_description)) {
            $meta_tags['description'] = $data->seo_description;
        } elseif (!empty($page->contents)) {
            $meta_tags['description'] = $this->getContentForSeo($page->contents, 'description');
        } else {
            $meta_tags['description'] = '';
        }
        if (!empty($data->og_description)) {
            $meta_tags['og_description'] = $data->og_description;
        } elseif (!empty($page->contents)) {
            $meta_tags['og_description'] = $this->getContentForSeo($page->contents, 'og_description');
        } else {
            $meta_tags['og_description'] = '';
        }
        if (!empty($data->og_image)) {
            $meta_tags['og_image'] = $data->og_image_path;
        } elseif (!empty($page->slider)) {
            $meta_tags['og_image'] = $this->getContentForSeo($page->slider, 'og_image');
        } else {
            $meta_tags['og_image'] = '';
        }
        $meta_tags['keywords'] = $data->seo_keywords;

        $this->set('metaTags', $meta_tags);
    }

    /**
     * Set breadcrumbs
     *
     * @return void
     */
    public function setBreadcrumbs()
    {
        if (isset($this->page->id)) {
            $i = 0;
            $len = count(explode('.', $this->page->ip));
            $breadcrumbs = [];
            foreach (explode('.', $this->page->ip) as $id) {
                $entity = $this->Pages->find('all')->where(['Pages.id' => $id, 'Pages.active' => 1])->first();
                if ($entity !== null && $entity->id !== 1) {
                    if ($i == $len - 1) {
                        $breadcrumbs[] = [
                            'name' => $entity->name,
                        ];
                    } else {
                        $breadcrumbs[] = [
                            'name' => $entity->name,
                            'link' => UrlnameHelper::generate($entity->urlname),
                        ];
                    }
                }
                $i++;
            }

            $this->set('breadcrumbs', $breadcrumbs);
        }
    }

    /**
     * Fetch static contents
     *
     * @return void
     */
    protected function fetchStaticContents()
    {
        $data = [];
        $this->loadModel('staticContents');
        $contents = $this->staticContents->find('all');
        if ($contents) {
            foreach ($contents as $content) {
                $data[$content->var_name] = $content->value;
            }
        }
        $this->set('statics', $data);
    }

    /**
     * Render content
     *
     * @param object|null $view   View instance.
     * @param string|null $layout Layout name.
     * @return Response
     */
    public function render($view = null, $layout = null)
    {
        $obj = parent::render();
        $body = $obj->getBody();
        $body->rewind();
        $this->response = $this->response->withStringBody($this->replaceFormBuilderTags($body->getContents()));

        return $this->response;
    }

    /**
     * Replace form builder tags
     *
     * @param string|null $body Body content.
     * @return mixed
     */
    public function replaceFormBuilderTags($body)
    {
        $this->FormBuilder->init($this);
        return $this->FormBuilder->generateForms($body);
    }

    /**
     * Set recent insights
     *
     * @return void
     */
    public function setRecentInsights()
    {
        $this->loadModel('Articles');
        $recentInsights = $this->Articles->findRecentInsights();
        $this->set(compact('recentInsights'));
    }

    /**
     * Set recent insights
     *
     * @return void
     */
    public function setChannels()
    {
        $this->loadModel('Channels');
        $channels = $this->Channels->findChannels(4);
        $this->set(compact('channels'));
    }

    /**
     * Set recent insights
     *
     * @return void
     */
    public function setRecentCaseStudies()
    {
        $this->loadModel('Articles');
        $recentCaseStudies = $this->Articles->findRecentCaseStudies();
        // debug($recentCaseStudies);
        // exit();
        $this->set(compact('recentCaseStudies'));
    }

    /**
     * Get content for seo
     *
     * @param array       $data  Array data to analyze.
     * @param string|null $field Field name.
     *
     * @return string
     */
    private function getContentForSeo(array $data = [], $field = '')
    {
        $return = '';
        if (!empty($data)) {
            if ($field == 'description' || $field == 'og_description') {
                foreach ($data as $key => $value) {
                    if ($value->content_name == 'main' || $key === 0) {
                        $tmp_value = strip_tags($value->content);
                        if (strpos($tmp_value, '.') > 0) {
                            $tmp_value = substr($tmp_value, 0, strpos($tmp_value, "."));
                        }
                        $return = $tmp_value;
                    }
                }
            } elseif ($field == 'og_image') {
                if (!empty($data[0]['path'])) {
                    $return = $data[0]['path'];
                }
            }
        }
        return $return;
    }

    /**
     * Json response
     *
     * @param array $data Data to return.
     *
     * @return Response
     */
    public function jsonResponse(array $data = [])
    {
        return $this->response->withType('application/json')
                              ->withStringBody(json_encode($data));
    }
}
