<?php
/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */

namespace App\Controller\Frontend;

use Cake\Http\Response;
use Cake\Utility\Inflector;

/**
 * SearchController
 */
class SearchController extends FrontController
{
    /**
     * Model
     * @var string
     */
    private $model  = '';

    /**
     * Default alerts value
     *
     * @var boolean
     */
    private $alerts = false;

    /**
     * Model fields to request data array keys mapping
     *
     * @var array
     */
    private $fieldMapping = [
        'user_id'     => 'user_id',
        'location'    => 'location',
        'purpose'     => 'purpose',
        'type_id'     => 'type',
        'min_price'   => 'min_price',
        'max_price'   => 'max_price',
        'keywords'    => 'keyword',
        'category_id' => 'service_category',
        'radius'      => 'range',
        'attributes'  => 'attributes',
        'alerts'      => 'alerts'
    ];

    /**
     * Save search results
     *
     * @return Response Redirects back to search results
     */
    public function save(): Response
    {
        $request = $this->getRequest();
        $request->allowMethod('post');

        $this->autoRender = false;

        $referer = $request->referer();
        $user    = $request->getSession()->read('Auth.User');
        $data    = $request->getData();

        if (!empty($user['id']) && empty($data['saved'])) {
            $data['user_id'] = $user['id'];
            $data['alerts']  = $this->alerts;
            $this->model     = ucfirst(Inflector::singularize($data['search'])) . 'Searches';

            $this->loadModel($this->model);
            $entity = $this->{$this->model}->newEntity($this->getEntityData($data));

            if ($this->{$this->model}->save($entity)) {
                $this->Flash->success('Your search has been saved to My Account - Alerts & Searches page.');
                $referer .= '&saved=1';
            }
        }

        return $this->redirect($referer);
    }

    /**
     * Get entity data
     *
     * @param array $data Request data.
     * @return array
     */
    private function getEntityData(array $data): ?array
    {
        $entityData = [];

        $schemaColumns = $this->{$this->model}->getSchema()->columns();

        foreach ($schemaColumns as $field) {
            if (isset($this->fieldMapping[$field])) {
                if (!empty($data[$this->fieldMapping[$field]]) && is_array($data[$this->fieldMapping[$field]])) {
                    foreach ($data[$this->fieldMapping[$field]] as $key => $value) {
                        if ($value == 0) {
                            unset($data[$this->fieldMapping[$field]][$key]);
                        }
                    }
                    $entityData[$field] = implode(',', array_keys($data[$this->fieldMapping[$field]]));
                } else {
                    $entityData[$field] = $data[$this->fieldMapping[$field]] ?? '';
                }
            }
        }

        return $entityData;
    }
}
