<?php

/**
 * @author Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-06)
 * @version 1.0
 */

namespace App\Controller\Frontend;

use Cake\Mailer\Email;

/**
 * Class FormsDataController
 */
class FormsDataController extends FrontController
{
    /**
     * Errors array data
     *
     * @var array
     */
    private $errors = [];

    /**
     * Get errors
     *
     * @return string
     */
    public function getErrors()
    {
        return join('<br />', $this->errors);
    }

    /**
     * Set errors
     *
     * @param string|null $errors Error value.
     *
     * @return void
     */
    public function setErrors($errors)
    {
        $this->errors[] = $errors;
    }

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('FormsData');
        $this->loadModel('Forms');
        $this->loadModel('FormDataFiles');
    }

    /**
     * Save method
     *
     * @return Response
     */
    public function save()
    {
        $request = $this->getRequest();
        if ($request->is('post') || $request->is('ajax')) {
            $data = $request->getData();
            $form = $this->getForm($data['form_name']);
            if (!$form) {
                return $this->jsonResponse(['message' => 'This form does not exist.', 'status' => 500]);
            }
            if (!$form->active) {
                return $this->jsonResponse(['message' => 'This form is currently inactive. Try again later.', 'status' => 500]);
            }

            $data = $this->generateData($form, $data);
            if ($request->is('ajax') && isset($data['error'])) {
                return $this->jsonResponse(['message' => $data['error'], 'status' => 500]);
            }
            $formsData = $this->FormsData->newEntity();
            $formsData = $this->FormsData->patchEntity($formsData, $data, ['associated' => 'FormDataFiles']);
            if ($this->Captcha->isValid() && $this->FormsData->save($formsData)) {
                if ($form->send_email) {
                    $this->sendEmail($form, $formsData);
                }
                if ($form->thank_you && isset($form->redirect_url) && trim($form->redirect_url) !== '') {
                    return $this->jsonResponse(['url' => $form->redirect_url, 'status' => 301]);
                }
                return $this->jsonResponse(['message' => 'Form has been sent', 'status' => 200]);
            } else {
                return $this->jsonResponse(['message' => 'Form hasn\'t been sent', 'status' => 500]);
            }
        }
    }

    /**
     * Generate data
     *
     * @param object|null $form    Form data.
     * @param array       $request Request data.
     *
     * @return array
     */
    public function generateData($form, array $request = [])
    {
        $patch = $data = [];
        $form_fields = json_decode($form->data);
        $patch['form_data_files'] = [];
        foreach ($form_fields as $key => $value) {
            $slug = !empty($value->label) ? $value->label : (!empty($value->placeholder) ? $value->placeholder : $key);
            switch ($value->type) {
                case 'file_document':
                    if (intval($value->required) === 1 && empty($request[$value->type . '_' . $key])) {
                        $this->setErrors(__('{0} is a mandatory field', $value->validation_text));
                    } else {
                        $file = $this->parseFile($request[$value->type . '_' . $key], 'document');
                        if ($file) {
                            $patch['form_data_files'][] = ['path' => $file, 'filename' => $request[$value->type . '_' . $key]['name']];
                        }
                    }
                    break;
                case 'image':
                    if (intval($value->required) === 1 && empty($request[$value->type . '_' . $key])) {
                        $this->setErrors(__('{0} is a mandatory field', $value->validation_text));
                    } else {
                        $file = $this->parseFile($request[$value->type . '_' . $key]);
                        if ($file) {
                            $patch['form_data_files'][] = ['path' => $file, 'filename' => $request[$value->type . '_' . $key]['name']];
                        }
                    }
                    break;
                case 'email':
                    $data[$slug] = isset($request[$key]) ? $request[$key] : '';
                    if (!filter_var($data[$slug], FILTER_VALIDATE_EMAIL)) {
                        $this->setErrors(__('{0} is not valid', $value->validation_text));
                    }
                    break;
                case 'checkbox':
                    if (isset($request[$key])) {
                        $data[$slug] = $request[$key];
                    }
                    break;
                case 'submit':
                    break;
                default:
                    if (intval($value->required) === 1 && empty($request[$key])) {
                        $this->setErrors(__('{0} is not valid', $value->validation_text));
                    } else {
                        $data[$slug] = isset($request[$key]) ? $request[$key] : '';
                    }
                    break;
            }
        }

        if (!empty($this->errors)) {
            return ['error' => $this->getErrors()];
        }

        $patch['id_forms'] = $form->id;
        $patch['data'] = $this->checkGDPRTickBox($data);
        $patch['ip'] = $this->request->clientIp();
        $patch['host'] = $this->request->host();
        $patch['referer'] = $this->referer();

        return $patch;
    }

    /**
     * Parse file
     *
     * @param object|array|null $file File object.
     * @param string|null       $type File type.
     *
     * @return boolean|string
     */
    public function parseFile($file, $type = '')
    {
        if (isset($file['name']) && !empty($file['name']) && !$file['error']) {
            $upload = new \upload($file);
            if ($type === 'document') {
                $upload->allowed = ['application/pdf', 'application/vnd.oasis.opendocument.text', 'application/msword'];
            } else {
                $upload->allowed = 'image/*';
            }
            $filename = md5(time());
            $upload->file_new_name_body = $filename;
            $upload->process(WWW_ROOT . '/files/forms/');
            if ($upload->processed) {
                return 'files/forms/' . $filename . '.' . $upload->file_dst_name_ext;
            } else {
                $this->setErrors(__('Error while trying to upload file: {0}', $upload->error));
            }
        }
        return false;
    }

    /**
     * Send email notification
     *
     * @param object|null $form      Form object.
     * @param object|null $formsData Forms data object.
     *
     * @return void
     */
    public function sendEmail($form, $formsData)
    {
        $formEmails = trim($form->emails) !== '' ? json_decode($form->emails) : false;
        if ($formEmails) {
            $data = json_decode($formsData->data);
            //add if you need
            //$attachments = $this->generateAttachments($formsData->files);
            $email = new Email('default');
            $email
                ->setSubject($form->name . ' from ' . $_SERVER['SERVER_NAME'])
                ->setEmailFormat('html')
                ->setTemplate('forms', 'forms')
                ->setViewVars(['data' => $data, 'form' => $form]);
            $email = $this->addRecipientsToEmail($formEmails, $email);
            $email = $this->addAttachmentsToEmail($email, $formsData['form_data_files']);
            $email->send();
        }
    }

    /**
     * Add eecipients to email
     *
     * @param object|null $emails Emails object.
     * @param Email       $email  Email object.
     *
     * @return Email
     */
    public function addRecipientsToEmail($emails, Email $email)
    {
        if (isset($emails->main) && is_array($emails->main) && !empty($emails->main)) {
            $email->setTo($emails->main);
        }
        if (isset($emails->cc) && is_array($emails->cc) && !empty($emails->cc)) {
            $email->setCc($emails->cc);
        }
        if (isset($emails->bcc) && is_array($emails->bcc) && !empty($emails->bcc)) {
            $email->setBcc($emails->bcc);
        }

        return $email;
    }

    /**
     * Add attachments to email
     *
     * @param Email $email Email object.
     * @param array $files Files.
     *
     * @return Email
     */
    public function addAttachmentsToEmail(Email $email, array $files = [])
    {
        if (!empty($files)) {
            foreach ($files as $file) {
                $email->addAttachments($file['path']);
            }
        }
        return $email;
    }

    /**
     * Get form
     *
     * @param string|null $name Form name.
     *
     * @return boolean|array
     */
    public function getForm($name = '')
    {
        $query = $this->Forms->find('all', [
            'conditions' => [
                'Forms.name' => $name
            ]
        ]);

        return !empty($query->toArray()) ? $query->toArray()[0] : false;
    }

    /**
     * Generate attachments
     *
     * @param array $files Files list.
     *
     * @return array
     */
    public function generateAttachments(array $files = [])
    {
        $attachments = [];
        if (isset($files) && !empty($files) && $files !== null) {
            foreach ($files as $file) {
                $attachments[$file->name] = $file->getFullPath();
            }
        }

        return $attachments;
    }


    /**
     * Check GDPR checkbox
     *
     * @param array $data Form data.
     *
     * @return string
     */
    public function checkGDPRTickBox(array $data)
    {
        foreach ($data as $fieldName => $value) {
            if (strpos($fieldName, 'Privacy') !== false) {
                unset($data[$fieldName]);
                $data['receive_news'] = 'Yes';
                return json_encode($data);
            } else {
                $data['receive_news'] = 'No';
            }
        }
        return json_encode($data);
    }
}
