<?php
/**
 * @author Marcin Nierobiś <marcin.nierobis@econnect4u.pl>
 * @date (2019-02-25)
 * @version 1.0
 */

namespace App\Controller\Frontend;

/**
 * Class ArticlesController
 */
class ArticlesController extends FrontController
{

    /**
     * Set whether publicly viewable or not
     */
    public $allowPublic = false;

    /**
     * Index Method - Articles listing
     *
     * @return void
     */
    public function index()
    {
        $this->setLayout('articles');
        $query      = $this->getRequest()->getQuery();
        $conditions = [];
        if (!empty($query['author'])) {
            $conditions = ['Articles.author_id' => $query['author']];
        }
        $articles = $this->Articles->findInsights($conditions);
        $this->paginate($articles, ['limit' => 12]);
        $this->set(compact('articles'));
    }

    /**
     * View method - Article individual page
     *
     * @param string $slug Article urlname.
     *
     * @return void|object
     */
    public function view(string $slug)
    {
        $this->setLayout('articles');
        $article = $this->Articles->findSpecifiedInsight($slug);
        if (empty($article)) {
            return $this->redirect('404', 404);
        }
        $breadcrumbs    = [
            0 => [
                'name' => 'Articles',
                'url' => 'articles'
            ],
            1 => [
                'name' => $article->title
            ]
        ];
        $this->setMetaTags($article);
        $relatedInsights = $this->Articles->findRelatedInsights($article);
        $this->set(compact('article', 'breadcrumbs', 'relatedInsights'));
    }
}
