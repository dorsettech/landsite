<?php
/**
 * @author Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @author Marcin Nierobiś <marcin.nierobis@econnect4u.pl>
 * @date (2019-06-27)
 * @version 1.0
 */

namespace App\Controller\Frontend;

use Cake\View\View;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

/**
 * Class SitemapController
 */
class SitemapController extends FrontController
{
    /**
     * Sitemap path
     *
     * @var string
     */
    private $sitemapPath = WWW_ROOT.'/sitemap.xml';

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadModel('Pages');
        $this->loadModel('Properties');
        $this->loadModel('Services');
        $this->loadModel('Articles');
        $this->loadModel('Users');
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        if ($this->getRequest()->getParam('_ext') === 'xml') {
            $this->viewBuilder()->setLayout(false);
            $this->response->withType('xml');
            if (file_exists($this->sitemapPath)) {
                readfile($this->sitemapPath);
                exit;
            }
            $this->set('xmlView', true);
        }
        $pages = $this->Pages->find('threaded', ['contain' => 'PageTypes'])->order(['position' => 'ASC'])->toArray();
        $this->set(compact('pages'));
    }
// Commented because the task no. LS-455.
//    /**
//     * Generate properties sitemap
//     *
//     * @return void
//     */
//    public function properties()
//    {
//        $items    = $this->Properties->findForSitemap();
//        $url      = 'properties';
//        $priority = '0.8';
//        $viewData = $this->renderContent($items, $url, $priority);
//        $this->viewBuilder()->setTemplate('view');
//        $this->set(compact('viewData'));
//    }
//
//   /**
//     * Generate services sitemap
//     *
//     * @return void
//     */
//    public function services()
//    {
//        $items    = $this->Services->findForSitemap();
//        $url      = 'professional-services';
//        $priority = '0.8';
//        $viewData = $this->renderContent($items, $url, $priority);
//        $this->viewBuilder()->setTemplate('view');
//        $this->set(compact('viewData'));
//    }
//
//   /**
//     * Generate articles sitemap
//     *
//     * @return void
//     */
//    public function articles()
//    {
//        $items    = $this->Articles->findArticlesForSitemap();
//        $url      = 'articles';
//        $priority = '0.7';
//        $viewData = $this->renderContent($items, $url, $priority);
//        $this->viewBuilder()->setTemplate('view');
//        $this->set(compact('viewData'));
//    }
//
//   /**
//     * Generate case studies sitemap
//     *
//     * @return void
//     */
//    public function caseStudies()
//    {
//        $items    = $this->Articles->findCaseStudiesForSitemap();
//        $url      = 'case-studies';
//        $priority = '0.7';
//        $viewData = $this->renderContent($items, $url, $priority);
//        $this->viewBuilder()->setTemplate('view');
//        $this->set(compact('viewData'));
//    }
//
//   /**
//     * Generate categories sitemap
//     *
//     * @return void
//     */
//    public function categories()
//    {
//        $items    = $this->Users->findMembersForSitemap();
//        $priority = '0.6';
//        $view     = new View();
//        $view->set(compact('items', 'priority'));
//        $view->setLayout(null);
//        $viewData = $view->render('Frontend/Sitemap/xml/categories', null);
//        $path     = WWW_ROOT.'files/sitemap/categories.xml';
//        $file     = new File($path, true);
//        $file->write($viewData);
//        $this->viewBuilder()->setTemplate('view');
//        $this->set(compact('viewData'));
//    }
//
//    /**
//     * Generate pages sitemap
//     *
//     * @return void
//     */
//    public function pages()
//    {
//        $items    = $this->Pages->find('threaded', ['contain' => 'PageTypes'])->order(['position' => 'ASC'])->toArray();
//        $view     = new View();
//        $view->set(compact('items'));
//        $view->setLayout(null);
//        $viewData = $view->render('Frontend/Sitemap/xml/pages', null);
//        $path     = WWW_ROOT.'files/sitemap/pages.xml';
//        $file     = new File($path, true);
//        $file->write($viewData);
//        $this->viewBuilder()->setTemplate('view');
//        $this->set(compact('viewData'));
//    }
//
//   /**
//     * Generate main sitemap
//     *
//     * @return void
//     */
//    public function mainSitemap()
//    {
//        $sitemapFolder = new Folder(WWW_ROOT.'files/sitemap');
//        $files         = $sitemapFolder->find('.*\.xml');
//        $view          = new View();
//        $view->set(compact('files'));
//        $view->setLayout(null);
//        $viewData      = $view->render('Frontend/Sitemap/xml/main', null);
//        $file          = new File($this->sitemapPath, true);
//        $file->write($viewData);
//        $this->viewBuilder()->setTemplate('view');
//        $this->set(compact('viewData'));
//    }
//
//   /**
//     * Render content for sitemap
//     *
//     * @param array  $items    Items array.
//     * @param string $url      Module url.
//     * @param string $priority Items priority.
//     *
//     * @return string
//     */
//    private function renderContent(array $items = [], string $url = '', string $priority = '')
//    {
//        $view     = new View();
//        $view->set(compact('items', 'url', 'priority'));
//        $view->setLayout(null);
//        $viewData = $view->render('Frontend/Sitemap/xml/sitemap', null);
//        $path     = WWW_ROOT.'files/sitemap/'.$url.'.xml';
//        $file     = new File($path, true);
//        $file->write($viewData);
//        return $viewData;
//    }
}
