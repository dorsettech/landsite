<?php
/**
 * @author Marcin Nierobiś <marcin.nierobis@econnect4u.pl>
 * @date (2019-02-25)
 * @version 1.0
 */

namespace App\Controller\Frontend;

use App\Model\Enum\ClickType;
use Cake\Mailer\MailerAwareTrait;

/**
 * Class ProfessionalServicesController
 */
class ProfessionalServicesController extends FrontController
{

    /**
     * MailerAware trait
     */
    use MailerAwareTrait;

    /**
     * Set whether publicly viewable or not
     */
    public $allowPublic = false;

    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {

        $this->loadModel('Services');
        $this->loadModel('Properties');
        $this->loadModel('Categories');
        $this->loadModel('Articles');
        $this->loadModel('UserSaved');
        $this->loadModel('ServiceEnquiries');
        $this->loadModel('ServiceClicks');

        parent::initialize();

    }

    /**
     * Index Method - Professional Services listing
     *
     * @return void
     */
    public function index()
    {
        $this->setLayout('professional-services');

        $data             = $this->getRequest()->getQuery();
        $services         = $this->Services->findServices($data);
        $services->mapReduce($this->Services->serviceMapper(), $this->Services->serviceReducer());
        $this->paginate($services, ['limit' => 12]);

        $services = $services->toArray();
        if (isset($services['all'])) {
            $services = $services['all'];
        }

        $recentProperties = $this->Properties->findRecentProperties($data)->toArray();

        $categoryNameForSeoTitle = $this->Categories->getCategoryNameForSeoTitle($data);

        $breadcrumbs    = [
            0 => [
                'name' => __('Search For Professional Services'),
                'url' => ['_name' => 'professional-services-listing']
            ]
        ];

        $this->set(compact('services', 'recentProperties', 'breadcrumbs', 'categoryNameForSeoTitle'));
    }

    /**
     * View method - Professional Service individual page
     *
     * @param string $urlname Professional Service urlname.
     *
     * @return object|void
     */
    public function view(string $urlname)
    {
        $this->setLayout('professional-services');
        $service = $this->Services->findSpecifiedService($urlname);

        if (empty($service)) {
            return $this->redirect(['action' => 'index']);
        }

        $this->sessionChecker($service->id);
        $isSaved = false;
        if (!empty($this->getRequest()->getSession()->read('Auth.User.id')) && $service->id) {
            $isSaved = $this->UserSaved->checkIsSaved($service->id, $this->getRequest()->getSession()->read('Auth.User.id'));
        }
        $service->case_studies = $this->Articles->findCaseStudiesByAuthor($service->user_id);
        $service->insights     = $this->Articles->findInsightsByAuthor($service->user_id);
        $otherServices         = $this->Services->findOtherServicesInCategory($service->category_id, $service->id);

        $this->set(compact('service', 'otherServices', 'isSaved'));
    }

    /**
     * Send Enquiry
     *
     * @return void|object
     */
    public function sendEnquiry()
    {
        if ($this->getRequest()->is('post')) {
            $data           = $this->getRequest()->getData();
            $serviceEnquiry = $this->ServiceEnquiries->newEntity($data);
            if ($this->Captcha->isValid() && $this->ServiceEnquiries->save($serviceEnquiry)) {
                $this->getMailer('Services')->send('businessEnquiry', [$data]);
                $this->Flash->success('Form has been sent.');
            } else {
                $this->Flash->error('Something went wrong.');
            }
            return $this->redirect($this->referer());
        }
    }

    /**
     * Save services as favourite
     *
     * @return object
     */
    public function save()
    {
        if ($this->getRequest()->is('post')) {
            $data   = $this->getRequest()->getData();
            $entity = $this->UserSaved->newEntity($data);
            if ($this->UserSaved->save($entity)) {
                return $this->jsonResponse(['message' => 'The business was added to your saved professionals list in My Account.', 'status' => 200]);
            } else {
                return $this->jsonResponse(['message' => 'Something went wrong.', 'status' => 500]);
            }
        }
    }

    /**
     * Check session and add record to ServiceViews.
     *
     * @param integer $serviceId Service ID.
     * @return void
     */
    private function sessionChecker(int $serviceId)
    {
        $this->loadModel('ServiceViews');
        $session = $this->getRequest()->getSession();
        if ($session->read('Service.' . $serviceId) === null) {
            $session->write('Service.' . $serviceId, $serviceId);
            $entity = $this->ServiceViews->newEntity(['service_id' => $serviceId]);
            $this->ServiceViews->save($entity);
        }
    }

    /**
     * Save clicks
     *
     * @return Response
     */
    public function saveClicks()
    {
        if ($this->getRequest()->is('post')) {
            $request = $this->getRequest()->getData();
            $data    = [
                'service_id' => $request['service_id'],
                'element'    => $request['element'] ?? ClickType::WEBSITE_URL,
            ];
            $entity  = $this->ServiceClicks->newEntity($data);
            if ($this->ServiceClicks->save($entity)) {
                return $this->jsonResponse(['status' => 200]);
            } else {
                return $this->jsonResponse(['status' => 500]);
            }
        }
    }
}
