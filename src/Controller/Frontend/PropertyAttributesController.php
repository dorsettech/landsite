<?php
/*
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */

namespace App\Controller\Frontend;

use App\Model\Table\PropertyAttributesTable;

/**
 * PropertyAttributesController
 *
 * @property PropertyAttributesTable $PropertyAttributes
 */
class PropertyAttributesController extends FrontController
{
    /**
     * Initialize
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
    }

    /**
     * Get by type
     *
     * @param integer|null $typeId Attribute type id.
     * @return void
     */
    public function getByType($typeId = null)
    {
        $this->getRequest()->allowMethod(['ajax', 'get']);

        $status  = AJAX_STATUS_FAIL;
        $message = '';

        $data = [];
        if (is_numeric($typeId)) {
            $data   = $this->PropertyAttributes->getByType($typeId);
            $status = AJAX_STATUS_OK;
        } elseif (empty($typeId)) {
            $status = AJAX_STATUS_OK;
        } else {
            $status  = AJAX_STATUS_ERROR;
            $message = __('An error occured while getting additional options');
        }

        $attributes = [
            'status'  => $status,
            'data'    => $data,
            'message' => $message
        ];

        $this->set('attributes', $attributes);
        $this->set('_serialize', 'attributes');
    }
}
