<?php

/**
 * @author Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-06)
 * @version 1.0
 */

namespace App\Controller\Frontend;

use Cake\Http\Exception\NotFoundException;

/**
 * Class PagesController
 */
class ChannelsController extends FrontController
{
    /**
     * Index Method - Articles listing
     *
     * @return void
     */
    public function index()
    {
        $this->setLayout('default');
        $channels = $this->Channels->findChannels();
        $this->paginate($channels, ['limit' => 16]);
        $this->set(compact('channels'));
    }

    /**
     * View method - Article individual page
     *
     * @param string $slug Article urlname.
     *
     * @return void|object
     */
    public function view(string $slug)
    {
        $this->setLayout('default');
        $channel = $this->Channels->findSpecifiedChannel($slug);

        if (empty($channel)) {
            throw new NotFoundException();
        }
        $this->setMetaTags($channel);

        $this->set(compact('channel'));

        $this->loadModel('Articles');
        $casestudies = $this->Articles->findRecentCaseStudies(10, $channel->id);
        $this->set('caseStudies', $casestudies);

        $insights = $this->Articles->findRecentInsights(10, $channel->id);
        $this->set('insights', $insights);

    }
}
