<?php

/**
 * @author Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-06)
 * @version 1.0
 */

namespace App\Controller\Frontend;

use Cake\Http\Exception\NotFoundException;

/**
 * Class PagesController
 */
class PagesController extends FrontController
{
    /**
     * Index Method
     *
     * @return void
     */
    public function index()
    {
        $this->loadModel('Categories');
        $categories = $this->Categories->find('all', [
            'order' => ['position' => 'ASC']
        ]);

        $this->set('categories', $categories);
    }

    /**
     * View method
     *
     * @return void
     * @throws NotFoundException NotFoundException.
     */
    public function view()
    {
        $page = $this->getPage();
        if (empty($page) && !$this->getRedirect()) {
            throw new NotFoundException();
        }
        if (!empty($page->redirection)) {
            $this->redirect($page->redirection, 301);
        }
        $this->viewBuilder()->setTemplate($page->layout);
    }
}
