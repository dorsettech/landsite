<?php
/**
 * @author Marcin Nierobiś <marcin.nierobis@econnect4u.pl>
 * @date (2019-02-25)
 * @version 1.0
 */

namespace App\Controller\Frontend;

/**
 * Class CaseStudiesController
 */
class CaseStudiesController extends FrontController
{

    /**
     * Set whether publicly viewable or not
     */
    public $allowPublic = false;

    /**
     * Index Method - Case Studies listing
     *
     * @return void
     */
    public function index()
    {
        $this->loadModel('Articles');
        $this->setLayout('case-studies');
        $query = $this->getRequest()->getQuery();
        $conditions = [];
        if (!empty($query['author'])) {
            $conditions = ['Articles.author_id' => $query['author']];
        }
        $caseStudies = $this->Articles->findCaseStudies($conditions);
        $this->paginate($caseStudies, ['limit' => 12]);
        $this->set(compact('caseStudies'));
    }

    /**
     * View method - Case Study individual page
     *
     * @param string $slug Case Study urlname.
     *
     * @return void|object
     */
    public function view(string $slug)
    {
        $this->loadModel('Articles');
        $this->setLayout('case-studies');
        $caseStudy = $this->Articles->findSpecifiedCaseStudy($slug);
        if (empty($caseStudy)) {
            return $this->redirect('404', 404);
        }
        $breadcrumbs     = [
            0 => [
                'name' => 'Case Studies',
                'url' => 'case-studies'
            ],
            1 => [
                'name' => $caseStudy->title
            ]
        ];
        $this->setMetaTags($caseStudy);
        $relatedInsights = $this->Articles->findRelatedInsights($caseStudy);
        $this->set(compact('caseStudy', 'breadcrumbs', 'relatedInsights'));
    }
}
