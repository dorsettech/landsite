<?php
/**
 * @author  Marcin Nierobiś <marcin.nierobis@econnect4u.pl>
 * @date (2019-02-26)
 * @version 1.0
 */

namespace App\Controller\Frontend;

use App\Model\Table\CategoriesTable;
use App\Model\Table\PropertiesTable;
use App\Model\Table\PropertyAttributesTable;
use Cake\Mailer\MailerAwareTrait;

/**
 * PropertiesController
 *
 * @property CategoriesTable         $CategoriesTable
 * @property PropertiesTable         $Properties
 * @property PropertyAttributesTable $PropertyAttributes
 */
class PropertiesController extends FrontController
{

    /**
     * MailerAware trait
     */
    use MailerAwareTrait;

    /**
     * Set whether publicly viewable or not
     */
    public $allowPublic = false;

    /**
     * Index Method - Properties listing
     *
     * @return void
     */
    public function index($serviceId = '', $serviceSlug = '')
    {
        $this->setLayout('properties');

        $breadcrumbs = [
            0 => [
                'name' => __('Search for Properties'),
                'url' => ['_name' => 'properties-listing']
            ]
        ];

        $data = $this->getRequest()->getQuery();

        $service = null;
        if (!empty($serviceId) || !empty($serviceSlug)) {
            $this->loadModel('Services');
            $service = $this->Services->getByIdAndSlug($serviceId, $serviceSlug);

            $data['user_id'] = $service->user_id;

            $breadcrumbs = [
                0 => [
                    'name' => __('Properties uploaded by {0}', h($service->company)),
                    'url' => ['_name' => 'properties-listing']
                ]
            ];
        }

        $properties = $this->Properties->findProperties($data);
        $this->paginate($properties, ['limit' => 12]);

        $this->loadModel('Categories');
        $servicesGroupedByCategories = $this->Categories->findFeaturedServicesGroupedByCategories($data);

        $this->set(compact('properties', 'service', 'servicesGroupedByCategories', 'breadcrumbs'));
    }

    /**
     * View method - Property individual page
     *
     * @param string $urlname Property urlname.
     *
     * @return object|void
     */
    public function view(string $urlname)
    {
        $this->setLayout('properties');
        $property = $this->Properties->findSpecifiedProperty($urlname);

        if (empty($property)) {
            return $this->redirect(['action' => 'index']);
        }
        $this->sessionChecker($property->id);
        $this->loadModel('Categories');
        $servicesGroupedByCategories = $this->Categories->findFeaturedServicesGroupedByCategories();

        $this->setMetaTags($property);

        $this->set(compact('property', 'servicesGroupedByCategories'));
    }

    /**
     * Check session and add record to PropertyViews.
     *
     * @param integer $propertyId Property ID.
     *
     * @return void
     */
    private function sessionChecker(int $propertyId)
    {
        $this->loadModel('PropertyViews');
        $session = $this->getRequest()->getSession();
        if ($session->read('Service.' . $propertyId) === null) {
            $session->write('Service.' . $propertyId, $propertyId);
            $entity = $this->PropertyViews->newEntity(['property_id' => $propertyId]);
            $this->PropertyViews->save($entity);
        }
    }

    /**
     * Send Enquiry
     *
     * @return void|object
     */
    public function sendEnquiry()
    {
        if ($this->getRequest()->is('post')) {
            $this->loadModel('PropertyEnquiries');
            $data = $this->getRequest()->getData();
            $propertyEnquiry = $this->PropertyEnquiries->newEntity($data);
            if ($this->Captcha->isValid() && $this->PropertyEnquiries->save($propertyEnquiry)) {
                $this->getMailer('Properties')->send('propertyEnquiry', [$data]);
                $this->Flash->success('Form has been sent');
            } else {
                $this->Flash->error("Something went wrong");
            }
            return $this->redirect($this->referer());
        }
    }

    /**
     * Save property as favourite
     *
     * @return object
     */
    public function save()
    {
        $this->loadModel('UserSaved');

        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData();
            $entity = $this->UserSaved->newEntity($data);
            if ($this->UserSaved->save($entity)) {
                return $this->jsonResponse(['message' => 'The property was added to your saved properties list in My Account.', 'status' => 200]);
            } else {
                return $this->jsonResponse(['message' => 'Something went wrong.', 'status' => 500]);
            }
        }
    }
}
