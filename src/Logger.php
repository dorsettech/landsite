<?php

namespace App;

use Cake\Core\Configure;

/**
 * Logger - helper for debugging and logging process inside browser.
 * Require PhpConsole extension for Chrome Browser.
 *
 * @author Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
class Logger
{

    /**
     * @var \PhpConsole\Connector
     */
    private static $connector;
    /**
     * @var \PhpConsole\Handler
     */
    private static $console;

    private static $consoleLabelPattern = '[server] %s';

    /**
     * @throws \Exception
     */
    public static function register(): void
    {
        self::$connector = \PhpConsole\Connector::getInstance();
        self::$console = \PhpConsole\Handler::getInstance();
        if (self::$connector->isActiveClient()) {
            self::$connector->setSourcesBasePath($_SERVER['DOCUMENT_ROOT']);
            self::$console->start();
            $debugPassword = Configure::read('Logger.password');
            if ($debugPassword !== '') {
                self::$connector->setPassword($debugPassword);
            }
        }
    }

    public static function disable(): void
    {
        if (self::$connector !== null) {
            self::$connector->disable();
        }
    }

    public static function getConsole(): \PhpConsole\Handler
    {
        return self::$console;
    }

    public static function log($object, $label = null): void
    {
        if (self::$console) {
            self::$console->debug($object, $label === null ? 'LOG' : sprintf(self::$consoleLabelPattern, $label));
        }
    }

    public static function error($object): void
    {
        if (self::$console) {
            self::$console->debug($object, sprintf(self::$consoleLabelPattern, 'ERROR'));
        }
    }

    public static function warn($object): void
    {
        if (self::$console) {
            self::$console->debug($object, sprintf(self::$consoleLabelPattern, 'WARN'));
        }
    }

    public static function info($object): void
    {
        if (self::$console) {
            self::$console->debug($object, sprintf(self::$consoleLabelPattern, 'INFO'));
        }
    }
}
