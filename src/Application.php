<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.3.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App;

use App\Http\Middleware\SubdomainCsrfProtectionMiddleware;
use Cake\Core\Configure;
use Cake\Core\Exception\MissingPluginException;
use Cake\Error\Middleware\ErrorHandlerMiddleware;
use Cake\Http\BaseApplication;
use Cake\Routing\Middleware\AssetMiddleware;
use Cake\Routing\Middleware\RoutingMiddleware;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Application setup class.
 *
 * This defines the bootstrapping logic and middleware layers you
 * want to use in your application.
 */
class Application extends BaseApplication
{
    /**
     * Bootstrap
     *
     * @return void
     * @throws \Exception
     */
    public function bootstrap()
    {
        parent::bootstrap();

        if (PHP_SAPI === 'cli') {
            try {
                $this->addPlugin('Bake');
            } catch (MissingPluginException $e) {
                // Do not halt if the plugin is missing
            }
            $this->addPlugin('Migrations');
        }

        /*
         * Only try to load DebugKit in development mode
         * Debug Kit should not be installed on a production system
         */
        if (Configure::read('debug')) {
            Configure::write('DebugKit.forceEnable', true);
            $this->addPlugin(\DebugKit\Plugin::class);
        }

        $this->addPlugin('Bootstrap');
        $this->addPlugin('Acl', ['bootstrap' => true, 'routes' => true, 'autoload' => true]);
        $this->addPlugin('CsvView');
        $this->addPlugin('PanelBake', ['bootstrap' => false, 'routes' => false]);
        $this->addPlugin('FileManager', ['bootstrap' => true, 'routes' => true, 'autoload' => true]);

        Settings::loadAll();
        Logger::register();
        if (self::isProduction()) {
            Logger::disable();
        }
    }

    public static function isProduction(): bool
    {
        return Configure::read('debug') === false;
    }

    /**
     * Setup the middleware queue your application will use.
     *
     * @param object|null $middlewareQueue The middleware queue to setup.
     *
     * @return MiddlewareQueue
     */
    public function middleware($middlewareQueue)
    {
        return $middlewareQueue
            ->add(ErrorHandlerMiddleware::class)
            ->add(new AssetMiddleware([
                'cacheTime' => Configure::read('Asset.cacheTime')
            ]))
            ->add(new RoutingMiddleware($this, '_cake_routes_'))
            ->add(static function (ServerRequestInterface $request, ResponseInterface $response, callable $next) {
                $params = $request->getAttribute('params', ['prefix' => '']);
                if (!isset($params['prefix']) || $params['prefix'] !== 'api') {
                    $csrf = new SubdomainCsrfProtectionMiddleware([
                        'httpOnly' => true
                    ]);
                    return $csrf($request, $response, $next);
                }
                return $next($request, $response);
            });

    }
}
