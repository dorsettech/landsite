<?php

namespace App\Event;

use App\Mailer\ServicesMailer;
use App\Model\Entity\Service;
use App\Model\Enum\ServiceStatus;
use App\Model\Enum\State;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\Mailer\MailerAwareTrait;

/**
 * Class ServiceEvents
 *
 * @package App\Event
 */
class ServiceEvents implements EventListenerInterface
{

    use MailerAwareTrait;

    protected $mailer;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->mailer = $this->getMailer(ServicesMailer::class);
    }

    /**
     * @return array
     */
    public function implementedEvents(): array
    {
        return [
            'Model.afterSave' => 'onAfterSave'
        ];
    }

    /**
     * @param Event   $event   Event.
     * @param Service $service Service.
     *
     * @return void
     */
    public function onAfterSave(Event $event, Service $service): void
    {
        if ($service->get('status') === ServiceStatus::PUBLISHED && $service->get('state') === State::PENDING) {
            $this->mailer->send('serviceWaitingForApproval', [$service]);
        }
    }
}
