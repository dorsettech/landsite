<?php

namespace App\Event;

use App\Controller\Panel\ExportPdfController;
use App\Mailer\Members\PaymentMailer;
use App\Model\Entity\Payment;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

/**
 * Class PaymentEvents
 *
 * @package App\Event
 */
class PaymentEvents implements EventListenerInterface
{

    use MailerAwareTrait;

    protected $mailer;

    /**
     * PaymentEvents constructor.
     */
    public function __construct()
    {
        $this->mailer = $this->getMailer(PaymentMailer::class);
    }

    /**
     * @return array
     */
    public function implementedEvents(): array
    {
        return [
            'Model.afterSave' => 'onSuccess'
        ];
    }

    /**
     * @param Event   $event   Event.
     * @param Payment $payment Payment entity.
     *
     * @return void
     */
    public function onSuccess(Event $event, Payment $payment): void
    {
        if (!$payment->isNew()) {
            return;
        }

        $invoiceThumb = md5((new ExportPdfController())->invoice($payment->id, false));

        $user = TableRegistry::getTableLocator()->get('Users')->get($payment->get('user_id'));
        $request = Router::getRequest();
        if ($request === null) {
            $host = Configure::read('Domain.subdomains.members') . '.' . Configure::read('Domain.root');
        } else {
            $host = $request->host();
        }

        $this->mailer->send('success', [$user, $payment, $host, $invoiceThumb]);
    }
}
