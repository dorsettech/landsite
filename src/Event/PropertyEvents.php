<?php

namespace App\Event;

use App\Mailer\PropertiesMailer;
use App\Model\Entity\Property;
use App\Model\Enum\PropertyStatus;
use App\Model\Enum\State;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\Mailer\MailerAwareTrait;

/**
 * Class PropertyEvents
 *
 * @package App\Event
 */
class PropertyEvents implements EventListenerInterface
{

    use MailerAwareTrait;

    protected $mailer;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->mailer = $this->getMailer(PropertiesMailer::class);
    }

    /**
     * @return array
     */
    public function implementedEvents(): array
    {
        return [
            /*
             * Waiting For Approval notification has been disabled
             * @see LS-413
             */
            //'Model.afterSave' => 'onAfterSave'
        ];
    }

    /**
     * @param Event    $event    Event.
     * @param Property $property Property.
     *
     * @return void
     */
    public function onAfterSave(Event $event, Property $property): void
    {
        if ($property->get('status') === PropertyStatus::PUBLISHED && $property->get('state') === State::PENDING && $property->get('deleted') === false) {
            $this->mailer->send('propertyWaitingForApproval', [$property]);
        }
    }
}
