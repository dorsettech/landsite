<?php

namespace App\Event;

use App\Mailer\ArticlesMailer;
use App\Model\Entity\Article;
use App\Model\Enum\ArticleStatus;
use App\Model\Enum\ArticleType;
use App\Model\Enum\State;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\Mailer\MailerAwareTrait;

/**
 * Class PaymentEvents
 *
 * @package App\Event
 */
class ArticleEvents implements EventListenerInterface
{

    use MailerAwareTrait;

    protected $mailer;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->mailer = $this->getMailer(ArticlesMailer::class);
    }

    /**
     * @return array
     */
    public function implementedEvents(): array
    {
        return [
            'Model.afterSave' => 'onAfterSave'
        ];
    }

    /**
     * @param Event   $event   Event.
     * @param Article $article Article.
     *
     * @return void
     */
    public function onAfterSave(Event $event, Article $article): void
    {
        if ($article->get('status') === ArticleStatus::PUBLISHED && $article->get('state') === State::PENDING) {
            if ($article->get('type') === ArticleType::CASE_STUDY) {
                $this->mailer->send('caseStudyWaitingForApproval', [$article]);
            } elseif ($article->get('type') === ArticleType::INSIGHT) {
                $this->mailer->send('newsWaitingForApproval', [$article]);
            }
        }
    }
}
