<?php

namespace App\Event;

use App\Model\Enum\DashboardType;
use App\Model\Enum\PropertyStatus;
use App\Model\Enum\ServiceStatus;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\ORM\TableRegistry;

/**
 * User events
 *
 * @package App\Event
 */
class UserEvents implements EventListenerInterface
{

    /**
     * @return array
     */
    public function implementedEvents()
    {
        return [
            'Dashboard.update' => 'onDashboardUpdate'
        ];
    }

    /**
     * @param Event   $event  Event.
     * @param integer $userId User Id.
     * @return void
     */
    public function onDashboardUpdate(Event $event, int $userId): void
    {
        $userDetails = TableRegistry::getTableLocator()->get('UserDetails');

        $details = $userDetails->find('all', [
            'conditions' => [
                'user_id' => $userId
            ]
        ])->first();

        if (!$details) {
            return;
        }

        $propertiesCount = TableRegistry::getTableLocator()->get('Properties')->query()->where([
            'user_id' => $userId,
            'status !=' => PropertyStatus::DRAFT,
            'publish_date IS NOT' => null
        ])->count();

        $servicesCount = TableRegistry::getTableLocator()->get('Services')->query()->where([
            'user_id' => $userId,
            'status !=' => ServiceStatus::DRAFT,
            'publish_date IS NOT' => null
        ])->count();

        $dashboardType = null;

        if ($propertiesCount && $servicesCount) {
            $dashboardType = DashboardType::SELLER_PROFESSIONAL;
        } elseif ($propertiesCount && !$servicesCount) {
            $dashboardType = DashboardType::SELLER;
        } elseif (!$propertiesCount && $servicesCount) {
            $dashboardType = DashboardType::PROFESSIONAL;
        }

        $details->set('dashboard_type', $dashboardType);
        $userDetails->save($details);
    }
}
