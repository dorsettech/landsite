<?php
/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @author  Marcin Czerwik    <marcin@econnect4u.pl>
 * @date (2018-12-13)
 * @version 1.0
 */

namespace App\Model\Behavior;

use App\Mime;
use App\Traits\GetType;
use ArrayObject;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\ORM\Behavior;
use Cake\Utility\Text;
use Cake\Validation\Validator;
use upload;

/**
 * Class UploadBehavior
 *
 * UploadHavior needs entity all fields to be accessible as patches $data on
 * beforeMarshall event with [$field . '-changed'] key, to mark entity as dirty.
 * It won't work if only specific fields of entity will be accessible which
 * might cause upload issue when file field is empty.
 */
class UploadBehavior extends Behavior
{

    use GetType;
    /**
     * Uploads container
     *
     * @var array
     */
    private $uploads = [];

    /**
     * Clear container
     *
     * @var array
     */
    private $clear = [];

    /**
     * Errors array
     *
     * @var array
     */
    private $errors = [];

    /**
     * Default config
     *
     * @var array
     */
    protected $_defaultConfig = [
        'fields'         => [],
        'filesDirectory' => 'files/',
        'directory'      => null,
        'uidDirectory'   => null,
        'uid'            => null,
        'randomName'     => false,
        'type'           => 'image',
        'mimeTypes'      => ['image/gif', 'image/png', 'image/jpg', 'image/jpeg'],
        'maxFileSize'    => '8MB'
    ];

    /**
     * Media config
     *
     * @var array
     */
    protected $media = [];

    /**
     * Tmp prefix for uploaded files
     *
     * @var string
     */
    protected $tmpPrefix = '';

    /**
     * Uid
     *
     * @var mixed Boolean or string.
     */
    protected $uid = true;

    /**
     * Initialize
     *
     * @param array $config Additional array.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $parsedFields = [];
        foreach ($this->getConfig('fields') as $field => $settings) {
            if (is_numeric($field)) {
                $field    = $settings;
                $settings = [];
            }
            $parsedFields[$field] = $settings;
        }
        $this->setConfig('fields', $parsedFields, false);

        if (isset($config['uid'])) {
            $this->setConfig('uid', $config['uid']);
            $this->uid = $config['uid'];
        }

        $this->setMediaConfig();
        $this->setMaxFileSize();
    }

    /**
     * Set media config
     *
     * @return void
     */
    protected function setMediaConfig(): void
    {
        Configure::load('website');
        $this->media     = Configure::read('Media');
        $this->tmpPrefix = $this->media['upload_tmp_prefix'] ?? '';
    }

    /**
     * Set max file size
     *
     * @return void Sets the max file size from php.ini if it's lower than set in config
     */
    public function setMaxFileSize(): void
    {
        $phpMaxFileSize = ini_get('upload_max_filesize');
        if ($phpMaxFileSize < $this->_defaultConfig['maxFileSize']) {
            $this->_defaultConfig['maxFileSize'] = $phpMaxFileSize;
        }
    }

    /**
     * Before marshal event
     *
     * @param Event        $event   Event.
     * @param \ArrayObject $data    Data to analyze.
     * @param \ArrayObject $options Additional options.
     *
     * @return void
     */
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        foreach ($this->getConfig('fields') as $field => $settings) {
            if (!empty($data[$field]) && is_array($data[$field])) {
                $this->setUploadErrors($field, $data[$field]);
            }
            if (isset($data[$field]['tmp_name']) && is_uploaded_file($data[$field]['tmp_name'])) {
                $this->uploads[$field]     = $data[$field];
                $data[$field . '-changed'] = true;
            } elseif (isset($data[$field])) {
                unset($data[$field]);
            }
            if (isset($data[$field . '-clear']) && $data[$field . '-clear'] == 1) {
                $this->clear[] = $field;
                $data[$field]  = null;
            }
        }
        // upload errors needs to be handled in controller by setting errors directly to entity, i.e.
        // $pages->setErrors($page->upload_errors);
        $data['upload_errors'] = $this->errors;
    }

    /**
     * Set upload errors
     *
     * @see http://php.net/manual/en/features.file-upload.errors.php
     * @param string $field        Field to set error for.
     * @param array  $uploadedFile Uploaded file array.
     * @return void Sets an error message for specific field if it's related to file upload error.
     */
    private function setUploadErrors(string $field, array $uploadedFile)
    {
        switch ($uploadedFile['error']) {
            case 1:
                $this->errors[$field] = __('The uploaded file exceeds the `upload_max_filesize` server directive, which is set to {0}. Please contact administrator to change that.', ini_get('upload_max_filesize'));
                break;
            case 2:
                $this->errors[$field] = __('The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form. Please contact administrator to change that.');
                break;
            case 3:
                $this->errors[$field] = __('The uploaded file was only partially uploaded.');
                break;
            case 6:
                $this->errors[$field] = __('Missing a temporary folder.');
                break;
            case 7:
                $this->errors[$field] = __('Failed to write file to disk.');
                break;
            case 8:
                $this->errors[$field] = __('A PHP extension stopped the file upload.');
                break;
        }
    }

    /**
     *
     * Build validator
     *
     * @param Event     $event     Event.
     * @param Validator $validator Validator object.
     *
     * @return Validator
     */
    public function buildValidator(Event $event, Validator $validator)
    {
        $fields = $this->getConfig('fields');
        if (is_array($fields) && !empty($fields)) {
            foreach ($fields as $field => $settings) {
                $this->addValidationToField($validator, $field, $settings);
            }
        }

        return $validator;
    }

    /**
     * Add validation to field
     *
     * @param Validator    $validator Validator object.
     * @param string|null  $field     Field name.
     * @param array|string $settings  Additional settings.
     *
     * @return void
     */
    private function addValidationToField(Validator $validator, $field, $settings)
    {
        $type = $this->getType($settings, $field);

        switch ($type) {
            case 'document':
                $mimeTypes   = $settings['mimeTypes'] ?? $this->media['mime_types_allowed']['document'] ?? $this->getConfig('mimeTypes');
                $maxFileSize = $settings['maxFileSize'] ?? $this->media['max_doc_size'] ?? $this->getConfig('maxFileSize');
                break;
            case 'image':
            default:
                $mimeTypes   = $settings['mimeTypes'] ?? $this->media['mime_types_allowed']['image'] ?? $this->getConfig('mimeTypes');
                $maxFileSize = $settings['maxFileSize'] ?? $this->media['max_image_size'] ?? $this->getConfig('maxFileSize');
                break;
        }

        $validator
            ->add($field, [
                'uploadError' => [
                    'rule'    => 'uploadError',
                    'message' => __('The file upload failed.')
                ],
                'mimeType'    => [
                    'rule'       => array('mimeType', $mimeTypes),
                    'message'    => __('Invalid mime type of file (available types: {0}).', implode(', ', $mimeTypes)),
                    'allowEmpty' => true
                ],
                'fileSize'    => [
                    'rule'    => array('fileSize', '<=', $maxFileSize),
                    'message' => __('{0} file must be less than {1}.', [$field, $this->_defaultConfig['maxFileSize']])
                ]
            ]);
    }

    /**
     * Get type
     *
     * @param array       $settings Settings used in validation.
     * @param string|null $field    Current field.
     * @return string
     */
    private function getType(array $settings, $field = ''): ?string
    {
        if (!empty($settings['type'])) {
            $type = $settings['type'];

            if ($settings['type'] == 'mixed' && !empty($this->uploads[$field]['type'])) {
                $type = $this->getTypeFromMime($this->uploads[$field]['type'], $this->media['mime_types_allowed']);
            }
        }

        return $type ?? $this->_defaultConfig['type'];
    }

    /**
     * Before Save
     *
     * @param Event           $event   Event.
     * @param EntityInterface $entity  Entity.
     * @param \ArrayObject    $options Options.
     *
     * @return EntityInterface
     */
    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        $this->setUid($entity);

        foreach ($this->uploads as $field => $uploadData) {
            $oldFile       = new File($entity->getOriginal($field));
            $oldFile->delete();
            $processedFile = $this->processFile($field, $uploadData);
            if (!empty($this->tmpPrefix)) {
                $processedFile = str_replace($this->tmpPrefix, '', $processedFile);
            }
            $entity->{$field} = $processedFile;
        }
        foreach ($this->clear as $field) {
            $file             = new File($entity->getOriginal($field));
            $file->delete();
            $entity->{$field} = null;
        }

        return $entity;
    }

    /**
     * After Save
     *
     * @param Event           $event   Event.
     * @param EntityInterface $entity  Entity.
     * @param \ArrayObject    $options Options.
     *
     * @return void
     */
    public function afterSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        if (!empty($this->tmpPrefix)) {
            foreach ($this->uploads as $field => $uploadData) {
                $filePath = $this->getDirectory() . $this->tmpPrefix . $entity->{$field};
                if (file_exists($filePath)) {
                    rename($filePath, $this->getDirectory() . $entity->{$field});
                }
            }
        }
    }

    /**
     * After delete
     *
     * @param Event           $event   Event.
     * @param EntityInterface $entity  Entity.
     * @param \ArrayObject    $options Options.
     *
     * @return void
     */
    public function afterDelete(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        foreach ($this->getConfig('fields') as $field => $settings) {
            if (!empty($entity->{$field})) {
                $path = $field . '_path';
                $file = new File($entity->{$path});
                $file->delete();
            }
        }
    }

    /**
     * Process file
     *
     * @param integer|null $field      Filed number/key.
     * @param object|null  $uploadData Upload data.
     * @return mixed|null
     */
    public function processFile($field, $uploadData): ?string
    {
        $upload       = new upload($uploadData);
        $fieldsConfig = $this->getConfig('fields');

        if (!$upload->uploaded) {
            return null;
        }

        if (isset($fieldsConfig[$field]['randomName']) && $fieldsConfig[$field]['randomName']) {
            $upload->file_new_name_body = $this->tmpPrefix . str_replace('-', '', Text::uuid());
            $upload->file_new_name_ext  = Mime::mimeToExtension($upload->file_src_mime);
        }

        $upload->process($this->getDirectory());
        if ($upload->processed) {
            return $upload->file_dst_name;
        }

        return null;
    }

    /**
     * Set uid for saving in separate subdirectories
     *
     * @param object $entity Entity object.
     * @return void
     */
    public function setUid(object $entity): void
    {
        $uidDirectory = '';
        $uid          = $this->uid;

        if ($this->uid === true && $entity->has('uid')) {
            $uidDirectory = $entity->uid;
        } elseif (is_string($this->uid) && !empty($this->uid)) {
            $relatedEntity = $this->getRelatedEntity($entity);

            if (is_object($relatedEntity) && $relatedEntity->has('uid')) {
                $uidDirectory = $relatedEntity->uid;
            }
        }

        $this->setConfig('uidDirectory', $uidDirectory);
    }

    /**
     * Get related entity
     *
     * @param object $entity Entity object.
     * @return object|null Returns related entity.
     */
    protected function getRelatedEntity(object $entity): ?object
    {
        $firstRelation = $this->uid;
        $contains      = '';
        if (strpos($this->uid, '.') > 0) {
            $related       = explode('.', $this->uid);
            $firstRelation = $related[0];
            array_shift($related);
            $contains      = implode('.', $related);
        }
        $foreignKey    = $this->_table->getAssociation($firstRelation)->getForeignKey();
        $relatedEntity = $this->_table->{$firstRelation}->find('all', [
            'conditions' => [$firstRelation . '.id' => $entity->{$foreignKey}],
        ]);

        if (!empty($contains)) {
            $relatedEntity->contain([$contains]);
        }

        $relatedEntity = $relatedEntity->first();

        if (!empty($contains)) {
            $property      = $this->_table->{$firstRelation}->getAssociation($contains)->getProperty();
            $relatedEntity = $relatedEntity->{$property} ?? null;
        }

        return $relatedEntity;
    }

    /**
     * Get directory where files needs to be uploaded
     *
     * @return string
     */
    public function getDirectory(): string
    {
        if (empty($this->getConfig('directory'))) {
            $this->setConfig('directory', $event->getSubject()->table());
        }

        $dir = $this->_defaultConfig['filesDirectory'] . $this->getConfig('directory') . '/';

        if ($this->getConfig('uidDirectory')) {
            $dir .= $this->getConfig('uidDirectory') . '/';
        }

        return $dir;
    }
}
