<?php

/**
 * @author Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-06)
 * @version 1.0
 */

namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use ArrayObject;
use Cake\Utility\Text;

/**
 * Slugify behavior
 */
class SlugifyBehavior extends Behavior
{
    /**
     * Default config
     *
     * @var array
     */
    protected $_defaultConfig = ['field' => 'urlname'];

    /**
     * Before save
     *
     * @param Event           $event  Event.
     * @param EntityInterface $entity Entity.
     *
     * @return void
     */
    public function beforeSave(Event $event, EntityInterface $entity)
    {
        $entity->{$this->_config['field']} = Text::slug($entity->{$this->_config['field']});
    }

    /**
     * BeforeMarshal
     *
     * @param Event        $event   Event.
     * @param \ArrayObject $data    Data to analyze.
     * @param \ArrayObject $options Additional options.
     *
     * @return void
     */
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        if (empty($data[$this->_config['field']])) {
            if (isset($data['title'])) {
                $data[$this->_config['field']] = Text::slug(($data['title']));
            } elseif (isset($data['name'])) {
                $data[$this->_config['field']] = Text::slug(($data['name']));
            }
        }
    }
}
