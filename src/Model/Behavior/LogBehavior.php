<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-06)
 * @version 1.0
 */

namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use ArrayObject;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\ORM\Entity;
use Cake\I18n\I18n;

/**
 * Class LogBehavior
 */
class LogBehavior extends Behavior
{
    /**
     * Default config data
     *
     * @var array
     */
    protected $_defaultConfig = [
        'ignore' => [],
        'user' => 'Unknown',
        'versioning' => true
    ];

    /**
     * Initialize
     *
     * @param array $config Additional config.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->request = Router::getRequest();
        if ($this->request !== null) {
            $user = $this->request->getSession()->read('Auth.User');
            if ($user) {
                $this->_configWrite('user', $user['first_name'] . ' ' . $user['last_name']);
            }
        }
    }

    /**
     * Generate content
     *
     * @param Entity $entity Entity.
     *
     * @return string
     */
    public function generateContent(Entity $entity)
    {
        if ($entity->isNew()) {
            $action = __('added new item to ');
        } else if ($this->request->params['action'] === "delete") {
            $action = __('removed record #{0} of ', $entity->id);
        } else {
            $action = __('edited record #{0} of ', $entity->id);
        }

        return __('{0} has {1}', $this->getConfig('user'), $action . $this->_table->getAlias());
    }

    /**
     * Before save
     *
     * @param Event           $event   Event.
     * @param EntityInterface $entity  Entity.
     * @param \ArrayObject    $options Options.
     *
     * @return void
     */
    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        if ($this->request !== null && !in_array($this->request->param('action'), $this->getConfig('ignore'))) {
            $this->addLog($entity);
        }
    }

    /**
     * Before delete
     *
     * @param Event           $event   Event.
     * @param EntityInterface $entity  Entity.
     * @param \ArrayObject    $options Options.
     *
     * @return void
     */
    public function beforeDelete(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        if ($this->request !== null && !in_array($this->request->param('action'), $this->config('ignore'))) {
            $this->addLog($entity);
        }
    }

    /**
     * Add log
     *
     * @param Entity $entity Entity.
     *
     * @return void
     */
    public function addLog(Entity $entity)
    {
        $logsTable = TableRegistry::getTableLocator()->get('Logs');
        $data = [
            'user_id' => $this->request->getSession()->check('Auth.User.id') ? $this->request->getSession()->read('Auth.User.id') : null,
            'ip' => $this->request->clientIp(),
            'host' => gethostbyaddr($this->request->clientIp()),
            'agent' => $this->request->getHeaderLine('User-Agent'),
            'content' => $this->generateContent($entity),
            'url' => $this->request->getAttribute('here')
        ];
        if ($this->getConfig('versioning')) {
            $data += [
                'entity' => serialize($entity),
                'foreign_key' => $entity->id,
                'model' => $this->_table->getAlias(),
                'locale' => I18n::getLocale()
            ];
        }
        $log = $logsTable->newEntity($data);
        $logsTable->save($log);
    }
}
