<?php
/**
 * @author Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @author Marcin Nierobiś <marcin.nierobis@econnect4u.pl>
 * @date (2018-11-28)
 * @version 1.1
 */

namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\Routing\Router;
use Cake\Event\Event;
use Cake\Database\Query;
use ArrayObject;

/**
 * Filter behavior
 */
class FilterBehavior extends Behavior
{
    /**
     * defaultConfig
     *
     * @var array
     */
    protected $_defaultConfig = [
        'fields' => [],
    ];

    /**
     * Request object
     *
     * @var null
     */
    private $request = null;

    /**
     * Initialize
     *
     * @param array $config Config array.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->request = Router::getRequest();
    }

    /**
     * before Find
     *
     * @param  Event        $event   Event.
     * @param  Query        $query   Query.
     * @param  ArrayObject  $options Additional options passed to query: `filter` => true|false|force.
     * @param  boolean|null $primary Is primary.
     * @return void
     */
    public function beforeFind(Event $event, Query $query, ArrayObject $options, $primary)
    {
        $filter = true;

        if (isset($options['filter'])) {
            $filter = $options['filter'];
        }

        if ($filter && $this->request) {
            $search          = $this->request->getQuery('filter_keyword');
            $keyword         = isset($search) ? $search : '';
            $queryRepository = $query->getRepository()->getAlias();
            $controller      = isset($this->_config['controller']) ? $this->_config['controller'] : $this->request->getParam('controller');
            $matching        = isset($this->_config['matching']) ? (array) $this->_config['matching'] : '';
            if ($filter === 'force' || ($primary && !empty($keyword) && $queryRepository == $controller)) {
                $conditions = [];
                foreach ($this->_config['fields'] as $fieldName) {
                    $conditions[$fieldName.' LIKE'] = '%'.$keyword.'%';
                }

                if (count($conditions) > 0) {
                    if (!empty($matching)) {
                        foreach ($matching as $value) {
                            $query->leftJoinWith($value);
                        }
                    }
                    $query->where(['OR' => $conditions]);
                }
            }
        }
    }
}
