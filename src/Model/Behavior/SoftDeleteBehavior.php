<?php

namespace App\Model\Behavior;

use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;

/**
 * SoftDeleteBehavior
 *
 * @author Stefan <marcin@econnect4u.pl>
 * @date date(2017-07-24)
 * @version 1.0
 */
class SoftDeleteBehavior extends Behavior
{

    /**
     * Before find event
     * Adds conditions to finder object
     *
     * @param Event $event
     * @param Query $query
     * @param ArrayObject $options
     * @param type $primary
     */
    public function beforeFind(Event $event, Query $query, ArrayObject $options, $primary)
    {
        if ($this->_table->hasField('deleted')) {
            $query->where([$this->_table->getAlias() . '.deleted' => false]);
        }
    }

    /**
     * Before delete
     * Stops propagating when entity has `deleted` field, marks it as deleted instead of deleting
     *
     * @param Event $event
     * @param EntityInterface $entity
     * @param ArrayObject $options
     * @return boolean
     */
    public function beforeDelete(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        if ($entity->has('deleted')) {
            $event->stopPropagation();

            $entity->deleted = true;
            $entity          = $this->patchUniqueFields($entity);

            if ($this->_table->save($entity)) {
                return true;
            }

            return false;
        }
    }

    /**
     * Patch unique fields before delete
     *
     * @param object $entity Entity object.
     * @return Returns entity with patched unique fields.
     */
    protected function patchUniqueFields($entity)
    {
        $schema      = $this->getTable()->getSchema();
        $constraints = $schema->constraints();

        $uniqueAppend = substr(md5(time()), 0, 5);
        foreach ($constraints as $constraint) {
            if ($constraint !== 'uid') {
                $constraintDetails = $schema->getConstraint($constraint);
                
                if ($constraintDetails['type'] == 'unique') {
                    foreach ($constraintDetails['columns'] as $column) {
                        $entity->{$column} = $entity->{$column}.'-'.$uniqueAppend;
                    }
                }
            }
        }

        return $entity;
    }
}
