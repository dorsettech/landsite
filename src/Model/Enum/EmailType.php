<?php

namespace App\Model\Enum;

use App\Model\Enum;

/**
 * Class EmailType
 * @package App\Model\Enum
 *
 * @author Marcin Nierobiś <marcin.nierobis@econnect4u.pl>
 */
abstract class EmailType extends Enum
{

    public const
        BUYING = 'BUYING',
        SELLING = 'SELLING',
        INSIGHTS = 'INSIGHTS',
        PROFESSIONALS = 'PROFESSIONALS';
}
