<?php

namespace App\Model\Enum;

use App\Model\Enum;

/**
 * Class ArticleStatis
 * @package App\Model\Enum
 *
 * @author Marcin Nierobiś <marcin.nierobis@econnect4u.pl>
 */
abstract class ArticleStatus extends Enum
{

    public const
        DRAFT = 'DRAFT',
        PUBLISHED = 'PUBLISHED';

    /**
     * Get names list
     *
     * @return array
     * @throws \ReflectionException Reflection Exception.
     */
    public static function getNamesList(): array
    {
        $types = self::getConstList();

        $data = [];
        foreach ($types as $key => $value) {
            switch ($value) {
                case self::DRAFT:
                    $data[$value] = 'Draft';
                    break;
                case self::PUBLISHED:
                    $data[$value] = 'Published';
                    break;
                default:
                    $data[$value] = 'Unknown';
            }
        }

        return $data;
    }
}
