<?php

namespace App\Model\Enum;

use App\Model\Enum;

/**
 * Class PropertyPriceSalePer
 *
 * @package App\Model\Enum
 */
abstract class PropertyPriceSalePer extends Enum
{
    public const
        FULL = 'F',
        FEET_SQUARE = 'FT2',
        METER_SQUARE = 'M2',
        PRICE_ON_APPLICATION = 'POA';

    /**
     * Get names list
     *
     * @return array
     */
    public static function getNamesList(): array
    {
        try {
            $types = self::getConstList();
        } catch (\ReflectionException $e) {
            $types = [];
        }
        $data = [];
        foreach ($types as $key => $value) {
            switch ($value) {
                case self::FULL:
                    $data[$value] = '';
                    break;
                case self::FEET_SQUARE:
                    $data[$value] = 'pft²';
                    break;
                case self::METER_SQUARE:
                    $data[$value] = 'pm²';
                    break;
                case self::PRICE_ON_APPLICATION:
                    $data[$value] = 'POA';
                    break;
                default:
                    $data[$value] = '';
            }
        }

        return $data;
    }

    /**
     * Get select list
     *
     * @return array
     */
    public static function getSelectList(): array
    {
        try {
            $types = self::getConstList();
        } catch (\ReflectionException $e) {
            $types = [];
        }
        $data = [];
        foreach ($types as $key => $value) {
            switch ($value) {
                case self::FULL:
                    $data[$value] = 'full sale price';
                    break;
                case self::FEET_SQUARE:
                    $data[$value] = 'per ft²';
                    break;
                case self::METER_SQUARE:
                    $data[$value] = 'per m²';
                    break;
                case self::PRICE_ON_APPLICATION:
                    $data[$value] = 'POA';
                    break;
                default:
                    $data[$value] = '';
            }
        }

        return $data;
    }
}
