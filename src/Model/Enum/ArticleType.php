<?php

namespace App\Model\Enum;

use App\Model\Enum;

/**
 * Class ArticleType
 * @package App\Model\Enum
 *
 * @author Marcin Nierobiś <marcin.nierobis@econnect4u.pl>
 */
abstract class ArticleType extends Enum
{

    public const
        INSIGHT = 'INSIGHT',
        CASE_STUDY = 'CASE_STUDY';

    /**
     * Get names list
     *
     * @return array
     * @throws \ReflectionException Reflection Exception.
     */
    public static function getNamesList(): array
    {
        $types = self::getConstList();

        $data = [];
        foreach ($types as $key => $value) {
            switch ($value) {
                case self::INSIGHT:
                    $data[$value] = 'News';
                    break;
                case self::CASE_STUDY:
                    $data[$value] = 'Case Study';
                    break;
                default:
                    $data[$value] = 'Unknown';
            }
        }

        return $data;
    }
}
