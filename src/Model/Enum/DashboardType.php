<?php

namespace App\Model\Enum;

use App\Model\Enum;

/**
 * Dashboard Type
 */
abstract class DashboardType extends Enum
{
    public const
        SELLER = 'S',
        PROFESSIONAL = 'P',
        SELLER_PROFESSIONAL = 'SP';
}
