<?php

namespace App\Model\Enum;

use App\Model\Enum;

/**
 * Class SettingType
 * @package App\Model\Enum
 *
 * @author Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
abstract class SettingType extends Enum
{

    public const
        BOOLEAN = 'BOOL',
        INTEGER = 'INT',
        FLOAT = 'FLOAT',
        STRING = 'STRING',
        JSON = 'JSON';

    /**
     * @return array
     * @throws \ReflectionException ReflectionException.
     */
    public static function getNamesList(): array
    {
        $types = self::getConstList();
        $data = [];
        foreach ($types as $key => $value) {
            switch ($value) {
                case self::BOOLEAN:
                    $data[$value] = 'Boolean';
                    break;
                case self::INTEGER:
                    $data[$value] = 'Integer';
                    break;
                case self::FLOAT:
                    $data[$value] = 'Float';
                    break;
                case self::STRING:
                    $data[$value] = 'String';
                    break;
                case self::JSON:
                    $data[$value] = 'Json';
                    break;
                default:
                    $data[$value] = 'Unknown';
            }
        }
        return $data;
    }
}
