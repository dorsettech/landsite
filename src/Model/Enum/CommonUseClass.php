<?php

namespace App\Model\Enum;

use App\Model\Enum;

/**
 * Common Use Class
 */
abstract class CommonUseClass extends Enum
{
    public const
        A1_SHOPS = 'A1',
        A2_FINANCIAL_PROFESSIONAL_SERVICES = 'A2',
        A3_RESTAURANTS_CAFES = 'A3',
        A4_DRINKING_ESTABLISHMENTS = 'A4',
        A5_HOT_FOOD_TAKE_AWAY = 'A5',
        B1_BUSINESS = 'B1',
        B2_GENERAL_INDUSTRIAL = 'B2',
        B8_STORAGE_DISTRIBUTION = 'B8',
        C1_HOTELS = 'C1',
        C2_RESIDENTIAL_INSTITUTIONS = 'C2',
        C2A_SECURE_RESIDENTIAL_INSTITUTION = 'C2A',
        C3_DWELLING_HOUSES = 'C3',
        C4_HMO = 'C4',
        D1_NON_RESIDENTIAL_INSTITUTIONS = 'D1',
        D2_ASSEMBLY_LEISURE = 'D2',
        SUI_GENERIS_1 = 'SUI1',
        SUI_GENERIS_2 = 'SUI2';

    /**
     * Get names list
     *
     * @return array
     * @throws \ReflectionException Reflection Exception.
     */
    public static function getNamesList(): array
    {
        $types = self::getConstList();

        $data = [];
        foreach ($types as $key => $value) {
            switch ($value) {
                case self::A1_SHOPS:
                    $data[$value] = 'A1 Shops';
                    break;
                case self::A2_FINANCIAL_PROFESSIONAL_SERVICES:
                    $data[$value] = 'A2 Financial and Professional Services';
                    break;
                case self::A3_RESTAURANTS_CAFES:
                    $data[$value] = 'A3 Restaurants and Cafes';
                    break;
                case self::A4_DRINKING_ESTABLISHMENTS:
                    $data[$value] = 'A4 Drinking Establishments';
                    break;
                case self::A5_HOT_FOOD_TAKE_AWAY:
                    $data[$value] = 'A5 Hot Food Take away';
                    break;
                case self::B1_BUSINESS:
                    $data[$value] = 'B1 Business';
                    break;
                case self::B2_GENERAL_INDUSTRIAL:
                    $data[$value] = 'B2 General Industrial';
                    break;
                case self::B8_STORAGE_DISTRIBUTION:
                    $data[$value] = 'B8 Storage and Distribution';
                    break;
                case self::C1_HOTELS:
                    $data[$value] = 'C1 Hotels';
                    break;
                case self::C2_RESIDENTIAL_INSTITUTIONS:
                    $data[$value] = 'C2 Residential Institutions';
                    break;
                case self::C2A_SECURE_RESIDENTIAL_INSTITUTION:
                    $data[$value] = 'C2A Secure Residential Institution';
                    break;
                case self::C3_DWELLING_HOUSES:
                    $data[$value] = 'C3 Dwelling Houses';
                    break;
                case self::C4_HMO:
                    $data[$value] = 'C4 HMO';
                    break;
                case self::D1_NON_RESIDENTIAL_INSTITUTIONS:
                    $data[$value] = 'D1 Non-Residential Institutions';
                    break;
                case self::D2_ASSEMBLY_LEISURE:
                    $data[$value] = 'D2 Assembly and Leisure';
                    break;
                case self::SUI_GENERIS_1:
                    $data[$value] = 'Sui_generis_1';
                    break;
                case self::SUI_GENERIS_2:
                    $data[$value] = 'Sui_generis_2';
                    break;
                default:
                    $data[$value] = 'Unknown';
            }
        }

        return $data;
    }
}
