<?php

namespace App\Model\Enum;

use App\Model\Enum;

/**
 * Class PropertyPriceRentPer
 *
 * @package App\Model\Enum
 */
abstract class PropertyPriceRentPer extends Enum
{
    public const
        MONTH = 'M',
        YEAR = 'Y',
        QUARTER = 'Q',
        FEET_SQUARE = 'FT2',
        METER_SQUARE = 'M2',
        PRICE_ON_APPLICATION = 'POA';

    /**
     * Get names list
     *
     * @return array
     */
    public static function getNamesList(): array
    {
        try {
            $types = self::getConstList();
        } catch (\ReflectionException $e) {
            $types = [];
        }
        $data = [];
        foreach ($types as $key => $value) {
            switch ($value) {
                case self::MONTH:
                    $data[$value] = 'p.m.';
                    break;
                case self::YEAR:
                    $data[$value] = 'p.a.';
                    break;
                case self::QUARTER:
                    $data[$value] = 'p.q.';
                    break;
                case self::FEET_SQUARE:
                    $data[$value] = 'pft²';
                    break;
                case self::METER_SQUARE:
                    $data[$value] = 'pm²';
                    break;
                case self::PRICE_ON_APPLICATION:
                    $data[$value] = 'POA';
                    break;
                default:
                    $data[$value] = '';
            }
        }

        return $data;
    }

    /**
     * Get select list
     *
     * @return array
     */
    public static function getSelectList(): array
    {
        try {
            $types = self::getConstList();
        } catch (\ReflectionException $e) {
            $types = [];
        }
        $data = [];
        foreach ($types as $key => $value) {
            switch ($value) {
                case self::MONTH:
                    $data[$value] = 'per month';
                    break;
                case self::QUARTER:
                    $data[$value] = 'per quarter';
                    break;
                case self::YEAR:
                    $data[$value] = 'per annum';
                    break;
                case self::FEET_SQUARE:
                    $data[$value] = 'per ft²';
                    break;
                case self::METER_SQUARE:
                    $data[$value] = 'per m²';
                    break;
                case self::PRICE_ON_APPLICATION:
                    $data[$value] = 'POA';
                    break;
                default:
                    $data[$value] = '';
            }
        }

        return $data;
    }
}
