<?php

namespace App\Model\Enum;

use App\Model\Enum;

/**
 * Class ClickType
 */
abstract class ClickType extends Enum
{
    /**
     * Click Types
     */
    public const WEBSITE_URL = 0;
    public const CALL_URL = 1;
}
