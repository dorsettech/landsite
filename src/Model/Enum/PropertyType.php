<?php

namespace App\Model\Enum;

use App\Model\Enum;

/**
 * Primary Property Type
 *
 * Property types are dynamical, stored in database. This class is only helper among others for API.
 */
abstract class PropertyType extends Enum
{
    public const
        LAND = 1,
        OFFICES = 2,
        COMMERCIAL_PROPERTY = 3,
        DEVELOPMENT_SITE = 5;

}
