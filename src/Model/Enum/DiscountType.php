<?php
/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */

namespace App\Model\Enum;

use App\Model\Enum;
use ReflectionException;

/**
 * DiscountType
 */
class DiscountType extends Enum
{
    /**
     * Discount type (the equivalent of discounts.type column in the database)
     */
    public const
        FIXEDSUM   = 'FIXEDSUM',
        FREE       = 'FREE',
        PERCENTAGE = 'PERCENTAGE';

    /**
     * Get names list
     *
     * @return array
     * @throws ReflectionException Reflection Exception.
     */
    public static function getNamesList(): array
    {
        $types = self::getConstList();

        $data = [];
        foreach ($types as $key => $value) {
            switch ($value) {
                case self::FIXEDSUM:
                    $data[$value] = 'Fixed Sum';
                    break;
                case self::FREE:
                    $data[$value] = 'Free';
                    break;
                case self::PERCENTAGE:
                    $data[$value] = 'Percentage';
                    break;
                default:
                    $data[$value] = 'Unknown';
            }
        }

        return $data;
    }
}
