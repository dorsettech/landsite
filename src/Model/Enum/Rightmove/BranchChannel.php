<?php

namespace App\Model\Enum\Rightmove;

use App\Model\Enum;

/**
 * Rightmove Branch Channel
 */
abstract class BranchChannel extends Enum
{
    public const
        SALES = 1,
        LETTINGS = 2;
}
