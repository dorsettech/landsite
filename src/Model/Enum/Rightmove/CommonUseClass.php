<?php

namespace App\Model\Enum\Rightmove;

use App\Model\Enum;

/**
 * Class CommonUseClass
 *
 * @package App\Model\Enum\Rightmove
 */
abstract class CommonUseClass extends Enum
{
    public const
        A1_SHOPS = 1,
        A2_FINANCIAL_PROFESSIONAL_SERVICES = 4,
        A3_RESTAURANTS_CAFES = 7,
        A4_DRINKING_ESTABLISHMENTS = 10,
        A5_HOT_FOOD_TAKE_AWAY = 13,
        B1_BUSINESS = 16,
        B2_GENERAL_INDUSTRIAL = 19,
        B8_STORAGE_DISTRIBUTION = 22,
        C1_HOTELS = 25,
        C2_RESIDENTIAL_INSTITUTIONS = 28,
        C2A_SECURE_RESIDENTIAL_INSTITUTION = 31,
        C3_DWELLING_HOUSES = 34,
        D1_NON_RESIDENTIAL_INSTITUTIONS = 37,
        D2_ASSEMBLY_LEISURE = 40,
        SUI_GENERIS_1 = 43,
        SUI_GENERIS_2 = 46;

    /**
     * @var array Conversion array between Rightmove and The Landsite.
     */
    private static $typeMatrix = [
        self::A1_SHOPS => Enum\CommonUseClass::A1_SHOPS,
        self::A2_FINANCIAL_PROFESSIONAL_SERVICES => Enum\CommonUseClass::A2_FINANCIAL_PROFESSIONAL_SERVICES,
        self::A3_RESTAURANTS_CAFES => Enum\CommonUseClass::A3_RESTAURANTS_CAFES,
        self::A4_DRINKING_ESTABLISHMENTS => Enum\CommonUseClass::A4_DRINKING_ESTABLISHMENTS,
        self::A5_HOT_FOOD_TAKE_AWAY => Enum\CommonUseClass::A5_HOT_FOOD_TAKE_AWAY,
        self::B1_BUSINESS => Enum\CommonUseClass::B1_BUSINESS,
        self::B2_GENERAL_INDUSTRIAL => Enum\CommonUseClass::B2_GENERAL_INDUSTRIAL,
        self::B8_STORAGE_DISTRIBUTION => Enum\CommonUseClass::B8_STORAGE_DISTRIBUTION,
        self::C1_HOTELS => Enum\CommonUseClass::C1_HOTELS,
        self::C2_RESIDENTIAL_INSTITUTIONS => Enum\CommonUseClass::C2_RESIDENTIAL_INSTITUTIONS,
        self::C2A_SECURE_RESIDENTIAL_INSTITUTION => Enum\CommonUseClass::C2A_SECURE_RESIDENTIAL_INSTITUTION,
        self::C3_DWELLING_HOUSES => Enum\CommonUseClass::C3_DWELLING_HOUSES,
        self::D1_NON_RESIDENTIAL_INSTITUTIONS => Enum\CommonUseClass::D1_NON_RESIDENTIAL_INSTITUTIONS,
        self::D2_ASSEMBLY_LEISURE => Enum\CommonUseClass::D2_ASSEMBLY_LEISURE,
        self::SUI_GENERIS_1 => Enum\CommonUseClass::SUI_GENERIS_1,
        self::SUI_GENERIS_2 => Enum\CommonUseClass::SUI_GENERIS_2
    ];

    /**
     * @param integer $value Property type value - right move representation (integer).
     *
     * @return string|null
     */
    public static function toTheLandsiteValue(int $value): ?string
    {
        return self::$typeMatrix[$value] ?? null;
    }
}
