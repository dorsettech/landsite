<?php

namespace App\Model\Enum\Rightmove;

use App\Model\Enum;

/**
 * Class MediaType
 *
 * @package App\Model\Enum\Rightmove
 */
abstract class MediaType extends Enum
{

    public const
        IMAGE = 1,
        FLOORPLAN = 2,
        BROCHURE = 3,
        VIRTUAL_TOUR = 4,
        AUDIO_TOUR = 5,
        EPC = 6,
        EPC_GRAPH = 7;

    /**
     * @var array Conversion array between Rightmove and The Landsite.
     */
    private static $typeMatrix = [
        self::IMAGE => Enum\MediaType::IMAGE,
        self::FLOORPLAN => Enum\MediaType::DOCUMENT,
        self::BROCHURE => Enum\MediaType::DOCUMENT,
        self::EPC => Enum\MediaType::DOCUMENT,
        self::EPC_GRAPH => Enum\MediaType::DOCUMENT
    ];

    /**
     * @param integer $value Property type value - right move representation (integer).
     *
     * @return string|null
     */
    public static function toTheLandsiteValue(int $value): ?string
    {
        return self::$typeMatrix[$value] ?? null;
    }
}
