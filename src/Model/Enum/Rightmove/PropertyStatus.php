<?php

namespace App\Model\Enum\Rightmove;

use App\Model\Enum;

/**
 * Class PropertyType
 *
 * @package App\Model\Enum\Rightmove
 */
abstract class PropertyStatus extends Enum
{
    public const
        AVAILABLE = 1,
        SSTC = 2,
        SSTCM = 3,
        UNDER_OFFER = 4,
        RESERVED = 5,
        LET_AGREED = 6;

    protected static $names = [
        1 => 'Available',
        2 => 'SSTC (sales only)',
        3 => 'SSTCM (Scottish sales only)',
        4 => 'Under Offer (sales only)',
        5 => 'Reserved (sales only)',
        6 => 'Let Agreed (lettings only)'
    ];
    /**
     * @var array Conversion array between Rightmove and The Landsite.
     */
    private static $typeMatrix = [
        self::AVAILABLE => 0,
        self::SSTC => 1,
        self::SSTCM => 1,
        self::UNDER_OFFER => 1,
        self::RESERVED => 1,
        self::LET_AGREED => 1
    ];

    /**
     * Get names list
     *
     * @return array
     * @throws \ReflectionException Reflection Exception.
     */
    public static function getNamesList(): array
    {
        $types = self::getConstList();

        $data = [];
        foreach ($types as $key => $value) {
            $data[$value] = self::$names[$value];
        }

        return $data;
    }

    /**
     * @param integer $value Property type value - right move representation (integer).
     *
     * @return integer|null
     */
    public static function toTheLandsiteValue(int $value): ?int
    {
        return self::$typeMatrix[$value] ?? null;
    }
}
