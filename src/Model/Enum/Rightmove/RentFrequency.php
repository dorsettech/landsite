<?php

namespace App\Model\Enum\Rightmove;

use App\Model\Enum;

/**
 * Class RentFrequency
 *
 * @package App\Model\Enum\Rightmove
 */
abstract class RentFrequency extends Enum
{
    public const
        YEARLY = 1,
        QUARTERLY = 4,
        MONTHLY = 12,
        WEEKLY = 52,
        DAILY = 365;

    protected static $names = [
        1 => 'Yearly',
        4 => 'Quarterly',
        12 => 'Monthly',
        52 => 'Weekly',
        365 => 'Daily'
    ];

    /**
     * @var array Conversion array between Rightmove and The Landsite.
     */
    private static $typeMatrix = [
        self::YEARLY => Enum\PropertyPriceRentPer::YEAR,
        self::QUARTERLY => Enum\PropertyPriceRentPer::QUARTER,
        self::MONTHLY => Enum\PropertyPriceRentPer::MONTH,
        self::WEEKLY => Enum\PropertyPriceRentPer::PRICE_ON_APPLICATION,
        self::DAILY => Enum\PropertyPriceRentPer::PRICE_ON_APPLICATION
    ];

    /**
     * Get names list
     *
     * @return array
     * @throws \ReflectionException Reflection Exception.
     */
    public static function getNamesList(): array
    {
        $types = self::getConstList();

        $data = [];
        foreach ($types as $key => $value) {
            $data[$value] = self::$names[$value];
        }

        return $data;
    }

    /**
     * @param integer $value Property type value - right move representation (integer).
     *
     * @return string|null
     */
    public static function toTheLandsiteValue(int $value): ?string
    {
        return self::$typeMatrix[$value] ?? null;
    }
}
