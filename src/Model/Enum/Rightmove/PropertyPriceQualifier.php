<?php

namespace App\Model\Enum\Rightmove;

use App\Model\Enum;
use App\Model\Enum\PropertyPriceQualifier as PropertyPriceQualifierTheLandsite;

/**
 * Class PropertyPriceQualifier
 *
 * @package App\Model\Enum\Rightmove
 */
abstract class PropertyPriceQualifier extends Enum
{
    public const
        DEFAULT = 0,
        POA = 1,
        GUIDE_PRICE = 2,
        FIXED_PRICE = 3,
        OIEO = 4,
        OIRO = 5,
        SALE_BY_TENDER = 6,
        FROM = 7,
        SHARED_OWNERSHIP = 9,
        OFFERS_OVER = 10,
        PART_BUY_RENT = 11,
        SHARED_EQUITY = 12,
        OFFERS_INVITED = 15,
        COMING_SOON = 16;

    protected static $names = [
        0 => 'Default',
        1 => 'POA',
        2 => 'Guide Price',
        3 => 'Fixed Price',
        4 => 'Offers in excess of',
        5 => 'OIRO',
        6 => 'Sale By Tender',
        7 => 'From',
        9 => 'Shared Ownership',
        10 => 'Offers over',
        11 => 'Part Buy, Part Rent',
        12 => 'Shared Equity',
        15 => 'Offers Invited',
        16 => 'Coming soon'
    ];
    /**
     * @var array Conversion array between Rightmove and The Landsite.
     */
    private static $typeMatrix = [
        self::DEFAULT => PropertyPriceQualifierTheLandsite::DEFAULT,
        self::POA => PropertyPriceQualifierTheLandsite::PRICE_ON_APPLICATION,
        self::GUIDE_PRICE => PropertyPriceQualifierTheLandsite::GUIDE_PRICE,
        self::FIXED_PRICE => PropertyPriceQualifierTheLandsite::FIXED_PRICE,
        self::OIEO => PropertyPriceQualifierTheLandsite::OFFERS_IN_EXCESS_OF,
        self::OIRO => PropertyPriceQualifierTheLandsite::OFFERS_IN_THE_REGION_OF,
        self::SALE_BY_TENDER => PropertyPriceQualifierTheLandsite::SALE_BY_TENDER,
        self::FROM => PropertyPriceQualifierTheLandsite::FROM,
        self::SHARED_OWNERSHIP => PropertyPriceQualifierTheLandsite::SHARED_OWNERSHIP,
        self::OFFERS_OVER => PropertyPriceQualifierTheLandsite::OFFERS_OVER,
        self::PART_BUY_RENT => PropertyPriceQualifierTheLandsite::PART_BUY,
        self::SHARED_EQUITY => PropertyPriceQualifierTheLandsite::SHARED_EQUITY,
        self::OFFERS_INVITED => PropertyPriceQualifierTheLandsite::OFFERS_INVITED,
        self::COMING_SOON => PropertyPriceQualifierTheLandsite::COMING_SOON
    ];

    /**
     * Get names list
     *
     * @return array
     * @throws \ReflectionException Reflection Exception.
     */
    public static function getNamesList(): array
    {
        $types = self::getConstList();

        $data = [];
        foreach ($types as $key => $value) {
            $data[$value] = self::$names[$value];
        }

        return $data;
    }

    /**
     * @param integer $value   Property type value - right move representation (integer).     *
     * @param string  $purpose Property Purpose
     *
     * @return string|null
     */
    public static function toTheLandsiteValue(int $value, string $purpose): ?string
    {
        $value = self::$typeMatrix[$value] ?? null;
        if ($value === PropertyPriceQualifierTheLandsite::PART_BUY && $purpose === Enum\PropertyPurpose::RENT) {
            $value = PropertyPriceQualifierTheLandsite::PART_RENT;
        }
        return $value;
    }
}
