<?php

namespace App\Model\Enum;

use App\Model\Enum;

/**
 * Class State
 *
 * @package App\Model\Enum
 *
 * @author  Marcin Nierobiś <marcin.nierobis@econnect4u.pl>
 */
abstract class State extends Enum
{
    public const
        APPROVED = 'APPROVED',
        PENDING = 'PENDING',
        REJECTED = 'REJECTED';
}
