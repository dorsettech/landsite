<?php

namespace App\Model\Enum;

use App\Model\Enum;

/**
 * AdType
 */
abstract class AdType extends Enum
{
    /**
     * Ad Types
     */
    public const
        STANDARD = 'STANDARD',
        FEATURED = 'FEATURED',
        PREMIUM  = 'PREMIUM';

    /**
     * Get names list
     *
     * @return array
     * @throws \ReflectionException Reflection Exception.
     */
    public static function getNamesList(): array
    {
        $types = self::getConstList();

        $data = [];
        foreach ($types as $key => $value) {
            switch ($value) {
                case self::STANDARD:
                    $data[$value] = 'Standard';
                    break;
                case self::FEATURED:
                    $data[$value] = 'Featured';
                    break;
                case self::PREMIUM:
                    $data[$value] = 'Premium';
                    break;
                default:
                    $data[$value] = 'Unknown';
            }
        }

        return $data;
    }
}
