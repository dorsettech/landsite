<?php

namespace App\Model\Enum;

use App\Model\Enum;

/**
 * MediaType
 */
abstract class MediaType extends Enum
{
    public const
        IMAGE = 'IMAGE',
        VIDEO = 'VIDEO',
        DOCUMENT = 'DOCUMENT';

    /**
     * Get names list
     *
     * @return array
     * @throws \ReflectionException Reflection Exception.
     */
    public static function getNamesList(): array
    {
        $types = self::getConstList();

        $data = [];
        foreach ($types as $key => $value) {
            switch ($value) {
                case self::IMAGE:
                    $data[$value] = 'Image/Photo';
                    break;
                case self::VIDEO:
                    $data[$value] = 'YouTube video';
                    break;
                case self::DOCUMENT:
                    $data[$value] = 'Document file';
                    break;
                default:
                    $data[$value] = 'Unknown';
            }
        }

        return $data;
    }
}
