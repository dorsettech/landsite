<?php

namespace App\Model\Enum;

use App\Model\Enum;

/**
 * PropertyStatus
 */
abstract class PropertyStatus extends Enum
{
    /**
     * Property status (the equivalent of properties.status column in the database)
     */
    public const
        DRAFT     = 'DRAFT',
        PUBLISHED = 'PUBLISHED',
        EXPIRED   = 'EXPIRED';

    /**
     * Get names list
     *
     * @return array
     * @throws \ReflectionException Reflection Exception.
     */
    public static function getNamesList(): array
    {
        $types = self::getConstList();
        
        $data = [];
        foreach ($types as $key => $value) {
            switch ($value) {
                case self::DRAFT:
                    $data[$value] = 'Draft';
                    break;
                case self::PUBLISHED:
                    $data[$value] = 'Published';
                    break;
                case self::EXPIRED:
                    $data[$value] = 'Expired';
                    break;
                default:
                    $data[$value] = 'Unknown';
            }
        }
        
        return $data;
    }
}
