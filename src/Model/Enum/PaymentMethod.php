<?php

namespace App\Model\Enum;

use App\Model\Enum;

/**
 * Class PaymentMethod
 *
 * @package App\Model\Enum
 */
abstract class PaymentMethod extends Enum
{
    public const
        STRIPE = 'STRIPE',
        PURCHASE_ORDER_NUMBER = 'ORDER';
}
