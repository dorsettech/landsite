<?php

namespace App\Model\Enum;

use App\Model\Enum;

/**
 * OpeningDayType
 */
abstract class OpeningDayType extends Enum
{
    public const
        MONDAY    = 0,
        TUESDAY   = 1,
        WEDNESDAY = 2,
        THURSDAY  = 3,
        FRIDAY    = 4,
        SATURDAY  = 5,
        SUNDAY    = 6;

    /**
     * @return array
     */
    public static function getNamesList(): array
    {
        $consts = self::getConstList();
        
        $data = [];
        foreach ($consts as $key => $value) {
            switch ($value) {
                case self::SUNDAY:
                    $data[$value] = 'Sunday';
                    break;
                case self::MONDAY:
                    $data[$value] = 'Monday';
                    break;
                case self::TUESDAY:
                    $data[$value] = 'Tuesday';
                    break;
                case self::WEDNESDAY:
                    $data[$value] = 'Wednesday';
                    break;
                case self::THURSDAY:
                    $data[$value] = 'Thursday';
                    break;
                case self::FRIDAY:
                    $data[$value] = 'Friday';
                    break;
                case self::SATURDAY:
                    $data[$value] = 'Saturday';
                    break;
                default:
                    $data[$value] = 'Unknown';
            }
        }
        
        return $data;
    }

    /**
     * @return array
     */
    public static function getShortNamesList(): array
    {
        $consts = self::getConstList();

        $data = [];
        foreach ($consts as $key => $value) {
            switch ($value) {
                case self::SUNDAY:
                    $data[$value] = 'Sun';
                    break;
                case self::MONDAY:
                    $data[$value] = 'Mon';
                    break;
                case self::TUESDAY:
                    $data[$value] = 'Tu';
                    break;
                case self::WEDNESDAY:
                    $data[$value] = 'Wed';
                    break;
                case self::THURSDAY:
                    $data[$value] = 'Thu';
                    break;
                case self::FRIDAY:
                    $data[$value] = 'Fri';
                    break;
                case self::SATURDAY:
                    $data[$value] = 'Sat';
                    break;
                default:
                    $data[$value] = 'Unknown';
            }
        }

        return $data;
    }
}
