<?php

namespace App\Model\Enum;

use App\Model\Enum;

/**
 * Class PropertyPriceQualifier
 *
 * @package App\Model\Enum
 */
abstract class PropertyPriceQualifier extends Enum
{
    public const
        DEFAULT = 'D',
        PRICE_ON_APPLICATION = 'POA',
        GUIDE_PRICE = 'GP',
        FIXED_PRICE = 'FP',
        OFFERS_IN_EXCESS_OF = 'OIEO',
        OFFERS_IN_THE_REGION_OF = 'OIRO',
        SALE_BY_TENDER = 'SBT',
        FROM = 'F',
        SHARED_OWNERSHIP = 'SO',
        OFFERS_OVER = 'OO',
        PART_BUY = 'PB',
        PART_RENT = 'PR',
        SHARED_EQUITY = 'SE',
        OFFERS_INVITED = 'OI',
        COMING_SOON = 'CS',
        NOW_LET = 'NL',
        SOLD_SUBJECT_TO_CONTRACT = 'SSTC';


    /**
     * Get names list (back values and names)
     *
     * @param string|null $purpose Property Purpose (enum)
     *
     * @return array
     * @throws \ReflectionException
     */
    public static function getNamesList($purpose = null): array
    {
        $types = self::getConstList();
        $data = [];
        foreach ($types as $key => $value) {
            switch ($value) {
                case self::DEFAULT:
                    $data[$value] = 'Default';
                    break;
                case self::PRICE_ON_APPLICATION:
                    $data[$value] = 'POA';
                    break;
                case self::GUIDE_PRICE:
                    $data[$value] = 'Guide Price';
                    break;
                case self::FIXED_PRICE:
                    $data[$value] = 'Fixed Price';
                    break;
                case self::OFFERS_IN_EXCESS_OF:
                    $data[$value] = 'Offers In Excess Of';
                    break;
                case self::OFFERS_IN_THE_REGION_OF:
                    $data[$value] = 'Offers In Region Of';
                    break;
                case self::SALE_BY_TENDER:
                    $data[$value] = 'Sale by Tender';
                    break;
                case self::FROM:
                    $data[$value] = 'From';
                    break;
                case self::SHARED_OWNERSHIP:
                    $data[$value] = 'Shared Ownership';
                    break;
                case self::OFFERS_OVER:
                    $data[$value] = 'Offers over';
                    break;
                case self::PART_BUY:
                    $data[$value] = 'Part Buy';
                    break;
                case self::PART_RENT:
                    $data[$value] = 'Part Rent';
                    break;
                case self::SHARED_EQUITY:
                    $data[$value] = 'Shared Equity';
                    break;
                case self::OFFERS_INVITED:
                    $data[$value] = 'Offers Invited';
                    break;
                case self::COMING_SOON:
                    $data[$value] = 'Coming soon';
                    break;
                case self::NOW_LET:
                    if ($purpose === null || $purpose === PropertyPurpose::RENT) {
                        $data[$value] = 'Now Let';
                    }
                    break;
                case self::SOLD_SUBJECT_TO_CONTRACT:
                    if ($purpose === null || $purpose === PropertyPurpose::SALE) {
                        $data[$value] = 'SSTC';
                    }
                    break;
                default:
                    $data[$value] = '';
            }
        }

        return $data;
    }
}
