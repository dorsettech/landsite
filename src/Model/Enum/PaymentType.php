<?php

namespace App\Model\Enum;

use App\Model\Enum;

/**
 * Class PaymentType
 *
 * @package App\Model\Enum
 */
abstract class PaymentType extends Enum
{
    public const
        PROPERTY = 'PROPERTY',
        SERVICE = 'SERVICE';
}
