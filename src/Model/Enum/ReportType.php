<?php

namespace App\Model\Enum;

use App\Model\Enum;
use ReflectionException;

/**
 * ReportType
 */
abstract class ReportType extends Enum
{
    /**
     * Report Types
     */
    public const
        PAYMENTS_LIST = 0,
        LIST_OF_ACCOUNT_NAMES_AND_EMAILS = 1,
        LIST_OF_BUSINESS_PROFILES = 2,
        LIST_OF_PROPERTIES = 3;

    /**
     * Get names list
     *
     * @return array
     * @throws ReflectionException Reflection Exception.
     */
    public static function getNamesList(): array
    {
        $types = self::getConstList();

        $data = [];
        foreach ($types as $key => $value) {
            switch ($value) {
                case self::PAYMENTS_LIST:
                    $data[$value] = 'Payments List';
                    break;
                case self::LIST_OF_ACCOUNT_NAMES_AND_EMAILS:
                    $data[$value] = 'List of account names and emails';
                    break;
                case self::LIST_OF_BUSINESS_PROFILES:
                    $data[$value] = 'List of business profiles';
                    break;
                case self::LIST_OF_PROPERTIES:
                    $data[$value] = 'List of properties';
                    break;
                default:
                    $data[$value] = 'Unknown';
            }
        }

        return $data;
    }
}
