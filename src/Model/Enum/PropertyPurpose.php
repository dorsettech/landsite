<?php

namespace App\Model\Enum;

use App\Model\Enum;

/**
 * PropertyPurpose
 */
abstract class PropertyPurpose extends Enum
{
    /**
     * Property purposes
     */
    public const
        SALE = 'SALE',
        RENT = 'RENT',
        BOTH = 'BOTH';

    /**
     * Get names list
     *
     * @return array
     * @throws \ReflectionException Reflection Exception.
     */
    public static function getNamesList(): array
    {
        $types = self::getConstList();

        $data = [];
        foreach ($types as $key => $value) {
            switch ($value) {
                case self::SALE:
                    $data[$value] = 'Sale';
                    break;
                case self::RENT:
                    $data[$value] = 'Rent';
                    break;
                case self::BOTH:
                    $data[$value] = 'Both';
                    break;
                default:
                    $data[$value] = 'Unknown';
            }
        }

        return $data;
    }
}
