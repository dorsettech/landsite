<?php

namespace App\Model\Enum;

use App\Model\Enum;

/**
 * CredentialType
 */
abstract class CredentialType extends Enum
{
    /**
     * Credential types
     */
    public const
        ACC = 'ACC',
        ASS = 'ASS';

    /**
     * Get names list
     *
     * @return array
     * @throws \ReflectionException Reflection Exception.
     */
    public static function getNamesList(): array
    {
        $types = self::getConstList();

        $data = [];
        foreach ($types as $key => $value) {
            switch ($value) {
                case self::ACC:
                    $data[$value] = 'Accreditation';
                    break;
                case self::ASS:
                    $data[$value] = 'Association';
                    break;
                default:
                    $data[$value] = 'Unknown';
            }
        }

        return $data;
    }
}
