<?php

namespace App\Model\Table;

use App\Model\Entity\Credential;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Credentials Model
 *
 * @property ServiceCredentialsTable|HasMany $ServiceCredentials
 *
 * @method Credential get($primaryKey, $options = [])
 * @method Credential newEntity($data = null, array $options = [])
 * @method Credential[] newEntities(array $data, array $options = [])
 * @method Credential|bool save(EntityInterface $entity, $options = [])
 * @method Credential|bool saveOrFail(EntityInterface $entity, $options = [])
 * @method Credential patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Credential[] patchEntities($entities, array $data, array $options = [])
 * @method Credential findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class CredentialsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('credentials');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Log');

        $this->hasMany('ServiceCredentials', [
            'foreignKey' => 'credential_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('type')
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        return $validator;
    }

    /**
     * Get credential by type
     *
     * @param string $credentialType Credential type id, use types from \App\Model\Enum\CredentialType.
     * @return object Returns query object.
     */
    public function getByType(string $credentialType)
    {
        $conditions = [
            'Credentials.type' => $credentialType
        ];

        return $this->find('all', [
                'conditions' => $conditions,
        ]);
    }

    /**
     * Get credential list by type
     *
     * @param array        $conditions Credentials conditions.
     * @param integer|null $limit      Limit results, default 200.
     * @return object Returns query object.
     */
    public function getListByType(array $conditions, $limit = 200)
    {
        return $this->find('list', [
                'conditions' => $conditions,
                'order'      => ['Credentials.name' => 'ASC'],
                'limit'      => $limit
        ]);
    }
}
