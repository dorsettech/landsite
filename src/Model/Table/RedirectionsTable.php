<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Redirections Model
 */
class RedirectionsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('redirections');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Filter', ['fields' => ['Redirections.redirection_from', 'Redirections.redirect_to']]);
        $this->addBehavior('Log');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('redirection_from', 'create')
            ->notEmpty('redirection_from')
            ->add('redirection_from', 'unique', ['rule' => 'validateUnique', 'provider' => 'table', 'message' => __('Name already exists.')]);

        $validator
            ->requirePresence('redirect_to', 'create')
            ->notEmpty('redirect_to');

        return $validator;
    }
}
