<?php

/**
 * @author Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-06)
 * @version 1.0
 */

namespace App\Model\Table;

use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Routing\Router;
use Cake\Utility\Text;
use Cake\Validation\Validator;

/**
 * Pages Model
 */
class PagesTable extends AppTable
{
    /**
     * Template location
     *
     * @var string
     */
    private $dirLayouts = '../src/Template/Frontend/Pages';

    /**
     * Excluded layouts
     *
     * @var array
     */
    private $excludeLayouts = ['ajax', 'error', 'error_layout'];

    /**
     * Base layout extension
     *
     * @var string
     */
    public $layoutExtension = 'ctp';

    /**
     * Initialize
     *
     * @param array $config Additional config.
     *
     * @return void
     */
    public function initialize(array $config)
    {

        parent::initialize($config);

        $this->setTable('pages');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tree');
        //$this->addBehavior('Translate', ['fields' => ['urlname', 'name', 'redirection', 'seo_title', 'seo_description', 'seo_keywords']]);
        $this->addBehavior('Log');
        $this->addBehavior('Filter', ['fields' => ['Pages.name', 'Pages.urlname']]);
        $this->addBehavior('Upload', ['fields' => ['menu_icon', 'og_image'], 'directory' => 'pages']);
        $this->addBehavior('SoftDelete');

        $this->belongsTo('Menus', [
            'foreignKey' => 'menu_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ParentPages', [
            'className' => 'Pages',
            'foreignKey' => 'parent_id'
        ]);
        $this->belongsTo('PageTypes', [
            'foreignKey' => 'page_type_id'
        ]);
        $this->hasMany('Contents', [
            'foreignKey' => 'page_id'
        ]);
        $this->hasMany('PageFiles', [
            'foreignKey' => 'page_id'
        ]);
        $this->hasMany('ChildPages', [
            'className' => 'Pages',
            'foreignKey' => 'parent_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence(['name', 'position', 'name', 'layout', 'page_type_id'], 'create');

        $validator
            ->integer('lft')
            ->allowEmpty('lft');

        $validator
            ->integer('rght')
            ->allowEmpty('rght');

        $validator
            ->integer('position')
            ->notEmpty('position');

        $validator
            ->allowEmpty('urlname')
            ->add('urlname', 'unique', ['rule' => 'validateUnique', 'provider' => 'table', 'message' => __('Urlname has to be unique.')]);

        $validator
            ->notEmpty('name');

        $validator
            ->notEmpty('layout');

        $validator
            ->notEmpty('page_type_id');

        $validator
            ->allowEmpty('redirection');

        $validator
            ->allowEmpty('target_blank');

        $validator
            ->allowEmpty('seo_title');

        $validator
            ->allowEmpty('seo_keywords');

        $validator
            ->allowEmpty('seo_description');

        $validator
            ->allowEmpty('seo_priority');

        $validator
            ->allowEmpty('og_image');

        $validator
            ->allowEmpty('ip');

        $validator
            ->allowEmpty('depth');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['menu_id'], 'Menus'));
        $rules->add($rules->existsIn(['parent_id'], 'ParentPages'));
        $rules->add($rules->isUnique(['urlname']));

        return $rules;
    }

    /**
     * Before Marshal
     *
     * @param Event        $event   Event.
     * @param \ArrayObject $data    Array data.
     * @param \ArrayObject $options Additional options.
     *
     * @return void
     */
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        $this->checkAndSetUrlname($data);
    }

    /**
     * Set default value for `urlname` field based on `name` or `title` value
     *
     * @param array|object $data Data to analyze.
     * @return array
     */
    public function checkAndSetUrlname($data)
    {
        if (isset($data['urlname']) && empty($data['urlname'])) {
            $data['urlname'] = $this->setUrlname(isset($data['name']) ? $data['name'] : (isset($data['title']) ? $data['title'] : ''));
            $request = Router::getRequest();
            $urlname = $request->getData('urlname');
            if (isset($urlname)) {
                $request->withData('urlname', $data['urlname']);
            }
        }

        return $data;
    }

    /**
     * Set urlname
     *
     * @param string|null $name    Slug name.
     * @param array       $options Additional options.
     *
     * @return string
     */
    public function setUrlname($name, array $options = [])
    {
        $separator = '-';
        $prefix = '';

        if (!empty($options)) {
            if (isset($options['separator'])) {
                $separator = $options['separator'];
            }
            if (isset($options['prefix'])) {
                $prefix = $options['prefix'];
            }
        }

        return Text::slug(strtolower($prefix . $separator . str_replace($prefix, '', $name)));
    }

    /**
     * Get menus for use in select dropdowns
     *
     * @return object
     */
    public function getPagesList()
    {
        return $this->find('treeList', [
            'limit' => 200
        ]);
    }

    /**
     * Get parent pages list
     *
     * @param string|null $menuId Menu id.
     * @return Query
     */
    public function getParentPagesList($menuId = '')
    {
        $conditions = [];
        if (!empty($menuId) && is_numeric($menuId)) {
            $conditions['ParentPages.menu_id'] = $menuId;
        }
        $conditions['ParentPages.active'] = 1;

        return $this->ParentPages->find('treeList', [
            'conditions' => $conditions,
            'order' => ['ParentPages.position' => 'ASC'],
            'limit' => 200
        ]);
    }

    /**
     * Get page details
     *
     * @param integer|null $id Page id.
     *
     * @return array
     */
    public function getPageDetails($id = 0)
    {
        return $this->get($id, [
            'contain' => ['Menus', 'ParentPages', 'Contents']
        ]);
    }

    /**
     * Get layouts list from layouts location
     *
     * @param string|null $dirLayouts Optional path to layouts folder.
     * @param array       $exclude    Optional array with layouts to exclude.
     * @return array|false
     */
    public function getLayoutsList($dirLayouts = '', array $exclude = [])
    {
        $files = $this->readLayoutsDir($dirLayouts, $exclude);

        if (!empty($files)) {
            return $this->parseLayoutFiles($files);
        }

        return false;
    }

    /**
     * Get layouts with details
     *
     * @param string|null $dirLayouts Dir layout.
     * @param array       $exclude    Exclude elements.
     * @return array|boolean
     */
    public function getLayoutsWithDetails($dirLayouts = '', array $exclude = [])
    {
        $files = $this->readLayoutsDir($dirLayouts, $exclude);

        if (!empty($files)) {
            $layouts = $this->parseLayouts($files);
        } else {
            $layouts = false;
        }

        return $layouts;
    }

    /**
     * Read layouts dir
     *
     * @param string|null $dirLayouts Dir layout.
     * @param array       $exclude    Exclude elements.
     * @return array
     */
    public function readLayoutsDir($dirLayouts = '', array $exclude = [])
    {
        if (empty($dirLayouts)) {
            $dirLayouts = $this->dirLayouts;
        }

        if (empty($exclude)) {
            $exclude = $this->excludeLayouts;
        }
        $excludeExpr = implode("|", $exclude);

        $dir = new Folder($dirLayouts);
        $files = $dir->find('^(?!' . $excludeExpr . '\.' . $this->layoutExtension . ')(.*)\.' . $this->layoutExtension);

        return $files;
    }

    /**
     * Parse layout files
     *
     * @param array $files List of layouts.
     * @return array
     */
    public function parseLayoutFiles(array $files = [])
    {
        $layouts = [];
        foreach ($files as $file) {
            $layout = str_replace('.' . $this->layoutExtension, '', $file);
            $details = $this->getLayoutDetails($file);
            $layouts[$layout] = isset($details['name']) ? $details['name'] : ucfirst($layout);
        }

        return $layouts;
    }

    /**
     * Parse layouts
     *
     * @param array $files List of layouts.
     * @return array
     */
    public function parseLayouts(array $files = [])
    {
        $layouts = [];

        foreach ($files as $file) {
            $layout = str_replace('.' . $this->layoutExtension, '', $file);
            $details = $this->getLayoutDetails($file);
            if (!isset($details['name'])) {
                $details['name'] = ucfirst($layout);
            }
            $layouts[$layout] = $details;
            $layouts[$layout]['file'] = $file;
        }

        return $layouts;
    }

    /**
     * Get layout details. Read layout file and parse it
     *
     * @param string|null $layout Layout name (with or withoug extension).
     *
     * @return string
     */
    public function getLayoutDetails($layout = '')
    {
        $layoutFile = new File($this->dirLayouts . DS . $layout . '.' . $this->layoutExtension);

        if (!$layoutFile->exists()) {
            return false;
        }
        $layoutContent = $layoutFile->read();
        $layoutFile->close();

        return $this->parseLayout($layoutContent);
    }

    /**
     * Get layout contents list
     *
     * @param string|null $layout Layout name.
     *
     * @return array
     */
    public function getLayoutContentsList($layout = '')
    {
        $contents = [];
        $layoutDetails = $this->getLayoutDetails($layout);
        if (isset($layoutDetails['contents'])) {
            foreach ($layoutDetails['contents'] as $content) {
                $temp = explode("|", $content);
                $filter = isset($temp[1]) ? $temp[1] : 'default';
                $content = $this->getCleanName($content);
                $contents[$content] = [$content, $filter];
            }
        }

        return $contents;
    }

    /**
     * Gets the string part before first occurence of "|" character
     *
     * @param string|null $string String.
     *
     * @return string
     */
    public function getCleanName($string)
    {
        return strpos($string, "|") ? substr($string, 0, strpos($string, "|")) : $string;
    }

    /**
     * Parses layout content and gets layout name and available tags
     *
     * @param string|null $layoutContent Layout content.
     * @return array
     */
    public function parseLayout($layoutContent = '')
    {
        $details = $contents = [];

        if (preg_match("#//(.*)layout:([ ]*)([A-Za-z0-9 _-|]+)#", $layoutContent)) {
            $details['name'] = trim(preg_replace("#(.*)(//)([ ]*)(layout:)([ ]*)(.*?)(\n|\?>)(.*)#is", '$6', $layoutContent));
        }

        $details['contents'] = [];
        if (preg_match_all("#content\.([a-zA-Z0-9_-]+)([|]*)([a-zA-Z0-9_-]*)#", $layoutContent, $contents)) {
            for ($i = 0; $i < count($contents); $i++) {
                for ($j = 0; $j < count($contents[$i]); $j++) {
                    if (!isset($contentsList[$j])) {
                        $contentsList[$j] = '';
                    }
                    if ($i == 0) {
                        continue;
                    }
                    $contentsList[$j] .= $contents[$i][$j];
                }
            }
            $details['contents'] = $contentsList;
            $details['contents'] = $this->sortContents($details['contents']);
        }

        $details['menus'] = [];
        $menuList = [];
        if (preg_match_all("#menu\.([a-zA-Z0-9_-]+)([|]*)([a-zA-Z0-9_-]*)([|]*)([a-zA-Z0-9_-|.]*)#", $layoutContent, $menus)) {
            for ($i = 0; $i < count($menus); $i++) {
                for ($j = 0; $j < count($menus[$i]); $j++) {
                    if (!isset($menuList[$j])) {
                        $menuList[$j] = '';
                    }
                    if ($i == 0) {
                        continue;
                    }
                    $menuList[$j] .= $menus[$i][$j];
                }
            }
            $details['menus'] = $menuList;
        }

        return $details;
    }

    /**
     * Sort contents array
     *
     * @param array $array Array data.
     *
     * @return array
     */
    public function sortContents(array $array = [])
    {
        sort($array);
        $mainKey = array_search('main', $array);
        if (is_numeric($mainKey)) {
            $main = array_splice($array, $mainKey, 1);
            array_unshift($array, $main[0]);
        }

        return $array;
    }

    /**
     * Get contents layout
     *
     * @param array $contents Array contents.
     *
     * @return array
     */
    public function getContentsLayout(array $contents = [])
    {
        $layoutContent = [];
        if (is_array($contents)) {
            foreach ($contents as $key => $content) {
                $layoutContent[$content->content_name] = $key;
            }
        }

        return $layoutContent;
    }

    /**
     * Generate form contents
     *
     * @param array $list Templates list.
     *
     * @return array
     */
    public function generateFormContents(array $list = [])
    {
        $formElements = [];
        foreach ($list as $element) {
            $file = include __DIR__ . '/../../Template/ContentsTemplates/' . $element[1] . '.' . $this->layoutExtension;
            $formElements[$element[0]] = $file;
        }
        return $formElements;
    }

    /**
     * Checks if urlname is valid
     *
     * @param string|null  $urlname Urlname.
     * @param integer|null $id      Page id.
     * @return boolean
     */
    public function checkUrlname($urlname = '', $id = 0)
    {
        $check = $this->find('all')->where(['Pages.urlname' => $urlname, 'Pages.id !=' => $id])->toArray();
        if (empty($check)) {
            return true;
        }

        return false;
    }

    /**
     * Get index page
     *
     * @return array
     */
    public function getIndexPage()
    {
        return $this->get(1, [
            'contain' => ['Menus', 'Contents', 'ParentPages', 'PageFiles' => function ($q) {
                return $q->where(['active' => 1])->order(['type' => 'ASC', 'position+0 ASC']);
            }]
        ]);
    }

    /**
     * Get Subpage
     *
     * @param array $requestedURL Requested URLs.
     *
     * @return array
     */
    public function getSubpage(array $requestedURL = [])
    {
        $lastPage = count($requestedURL) - 1;
        $page = $this->find('all')
                     ->contain(['Menus', 'Contents', 'ParentPages',
                                   'PageFiles' => function ($q) {
                                       return $q->where(['active' => 1])->order(['type' => 'ASC', 'position+0 ASC']);
                                   }])
                     ->where(['Pages.urlname' => $requestedURL[$lastPage], 'Pages.active' => 1])
                     ->first();

        if ($page !== null) {
            $pagesList = explode('.', $page->ip);
            if (count($pagesList) > 1) {
                unset($pagesList[count($pagesList) - 1], $requestedURL[$lastPage]);
            }
            if (count($requestedURL) !== count($pagesList)) {
                $page = null;
            } else {
                foreach ($pagesList as $key => $ip) {
                    $prevPage = $this->find('all')
                                     ->where(['Pages.id' => $ip, 'Pages.urlname' => $requestedURL[$key], 'Pages.active' => 1])
                                     ->first();
                    if ($prevPage === null) {
                        return null;
                    }
                }
            }
        }

        return $page;
    }

    /**
     * Get page by IP address
     *
     * @param string|null $ip Ip address.
     * @return array
     */
    public function getPageByIP($ip)
    {
        return $this->find('all')->where(['Pages.ip' => $ip])->first();
    }
}
