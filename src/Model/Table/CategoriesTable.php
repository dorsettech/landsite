<?php

namespace App\Model\Table;

use App\Model\Entity\Category;
use App\Model\Enum\AdType;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Categories Model
 *
 * @property ArticlesTable|HasMany $Articles
 * @property ServiceSearchesTable|HasMany $ServiceSearches
 * @property ServicesTable|HasMany $Services
 *
 * @method Category get($primaryKey, $options = [])
 * @method Category newEntity($data = null, array $options = [])
 * @method Category[] newEntities(array $data, array $options = [])
 * @method Category|bool save(EntityInterface $entity, $options = [])
 * @method Category|bool saveOrFail(EntityInterface $entity, $options = [])
 * @method Category patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Category[] patchEntities($entities, array $data, array $options = [])
 * @method Category findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class CategoriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('categories');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Upload', ['fields' => ['image' => ['randomName' => true]], 'directory' => 'categories']);
        $this->addBehavior('Log');

        $this->hasMany('Articles', [
            'foreignKey' => 'category_id'
        ]);
        $this->hasMany('ServiceSearches', [
            'foreignKey' => 'category_id'
        ]);
        $this->hasMany('Services', [
            'foreignKey' => 'category_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('description')
            ->allowEmpty('description');

        $validator
            ->scalar('slug')
            ->maxLength('slug', 255)
            ->requirePresence('slug', 'create')
            ->notEmpty('slug')
            ->add('slug', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('image');

        $validator
            ->integer('position')
            ->requirePresence('position', 'create')
            ->notEmpty('position');

        $validator
            ->boolean('is_recommended')
            ->requirePresence('is_recommended', 'create')
            ->notEmpty('is_recommended');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['slug']));

        return $rules;
    }

    /**
     * Find premium services grouped by categories to feed the properties
     * listing sidebar.
     *
     * @param array|null $data Query data for conditions.
     * @return query
     */
    public function findFeaturedServicesGroupedByCategories(array $data = [])
    {
        $searchFunction = function ($q) use ($data) {
            $tableLocator  = TableRegistry::getTableLocator();
            $servicesModel = $tableLocator->get('Services');

            $conditions                     = $servicesModel->getDefaultConditions();
            $conditions['Services.ad_type'] = AdType::FEATURED;
            if (!empty($data['location'])) {
                $conditions[]                   = $servicesModel->getLocationConditions($q, $data);
            }

            return $q->where($conditions);
        };

        $query = $this
            ->find('all')
            ->contain(['Services' => $searchFunction])
            ->matching('Services', $searchFunction)
            ->group('Categories.id');

        return $this->prepareServicesForDisplaying($query);
    }

    /**
     * Prepare premium services for displaying it in the sidebar.
     *
     * @param Query $query Categories finidng query.
     *
     * @return array
     */
    public function prepareServicesForDisplaying(Query $query)
    {
        $array = [];
        $row   = 0;

        foreach ($query as $key => $category) {
            $array[$row]['categories'][$category->name] = $category;
            $array[$row]['services'][$category->name]   = $category->services;
            if (($key + 1) % 4 === 0) {
                $row++;
            }
        }

        return $array;
    }

    /**
     * Related categories list
     *
     * @param array $categoriesIds Categories ids array.
     * @return object
     */
    public function relatedCategoriesList(array $categoriesIds = []): object
    {
        return $this->find('all', [
                'conditions' => ['id IN' => $categoriesIds]
        ]);
    }

    /**
     * Get category name for Seo Title
     *
     * @param array $queryData Query data.
     * @return string
     */
    public function getCategoryNameForSeoTitle(array $queryData)
    {
        $name = '';
        if (!empty($queryData['service_category'])) {
            $category = $this->get($queryData['service_category']);
            if (!empty($category)) {
                $name = $category->name;
            }
        }
        return $name;
    }
}
