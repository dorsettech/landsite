<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Menus Model
 */
class MenusTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('menus');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Log');
        $this->addBehavior('Upload', ['fields' => ['image'], 'directory' => 'menus']);
 
        $this->hasMany('Pages', [
            'foreignKey' => 'menu_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param  Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence(['position', 'urlname', 'name'], 'create');

        $validator
            ->integer('position')
            ->notEmpty('position');

        $validator
            ->allowEmpty('urlname')
            ->add('urlname', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('image');

        $validator
            ->notEmpty('name');

        return $validator;
    }

    /**
     * Get menus for use in select dropdowns
     *
     * @param array $opts Additional options.
     *
     * @return Query
     */
    public function getMenusList(array $opts = [])
    {
        $options = [
            'order' => [
                'Menus.position+0' => 'ASC',
                'Menus.name' => 'ASC',
            ],
            'limit' => 200
        ];

        $options += $opts;

        return $this->find('list', $options);
    }

    /**
     * Get sidebar menus
     *
     * @return array
     */
    public function getSidebarMenus()
    {
        $menus = $this->find('all', [
            'order' => [
                'Menus.position+0' => 'ASC',
                'Menus.name' => 'ASC',
            ],
            'limit' => 200
        ]);

        $sidebarMenus = [];
        foreach ($menus as $menu) {
            $sidebarMenus[] = [
                "controller" => "Menus",
                "action" => "view",
                "label" => h($menu['name']),
                "params" => [
                    $menu['urlname']
                ],
                "enable" => true
            ];
        }

        return $sidebarMenus;
    }

    /**
     * Get menu with page by specific urlname
     *
     * @param string|null $urlname Url name.
     *
     * @return object
     */
    public function getMenuPage($urlname)
    {
        return $this->find('all', [
            'contain' => [
                'Pages' => function ($q) {
                    return $q->find('threaded')
                             ->contain(['Contents'])
                             ->order(['Pages.position' => 'asc']);
                }
            ],
            'conditions' => [
                'Menus.urlname' => $urlname
            ],
        ])->first();
    }

    /**
     * Get menu details based on ID
     *
     * @param integer|null $id Menu id.
     *
     * @return object
     */
    public function getMenuById($id)
    {
        return $this->get($id);
    }

    /**
     * Get menu details based on Urlname
     *
     * @param string|null $urlname Url name.
     *
     * @return object
     */
    public function getMenuByUrlName($urlname)
    {
        return $this->find('all', [
            'conditions' => ['Menus.urlname' => $urlname]
        ])->first();
    }

    /**
     * Get pages
     *
     * @param string|null $urlname Url name.
     * @param string|null $ip      IP.
     *
     * @return array|object|null
     */
    public function getPages($urlname, $ip)
    {
        if ($ip === "0") {
            $conditions = [
                'conditions' => [
                    'ip NOT LIKE' => "%.%"
                ]
            ];
        } else if ($ip === 'all') {
            $conditions = [
                'conditions' => []
            ];
        } else {
            $firstParent = explode('.', $ip);
            $conditions = [
                'conditions' => [
                    'OR' => [
                        ['ip LIKE' => "$ip.%"],
                        ['ip' => $ip],
                        ['ip' => $firstParent[0]]
                    ]
                ]
            ];
        }
        $conditions['contain'] = ['Contents'];

        $query = $this->find('all', [
            'contain' => [
                'Pages' => function ($q) use ($ip, $conditions) {
                    return $q
                        ->find('threaded', $conditions)
                        ->contain(['PageTypes'])
                        ->where([
                                    'Pages.active' => true,
                                    'Pages.in_menu' => true,
                                ])
                        ->order(['Pages.position' => 'asc']);
                }
            ],
            'conditions' => [
                'Menus.urlname' => $urlname
            ],
        ])->first();

        foreach ($query->pages as $page) {
            if (!empty($page->contents)) {
                $page->contents_parsed = new \stdClass();
                foreach ($page->contents as $content) {
                    $page->contents_parsed->{$content->content_name} = $content;
                }
            }
        }

        return $query;
    }
}
