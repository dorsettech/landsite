<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FormsData Model
 */
class FormsDataTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('forms_data');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Forms', [
            'foreignKey' => 'id_forms'
        ]);
        $this->hasMany('FormDataFiles', [
            'foreignKey' => 'form_data_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id_forms')
            ->allowEmpty('id_forms');

        $validator
            ->allowEmpty('data');

        $validator
            ->boolean('view')
            ->allowEmpty('view');

        $validator
            ->allowEmpty('ip');

        $validator
            ->allowEmpty('host');

        $validator
            ->allowEmpty('referer');

        return $validator;
    }

    /**
     * Get forms data
     *
     * @return object Returns all forms data query.
     */
    public function getFormsData()
    {
        return $this->find('all', [
            'contain' => [
                'Forms'
            ],
            'order' => [
                'FormsData.view' => 'ASC',
                'FormsData.created' => 'DESC'
            ]
        ]);
    }
}
