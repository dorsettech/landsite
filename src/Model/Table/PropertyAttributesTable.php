<?php

namespace App\Model\Table;

use App\Model\Entity\PropertyAttribute;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PropertyAttributes Model
 *
 * @property PropertyTypesTable|BelongsTo $PropertyTypes
 * @property PropertiesTable|BelongsToMany $Properties
 *
 * @method PropertyAttribute get($primaryKey, $options = [])
 * @method PropertyAttribute newEntity($data = null, array $options = [])
 * @method PropertyAttribute[] newEntities(array $data, array $options = [])
 * @method PropertyAttribute|bool save(EntityInterface $entity, $options = [])
 * @method PropertyAttribute|bool saveOrFail(EntityInterface $entity, $options = [])
 * @method PropertyAttribute patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method PropertyAttribute[] patchEntities($entities, array $data, array $options = [])
 * @method PropertyAttribute findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class PropertyAttributesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('property_attributes');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        
        $this->addBehavior('Upload', ['fields' => ['image'], 'directory' => 'property_attributes']);
        $this->addBehavior('Filter', ['fields' => ['PropertyAttributes.name']]); // you can customize Filter Behavior by adding fields which you need to be filtered
        $this->addBehavior('Timestamp');
        $this->addBehavior('Log');

        $this->belongsTo('PropertyTypes', [
            'foreignKey' => 'type_id'
        ]);
        $this->belongsToMany('Properties', [
            'foreignKey'       => 'property_attribute_id',
            'targetForeignKey' => 'property_id',
            'joinTable'        => 'properties_property_attributes'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('description')
            ->allowEmpty('description');

        $validator
            ->maxLength('image', 255)
            ->allowEmpty('image');

        $validator
            ->boolean('visibility')
            ->requirePresence('visibility', 'create')
            ->notEmpty('visibility');

        $validator
            ->integer('position')
            ->requirePresence('position', 'create')
            ->notEmpty('position');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['type_id'], 'PropertyTypes'));

        return $rules;
    }

    /**
     * Get by type
     *
     * @param integer|null $typeId Attribute type id.
     * @return array|null Returns array with attributes related to given type.
     */
    public function getByType($typeId = ''): ?array
    {
        return $this->find('all', [
            'fields' => ['id', 'type_id', 'name', 'description', 'image', 'visibility', 'position'],
            'conditions' => [
                'PropertyAttributes.type_id' => $typeId,
                'PropertyAttributes.visibility' => true
            ],
            'order' => ['PropertyAttributes.position' => 'ASC']
        ])
            ->enableHydration(false)
            ->toArray();
    }
}
