<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\I18n\Time;

/**
 * Loginbans Model
 */
class LoginbansTable extends AppTable
{
    /**
     * Default ban time in seconds (it can be overwritten in controller)
     *
     * @var integer
     */
    public $banTime = 900;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('loginbans');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->notEmpty('remote_addr');

        $validator
            ->allowEmpty('user');

        $validator
            ->dateTime('ban_expires')
            ->allowEmpty('ban_expires');

        return $validator;
    }

    /**
     * Set ban time in seconds
     *
     * @param integer|null $banTime Time in seconds.
     *
     * @return void
     */
    public function setBanTime($banTime = 0)
    {
        $this->banTime = $banTime;
    }

    /**
     * Get ban count of given remote address
     *
     * @param string|null $remote_addr Client IP.
     *
     * @return integer
     */
    public function getBanCount($remote_addr = '')
    {
        return $this->find('all', [
            'fields' => ['id'],
            'conditions' => [
                'remote_addr' => $remote_addr,
                'created >= ' => new Time("-" . $this->banTime . "seconds")
            ],
        ])->count();
    }

    /**
     * Checks if specific remote address has been banned
     *
     * @param string|null $remote_addr Client IP.
     *
     * @return boolean
     */
    public function checkIpBan($remote_addr = '')
    {
        $checkBans = $this->find('all', [
            'fields' => ['remote_addr', 'ban_expires'],
            'conditions' => [
                'remote_addr' => $remote_addr,
                'ban_expires >= NOW()'
            ]
        ])->count();

        return ($checkBans > 0);
    }

    /**
     * Save bad login
     *
     * @param array $data Data to save.
     *
     * @return EntityInterface|boolean
     */
    public function saveBadLogin(array $data = [])
    {
        $badlogin = $this->newEntity($data);

        return $this->save($badlogin);
    }
}
