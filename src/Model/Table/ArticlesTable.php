<?php

namespace App\Model\Table;

use App\Event\ArticleEvents;
use App\Model\Entity\Article;
use App\Model\Enum\State;
use App\Model\Enum\ArticleStatus;
use App\Model\Enum\ArticleType;
use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Articles Model
 *
 * @property UsersTable|BelongsTo      $Users
 * @property CategoriesTable|BelongsTo $Categories
 *
 * @method Article get($primaryKey, $options = [])
 * @method Article newEntity($data = null, array $options = [])
 * @method Article[] newEntities(array $data, array $options = [])
 * @method Article|bool save(EntityInterface $entity, $options = [])
 * @method Article|bool saveOrFail(EntityInterface $entity, $options = [])
 * @method Article patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Article[] patchEntities($entities, array $data, array $options = [])
 * @method Article findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ArticlesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('articles');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Log');
        $this->addBehavior('Filter', ['fields' => ['Articles.title', 'Articles.slug', 'Articles.headline', 'Articles.body'], 'controller' => 'Articles']);
        $this->addBehavior('Upload', ['fields' => ['image'], 'directory' => 'articles', 'uid' => 'Users']);

        $this->getEventManager()->on(new ArticleEvents());

        $this->belongsTo('Users', [
            'foreignKey' => 'author_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Posters', [
            'className' => 'Users',
            'foreignKey' => 'author_id', //'posted_by',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Channels', [
            'foreignKey' => 'channel_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('posted_by');

        $validator
            ->scalar('type')
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->scalar('slug')
            ->maxLength('slug', 255)
            ->requirePresence('slug', 'create')
            ->notEmpty('slug')
            ->add('slug', 'unique', [
                'rule' => 'validateUnique',
                'provider' => 'table',
                'message' => 'Insight title has to be unique. Please change the title.'
            ]);

        $validator
            ->scalar('headline')
            ->requirePresence('headline', 'create')
            ->notEmpty('headline');

        $validator
            ->scalar('body')
            ->allowEmpty('body');

        $validator
            ->allowEmpty('image');

        $validator
            ->scalar('status')
            ->allowEmpty('status');

        $validator
            ->scalar('state')
            ->allowEmpty('state');

        $validator
            ->scalar('rejection_reason')
            ->maxLength('rejection_reason', 255)
            ->allowEmpty('rejection_reason');

        $validator
            ->dateTime('publish_date')
            ->allowEmpty('publish_date');

        $validator
            ->boolean('visibility')
            ->allowEmpty('visibility');

        $validator
            ->scalar('meta_title')
            ->maxLength('meta_title', 70)
            ->allowEmpty('meta_title');

        $validator
            ->scalar('meta_description')
            ->maxLength('meta_description', 160)
            ->allowEmpty('meta_description');

        $validator
            ->scalar('meta_keywords')
            ->maxLength('meta_keywords', 255)
            ->allowEmpty('meta_keywords');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     *
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['slug']));
        $rules->add($rules->existsIn(['author_id'], 'Users'));
        $rules->add($rules->existsIn(['category_id'], 'Categories'));
        $rules->add($rules->existsIn(['channel_id'], 'Channels'));
        return $rules;
    }

    /**
     * Before Save
     *
     * @param Event           $event   Event.
     * @param EntityInterface $entity  Entity.
     * @param \ArrayObject    $options Options.
     *
     * @return EntityInterface
     */
    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        if ($entity->isDirty()) {
            if ($entity->status === ArticleStatus::PUBLISHED && empty($entity->state)) {
                $entity->state = State::PENDING;
            } elseif ($entity->status === ArticleStatus::DRAFT) {
                $entity->state = null;
            }
        }
        return $entity;
    }

    /**
     * Get article by type
     *
     * @param string $articleType Article type id, use types from \App\Model\Enum\ArticleType.
     *
     * @return object Returns query object.
     */
    public function getByType(string $articleType)
    {
        $conditions = [
            'Articles.type' => $articleType
        ];

        return $this->find('all', [
            'conditions' => $conditions,
            'order' => ['Articles.created' => 'DESC']
        ]);
    }

    /**
     * Find insights
     *
     * @param array $extraConditions Additional conditions array.
     *
     * @return query
     */
    public function findInsights(array $extraConditions = [])
    {
        $conditions = $this->getDefaultInsightsConditions();
        if (!empty($extraConditions)) {
            $conditions = array_merge($conditions, $extraConditions);
        }
        return $this
            ->find('all')
            ->contain(['Users' => ['Services']])
            ->where($conditions)
            ->order(['Articles.publish_date' => 'DESC']);
    }

    /**
     * Get default insights conditions
     *
     * @return array
     */
    public function getDefaultInsightsConditions()
    {
        $conditions = [
            'Articles.type' => ArticleType::INSIGHT,
            'Articles.status' => ArticleStatus::PUBLISHED,
            'Articles.state' => State::APPROVED,
            'Articles.visibility' => true
        ];
        return $conditions;
    }

    /**
     * Get latest insights
     *
     * @param string $extraConditions Categories ids splited with ','.
     *
     * @return query
     */
    public function getLatestInsights(string $extraConditions = '')
    {
        $categories = [0];
        if (!empty($extraConditions)) {
            if ($extraConditions === 'ALL') {
                $categories = null;
            } else {
                $categories = explode(',', $extraConditions);
            }
        }
        $defaultConditions = $this->getDefaultInsightsConditions();

        $query = $this
            ->find('all')
            ->contain(['Users' => ['Services']])
            ->where($defaultConditions);

        if (!empty($categories)) {
            $query->andWhere(['Articles.category_id IN' => $categories]);
        }

        $query
            ->order(['Articles.publish_date' => 'DESC'])
            ->limit(6);

        return $query;
    }

    /**
     * Find Case Studies
     *
     * @param array $extraConditions Additional conditions array.
     *
     * @return query
     */
    public function findCaseStudies(array $extraConditions = [])
    {
        $conditions = $this->getDefaultCaseStudiesConditions();
        if (!empty($extraConditions)) {
            $conditions = array_merge($conditions, $extraConditions);
        }
        return $this
            ->find('all')
            ->contain(['Users' => ['Services']])
            ->where($conditions)
            ->order(['Articles.publish_date' => 'DESC']);
    }

    /**
     * Get default case studies conditions
     *
     * @return array
     */
    public function getDefaultCaseStudiesConditions()
    {
        $conditions = [
            'Articles.type' => ArticleType::CASE_STUDY,
            'Articles.status' => ArticleStatus::PUBLISHED,
            'Articles.state' => State::APPROVED,
            'Articles.visibility' => true
        ];
        return $conditions;
    }

    /**
     * Find specified insight
     *
     * @param string $slug Insight urlname.
     *
     * @return query
     */
    public function findSpecifiedInsight(string $slug)
    {
        return $this
            ->find('all')
            ->contain(['Users' => ['Services']])
            ->where(['Articles.slug' => $slug])
            ->andWhere($this->getDefaultInsightsConditions())
            ->first();
    }

    /**
     * Find specified case study
     *
     * @param string $slug Case Study urlname.
     *
     * @return query
     */
    public function findSpecifiedCaseStudy(string $slug)
    {
        return $this
            ->find('all')
            ->contain(['Users' => ['Services']])
            ->where(['Articles.slug' => $slug])
            ->andWhere($this->getDefaultCaseStudiesConditions())
            ->first();
    }

    /**
     * Find related insights
     *
     * @param Article $article Insight object.
     * @param integer $limit   Limit.
     *
     * @return array
     */
    public function findRelatedInsights(Article $article, int $limit = 3)
    {
        return $this
            ->find('all')
            ->matching('Categories', function ($q) use ($article) {
                return $q->where(['Categories.id' => $article->category_id]);
            })
            ->where(['Articles.slug != ' => $article->slug])
            ->andWhere($this->getDefaultInsightsConditions())
            ->order(['Articles.publish_date' => 'DESC'])
            ->limit($limit)
            ->toArray();
    }

    /**
     * Find recent insights
     *
     * @param integer $limit Limit.
     *
     * @return array
     */
    public function findRecentInsights(int $limit = 3, int $channel_id = null)
    {
        $query = $this->find('all')
            ->contain(['Users' => ['Services']])
            ->where($this->getDefaultInsightsConditions());

        if($channel_id) {
            $query->where(['channel_id' => $channel_id]);
        }

        return $query
            ->order(['Articles.publish_date' => 'DESC'])
            ->limit($limit)
            ->toArray();
    }

    /**
     * Find recent case studies
     *
     * @param integer $limit Limit.
     *
     * @return array
     */
    public function findRecentCaseStudies(int $limit = 3, int $channel_id = null)
    {
        $query = $this->find('all')
            ->contain(['Users' => ['Services']])
            ->where($this->getDefaultCaseStudiesConditions());

        if($channel_id) {
            $query->where(['channel_id' => $channel_id]);
        }

        return $query
            ->order(['Articles.publish_date' => 'DESC'])
            ->limit($limit)
            ->toArray();

    }

    /**
     * Find Case studies by author
     *
     * @param integer $authorId Author ID.
     *
     * @return query
     */
    public function findCaseStudiesByAuthor(int $authorId)
    {
        return $this
            ->find('all')
            ->where(['Articles.author_id' => $authorId])
            ->andWhere($this->getDefaultCaseStudiesConditions())
            ->order(['Articles.publish_date' => 'DESC']);
    }

    /**
     * Find Insights by author
     *
     * @param integer $authorId Author ID.
     *
     * @return query
     */
    public function findInsightsByAuthor(int $authorId)
    {
        return $this
            ->find('all')
            ->where(['Articles.author_id' => $authorId])
            ->andWhere($this->getDefaultInsightsConditions())
            ->order(['Articles.publish_date' => 'DESC']);
    }

    /**
     * Find article with type INSIGHT by Id
     *
     * @param Query $query   Query.
     * @param array $options Options array.
     *
     * @return \Cake\ORM\Query
     */
    public function findInsightById(Query $query, array $options): Query
    {
        return $this
            ->find('all', [
                'conditions' => [
                    'type' => ArticleType::INSIGHT,
                    'id' => $options['id']
                ]
            ]);
    }

    /**
     * Find article with type CASE_STUDY by Id
     *
     * @param Query $query   Query.
     * @param array $options Options array.
     *
     * @return \Cake\ORM\Query
     */
    public function findCaseStudyById(Query $query, array $options): Query
    {
        return $this
            ->find('all', [
                'conditions' => [
                    'type' => ArticleType::CASE_STUDY,
                    'id' => $options['id']
                ]
            ]);
    }

    /**
     * Find articles for sitemap
     *
     * @return array
     */
    public function findArticlesForSitemap()
    {
        return $this
            ->find('all')
            ->select(['id', 'title', 'slug', 'image', 'modified'])
            ->where($this->getDefaultInsightsConditions())
            ->order(['Articles.publish_date' => 'DESC'])
            ->toArray();
    }

    /**
     * Find case studies for sitemap
     *
     * @return array
     */
    public function findCaseStudiesForSitemap()
    {
        return $this
            ->find('all')
            ->select(['id', 'title', 'slug', 'image', 'modified'])
            ->where($this->getDefaultCaseStudiesConditions())
            ->order(['Articles.publish_date' => 'DESC'])
            ->toArray();
    }
}
