<?php

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserDetails Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\UserDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserDetail|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserDetail findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     *
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('dashboard_type')
            ->allowEmpty('dashboard_type');

        $validator->boolean('profile_completion');

        $validator
            ->scalar('company')
            ->maxLength('company', 255)
            ->allowEmpty('company');

        $validator
            ->scalar('company_description')
            ->allowEmpty('company_description');

        $validator
            ->scalar('location')
            ->maxLength('location', 255)
            ->allowEmpty('location');

        $validator
            ->scalar('postcode')
            ->maxLength('postcode', 25)
            ->allowEmpty('postcode');

        $validator
            ->scalar('address')
            ->maxLength('address', 255)
            ->allowEmpty('address');

        $validator
            ->boolean('pref_buying')
            ->requirePresence('pref_buying', 'create')
            ->notEmpty('pref_buying');

        $validator
            ->boolean('pref_selling')
            ->requirePresence('pref_selling', 'create')
            ->notEmpty('pref_selling');

        $validator
            ->boolean('pref_professional')
            ->requirePresence('pref_professional', 'create')
            ->notEmpty('pref_professional');

        $validator
            ->boolean('pref_insights')
            ->requirePresence('pref_insights', 'create')
            ->notEmpty('pref_insights');

        $validator
            ->scalar('pref_insights_list')
            ->maxLength('pref_insights_list', 4294967295)
            ->allowEmpty('pref_insights_list');

        $validator
            ->boolean('use_billing_details')
            ->requirePresence('use_billing_details', 'create')
            ->notEmpty('use_billing_details');

        $validator
            ->boolean('marketing_agreement_1')
            ->requirePresence('marketing_agreement_1', 'create')
            ->notEmpty('marketing_agreement_1');

        $validator
            ->boolean('marketing_agreement_2')
            ->requirePresence('marketing_agreement_2', 'create')
            ->notEmpty('marketing_agreement_2');

        $validator
            ->boolean('marketing_agreement_3')
            ->requirePresence('marketing_agreement_3', 'create')
            ->notEmpty('marketing_agreement_3');

        $validator->allowEmpty('stripe_customer_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     *
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
