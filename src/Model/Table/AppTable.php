<?php

namespace App\Model\Table;

use Cake\ORM\Table;

/**
 * AppTable
 */
class AppTable extends Table
{
    public $prefix = '';
}
