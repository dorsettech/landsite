<?php
namespace App\Model\Table;

use App\Model\Entity\UserBillingAddress;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserBillingAddresses Model
 *
 * @property UsersTable|BelongsTo $Users
 *
 * @method UserBillingAddress get($primaryKey, $options = [])
 * @method UserBillingAddress newEntity($data = null, array $options = [])
 * @method UserBillingAddress[] newEntities(array $data, array $options = [])
 * @method UserBillingAddress|bool save(EntityInterface $entity, $options = [])
 * @method UserBillingAddress|bool saveOrFail(EntityInterface $entity, $options = [])
 * @method UserBillingAddress patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method UserBillingAddress[] patchEntities($entities, array $data, array $options = [])
 * @method UserBillingAddress findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class UserBillingAddressesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_billing_addresses');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->boolean('is_default')
            ->requirePresence('is_default', 'create')
            ->notEmpty('is_default');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 255)
            ->allowEmpty('first_name');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 255)
            ->allowEmpty('last_name');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 50)
            ->allowEmpty('phone');

        $validator
            ->scalar('company')
            ->maxLength('company', 255)
            ->allowEmpty('company');

        $validator
            ->scalar('postcode')
            ->maxLength('postcode', 25)
            ->allowEmpty('postcode');

        $validator
            ->scalar('address')
            ->maxLength('address', 255)
            ->allowEmpty('address');

        return $validator;
    }

    /**
     * Mandatory validation
     *
     * @param Validator $validator
     * @return Validator
     */
    public function validationMandatory(Validator $validator)
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->requirePresence('first_name')
            ->notEmpty('first_name');

        $validator
            ->requirePresence('last_name')
            ->notEmpty('last_name');

        $validator
            ->requirePresence('email')
            ->notEmpty('email');

        $validator
            ->requirePresence('phone')
            ->notEmpty('phone');

        $validator
            ->requirePresence('postcode')
            ->notEmpty('postcode');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
