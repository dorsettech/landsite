<?php

namespace App\Model\Table;

use App\Event\Payment;
use App\Event\PaymentEvents;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Payments Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Payment get($primaryKey, $options = [])
 * @method \App\Model\Entity\Payment newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Payment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Payment|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Payment|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Payment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Payment[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Payment findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PaymentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('payments');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->getEventManager()->on(new PaymentEvents());

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     *
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->decimal('amount')
            ->greaterThanOrEqual('amount', 0)
            ->requirePresence('amount', 'create')
            ->notEmpty('amount');

        $validator
            ->nonNegativeInteger('amount_pos')
            ->requirePresence('amount_pos', 'create')
            ->notEmpty('amount_pos');

        $validator
            ->decimal('vat')
            ->greaterThanOrEqual('vat', 0)
            ->requirePresence('vat', 'create')
            ->notEmpty('vat');

        $validator
            ->decimal('vat_rate')
            ->greaterThanOrEqual('vat_rate', 0)
            ->requirePresence('vat_rate', 'create')
            ->notEmpty('vat_rate');

        $validator
            ->scalar('currency')
            ->maxLength('currency', 3)
            ->requirePresence('currency', 'create')
            ->notEmpty('currency');

        $validator
            ->scalar('type')
            ->allowEmpty('type');

        $validator
            ->scalar('payload')
            ->maxLength('payload', 4294967295)
            ->allowEmpty('payload');

        $validator
            ->scalar('meta')
            ->maxLength('meta', 4294967295)
            ->allowEmpty('meta');

        $validator
            ->maxLength('invoice', 255)
            ->allowEmpty('invoice');

        $validator
            ->boolean('refunded')
            ->notEmpty('refunded');

        $validator
            ->decimal('discount')
            ->greaterThanOrEqual('discount', 0)
            ->allowEmpty('discount');

        $validator
            ->allowEmpty('discount_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     *
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
