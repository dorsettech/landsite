<?php

namespace App\Model\Table;

use App\Model\Entity\Channel;
use App\Model\Enum\AdType;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Channels Model
 *
 * @property ArticlesTable|HasMany $Articles
 * @property ServiceSearchesTable|HasMany $ServiceSearches
 * @property ServicesTable|HasMany $Services
 *
 * @method Channel get($primaryKey, $options = [])
 * @method Channel newEntity($data = null, array $options = [])
 * @method Channel[] newEntities(array $data, array $options = [])
 * @method Channel|bool save(EntityInterface $entity, $options = [])
 * @method Channel|bool saveOrFail(EntityInterface $entity, $options = [])
 * @method Channel patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Channel[] patchEntities($entities, array $data, array $options = [])
 * @method Channel findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class ChannelsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('channels');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Upload', ['fields' => ['image' => ['randomName' => true], 'og_image' => ['randomName' => true]], 'directory' => 'channels']);
        $this->addBehavior('Log');

        $this->hasMany('Articles', [
            'foreignKey' => 'channel_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('label')
            ->maxLength('label', 255)
            ->requirePresence('label', 'create')
            ->notEmpty('label');

        $validator
            ->scalar('description')
            ->allowEmpty('description');

        $validator
            ->scalar('description2')
            ->allowEmpty('description2');

        $validator
            ->scalar('sponsors')
            ->allowEmpty('sponsors');

        $validator
            ->scalar('advert1')
            ->allowEmpty('advert1');

        $validator
            ->scalar('advert2')
            ->allowEmpty('advert2');

        $validator
            ->scalar('advert3')
            ->allowEmpty('advert3');

        $validator
            ->scalar('advert4')
            ->allowEmpty('advert4');

        $validator
            ->allowEmpty('seo_title');

        $validator
            ->allowEmpty('seo_keywords');

        $validator
            ->allowEmpty('seo_description');

        $validator
            ->allowEmpty('seo_priority');

        $validator
            ->allowEmpty('og_image');

        $validator
            ->scalar('slug')
            ->maxLength('slug', 255)
            ->requirePresence('slug', 'create')
            ->notEmpty('slug')
            ->add('slug', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('image');

        $validator
            ->integer('position')
            ->requirePresence('position', 'create')
            ->notEmpty('position');

        $validator
            ->boolean('active')
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        $validator
            ->scalar('css')
            ->allowEmpty('css');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['slug']));

        return $rules;
    }

    /**
     * Get default channels conditions
     *
     * @return array
     */
    public function getDefaultChannelConditions()
    {
        $conditions = [
            'Channels.active' => true
        ];
        return $conditions;
    }

    /**
     * Find all channels
     *
     * @param string $slug Insight urlname.
     *
     * @return query
     */
    public function findChannels(int $limit = null)
    {
        return $this
            ->find('all')
            ->where($this->getDefaultChannelConditions())
            ->limit($limit)
            ->order(['Channels.position' => 'ASC']);
    }

    /**
     * Find channels insight
     *
     * @param string $slug Insight urlname.
     *
     * @return query
     */
    public function findSpecifiedChannel(string $slug)
    {
        return $this
            ->find('all')
            // ->contain(['Articles'])
            ->where(['Channels.slug' => $slug])
            ->andWhere($this->getDefaultChannelConditions())
            ->first();
    }


}
