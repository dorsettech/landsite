<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PageTypes Model
 */
class PageTypesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('page_types');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->hasMany('Pages', [
            'foreignKey' => 'page_type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param  Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->allowEmpty('model');

        $validator
            ->allowEmpty('menu');

        $validator
            ->requirePresence('sitemap', 'create')
            ->notEmpty('sitemap');

        return $validator;
    }
}
