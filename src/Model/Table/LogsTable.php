<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Network\Request;
use Cake\ORM\Entity;
use Cake\Routing\Router;
use Cake\Database\Schema\Table as Schema;

/**
 * Logs Model
 */
class LogsTable extends AppTable
{
    /**
     * Contains
     *
     * @var array
     */
    protected $contains = [
        'Users'
    ];

    /**
     * Static find params
     *
     * @var array
     */
    protected $staticFindParams = [];

    /**
     * Initialize
     *
     * @param array $config Additional config.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setTable('logs');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Filter', ['fields' => ['Logs.content', 'Logs.url', 'Logs.host']]);
        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Initialize schema
     *
     * @param Schema $schema Schema.
     *
     * @return Schema|TableSchema
     */
    protected function _initializeSchema(Schema $schema)
    {
        $schema->setColumnType('entity', 'serialize');

        return $schema;
    }

    /**
     * Get static find params
     *
     * @return array
     */
    public function getStaticFindParams()
    {
        return $this->staticFindParams;
    }

    /**
     * Set static find params
     *
     * @param array $staticFindParams Params.
     *
     * @return $this
     */
    public function setStaticFindParams(array $staticFindParams = [])
    {
        $this->staticFindParams = $staticFindParams;

        return $this;
    }

    /**
     * Add log
     *
     * @param Request     $request      Request.
     * @param string|null $message      Message.
     * @param null|object $beforeChange Before change.
     *
     * @return void
     */
    public function addLog(Request $request, $message = '', $beforeChange = null)
    {
        if ($beforeChange instanceof Entity && isset($beforeChange->user)) {
            unset($beforeChange->user);
        }
        $request = $request ?: Router::getRequest();
        $entity = $this->newEntity([
                                       'user_id' => $request->getSession()->check('Auth.User.id') ? $request->getSession()->read('Auth.User.id') : null,
                                       'ip' => $request->clientIp(),
                                       'host' => gethostbyaddr($request->clientIp()),
                                       'agent' => $request->getHeader('User-Agent'),
                                       'content' => ucfirst($message),
                                       'url' => $request->getRequestTarget()
                                   ]);
        $this->save($entity);
    }
}
