<?php

namespace App\Model\Table;

use App\Event\ServiceEvents;
use App\Event\UserEvents;
use App\Library\Geocoding;
use App\Model\Entity\Service;
use App\Model\Enum\AdType;
use App\Model\Enum\CredentialType;
use App\Model\Enum\ServiceStatus;
use App\Model\Enum\State;
use App\Settings;
use ArrayObject;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Validation\Validator;

/**
 * Services Model
 *
 * @property CategoriesTable|BelongsTo       $Categories
 * @property UsersTable|BelongsTo            $Users
 * @property ServiceAreasTable|HasMany       $ServiceAreas
 * @property ServiceCredentialsTable|HasMany $ServiceCredentials
 * @property ServiceEnquiriesTable|HasMany   $ServiceEnquiries
 * @property ServiceMediaTable|HasMany       $ServiceMedia
 * @property ServiceProductsTable|HasMany    $ServiceProducts
 * @property ServiceViewsTable|HasMany       $ServiceViews
 * @property UserSavedTable|HasMany          $UserSaved
 *
 * @method Service get($primaryKey, $options = [])
 * @method Service newEntity($data = null, array $options = [])
 * @method Service[] newEntities(array $data, array $options = [])
 * @method Service|bool save(EntityInterface $entity, $options = [])
 * @method Service|bool saveOrFail(EntityInterface $entity, $options = [])
 * @method Service patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Service[] patchEntities($entities, array $data, array $options = [])
 * @method Service findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class ServicesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('services');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

		$this->addBehavior('Filter', ['fields' => ['Users.first_name', 'Users.last_name', 'Users.email', 'Services.company']]);
        $this->addBehavior('Timestamp');
        $this->addBehavior('Log');

        $this->getEventManager()->on(new UserEvents());
        $this->getEventManager()->on(new ServiceEvents());

        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('ServiceAreas', [
            'foreignKey' => 'service_id',
            'saveStrategy' => 'replace'
        ]);
        $this->hasMany('ServiceCredentials', [
            'foreignKey' => 'service_id',
            'saveStrategy' => 'replace'
        ]);
        $this->hasMany('ServiceEnquiries', [
            'foreignKey' => 'service_id'
        ]);
        $this->hasMany('ServiceMedia', [
            'foreignKey' => 'service_id',
            'saveStrategy' => 'replace',
            'sort' => [
                'position' => 'ASC'
            ]
        ]);
        $this->hasMany('ServiceProducts', [
            'foreignKey' => 'service_id',
            'saveStrategy' => 'replace'
        ]);
        $this->hasMany('ServiceContacts', [
            'foreignKey' => 'service_id',
            'saveStrategy' => 'replace'
        ]);
        $this->hasMany('ServiceViews', [
            'foreignKey' => 'service_id'
        ]);
        $this->hasMany('UserSaved', [
            'foreignKey' => 'service_id'
        ]);
        $this->belongsToMany('Areas', [
            'joinTable' => 'service_areas',
            'className' => 'Areas',
            'foreignKey' => 'service_id',
            'targetForeignKey' => 'area_id'
        ]);
        $this->belongsToMany('Credentials', [
            'joinTable' => 'service_credentials',
            'className' => 'Credentials',
            'foreignKey' => 'service_id',
            'targetForeignKey' => 'credential_id'
        ]);
        $this->belongsToMany('Accreditations', [
            'joinTable' => 'service_credentials',
            'className' => 'Credentials',
            'foreignKey' => 'service_id',
            'targetForeignKey' => 'credential_id',
            'conditions' => ['Accreditations.type' => CredentialType::ACC]
        ]);
        $this->belongsToMany('Associations', [
            'joinTable' => 'service_credentials',
            'className' => 'Credentials',
            'foreignKey' => 'service_id',
            'targetForeignKey' => 'credential_id',
            'conditions' => ['Associations.type' => CredentialType::ASS]
        ]);
        $this->belongsToMany('Products', [
            'joinTable' => 'service_products',
            'className' => 'Products',
            'foreignKey' => 'service_id',
            'targetForeignKey' => 'product_id'
        ]);
        $this->hasMany('ServiceClicks', [
            'foreign_key' => 'service_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->integer('category_id')
            ->requirePresence('category_id', 'create')
            ->notEmpty('category_id', 'create');

        $validator
            ->integer('user_id')
            ->requirePresence('user_id', 'create')
            ->notEmpty('user_id', 'create')
            ->add('user_id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table', 'message' => __('Selected member has related company already.')]);

        $validator
            ->scalar('uid')
            ->allowEmpty('uid')
            ->add('uid', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('slug')
            ->maxLength('slug', 70)
            ->requirePresence('slug', 'create')
            ->notEmpty('slug');

        $validator
            ->scalar('company')
            ->maxLength('company', 255)
            ->requirePresence('company', 'create')
            ->notEmpty('company');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 50)
            ->allowEmpty('phone');

        $validator
            ->scalar('postcode')
            ->maxLength('postcode', 25)
            ->requirePresence('postcode', 'create')
            ->notEmpty('postcode');

        $validator
            ->decimal('lat')
            ->allowEmpty('lat');

        $validator
            ->decimal('lng')
            ->allowEmpty('lng');

        $validator
            ->scalar('description')
            ->maxLength('description', 10000)
            ->allowEmpty('description');

        $validator
            ->scalar('description_short')
            ->maxLength('description_short', 260)
            ->allowEmpty('description_short');

        $validator
            ->scalar('website_url')
            ->maxLength('website_url', 255)
            ->allowEmpty('website_url');

        $validator
            ->scalar('google_url')
            ->maxLength('google_url', 255)
            ->allowEmpty('google_url');

        $validator
            ->scalar('facebook_url')
            ->maxLength('facebook_url', 255)
            ->allowEmpty('facebook_url');

        $validator
            ->scalar('linkedin_url')
            ->maxLength('linkedin_url', 255)
            ->allowEmpty('linkedin_url');

        $validator
            ->scalar('twitter_url')
            ->maxLength('twitter_url', 255)
            ->allowEmpty('twitter_url');

        $validator
            ->scalar('hubspot_url')
            ->maxLength('hubspot_url', 255)
            ->allowEmpty('hubspot_url');

        $validator
            ->scalar('quick_win_1')
            ->maxLength('quick_win_1', 25)
            ->allowEmpty('quick_win_1');

        $validator
            ->scalar('quick_win_2')
            ->maxLength('quick_win_2', 25)
            ->allowEmpty('quick_win_2');

        $validator
            ->scalar('quick_win_3')
            ->maxLength('quick_win_3', 25)
            ->allowEmpty('quick_win_3');

        $validator
            ->time('opening_time_from')
            ->allowEmpty('opening_time_from');

        $validator
            ->time('opening_time_to')
            ->allowEmpty('opening_time_to');

        $validator
            ->allowEmpty('opening_days');

        $validator
            ->allowEmpty('opening_hours');

        $validator
            ->scalar('status');

        $validator
            ->scalar('state')
            ->allowEmpty('state');

        $validator
            ->scalar('rejection_reason')
            ->maxLength('rejection_reason', 255)
            ->allowEmpty('rejection_reason');

        $validator
            ->dateTime('publish_date')
            ->allowEmpty('publish_date');

        $validator
            ->scalar('ad_type')
            ->notEmpty('ad_type');

        $validator
            ->scalar('ad_plan_type')
            ->allowEmpty('ad_plan_type');

        $validator
            ->dateTime('ad_expiry_date')
            ->allowEmpty('ad_expiry_date');

        $validator
            ->decimal('ad_cost')
            ->greaterThanOrEqual('ad_cost', 0)
            ->notEmpty('ad_cost');

        $validator
            ->decimal('ad_total_cost')
            ->greaterThanOrEqual('ad_total_cost', 0)
            ->notEmpty('ad_total_cost');

        $validator
            ->allowEmpty('ad_basket');

        $validator
            ->nonNegativeInteger('enquiries')
            ->allowEmpty('enquiries');

        $validator
            ->boolean('visibility')
            ->allowEmpty('visibility');

        $validator
            ->nonNegativeInteger('views')
            ->allowEmpty('views');

        return $validator;
    }

    /**
     * Before Save
     *
     * @param Event           $event   Event.
     * @param EntityInterface $entity  Entity.
     * @param \ArrayObject    $options Options.
     *
     * @return EntityInterface
     */
    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options): EntityInterface
    {
        if ($entity->isDirty()) {
            if ($entity->status === ServiceStatus::PUBLISHED && empty($entity->state)) {
                $entity->state = State::PENDING;
            } elseif ($entity->status === ServiceStatus::DRAFT) {
                $entity->state = null;
            }
        }
        return $entity;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     *
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['uid']));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['category_id'], 'Categories'));

        return $rules;
    }

    /**
     * Before marshal event
     *
     * @param Event       $event   Event.
     * @param ArrayObject $data    Request data.
     * @param ArrayObject $options Additional options.
     *
     * @return void
     */
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        if (!empty($data['opening_days']) && is_array($data['opening_days'])) {
            $data['opening_days'] = implode(',', $data['opening_days']);
        }
        $credentials = [];
        if (!empty($data['accreditations']['_ids'])) {
            $credentials = array_merge($credentials, $data['accreditations']['_ids']);
        }
        if (!empty($data['associations']['_ids'])) {
            $credentials = array_merge($credentials, $data['associations']['_ids']);
        }
        $data['credentials']['_ids'] = $credentials;
    }

    /**
     * Find Services
     *
     * @param array $data Conditions data.
     *
     * @return query
     */
    public function findServices(array $data = [])
    {
        $query = $this->find('all', [
            'contain' => ['Users']
        ]);
        $query->order(["FIELD(Services.ad_type,'".AdType::FEATURED."', '".AdType::PREMIUM."', '".AdType::STANDARD."')", 'Services.modified' => 'DESC']);

        $conditions = $this->getSearchConditions($query, $data);
        $query->where($conditions);

        return $query;
    }

    /**
     * Get search conditions
     *
     * @param Query $query Database query.
     * @param array $data  Conditions data.
     *
     * @return array
     */
    public function getSearchConditions(Query $query, array $data)
    {
        $conditions = $this->getDefaultConditions();

        if (!empty($data['service_category'])) {
            $conditions['Services.category_id'] = h($data['service_category']);
        }
        if (!empty($data['keyword'])) {
            $keyword             = h($data['keyword']);
            $credentialsMatching = $this->getMatchingIds('Credentials', $keyword);
            $productsMatching    = $this->getMatchingIds('Products', $keyword);
            $ids                 = implode(',', array_merge($credentialsMatching, $productsMatching));

            $conditions['OR'] = [
                'Services.company LIKE "%'.$keyword.'%"',
                'Services.description LIKE "%'.$keyword.'%"',
            ];
            if (!empty($ids)) {
                $conditions['OR'][] = 'Services.id IN ('.implode(',', array_merge($credentialsMatching, $productsMatching)).')';
            }
        }
        if (!empty($data['location'])) {
            $conditions[] = $this->getLocationConditions($query, $data);
        }

        return $conditions;
    }

    /**
     * Get default services conditions
     *
     * @return array
     */
    public function getDefaultConditions()
    {
        return [
            'Services.status' => ServiceStatus::PUBLISHED,
            'Services.state' => State::APPROVED,
            'Services.visibility' => true
        ];
    }

    /**
     * Get ids matching given model name
     *
     * @param string $model   Model name.
     * @param string $keyword Keyword to search for.
     *
     * @return array Returns array of ID's.
     */
    public function getMatchingIds(string $model, string $keyword): array
    {
        $ids = $this->find('all', [
                'fields' => ['id'],
                'conditions' => $this->getDefaultConditions()
            ])
            ->matching($model, function ($q) use ($keyword) {
                return $q->where(['name LIKE "%'.$keyword.'%"']);
            })
            ->enableHydration(false);

        return Hash::combine($ids->toArray(), '{n}.id', '{n}.id');
    }

    /**
     * Get location conditions
     *
     * @param Query $query Database query.
     * @param array $data  Conditions data, $data['location'] is mandatory.
     *
     * @return string
     */
    public function getLocationConditions(Query $query, array $data): string
    {
        $geo    = new Geocoding(['api_key' => Settings::get('google-api.geocoding'), 'address' => h($data['location'])]);
        $coords = $geo->getCoords();
        $radius = !empty($data['range']) ? h($data['range']) : 5;
        $ids    = $this->getCoordsIds($coords, $radius);

        $conditions = 'Services.id = false';
        if (!empty($ids)) {
            $conditions = 'Services.id IN ('.implode(',', $ids).')';
            $query->order(['FIELD(Services.id, '.implode(',', $ids).')']);
        }

        return $conditions;
    }

    /**
     * Get coords ids
     *
     * Sample query:
     * "SELECT id, name, address, lat, lng, ( 3959 * acos( cos( radians($center_lat) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians($center_lng) ) + sin( radians($center_lat) ) * sin( radians( lat ) ) ) ) AS distance FROM markers HAVING distance < '$radius' ORDER BY distance LIMIT 0 , 20"
     *
     * @see https://developers.google.com/maps/solutions/store-locator/clothing-store-locator#findnearsql
     *
     * @param array        $coords Coordinates array: lat, lng.
     * @param integer|null $radius Radius.
     * @param string|null  $unit   Radius unit: miles or kilometers.
     *
     * @return array Returns array with matching ID's.
     */
    public function getCoordsIds(array $coords, $radius = 5, $unit = 'miles'): ?array
    {
        $ratio = [
            'miles' => 3959,
            'kilometers' => 6371
        ];

        $connection  = ConnectionManager::get('default');
        $calculation = '('.$ratio[$unit].' * acos(cos(radians('.$coords['lat'].')) * cos(radians(lat)) * cos(radians(lng) - radians('.$coords['lng'].')) + sin(radians('.$coords['lat'].')) * sin(radians(lat))))';
        $results     = $connection->execute("SELECT id, ".$calculation." as distance FROM services HAVING distance < ".$radius." ORDER BY distance")->fetchAll('assoc');

        return Hash::extract($results, '{n}.id');
    }

    /**
     * Get services to send in the CronEmailQueue
     *
     * @return array
     */
    public function getServicesToSend()
    {
        $defaultConditions = $this->getDefaultConditions();
        $services = $this
            ->find('all')
            ->contain(['Users'])
            ->where($defaultConditions)
            ->andWhere(['Services.send_in_email' => true])
            ->order(['Services.publish_date' => 'DESC'])
            ->limit(6);

        $counter = $services->count();
        $latest = [];

        if ($counter < 6) {
            $ids = $this->generateArrayWithIds($services);
            $latest = $this->getLatest(['Services.id NOT IN' => $ids], 6 - $counter)->toArray();
        }
        return array_merge($services->toArray(), $latest);
    }

    /**
     * Get latest services
     *
     * @param array   $extraConditions Array with extra conditions.
     * @param integer $limit           Query limit.
     *
     * @return query
     */
    public function getLatest(array $extraConditions = [], int $limit = 6)
    {
        $defaultConditions = $this->getDefaultConditions();
        $conditions        = array_merge($defaultConditions, $extraConditions);

        return $this
                ->find('all')
                ->contain(['Users'])
                ->where($conditions)
                ->order(['Services.publish_date' => 'DESC'])
                ->limit($limit);
    }

    /**
     * Find specified service by urlname
     *
     * @param string $urlname Service urlname.
     *
     * @return object
     */
    public function findSpecifiedService(string $urlname)
    {
        $serviceId = $this->getIdFromUrlname($urlname);

        $service = $this
            ->find('all')
            ->contain([
                'Users' => [
                    'UserDetails'
                ],
                'Areas',
                'Categories',
                'Credentials',
                'Products',
                'ServiceMedia',
            ])
            ->where(['Services.id' => $serviceId])
            ->andWhere($this->getDefaultConditions())
            ->mapReduce($this->serviceMapper(), $this->serviceReducer())
            ->first();

        if (!empty($service[0])) {
            $service = $service[0];
        }

        if (!empty($service->credentials)) {
            $service->credentials = $this->splitCredentialsBaseOnType($service->credentials);
        }

        return $service;
    }

    /**
     * Get Service id from urlname.
     *
     * @param string $urlname Service urlname.
     *
     * @return string
     */
    private function getIdFromUrlname(string $urlname)
    {
        $array = explode('_', $urlname);

        return $array[0];
    }

    /**
     * Service mapper
     *
     * @return callable
     */
    public function serviceMapper()
    {
        return function ($service, $key, $mapReduce) {
            $bucket = 'all';

            $service->properties_count = $this->Users->Properties->getPropertiesCount($service->user_id);

            $mapReduce->emitIntermediate($service, $bucket);
        };
    }

    /**
     * Service reducer
     *
     * @return callable
     */
    public function serviceReducer()
    {
        return function ($services, $bucket, $mapReduce) {
            $mapReduce->emit($services, $bucket);
        };
    }

    /**
     * Split credentials base on type field.
     *
     * @param array $credentials Credentials list.
     *
     * @return array
     */
    private function splitCredentialsBaseOnType(array $credentials = [])
    {
        $array = [];
        foreach ($credentials as $credential) {
            $array[$credential->type][] = $credential;
        }
        return $array;
    }

    /**
     * Find other services related by category.
     *
     * @param integer $categoryId         Category id.
     * @param integer $displayedServiceId Displayed Service id.
     *
     * @return query
     */
    public function findOtherServicesInCategory(int $categoryId, int $displayedServiceId)
    {
        return $this
                ->find('all')
                ->where([
                    'Services.category_id' => $categoryId,
                    'Services.id !=' => $displayedServiceId,
                ])
                ->andWhere($this->getDefaultConditions())
                ->order(['Services.publish_date' => 'DESC'])
                ->limit(5);
    }

    /**
     * Get details
     *
     * @param integer $id Service id.
     *
     * @return object
     */
    public function getDetails(int $id): object
    {
        return $this->get($id, [
                'contain' => ['Categories', 'Users', 'ServiceContacts']
        ]);
    }

    /**
     * Get by id and slug
     *
     * @param integer $id   Service id.
     * @param string  $slug Service slug.
     *
     * @return object
     */
    public function getByIdAndSlug(int $id, string $slug)
    {
        return $this->get($id, [
                'fields' => ['id', 'user_id', 'slug', 'company'],
                'conditions' => ['slug' => $slug]
        ]);
    }

    /**
     * Generate array with IDs
     *
     * @param query $services Services items.
     *
     * @return array
     */
    private function generateArrayWithIds(query $services)
    {
        $ids = [0 => '0'];
        if ($services->count() > 1) {
            foreach ($services as $service) {
                $ids[] = $service->id;
            }
        }
        return $ids;
    }

    /**
     * Find services for sitemap
     *
     * @return array
     */
    public function findForSitemap()
    {
        return $this
                ->find('all')
                ->select(['id', 'company', 'slug', 'modified'])
                ->where($this->getDefaultConditions())
                ->order(['Services.publish_date' => 'DESC'])
                ->toArray();
    }
}
