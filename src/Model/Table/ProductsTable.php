<?php

namespace App\Model\Table;

use App\Model\Entity\Product;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Products Model
 *
 * @property ServiceProductsTable|HasMany $ServiceProducts
 *
 * @method Product get($primaryKey, $options = [])
 * @method Product newEntity($data = null, array $options = [])
 * @method Product[] newEntities(array $data, array $options = [])
 * @method Product|bool save(EntityInterface $entity, $options = [])
 * @method Product|bool saveOrFail(EntityInterface $entity, $options = [])
 * @method Product patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Product[] patchEntities($entities, array $data, array $options = [])
 * @method Product findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class ProductsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('products');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Filter', ['fields' => ['name', 'description']]); // you can customize Filter Behavior by adding fields which you need to be filtered
        $this->addBehavior('Timestamp');
        $this->addBehavior('Log');

        $this->hasMany('ServiceProducts', [
            'foreignKey' => 'product_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 50)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->allowEmpty('description');

        return $validator;
    }
}
