<?php

namespace App\Model\Table;

use App\Model\Entity\PropertyMedia;
use App\Model\Enum\MediaType;
use App\Model\Table\PropertiesTable;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PropertyMedia Model
 *
 * @property PropertiesTable|BelongsTo $Properties
 *
 * @method PropertyMedia get($primaryKey, $options = [])
 * @method PropertyMedia newEntity($data = null, array $options = [])
 * @method PropertyMedia[] newEntities(array $data, array $options = [])
 * @method PropertyMedia|bool save(EntityInterface $entity, $options = [])
 * @method PropertyMedia|bool saveOrFail(EntityInterface $entity, $options = [])
 * @method PropertyMedia patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method PropertyMedia[] patchEntities($entities, array $data, array $options = [])
 * @method PropertyMedia findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class PropertyMediaTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('property_media');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Upload', ['fields' => ['source' => ['randomName' => true, 'type' => 'mixed']], 'directory' => 'properties', 'uid' => 'Properties.Users']);
        $this->addBehavior('Timestamp');
        $this->addBehavior('Log');

        $this->belongsTo('Properties', [
            'foreignKey' => 'property_id',
            'joinType'   => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('type')
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->requirePresence('source', 'create')
            ->notEmpty('source');

        $validator
            ->scalar('extra')
            ->allowEmpty('extra');

        $validator
            ->scalar('url')
            ->allowEmpty('url');

        $validator
            ->integer('position');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['property_id'], 'Properties'));

        return $rules;
    }

    /**
     * Get property media
     *
     * @param integer $propertyId Property id.
     * @return object
     */
    public function getPropertyMedia(int $propertyId): object
    {
        return $this->find('all', [
                'conditions' => ['property_id' => $propertyId],
                'order'      => ["FIELD(PropertyMedia.type, '" . MediaType::IMAGE . "', '" . MediaType::VIDEO . "', '" . MediaType::DOCUMENT . "')"],
        ]);
    }
}
