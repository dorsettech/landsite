<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\Model\Table;

use App\Model\Entity\Language;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Languages Model
 */
class LanguagesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('languages');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Log');
    }

    /**
     * Default validation rules.
     *
     * @param  Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence(['position', 'name', 'locale'], 'create');

        $validator
            ->integer('position')
            ->notEmpty('position');

        $validator
            ->notEmpty('name');

        $validator
            ->notEmpty('locale');

        $validator
            ->add('is_default', 'valid', ['rule' => 'boolean', 'message' => __('"Default" needs to have a boolean value.')])
            ->allowEmpty('is_default');

        return $validator;
    }

    /**
     * Check is available any language
     *
     * @return boolean
     */
    public function isAvailableAnyLanguage()
    {
        return $this->exists([]);
    }

    /**
     * Check is available any language
     *
     * @return boolean
     */
    public function isAvailableManyLanguage()
    {
        return $this->find()->where(['active' => true])->count() > 1;
    }

    /**
     * Check is lang exist
     *
     * @param string|nnull $locale Local.
     *
     * @return boolean
     */
    public function hasLanguage($locale = '')
    {
        return $this->exists(['locale' => $locale, 'active' => true]);
    }

    /**
     * Return default lang
     *
     * @return object
     */
    public function getDefaultLanguage()
    {
        return $this->find()
                    ->where(['is_default' => true])
                    ->first();
    }

    /**
     * Return collection of all avalibles langs
     *
     * @return array
     */
    public function getAvalibleLanguages()
    {
        return $this->find()
                    ->select(['locale', 'name'])
                    ->where(['active' => true])
                    ->order(['position' => 'ASC'])
                    ->toArray();
    }

    /**
     * Get avalible languages array
     *
     * @return array
     */
    public function getAvalibleLanguagesAsArray()
    {
        $tab = [];
        $languages = $this->getAvalibleLanguages();
        foreach ($languages as $lang) {
            $tab[] = $lang->locale;
        }

        return $tab;
    }
}
