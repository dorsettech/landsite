<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\Model\Table;

use Cake\Database\Schema\TableSchema;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\Datasource\EntityInterface;

/**
 * Groups Model
 */
class GroupsTable extends AppTable
{
    /**
     * Initialize schema
     * Mapping Custom Datatypes
     *
     * @param TableSchema $schema Table schema.
     * @return TableSchema
     */
    protected function _initializeSchema(TableSchema $schema)
    {
        $schema->setColumnType('permissions', 'json');
        return $schema;
    }

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('groups');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tree');
        $this->addBehavior('Log');

        $this->belongsTo('ParentGroups', [
            'className' => 'Groups',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('ChildGroups', [
            'className' => 'Groups',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('Users', [
            'foreignKey' => 'group_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param  Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('parent_id');

        $validator
            ->notEmpty('name', __('Group name is mandatory.'));

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     *
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['parent_id'], 'ParentGroups'));

        return $rules;
    }

    /**
     * After save
     *
     * @param Event           $event   Event.
     * @param EntityInterface $entity  Entity.
     * @param \ArrayObject    $options Additional options.
     *
     * @return void
     */
    public function afterSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        // $this->recover();
    }

    /**
     * Get available groups list base on user group id
     *
     * @param integer|null $userGroupId User group id.
     *
     * @return object
     */
    public function getGroupsList($userGroupId = '')
    {
        return $this->find('treeList', [
            'conditions' => $this->groupConditions($userGroupId),
            'order' => [
                'Groups.parent_id' => 'ASC',
                'Groups.id' => 'ASC'
            ]
        ]);
    }

    /**
     * Group conditions
     *
     * @param integer|null $userGroupId Group id.
     *
     * @return array
     */
    public function groupConditions($userGroupId = 0)
    {
        return ['Groups.id IN (' . implode(", ", $this->getAllowedGroupIds($userGroupId)) . ')'];
    }

    /**
     * Get allowed group ids
     *
     * @param integer|null $userGroupId Group id.
     *
     * @return array
     */
    public function getAllowedGroupIds($userGroupId = 0)
    {
        $wholeIds = $excludedIds = [];

        $wholeTree = $this->find('all')->select(['Groups.id'])->enableHydration(false)->toArray();
        $excludedChildren = $this->find('children', ['for' => $userGroupId])->select(['Groups.id'])->enableHydration(false)->toArray();

        foreach ($wholeTree as $node) {
            $wholeIds[] = $node['id'];
        }

        foreach ($excludedChildren as $node) {
            $excludedIds[] = $node['id'];
        }

        return array_diff($wholeIds, $excludedIds);
    }
}
