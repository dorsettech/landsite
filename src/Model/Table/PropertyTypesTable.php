<?php

namespace App\Model\Table;

use App\Model\Entity\PropertyType;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PropertyTypes Model
 *
 * @property PropertyTypesTable|BelongsTo $ParentPropertyTypes
 * @property PropertyTypesTable|HasMany $ChildPropertyTypes
 *
 * @method PropertyType get($primaryKey, $options = [])
 * @method PropertyType newEntity($data = null, array $options = [])
 * @method PropertyType[] newEntities(array $data, array $options = [])
 * @method PropertyType|bool save(EntityInterface $entity, $options = [])
 * @method PropertyType|bool saveOrFail(EntityInterface $entity, $options = [])
 * @method PropertyType patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method PropertyType[] patchEntities($entities, array $data, array $options = [])
 * @method PropertyType findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class PropertyTypesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('property_types');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Filter', ['fields' => ['PropertyTypes.name']]); // you can customize Filter Behavior by adding fields which you need to be filtered
        $this->addBehavior('Timestamp');
        $this->addBehavior('Log');
        $this->addBehavior('Tree');

        $this->belongsTo('ParentPropertyTypes', [
            'className' => 'PropertyTypes',
            'foreignKey' => 'parent_id'
        ]);

        $this->hasMany('ChildPropertyTypes', [
            'className' => 'PropertyTypes',
            'foreignKey' => 'parent_id'
        ]);

        /*
         * Only visible attributes
         */
        $this->hasMany('PropertyAttributes', [
            'className' => 'PropertyAttributes',
            'foreignKey' => 'type_id',
            'conditions' => [
                'PropertyAttributes.visibility' => true
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->integer('position')
            ->requirePresence('position', 'create')
            ->notEmpty('position');

        $validator
            ->boolean('visibility')
            ->requirePresence('visibility', 'create')
            ->notEmpty('visibility');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['parent_id'], 'ParentPropertyTypes'));

        return $rules;
    }
}
