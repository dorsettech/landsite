<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Database\Schema\Table as Schema;

/**
 * Contents Model
 */
class ContentsTable extends AppTable
{
    /**
     * @param Schema $schema Schema.
     *
     * @return Schema
     */
    protected function _initializeSchema(Schema $schema)
    {
        $schema->setColumnType('gallery', 'json');

        return $schema;
    }

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('contents');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Pages', [
            'foreignKey' => 'page_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param  Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('page_id')
            ->notEmpty('page_id');

        $validator
            ->integer('position')
            ->allowEmpty('position');

        $validator
            ->integer('visible')
            ->allowEmpty('visible');

        $validator
            ->allowEmpty('file');

        $validator
            ->allowEmpty('content_name');

        $validator
            ->allowEmpty('title');

        $validator
            ->allowEmpty('content');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     *
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['page_id'], 'Pages'));

        return $rules;
    }

    /**
     * Get content by name
     *
     * @param string|null $content_name Content name.
     * @param object|null $page         Page object.
     *
     * @return array
     */
    public function getContentByName($content_name, $page)
    {
        return $this->find('all', [
            'conditions' => [
                'Contents.content_name' => $content_name,
                'Contents.page_id' => isset($page->id) ? $page->id : null,
                'Contents.visible' => true
            ],
        ])->first();
    }
}
