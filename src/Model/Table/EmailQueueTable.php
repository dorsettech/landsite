<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * EmailQueue Model
 *
 * @method \App\Model\Entity\EmailQueue get($primaryKey, $options = [])
 * @method \App\Model\Entity\EmailQueue newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\EmailQueue[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\EmailQueue|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EmailQueue|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EmailQueue patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\EmailQueue[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\EmailQueue findOrCreate($search, callable $callback = null, $options = [])
 */
class EmailQueueTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('email_queue');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Filter', ['fields' => ['email', 'subject']]); // you can customize Filter Behavior by adding fields which you need to be filtered
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->requirePresence('subject', 'create')
            ->notEmpty('subject');

        $validator
            ->requirePresence('content', 'create')
            ->notEmpty('content');

        $validator
            ->boolean('is_sent')
            ->requirePresence('is_sent', 'create')
            ->notEmpty('is_sent');

        $validator
            ->allowEmpty('options');

        $validator
            ->dateTime('send_date')
            ->allowEmpty('send_date');

        return $validator;
    }

    /**
     * Find emails to send.
     *
     * @param integer $limit Query limit.
     * @return query
     */
    public function findEmailsToSend(int $limit = 30)
    {
        return $this
                ->find('all')
                ->where(['is_sent' => false])
                ->limit($limit);
    }

    /**
     * Find emails by date or all.
     *
     * @param array $query Request query.
     * @return query
     */
    public function findByDate(array $query = [])
    {
        $data = $this
            ->find('all')
            ->where(['is_sent' => true]);

        if (!empty($query)) {
            if (!empty($query['from'])) {
                $data->andWhere(['send_date >= ' => $query['from']]);
            }
            if (!empty($query['to'])) {
                $data->andWhere(['send_date <= ' => $query['to']]);
            }
        }
        $data->order(['send_date' => 'DESC']);
        
        return $data;
    }
}
