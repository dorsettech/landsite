<?php

namespace App\Model\Table;

use App\Model\Entity\Discount;
use App\Model\Table\UsersTable;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Discounts Model
 *
 * @property UsersTable|BelongsTo $Users
 *
 * @method Discount get($primaryKey, $options = [])
 * @method Discount newEntity($data = null, array $options = [])
 * @method Discount[] newEntities(array $data, array $options = [])
 * @method Discount|bool save(EntityInterface $entity, $options = [])
 * @method Discount|bool saveOrFail(EntityInterface $entity, $options = [])
 * @method Discount patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Discount[] patchEntities($entities, array $data, array $options = [])
 * @method Discount findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class DiscountsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('discounts');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        $this->addBehavior('SoftDelete');
        $this->addBehavior('Filter', ['fields' => ['Discounts.code', 'Discounts.name', 'Users.first_name', 'Users.last_name', 'Users.email']]);
        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('DiscountUses', [
            'foreignKey' => 'discount_id',
            'className'  => 'DiscountUses'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 200)
            ->allowEmpty('name');

        $validator
            ->scalar('code')
            ->maxLength('code', 24)
            ->requirePresence('code', 'create')
            ->notEmpty('code')
            ->add('code', 'unique', ['rule' => 'validateUnique', 'provider' => 'table', 'message' => __('Discount code already exists.')]);

        $validator
            ->date('date_start')
            ->allowEmpty('date_start');

        $validator
            ->date('date_end')
            ->allowEmpty('date_end');

        $validator
            ->scalar('type')
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->decimal('value')
            ->allowEmpty('value');

        $validator
            ->integer('per_customer')
            ->allowEmpty('per_customer');

        $validator
            ->integer('per_code')
            ->allowEmpty('per_code');

        $validator
            ->boolean('active')
            ->allowEmpty('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['code']));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    /**
     * Get all
     *
     * @return object
     */
    public function getAll(): object
    {
        return $this->find('all', [
                    'contain' => ['Users']
                ])
                ->mapReduce($this->discountMapper(), $this->discountReducer());
    }

    /**
     * Discounts mapper
     *
     * @return callable
     */
    public function discountMapper()
    {
        return function ($discount, $key, $mapReduce) {
            $bucket = 'all';

            $discount->times_used = $this->DiscountUses->getCount($discount->id);

            $mapReduce->emitIntermediate($discount, $bucket);
        };
    }

    /**
     * Discounts reducer
     *
     * @return callable
     */
    public function discountReducer()
    {
        return function ($discounts, $bucket, $mapReduce) {
            $mapReduce->emit($discounts, $bucket);
        };
    }
}
