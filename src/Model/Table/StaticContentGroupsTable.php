<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * StaticContentGroups Model
 */
class StaticContentGroupsTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('static_content_groups');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Filter', ['fields' => ['StaticContentGroups.name']]);
        $this->addBehavior('Log');

        $this->hasMany('StaticContents', [
            'foreignKey' => 'static_group_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }

    /**
     * Get configuration values from static content groups: Configuration
     * Contains all the values of static contents from that group
     *
     * @return object
     */
    public function getConfigurationValues()
    {
        return $this->find('all', [
            'conditions' => ['name' => 'Configuration'],
            'contain' => ['StaticContents']
        ]);
    }

    /**
     * Get sidebar layouts
     *
     * @return array
     */
    public function getSidebarLayouts()
    {
        $layouts = $this->find('all', [
            'order' => [
                'name' => 'ASC',
            ],
            'limit' => 200,
        ]);

        $sidebarLayouts = [];
        foreach ($layouts as $layout) {
            $sidebarLayouts[] = [
                "controller" => "StaticContentGroups",
                "action" => "editGroup",
                "label" => h($layout['name']),
                "params" => [
                    $layout['id']
                ],
                "enable" => true
            ];
        }

        return $sidebarLayouts;
    }
}
