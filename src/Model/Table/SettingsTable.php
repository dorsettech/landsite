<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Settings Model
 *
 * @method \App\Model\Entity\Setting get($primaryKey, $options = [])
 * @method \App\Model\Entity\Setting newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Setting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Setting|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Setting|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Setting patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Setting[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Setting findOrCreate($search, callable $callback = null, $options = [])
 */
class SettingsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('settings');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('key')
            ->maxLength('key', 255)
            ->requirePresence('key', 'create')
            ->notEmpty('key');

        $validator
            ->scalar('group')
            ->maxLength('group', 255)
            ->requirePresence('group', 'create')
            ->notEmpty('group');

        $validator
            ->scalar('value')
            ->maxLength('value', 4294967295)
            ->allowEmpty('value');

        $validator
            ->scalar('type')
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->scalar('description')
            ->allowEmpty('description');

        $validator
            ->boolean('editable')
            ->requirePresence('editable', 'create')
            ->notEmpty('editable');

        return $validator;
    }

    /**
     * Panel validation for value only
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationPanel(Validator $validator)
    {
        $validator
            ->scalar('value')
            ->maxLength('value', 4294967295)
            ->allowEmpty('value');

        return $validator;
    }

    /**
     * Get group by name
     *
     * @param string $groupName Setting group name.
     * @return object Returns query finder object.
     */
    public function getGroup(string $groupName): object
    {
        return $this->find('all', [
                'conditions' => ['Settings.group' => $groupName, 'Settings.editable' => true],
                'order'      => ['Settings.group' => 'ASC', 'Settings.key' => 'ASC']
        ]);
    }
}
