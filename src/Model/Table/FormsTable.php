<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Forms Model
 */
class FormsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('forms');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Log');
        $this->addBehavior('Filter', ['fields' => ['Forms.name']]);

        $this->hasMany('FormsData', [
            'foreignKey' => 'id_forms'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table', 'message' => __('Name already exists')]);

        $validator
            ->allowEmpty('html_class');

        $validator
            ->allowEmpty('data');

        return $validator;
    }

    /**
     * Get form data
     *
     * @param integer|null $id Form id.
     *
     * @return object
     */
    public function getForm($id)
    {
        return $this->find('all', [
            'conditions' => [
                'Forms.id' => $id
            ]
        ])->first();
    }

    /**
     * Get forms with data
     *
     * @param array $options Additional options.
     *
     * @return Query
     */
    public function getFormsWithData(array $options = [])
    {
        $conditions = [];
        if (isset($options['conditions']) && !empty($options['conditions'])) {
            $conditions = $options['conditions'];
        }

        $forms = $this->find('all', [
            'fields' => ['id', 'name'],
            'contain' => ['FormsData' => function ($q) {
                return $q->select(['id_forms', 'id', 'data', 'created']);
            }],
            'conditions' => $conditions
        ]);

        if (isset($options['matching']) && !empty($options['matching'])) {
            $forms->matching('FormsData', function ($q) use ($options) {
                return $q->where($options['matching']);
            });
        }

        return $forms;
    }
}
