<?php
/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * StaticContents Model
 */
class StaticContentsTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('static_contents');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Log');

        $this->belongsTo('StaticContentGroups', [
            'foreignKey' => 'static_group_id',
            'joinType'   => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param  Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence(['var_name', 'type', 'description'], 'create');

        $validator
            ->notEmpty('var_name')
            ->add('var_name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('value', 'update')
            ->allowEmpty('value');

        $validator
            ->notEmpty('type');

        $validator
            ->notEmpty('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating application integrity.
     *
     * @param  RulesChecker $rules The rules object to be modified.
     *
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['key']));
        $rules->add($rules->existsIn(['static_group_id'], 'StaticContentGroups'));

        return $rules;
    }

    /**
     * Get static content value
     *
     * @param string $varName Variable name.
     * @return object|null Returns static content object with limited fields or null if not found.
     */
    public function getValue($varName)
    {
        $staticContent = $this->find('all', [
                'fields'     => ['value'],
                'conditions' => ['var_name' => $varName]
            ])
            ->first();

        if (!empty($staticContent)) {
            return $staticContent->value;
        }
        
        return null;
    }
}
