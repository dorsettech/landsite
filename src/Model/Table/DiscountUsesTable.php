<?php

namespace App\Model\Table;

use App\Model\Entity\DiscountUse;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DiscountUses Model
 *
 * @property DiscountsTable|BelongsTo $Discounts
 * @property UsersTable|BelongsTo $Users
 *
 * @method DiscountUse get($primaryKey, $options = [])
 * @method DiscountUse newEntity($data = null, array $options = [])
 * @method DiscountUse[] newEntities(array $data, array $options = [])
 * @method DiscountUse|bool save(EntityInterface $entity, $options = [])
 * @method DiscountUse|bool saveOrFail(EntityInterface $entity, $options = [])
 * @method DiscountUse patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method DiscountUse[] patchEntities($entities, array $data, array $options = [])
 * @method DiscountUse findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class DiscountUsesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('discount_uses');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Discounts', [
            'foreignKey' => 'discount_id',
            'joinType'   => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['discount_id'], 'Discounts'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    /**
     * Get count
     *
     * @param  $discountId Discount id.
     * @return integer
     */
    public function getCount($discountId = ''): int
    {
        $usesQuery = $this->find('all', ['conditions' => ['DiscountUses.discount_id' => $discountId]]);
        $uses      = $usesQuery->select(['count' => $usesQuery->func()->count('*')])->first();

        return $uses->count;
    }
}
