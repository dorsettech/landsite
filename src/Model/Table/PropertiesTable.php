<?php

namespace App\Model\Table;

use App\Event\PropertyEvents;
use App\Event\UserEvents;
use App\Library\Geocoding;
use App\Model\Entity\Property;
use App\Model\Enum\AdType;
use App\Model\Enum\MediaType;
use App\Model\Enum\PropertyPurpose;
use App\Model\Enum\PropertyStatus;
use App\Model\Enum\State;
use App\Settings;
use ArrayObject;
use Cake\Database\Schema\TableSchema;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Utility\Hash;
use Cake\Validation\Validator;

/**
 * Properties Model
 *
 * @property PropertyTypesTable|BelongsTo          $PropertyTypes
 * @property UsersTable|BelongsTo                  $Users
 * @property PropertyEnquiriesTable|HasMany        $PropertyEnquiries
 * @property PropertyMediaTable|HasMany            $PropertyMedia
 * @property PropertyViewsTable|HasMany            $PropertyViews
 * @property UserSavedTable|HasMany                $UserSaved
 * @property PropertyAttributesTable|BelongsToMany $PropertyAttributes
 *
 * @method Property get($primaryKey, $options = [])
 * @method Property newEntity($data = null, array $options = [])
 * @method Property[] newEntities(array $data, array $options = [])
 * @method Property|bool save(EntityInterface $entity, $options = [])
 * @method Property|bool saveOrFail(EntityInterface $entity, $options = [])
 * @method Property patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Property[] patchEntities($entities, array $data, array $options = [])
 * @method Property findOrCreate($search, callable $callback = null, $options = [])
 */
class PropertiesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('properties');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Filter', ['fields' => ['Properties.title', 'Properties.description']]); // you can customize Filter Behavior by adding fields which you need to be filtered
        $this->addBehavior('Timestamp');
        $this->addBehavior('Log');
        $this->addBehavior('SoftDelete');

        $this->getEventManager()->on(new UserEvents());
        $this->getEventManager()->on(new PropertyEvents());

        $this->belongsTo('PropertyTypes', [
            'foreignKey' => 'type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('PropertyEnquiries', [
            'foreignKey' => 'property_id'
        ]);
        $this->hasMany('PropertyMedia', [
            'foreignKey' => 'property_id',
            'saveStrategy' => 'replace',
            'sort' => [
                'position' => 'ASC'
            ]
        ]);
        $this->hasMany('PropertyViews', [
            'foreignKey' => 'property_id'
        ]);
        $this->hasMany('UserSaved', [
            'foreignKey' => 'property_id'
        ]);
        $this->belongsToMany('PropertyAttributes', [
            'foreignKey' => 'property_id',
            'targetForeignKey' => 'property_attribute_id',
            'joinTable' => 'properties_property_attributes'
        ]);

        $this->hasMany('PropertiesPropertyAttributes', [
            'foreignKey' => 'property_id',
            'saveStrategy' => 'replace'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->integer('type_id')
            ->requirePresence('type_id', 'create')
            ->notEmpty('type_id');

        $validator
            ->integer('user_id')
            ->requirePresence('user_id', 'create')
            ->notEmpty('user_id');

        $validator
            ->allowEmpty('uid');

        $validator
            ->scalar('reference')
            ->allowEmpty('reference');

        $validator
            ->scalar('purpose')
            ->requirePresence('purpose', 'create')
            ->notEmpty('purpose');

        $validator
            ->allowEmpty('comm_use_class');

        $validator
            ->scalar('title')
            ->maxLength('title', 70)
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->scalar('slug')
            ->maxLength('slug', 70)
            ->requirePresence('slug', 'create')
            ->notEmpty('slug');

        $validator
            ->decimal('price_sale')
            ->greaterThanOrEqual('price_sale', 0)
            ->requirePresence('price_sale', 'create')
            ->notEmpty('price_sale');

        $validator
            ->decimal('price_rent')
            ->greaterThanOrEqual('price_rent', 0)
            ->requirePresence('price_rent', 'create')
            ->notEmpty('price_rent');

        $validator
            ->scalar('price_rent_per')
            ->allowEmpty('price_rent_per');

        $validator
            ->scalar('price_sale_per')
            ->allowEmpty('price_sale_per');

        $validator
            ->scalar('price_sale_qualifier')
            ->allowEmpty('price_sale_qualifier');

        $validator
            ->scalar('price_rent_qualifier')
            ->allowEmpty('price_rent_qualifier');

        $validator
            ->scalar('location')
            ->maxLength('location', 255)
            ->requirePresence('location', 'create')
            ->notEmpty('location');

        $validator
            ->decimal('lat')
            ->allowEmpty('lat');

        $validator
            ->decimal('lng')
            ->allowEmpty('lng');

        $validator
            ->scalar('postcode')
            ->maxLength('postcode', 25)
            ->requirePresence('postcode', 'create')
            ->notEmpty('postcode');

        $validator
            ->scalar('town')
            ->maxLength('town', 255)
            ->requirePresence('town', 'create')
            ->notEmpty('town');

        $validator
            ->scalar('headline')
            ->maxLength('headline', 1000)
            ->requirePresence('headline', 'create')
            ->notEmpty('headline');

        $validator
            ->scalar('description')
            ->maxLength('description', 32e3)
            ->allowEmpty('description');

        $validator
            ->scalar('website_url')
            ->maxLength('website_url', 255)
            ->allowEmpty('website_url');

        $validator
            ->scalar('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->scalar('state')
            ->allowEmpty('state');

        $validator
            ->scalar('rejection_reason')
            ->maxLength('rejection_reason', 255)
            ->allowEmpty('rejection_reason');

        $validator
            ->dateTime('publish_date')
            ->allowEmpty('publish_date');

        $validator
            ->scalar('ad_type')
            ->notEmpty('ad_type');

        $validator
            ->dateTime('ad_expiry_date')
            ->allowEmpty('ad_expiry_date');

        $validator
            ->decimal('ad_cost')
            ->greaterThanOrEqual('ad_cost', 0)
            ->notEmpty('ad_cost');

        $validator
            ->decimal('ad_total_cost')
            ->greaterThanOrEqual('ad_total_cost', 0)
            ->notEmpty('ad_total_cost');

        $validator
            ->allowEmpty('ad_basket');

        $validator
            ->allowEmpty('enquiries');

        $validator
            ->boolean('visibility')
            ->allowEmpty('visibility');

        $validator
            ->boolean('under_offer')
            ->allowEmpty('under_offer');

        $validator
            ->boolean('auction')
            ->allowEmpty('auction');

        $validator
            ->allowEmpty('deletion_reason');

        $validator
            ->allowEmpty('views');

        return $validator;
    }

    /**
     * Before Save
     *
     * @param Event           $event   Event.
     * @param EntityInterface $entity  Entity.
     * @param \ArrayObject    $options Options.
     *
     * @return EntityInterface
     */
    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options): EntityInterface
    {
        if ($entity->isDirty()) {
            if ($entity->status === PropertyStatus::PUBLISHED && empty($entity->state)) {
                $entity->state = State::PENDING;
            } elseif ($entity->status === PropertyStatus::DRAFT) {
                $entity->state = null;
            }
        }
        return $entity;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     *
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['uid']));
        $rules->add($rules->existsIn(['type_id'], 'PropertyTypes'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    /**
     * Find recent properties
     *
     * @param array $data Conditions data.
     *
     * @return query
     */
    public function findRecentProperties(array $data = [])
    {
        $query = $this->find('all', [
            'contain' => [
                'PropertyAttributes',
                'PropertyMedia' => function ($q) {
                    return $q
                        ->where(['PropertyMedia.type' => MediaType::IMAGE])
                        ->order(['PropertyMedia.position' => 'ASC']);
                },
                'Users' => ['Services']
            ],
            'conditions' => ['Properties.ad_type' => AdType::FEATURED]
        ]);

        $conditions = $this->getSearchConditions($query, $data);
        $query->where($conditions);

        return $query->order(['Properties.publish_date' => 'DESC'])->limit(5);
    }

    /**
     * Get search conditions
     *
     * @param Query $query Database query.
     * @param array $data  Conditions data.
     *
     * @return array
     */
    public function getSearchConditions(Query $query, array $data): array
    {
        $conditions = $this->getDefaultConditions();
        if (!empty($data['user_id'])) {
            $conditions['Properties.user_id'] = $data['user_id'];
        }
        if (!empty($data['purpose'])) {
            $conditions['OR'] = [
                ['Properties.purpose' => PropertyPurpose::getConstList()[h($data['purpose'])]],
                ['Properties.purpose' => PropertyPurpose::BOTH],
            ];
        }
        if (!empty($data['type'])) {
            $conditions['Properties.type_id'] = h($data['type']);
        }
        if (!empty($data['min_price'])) {
            if (empty($data['purpose'])) {
                $conditions['OR'] = [
                    ['Properties.purpose' => PropertyPurpose::SALE, 'Properties.price_sale >= ' . h($data['min_price'])],
                    ['Properties.purpose' => PropertyPurpose::RENT, 'Properties.price_rent >= ' . h($data['min_price'])],
                ];
            } elseif ($data['purpose'] == PropertyPurpose::SALE) {
                $conditions[] = 'Properties.price_sale >= ' . h($data['min_price']);
            } elseif ($data['purpose'] == PropertyPurpose::RENT) {
                $conditions[] = 'Properties.price_rent >= ' . h($data['min_price']);
            }
        }
        if (!empty($data['max_price'])) {
            if (empty($data['purpose'])) {
                $conditions['OR'] = [
                    ['Properties.purpose' => PropertyPurpose::SALE, 'Properties.price_sale <= ' . h($data['max_price'])],
                    ['Properties.purpose' => PropertyPurpose::RENT, 'Properties.price_rent <= ' . h($data['max_price'])],
                ];
            } elseif ($data['purpose'] == PropertyPurpose::SALE) {
                $conditions[] = 'Properties.price_sale <= ' . h($data['max_price']);
            } elseif ($data['purpose'] == PropertyPurpose::RENT) {
                $conditions[] = 'Properties.price_rent <= ' . h($data['max_price']);
            }
        }

        /**
         * Price order (if set) needs to be applied before distance order
         */
        if (!empty($data['sort']) && $data['sort'] == 'price_asc') {
            $query->order(['Properties.price_rent' => 'ASC', 'Properties.price_sale' => 'ASC']);
        } elseif (!empty($data['sort']) && $data['sort'] == 'price_desc') {
            $query->order(['Properties.price_sale' => 'DESC', 'Properties.price_rent' => 'DESC']);
        }

        if (!empty($data['attributes'])) {
            $ids = $this->getPropertyIdsMatchingAttributes($data);

            if (!empty($ids)) {
                $conditions[] = 'Properties.id IN (' . implode(',', $ids) . ')';
            }
        }
        if (!empty($data['location'])) {
            $conditions[] = $this->getLocationConditions($query, $data);
        }

        return $conditions;
    }

    /**
     * Get default services conditions
     *
     * @return array
     */
    private function getDefaultConditions()
    {
        return [
            'Properties.status' => PropertyStatus::PUBLISHED,
            'Properties.state' => State::APPROVED,
            'Properties.visibility' => true,
        ];
    }

    /**
     * Get property IDs matching attributes
     *
     * @param array $data Query data.
     *
     * @return array Return array with matching ID's.
     */
    public function getPropertyIdsMatchingAttributes(array $data): ?array
    {
        $conditions['OR'] = [];
        foreach ($data['attributes'] as $attributeId => $value) {
            if ($value == 1) {
                $conditions['OR'][] = ['property_attribute_id' => $attributeId, 'attribute_value' => $value];
            }
        }
        $results = $this->PropertiesPropertyAttributes->find('all', [
            'fields' => ['property_id'],
            'conditions' => $conditions
        ])->enableHydration(false);

        return Hash::extract($results->toArray(), '{n}.property_id');
    }

    /**
     * Get location conditions
     *
     * @param Query $query Database query.
     * @param array $data  Conditions data, $data['location'] is mandatory.
     *
     * @return string
     */
    public function getLocationConditions(Query $query, array $data): string
    {
        $geo = new Geocoding(['api_key' => Settings::get('google-api.geocoding'), 'address' => h($data['location'])]);
        $coords = $geo->getCoords();

        /**
         * Commenting that part as it doesn't give results B4B/client expecting, i.e. when typing location:
         * Bournemouththey are expecting results only from Bournemouth, and when matching properties by postcode
         * gives otherlocations as well (Poole, Ringwood etc) as all of that cities has postcodes begins with BH
         *
         * $postcode = $geo->getPostcode();
         * if (!empty($postcode)) {
         * $postcodeBeginning = Postcodes::getBeginning($postcode)[0];
         * $conditions[]      = 'Properties.postcode LIKE "' . $postcodeBeginning . '%"';
         * }
         */
        $sort = !empty($data['sort']) ? h($data['sort']) : '';
        $radius = !empty($data['range']) ? h($data['range']) : 5;
        $ids = $this->getCoordsIds($coords, ['radius' => $radius, 'sort' => $sort]);

        $conditions = 'Properties.id = false';
        if (!empty($ids)) {
            $conditions = 'Properties.id IN (' . implode(',', $ids) . ')';
            $query->order(['FIELD(Properties.id, ' . implode(',', $ids) . ')']);
        }

        return $conditions;
    }

    /**
     * Get coords ids
     *
     * Sample query:
     * "SELECT id, name, address, lat, lng, ( 3959 * acos( cos( radians($center_lat) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians($center_lng) ) + sin( radians($center_lat) ) * sin( radians( lat ) ) ) ) AS distance FROM markers HAVING distance < '$radius' ORDER BY distance LIMIT 0 , 20"
     *
     * @see https://developers.google.com/maps/solutions/store-locator/clothing-store-locator#findnearsql
     *
     * @param array      $coords  Coordinates array: lat, lng.
     * @param array|null $options Additional options, available keys: radius, unit, sort.
     *
     * @return array Returns array with matching ID's.
     */
    public function getCoordsIds(array $coords, $options = []): ?array
    {
        $radius = (!empty($options['radius'])) ? $options['radius'] : 10;
        $unit = (!empty($options['unit'])) ? $options['unit'] : 'miles';
        $sort = (!empty($options['sort'])) ? $options['sort'] : 'distance_asc';

        $order = 'distance ASC';
        if ($sort == 'distance_desc') {
            $order = 'distance DESC';
        }

        $ratio = [
            'miles' => 3959,
            'kilometers' => 6371
        ];

        $connection = ConnectionManager::get('default');
        $calculation = '(' . $ratio[$unit] . ' * acos(cos(radians(' . $coords['lat'] . ')) * cos(radians(lat)) * cos(radians(lng) - radians(' . $coords['lng'] . ')) + sin(radians(' . $coords['lat'] . ')) * sin(radians(lat))))';
        $sql = "SELECT id, " . $calculation . " as distance FROM properties HAVING distance < " . $radius . " ORDER BY " . $order;
        $results = $connection->execute($sql)->fetchAll('assoc');

        return Hash::extract($results, '{n}.id');
    }

    /**
     * Find Services
     *
     * @param array $data Conditions data.
     *
     * @return query
     */
    public function findProperties(array $data = [])
    {
        $userId = Router::getRequest()->getSession()->read('Auth.User.id');
        $query = $this->find('all', [
            'contain' => [
                'PropertyAttributes',
                'PropertyMedia' => function ($q) {
                    return $q
                        ->where(['PropertyMedia.type' => MediaType::IMAGE])
                        ->order(['PropertyMedia.position' => 'ASC']);
                },
                'UserSaved' => function ($q) use ($userId) {
                    return $q->where(['UserSaved.user_id' => $userId]);
                },
                'Users' => ['Services']
            ]
        ]);
        $query->order(["FIELD(Properties.ad_type,'" . AdType::FEATURED . "', '" . AdType::PREMIUM . "', '" . AdType::STANDARD . "')"]);

        $conditions = $this->getSearchConditions($query, $data);
        $query->where($conditions);

        $query->order(['Properties.created' => 'DESC']);

        return $query;
    }

    /**
     * Find specified property by urlname
     *
     * @param string $urlname Property urlname.
     *
     * @return object
     */
    public function findSpecifiedProperty(string $urlname)
    {
        $propertyId = $this->getIdFromUrlname($urlname);

        return $this
            ->find('all')
            ->contain([
                'Users' => [
                    'UserDetails',
                    'Services' => ['Users']
                ],
                'UserSaved',
                'PropertyAttributes',
                'PropertyMedia' => function ($q) {
                    return $q
                        ->order(['PropertyMedia.position' => 'ASC']);
                },
                'PropertyTypes',
            ])
            ->where(['Properties.id' => $propertyId])
            ->andWhere($this->getDefaultConditions())
            ->first();
    }

    /**
     * Get Service id from urlname.
     *
     * @param string $urlname Service urlname.
     *
     * @return string
     */
    private function getIdFromUrlname(string $urlname)
    {
        $array = explode('_', $urlname);

        return $array[0];
    }

    /**
     * Get latest properties
     *
     * @param array   $extraConditions Array with extra conditions.
     * @param integer $limit           Query limit.
     *
     * @return query
     */
    public function getLatest(array $extraConditions = [], int $limit = 6)
    {
        $defaultConditions = $this->getDefaultConditions();
        $conditions = array_merge($defaultConditions, $extraConditions);
        return $this
            ->find('all')
            ->contain([
                'PropertyAttributes',
                'PropertyMedia' => function ($q) {
                    return $q
                        ->where(['PropertyMedia.type' => MediaType::IMAGE])
                        ->order(['PropertyMedia.position' => 'ASC']);
                }
            ])
            ->where($conditions)
            ->order(['Properties.publish_date' => 'DESC'])
            ->limit($limit);
    }

    /**
     * Get properties to send in the CronEmailQueue
     *
     * @return array
     */
    public function getPropertiesToSend()
    {
        $defaultConditions = $this->getDefaultConditions();
        $properties = $this
            ->find('all')
            ->contain([
                'PropertyAttributes',
                'PropertyMedia' => function ($q) {
                    return $q
                        ->where(['PropertyMedia.type' => MediaType::IMAGE])
                        ->order(['PropertyMedia.position' => 'ASC']);
                }
            ])
            ->where($defaultConditions)
            ->andWhere(['Properties.send_in_email' => true])
            ->order(['Properties.publish_date' => 'DESC'])
            ->limit(6);

        $counter = $properties->count();
        $latest = [];

        if ($counter < 6) {
            $ids = $this->generateArrayWithIds($properties);
            $latest = $this->getLatest(['Properties.id NOT IN' => $ids], 6 - $counter)->toArray();
        }
        return array_merge($properties->toArray(), $latest);
    }

    /**
     * Get properties
     *
     * @return object
     */
    public function getProperties(): Query
    {
        return $this->find('all', [
            'contain' => [
                'PropertyTypes',
                'PropertyMedia' => function ($q) {
                    return $q
                        ->order(['PropertyMedia.position' => 'ASC'])
                        ->limit(1);
                },
                'Users' => ['UserDetails', 'Services']
            ]
        ]);
    }

    /**
     * Get property with relations
     *
     * @param integer $id Property id.
     *
     * @return Property|null
     */
    public function getProperty(int $id): ?Property
    {
        return $this->get($id, [
            'contain' => [
                'PropertyTypes',
                'PropertyAttributes',
                'PropertyMedia',
                'Users' => ['Services']
            ]
        ]);
    }

    /**
     * Get properties count
     *
     * @param integer $userId User id.
     *
     * @return integer
     */
    public function getPropertiesCount(int $userId): int
    {
        $query = $this->find()->where(['user_id' => $userId]);
        $query->select(['count' => $query->func()->count('id')]);

        return $query->first()->count;
    }

    /**
     * @param TableSchema $schema Table schema.
     *
     * @return TableSchema
     */
    protected function _initializeSchema(TableSchema $schema): TableSchema
    {
        $schema->setColumnType('comm_use_class', 'set');
        return $schema;
    }

    /**
     * Generate array with IDs
     *
     * @param query $properties Properties items.
     *
     * @return array
     */
    private function generateArrayWithIds(query $properties)
    {
        $ids = [0 => '0'];
        if ($properties->count() > 1) {
            foreach ($properties as $property) {
                $ids[] = $property->id;
            }
        }
        return $ids;
    }

    /**
     * Find properties for sitemap
     *
     * @return array
     */
    public function findForSitemap()
    {
        return $this
                ->find('all')
                ->select(['id', 'title', 'slug', 'modified'])
                ->where($this->getDefaultConditions())
                ->order(['Properties.publish_date' => 'DESC'])
                ->toArray();
    }
}
