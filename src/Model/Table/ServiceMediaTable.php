<?php

namespace App\Model\Table;

use App\Model\Entity\ServiceMedia;
use App\Model\Enum\MediaType;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ServiceMedia Model
 *
 * @property ServicesTable|BelongsTo $Services
 *
 * @method ServiceMedia get($primaryKey, $options = [])
 * @method ServiceMedia newEntity($data = null, array $options = [])
 * @method ServiceMedia[] newEntities(array $data, array $options = [])
 * @method ServiceMedia|bool save(EntityInterface $entity, $options = [])
 * @method ServiceMedia|bool saveOrFail(EntityInterface $entity, $options = [])
 * @method ServiceMedia patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method ServiceMedia[] patchEntities($entities, array $data, array $options = [])
 * @method ServiceMedia findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class ServiceMediaTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('service_media');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Upload', ['fields' => ['source' => ['randomName' => true, 'type' => 'image']], 'directory' => 'services', 'uid' => 'Services.Users']);
        $this->addBehavior('Timestamp');
        $this->addBehavior('Log');

        $this->belongsTo('Services', [
            'foreignKey' => 'service_id',
            'joinType'   => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->requirePresence('source', 'create')
            ->notEmpty('source');

        $validator
            ->scalar('extra')
            ->allowEmpty('extra');

        $validator
            ->integer('position');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['service_id'], 'Services'));

        return $rules;
    }

    /**
     * Get service media related to specific service
     *
     * @param integer $serviceId Service id.
     * @return object
     */
    public function getServiceMedia(int $serviceId): object
    {
        return $this->find('all', [
                'conditions' => ['service_id' => $serviceId],
                'order'      => ["FIELD(ServiceMedia.type, '" . MediaType::IMAGE . "', '" . MediaType::VIDEO . "', '" . MediaType::DOCUMENT . "')"],
        ]);
    }
}
