<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PropertiesPropertyAttributes Model
 *
 * @property \App\Model\Table\PropertiesTable|\Cake\ORM\Association\BelongsTo $Properties
 * @property \App\Model\Table\PropertyAttributesTable|\Cake\ORM\Association\BelongsTo $PropertyAttributes
 *
 * @method \App\Model\Entity\PropertiesPropertyAttribute get($primaryKey, $options = [])
 * @method \App\Model\Entity\PropertiesPropertyAttribute newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PropertiesPropertyAttribute[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PropertiesPropertyAttribute|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PropertiesPropertyAttribute|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PropertiesPropertyAttribute patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PropertiesPropertyAttribute[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PropertiesPropertyAttribute findOrCreate($search, callable $callback = null, $options = [])
 */
class PropertiesPropertyAttributesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('properties_property_attributes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Properties', [
            'foreignKey' => 'property_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PropertyAttributes', [
            'foreignKey' => 'property_attribute_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->boolean('attribute_value')
            ->requirePresence('attribute_value', 'create')
            ->notEmpty('attribute_value');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['property_id'], 'Properties'));
        $rules->add($rules->existsIn(['property_attribute_id'], 'PropertyAttributes'));

        return $rules;
    }
}
