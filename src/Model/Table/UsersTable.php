<?php
/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-12-13)
 * @version 1.0
 */

namespace App\Model\Table;

use App\Model\Entity\Group;
use App\Model\Entity\User;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * Users Model
 */
class UsersTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setPrimaryKey('id');
        $this->setDisplayField('full_name');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Upload', ['fields' => ['image' => ['randomName' => true, 'type' => 'image']], 'directory' => 'members', 'uid' => true]);
        $this->addBehavior('Log', ['ignore' => ['register', 'password', 'verifyEmail']]);
        $this->addBehavior('SoftDelete');
        $this->addBehavior('Filter', ['fields' => ['Users.first_name', 'Users.last_name', 'Users.email', 'UserDetails.company', 'UserDetails.postcode'], 'matching' => 'UserDetails']);

        $this->belongsTo('Groups', [
            'foreignKey' => 'group_id',
            'joinType' => 'INNER'
        ]);
        $this->hasOne('UserDetails', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('UserBillingAddresses', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Articles', [
            'foreignKey' => 'author_id'
        ]);
        $this->hasMany('Properties', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('PropertySearches', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasOne('Services', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('ServiceSearches', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('group_id', 'valid', ['rule' => 'numeric', 'message' => __('"Group ID" must be numeric')])
            ->notEmpty('group_id', __('Please provide a valid group ID'));

        $validator
            ->notEmpty('email', __('Please provide a valid e-mail address'))
            ->add('email', 'valid', ['rule' => 'email', 'message' => __('Please provide a valid e-mail address')])
            ->add('email', ['unique' => ['rule' => 'validateUnique', 'provider' => 'table', 'message' => __('This e-mail address is already present in the database')]])
            ->requirePresence('email');

        $validator
            ->add('password', 'length', ['rule' => ['minLength', 6], 'message' => __('Password need to be at least 6 characters long')])
            ->requirePresence('password', 'create')
            ->notEmpty('password', __('Please provide a valid passphrase'), 'create');

        $validator
            ->add('confirm_password', 'no-misspeling', [
                'rule' => ['compareWith', 'password'],
                'message' => __('Passwords are not equal')
            ])
            ->requirePresence('confirm_password', 'create')
            ->notEmpty('confirm_password', __('Please repeat your new password'), 'create');

        $validator
            ->allowEmpty('image');

        $validator
            ->requirePresence('first_name')
            ->notEmpty('first_name');

        $validator
            ->requirePresence('last_name')
            ->notEmpty('last_name');

        $validator
            ->allowEmpty('job_role');

        $validator
            ->allowEmpty('phone');

        return $validator;
    }

    /**
     * Passwords validation rules.
     * Checks current_password, password and confirm_password
     *
     * @param Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationPasswords(Validator $validator)
    {
        $validator
            ->add('current_password', 'checkCurrent', [
                'rule' => function ($value, $context) {
                    $query = $this->get($context['data']['id']);
                    return DefaultPasswordHasher::check($value, $query->password);
                },
                'message' => __('Current password is incorrect')
            ])
            ->requirePresence('current_password')
            ->notEmpty('current_password', __('Please provide your current password'));

        $validator
            ->add('password', 'length', ['rule' => ['minLength', 6], 'message' => __('Password need to be at least 6 characters long')])
            ->requirePresence('password')
            ->notEmpty('password', __('Please provide a valid passphrase'));

        $validator
            ->add('confirm_password', 'no-misspeling', [
                'rule' => ['compareWith', 'password'],
                'message' => __('Passwords are not equal')
            ])
            ->requirePresence('confirm_password')
            ->notEmpty('confirm_password', __('Please repeat your new password'));

        return $validator;
    }

    /**
     * Passwords validation rules for reset password ONLY.
     * Checks password and confirm_password
     *
     * @param Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationPasswordreset(Validator $validator)
    {
        $validator
            ->add('password', 'length', ['rule' => ['minLength', 6], 'message' => __('Password need to be at least 6 characters long')])
            ->requirePresence('password')
            ->notEmpty('password', __('Please provide a valid passphrase'));

        $validator
            ->add('confirm_password', 'no-misspeling', [
                'rule' => ['compareWith', 'password'],
                'message' => __('Passwords are not equal')
            ])
            ->requirePresence('confirm_password')
            ->notEmpty('confirm_password', __('Please repeat your new password'));

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     *
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['group_id'], 'Groups'));

        return $rules;
    }

    /**
     * Sets user group
     *
     * @param integer|null $userId  User id.
     * @param integer|null $groupId Group id.
     *
     * @return boolean
     */
    public function setUserGroup($userId = null, $groupId = null)
    {
        if (!$userId || !$groupId || !is_numeric($userId) || !is_numeric($groupId)) {
            return false;
        }

        // remove email from valiation rules as we won't need that
        $this->getValidator('default')->remove('email');

        $user = $this->get($userId);
        $data['group_id'] = $groupId;
        $user = $this->patchEntity($user, $data);

        if ($this->save($user)) {
            return true;
        }

        return false;
    }

    /**
     * Get user by groups list
     *
     * @param array $group_ids Group ids.
     *
     * @return Query
     */
    public function getUserByGroupsList(array $group_ids = [])
    {
        $conditions = [];
        if (!empty($group_ids)) {
            if (is_array($group_ids)) {
                $conditions['group_id IN'] = $group_ids;
            } else {
                $conditions['group_id'] = $group_ids;
            }
        }
        return $this->find('list', [
            'keyField' => 'id',
            'valueField' => 'full_name_with_group_name',
            'contain' => ['Groups'],
            'conditions' => $conditions
        ]);
    }

    /**
     * Get users list
     *
     * @param array|null   $conditions Optional conditions.
     * @param integer|null $limit      Limit resutlts.
     *
     * @return Query
     */
    public function getUsersList($conditions = [], $limit = 5000): Query
    {
        return $this->find('list', [
            'conditions' => $conditions,
            'order' => ['first_name' => 'ASC', 'last_name' => 'ASC'],
            'limit' => $limit
        ]);
    }

    /**
     * Get users list with service names
     *
     * @param array|null   $conditions Optional conditions.
     * @param integer|null $limit      Limit resutlts.
     *
     * @return array
     */
    public function getUsersListWithServiceNames($conditions = [], $limit = 1000): array
    {
        $usersWithServices = $this->find('all', [
            'contain' => ['Services'],
            'conditions' => $conditions,
            'order' => ['Services.company' => 'ASC', 'first_name' => 'ASC', 'last_name' => 'ASC'],
            'limit' => $limit
        ]);
        $array = [];

        foreach ($usersWithServices as $item) {
            if ($item->has('service')) {
                $array[$item->id] = $item->service->company;
            } else {
                $array[$item->id] = $item->full_name;
            }
        }
        return $array;
    }

    /**
     * Find auth
     *
     * @param Query $query   Orm query.
     * @param array $options Additional options.
     *
     * @return Query
     */
    public function findAuth(Query $query, array $options): Query
    {
        $query->select([
            'id', 'uid', 'group_id', 'image', 'email', 'password', 'first_name', 'last_name', 'phone', 'active', 'created', 'modified', 'email_verified', 'phpbbId'
        ])->contain([
            'Groups'
        ]);

        if (isset($options['group_id'])) {
            $query->andWhere(['Users.group_id' => $options['group_id']]);
        }
        if (isset($options['api'])) {
            $query->andWhere(['Users.api' => $options['api']]);
        }

        return $query;
    }

    /**
     * Safety setting an image field omitting the ORM.
     * This is the patch for an issue with patchEntity function on UsersTable
     *
     * @param User   $user  User entity.
     * @param string $image Image path.
     *
     * @return integer
     */
    public function setImageFor(User $user, string $image): int
    {
        return $this->getConnection()->execute("UPDATE {$this->getTable()} SET image = :image WHERE id = :id", [
            ':image' => $image,
            ':id' => $user->id
        ])->count();
    }

    /**
     * Get mebmer with all relations
     *
     * @param integer $userId User id.
     *
     * @return object
     */
    public function getMemberWithDetails(int $userId): object
    {
        return $this->get($userId, [
            'conditions' => ['Users.group_id' => Group::MEMBERS],
            'contain' => [
                'UserDetails',
                'UserBillingAddresses',
                'Articles', 'Articles.Categories',
                'Properties', 'Properties.PropertyMedia', 'Properties.PropertyTypes',
                'PropertySearches' => function ($q) {
                    return $q->order(['PropertySearches.created' => 'DESC']);
                }, 'PropertySearches.PropertyTypes',
                'Services', 'Services.Categories',
                'ServiceSearches' => function ($q) {
                    return $q->order(['ServiceSearches.created' => 'DESC']);
                }, 'ServiceSearches.Categories'
            ]
        ]);
    }

    /**
     * Find users with newsletter agreements.
     *
     * @return query
     */
    public function findUsersToWeeklyNewsletter()
    {
        return $this->getUsers([
            'Users.group_id' => Group::MEMBERS,
            'OR' => [
                'UserDetails.pref_buying' => true,
                'UserDetails.pref_selling' => true,
                'UserDetails.pref_professional' => true,
                'UserDetails.pref_insights' => true,
            ]
        ]);
    }

    /**
     * Get users
     *
     * @param array|null $conditions Optional conditions.
     *
     * @return object Returns users query with all contains needed.
     */
    public function getUsers($conditions = [])
    {
        return $this->find('all', [
            'contain' => ['Groups', 'UserDetails'],
            'conditions' => $conditions,
            'filter' => 'force'
        ]);
    }

    /**
     * Find members to categories sitemap
     *
     * @return array
     */
    public function findMembersForSitemap()
    {
        return $this
                ->find('all')
                ->select(['id'])
                ->where(['Users.group_id' => Group::MEMBERS])
                ->toArray();
    }
}
