<?php

namespace App\Model\Table;

use App\Model\Entity\Area;
use App\Model\Table\ServiceAreasTable;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Areas Model
 *
 * @property ServiceAreasTable|HasMany $ServiceAreas
 *
 * @method Area get($primaryKey, $options = [])
 * @method Area newEntity($data = null, array $options = [])
 * @method Area[] newEntities(array $data, array $options = [])
 * @method Area|bool save(EntityInterface $entity, $options = [])
 * @method Area|bool saveOrFail(EntityInterface $entity, $options = [])
 * @method Area patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Area[] patchEntities($entities, array $data, array $options = [])
 * @method Area findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class AreasTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('areas');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        
        $this->addBehavior('Filter', ['fields' => ['name']]); // you can customize Filter Behavior by adding fields which you need to be filtered
        $this->addBehavior('Timestamp');
        $this->addBehavior('Log');

        $this->hasMany('ServiceAreas', [
            'foreignKey' => 'area_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 50)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }
}
