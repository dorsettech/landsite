<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\Model\Table;

use App\Model\Entity\PageFile;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PageFiles Model
 */
class PageFilesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('page_files');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Pages', [
            'foreignKey' => 'page_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence(['type', 'position'], 'create');

        $validator
            ->integer('type')
            ->notEmpty('type');

        $validator
            ->allowEmpty('path');

        $validator
            ->allowEmpty('name');

        $validator
            ->allowEmpty('description');

        $validator
            ->allowEmpty('urlname');

        $validator
            ->boolean('target_blank')
            ->allowEmpty('target_blank');

        $validator
            ->integer('position')
            ->notEmpty('position');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     *
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['page_id'], 'Pages'));

        return $rules;
    }

    /**
     * Parse files
     *
     * @param object|null $page Page object.
     *
     * @return object
     */
    public function parseFiles($page)
    {
        $page->slider = [];
        $page->gallery = [];
        $page->documents = [];

        foreach ($page->page_files as $key => $value) {
            switch ($value->type) {
                case PageFile::SLIDER:
                    $page->slider[] = $value->toArray();
                    break;
                case PageFile::GALLERY:
                    $page->gallery[] = $value->toArray();
                    break;
                case PageFile::DOCUMENTS:
                    $page->documents[] = $value->toArray();
                    break;
            }
        }
        unset($page->page_files);

        return $page;
    }
}
