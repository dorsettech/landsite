<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * EmailQueueMembers Model
 *
 * @method \App\Model\Entity\EmailQueueMember get($primaryKey, $options = [])
 * @method \App\Model\Entity\EmailQueueMember newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\EmailQueueMember[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\EmailQueueMember|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EmailQueueMember|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EmailQueueMember patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\EmailQueueMember[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\EmailQueueMember findOrCreate($search, callable $callback = null, $options = [])
 */
class EmailQueueMembersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('email_queue_members');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('first_name', 'create')
            ->notEmpty('first_name');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->allowEmpty('email_type');

        $validator
            ->allowEmpty('options');

        return $validator;
    }
}
