<?php
/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */

namespace App\Model\Entity;

use Cake\ORM\Locator\TableLocator;

/**
 * UploadEntity
 */
class UploadEntity extends ExtendedEntity
{
    /**
     * Construct
     *
     * @param array $properties Properties.
     * @param array $options    Options.
     */
    public function __construct(array $properties = array(), array $options = array())
    {
        parent::__construct($properties, $options);

        $this->setUploadVirtualFields();
    }

    /**
     * Set virtual fields from upload behavior
     *
     * @return void
     */
    protected function setUploadVirtualFields(): void
    {
        $model = (new TableLocator())->get($this->getSource());

        if ($model->behaviors()->has('Upload')) {
            $behavior = $model->behaviors()->get('Upload');
            $behavior->setUid($this);
            $dir      = $behavior->getDirectory();

            $uploadFields = $behavior->getConfig('fields');

            foreach ($uploadFields as $field => $config) {
                $virtualName          = $field . '_path';
                $this->_virtual[]     = $virtualName;
                $this->{$virtualName} = $dir . $this->{$field};
            }
        }
    }
}
