<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PropertyEnquiry Entity
 *
 * @property int $id
 * @property int $property_id
 * @property int $user_id
 * @property string $title
 * @property string $full_name
 * @property string $phone
 * @property string|null $message
 * @property \Cake\I18n\FrozenTime|null $created
 *
 * @property \App\Model\Entity\Property $property
 * @property \App\Model\Entity\User $user
 */
class PropertyEnquiry extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'property_id' => true,
        'user_id' => true,
        'title' => true,
        'full_name' => true,
        'phone' => true,
        'message' => true,
        'created' => true,
        'property' => true,
        'user' => true
    ];
}
