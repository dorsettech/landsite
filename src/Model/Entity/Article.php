<?php

namespace App\Model\Entity;

/**
 * Article Entity
 *
 * @property int $id
 * @property int $author_id
 * @property int|null $posted_by
 * @property int $category_id
 * @property int $channel_id
 * @property string $type
 * @property string $title
 * @property string $slug
 * @property string $headline
 * @property string $body
 * @property string|null $image
 * @property string|null $status
 * @property string|null $state
 * @property string|null $rejection_reason
 * @property \Cake\I18n\FrozenTime|null $publish_date
 * @property bool $visibility
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property string|null $meta_keywords
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\Channel[] $channel
 */
class Article extends UploadEntity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'author_id' => true,
        'posted_by' => true,
        'category_id' => true,
        'channel_id' => true,
        'type' => true,
        'title' => true,
        'slug' => true,
        'headline' => true,
        'body' => true,
        'image' => true,
        'status' => true,
        'state' => true,
        'rejection_reason' => true,
        'publish_date' => true,
        'visibility' => true,
        'meta_title' => true,
        'meta_description' => true,
        'meta_keywords' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'category' => true,
        'channel' => true
    ];
}
