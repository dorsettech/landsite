<?php

namespace App\Model\Entity;

use App\Model\Enum\AdType;
use App\Model\Enum\MediaType;
use App\Model\Enum\OpeningDayType;
use App\Model\Enum\ServiceStatus;
use Cake\I18n\FrozenTime;

/**
 * Service Entity
 *
 * @property int                 $id
 * @property int                 $category_id
 * @property int                 $user_id
 * @property string              $uid
 * @property string              $company
 * @property string|null         $phone
 * @property string              $postcode
 * @property float|null          $lat
 * @property float|null          $lng
 * @property string              $description
 * @property string|null         $website_url
 * @property string|null         $google_url
 * @property string|null         $facebook_url
 * @property string|null         $linkedin_url
 * @property string|null         $twitter_url
 * @property string|null         $hubspot_url
 * @property string|null         $quick_win_1
 * @property string|null         $quick_win_2
 * @property string|null         $quick_win_3
 * @property FrozenTime|null     $opening_time_from
 * @property FrozenTime|null     $opening_time_to
 * @property string|null         $opening_days
 * @property string|null         $opening_hours
 * @property string              $status
 * @property string|null         $state
 * @property string|null         $rejection_reason
 * @property FrozenTime|null     $publish_date
 * @property string              $ad_type
 * @property string              $ad_plan_type
 * @property FrozenTime|null     $ad_expiry_date
 * @property float               $ad_cost
 * @property float               $ad_total_cost
 * @property string              $ad_basket
 * @property int                 $enquiries
 * @property bool                $visibility
 * @property int                 $views
 * @property FrozenTime|null     $created
 * @property FrozenTime|null     $modified
 *
 * @property Category            $category
 * @property User                $user
 * @property ServiceArea[]       $service_areas
 * @property ServiceCredential[] $service_credentials
 * @property ServiceEnquiry[]    $service_enquiries
 * @property ServiceMedia[]      $service_media
 * @property ServiceProduct[]    $service_products
 * @property ServiceView[]       $service_views
 * @property UserSaved[]         $user_saved
 */
class Service extends ExtendedEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => true,
        'status_name' => false,
        'opening_day_names' => false,
        'opening_days_information' => false,
        'opening_hours_list' => false,
        'ad_type_name' => false,
        'enquiries' => false,
        'views' => false,
        'urlname' => false,
        'images' => false,
        'videos' => false,
        'phone_number' => false
    ];

    /**
     * Get opening days
     *
     * Converts days separated by comma to array to handle SET type on database.
     *
     * @param string|null $openingDays Days no. separated by comma.
     *
     * @return mixed Returns array if there's more days or string otherwise.
     */
    protected function _getOpeningDays($openingDays)
    {
        if (!$this->isDirty() && is_string($openingDays) && strpos($openingDays, ',')) {
            return explode(',', $openingDays);
        }

        return $openingDays;
    }

    /**
     * Get status name
     *
     * @return string|null
     */
    protected function _getStatusName(): ?string
    {
        return ServiceStatus::getNamesList()[$this->_properties['status']] ?? '';
    }

    /**
     * Get opening day names
     *
     * @return string
     */
    protected function _getOpeningDayNames(): ?string
    {
        $openingDays[] = $this->_properties['opening_days'];

        if (strpos($this->_properties['opening_days'], ',')) {
            $openingDays = explode(',', $this->_properties['opening_days']);
        }

        $openingDays = array_flip($openingDays);
        $openingDayTypes = OpeningDayType::getNamesList();

        foreach ($openingDays as $dayNo => $value) {
            $openingDays[$dayNo] = $openingDayTypes[$dayNo] ?? '';
        }

        return implode(', ', $openingDays);
    }

    /**
     * Get opening days information
     *
     * @return string
     */
    protected function _getOpeningDaysInformation(): ?string
    {
        /**
         * @todo needs refactoring
         *
         * $openingDays[] = $this->_properties['opening_days'];
         *
         * if (strpos($this->_properties['opening_days'], ',')) {
         * $openingDays = explode(',', $this->_properties['opening_days']);
         * }
         * if (is_array($openingDays)) {
         * $openingDays     = array_flip($openingDays);
         * }
         *
         * $openingDayTypes = OpeningDayType::getShortNamesList();
         *
         * foreach ($openingDays as $dayNo => $value) {
         * $openingDays[$dayNo] = $openingDayTypes[$dayNo] ?? '';
         * }
         *
         * return implode(', ', $openingDays);
         *
         */

        return '';
    }

    /**
     * Opening hours list
     *
     * @return array|null Returns formatted array with details for specific day.
     */
    protected function _getOpeningHoursList(): ?array
    {
        $list = [];

        if (!empty($this->_properties['opening_hours'])) {
            $openingHours = json_decode($this->_properties['opening_hours']);
            $openingDayTypes = OpeningDayType::getNamesList();

            foreach ($openingHours as $hours) {
                foreach ($hours->days as $day) {
                    $list[$day] = [
                        'day' => $day,
                        'day_name' => $openingDayTypes[$day],
                        'from' => (new \Cake\I18n\Time($hours->from)),
                        'to' => (new \Cake\I18n\Time($hours->to)),
                    ];
                }
            }

            ksort($list);
        }

        return $list;
    }

    /**
     * Get ad type name
     *
     * @return string|null
     */
    protected function _getAdTypeName(): ?string
    {
        return AdType::getNamesList()[$this->_properties['ad_type']] ?? '';
    }

    /**
     * Get urlname
     *
     * @return string|null
     */
    protected function _getUrlname(): ?string
    {
        return $this->_properties['id'] . '_' . $this->_properties['slug'];
    }

    /**
     * Return well formatted phone number for tel: links.
     *
     * @return string
     */
    protected function _getPhoneNumber(): string
    {
        return empty($this->_properties['phone']) ? '' : preg_replace('/[^0-9]/', '', $this->_properties['phone']);
    }

    /**
     * Get images from ServiceMedia
     *
     * @return array|null
     */
    protected function _getImages(): ?array
    {
        $images = [];
        if (!empty($this->_properties['service_media'])) {
            foreach ($this->_properties['service_media'] as $media) {
                if ($media->type === MediaType::IMAGE) {
                    $images[] = $media;
                }
            }
        }
        return $images;
    }

    /**
     * Get images from ServiceMedia
     *
     * @return array|null
     */
    protected function _getVideos(): ?array
    {
        $videos = [];
        if (!empty($this->_properties['service_media'])) {
            foreach ($this->_properties['service_media'] as $media) {
                if ($media->type === MediaType::VIDEO) {
                    $videos[] = $media;
                }
            }
        }
        return $videos;
    }
}
