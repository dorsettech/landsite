<?php

namespace App\Model\Entity;

/**
 * UserDetail Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $dashboard_type
 * @property bool $profile_completion
 * @property string|null $company
 * @property string|null $company_description
 * @property string|null $location
 * @property string|null $postcode
 * @property string|null $address
 * @property bool $pref_buying
 * @property bool $pref_selling
 * @property bool $pref_professional
 * @property bool $pref_insights
 * @property string|null $pref_insights_list
 * @property bool $use_billing_details
 * @property bool $marketing_agreement_1
 * @property bool $marketing_agreement_2
 * @property bool $marketing_agreement_3
 * @property string $stripe_customer_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 */
class UserDetail extends ExtendedEntity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'dashboard_type' => true,
        'profile_completion' => true,
        'company' => true,
        'company_description' => true,
        'location' => true,
        'postcode' => true,
        'address' => true,
        'pref_buying' => true,
        'pref_selling' => true,
        'pref_professional' => true,
        'pref_insights' => true,
        'pref_insights_list' => true,
        'use_billing_details' => true,
        'marketing_agreement_1' => true,
        'marketing_agreement_2' => true,
        'marketing_agreement_3' => true,
        'stripe_customer_id' => true,
        'created' => true,
        'modified' => true,
        'user' => true
    ];
}
