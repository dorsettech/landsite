<?php

namespace App\Model\Entity;

use App\Model\Entity\ServiceCredential;
use App\Model\Enum\CredentialType;
use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * Credential Entity
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property FrozenTime|null $created
 *
 * @property ServiceCredential[] $service_credentials
 */
class Credential extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*'         => true,
        'id'        => false,
        'type_name' => false
    ];

    /**
     * Get type name virtual property
     * @return type
     */
    protected function _getTypeName(): ?string
    {
        return CredentialType::getNamesList()[$this->_properties['type']] ?? '';
    }
}
