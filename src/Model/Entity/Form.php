<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-06)
 * @version 1.0
 */

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Form Entity.
 */
class Form extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Get unread messages
     *
     * @return integer|null
     */
    public function getUnreadMessages()
    {
        $formsData = TableRegistry::getTableLocator()->get('FormsData')->find('all', [
            'conditions' => [
                'FormsData.id_forms' => $this->id,
                'FormsData.view' => false
            ]
        ]);

        return $formsData !== null ? $formsData->count() : 0;
    }
}
