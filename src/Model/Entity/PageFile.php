<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-06)
 * @version 1.0
 */

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PageFile Entity.
 */
class PageFile extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    const SLIDER = 1;
    const GALLERY = 2;
    const DOCUMENTS = 3;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
