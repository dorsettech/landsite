<?php
/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-12-13)
 * @version 1.0
 */

namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Cache\Cache;
use Cake\Utility\Inflector;

/**
 * User Entity.
 */
class User extends UploadEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        '*'                         => true,
        'id'                        => false,
        'profile_image'             => false,
        'full_name'                 => false,
        'full_name_with_email'      => false,
        'full_name_with_group_name' => false
    ];

    /**
     * Hash password; Internal function
     *
     * @param string|null $value Password value.
     *
     * @return string
     */
    protected function _setPassword($value = '')
    {
        $hasher = new DefaultPasswordHasher();
        return $hasher->hash($value);
    }

    /**
     * Get full name from virtual property, in format [first_name . ' ' . last_name].
     * If both names are empty, then function returns 'name not set' (note: i18n).
     *
     * @return string
     */
    protected function _getFullName()
    {
        if (empty($this->_properties['first_name']) && empty($this->_properties['last_name'])) {
            return __('Name not set');
        }

        return $this->_properties['first_name'] . ' ' . $this->_properties['last_name'];
    }

    /**
     * Get full name from virtual property, in format [first_name . ' ' . last_name].
     * If both names are empty, then function returns 'name not set' (note: i18n).
     * At the end an email address inside ( brackets ) is added.
     *
     * @return string
     */
    protected function _getFullNameWithEmail()
    {
        return $this->_getFullName() . ' (' . $this->_properties['email'] . ')';
    }

    /**
     * Get full name from virtual property, in format [first_name . ' ' . last_name].
     * If both names are empty, then function returns 'name not set' (note: i18n).
     * At the end an email address inside ( brackets ) is added.
     *
     * @return string
     */
    protected function _getFullNameWithGroupName()
    {
        return $this->_getFullName() . ' (' . (!empty($this->_properties['company']) ? $this->_properties['group']->name . ': ' . $this->_properties['company']->company_name : $this->_properties['group']->name) . ')';
    }

    /**
     * Get user Image
     *
     * @return null|string
     */
    protected function _getProfileImage()
    {
        $image = null;

        if (!empty($this->_properties['image_path']) && file_exists(WWW_ROOT . $this->_properties['image_path'])) {
            $image = $this->_properties['image_path'];
        }

        if ($image === null && isset($this->_properties['group'], $this->_properties['group']->name) && !empty($this->_properties['group']->name)) {
            $image        = WWW_ROOT . 'panel/img/user/user_' . Inflector::underscore($this->_properties['group']->name) . '.png';
            $imageDefault = WWW_ROOT . 'panel/img/user/user_default.png';
            if (!file_exists($image)) {
                if (file_exists($imageDefault)) {
                    $image = $imageDefault;
                }
            }
        }

        return $image;
    }

    public function getImageRaw()
    {
        return $this->_properties['image'];
    }
}
