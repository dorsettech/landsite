<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ServiceEnquiry Entity
 *
 * @property int $id
 * @property int $service_id
 * @property int|null $user_id
 * @property string $full_name
 * @property string $phone
 * @property string $email
 * @property string|null $message
 * @property \Cake\I18n\FrozenTime|null $created
 *
 * @property \App\Model\Entity\Service $service
 * @property \App\Model\Entity\User $user
 */
class ServiceEnquiry extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'service_id' => true,
        'user_id' => true,
        'full_name' => true,
        'phone' => true,
        'email' => true,
        'message' => true,
        'created' => true,
        'service' => true,
        'user' => true
    ];
}
