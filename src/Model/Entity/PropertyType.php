<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * PropertyType Entity
 *
 * @property int $id
 * @property int|null $parent_id
 * @property string $name
 * @property int $position
 * @property bool $visibility
 * @property FrozenTime|null $created
 * @property FrozenTime|null $modified
 *
 * @property PropertyType $parent_property_type
 * @property PropertyType[] $child_property_types
 * @property PropertyAttribute[] $property_attributes
 */
class PropertyType extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*'  => true,
        'id' => false
    ];
}
