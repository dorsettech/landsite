<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;

/**
 * PropertySearch Entity
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $location
 * @property string|null $purpose
 * @property int|null $type_id
 * @property float|null $min_price
 * @property float|null $max_price
 * @property int|null $radius
 * @property float|null $lat
 * @property float|null $lng
 * @property string|null $attributes
 * @property bool $alerts
 * @property FrozenTime|null $created
 *
 * @property User $user
 * @property \App\Model\Entity\Type $type
 */
class PropertySearch extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*'               => true,
        'id'              => false,
        'attribute_names' => false,
        'query_array'     => false
    ];

    /**
     * Get attribute names
     *
     * @return string|null Returns attribute names if attributes are not empty.
     */
    protected function _getAttributeNames(): ?string
    {
        $names = '';

        if (!empty($this->_properties['attributes'])) {
            $propertyAttributes = TableRegistry::getTableLocator()->get('PropertyAttributes');
            $attributes         = $propertyAttributes->find('all', [
                    'fields'     => ['name'],
                    'conditions' => ['id IN (' . $this->_properties['attributes'] . ')']
                ])
                ->enableHydration(false)
                ->toArray();
            $names              = implode(' | ', Hash::extract($attributes, '{n}.name'));
        }

        return $names;
    }

    /**
     * Get query array
     *
     * @return array Returns array with details for URL query.
     */
    protected function _getQueryArray(): array
    {
        $queryArray = [
            'saved'     => 1,
            'purpose'   => $this->_properties['purpose'],
            'type'      => $this->_properties['type_id'],
            'min_price' => $this->_properties['min_price'],
            'max_price' => $this->_properties['max_price'],
            'location'  => $this->_properties['location'],
            'range'     => $this->_properties['radius'],
        ];

        if (!empty($this->_properties['attributes'])) {
            $attributes = explode(',', $this->_properties['attributes']);
            $attrs      = [];
            foreach ($attributes as $attributeId) {
                $attrs[$attributeId] = 1;
            }
            
            $queryArray['attributes'] = $attrs;
        }

        return $queryArray;
    }
}
