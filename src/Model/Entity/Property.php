<?php

namespace App\Model\Entity;

use App\Model\Enum\AdType;
use App\Model\Enum\CommonUseClass;
use App\Model\Enum\MediaType;
use App\Model\Enum\PropertyPriceQualifier;
use App\Model\Enum\PropertyPurpose;
use App\Model\Enum\PropertyStatus;
use Cake\I18n\FrozenTime;

/**
 * Property Entity
 *
 * @property int                 $id
 * @property int                 $type_id
 * @property int                 $user_id
 * @property string              $uid
 * @property string              $reference
 * @property string              $purpose
 * @property string|null         $comm_use_class
 * @property string              $title
 * @property string              $slug
 * @property float               $price_sale
 * @property string              $price_sale_per
 * @property string              $price_sale_qualifier
 * @property float               $price_rent
 * @property string              $price_rent_per
 * @property string              $price_rent_qualifier
 * @property string              $location
 * @property float|null          $lat
 * @property float|null          $lng
 * @property string              $postcode
 * @property string              $town
 * @property string              $headline
 * @property string              $description
 * @property string|null         $website_url
 * @property string              $status
 * @property string|null         $state
 * @property string|null         $rejection_reason
 * @property FrozenTime|null     $publish_date
 * @property string              $ad_type
 * @property FrozenTime|null     $ad_expiry_date
 * @property float               $ad_cost
 * @property float               $ad_total_cost
 * @property string              $ad_basket
 * @property int                 $enquiries
 * @property bool                $under_offer
 * @property bool                $auction
 * @property bool                $visibility
 * @property bool                $deleted
 * @property string              $deletion_reason
 * @property int                 $views
 *
 * @property PropertyType        $property_type
 * @property User                $user
 * @property PropertyAttribute[] $property_attributes
 * @property PropertyEnquiry[]   $property_enquiries
 * @property PropertyMedia[]     $property_media
 * @property PropertyView[]      $property_views
 * @property UserSaved[]         $user_saved
 */
class Property extends ExtendedEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => true,
        'enquiries' => false,
        'views' => false,
        'purpose_name' => false,
        'status_name' => false,
        'ad_type_name' => false,
        'urlname' => false,
        'is_saved' => false,
        'main_image' => false,
        'gallery' => false,
        'documents' => false,
        'common_use_classes_names' => false
    ];

    /**
     * Get purpose name
     *
     * @return string|null
     */
    protected function _getPurposeName(): ?string
    {
        return PropertyPurpose::getNamesList()[$this->_properties['purpose']] ?? '';
    }

    /**
     * @return string|null
     * @throws \ReflectionException Inherited exception.
     */
    protected function _getCommonUseClassesNames(): ?string
    {
        $names = [];
        $namesList = CommonUseClass::getNamesList();
        $classes = $this->get('comm_use_class');
        if (!$classes) {
            return '';
        }
        foreach ($classes as $class) {
            $names[] = $namesList[$class];
        }

        return implode(', ', $names);
    }

    /**
     * Get status name
     *
     * @return string|null
     */
    protected function _getStatusName(): ?string
    {
        return PropertyStatus::getNamesList()[$this->_properties['status']] ?? '';
    }

    /**
     * Get ad type name
     *
     * @return string|null
     */
    protected function _getAdTypeName(): ?string
    {
        return AdType::getNamesList()[$this->_properties['ad_type']] ?? '';
    }

    /**
     * @return string
     * @throws \ReflectionException Inherited exception.
     */
    protected function _getSaleQualifierName()
    {
        return $this->_properties['price_sale_qualifier'] === PropertyPriceQualifier::DEFAULT ? '' : PropertyPriceQualifier::getNamesList()[$this->_properties['price_sale_qualifier']];
    }

    /**
     * @return string
     * @throws \ReflectionException Inhereited exception.
     */
    protected function _getRentQualifierName()
    {
        return $this->_properties['price_rent_qualifier'] === PropertyPriceQualifier::DEFAULT ? '' : PropertyPriceQualifier::getNamesList()[$this->_properties['price_rent_qualifier']];
    }

    /**
     * Get urlname
     *
     * @return string|null
     */
    protected function _getUrlname(): ?string
    {
        return $this->_properties['id'] . '_' . $this->_properties['slug'];
    }

    /**
     * Get is saved
     *
     * @return string|null
     */
    protected function _getIsSaved(): ?bool
    {
        $propertyId = $this->_properties['id'];
        if (!empty($this->_properties['user_saved'])) {
            foreach ($this->_properties['user_saved'] as $saved) {
                if ($saved->property_id === $propertyId) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Get main image
     *
     * @return string|null
     */
    protected function _getMainImage(): ?string
    {
        if (!empty($this->_properties['property_media'])) {
            return $this->_properties['property_media'][0]['source_path'];
        }
        return '';
    }

    /**
     * Get gallery
     *
     * @return array|null
     */
    protected function _getGallery(): ?array
    {
        $array = [];
        if (!empty($this->_properties['property_media'])) {
            foreach ($this->_properties['property_media'] as $media) {
                if ($media->type === MediaType::IMAGE) {
                    $array[] = $media;
                }
            }
        }
        return $array;
    }

    /**
     * Get documents
     *
     * @return array|null
     */
    protected function _getDocuments(): ?array
    {
        $array = [];
        if (!empty($this->_properties['property_media'])) {
            foreach ($this->_properties['property_media'] as $media) {
                if ($media->type === MediaType::DOCUMENT) {
                    $array[] = $media;
                }
            }
        }
        return $array;
    }
}
