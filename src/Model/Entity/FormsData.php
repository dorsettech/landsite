<?php
/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-06)
 * @version 1.0
 */

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FormsData Entity
 */
class FormsData extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
        'from' => false,
        'message' => false,
    ];

    /**
     * Get form
     *
     * @return string
     */
    protected function _getFrom()
    {
        if (isset($this->_properties['data'])) {
            foreach (json_decode($this->_properties['data']) as $key => $data) {
                if (strpos(strtolower($key), 'email') !== false) {
                    return $data;
                }
            }
        }
        return 'N/A';
    }

    /**
     * Get message
     *
     * @return string
     */
    protected function _getMessage()
    {
        if (isset($this->_properties['data'])) {
            foreach (json_decode($this->_properties['data']) as $key => $data) {
                if (strpos(strtolower($key), 'message') !== false || strpos(strtolower($key), 'enquiry') !== false) {
                    return $data;
                }
            }
        }
        return 'N/A';
    }

}
