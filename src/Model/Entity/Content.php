<?php

/**
 * @author Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-06)
 * @version 1.0
 */

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Content Entity.
 */
class Content extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Return string represantation of the entity
     *
     * @return string
     */
    public function __toString()
    {
        return $this->title;
    }
}
