<?php

namespace App\Model\Entity;

/**
 * UserBillingAddress Entity
 *
 * @property int $id
 * @property int $user_id
 * @property bool $is_default
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $email
 * @property string|null $phone
 * @property string|null $company
 * @property string|null $postcode
 * @property string|null $address
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 */
class UserBillingAddress extends ExtendedEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
        'full_name' => false,
    ];

    /**
     * Get full name from virtual property, in format [first_name . ' ' . last_name].
     * If both names are empty, then function returns 'name not set' (note: i18n).
     *
     * @return string
     */
    protected function _getFullName()
    {
        if (empty($this->_properties['first_name']) && empty($this->_properties['last_name'])) {
            return __('Name not set');
        }

        return $this->_properties['first_name'] . ' ' . $this->_properties['last_name'];
    }
}
