<?php /** @noinspection SlowArrayOperationsInLoopInspection */

namespace App\Model\Entity;

use App\Application;
use App\Logger;
use Cake\ORM\Entity;

/**
 * Class ExtendedEntity
 *
 * @package App\Model\Entity
 */
class ExtendedEntity extends Entity
{
    /**
     * @return boolean
     */
    public function hasErrors(): bool
    {
        return is_array($this->_errors) && count($this->_errors);
    }

    /**
     * @param mixed   $field        Field name.
     * @param string  $delimiter    Delimiter.
     * @param boolean $prefixNested With prefix.
     *
     * @return string
     */
    public function getErrorsMessages($field = null, string $delimiter = PHP_EOL, bool $prefixNested = false): string
    {
        if ($field === null) {
            $errors = [];
            $errs = $this->getErrors();
            if (!Application::isProduction()) {
                Logger::info($errs);
            }
            foreach ($errs as $key => $error) {
                foreach ($error as $err) {
                    if (is_array($err)) {
                        $errors = array_merge($errors, $prefixNested ? array_map(static function ($el) use ($key) {
                            return "[$key] $el";
                        }, array_values($err)) : array_values($err));
                    } else {
                        $errors[] = $prefixNested ? "[$key] $err" : $err;
                    }
                }
            }
        } else {
            $errors = array_values($this->getError($field));
        }
        return implode($delimiter, $errors);
    }
}
