<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PaymentToken Entity
 *
 * @property int $id
 * @property string $token
 * @property int $amount_pos
 * @property string $payload
 * @property bool $used
 * @property \Cake\I18n\FrozenTime|null $created
 */
class PaymentToken extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'token' => true,
        'amount_pos' => true,
        'payload' => true,
        'used' => true,
        'created' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'token'
    ];
}
