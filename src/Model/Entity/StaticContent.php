<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-06)
 * @version 1.0
 */

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * StaticContent Entity.
 */
class StaticContent extends Entity
{
    const TYPE_TEXT = 0;
    const TYPE_NUMBER = 1;
    const TYPE_EMAIL = 2;
    const TYPE_TEXTAREA = 3;
    const TYPE_CKEDITOR = 4;
    const TYPE_CODEEITOR = 5;
    const TYPE_CHECKBOX = 6;
    const TYPE_FILE = 7;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    /**
     * Array types
     *
     * @var array
     */
    public $types = [
        0 => 'text',
        1 => 'number',
        2 => 'email',
        3 => 'textarea',
        4 => 'ckeditor',
        5 => 'codeeditor',
        6 => 'checkbox',
        7 => 'file'
    ];
}
