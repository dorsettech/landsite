<?php

namespace App\Model\Entity;

use App\Model\Enum\DiscountType;
use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * Discount Entity
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $name
 * @property string $code
 * @property FrozenDate|null $date_start
 * @property FrozenDate|null $date_end
 * @property string $type
 * @property float|null $value
 * @property int|null $per_customer
 * @property int|null $per_code
 * @property bool $active
 * @property bool $deleted
 * @property FrozenTime|null $created
 * @property FrozenTime|null $modified
 *
 * @property User $user
 */
class Discount extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*'  => true,
        'id' => false,
        'amount' => false
    ];

    /**
     * Get amount
     *
     * @return string
     */
    protected function _getAmount()
    {
        $amount = '';

        if (!empty($this->_properties['type']) && isset($this->_properties['value']) && $this->_properties['value'] > 0) {
            $type = $this->_properties['type'];
            
            if ($type == DiscountType::FIXEDSUM) {
                $amount = '&pound;' . $this->_properties['value'];
            }
            if ($type == DiscountType::PERCENTAGE) {
                $amount = $this->_properties['value'] . '%';
            }
        }

        return $amount;
    }
}
