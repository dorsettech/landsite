<?php

namespace App\Model\Entity;

use App\Model\Enum\SettingType;
use Cake\ORM\Entity;

/**
 * Setting Entity
 *
 * @property int $id
 * @property string $key
 * @property string $group
 * @property string|null $value
 * @property string $type
 * @property string|null $description
 * @property bool $editable
 */
class Setting extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'key' => true,
        'group' => true,
        'value' => true,
        'type' => true,
        'description' => true,
        'editable' => true
    ];

    /**
     * @param SettingType $type
     * @return bool
     */
    public function is($type): bool
    {
        return $this->get('type') === $type;
    }

    /**
     * @return bool|float|int|mixed|null|string
     */
    public function asType()
    {
        switch ($this->type) {
            case SettingType::BOOLEAN:
                return $this->value === 'true';
                break;
            case SettingType::INTEGER:
                return (int)$this->value;
                break;
            case SettingType::FLOAT:
                return (float)$this->value;
                break;
            case SettingType::STRING:
                return (string)$this->value;
                break;
            case SettingType::JSON:
                return json_decode($this->value, true);
                break;
        }
        return null;
    }
}
