<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ServiceClick Entity
 *
 * @property int                        $id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property int                        $service_id
 * @property int                        $element
 *
 * @property \App\Model\Entity\Service  $service
 */
class ServiceClick extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'created' => true,
        'service_id' => true,
        'element' => true,
        'service' => true
    ];
}
