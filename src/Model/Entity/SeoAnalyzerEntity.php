<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-12-14)
 * @version 1.0
 */

namespace App\Model\Entity;

use Cake\Core\Configure;
use Cake\ORM\Entity;

/**
 * SeoAnalyzerEntity
 */
class SeoAnalyzerEntity extends UploadEntity
{
    /**
     * Seo analyzer
     * Used in: [page]
     *
     * @param object|null $entity Entity object.
     *
     * @return array
     */
    protected function _getSeoAnalyzer($entity): array
    {
        Configure::load('website');
        $limits = Configure::readOrFail('SeoAnalyzer');

        $from = 0;
        $max = 10;
        $report = [];
        foreach ($limits['limit'] as $field => $data) {
            $valid = true;
            if ($entity->has($field)) {
                $length = strlen($entity->$field);
                if (!empty($data['min']) && $length < $data['min']) {
                    $report['danger'][] = __('Min. field <strong>{0}</strong> length should be <strong>{1}</strong>, current value is <strong>{2}</strong>.', $field, $data['min'], $length);
                    $valid = false;
                    $from--;
                }
                if (!empty($data['max']) && $length > $data['max']) {
                    $report['danger'][] = __('Max. field <strong>{0}</strong> length should be <strong>{1}</strong>, current value is <strong>{2}</strong>.', $field, $data['max'], $length);
                    $valid = false;
                    $from--;
                }
            } else {
                /* no required filed*/
                $report['danger'][] = __('No field <strong>{0}</strong> on table.', $field);
                $valid = false;
                $from -= 2;
            }
            if ($valid) {
                $report['success'][] = __('Field <strong>{0}</strong> is OK.', $field);
                $from += 2;
            }
        }
        if ($from < 0) {
            $from = 0;
        }
        if ($from > $max) {
            $from = $max;
        }

        switch ($from) {
            case 8:
            case 9:
            case 10:
                $statusLevel = 'success';
                break;
            case 7:
            case 6:
            case 5:
                $statusLevel = 'warning';
                break;
            default:
                $statusLevel = 'danger';
                break;
        }

        return [
            'internal_ranking' => [
                'from' => $from,
                'to' => $max,
                'statusLevel' => $statusLevel,
                'report' => $report
            ]
        ];
    }
}
