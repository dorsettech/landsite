<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Payment Entity
 *
 * @property int                        $id
 * @property int                        $user_id
 * @property string                     $title
 * @property float                      $amount
 * @property int                        $amount_pos
 * @property float                      $vat
 * @property float                      $vat_rate
 * @property string                     $currency
 * @property string|null                $type
 * @property string|null                $token_id
 * @property string|null                $charge_id
 * @property int|null                   $object_id
 * @property string|null                $payload
 * @property string|null                $meta
 * @property bool                       $refunded
 * @property float|null                 $discount
 * @property int|null                $discount_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User     $user
 */
class Payment extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'title' => true,
        'amount' => true,
        'amount_pos' => true,
        'vat' => true,
        'vat_rate' => true,
        'currency' => true,
        'type' => true,
        'token_id' => true,
        'charge_id' => true,
        'object_id' => true,
        'payload' => true,
        'meta' => true,
        'invoice' => true,
        'refunded' => true,
        'discount' => true,
        'discount_id' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'token' => true,
        'charge' => true,
        'invoice_path' => false,
        'net' => false
    ];

    /**
     * Get invoice path
     *
     * @return string|null
     */
    protected function _getInvoicePath(): ?string
    {
        if (!empty($this->_properties['invoice'])) {
            return WWW_ROOT . '/files/invoices/' . $this->_properties['user_id'] . '/' . $this->_properties['invoice'];
        }

        return '';
    }

    /**
     * Get net value of the payment.
     *
     * @return float
     */
    protected function _getNet(): float
    {
        return $this->_properties['amount'] - $this->_properties['vat'];
    }
}
