<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-06)
 * @version 1.0
 */

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Group Entity.
 */
class Group extends Entity
{
    /**
     * Members group ID. Static and unchangeable value.
     */
    public const MEMBERS = 6;

    /**
     * CMS groups
     */
    public const MODERATOR = 3;
    public const ADMINISTRATOR = 4;
    public const ROOT = 5;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
