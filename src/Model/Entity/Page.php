<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-12-10)
 * @version 1.0
 */

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Page Entity.
 */
class Page extends SeoAnalyzerEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
        'seo_analyzer' => false
    ];

    /**
     * Seo analyzer
     *
     * @param object $entity Page entity object.
     *
     * @return array
     */
    protected function _getSeoAnalyzer($entity): array
    {
        /* you can change logic with this data if necessary */
        $results = parent::_getSeoAnalyzer($this);

        return $results;
    }
}
