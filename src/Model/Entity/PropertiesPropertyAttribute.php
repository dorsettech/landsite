<?php

namespace App\Model\Entity;

/**
 * PropertiesPropertyAttribute Entity
 *
 * @property int $id
 * @property int $property_id
 * @property int $property_attribute_id
 * @property bool $attribute_value
 *
 * @property \App\Model\Entity\Property $property
 * @property \App\Model\Entity\PropertyAttribute $property_attribute
 */
class PropertiesPropertyAttribute extends ExtendedEntity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'property_id' => true,
        'property_attribute_id' => true,
        'attribute_value' => true,
        'property' => true,
        'property_attribute' => true
    ];
}
