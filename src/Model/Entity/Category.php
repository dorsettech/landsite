<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Category Entity
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string $slug
 * @property string|null $image
 * @property int $position
 * @property bool $is_recommended
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Article[] $articles
 * @property \App\Model\Entity\ServiceSearch[] $service_searches
 * @property \App\Model\Entity\Service[] $services
 */
class Category extends UploadEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'description' => true,
        'slug' => true,
        'image' => true,
        'position' => true,
        'is_recommended' => true,
        'created' => true,
        'modified' => true,
        'articles' => true,
        'service_searches' => true,
        'services' => true
    ];
}
