<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ServiceSearch Entity
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $location
 * @property string|null $keywords
 * @property int|null $category_id
 * @property int|null $radius
 * @property float|null $lat
 * @property float|null $lng
 * @property bool $alerts
 * @property \Cake\I18n\FrozenTime|null $created
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Category $category
 */
class ServiceSearch extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*'           => true,
        'id'          => false,
        'query_array' => false
    ];

    /**
     * Get query array
     *
     * @return array Returns array with details for URL query.
     */
    protected function _getQueryArray(): array
    {
        $queryArray = [
            'saved'            => 1,
            'service_category' => $this->_properties['category_id'],
            'keyword'          => $this->_properties['keywords'],
            'location'         => $this->_properties['location'],
            'range'            => $this->_properties['radius'],
        ];

        return $queryArray;
    }
}
