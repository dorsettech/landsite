<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;

/**
 * PropertyMedia Entity
 *
 * @property int $id
 * @property int $property_id
 * @property string $type
 * @property string $source
 * @property string|null $extra
 * @property string|null $url
 * @property int $position
 * @property FrozenTime|null $created
 * @property FrozenTime|null $modified
 *
 * @property Property $property
 */
class PropertyMedia extends UploadEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*'  => true,
        'id' => true
    ];
}
