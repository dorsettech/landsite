<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;

/**
 * ServiceMedia Entity
 *
 * @property int $id
 * @property int $service_id
 * @property string $type
 * @property string $source
 * @property string|null $extra
 * @property int $position
 * @property FrozenTime|null $created
 * @property FrozenTime|null $modified
 *
 * @property Service $service
 */
class ServiceMedia extends UploadEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*'  => true,
        'id' => false
    ];
}
