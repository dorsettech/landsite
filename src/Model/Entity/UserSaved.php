<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserSaved Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $property_id
 * @property int|null $service_id
 * @property \Cake\I18n\FrozenTime|null $created
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Property $property
 * @property \App\Model\Entity\Service $service
 */
class UserSaved extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'property_id' => true,
        'service_id' => true,
        'created' => true,
        'user' => true,
        'property' => true,
        'service' => true
    ];
}
