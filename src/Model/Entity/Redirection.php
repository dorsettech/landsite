<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-06)
 * @version 1.0
 */

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Redirection Entity
 */
class Redirection extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Get redirection from
     *
     * @param string $redirectionFrom Redirection from.
     * @return string Add '/' sign if it's missing at beginning of redirection string.
     */
    protected function _getRedirectionFrom(string $redirectionFrom = null)
    {
        if (!empty($redirectionFrom)) {
            $slashPos = strpos($redirectionFrom, '/');

            if ($slashPos === false || $slashPos > 0) {
                $redirectionFrom = '/' . $redirectionFrom;
            }
        }

        return $redirectionFrom;
    }
}
