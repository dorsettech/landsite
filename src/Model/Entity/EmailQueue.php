<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * EmailQueue Entity
 *
 * @property int $id
 * @property string $email
 * @property string $subject
 * @property string $content
 * @property bool $is_sent
 * @property string|null $options
 * @property \Cake\I18n\FrozenTime|null $send_date
 */
class EmailQueue extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'email' => true,
        'subject' => true,
        'content' => true,
        'is_sent' => true,
        'options' => true,
        'send_date' => true,
        'type' => false,
    ];

    /**
     * Get email type
     *
     * @return string|null
     */
    protected function _getType(): ?string
    {
        if (strpos($this->_properties['subject'], 'Businesses') !== false) {
            return 'Latest Businesses';
        } elseif (strpos($this->_properties['subject'], 'Properties') !== false) {
            return 'Latest Properties';
        } elseif (strpos($this->_properties['subject'], 'News') !== false) {
            return 'Latest News';
        }
    }
}
