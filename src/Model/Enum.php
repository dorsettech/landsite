<?php

namespace App\Model;

use ReflectionClass;

/**
 * Enum - emulate simple and efficient enumeriations
 *
 * @package App\Model
 * @author Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
abstract class Enum
{

    private static $constCacheArray;

    final private function __construct()
    {
    }

    /**
     * @param $name
     * @param bool $strict
     * @return bool
     * @throws \ReflectionException
     */
    public static function hasName($name, $strict = false): bool
    {
        $constants = self::getConstList();

        if ($strict) {
            return array_key_exists($name, $constants);
        }

        $keys = array_map('strtolower', array_keys($constants));
        return in_array(strtolower($name), $keys, true);
    }

    /**
     * @return mixed
     * @throws \ReflectionException
     */
    public static function getConstList()
    {
        if (self::$constCacheArray === NULL) {
            self::$constCacheArray = [];
        }
        $calledClass = static::class;
        if (!array_key_exists($calledClass, self::$constCacheArray)) {
            $reflect = new ReflectionClass($calledClass);
            self::$constCacheArray[$calledClass] = $reflect->getConstants();
        }
        return self::$constCacheArray[$calledClass];
    }

    /**
     * @param $value
     * @return bool
     * @throws \ReflectionException
     */
    public static function hasValue($value): bool
    {
        $values = array_values(self::getConstList());
        return in_array($value, $values, $strict = true);
    }

    /**
     * @param $value
     * @return mixed
     * @throws \ReflectionException
     */
    public static function toString($value)
    {
        $constList = self::getConstList();
        $flipped = array_flip($constList);

        return $flipped[$value];
    }
}
