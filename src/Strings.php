<?php

namespace App;

/**
 * String Utilities
 *
 * @package App
 * @author Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
class Strings {

    public static $aUtf8LowerAccents = array(
        '� ' => 'a', 'ô' => 'o', 'ď' => 'd', 'ḟ' => 'f', 'ë' => 'e', 'š' => 's', 'ơ' => 'o',
        'ß' => 'ss', 'ă' => 'a', 'ř' => 'r', 'ț' => 't', 'ň' => 'n', 'ā' => 'a', 'ķ' => 'k',
        'ŝ' => 's', 'ỳ' => 'y', 'ņ' => 'n', 'ĺ' => 'l', 'ħ' => 'h', 'ṗ' => 'p', 'ó' => 'o',
        'ú' => 'u', 'ě' => 'e', 'é' => 'e', 'ç' => 'c', 'ẁ' => 'w', 'ċ' => 'c', 'õ' => 'o',
        'ṡ' => 's', 'ø' => 'o', 'ģ' => 'g', 'ŧ' => 't', 'ș' => 's', 'ė' => 'e', 'ĉ' => 'c',
        'ś' => 's', 'î' => 'i', 'ű' => 'u', 'ć' => 'c', 'ę' => 'e', 'ŵ' => 'w', 'ṫ' => 't',
        'ū' => 'u', 'č' => 'c', 'ö' => 'oe', 'è' => 'e', 'ŷ' => 'y', 'ą' => 'a', 'ł' => 'l',
        'ų' => 'u', 'ů' => 'u', 'ş' => 's', 'ğ' => 'g', 'ļ' => 'l', 'ƒ' => 'f', 'ž' => 'z',
        'ẃ' => 'w', 'ḃ' => 'b', 'å' => 'a', 'ì' => 'i', 'ï' => 'i', 'ḋ' => 'd', 'ť' => 't',
        'ŗ' => 'r', 'ä' => 'ae', 'í' => 'i', 'ŕ' => 'r', 'ê' => 'e', 'ü' => 'ue', 'ò' => 'o',
        'ē' => 'e', 'ñ' => 'n', 'ń' => 'n', 'ĥ' => 'h', 'ĝ' => 'g', 'đ' => 'd', 'ĵ' => 'j',
        'ÿ' => 'y', 'ũ' => 'u', 'ŭ' => 'u', 'ư' => 'u', 'ţ' => 't', 'ý' => 'y', 'ő' => 'o',
        'â' => 'a', 'ľ' => 'l', 'ẅ' => 'w', 'ż' => 'z', 'ī' => 'i', 'ã' => 'a', 'ġ' => 'g',
        'ṁ' => 'm', 'ō' => 'o', 'ĩ' => 'i', 'ù' => 'u', 'į' => 'i', 'ź' => 'z', 'á' => 'a',
        'û' => 'u', 'þ' => 'th', 'ð' => 'dh', 'æ' => 'ae', 'µ' => 'u', 'ĕ' => 'e',
    );

    public static $aUtf8UpperAccents = array(
        'À' => 'A', 'Ô' => 'O', 'Ď' => 'D', 'Ḟ' => 'F', 'Ë' => 'E', '� ' => 'S', '� ' => 'O',
        'Ă' => 'A', 'Ř' => 'R', 'Ț' => 'T', 'Ň' => 'N', 'Ā' => 'A', 'Ķ' => 'K',
        'Ŝ' => 'S', 'Ỳ' => 'Y', 'Ņ' => 'N', 'Ĺ' => 'L', 'Ħ' => 'H', 'Ṗ' => 'P', 'Ó' => 'O',
        'Ú' => 'U', 'Ě' => 'E', 'É' => 'E', 'Ç' => 'C', 'Ẁ' => 'W', 'Ċ' => 'C', 'Õ' => 'O',
        '� ' => 'S', 'Ø' => 'O', 'Ģ' => 'G', 'Ŧ' => 'T', 'Ș' => 'S', 'Ė' => 'E', 'Ĉ' => 'C',
        'Ś' => 'S', 'Î' => 'I', 'Ű' => 'U', 'Ć' => 'C', 'Ę' => 'E', 'Ŵ' => 'W', 'Ṫ' => 'T',
        'Ū' => 'U', 'Č' => 'C', 'Ö' => 'Oe', 'È' => 'E', 'Ŷ' => 'Y', 'Ą' => 'A', 'Ł' => 'L',
        'Ų' => 'U', 'Ů' => 'U', 'Ş' => 'S', 'Ğ' => 'G', 'Ļ' => 'L', 'Ƒ' => 'F', 'Ž' => 'Z',
        'Ẃ' => 'W', 'Ḃ' => 'B', 'Å' => 'A', 'Ì' => 'I', 'Ï' => 'I', 'Ḋ' => 'D', 'Ť' => 'T',
        'Ŗ' => 'R', 'Ä' => 'Ae', 'Í' => 'I', 'Ŕ' => 'R', 'Ê' => 'E', 'Ü' => 'Ue', 'Ò' => 'O',
        'Ē' => 'E', 'Ñ' => 'N', 'Ń' => 'N', 'Ĥ' => 'H', 'Ĝ' => 'G', 'Đ' => 'D', 'Ĵ' => 'J',
        'Ÿ' => 'Y', 'Ũ' => 'U', 'Ŭ' => 'U', 'Ư' => 'U', 'Ţ' => 'T', 'Ý' => 'Y', 'Ő' => 'O',
        'Â' => 'A', 'Ľ' => 'L', 'Ẅ' => 'W', 'Ż' => 'Z', 'Ī' => 'I', 'Ã' => 'A', '� ' => 'G',
        'Ṁ' => 'M', 'Ō' => 'O', 'Ĩ' => 'I', 'Ù' => 'U', 'Į' => 'I', 'Ź' => 'Z', 'Á' => 'A',
        'Û' => 'U', 'Þ' => 'Th', 'Ð' => 'Dh', 'Æ' => 'Ae', 'Ĕ' => 'E',
    );

    public static $aUtf8Romanization = array(
        //russian cyrillic
        'а' => 'a', 'А' => 'A', 'б' => 'b', 'Б' => 'B', 'в' => 'v', 'В' => 'V', 'г' => 'g', 'Г' => 'G',
        'д' => 'd', 'Д' => 'D', 'е' => 'e', 'Е' => 'E', 'ё' => 'jo', 'Ё' => 'Jo', 'ж' => 'zh', 'Ж' => 'Zh',
        'з' => 'z', 'З' => 'Z', 'и' => 'i', 'И' => 'I', 'й' => 'j', 'Й' => 'J', 'к' => 'k', 'К' => 'K',
        'л' => 'l', 'Л' => 'L', 'м' => 'm', 'М' => 'M', 'н' => 'n', 'Н' => 'N', 'о' => 'o', 'О' => 'O',
        'п' => 'p', 'П' => 'P', 'р' => 'r', '� ' => 'R', 'с' => 's', 'С' => 'S', 'т' => 't', 'Т' => 'T',
        'у' => 'u', 'У' => 'U', 'ф' => 'f', 'Ф' => 'F', 'х' => 'x', 'Х' => 'X', 'ц' => 'c', 'Ц' => 'C',
        'ч' => 'ch', 'Ч' => 'Ch', 'ш' => 'sh', 'Ш' => 'Sh', 'щ' => 'sch', 'Щ' => 'Sch', 'ъ' => '',
        'Ъ' => '', 'ы' => 'y', 'Ы' => 'Y', 'ь' => '', 'Ь' => '', 'э' => 'eh', 'Э' => 'Eh', 'ю' => 'ju',
        'Ю' => 'Ju', 'я' => 'ja', 'Я' => 'Ja',
        // Ukrainian cyrillic
        'Ґ' => 'Gh', 'ґ' => 'gh', 'Є' => 'Je', 'є' => 'je', 'І' => 'I', 'і' => 'i', 'Ї' => 'Ji', 'ї' => 'ji',
        // Georgian
        'ა' => 'a', 'ბ' => 'b', 'გ' => 'g', 'დ' => 'd', 'ე' => 'e', 'ვ' => 'v', 'ზ' => 'z', 'თ' => 'th',
        'ი' => 'i', 'კ' => 'p', 'ლ' => 'l', 'მ' => 'm', 'ნ' => 'n', 'ო' => 'o', 'პ' => 'p', 'ჟ' => 'zh',
        '� ' => 'r', 'ს' => 's', 'ტ' => 't', 'უ' => 'u', 'ფ' => 'ph', 'ქ' => 'kh', 'ღ' => 'gh', 'ყ' => 'q',
        'შ' => 'sh', 'ჩ' => 'ch', 'ც' => 'c', 'ძ' => 'dh', 'წ' => 'w', 'ჭ' => 'j', 'ხ' => 'x', 'ჯ' => 'jh',
        'ჰ' => 'xh',
        //Sanskrit
        'अ' => 'a', 'आ' => 'ah', 'इ' => 'i', 'ई' => 'ih', 'उ' => 'u', 'ऊ' => 'uh', 'ऋ' => 'ry',
        '� ' => 'ryh', 'ऌ' => 'ly', 'ॡ' => 'lyh', 'ए' => 'e', 'ऐ' => 'ay', 'ओ' => 'o', 'औ' => 'aw',
        'अं' => 'amh', 'अः' => 'aq', 'क' => 'k', 'ख' => 'kh', 'ग' => 'g', 'घ' => 'gh', 'ङ' => 'nh',
        'च' => 'c', 'छ' => 'ch', 'ज' => 'j', 'झ' => 'jh', 'ञ' => 'ny', 'ट' => 'tq', '� ' => 'tqh',
        'ड' => 'dq', 'ढ' => 'dqh', 'ण' => 'nq', 'त' => 't', 'थ' => 'th', 'द' => 'd', 'ध' => 'dh',
        'न' => 'n', 'प' => 'p', 'फ' => 'ph', 'ब' => 'b', 'भ' => 'bh', 'म' => 'm', 'य' => 'z', 'र' => 'r',
        'ल' => 'l', 'व' => 'v', 'श' => 'sh', 'ष' => 'sqh', 'स' => 's', 'ह' => 'x',
        //Hebrew
        'א' => 'a', 'ב' => 'b', 'ג' => 'g', 'ד' => 'd', 'ה' => 'h', 'ו' => 'v', 'ז' => 'z', 'ח' => 'kh', 'ט' => 'th',
        'י' => 'y', 'ך' => 'h', 'כ' => 'k', 'ל' => 'l', 'ם' => 'm', 'מ' => 'm', 'ן' => 'n', '� ' => 'n',
        'ס' => 's', 'ע' => 'ah', 'ף' => 'f', 'פ' => 'p', 'ץ' => 'c', 'צ' => 'c', 'ק' => 'q', 'ר' => 'r',
        'ש' => 'sh', 'ת' => 't',
        //Arabic
        'ا' => 'a', 'ب' => 'b', 'ت' => 't', 'ث' => 'th', 'ج' => 'g', 'ح' => 'xh', 'خ' => 'x', 'د' => 'd',
        'ذ' => 'dh', 'ر' => 'r', 'ز' => 'z', 'س' => 's', 'ش' => 'sh', 'ص' => 's\'', 'ض' => 'd\'',
        'ط' => 't\'', 'ظ' => 'z\'', 'ع' => 'y', 'غ' => 'gh', 'ف' => 'f', 'ق' => 'q', 'ك' => 'k',
        'ل' => 'l', 'م' => 'm', 'ن' => 'n', 'ه' => 'x\'', 'و' => 'u', 'ي' => 'i',

        // Japanese hiragana
        'あ' => 'a', 'え' => 'e', 'い' => 'i', 'お' => 'o', 'う' => 'u', 'ば' => 'ba', 'べ' => 'be',
        'び' => 'bi', 'ぼ' => 'bo', 'ぶ' => 'bu', 'し' => 'ci', '� ' => 'da', 'で' => 'de', 'ぢ' => 'di',
        'ど' => 'do', 'づ' => 'du', 'ふぁ' => 'fa', 'ふぇ' => 'fe', 'ふぃ' => 'fi', 'ふぉ' => 'fo',
        'ふ' => 'fu', 'が' => 'ga', 'げ' => 'ge', 'ぎ' => 'gi', 'ご' => 'go', 'ぐ' => 'gu', 'は' => 'ha',
        'へ' => 'he', 'ひ' => 'hi', 'ほ' => 'ho', 'ふ' => 'hu', 'じゃ' => 'ja', 'じぇ' => 'je',
        'じ' => 'ji', 'じょ' => 'jo', 'じゅ' => 'ju', 'か' => 'ka', 'け' => 'ke', 'き' => 'ki',
        'こ' => 'ko', 'く' => 'ku', 'ら' => 'la', 'れ' => 'le', 'り' => 'li', 'ろ' => 'lo', 'る' => 'lu',
        'ま' => 'ma', 'め' => 'me', 'み' => 'mi', 'も' => 'mo', 'む' => 'mu', 'な' => 'na', 'ね' => 'ne',
        'に' => 'ni', 'の' => 'no', 'ぬ' => 'nu', 'ぱ' => 'pa', 'ぺ' => 'pe', 'ぴ' => 'pi', 'ぽ' => 'po',
        'ぷ' => 'pu', 'ら' => 'ra', 'れ' => 're', 'り' => 'ri', 'ろ' => 'ro', 'る' => 'ru', 'さ' => 'sa',
        'せ' => 'se', 'し' => 'si', 'そ' => 'so', 'す' => 'su', 'た' => 'ta', 'て' => 'te', 'ち' => 'ti',
        'と' => 'to', 'つ' => 'tu', 'ヴぁ' => 'va', 'ヴぇ' => 've', 'ヴぃ' => 'vi', 'ヴぉ' => 'vo',
        'ヴ' => 'vu', 'わ' => 'wa', 'うぇ' => 'we', 'うぃ' => 'wi', 'を' => 'wo', 'や' => 'ya', 'いぇ' => 'ye',
        'い' => 'yi', 'よ' => 'yo', 'ゆ' => 'yu', 'ざ' => 'za', 'ぜ' => 'ze', 'じ' => 'zi', 'ぞ' => 'zo',
        'ず' => 'zu', 'びゃ' => 'bya', 'びぇ' => 'bye', 'びぃ' => 'byi', 'びょ' => 'byo', 'びゅ' => 'byu',
        'ちゃ' => 'cha', 'ちぇ' => 'che', 'ち' => 'chi', 'ちょ' => 'cho', 'ちゅ' => 'chu', 'ちゃ' => 'cya',
        'ちぇ' => 'cye', 'ちぃ' => 'cyi', 'ちょ' => 'cyo', 'ちゅ' => 'cyu', 'でゃ' => 'dha', 'でぇ' => 'dhe',
        'でぃ' => 'dhi', 'でょ' => 'dho', 'でゅ' => 'dhu', 'どぁ' => 'dwa', 'どぇ' => 'dwe', 'どぃ' => 'dwi',
        'どぉ' => 'dwo', 'どぅ' => 'dwu', 'ぢゃ' => 'dya', 'ぢぇ' => 'dye', 'ぢぃ' => 'dyi', 'ぢょ' => 'dyo',
        'ぢゅ' => 'dyu', 'ぢ' => 'dzi', 'ふぁ' => 'fwa', 'ふぇ' => 'fwe', 'ふぃ' => 'fwi', 'ふぉ' => 'fwo',
        'ふぅ' => 'fwu', 'ふゃ' => 'fya', 'ふぇ' => 'fye', 'ふぃ' => 'fyi', 'ふょ' => 'fyo', 'ふゅ' => 'fyu',
        'ぎゃ' => 'gya', 'ぎぇ' => 'gye', 'ぎぃ' => 'gyi', 'ぎょ' => 'gyo', 'ぎゅ' => 'gyu', 'ひゃ' => 'hya',
        'ひぇ' => 'hye', 'ひぃ' => 'hyi', 'ひょ' => 'hyo', 'ひゅ' => 'hyu', 'じゃ' => 'jya', 'じぇ' => 'jye',
        'じぃ' => 'jyi', 'じょ' => 'jyo', 'じゅ' => 'jyu', 'きゃ' => 'kya', 'きぇ' => 'kye', 'きぃ' => 'kyi',
        'きょ' => 'kyo', 'きゅ' => 'kyu', 'りゃ' => 'lya', 'りぇ' => 'lye', 'りぃ' => 'lyi', 'りょ' => 'lyo',
        'りゅ' => 'lyu', 'みゃ' => 'mya', 'みぇ' => 'mye', 'みぃ' => 'myi', 'みょ' => 'myo', 'みゅ' => 'myu',
        'ん' => 'n', 'にゃ' => 'nya', 'にぇ' => 'nye', 'にぃ' => 'nyi', 'にょ' => 'nyo', 'にゅ' => 'nyu',
        'ぴゃ' => 'pya', 'ぴぇ' => 'pye', 'ぴぃ' => 'pyi', 'ぴょ' => 'pyo', 'ぴゅ' => 'pyu', 'りゃ' => 'rya',
        'りぇ' => 'rye', 'りぃ' => 'ryi', 'りょ' => 'ryo', 'りゅ' => 'ryu', 'しゃ' => 'sha', 'しぇ' => 'she',
        'し' => 'shi', 'しょ' => 'sho', 'しゅ' => 'shu', 'すぁ' => 'swa', 'すぇ' => 'swe', 'すぃ' => 'swi',
        'すぉ' => 'swo', 'すぅ' => 'swu', 'しゃ' => 'sya', 'しぇ' => 'sye', 'しぃ' => 'syi', 'しょ' => 'syo',
        'しゅ' => 'syu', 'てゃ' => 'tha', 'てぇ' => 'the', 'てぃ' => 'thi', 'てょ' => 'tho', 'てゅ' => 'thu',
        'つゃ' => 'tsa', 'つぇ' => 'tse', 'つぃ' => 'tsi', 'つょ' => 'tso', 'つ' => 'tsu', 'とぁ' => 'twa',
        'とぇ' => 'twe', 'とぃ' => 'twi', 'とぉ' => 'two', 'とぅ' => 'twu', 'ちゃ' => 'tya', 'ちぇ' => 'tye',
        'ちぃ' => 'tyi', 'ちょ' => 'tyo', 'ちゅ' => 'tyu', 'ヴゃ' => 'vya', 'ヴぇ' => 'vye', 'ヴぃ' => 'vyi',
        'ヴょ' => 'vyo', 'ヴゅ' => 'vyu', 'うぁ' => 'wha', 'うぇ' => 'whe', 'うぃ' => 'whi', 'うぉ' => 'who',
        'うぅ' => 'whu', 'ゑ' => 'wye', 'ゐ' => 'wyi', 'じゃ' => 'zha', 'じぇ' => 'zhe', 'じぃ' => 'zhi',
        'じょ' => 'zho', 'じゅ' => 'zhu', 'じゃ' => 'zya', 'じぇ' => 'zye', 'じぃ' => 'zyi', 'じょ' => 'zyo',
        'じゅ' => 'zyu',
        // Japanese katakana
        'ア' => 'a', 'エ' => 'e', 'イ' => 'i', 'オ' => 'o', 'ウ' => 'u', 'バ' => 'ba', 'ベ' => 'be', 'ビ' => 'bi',
        'ボ' => 'bo', 'ブ' => 'bu', 'シ' => 'ci', 'ダ' => 'da', 'デ' => 'de', 'ヂ' => 'di', 'ド' => 'do',
        'ヅ' => 'du', 'ファ' => 'fa', 'フェ' => 'fe', 'フィ' => 'fi', 'フォ' => 'fo', 'フ' => 'fu', 'ガ' => 'ga',
        'ゲ' => 'ge', 'ギ' => 'gi', 'ゴ' => 'go', 'グ' => 'gu', 'ハ' => 'ha', 'ヘ' => 'he', 'ヒ' => 'hi', 'ホ' => 'ho',
        'フ' => 'hu', 'ジャ' => 'ja', 'ジェ' => 'je', 'ジ' => 'ji', 'ジョ' => 'jo', 'ジュ' => 'ju', 'カ' => 'ka',
        'ケ' => 'ke', 'キ' => 'ki', 'コ' => 'ko', 'ク' => 'ku', 'ラ' => 'la', 'レ' => 'le', 'リ' => 'li', 'ロ' => 'lo',
        'ル' => 'lu', 'マ' => 'ma', 'メ' => 'me', 'ミ' => 'mi', 'モ' => 'mo', '� ' => 'mu', 'ナ' => 'na', 'ネ' => 'ne',
        'ニ' => 'ni', 'ノ' => 'no', 'ヌ' => 'nu', 'パ' => 'pa', 'ペ' => 'pe', 'ピ' => 'pi', 'ポ' => 'po', 'プ' => 'pu',
        'ラ' => 'ra', 'レ' => 're', 'リ' => 'ri', 'ロ' => 'ro', 'ル' => 'ru', 'サ' => 'sa', 'セ' => 'se', 'シ' => 'si',
        'ソ' => 'so', 'ス' => 'su', 'タ' => 'ta', 'テ' => 'te', 'チ' => 'ti', 'ト' => 'to', 'ツ' => 'tu', 'ヴァ' => 'va',
        'ヴェ' => 've', 'ヴィ' => 'vi', 'ヴォ' => 'vo', 'ヴ' => 'vu', 'ワ' => 'wa', 'ウェ' => 'we', 'ウィ' => 'wi',
        'ヲ' => 'wo', 'ヤ' => 'ya', 'イェ' => 'ye', 'イ' => 'yi', 'ヨ' => 'yo', 'ユ' => 'yu', 'ザ' => 'za', 'ゼ' => 'ze',
        'ジ' => 'zi', 'ゾ' => 'zo', 'ズ' => 'zu', 'ビャ' => 'bya', 'ビェ' => 'bye', 'ビィ' => 'byi', 'ビョ' => 'byo',
        'ビュ' => 'byu', 'チャ' => 'cha', 'チェ' => 'che', 'チ' => 'chi', 'チョ' => 'cho', 'チュ' => 'chu',
        'チャ' => 'cya', 'チェ' => 'cye', 'チィ' => 'cyi', 'チョ' => 'cyo', 'チュ' => 'cyu', 'デャ' => 'dha',
        'デェ' => 'dhe', 'ディ' => 'dhi', 'デョ' => 'dho', 'デュ' => 'dhu', 'ドァ' => 'dwa', 'ドェ' => 'dwe',
        'ドィ' => 'dwi', 'ドォ' => 'dwo', 'ドゥ' => 'dwu', 'ヂャ' => 'dya', 'ヂェ' => 'dye', 'ヂィ' => 'dyi',
        'ヂョ' => 'dyo', 'ヂュ' => 'dyu', 'ヂ' => 'dzi', 'ファ' => 'fwa', 'フェ' => 'fwe', 'フィ' => 'fwi',
        'フォ' => 'fwo', 'フゥ' => 'fwu', 'フャ' => 'fya', 'フェ' => 'fye', 'フィ' => 'fyi', 'フョ' => 'fyo',
        'フュ' => 'fyu', 'ギャ' => 'gya', 'ギェ' => 'gye', 'ギィ' => 'gyi', 'ギョ' => 'gyo', 'ギュ' => 'gyu',
        'ヒャ' => 'hya', 'ヒェ' => 'hye', 'ヒィ' => 'hyi', 'ヒョ' => 'hyo', 'ヒュ' => 'hyu', 'ジャ' => 'jya',
        'ジェ' => 'jye', 'ジィ' => 'jyi', 'ジョ' => 'jyo', 'ジュ' => 'jyu', 'キャ' => 'kya', 'キェ' => 'kye',
        'キィ' => 'kyi', 'キョ' => 'kyo', 'キュ' => 'kyu', 'リャ' => 'lya', 'リェ' => 'lye', 'リィ' => 'lyi',
        'リョ' => 'lyo', 'リュ' => 'lyu', 'ミャ' => 'mya', 'ミェ' => 'mye', 'ミィ' => 'myi', 'ミョ' => 'myo',
        'ミュ' => 'myu', 'ン' => 'n', 'ニャ' => 'nya', 'ニェ' => 'nye', 'ニィ' => 'nyi', 'ニョ' => 'nyo',
        'ニュ' => 'nyu', 'ピャ' => 'pya', 'ピェ' => 'pye', 'ピィ' => 'pyi', 'ピョ' => 'pyo', 'ピュ' => 'pyu',
        'リャ' => 'rya', 'リェ' => 'rye', 'リィ' => 'ryi', 'リョ' => 'ryo', 'リュ' => 'ryu', 'シャ' => 'sha',
        'シェ' => 'she', 'シ' => 'shi', 'ショ' => 'sho', 'シュ' => 'shu', 'スァ' => 'swa', 'スェ' => 'swe',
        'スィ' => 'swi', 'スォ' => 'swo', 'スゥ' => 'swu', 'シャ' => 'sya', 'シェ' => 'sye', 'シィ' => 'syi',
        'ショ' => 'syo', 'シュ' => 'syu', 'テャ' => 'tha', 'テェ' => 'the', 'ティ' => 'thi', 'テョ' => 'tho',
        'テュ' => 'thu', 'ツャ' => 'tsa', 'ツェ' => 'tse', 'ツィ' => 'tsi', 'ツョ' => 'tso', 'ツ' => 'tsu',
        'トァ' => 'twa', 'トェ' => 'twe', 'トィ' => 'twi', 'トォ' => 'two', 'トゥ' => 'twu', 'チャ' => 'tya',
        'チェ' => 'tye', 'チィ' => 'tyi', 'チョ' => 'tyo', 'チュ' => 'tyu', 'ヴャ' => 'vya', 'ヴェ' => 'vye',
        'ヴィ' => 'vyi', 'ヴョ' => 'vyo', 'ヴュ' => 'vyu', 'ウァ' => 'wha', 'ウェ' => 'whe', 'ウィ' => 'whi',
        'ウォ' => 'who', 'ウゥ' => 'whu', 'ヱ' => 'wye', 'ヰ' => 'wyi', 'ジャ' => 'zha', 'ジェ' => 'zhe',
        'ジィ' => 'zhi', 'ジョ' => 'zho', 'ジュ' => 'zhu', 'ジャ' => 'zya', 'ジェ' => 'zye', 'ジィ' => 'zyi',
        'ジョ' => 'zyo', 'ジュ' => 'zyu',

        // "Greeklish"
        'Γ' => 'G', 'Δ' => 'E', 'Θ' => 'Th', 'Λ' => 'L', 'Ξ' => 'X', '� ' => 'P', 'Σ' => 'S', 'Φ' => 'F', 'Ψ' => 'Ps',
        'γ' => 'g', 'δ' => 'e', 'θ' => 'th', 'λ' => 'l', 'ξ' => 'x', 'π' => 'p', 'σ' => 's', 'φ' => 'f', 'ψ' => 'ps',

        // Thai
        'ก' => 'k', 'ข' => 'kh', 'ฃ' => 'kh', 'ค' => 'kh', 'ฅ' => 'kh', 'ฆ' => 'kh', 'ง' => 'ng', 'จ' => 'ch',
        'ฉ' => 'ch', 'ช' => 'ch', 'ซ' => 's', 'ฌ' => 'ch', 'ญ' => 'y', 'ฎ' => 'd', 'ฏ' => 't', 'ฐ' => 'th',
        'ฑ' => 'd', 'ฒ' => 'th', 'ณ' => 'n', 'ด' => 'd', 'ต' => 't', 'ถ' => 'th', 'ท' => 'th', 'ธ' => 'th',
        'น' => 'n', 'บ' => 'b', 'ป' => 'p', 'ผ' => 'ph', 'ฝ' => 'f', 'พ' => 'ph', 'ฟ' => 'f', '� ' => 'ph',
        'ม' => 'm', 'ย' => 'y', 'ร' => 'r', 'ฤ' => 'rue', 'ฤๅ' => 'rue', 'ล' => 'l', 'ฦ' => 'lue',
        'ฦๅ' => 'lue', 'ว' => 'w', 'ศ' => 's', 'ษ' => 's', 'ส' => 's', 'ห' => 'h', 'ฬ' => 'l', 'ฮ' => 'h',
        'ะ' => 'a', '–ั' => 'a', 'รร' => 'a', 'า' => 'a', 'รร' => 'an', 'ำ' => 'am', '–ิ' => 'i', '–ี' => 'i',
        '–ึ' => 'ue', '–ื' => 'ue', '–ุ' => 'u', '–ู' => 'u', 'เะ' => 'e', 'เ–็' => 'e', 'เ' => 'e', 'แะ' => 'ae',
        'แ' => 'ae', 'โะ' => 'o', 'โ' => 'o', 'เาะ' => 'o', 'อ' => 'o', 'เอะ' => 'oe', 'เ–ิ' => 'oe',
        'เอ' => 'oe', 'เ–ียะ' => 'ia', 'เ–ีย' => 'ia', 'เ–ือะ' => 'uea', 'เ–ือ' => 'uea', '–ัวะ' => 'ua',
        '–ัว' => 'ua', 'ว' => 'ua', 'ใ' => 'ai', 'ไ' => 'ai', '–ัย' => 'ai', 'ไย' => 'ai', 'าย' => 'ai',
        'เา' => 'ao', 'าว' => 'ao', '–ุย' => 'ui', 'โย' => 'oi', 'อย' => 'oi', 'เย' => 'oei', 'เ–ือย' => 'ueai',
        'วย' => 'uai', '–ิว' => 'io', 'เ–็ว' => 'eo', 'เว' => 'eo', 'แ–็ว' => 'aeo', 'แว' => 'aeo',
        'เ–ียว' => 'iao',

        // Korean
        'ㄱ' => 'k', 'ㅋ' => 'kh', 'ㄲ' => 'kk', 'ㄷ' => 't', 'ㅌ' => 'th', 'ㄸ' => 'tt', 'ㅂ' => 'p',
        'ㅍ' => 'ph', 'ㅃ' => 'pp', 'ㅈ' => 'c', 'ㅊ' => 'ch', 'ㅉ' => 'cc', 'ㅅ' => 's', 'ㅆ' => 'ss',
        'ㅎ' => 'h', 'ㅇ' => 'ng', 'ㄴ' => 'n', 'ㄹ' => 'l', 'ㅁ' => 'm', 'ㅏ' => 'a', 'ㅓ' => 'e', 'ㅗ' => 'o',
        'ㅜ' => 'wu', 'ㅡ' => 'u', 'ㅣ' => 'i', 'ㅐ' => 'ay', 'ㅔ' => 'ey', 'ㅚ' => 'oy', 'ㅘ' => 'wa', 'ㅝ' => 'we',
        'ㅟ' => 'wi', 'ㅙ' => 'way', 'ㅞ' => 'wey', 'ㅢ' => 'uy', 'ㅑ' => 'ya', 'ㅕ' => 'ye', 'ㅛ' => 'oy',
        '� ' => 'yu', 'ㅒ' => 'yay', 'ㅖ' => 'yey',
    );

    /**
     * Unescape escaped string with JavaScript encodeURIComponent function.
     * UTF-8 supported.
     */
    public static function decodeURIComponent($value) {
        return html_entity_decode(
            preg_replace("/%u([0-9a-f]{3,4})/i", "&#x\\1;", urldecode($value))
            , null, 'UTF-8');
    }

    /**
     * Unescape escaped string with JavaScript escape function.
     * UTF-8 supported.
     *
     * @uses Strings::codeToUTF
     * @param string $input
     * @param string $iconvTo
     * @return string
     */
    public static function unescape($input, $iconvTo = 'UTF-8') {
        $output = '';
        $iPos = 0;
        $len = strlen($input);

        while ($iPos < $len) {
            $charAt = substr($input, $iPos, 1);
            if ($charAt == '%') {
                $iPos++;
                $charAt = substr($input, $iPos, 1);
                if ($charAt == 'u') {
                    // Unicode character
                    $iPos++;
                    $unicodeHexVal = substr($input, $iPos, 4);
                    $unicode = hexdec($unicodeHexVal);
                    $output .= Strings::codeToUTF($unicode);
                    $iPos += 4;
                } else {
                    // Escaped ascii character
                    $hexVal = substr($input, $iPos, 2);
                    if (hexdec($hexVal) > 127) {
                        // Convert to Unicode
                        $output .= Strings::codeToUTF(hexdec($hexVal));
                    } else {
                        $output .= chr(hexdec($hexVal));
                    }
                    $iPos += 2;
                }
            } else {
                $output .= $charAt;
                $iPos++;
            }
        }

        if ($iconvTo != 'UTF-8') {
            $output = iconv('UTF-8', $iconvTo, $output);
        }
        return $output;
    }

    /**
     * Convert number code to char representation in UTF.
     * Function required for Strings::unescape
     *
     * @param int $num
     * @return string
     */
    private static function codeToUTF($num) {
        if ($num < 128)
            return chr($num);
        if ($num < 1024)
            return chr(($num >> 6) + 192) . chr(($num & 63) + 128);
        if ($num < 32768)
            return chr(($num >> 12) + 224) . chr((($num >> 6) & 63) + 128) . chr(($num & 63) + 128);
        if ($num < 2097152)
            return chr(($num >> 18) + 240) . chr((($num >> 12) & 63) + 128) . chr((($num >> 6) & 63) + 128) . chr(($num & 63) + 128);
        return '';
    }

    /**
     * Convert UTF8 string to HTML compatible.
     * mb_string ext. required
     *
     * @param string $input
     * @return string
     */
    public static function utf8Entity($input) {
        return mb_convert_encoding($input, 'HTML-ENTITIES', 'UTF-8');
    }

    public static function utf2win($input) {
        return iconv('UTF-8', 'WINDOWS-1250//TRANSLIT', $input);
    }

    public static function utf2iso($input) {
        return iconv('UTF-8', 'ISO-8859-2', $input);
    }

    public static function crc16($data) {
        $crc = 0xFFFF;
        for ($i = 0; $i < strlen($data); $i++) {
            $x = (($crc >> 8) ^ ord($data[$i])) & 0xFF;
            $x ^= $x >> 4;
            $crc = (($crc << 8) ^ ($x << 12) ^ ($x << 5) ^ $x) & 0xFFFF;
        }
        return $crc;
    }

    public static function crc32($data, $unsigned = false) {
        if ($unsigned)
            return sprintf('%u', crc32($data));
        else
            crc32($data);
    }

    public static function trim($input) {
        $patterns = array("/\n/",
            "/\t/",
            "/\r/",
            "/\x0B/",
            '/\x00/',
            '/ ( )+ /i',
            "/^( )+/i",
            "/( )+$/i");

        $replacements = array('',
            '',
            '',
            '',
            '',
            ' ',
            '');
        return preg_replace($patterns, $replacements, $input);
    }

    public static function strip($input) {
        $patterns = array("'<form [^>]*?>.*?</form>'si",
            "'<script [^>]*?>.*?</script>'si",
            "'<style [^>]*?>.*?</style>'si"
        );

        $replacements = array('',
            '',
            '');
        return strip_tags(preg_replace($patterns, $replacements, $input));
    }

    public static function replaceOnce($search, $replace, $subject) {
        $firstChar = strpos($subject, $search);
        if ($firstChar !== false) {
            $beforeStr = substr($subject, 0, $firstChar);
            $afterStr = substr($subject, $firstChar + strlen($search));
            return $beforeStr . $replace . $afterStr;
        } else {
            return $subject;
        }
    }

    public static function jsonDecode($json, $toArray = false) {
        return json_decode(self::removeTrailingCommas(Strings::utf8Repair($json)), $toArray);
    }

    public static function removeTrailingCommas($json) {
        return preg_replace('/,\s*([\]}])/m', '$1', $json);
    }

    public static function utf8Repair($string) {
        $string = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]' .
            '|[\x00-\x7F][\x80-\xBF]+' .
            '|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*' .
            '|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})' .
            '|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/S',
            '', $string);

        $string = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]' .
            '|\xE2[\x80-\x9F][\x80-\xBF]' .
            '|\xED[\xA0-\xBF][\x80-\xBF]/S', '', $string);

        $string = preg_replace("/([^\r])\n/is", '$1', $string);
        $string = @iconv('UTF-8', 'UTF-8//IGNORE', $string);
        return $string;
    }

    public static function utf8JsonDecode($json, $toArray = false) {
        return json_decode(self::removeTrailingCommas(Strings::utf8Repair(utf8_decode($json))), $toArray);
    }

    public static function utf8Validate($string, $allowControlCodes = false)
        // returns true if this is a valid utf-8 string, false otherwise.
        // if allowcontrolcodes is false (default), then most C0 codes below 0x20, as
        // well as C1 codes 127-159, will be denied - recommend false for html/xml
    {
        if ($string == '') return '';
        return preg_match($allowControlCodes
            ? '/^[\x00-\x{d7ff}\x{e000}-\x{10ffff}]++$/u'
            : '/^[\x20-\x7e\x0a\x09\x0d\x{a0}-\x{d7ff}\x{e000}-\x{10ffff}]++$/u',
            $string) ? true : false;
    }

    /**
     * Replace accented UTF-8 characters by unaccented ASCII-7 equivalents
     *
     * $sCharHyperlink = - change spaces to given char
     *
     * @author Andreas Gohr <andi@splitbrain.org>
     * @author gzagrobelny (php5 lib)
     * @author mkuzior (hyperlink output)
     */
    public static function utf8Deaccent($string, $charHyperlink = false, $toLower = true) {

        $string = str_replace(array_keys(Strings::$aUtf8LowerAccents), array_values(Strings::$aUtf8LowerAccents), $string);
        $string = str_replace(array_keys(Strings::$aUtf8UpperAccents), array_values(Strings::$aUtf8UpperAccents), $string);
        $string = str_replace(array_keys(Strings::$aUtf8Romanization), array_values(Strings::$aUtf8Romanization), $string);

        if ($toLower) {
            $string = mb_strtolower($string);
        }

        if ($charHyperlink) {
            $aSearchArray = array("'([^0-9 a-z" . preg_quote($charHyperlink) . "])'si", "' 'i");
            $aReplaceArray = array("", $charHyperlink);
            return preg_replace($aSearchArray, $aReplaceArray, $string);
        } else return $string;
    }

    public static function substrToChar($sString, $sChar, $sMoreTag, $iLength) {
        if (mb_strlen($sString) > $iLength) {
            $sString = mb_substr($sString, 0, $iLength);
            $iStrPosition = mb_strrpos($sString, $sChar);
            if ($iStrPosition === FALSE) {
                return $sString . $sMoreTag;
            } else {
                return substr($sString, 0, $iStrPosition) . $sMoreTag;
            }
        } else {
            return $sString;
        }

    }

    public static function br2nl($str) {
        $str = preg_replace("/(\r\n|\n|\r)/", "", $str);
        return preg_replace("=<br */?>=i", "\\n", $str);
    }

    /**
     * Pretty cut given string.
     *
     * @param string $string
     * @param int $charsAmount
     * @param string $endSign
     * @param string $lineBreakChar
     * @return string
     */
    public static function wordSubstr($string, $charsAmount, $endSign = '...', $lineBreakChar = ' ') {
        $string = str_replace('&nbsp;', ' ', strip_tags($string));
        if ((mb_strlen($string) > $charsAmount)) {
            $iWhitespacePosition = mb_strpos($string, $lineBreakChar, $charsAmount - 10);
            return $string = mb_substr($string, 0, $iWhitespacePosition) . $endSign;
        } else return $string;
    }

    /**
     * Convert named entities to decimals entities.
     * (sometimes needed to format xml strings)
     *
     * @var String $content
     * @return String
     */
    public static function entitiesNamedToDecimals($content) {
        $aDecimals = array('&#948;', '&#916;', '&#8221;', '&#8211;', '&#34;', '&#38;', '&#38;', '&#60;', '&#62;', '&#160;', '&#161;', '&#162;', '&#163;', '&#164;', '&#165;', '&#166;', '&#167;', '&#168;', '&#169;', '&#170;', '&#171;', '&#172;', '&#173;', '&#174;', '&#175;', '&#176;', '&#177;', '&#178;', '&#179;', '&#180;', '&#181;', '&#182;', '&#183;', '&#184;', '&#185;', '&#186;', '&#187;', '&#188;', '&#189;', '&#190;', '&#191;', '&#192;', '&#193;', '&#194;', '&#195;', '&#196;', '&#197;', '&#198;', '&#199;', '&#200;', '&#201;', '&#202;', '&#203;', '&#204;', '&#205;', '&#206;', '&#207;', '&#208;', '&#209;', '&#210;', '&#211;', '&#212;', '&#213;', '&#214;', '&#215;', '&#216;', '&#217;', '&#218;', '&#219;', '&#220;', '&#221;', '&#222;', '&#223;', '&#224;', '&#225;', '&#226;', '&#227;', '&#228;', '&#229;', '&#230;', '&#231;', '&#232;', '&#233;', '&#234;', '&#235;', '&#236;', '&#237;', '&#238;', '&#239;', '&#240;', '&#241;', '&#242;', '&#243;', '&#244;', '&#245;', '&#246;', '&#247;', '&#248;', '&#249;', '&#250;', '&#251;', '&#252;', '&#253;', '&#254;', '&#255;');
        $aNamed = array('&delta;', '&Delta;', '&rdquo;', '&ndash;', '&quot;', '&amp;', '&amp;', '&lt;', '&gt;', '&nbsp;', '&iexcl;', '&cent;', '&pound;', '&curren;', '&yen;', '&brvbar;', '&sect;', '&uml;', '&copy;', '&ordf;', '&laquo;', '&not;', '&shy;', '&reg;', '&macr;', '&deg;', '&plusmn;', '&sup2;', '&sup3;', '&acute;', '&micro;', '&para;', '&middot;', '&cedil;', '&sup1;', '&ordm;', '&raquo;', '&frac14;', '&frac12;', '&frac34;', '&iquest;', '&Agrave;', '&Aacute;', '&Acirc;', '&Atilde;', '&Auml;', '&Aring;', '&AElig;', '&Ccedil;', '&Egrave;', '&Eacute;', '&Ecirc;', '&Euml;', '&Igrave;', '&Iacute;', '&Icirc;', '&Iuml;', '&ETH;', '&Ntilde;', '&Ograve;', '&Oacute;', '&Ocirc;', '&Otilde;', '&Ouml;', '&times;', '&Oslash;', '&Ugrave;', '&Uacute;', '&Ucirc;', '&Uuml;', '&Yacute;', '&THORN;', '&szlig;', '&agrave;', '&aacute;', '&acirc;', '&atilde;', '&auml;', '&aring;', '&aelig;', '&ccedil;', '&egrave;', '&eacute;', '&ecirc;', '&euml;', '&igrave;', '&iacute;', '&icirc;', '&iuml;', '&eth;', '&ntilde;', '&ograve;', '&oacute;', '&ocirc;', '&otilde;', '&ouml;', '&divide;', '&oslash;', '&ugrave;', '&uacute;', '&ucirc;', '&uuml;', '&yacute;', '&thorn;', '&yuml;');
        $content = str_replace($aNamed, $aDecimals, $content);
        $content = str_ireplace($aNamed, $aDecimals, $content);
        $content = str_ireplace('&', '&#38;', $content);
        return $content;
    }

    public static function isXml($xml) {
        libxml_use_internal_errors(true);

        $oDocument = new DOMDocument('1.0', 'utf-8');
        $oDocument->loadXML($xml);
        $aErrors = libxml_get_errors();
        if (count($aErrors)) {
            libxml_clear_errors();
            return false;
        } else {
            return true;
        }
    }

    /**
     * Length must be a multiple of 2. So 14 will work,
     * 15 won't, 16 will, 17 won't and so on.
     * @param int $length
     */
    public static function getReadablePassword($length = 8) {
        $conso = array('b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z');
        $vocal = array('a', 'e', 'i', 'o', 'u');
        $password = '';
        srand((double)microtime() * 1000000);
        $max = $length / 2;
        for ($i = 1; $i <= $max; $i++) {
            $password .= $conso[rand(0, 19)];
            $password .= $vocal[rand(0, 4)];
        }
        return $password;
    }

    public static function getRandomizedChars($length = 10) {
        $letters = '';
        $alphabet = range('a', 'z');
        while ($length > 0) {
            $letters .= $alphabet[rand(0, 25)];
            $length--;
        }
        return $letters;
    }

    public static function utf8Decode($content) {
        return str_replace(array(
            '&#xC4;&#x99;', '&#xC3;&#xB3;', '&#xC4;&#x85;', '&#xC5;&#x9B;', '&#xC5;&#x82;', '&#xC5;&#xBC;', '&#xC5;&#xBA;', '&#xC4;&#x87;', '&#xC5;&#x84;',
            '&#xC4;&#x98;', '&#xC3;&#x93;', '&#xC4;&#x84;', '&#xC5;&#x9A;', '&#xC5;&#x81;', '&#xC5;&#xBB;', '&#xC5;&#xB9;', '&#xC4;&#x86;', '&#xC5;&#x83;'
        ), array(
            'ę', 'ó', 'ą', 'ś', 'ł', 'ż', 'ź', 'ć', 'ń',
            'Ę', 'Ó', 'Ą', 'Ś', 'Ł', 'Ż', 'Ź', 'Ć', 'Ń'
        ), $content);
    }

    /**
     * Compare versions in format: 1.0.0 (Major, Minor, Fixes)
     *
     * @param type $program Program version to compare
     * @param type $current Current version that should be
     * @return boolean
     */
    public static function isNewerVersion($program, $current) {
        $program = explode('.', $program);
        $current = explode('.', $current);

        if (count($program) != count($current)) return true;

        if ((int)$current[0] > (int)$program[0]) {
            return true;
        } else {
            if ((int)$current[1] > (int)$program[1]) {
                return true;
            } else {
                if ((int)$current[1] == (int)$program[1] && (int)$current[2] > (int)$program[2]) {
                    return true;
                }
            }
        }

        return false;
    }

    public static function replaceUnicodeEscapeSequence($string) {

        function replace_unicode_escape_sequence($match) {
            return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
        }

        return preg_replace_callback('/\\\\u([0-9a-f]{4})/i', 'replace_unicode_escape_sequence', $string);
    }

    public static function isJSON($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}
