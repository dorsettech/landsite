<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-08)
 * @version 1.0
 */

namespace App\View;

use Cake\View\View;

/**
 * Application View
 *
 * Your application’s default view class
 *
 * @link http://book.cakephp.org/3.0/en/views.html#the-app-view
 */
class AppView extends View
{
    /**
     * Initialization hook method.
     *
     * @return void
     */
    public function initialize()
    {
        $this->loadGlobalHelpers();
    }

    /**
     * Load global helpers for use in administration panel and front end
     *
     * @return void
     */
    public function loadGlobalHelpers()
    {
        $this->loadHelper('Form', ['className' => 'Form', 'useCustomFileInput' => true, 'templates' => 'form-templates', 'widgets' => ['switcher' => ['Switcher']]]);
        $this->loadHelper('Modal', ['className' => 'Bootstrap.Modal']);
        $this->loadHelper('Navbar', ['className' => 'Bootstrap.Navbar']);
        $this->loadHelper('Paginator', ['className' => 'Bootstrap.Paginator', 'templates' => 'paginator-templates']);
    }
}
