<?php
/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */

namespace App\View\Cell;

use App\Model\Entity\Service;
use Cake\Routing\Router;
use Cake\View\Cell;

/**
 * PropertiesSearchCell
 */
class PropertiesSearchCell extends Cell
{
    /**
     * Display method
     *
     * @param string|null $purpose Property purpose.
     * @return void
     */
    public function display($purpose = ''): void
    {
        $this->loadModel('PropertyTypes');
        $this->loadModel('PropertyAttributes');

        $action = ['_name' => 'properties-listing'];
        if ($purpose instanceof Service) {
            $action = ['_name' => 'service-properties', $purpose->id, $purpose->slug];
            $purpose = '';
        }

        $data               = Router::getRequest()->getQuery();
        $propertyType       = !empty($data['type']) ? (int) $data['type'] : '';
        $propertyTypes      = $this->PropertyTypes->find('list', ['conditions' => ['visibility' => true], 'order' => ['position' => 'ASC']]);
        $propertyAttributes = $this->PropertyAttributes->getByType($propertyType);

        $this->set(compact('propertyTypes', 'propertyAttributes', 'purpose', 'data', 'action'));
    }
}
