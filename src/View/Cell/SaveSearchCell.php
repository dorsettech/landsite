<?php
/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */

namespace App\View\Cell;

use Cake\Routing\Router;
use Cake\View\Cell;

/**
 * SaveSearchCell
 */
class SaveSearchCell extends Cell
{

    /**
     * Display method
     *
     * @param string|null $type Search type, currently available: services, properties.
     * @return void
     */
    public function display($type = 'services')
    {
        $request = Router::getRequest();
        $data    = $request->getQuery();
        $user    = $request->getSession()->read('Auth.User');

        $displaySaveSearch = false;
        if (!empty($user['id']) && !empty($data) && empty($data['saved'])) {
            $displaySaveSearch = true;
        }

        $this->set(compact(['displaySaveSearch', 'type', 'data']));
    }
}
