<?php
/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */

namespace App\View\Cell;

use Cake\Routing\Router;
use Cake\View\Cell;

/**
 * ServicesSearchCell
 */
class ServicesSearchCell extends Cell
{
    /**
     * Display method
     *
     * @return void
     */
    public function display()
    {
        $data = Router::getRequest()->getQuery();

        $this->loadModel('Categories');
        $categories = $this->Categories->find('list', ['order' => ['position' => 'ASC', 'name' => 'ASC']]);

        $this->set(compact(['categories', 'data']));
    }
}
