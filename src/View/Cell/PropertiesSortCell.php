<?php
/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */

namespace App\View\Cell;

use Cake\Routing\Router;
use Cake\View\Cell;

/**
 * PropertiesSortCell
 */
class PropertiesSortCell extends Cell
{
    /**
     * Sort options
     *
     * @var array
     */
    public $sortOptions = [
        'price_desc'    => 'Price: High - Low',
        'price_asc'     => 'Price: Low - High',
        'distance_asc'  => 'Distance: Closest',
        'distance_desc' => 'Distance: Furthest'
    ];

    /**
     * Display method
     *
     * @return void
     */
    public function display(): void
    {
        $request   = Router::getRequest();
        $path      = $request->getPath();
        $sortQuery = $request->getQuery();

        $sortUrl     = $path;
        $sortOptions = $this->sortOptions;

        $this->set(compact(['sortUrl', 'sortQuery', 'sortOptions']));
    }
}
