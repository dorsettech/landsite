<?php

/**
 * @author  Kamil
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\View\Helper;

use Cake\Routing\Router;
use Cake\View\Helper;

/**
 * MenuHelper
 */
class SidebarHelper extends Helper
{
    /**
     * Helper list
     *
     * @var array
     */
    public $helpers = ['Html', 'Form', 'Frontend', 'ACL'];

    /**
     * Levels
     *
     * @var array
     */
    private $levels = ['nav', 'sub-menu', 'sub-menu'];

    /**
     * Html
     *
     * @var string
     */
    private $html = '';

    /**
     * Generate menu
     *
     * @param array $menus Array data.
     *
     * @return string
     */
    public function generateMenu(array $menus = [])
    {
        return $this->menu($menus);
    }

    /**
     * Menu
     *
     * @param array        $array Array data.
     * @param integer|null $level Level number.
     *
     * @return void
     */
    private function menu(array $array = [], $level = 0)
    {
        $items = '';
        foreach ($array as $val) {
            if (isset($val['enable']) && !$val['enable']) {
                continue;
            }
            $items .= $this->singleItem($val, $level);
        }

        $html = "<ul class='" . $this->levels[$level] . "'>";

        if ($level == 0) {
            $html .= '<li class="nav-header">' . __('Navigation') . '</li>';
        }

        if (!empty($items)) {
            $html .= $items;
        }

        if ($level == 0) {
            $html .= '<li><a href="' . $this->request->getRequestTarget() . '#" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>';
        }

        $html .= "</ul>";

        if (empty($items)) {
            $html = '';
        }

        return $html;
    }

    /**
     * Single item
     *
     * @param array|null   $val   Array value.
     * @param integer|null $level Level number.
     *
     * @return void
     */
    private function singleItem($val, $level)
    {
        $html = $target = "";

        $hasChildren = isset($val['children']);

        $href = $this->request->getRequestTarget() . '#';
        if (!empty($val['controller'])) {
            $baseUrl           = ['plugin' => !empty($val['plugin']) ? $val['plugin'] : null, 'controller' => $val['controller'], 'action' => isset($val['action']) ? $val['action'] : ''];
            $baseUrl['prefix'] = (!empty($val['prefix'])) ? $val['prefix'] : $this->_View->viewVars['prefix'];
            $href              = array_merge($baseUrl, (isset($val['params']) ? $val['params'] : []));
        }

        if ($this->ACL->hasAccess($href)) {
            $liClass = [];
            if (isset($val['class'])) {
                $liClass[] = $val['class'];
            }
            if ($hasChildren) {
                $liClass[] = 'has-sub';
            }
            if (self::isActive($val, !($level === 0 && !isset($val['children']) && isset($val['action']))) || self::hasChildActive($val)) {
                $liClass[] = 'active';
            }

            $html .= "<li";
            if (!empty($liClass)) {
                $html .= ' class="' . implode(' ', $liClass) . '"';
            }
            $html .= ">";

            $target = (isset($val['newtab']) && $val['newtab'] === true) ? '_blank' : false;

            $linkName = '';
            if ($hasChildren) {
                $linkName .= '<b class="caret"></b> ';
            }
            if (isset($val['icon'])) {
                $linkName .= '<i class="material-icons">' . $val['icon'] . '</i>';
                $linkName .= '<span class="nav-label">' . $val['label'] . '</span>';
            } else {
                $linkName .= isset($val['label']) ? $val['label'] : $val['controller'];
            }
            $html .= $this->Html->link($linkName, $href, ['target' => $target, 'escape' => false]);

            if (!empty($val['children'])) {
                $children = $this->menu($val['children'], $level + 1);
                if (empty($children)) {
                    return '';
                }
                $html .= $children;
            }
            $html .= "</li>";
        }

        return $html;
    }

    /**
     * Get role
     *
     * @param array $val Array data.
     *
     * @return string
     */
    private function getRole(array $val = [])
    {
        if (isset($val['action']) && $val['action'] == 'add') {
            return 'can_create';
        }

        return 'can_read';
    }

    /**
     * Value update
     *
     * @param array|null $val Value.
     *
     * @return mixed
     */
    private function valUpdate($val)
    {
        $val['action'] = isset($val['action']) ? $val['action'] : 'index';
        $val['prefix'] = isset($val['prefix']) ? $val['prefix'] : $this->request->getParam('prefix');

        return $val;
    }

    /**
     * Has child active
     *
     * @param array|null $val Value.
     *
     * @return boolean
     */
    private function hasChildActive($val)
    {
        if (empty($val['children'])) {
            return false;
        }
        foreach ($val['children'] as $child) {
            if (self::isActive($child, false)) {
                return true;
            } elseif (self::hasChildActive($child)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Is active
     *
     * @param array|null   $val         Value.
     * @param boolean|null $checkAction Check action.
     *
     * @return boolean
     */
    private function isActive(array $val = [], $checkAction = true)
    {
        if ($checkAction) {
            $action = (isset($val['action']) ? strtolower($val['action']) : 'index');
        }
        if (isset($val['controller'])) {
            return strtolower($val['controller']) === strtolower($this->request->getParam('controller')) &&
                (($checkAction) ? strtolower($action) === strtolower($this->request->getParam('action')) : true) &&
                (isset($val['params']) ? array_diff($val['params'], $this->request->getParam('pass')) === [] : true);
        }

        return false;
    }
}
