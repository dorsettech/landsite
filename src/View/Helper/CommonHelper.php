<?php

/**
 * @author  Stefan
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\View\Helper;

use Cake\View\Helper;

/**
 * CommonHelper
 */
class CommonHelper extends Helper
{
    /**
     * Required helpers
     *
     * @var array
     */
    public $helpers = ['Html'];

    /**
     * Generates add button
     * Mandatory keys in array: key and url
     *
     * @param array $data Array data.
     *
     * @return string|null
     */
    public function addButton(array $data = [])
    {
        if (!empty($data) && isset($data['name']) && isset($data['url'])) {
            return $this->Html->link($data['name'], $data['url'], ['class' => 'btn btn-primary']);
        }

        return null;
    }

    /**
     * Generates back (cancel) button
     * Mandatory keys in array: key and url
     *
     * @param array $array Array data.
     *
     * @return string|null
     */
    public function backButton(array $array = [])
    {
        $btnName = isset($array['name']) ? $array['name'] : __('Cancel');
        $btnUrl = isset($array['url']) ? $array['url'] : ['action' => 'index'];

        return $this->Html->link($btnName, $btnUrl, ['class' => 'btn btn-sm btn-warning']);
    }

    /**
     * Display ico image with preview image if it's set
     *
     * @param object|null $image Image object.
     *
     * @return string|null
     */
    public function displayIcoImage($image)
    {
        if (empty($image)) {
            return null;
        }

        $link1 = $link2 = '';

        if (!empty($image->image_prev)) {
            $link1 = '<a href="' . $image->image_prev . '" class="image-link">';
            $link2 = '</a>';
        }

        return '<div class="image-ico">' . $link1 . '<img src="' . $image->image_ico . '" alt="' . $image->alt . '" title="' . $image->title . '" />' . $link2 . '</div>';
    }

    /**
     * Displays information about possible image types and max file size
     *
     * @param array $imageIndicators Image array data.
     *
     * @return string
     */
    public function imageIndicators(array $imageIndicators = [])
    {
        $fileExtensions = [];
        foreach ($imageIndicators['available_image_types'] as $fileMime) {
            $fileExtensions[] = $fileMime;
        }

        return '<div class="image-indicator text-danger">' . __('Supported file image formats: {0}. Max file size {1} MB', strtoupper(implode(", ", $fileExtensions)), strtoupper($imageIndicators['image_max_size'])) . '</div>';
    }

    /**
     * File indicators
     *
     * @param array $fileIndicators File array.
     *
     * @return string
     */
    public function fileIndicators(array $fileIndicators = [])
    {
        $fileExtensions = [];
        foreach ($fileIndicators['available_file_types'] as $fileMime) {
            $fileExtensions[] = $fileMime;
        }

        return '<div class="image-indicator text-danger">' . __('Supported file image formats: {0}. Max file size {1} MB', strtoupper(implode(", ", $fileExtensions)), strtoupper($fileIndicators['image_max_size'])) . '</div>';
    }

    /**
     * App indicators
     *
     * @param array $appIndicators Array types.
     *
     * @return string
     */
    public function appIndicators(array $appIndicators = [])
    {
        $appExtensions = [];
        foreach ($appIndicators['available_app_types'] as $appMime) {
            $appExtensions[] = $appMime;
        }

        return '<div class="image-indicator text-danger">' . __('Supported file image formats: {0}. Max file size {1} MB', strtoupper(implode(", ", $appExtensions)), strtoupper($appIndicators['image_max_size'])) . '</div>';
    }
}
