<?php
/**
 * @author  Kamil
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\View\Helper;

use Cake\ORM\TableRegistry;
use App\Model\Entity\Page;
use Cake\View\Helper;

/**
 * MenuHelper
 */
class MenuHelper extends Helper
{
    /**
     * Helpers
     *
     * @var array
     */
    public $helpers = ['Html', 'Form', 'Urlname'];

    /**
     * Href
     *
     * @var string
     */
    private $href = '';

    /**
     * Request url
     *
     * @var string
     */
    private $requestUrl = '';

    /**
     * Generate default
     *
     * @param object|null $menu    Menu.
     * @param array       $options Additional options.
     *
     * @return string|null
     */
    public function generateDefault($menu, array $options = [])
    {
        if (empty($menu)) {
            return null;
        }
        if (isset($options['rawMenu']) && $options['rawMenu']) {
            return $menu;
        }
        $this->requestUrl = '/'.$this->request->getRequestTarget();

        $return = $this->listCreate($options);
        if (isset($options['noParent']) && $options['noParent']) {
            $return = '';
        }
        foreach ($menu->pages as $element) {
            if (isset($element['page_type']) && !empty($element['page_type']['menu'])) {
                $model               = TableRegistry::getTableLocator()->get($element->page_type->model);
                $element['children'] = $model->{$element->page_type->menu}();
            }
            $return .= $this->getItem($element, $options);
        }
        $return .= '</ul>';

        return $return;
    }

    /**
     * Get item
     *
     * @param object|array $element Element to analyze.
     * @param array        $options Additional options.
     *
     * @return string
     */
    public function getItem($element, array $options = [])
    {
        if ($element instanceof Page) {
            $this->href = $this->Urlname->generate($element['urlname']);
            $this->href = ($element->id === 1 ? '' : $this->href);
        } else {
            $this->href = $element['urlname'];
        }

        //in_menu added, if parent isn't display in menu, children should be
        if ((isset($options['noParent']) && $options['noParent']) || (isset($element['in_menu']) && $element['in_menu'] === 0)) {
            $return              = '';
            $options['noParent'] = false;
        } else {
            $liClass = (!empty($element['children']) ? 'dropdown' : '').($this->checkActive($this->href) ? ' active' : '');
            $href    = isset($element['redirection']) && $element['redirection'] !== '' ? $element['redirection'] : $this->href;
            $blank   = isset($element['target_blank']) && $element['target_blank'] ? ' target="_blank"' : '';
            $aAttrs  = !empty($element['children']) ? ' class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"' : '';
            $caret   = !empty($element['children']) ? ' <span class="caret"></span>' : '';
            if (!empty($element['children'])) {
                $href = $this->requestUrl.'#'.$element['urlname'];
            }
            $return = '<li class="'.$liClass.'"><a href="'.$href.'"'.$blank.$aAttrs.'>'.h($element['name']).$caret.'</a>';
        }

        if (!empty($element['children'])) {
            $return .= $this->generateDropdown($element['children'], ['className' => 'dropdown-menu']);
        }
        $return .= '</li>';

        return $return;
    }

    /**
     * Create list
     *
     * @param array $options Additional options.
     *
     * @return string
     */
    public function listCreate(array $options = [])
    {
        $class = isset($options['className']) ? $options['className'] : '';
        $id    = isset($options['id']) ? $options['id'] : '';

        return '<ul'.($id ? ' id="'.$id.'"' : '').($class ? ' class="'.$class.'"' : '').'>';
    }

    /**
     * Create div
     *
     * @param array $options Additional options.
     *
     * @return string
     */
    public function divCreate($options = [])
    {
        $class = isset($options['className']) ? $options['className'] : '';
        $id    = isset($options['id']) ? $options['id'] : '';

        return '<div'.($id ? ' id="'.$id.'"' : '').($class ? ' class="'.$class.'"' : '').'>';
    }

    /**
     * Generate breadcrumbs
     *
     * @param array $breadcrumbs Breadcrumbs.
     *
     * @return string
     */
    public function generateBreadcrumbs(array $breadcrumbs = [])
    {
        $breadcrumbIndex  = 0;
        $list             = '';
        $breadcrumbLength = count($breadcrumbs) - 1;
        $list             .= '<ol class="breadcrumb">';
        $list             .= '<li class="breadcrumb-item"><a href="/">Home</a></li>';
        foreach ($breadcrumbs as $breadcrumb) {
            if ($breadcrumbIndex !== $breadcrumbLength) {
                $list .= '<li class="breadcrumb-item">';
                if (isset($breadcrumb['redirect']) && $breadcrumb['redirect']) {
                    $list .= '<a href="'.$breadcrumb['redirection'].'">';
                } else {
                    $list .= '<a href="'.$this->Urlname->generate($breadcrumb['url']).'">';
                }

                $list .= $breadcrumb['name'];
                $list .= '</a>';
                $list .= '</li>';
            } else {
                $list .= '<li class="breadcrumb-item active" aria-current="page">'.$breadcrumb['name'].'</li>';
            }
            $breadcrumbIndex++;
        }
        $list .= '</ol>';

        return $list;
    }

    /**
     * Check active
     *
     * @param string|null $href Href.
     *
     * @return boolean
     */
    public function checkActive($href = '')
    {
        if ($this->requestUrl === $href) {
            return true;
        }

        return false;
    }

    /**
     * Generate dropdown
     *
     * @param object|null $menu    Menu.
     * @param array       $options Additional options.
     *
     * @return string|null
     */
    public function generateDropdown($menu, $options = [])
    {
        if (empty($menu)) {
            return null;
        }
        if (isset($options['rawMenu']) && $options['rawMenu']) {
            return $menu;
        }
        $this->requestUrl = '/'.$this->request->getRequestTarget();

        $return = $this->divCreate($options);
        if (isset($options['noParent']) && $options['noParent']) {
            $return = '';
        }
        foreach ($menu as $element) {
            if (isset($element['page_type']) && !empty($element['page_type']['menu'])) {
                $model               = TableRegistry::get($element->page_type->model);
                $element['children'] = $model->{$element->page_type->menu}();
            }
            $return .= $this->getDropdownItem($element, $options);
        }
        $return .= '</div>';
        return $return;
    }

    /**
     * Get dropdown item
     *
     * @param object|array $element Element to analyze.
     * @param array        $options Additional options.
     *
     * @return string
     */
    public function getDropdownItem($element, $options = [])
    {
        if ($element instanceof Page) {
            $this->href = $this->Urlname->generate($element['urlname']);
            $this->href = ($element->id === 1 ? '' : $this->href);
        } else {
            $this->href = $element['urlname'];
        }

        //in_menu added, if parent isn't display in menu, children should be
        if ((isset($options['noParent']) && $options['noParent']) || (isset($element['in_menu']) && $element['in_menu'] === 0)) {
            $return              = '';
            $options['noParent'] = false;
        } else {
            $liClass = (!empty($element['children']) ? 'dropdown' : '').($this->checkActive($this->href) ? ' active' : '');
            $href    = isset($element['redirection']) && $element['redirection'] !== '' ? $element['redirection'] : $this->href;
            $blank   = isset($element['target_blank']) && $element['target_blank'] ? ' target="_blank"' : '';
            $aAttrs  = !empty($element['children']) ? ' class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"' : ' class="nav-link '.($this->checkActive($this->href) ? ' active' : '').'"';
            if (!empty($element['children'])) {
                $href = $this->requestUrl.'#'.$element['urlname'];
            }
            $return = ''
                .'<a href="'.$href.'"'.$blank.$aAttrs.'>'.h($element['name']).'</a>';
        }

        if (!empty($element['children'])) {
            $return .= $this->generateDropdown($element['children'], ['className' => 'dropdown-menu']);
        }

        return $return;
    }

}
