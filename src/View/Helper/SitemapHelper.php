<?php
/**
 * @author  Zordon
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\View\Helper;

use Cake\ORM\TableRegistry;
use Cake\View\Helper;
use Cake\Core\Configure;

/**
 * Sitemap Helper
 */
class SitemapHelper extends Helper
{
    /**
     * Helper list
     *
     * @var array
     */
    public $helpers = ['Url', 'Urlname'];

    /**
     * Project url
     */
    public $projectUrl;

    /**
     * Initialize
     *
     * @param array $config Default config.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->projectUrl = Configure::read('Website.url');
    }

    /**
     * Generate html sitemap
     *
     * @param array $list List.
     *
     * @return string
     */
    public function generateHtmlSitemap(array $list = [])
    {
        $return = "<ul>";
        foreach ($list as $element) {
            if (isset($element['page_type']) && !empty($element['page_type']['sitemap'])) {
                $model               = TableRegistry::getTableLocator()->get($element->page_type->model);
                $element['children'] = $model->{$element->page_type->sitemap}();
            }
            $return .= $this->createHtmlNode($element);
        }
        $return .= '</ul>';

        return $return;
    }

    /**
     * Create html node
     *
     * @param array|null $element Element.
     *
     * @return string
     */
    public function createHtmlNode($element)
    {
        if (isset($element['redirection']) && !empty($element['redirection'])) {
            $href = $element['redirection'];
        } else {
            $href = $this->Urlname->generate($element['urlname']);
        }
        $return = '<li><a href="'.$href.'">'.h($element['name']).'</a>';
        if (isset($element['children']) && !empty($element['children'])) {
            $return .= $this->generateHtmlSitemap($element['children']);
        }
        $return .= '</li>';

        return $return;
    }

    /**
     * Create xml node
     *
     * @param array $element Element.
     *
     * @return string
     */
    public function createXmlNode(array $element = [])
    {
        $return = "<url>";
        if (isset($element['redirection']) && !empty($element['redirection'])) {
            $href = $element['redirection'];
        } else {
            $href = $element['urlname'];
        }
        $return .= "<loc>".$this->projectUrl.$this->Urlname->generate($href, true)."</loc>";
        if (isset($element['seo_priority'])) {
            $return .= "<priority>".$element['seo_priority']."</priority>";
        }

        if (isset($element['modified']) && !empty($element['modified'])) {
            $return .= "<lastmod>".$element['modified']."</lastmod>\r\n";
        }
        $return .= '</url>';
        if (isset($element['children']) && !empty($element['children'])) {
            $return .= $this->generateXmlSitemap($element['children']);
        }

        return $return;
    }

    /**
     * Generate xml sitemap
     *
     * @param array $list List.
     *
     * @return string
     */
    public function generateXmlSitemap(array $list = [])
    {
        $return = '';
        foreach ($list as $element) {
            if (is_object($element)) {
                $element = json_decode(json_encode($element), true);
            }
            if (isset($element['page_type']) && !empty($element['page_type']['sitemap'])) {
                $model               = TableRegistry::getTableLocator()->get($element->page_type->model);
                $element['children'] = $model->{$element->page_type->sitemap}();
            }
            $return .= $this->createXmlNode($element);
        }

        return $return;
    }

    /**
     * Generate xml sitemap
     *
     * @param array  $list     List.
     * @param string $module   Module urlname.
     * @param string $priority Priority value.
     *
     * @return string
     */
    public function generateTieredSitemap(array $list = [], string $module = '', string $priority = '')
    {
        $return = '';
        foreach ($list as $element) {
            $return .= $this->createXmlNodeForModules($element, $module, $priority);
        }

        return $return;
    }

    /**
     * Create xml node for modules
     *
     * @param object $element        Items element.
     * @param string $module_urlname Module urlname.
     * @param string $priority       Priority value.
     *
     * @return string
     */
    public function createXmlNodeForModules($element, string $module_urlname = '', string $priority = '')
    {
        $return = "<url>\r\n";
        if (isset($element->urlname) && !empty($element->urlname)) {
            $href = $element->urlname;
        } elseif (isset($element->slug) && !empty($element->slug)) {
            $href = $element->slug;
        }
        if (!empty($href)) {
            $return .= "<loc>".$this->projectUrl.$module_urlname.'/'.$href."</loc>\r\n";
        } elseif (empty($href) && !empty($element->id)) {
            $return .= "<loc>".$this->projectUrl.$module_urlname.'?author='.$element->id."</loc>\r\n";
        }
        if (isset($element->modified) && !empty($element->modified)) {
            $return .= "<lastmod>".$element->modified->format('Y-m-d H:i:s')."</lastmod>\r\n";
        }
        if (!empty($priority)) {
            $return .= "<priority>".$priority."</priority>\r\n";
        }
        if (!empty($href)) {
            $return .= $this->generateImagePart($element);
        }
        $return .= '</url>';

        return $return;
    }

    /**
     * Generate image part of the sitemap.
     *
     * @param object $element Items element.
     *
     * @return string
     */
    public function generateImagePart($element)
    {
        $return = '';
        if (!empty($element->main_image)) {
            $return .= '<image:image><image:loc>'.$this->projectUrl.$element->main_image."</image:loc></image:image>\r\n";
        } elseif (!empty($element->image_path)) {
            $return .= '<image:image><image:loc>'.$this->projectUrl.$element->image_path."</image:loc></image:image>\r\n";
        }
        return $return;
    }

    /**
     * Generate image part of the sitemap.
     *
     * @param string $file File name.
     *
     * @return string
     */
    public function generateMainSitemap(string $file = '')
    {
        $return = '';
        $now = new \DateTime();
        if (!empty($file)) {
            $return .= '<sitemap>';
            $return .= '<loc>'.$this->projectUrl.'files/sitemap/'.$file.'</loc>';
            $return .= '<lastmod>'.$now->format('Y-m-d H:i:s').'</lastmod>';
            $return .= '</sitemap>';
        }
        return $return;
    }
}
