<?php
/**
 * Based on orginal 'prosty javascriptowy sposob na zabezpieczenie formow przed automatycznymi robotami gdy nie ma
 * targetowanego ataku' AKA DragonCaptcha by Dragon <lukasz.dragan@econnect4u.pl>
 *
 * @author Stefan <marcin@econnect4u.pl>
 * @date date(2018-02-06)
 * @version 1.1
 */

namespace App\View\Helper;

use Cake\Routing\Router;
use Cake\View\Helper;

/**
 * RecaptchaHelper
 */
class CaptchaHelper extends Helper
{
    /**
     * Additional helpers
     *
     * @var array
     */
    public $helpers = ['Form', 'Html'];

    /**
     * CaptchaOn
     *
     * @var boolean
     */
    protected $captchaIsEnabled = false;

    /**
     * Captcha variables array
     *
     * @var array
     */
    protected $captcha = [];

    /**
     * Form number
     *
     * @var integer
     */
    protected $formNo = 0;

    /**
     * Initialize
     *
     * @param array $config Configuration array.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setViewVars();
        $this->setCaptchaVars();

        if ($this->checkCaptchaVars()) {
            $this->captchaIsEnabled = true;
        }
    }

    /**
     * Set view vars
     *
     * @return void
     */
    protected function setViewVars()
    {
        $this->viewVars = $this->_View->viewVars;
    }

    /**
     * Set captcha vars
     *
     * @return void
     */
    protected function setCaptchaVars()
    {
        $this->captcha = $this->getCaptchaVars();
    }

    /**
     * Get captcha variables
     *
     * @return array Returns captcha variables previously set in \App\Controller\Component\CaptchaComponent
     */
    protected function getCaptchaVars()
    {
        $captchaVars = [];
        if (isset($this->viewVars['captcha'])) {
            $captchaVars = $this->viewVars['captcha'];
        }

        return $captchaVars;
    }

    /**
     * Get captcha variable
     *
     * @param string $name Captcha key name.
     * @return string|null Returns captcha key value or null if not found.
     */
    protected function getCaptchaVar(string $name)
    {
        if (!empty($name) && isset($this->captcha[$name])) {
            return $this->captcha[$name];
        }

        return null;
    }

    /**
     * Check if captcha variables is set
     *
     * @return boolean
     */
    protected function checkCaptchaVars()
    {
        if (!empty($this->captcha)) {
            return true;
        }

        return false;
    }

    /**
     * Form start
     *
     * Starts form using Form helper and modifies it if captcha is turned on
     *
     * @param object|string $context The context for which the form is being defined. Can be an ORM entity, ORM resultset, array of metadata or false/null (to make a model-less form).
     * @param array         $options An array of options and/or HTML attributes.
     * @return string Returns form beginning tag.
     */
    public function formStart(mixed $context = null, array $options = [])
    {
        $html = '';
        if ($this->captchaIsEnabled) {
            $requestedTarget         = $this->request->getRequestTarget();
            $this->captcha['action'] = isset($options['url']) ? Router::reverse($options['url']) : $requestedTarget;

            if (isset($options['url']['_name'])) {
                $this->captcha['action'] = $options['url']['_name'];
            }

            $options['url']   = $requestedTarget.'.html';
            $options['type']  = 'get';
            $options['id']    = $this->getCaptchaVar('form').'f'.$this->formNo;
            $options['class'] = $this->getCaptchaVar('form').(isset($options['class']) ? ' '.$options['class'] : '');

            $this->formNo++;
        }

        $html .= $this->Form->create($context, $options);
        $html .= $this->Form->control('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]);

        return $html;
    }

    /**
     * Form start when FormBuilder is in use
     *
     * @param string $form Form name.
     * @return string Returns form beginning tag and hidden input field if form is active.
     */
    public function replaceStart(string $form)
    {
        $html = '';

        if (isset($this->viewVars['forms'][$form])) {
            $form = $this->viewVars['forms'][$form];
            if ($form->active) {
                $html .= $this->formStart(null, ['url' => ['controller' => 'FormsData', 'action' => 'save'], 'class' => $form->html_class.' formBuilder', 'enctype' => 'multipart/form-data']);
                $html .= $this->Form->control('form_name', ['type' => 'hidden', 'value' => $form->name]);
                $html .= $this->Form->control('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]);
            }
        }

        return $html;
    }

    /**
     * Form end
     * Ends the form and adds script to the bottom scripts
     *
     * @return string
     */
    public function formEnd()
    {
        $html = $this->Form->end();

        if ($this->formNo === 1) {
            $html .= $this->setCaptchaScript();
        }

        return $html;
    }

    /**
     * Get captcha script
     *
     * @return void
     */
    public function setCaptchaScript()
    {
        $html    = '';
        $newLine = " ";    // "\n"

        if ($this->captchaIsEnabled) {
            $html .= '$(document).on(\'submit\', \'.'.$this->captcha['form'].'\', function (e) {'.$newLine;
            $html .= 'if (typeof $(this).attr(\'captcha\') == \'undefined\' || $(this).attr(\'captcha\') === false) {'.$newLine;
            $html .= 'e.preventDefault();'.$newLine;
            $html .= 'var '.$this->captcha['var1'].' = \''.$this->captcha['name'].'\';'.$newLine;
            $html .= 'var '.$this->captcha['var2'].' = \''.$this->captcha['value'].'\';'.$newLine;
            $html .= 'var '.$this->captcha['var3'].' = \''.$this->captcha['action'].'\';'.$newLine;
            $html .= 'var '.$this->captcha['var4'].' = \'post\';'.$newLine;
            $html .= '$("<input/>", {type: "hidden", name: '.$this->captcha['var1'].', value: '.$this->captcha['var2'].'}).appendTo($(this));'.$newLine;
            $html .= '$(this).attr({action: '.$this->captcha['var3'].', method: '.$this->captcha['var4'].', captcha: true});'.$newLine;
            $html .= 'if (!$(this).hasClass(\'formBuilder\')) {'.$newLine;
            $html .= '$(this).submit();'.$newLine;
            $html .= '}'.$newLine;
            $html .= '}'.$newLine;
            $html .= '});'.$newLine;

            $this->Html->scriptBlock($html, ['block' => 'scriptBottom']);
        }
    }
}
