<?php

namespace App\View\Helper;

use Cake\View\Helper;

/**
 * SwitcherHelper
 */
class SwitcherHelper extends Helper
{
    /**
     * Helpers
     *
     * @var array
     */
    public $helpers = ['Form', 'ACL'];

    /**
     * Create switcher if it's available in ACL
     *
     * @param string $name Column name to toggle.
     * @param array  $options Options to pass, mandatory keys: val, data-id.
     * @return string|null Returns switcher widget if enabled in ACL, null otherwise.
     */
    public function create(string $name, array $options = []): ?string
    {
        $url = [
            'controller' => $this->request->getParam('controller'),
            'action'     => 'toggleColumnValue'
        ];

        if (!$this->ACL->hasAccess($url)) {
            return null;
        }

        return $this->Form->switcher($name, $options);
    }
}
