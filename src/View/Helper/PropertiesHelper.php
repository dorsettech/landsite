<?php

namespace App\View\Helper;

use App\Model\Enum\PropertyPurpose;
use Cake\View\Helper;
use App\Model\Entity\Property;
use Cake\Core\Configure;
use App\Model\Enum\PropertyPriceSalePer;
use App\Model\Enum\PropertyPriceRentPer;

/**
 * PropertiesHelper
 */
class PropertiesHelper extends Helper
{
    public $helpers = ['Number'];

    /**
     * Price displayer - display price base on purpose
     *
     * @param Property $property Property object.
     * @return string
     */
    public function price(Property $property): ?string
    {
        switch ($property->purpose) {
            case PropertyPurpose::BOTH:
                return
                    '<div class="sale"><span>For sale</span><br>' . $this->formatPrice($property->price_sale, $property->price_sale_per, ['type' => PropertyPurpose::SALE]) . '<span class="qualifier">'.$property->sale_qualifier_name.'</span></div>
                    <div class="rent"><span>To rent</span><br>' . $this->formatPrice($property->price_rent, $property->price_rent_per, ['type' => PropertyPurpose::RENT]) . '<span class="qualifier">'.$property->rent_qualifier_name.'</span></div>';
            case PropertyPurpose::SALE:
                return '<div class="sale"><span>For sale</span><br>' . $this->formatPrice($property->price_sale, $property->price_sale_per, ['type' => $property->purpose]) . '<span class="qualifier">'.$property->sale_qualifier_name.'</span></div>';
            case PropertyPurpose::RENT:
                return '<div class="rent"><span>To rent</span><br>' . $this->formatPrice($property->price_rent, $property->price_rent_per, ['type' => $property->purpose]) . '<span class="qualifier">'.$property->rent_qualifier_name.'</span></div>';
        }
    }

    /**
     * Price displayer - display price in email template base on purpose
     *
     * @param Property $property Property object.
     * @return string
     */
    public function emailPrice(Property $property): ?string
    {
        switch ($property->purpose) {
            case PropertyPurpose::BOTH:
                return
                    '<td align="center" bgcolor="#123953" class="mobile-100" width="50%" style="padding: 5px 0 5px 0; color: #fff; font-family: \'Work Sans\', sans-serif; font-size: 14px;">
                        FOR SALE<br><span style="font-size: 20px;">' . $this->formatPrice($property->price_sale, $property->price_sale_per, ['type' => PropertyPurpose::SALE]) . '</span>
                    </td>
                    <td align="center" bgcolor="#487fbd" class="mobile-100" width="50%" style="padding: 5px 0 5px 0; color: #fff; font-family: \'Work Sans\', sans-serif; font-size: 14px;">
                        TO RENT<br><span style="font-size: 20px;">' . $this->formatPrice($property->price_rent, $property->price_rent_per, ['type' => PropertyPurpose::RENT]) . '</span>
                    </td>';
            case PropertyPurpose::SALE:
                return '<td align="center" bgcolor="#123953" class="mobile-100" width="50%" style="padding: 5px 0 5px 0; color: #fff; font-family: \'Work Sans\', sans-serif; font-size: 14px;">
                            FOR SALE<br><span style="font-size: 20px;">' . $this->formatPrice($property->price_sale, $property->price_sale_per, ['type' => $property->purpose]) . '</span>
                        </td>';
            case PropertyPurpose::RENT:
                return '<td align="center" bgcolor="#487fbd" class="mobile-100" width="50%" style="padding: 5px 0 5px 0; color: #fff; font-family: \'Work Sans\', sans-serif; font-size: 14px;">
                            TO RENT<br><span style="font-size: 20px;">' . $this->formatPrice($property->price_rent, $property->price_rent_per, ['type' => $property->purpose]) . '</span>
                        </td>';
        }
    }

    /**
     * Format value as a price
     *
     * Options:
     * <ul>
     *  <li>currency boolean|string returns value as a price with default currency if it's boolean or passes string
     *  as a currency (i.e. GBP, PLN)</li>
     * </ul>
     *
     * @param float|null $value     Price value.
     * @param string     $price_per Price value.
     * @param array      $options   Options array.
     *
     * @return mixed
     */
    public function formatPrice($value, string $price_per = '', array $options = [])
    {
        $type  = !empty($options['type']) ? $options['type'] : PropertyPurpose::RENT;
        $class = ($type === PropertyPurpose::SALE) ? PropertyPriceSalePer::class : PropertyPriceRentPer::class;

        $suffix = '';
        if (!empty($price_per)) {
            $suffix = ' ' . $class::getNamesList()[$price_per];
        }

        if ($price_per === $class::PRICE_ON_APPLICATION) {
            return $suffix;
        }
        if (isset($options['clean']) && $options['clean'] == true) {
            return $this->Number->format($value, ['places' => 0]) . $suffix;
        }

        $viewVars = $this->_View->viewVars;
        $currency = $viewVars['_currency'] ?? 'GBP';

        return $this->Number->currency($value, $currency, ['places' => 0, 'precision' => 0, 'useIntlCode' => false, 'locale' => Configure::read('App.defaultLocale')]) . $suffix;
    }
}
