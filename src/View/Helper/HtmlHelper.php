<?php

namespace App\View\Helper;

use App\Library\ACL;
use Bootstrap\View\Helper\HtmlHelper as BootstrapHtmlHelper;
use Cake\Routing\Router;

/**
 * HtmlHelper
 */
class HtmlHelper extends BootstrapHtmlHelper
{
    /**
     * Creates an HTML link.
     *
     * If $url starts with "http://" this is treated as an external link. Else,
     * it is treated as a path to controller/action and parsed with the
     * UrlHelper::build() method.
     *
     * If the $url is empty, $title is used instead.
     *
     * ### Options
     *
     * - `escape` Set to false to disable escaping of title and attributes.
     * - `escapeTitle` Set to false to disable escaping of title. Takes precedence
     *   over value of `escape`)
     * - `confirm` JavaScript confirmation message.
     *
     * @param string|array      $title   The content to be wrapped by `<a>` tags.
     *          Can be an array if $url is null. If $url is null, $title will be used as both the URL and title.
     * @param string|array|null $url     Cake-relative URL or array of URL parameters, or external URL (starts with http://).
     * @param array             $options Array of options and HTML attributes.
     * @return string An `<a />` element.
     * @link https://book.cakephp.org/3.0/en/views/helpers/html.html#creating-links
     */
    public function link($title, $url = null, array $options = [])
    {
        $escapeTitle = true;
        $aclCheck    = true;

        if (is_string($url) && strpos($url, '#') === 0) {
            $aclCheck = false;
        } elseif ($url !== null) {
            $url = $this->Url->build($url, $options);
            unset($options['fullBase']);
        } else {
            $url         = $this->Url->build($title);
            $title       = htmlspecialchars_decode($url, ENT_QUOTES);
            $title       = h(urldecode($title));
            $escapeTitle = false;
        }

        if (isset($options['escapeTitle'])) {
            $escapeTitle = $options['escapeTitle'];
            unset($options['escapeTitle']);
        } elseif (isset($options['escape'])) {
            $escapeTitle = $options['escape'];
        }

        if ($escapeTitle === true) {
            $title = h($title);
        } elseif (is_string($escapeTitle)) {
            $title = htmlentities($title, ENT_QUOTES, $escapeTitle);
        }

        $confirmMessage = null;
        if (isset($options['confirm'])) {
            $confirmMessage = $options['confirm'];
            unset($options['confirm']);
        }
        if ($confirmMessage) {
            $options['onclick'] = $this->_confirm($confirmMessage, 'return true;', 'return false;', $options);
        }

        $templater = $this->templater();

        $View      = $this->getView();
        $aclHelper = new ACLHelper($View);

        $request = Router::getRequest();

        if (empty($request) ||  (!empty($request) && in_array($request->getParam('prefix'), ACL::PREFIX_ALLOWED))) {
            $aclCheck = false;
        }

        if ($aclCheck && !$aclHelper->hasAccess($url)) {
            if (!empty($options['class']) && preg_match('#btn|btn-#', $options['class'])) {
                return '';
            }

            $options['class'] = (!empty($options['class']) ? $options['class'] . ' disabled' : 'disabled');

            return $templater->format('tag', [
                    'tag'     => 'span',
                    'attrs'   => $templater->formatAttributes($options),
                    'content' => $title
            ]);
        }

        return $templater->format('link', [
                'url'     => $url,
                'attrs'   => $templater->formatAttributes($options),
                'content' => $title
        ]);
    }
}
