<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\View\Helper;

use Cake\I18n\Time;
use Cake\View\Helper;

/**
 * Class TransformHelper
 */
class TransformHelper extends Helper
{
    const DATE_FORMAT = 'YYYY-MM-dd';
    const DATE_TIME_FORMAT = 'YYYY-MM-dd HH:mm:ss';

    /**
     * Date formater
     *
     * @param string|null $time   Time value.
     * @param string|null $option Options.
     *
     * @return string|null
     */
    public function date($time, $option = null)
    {
        if (empty($time)) {
            return null;
        }

        $time = new Time($time);

        switch ($option) {
            case 'date':
                return $time->i18nFormat(self::DATE_FORMAT);
                break;
            case 'dateuk':
                return $time->i18nFormat('d/MM/Y');
                break;
            case 'datetimeuk':
                return $time->i18nFormat('d/MM/Y HH:mm:ss');
                break;
            case 'time':
                return $time->i18nFormat('HH:mm');
            case 'unix':
                return $time->toUnixString();
                break;
            case 'human':
                return $time->nice();
                break;
            default:
                return $time->i18nFormat(self::DATE_TIME_FORMAT);
                break;
        }
    }

    /**
     * Do the mapping for select fields in layouts
     *
     * @param array|null  $array           Array to parse.
     * @param string|null $valueFieldName  Field name in object/array to use as an option value parameter.
     * @param string|null $optionFieldName Field name in object/array to use as an option name.
     *
     * @return array
     */
    public function selectMapping($array, $valueFieldName, $optionFieldName)
    {
        $return = [];
        foreach ($array as $element) {
            if (is_object($element)) {
                if (isset($element->$valueFieldName) && !empty($element->$valueFieldName)) {
                    $return[$element->$valueFieldName] = $element->$optionFieldName;
                }
            } else if (is_array($element)) {
                if (isset($element[$valueFieldName]) && !empty($element[$valueFieldName])) {
                    $return[$element[$valueFieldName]] = $element[$optionFieldName];
                }
            }
        }

        return $return;
    }

    /**
     * Limit words to specific amount for use in tables, boxes etc.
     *
     * @param string|null $string  String to be parsed.
     * @param array       $options Additional options for wordsLimit: separator, wordLimit, ending.
     *
     * @return string
     */
    public function wordsLimit($string = '', array $options = [])
    {
        if (!isset($options['separator'])) {
            $options['separator'] = ' ';
        }
        if (!isset($options['wordLimit'])) {
            $options['wordLimit'] = 10;
        }
        if (!isset($options['ending'])) {
            $options['ending'] = '&hellip;';
        }

        $words = explode($options['separator'], $string);
        $countWords = count($words);

        $return = implode(" ", array_splice($words, 0, $options['wordLimit']));
        if ($countWords > $options['wordLimit']) {
            $return .= $options['ending'];
        }

        return $return;
    }

    /**
     * Display size
     *
     * @param integer|null $bytes     Bytes.
     * @param integer|null $precision Precision number.
     *
     * @return string
     */
    public function displaySize($bytes = 0, $precision = 2)
    {
        if (is_numeric($bytes)) {
            $units = array('B', 'KB', 'MB');

            $bytes = max($bytes, 0);
            $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
            $pow = min($pow, count($units) - 1);
            $bytes /= pow(1024, $pow);

            $bytes = round($bytes, $precision) . ' ' . $units[$pow];
        }

        return $bytes;
    }
}
