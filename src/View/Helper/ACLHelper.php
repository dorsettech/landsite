<?php
/**
 * @author Jakub Gałyga <jakub@ninjacode.pl>
 * @author Stefan <marcin@econnect4u.pl>
 * @date date(2019-01-07)
 * @version 2.0
 */

namespace App\View\Helper;

use App\Library\ACL;
use Cake\Routing\Router;
use Cake\Utility\Inflector;
use Cake\View\Helper;
use GuzzleHttp\Psr7\ServerRequest;
use ReflectionClass;
use ReflectionException;
/**
 * Class ACL Helper
 *
 * @package App\View\Helper
 */
class ACLHelper extends Helper
{
    /**
     * Controler description method
     *
     * @param string $namespace Namespace.
     * @param string $name      Controller name.
     * @return string
     * @throws ReflectionException Reflection exception.
     */
    public function controllerDescription(string $namespace, string $name): string
    {
        $className = $namespace . '\\' . $name . 'Controller';
        $class     = new ReflectionClass($className);
        $pattern   = "/@description (.*)\n/";
        preg_match_all($pattern, $class->getDocComment(), $matches, PREG_PATTERN_ORDER);
        if (isset($matches[1][0])) {
            return $matches[1][0];
        }

        return Inflector::humanize(Inflector::underscore($name)) . ' ' . __('Module');
    }

    /**
     * Action description method
     *
     * @param string $namespace  Namespace.
     * @param string $controller Controller name.
     * @param string $action     Action name.
     * @return string
     * @throws ReflectionException Reflection exception.
     */
    public function actionDescription(string $namespace, string $controller, string $action): string
    {
        $className = $namespace . '\\' . $controller . 'Controller';
        $class     = new ReflectionClass($className);
        $action    = $class->getMethod($action);

        $pattern = "/@description (.*)\n/";
        preg_match_all($pattern, $action->getDocComment(), $matches, PREG_PATTERN_ORDER);
        if (isset($matches[1][0])) {
            return $matches[1][0];
        }

        $singularName = Inflector::singularize(Inflector::humanize(Inflector::underscore($controller)));

        return $this->getCommonActionDescription($singularName, $action->name);
    }

    /**
     * Get common action description
     *
     * @param string $singularName Controller singular name.
     * @param string $action       Action name.
     * @return string Returns automatically generated action name.
     */
    protected function getCommonActionDescription($singularName, $action): string
    {
        switch ($action) {
            case 'index':
                return __('{0} List', Inflector::pluralize($singularName));
            case 'add':
                return __('Add {0}', $singularName);
            case 'edit':
                return __('Edit {0}', $singularName);
            case 'delete':
                return __('Delete {0}', $singularName);
        }

        return Inflector::humanize(Inflector::underscore($action));
    }

    /**
     * Action related method
     *
     * @param string $namespace  Namespace.
     * @param string $controller Controller name.
     * @param string $action     Action name.
     * @return array
     * @throws ReflectionException Reflection exception.
     */
    public function actionRelated(string $namespace, string $controller, string $action): ?string
    {
        $className = $namespace . '\\' . $controller . 'Controller';
        $class     = new ReflectionClass($className);
        $action    = $class->getMethod($action);
        $related   = [];
        $pattern   = "/@related (.*)\n/";
        preg_match_all($pattern, $action->getDocComment(), $matches, PREG_PATTERN_ORDER);

        if (isset($matches[1][0]) && !empty($matches[1][0])) {
            $related = explode(',', $matches[1][0]);
        }

        foreach ($related as &$value) {
            $value = trim($value);
        }

        if (!empty($related)) {
            return base64_encode(json_encode($related));
        }

        return null;
    }

    /**
     * Has access
     *
     * @param mixed $url URL as an array.
     * @return boolean
     */
    public function hasAccess($url): bool
    {
        if (empty($url) || (is_string($url) && strpos($url, 'http') === 0)) {
            return true;
        }

        if (is_string($url)) {
            $url = $this->parseUri($url);
        }

        $viewVars = $this->getView()->viewVars;

        if (isset($viewVars['_user'], $viewVars['_user']['is_root']) && $viewVars['_user']['is_root'] && ACL::ROOT_ACCESS) {
            return true;
        }

        if (isset($viewVars['_permissions']) && is_array($url)) {
            $permissions = $viewVars['_permissions'];

            if (empty($url['controller'])) {
                $url['controller'] = Router::getRequest()->getParam('controller');
            }
            if (empty($url['action'])) {
                $url['action'] = 'index';
            }
            if (isset($permissions[$url['controller']][$url['action']]) && $permissions[$url['controller']][$url['action']]) {
                return true;
            }
        }

        return ACL::HAS_ACCESS;
    }

    /**
     * Parse URI
     *
     * @param string $uri URI to parse.
     * @return array|string Returns parsed URI as an array.
     */
    public function parseUri(string $uri)
    {
        if (!empty($uri)) {
            $serverRequest = new ServerRequest('GET', $uri);

            return Router::parseRequest($serverRequest);
        }

        return $uri;
    }
}
