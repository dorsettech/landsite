<?php

namespace App\View\Helper;

use Cake\Utility\Inflector;
use Cake\View\Helper;


/**
 * Class InflectorHelper
 *
 * @author Jakub Gałyga <jakub@ninjacode.pl>
 * @date date(2017-XX-XX)
 * @version 1.0
 *
 * @package App\View\Helper
 */
class InflectorHelper extends Helper
{

    /**
     * __call method
     *
     * @param string $method
     * @param array $args
     * @return mixed
     */
    public function __call($method, $args) : string
    {

        return Inflector::{$method}($args[0]);
    }


}
