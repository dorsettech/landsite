<?php
/**
 * @author  Stefan
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @author  Marcin Nierobiś <marcin.nierobis@econnect4u.pl>
 * @date (2019-04-05)
 * @version 1.0
 */

namespace App\View\Helper;

use Cake\View\Helper;
use App\Model\Entity\Group;

/**
 * PageHelper
 */
class PageHelper extends Helper
{
    /**
     * Required helpers
     *
     * @var array
     */
    public $helpers = ['Html', 'Form', 'Acl', 'Urlname'];

    /**
     * Menu urlname
     *
     * @var string
     */
    private $menuUrlname;

    /**
     * Available layouts with details
     *
     * @var array
     */
    private $layouts = [];

    /**
     * Additional access
     *
     * @var boolean
     */
    private $is_root = false;

    /**
     * Statics variables set in database
     *
     * @var array
     */
    private $statics = [];

    /**
     * Initialize
     *
     * @param array $config Additional config.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        if (isset($this->_View->viewVars['menu'])) {
            $this->menuUrlname = $this->_View->viewVars['menu']->urlname;
        }
        if (isset($this->_View->viewVars['_user']) && is_bool($this->_View->viewVars['_user']->is_root)) {
            $this->is_root = $this->_View->viewVars['_user']->is_root;
        }
        if (isset($this->_View->viewVars['layouts'])) {
            $this->layouts = $this->_View->viewVars['layouts'];
        }
        if (isset($this->_View->viewVars['statics'])) {
            $this->statics = $this->_View->viewVars['statics'];
        }
    }

    /**
     * Get list
     *
     * @param object|array $page    Page.
     * @param array        $options Additional options.
     *
     * @return null|string
     */
    public function getList($page, array $options = [])
    {
        if (empty($page)) {
            return null;
        }

        $return = $this->listCreate($options);
        foreach ($page as $element) {
            $return .= $this->getItem($element);
        }
        $return .= $this->listEnd();

        return $return;
    }

    /**
     * List create
     *
     * @param array $options Additional options.
     *
     * @return string
     */
    public function listCreate(array $options = [])
    {
        $class = 'dd-list';
        if (isset($options['classList'])) {
            $class .= ' '.$options['classList'];
        }

        return '<ol class="'.$class.'">';
    }

    /**
     * List end
     *
     * @return string
     */
    public function listEnd()
    {
        return '</ol>';
    }

    /**
     * Get page actions
     *
     * @return string
     */
    public function getPageActions()
    {
        $html = '<div class="row mb-3">';
        $html .= '<div class="col-12 col-sm-6">';
        $html .= $this->Form->create(null, ['url' => ['controller' => 'Pages', 'action' => 'updatePage'], 'class' => 'page-update-form']);
        $html .= $this->Form->control('page', ['type' => 'hidden', 'id' => false, 'class' => 'menu-page-output']);
        $html .= $this->Form->button(__('Update menu structure'), ['btype' => 'primary', 'class' => 'disabled pages-update']);
        $html .= $this->Form->end();
        $html .= '</div>';
        $html .= '<div class="col-12 col-sm-6 text-right">';
        $html .= '<div class="page-details">';
        $html .= '<div class="btn-group">';
        $html .= '<span class="btn btn-default btn-details-toggle" data-show="'.__('Show details').'" data-hide="'.__('Hide details').'">';
        $html .= '<i class="fas fa-eye"></i> ';
        $html .= '<span>'.__('Show details').'</span>';
        $html .= '</span>';
        $html .= $this->Html->link(__('Add Page'), ['controller' => 'Pages', 'action' => 'add', $this->menuUrlname], ['class' => 'btn btn-primary']);
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }

    /**
     * Get item
     *
     * @param object|null $element Object.
     *
     * @return string
     */
    public function getItem($element)
    {
        $class  = 'dd-item';
        $html   = [];
        $html[] = '<li class="'.$class.'" data-id="'.$element->id.'">';
        $html[] = '<div class="dd-handle-custom">'.__('Move').'</div>';
        $html[] = '<div class="dd-content clearfix">';
        $html[] = $this->getActions($element);
        $html[] = '<span class="page-name">';
        $html[] = ($this->is_root ? '<small>#'.$element->id.'</small> ' : '');
        $html[] = $this->Html->link(h($element->name), ['controller' => 'Pages', 'action' => 'editcontents', $element->id], ['class' => (!$element->active ? 'text-muted' : '')]);
        $html[] = '</span>';
        $html[] = $this->getContents($element);
        $html[] = '</div>';

        if (!empty($element['children'])) {
            $html[] = $this->getList($element['children']);
        }
        $html[] = '</li>';

        return join('', $html);
    }

    /**
     * Get layout name
     *
     * @param string|null $layout Layout name.
     *
     * @return mixed
     */
    public function getLayoutName($layout)
    {
        return (isset($this->layouts[$layout]) && isset($this->layouts[$layout]['name'])) ? $this->layouts[$layout]['name'] : $layout;
    }

    /**
     * Get actions list
     *
     * @param object|null $element Element object.
     *
     * @return string
     */
    public function getActions($element)
    {
        return '<span class="page-actions btn-group">'
            .$this->Html->link('<i class="fa fa-external-link-alt"></i>', $this->Urlname->generate($element->urlname), ['class' => 'btn btn-default preview', 'escape' => false, 'target' => '_blank', 'title' => __('Preview page'), 'data-toggle' => 'tooltip', 'data-placement' => 'top'])
            .$this->Html->link('<i class="fa fa-clone"></i>', ['controller' => 'Pages', 'action' => 'clonePage', $element->id], ['class' => 'btn btn-default', 'escape' => false, 'title' => __('Clone page'), 'data-toggle' => 'tooltip', 'data-placement' => 'top'])
            .$this->Html->link('<i class="fa fa-edit"></i>', ['controller' => 'Pages', 'action' => 'edit-contents', $element->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Edit page'), 'data-toggle' => 'tooltip', 'data-placement' => 'top'])
            .(!$element->protected ? $this->Form->postLink('<i class="fa fa-times"></i>', ['controller' => 'Pages', 'action' => 'delete', $element->id], ['class' => 'btn btn-danger', 'confirm' => __("Are you sure you want to delete page #{0}?", $element->id), 'escape' => false, 'title' => __('Delete page'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) : '')
            .'</span>';
    }

    /**
     * Get contents
     *
     * @param object|null $element Element object.
     *
     * @return string
     */
    public function getContents($element)
    {
        $return = '';
        $return .= $this->getPageDetails($element);
        if (!empty($element->redirection)) {
            $return .= '<i class="fas '.($element->target_blank ? 'fa-external-link-alt' : 'long-arrow-alt-right').'"></i> '.h($element->redirection);
        } elseif (!empty($element->contents) && is_array($element->contents)) {
            foreach ($element->contents as $content) {
                $return .= $this->getContent($content);
            }
        } else {
            if (isset($this->_View->viewVars['contents'][$element->type])) {
                $contentName = h($this->_View->viewVars['contents'][$element->type]);
                $return      .= '<span class="content-type" data-toggle="tooltip" data-placement="top" title="'.$contentName.'"><i class="fa fa-cube"></i> '.$contentName.'<span>';
            }
        }

        return '<span class="page-contents d-none">'.$return.'</span>';
    }

    /**
     * Get content
     *
     * @param object|null $content Content object.
     *
     * @return string
     */
    public function getContent($content)
    {
        $contentName = (!empty($content->title)) ? h($content->title) : '#'.$content->id;

        if ($content->content_name == 'main') {
            $iconClass = 'info';
            $tooltip   = __('Content type: main');
        } elseif (empty($content->content_name)) {
            $iconClass = 'warning';
            $tooltip   = __('Content type: none');
        } else {
            $iconClass = 'default';
            $tooltip   = __('Content type: {0}', $content->urlname);
        }

        return '<span class="page-content">'
            .'<span class="content-type" data-toggle="tooltip" data-placement="top" title="'.$tooltip.'">'
            .'<i class="fas fa-file-alt text-'.$iconClass.'"></i> '.h($content->content_name)
            .'</span> '
            .'<span class="content-name">'.$this->Html->link($contentName, ['controller' => 'Pages', 'action' => 'editcontents', $content->page_id, '#' => 'tab-'.$content->content_name.'-content'], ['escape' => false]).'</span>'
            .'</span>';
    }

    /**
     * Statics
     *
     * @param string|null $varName Var name.
     *
     * @return mixed|null
     */
    public function statics($varName = '')
    {
        if (isset($this->statics[$varName])) {
            return $this->statics[$varName];
        }

        return null;
    }

    /**
     * Get page details
     *
     * @param object|null $page Page element.
     * @return string
     */
    public function getPageDetails($page)
    {
        $html = '<span class="page-info d-block mb-2">';
        if (!empty($page)) {
            if (!empty($page->urlname)) {
                $html .= '<span class="badge badge-lignt mr-1">URL: '.$page->urlname.'</span>';
            }
            $html .= '<span class="badge badge-info mr-1">Layout: '.$page->layout.'</span>';
            if ($page->active === true) {
                $html .= '<span class="badge badge-success mr-1">Active</span>';
            } else {
                $html .= '<span class="badge badge-danger ">Inactive</span>';
            }
        }
        $html .= '</span>';
        return $html;
    }

    /**
     * Article posted by
     *
     * @param object $article Article object.
     * @param array  $options Additional options.
     * @return string
     */
    public function articlePostedBy($article, array $options = [])
    {
        $return = '';
        if ($article->has('user')) {
            if ($article->user->group_id !== Group::MEMBERS) {
                $return = __('The Landsite');
            } elseif ($article->user->has('service')) {
                if (!empty($options) && $options['link'] === true) {
                    $return = $this->Html->link($article->user->service->company, ['_name' => 'professional-services-individual', $article->user->service->urlname]);
                } else {
                    $return = $article->user->service->company;
                }
            } else {
                $return = $article->user->full_name;
            }
        }
        return $return;
    }

    /**
     * Article posted by used in email template
     *
     * @param object $article Article object.
     * @param array  $options Additional options.
     * @return string
     */
    public function emailArticlePostedBy($article, array $options = [])
    {
        $style  = "color: #e52338; font-family: 'Raleway', sans-serif; font-size: 12px; text-decoration: underline;";
        $posted = 'Posted by ';
        $iStart = '<i style="'.$style.'">'.$posted;
        $iEnd   = '</i>';
        $return = '';
        if ($article->has('user')) {
            if (FALSE && $article->user->group_id !== Group::MEMBERS) {
                $return = $iStart.__('The Landsite').$iEnd;
            } elseif ($article->user->has('service')) {
                if (!empty($options) && $options['link'] === true) {
                    $return = $this->Html->link('<i>'.$posted.$article->user->service->company.$iEnd, ['_name' => 'professional-services-individual', $article->user->service->urlname], ['style' => $style, 'escape' => false]);
                } else {
                    $return = $iStart.$article->user->service->company.$iEnd;
                }
            } else {
                $return = $iStart.$article->user->full_name.$iEnd;
            }
        }
        return $return;
    }
}
