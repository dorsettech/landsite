<?php

/**
 * @author  Zordon
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\View\Helper;

use Cake\View\Helper;

/**
 * CakePHP FrontendHelper
 */
class ImagesHelper extends Helper
{
    /**
     * File object
     *
     * @var null
     */
    public $file = null;

    /**
     * Options
     *
     * @var array
     */
    public $options = [];

    /**
     * Flag resize
     *
     * @var null
     */
    public $resize = null;

    /**
     * Suffix
     *
     * @var string
     */
    public $suffix = '';

    /**
     * Flag overwrite
     *
     * @var bool
     */
    public $overwrite = false;

    /**
     * Dir location
     *
     * @var string
     */
    public $dir = 'files/thumbs/';

    /**
     * Placeholder image path
     *
     * @var string
     */
    public $placeholder = 'img/placeholder.png';

    /**
     * Get name without extension
     *
     * @return string
     */
    protected function _getNameWithoutExtension()
    {
        return pathinfo($this->file)['filename'];
    }

    /**
     * Get file extension
     *
     * @return string
     */
    protected function _getFileExtension()
    {
        return pathinfo($this->file)['extension'];
    }

    /**
     * Get thumb path
     *
     * @param integer|null $width  Width.
     * @param integer|null $height Height.
     *
     * @return string
     */
    public function getThumbPath($width = null, $height = null)
    {
        return $this->dir . $this->_getNameWithoutExtension() . (!empty($width) ? '_w' . $width : '') . (!empty($height) ? '_h' . $height : '') . '.' . $this->_getFileExtension();
    }

    /**
     * Get thumb name
     *
     * @param integer|null $width  Width.
     * @param integer|null $height Height.
     *
     * @return string
     */
    public function getThumbName($width = null, $height = null)
    {
        return $this->_getNameWithoutExtension() . (!empty($width) ? '_w' . $width : '') . (!empty($height) ? '_h' . $height : '') . '.' . $this->_getFileExtension();
    }

    /**
     * Get thumbs regenerate flag
     *
     * @return boolean Returns true if thumb file has older modification time than source file
     */
    public function getThumbRegenerate()
    {
        if (file_exists($this->thumb_path) && file_exists($this->file)) {
            return filemtime($this->file) > filemtime($this->thumb_path);
        }

        return false;
    }

    /**
     * Set options
     *
     * @param string|null   $path   File path.
     * @param integer|array $width  Resize image width or array with options if no more parameters is given.
     * @param integer|null  $height Resize image height.
     * @param string|array  $type   Resize option, also array with options can be passed, or array with options can be passed as a second parameter.
     *
     * @return void
     */
    private function setOptions($path, $width, $height, $type)
    {
        $this->file = $path;
        $this->dir = isset($this->options['dir']) ? $this->options['dir'] : $this->dir;
        $this->width = $width;
        $this->height = $height;
        $this->resize = $type;
        $this->suffix = (!empty($width) ? '_w' . $width : '') . (!empty($height) ? '_h' . $height : '');
        $this->thumb_name = $this->getThumbName($this->width, $this->height);
        $this->thumb_path = $this->getThumbPath($this->width, $this->height);
        $this->thumb_regenerate = $this->getThumbRegenerate();
    }

    /**
     * Scale image
     *
     * @param string|null  $path   Path to image.
     * @param integer|null $width  Width.
     * @param integer|null $height Height.
     * @param string|null  $type   Null|ratio|ratio_crop|ratio_fill.
     *
     * @return string|null
     */
    public function scale($path, $width, $height, $type = 'ratio_crop')
    {
        if (!file_exists($path)) {
            $path = $this->placeholder;
        }

        if (!file_exists($path)) {
            return null;
        }

        $this->setOptions($path, $width, $height, $type);

        if (file_exists($this->thumb_path) && !$this->thumb_regenerate && !$this->overwrite) {
            $thumbMTime = filemtime($this->thumb_path);
            return $this->thumb_path . '?' . $thumbMTime;
        }

        return $this->createImage();
    }

    /**
     * Create image based on given parameters/options
     *
     * @return string|null
     */
    public function createImage()
    {
        if (!file_exists($this->file)) {
            return null;
        }

        $tFile = new \upload($this->file);
        if ($tFile->file_is_image) {
            $tFile->image_resize = true;
            if ($this->resize == 'ratio') {
                $tFile->image_ratio = true;
                $tFile->image_ratio_no_zoom_in = true;
            } else if ($this->resize == 'ratio_crop') {
                $tFile->image_ratio_crop = true;
            } else if ($this->resize == 'ratio_fill') {
                $tFile->image_ratio_fill = true;
            }

            if ($this->thumb_regenerate) {
                $this->overwrite = true;
            }

            if ($this->overwrite) {
                $tFile->file_overwrite = $this->overwrite;
            }

            $tFile->file_name_body_add = $this->suffix;
            if (!empty($this->width) && !empty($this->height)) {
                $tFile->image_x = $this->width;
                $tFile->image_y = $this->height;
            }
            if (!empty($this->height) && empty($this->width)) {
                $tFile->image_ratio_x = true;
                $tFile->image_y = $this->height;
            }
            if (!empty($this->width) && empty($this->height)) {
                $tFile->image_ratio_y = true;
                $tFile->image_x = $this->width;
            }
            if (!file_exists($this->thumb_path) || $this->overwrite) {
                $tFile->process($this->dir);
            }
            if (file_exists($this->thumb_path)) {
                return $this->thumb_path;
            }

            if (!$tFile->processed) {
                return null;
            }
        }

        return null;
    }

    /**
     * Alt
     *
     * @param string|null $path File path.
     *
     * @return string
     */
    public function alt($path = null)
    {
        if (!empty($path)) {
            $array = explode('.', $path);
            $fileExtension = '.' . end($array);
            return basename($path, $fileExtension);
        }

        return '';
    }
}
