<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\View\Helper;

use Cake\ORM\TableRegistry;
use Cake\View\Helper;

/**
 * Class UrlnameHelper
 */
class UrlnameHelper extends Helper
{
    /**
     * Generate urlname
     *
     * @param string|null $urlname Urlname.
     * @param boolean     $clean   Option to remove first "/".
     *
     * @return string
     */
    public static function generate($urlname = '', bool $clean = false)
    {
        $path = '';
        $pageList = self::getIPs($urlname);
        if ($pageList !== null && !empty($pageList)) {
            foreach ($pageList as $ip) {
                $page = TableRegistry::getTableLocator()->get('Pages')
                                     ->find('all')
                                     ->where(['Pages.id' => $ip])
                                     ->first();
                if ($page !== null) {
                    $path .= '/' . $page->urlname;
                }
            }
        }

        if ($clean === true) {
            $path = ltrim($path, $path[0]);
        }

        return $path;
    }

    /**
     * Get IPS
     *
     * @param string|null $urlname Urlname.
     *
     * @return array|null
     */
    public static function getIPs($urlname = '')
    {
        $pageList = null;
        $page = TableRegistry::getTableLocator()->get('Pages')
                             ->find('all')
                             ->where(['Pages.urlname' => $urlname])
                             ->first();

        if ($page !== null) {
            $pageList = explode('.', $page->ip);
        }

        return $pageList;
    }
}
