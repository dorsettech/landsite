<?php

namespace App\View\Widget;

use Cake\View\Form\ContextInterface;
use Cake\View\Widget\WidgetInterface;

/**
 * SwitcherWidget
 */
class SwitcherWidget implements WidgetInterface
{
    /**
     * Templates
     *
     * @var object
     */
    protected $_templates;

    /**
     * Construct
     *
     * @param object $templates Templates object.
     */
    public function __construct($templates)
    {
        $this->_templates = $templates;
    }

    /**
     * Render a switcher element.
     *
     * Data supports the following keys:
     *
     * - `name` - The name of the input.
     * - `value` - The value attribute. Defaults to '1'.
     * - `val` - The current value. If it matches `value` the checkbox will be checked.
     *   You can also use the 'checked' attribute to make the checkbox checked.
     *
     * Any other attributes passed in will be treated as HTML attributes.
     *
     * @param array            $data    The data to create a checkbox with.
     * @param ContextInterface $context The current form context.
     * @return string Generated HTML string.
     */
    public function render(array $data, ContextInterface $context)
    {
        $data += [
            'name'    => '',
            'value'   => 1,
            'val'     => null,
            'data-id' => '',
            'id'      => 'switcher-box-' . $data['name'] . '-' . $data['data-id']
        ];
        if ($this->isChecked($data)) {
            $data['checked'] = true;
        }
        unset($data['val']);

        $switcher      = $this->_templates->format('switcher', [
            'name'  => $data['name'],
            'value' => $data['value'],
            'attrs' => $this->_templates->formatAttributes($data, ['name', 'value'])
        ]);
        $switcherLabel = $this->_templates->format('switcherLabel', [
            'attrs' => $this->_templates->formatAttributes(['for' => $data['id']])
        ]);

        return $this->_templates->format('switcherContainer', [
                'switcher'      => $switcher,
                'switcherLabel' => $switcherLabel
        ]);
    }

    /**
     * Returns a list of fields that need to be secured for
     * this widget. Fields are in the form of Model[field][suffix]
     *
     * @param array $data The data to render.
     * @return array Array of fields to secure.
     */
    public function secureFields(array $data)
    {
        return [$data['name'], $data['data-id']];
    }

    /**
     * Check whether or not the checkbox should be checked.
     *
     * @param array $data Data to look at and determine checked state.
     * @return boolean
     */
    protected function isChecked(array $data)
    {
        if (array_key_exists('checked', $data)) {
            return (bool) $data['checked'];
        }

        return (string) $data['val'] === (string) $data['value'];
    }
}
