<?php

namespace App;

use App\Model\Enum;

/**
 * Denotes Responses Types.
 *
 * Main purpose - avoiding use literals in sources
 *
 * @package App
 * @author Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
abstract class ResponseType extends Enum
{

    CONST
        JSON = 'json',
        XML = 'xml',
        AJAX = 'ajax',
        NONE = '';

}
