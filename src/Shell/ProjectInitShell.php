<?php
/**
 * @author  Stefan <m.czerwik@econnect4u.pl>
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-08)
 * @version 1.0
 */

namespace App\Shell;

use Cake\Console\Shell;

/**
 * ProjectInitShell
 */
class ProjectInitShell extends Shell
{
    /**
     * Migrations
     *
     * @var null
     */
    public $migrations = null;

    /**
     * Main
     *
     * @return void
     */
    public function main()
    {
        $this->migrations = new \Migrations\Migrations();

        $this->clear();
        $this->out('<info>Project Init v.2.0</info>');
        $this->createStructure();
        $this->seedStructure();
    }

    /**
     *
     * Create structure
     *
     * @return void
     */
    public function createStructure()
    {
        $ts = time();
        $this->hr();
        $this->out('Creating database structure...');

        if ($this->migrations->migrate()) {
            $this->out('<success>Done.</success> [' . (time() - $ts) . 's]');
        }
    }

    /**
     * Seed structure
     *
     * @return void
     */
    public function seedStructure()
    {
        $ts = time();
        $this->hr();
        $this->out('Seeding database with default data...');

        $seedOptions = [
            'source' => 'Seeds',
            'seed' => 'InitSeed'
        ];

        if ($this->migrations->seed($seedOptions)) {
            $this->out('<success>Done.</success> [' . (time() - $ts) . 's]');
        }
    }
}
