<?php

namespace App\Shell\Task;

use App\Mailer\PropertiesMailer;
use App\Model\Entity\Property;
use Cake\Console\Shell;
use Cake\Mailer\Mailer;
use Cake\Mailer\MailerAwareTrait;

/**
 * Class PropertyTask
 *
 * @package App\Shell\Task
 */
class PropertyTask extends Shell
{
    use MailerAwareTrait;

    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->mailer = $this->getMailer(PropertiesMailer::class);
    }

    /**
     * @param Property $property Property entity.
     * @param string   $what     Notification name.
     *
     * @return void
     */
    public function notify(Property $property, string $what): void
    {
        $this->$what($property);
    }

    /**
     * @param Property $property Property entity.
     *
     * @return void
     */
    public function expired(Property $property): void
    {
        $this->info('Property #' . $property->get('id') . ' expired');
        $this->mailer->send('propertyExpired', [$property]);
    }

    /**
     * @param Property $property Property entity.
     *
     * @return void
     */
    public function expiring(Property $property): void
    {
        $this->info('Property #' . $property->get('id') . ' expiring soon');
        $this->mailer->send('propertyExpireSoon', [$property]);
    }
}
