<?php

namespace App\Shell\Task;

use App\Mailer\Members\AccountMailer;
use App\Model\Entity\Group;
use App\Model\Entity\User;
use App\Model\Table\UsersTable;
use Cake\Console\ConsoleOptionParser;
use Cake\Console\Shell;
use Cake\Mailer\Mailer;
use Cake\Mailer\MailerAwareTrait;

/**
 * Class ProfileTask
 *
 * @property UsersTable $Users
 *
 * @package App\Shell\Task
 */
class ProfileTask extends Shell
{

    use MailerAwareTrait;

    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Users');

        $this->mailer = $this->getMailer(AccountMailer::class);
    }

    /**
     * @return void
     */
    public function main(): void
    {
        $this->abort('No task defined. Available tasks: [incomplete]');
    }

    /**
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->addSubcommand('incomplete', [
            'help' => 'Notify incomplete profiles'
        ]);
        return $parser;
    }

    /**
     * @return void
     */
    public function incomplete(): void
    {
		/*
		// NR 2019-09-05 - Stop 'incomplete details' email from being sent
        $this->Users->find('all', [
            'conditions' => [
                'group_id' => Group::MEMBERS,
                'deleted' => false,
                'active' => true,
                'UserDetails.profile_completion' => false
            ],
            'contain' => 'UserDetails'
        ])->each(function (User $user) {
            $this->mailer->send('incomplete', [$user]);
            $this->info($user->get('email') . ' notified.');
        });
		*/
    }
}
