<?php

namespace App\Shell\Task;

use App\Mailer\ServicesMailer;
use App\Model\Entity\Service;
use Cake\Console\Shell;
use Cake\Mailer\Mailer;
use Cake\Mailer\MailerAwareTrait;

/**
 * Class ServiceTask
 *
 * @package App\Shell\Task
 */
class ServiceTask extends Shell
{
    use MailerAwareTrait;

    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->mailer = $this->getMailer(ServicesMailer::class);
    }

    /**
     * @param Service $service Service entity.
     * @param string  $what    Notification name.
     *
     * @return void
     */
    public function notify(Service $service, string $what): void
    {
        $this->$what($service);
    }

    /**
     * @param Service $service Service entity.
     *
     * @return void
     */
    public function expired(Service $service): void
    {
        $this->info('Service #' . $service->get('id') . ' expired');
        $this->mailer->send('serviceExpired', [$service]);
    }

    /**
     * @param Service $service Service entity.
     *
     * @return void
     */
    public function expiring(Service $service): void
    {
        $this->info('Service #' . $service->get('id') . ' expiring soon');
        $this->mailer->send('serviceExpireSoon', [$service]);
    }
}
