<?php

/**
 * @author  Lukasz Dragan <lukasz.dragan@econnect4u.pl>
 * @author  Marcin Nierobiś <marcin.nierobis@econnect4u.pl>
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-12-21)
 * @version 1.1
 */

namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;

/**
 * Class RegenerateSeedsShell
 */
class RegenerateSeedsShell extends Shell
{
    /**
     * List of excluded files
     *
     * @var array
     */
    private $excludedFiles = ['UniversalSeed.php', 'InitSeed.php', 'GlobalSeed.php'];

    /**
     * Regenerate all seed files based on files already in config/Seeds directory.
     *
     * @return void
     */
    public function main()
    {
        $this->clear();
        $this->hr();
        $this->out('<info>Regenerating...</info>');
        $this->out('<info>Original template file: /plugins/Seed/src/Template/Bake/Seed/seed.ctp</info>');
        $this->hr();
        $seedDirectory = new Folder(CONFIG . "Seeds");
        $seedFiles = $seedDirectory->find('.*Seed\.php');
        foreach ($seedFiles as $key => $value) {
            if (!in_array($value, $this->excludedFiles) && !is_dir($value)) {
                $this->dispatchShell('bake seed --force --data ' . str_replace('Seed.php', '', $value) . ' --theme Seed');
            }
        }
        $this->hr();
        $this->out('<info>Done.</info>');
    }
}
