<?php

namespace App\Shell;

use Cake\Console\ConsoleOptionParser;
use Cake\Console\Shell;

/**
 * @package App\Shell
 */
class UserShell extends Shell
{

    public $tasks = ['Profile'];

    /**
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();
    }

    /**
     * @return ConsoleOptionParser
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->addSubcommand('profile', [
            'help' => 'Profile commands',
            'parser' => $this->Profile->getOptionParser(),
        ]);
        return $parser;
    }

    /**
     * @return boolean|integer|void|null
     */
    public function main()
    {
        $this->abort('No task defined. Available shells: [profile]');
    }
}
