<?php

namespace App\Shell;

use Cake\Console\Shell;
use App\Controller\Frontend\SitemapController;
use Cake\Core\Configure;

/**
 * Sitemap Shell
 *
 * @author Marcin Nierobiś <marcin.nierobis@econnect4u.pl>
 * @date date(2019-06-27)
 * @version 1.0
 */
class SitemapShell extends Shell
{

    /**
     * Startup method.
     *
     * @return void
     */
    public function startup()
    {
        parent::startup();
        $this->clear();
    }

    /**
     * __construct
     *
     * @param \Cake\Console\ConsoleIo|null $io An io instance.
     * @return void
     */
    public function __construct(ConsoleIo $io = null)
    {
        parent::__construct($io);
    }

    /**
     * Main method
     *
     * @return void
     */
    public function main()
    {
// Commented because of the task no. LS-455
//        $this->generateSitemaps();
//        $this->updateGoogleAndBing();
    }

    /**
     * Generate sitemaps
     *
     * @return void
     */
    public function generateSitemaps()
    {
        $sitemapController = new SitemapController();

        $sitemapController->articles();
        $sitemapController->caseStudies();
        $sitemapController->properties();
        $sitemapController->services();
        $sitemapController->pages();
        $sitemapController->categories();
        $sitemapController->mainSitemap();
    }

    /**
     * Submit sitemap to Google and Bing.
     *
     * @return void
     */
    public function updateGoogleAndBing()
    {
        $projectUrl  = Configure::read('Website.url');
        $sitemapFile = 'sitemap.xml';
        if (file_exists(WWW_ROOT.$sitemapFile)) {
            file_get_contents("http://www.google.com/ping?sitemap=".$projectUrl.$sitemapFile);
            file_get_contents("http://www.bing.com/webmaster/ping.aspx?siteMap=".$projectUrl.$sitemapFile);
            $this->out('<info>Sitemap has been updated.</info>');
        } else {
            $this->out('<info>Sitemap file doesn\'t exists.</info>');
        }
    }
}
