<?php

namespace App\Shell;

use Cake\Console\Shell;
use App\Controller\Component\EmailComponent;
use Cake\View\View;
use App\Model\Enum\EmailType;
use App\Model\Enum\PropertyPurpose;
use Cake\Core\Configure;
use Cake\Mailer\Email;
use Cake\I18n\Time;

/**
 * Cron Email Queue
 *
 * @author Marcin Nierobiś <marcin.nierobis@econnect4u.pl>
 * @date date(2019-03-27)
 * @version 1.0
 */
class CronEmailQueueShell extends Shell
{
    /**
     * EmailComponent
     *
     * @var EmailComponent|null
     */
    protected $Email = null;

    /**
     * Startup method.
     *
     * @return void
     */
    public function startup()
    {
        parent::startup();
        $this->clear();
    }

    /**
     * __construct
     *
     * @param \Cake\Console\ConsoleIo|null $io An io instance.
     * @return void
     */
    public function __construct(ConsoleIo $io = null)
    {
        parent::__construct($io);
        $this->projectUrl = Configure::read('Website.url');
        $this->loadModel('Articles');
        $this->loadModel('Properties');
        $this->loadModel('Services');
    }

    /**
     * Main method
     *
     * @return void
     */
    public function main()
    {
        $this->findMembersToNewsletter();
    }

    /**
     * Find and generate records for EmailQueueMembers.
     *
     * @return void
     */
    public function findMembersToNewsletter()
    {
		/*
        $this->loadModel('Users');
        $this->loadModel('EmailQueueMembers');
        $this->loadModel('ServiceContacts');
        $users = $this->Users->findUsersToWeeklyNewsletter();
        $contacts = $this->ServiceContacts->find('all');

        $data = [];
        foreach ($users as $key => $user) {
            if ($user->user_detail->pref_buying === true) {
                $data[$key.'1']['first_name'] = $user->first_name;
                $data[$key.'1']['email']      = $user->email;
                $data[$key.'1']['email_type'] = EmailType::BUYING;
            }
            if ($user->user_detail->pref_selling === true) {
                $data[$key.'2']['first_name'] = $user->first_name;
                $data[$key.'2']['email']      = $user->email;
                $data[$key.'2']['email_type'] = EmailType::SELLING;
            }
            if ($user->user_detail->pref_professional === true) {
                $data[$key.'3']['first_name'] = $user->first_name;
                $data[$key.'3']['email']      = $user->email;
                $data[$key.'3']['email_type'] = EmailType::PROFESSIONALS;
            }
            if ($user->user_detail->pref_insights === true) {
                $data[$key.'4']['first_name'] = $user->first_name;
                $data[$key.'4']['email']      = $user->email;
                $data[$key.'4']['email_type'] = EmailType::INSIGHTS;
                $data[$key.'4']['options']    = $user->user_detail->pref_insights_list;
            }
        }
        foreach ($contacts as $contactKey => $contact) {
            $data[$contactKey.'1']['first_name'] = $contact->name;
            $data[$contactKey.'1']['email']      = $contact->email;
            $data[$contactKey.'1']['email_type'] = EmailType::BUYING;

            $data[$contactKey.'2']['first_name'] = $contact->name;
            $data[$contactKey.'2']['email']      = $contact->email;
            $data[$contactKey.'2']['email_type'] = EmailType::PROFESSIONALS;

            $data[$contactKey.'3']['first_name'] = $contact->name;
            $data[$contactKey.'3']['email']      = $contact->email;
            $data[$contactKey.'3']['email_type'] = EmailType::INSIGHTS;
            $data[$contactKey.'3']['options']    = 'ALL';
        }

        $entities = $this->EmailQueueMembers->newEntities($data);
        $this->out('<info>'.count($entities).' records found.</info>');
        $this->EmailQueueMembers->saveMany($entities);
        $this->out('<info>Saved.</info>');
        $this->out('<info>Everything went well.</info>');
		*/
		
		/**********************************************************
		 *
		 * BLUEFRONTIER
		 *
		 **********************************************************/
		require(__DIR__  . '/../../bluefrontier.php');
		\BlueFrontier\Helper::membersToNewsletter();
		/**********************************************************
		 *
		 * /BLUEFRONTIER
		 *
		 **********************************************************/
		
    }

    /**
     * Get ctatics contents.
     *
     * @return array
     */
    protected function getStaticContents()
    {
        $data = [];
        $this->loadModel('StaticContents');
        $contents = $this->StaticContents->find('all');
        if ($contents) {
            foreach ($contents as $content) {
                $data[$content->var_name] = $content->value;
            }
        }
        return $data;
    }

    /**
     * Generate emails records for EmailQueue.
     *
     * @return void
     */
    public function generateEmails()
    {
        $this->loadModel('EmailQueue');
        $this->loadModel('EmailQueueMembers');
        $members = $this->EmailQueueMembers->find('all')->limit(30);
        $this->out('<info>'.$members->count().' records found.</info>');
        $this->out('<info>Generating...</info>');

        $data = [];
        foreach ($members as $key => $member) {
            $content = $this->generateEmailContent($member);
            if (!empty($content)) {
                $data[$key]['email'] = $member->email;
                $data[$key]['subject'] = $this->generateEmailSubject($member);
                $data[$key]['content'] = $content;
                $data[$key]['is_sent'] = false;
            } else {
                $this->out('<info>There is no data to send email ('.$member->email_type.') to '.$member->email.'</info>');
            }
            $this->EmailQueueMembers->delete($member);
        }
        $savedCounter    = 0;
        $nonSavedCounter = 0;
        $dataCounter     = count($data);
        foreach ($data as $item) {
            $entity = $this->EmailQueue->newEntity($item);
            if ($this->EmailQueue->save($entity)) {
                $savedCounter++;
            } else {
                $nonSavedCounter++;
            }
        }
        if ($nonSavedCounter > 0) {
            $this->out('<info>Something went wrong with saving '.$nonSavedCounter.' items</info>');
        }
        $this->out('<info>Saved '.$savedCounter.' of '.$dataCounter.' items.</info>');
    }

    /**
     * Generate emails subject.
     *
     * @param object $member Item of the EmailQueueMembers table.
     * @return string
     */
    private function generateEmailSubject($member)
    {
        $subject = '';

        switch ($member->email_type) {
            case EmailType::PROFESSIONALS:
                $subject = 'Latest Businesses from The Landsite';
                break;
            case EmailType::INSIGHTS:
                $subject = 'Latest News from The Landsite';
                break;
            case EmailType::BUYING || EmailType::SELLING:
                $subject = 'Latest Properties from The Landsite';
                break;
        }

        return $subject;
    }

    /**
     * Generate emails content.
     *
     * @param object $member Item of the EmailQueueMembers table.
     * @return string
     */
    private function generateEmailContent($member)
    {
        $view         = new View();
        $emailData    = null;
        $templatePath = 'Email/html/';
        switch ($member->email_type) {
            case EmailType::PROFESSIONALS:
                $emailData    = $this->Services->getServicesToSend();
                $templatePath .= 'latest_businesses';
                break;
            case EmailType::INSIGHTS:
                $emailData    = $this->Articles->getLatestInsights($member->options);
                $templatePath .= 'latest_insights';
                break;
            case EmailType::BUYING || EmailType::SELLING:
                $emailData    = $this->Properties->getPropertiesToSend();
                $templatePath .= 'latest_properties';
                break;
        }
        if (!empty($emailData)) {
            $view->set('statics', $this->getStaticContents());
            $view->set('_projectUrl', $this->projectUrl);
            $view->set(compact('member', 'emailData'));
            $body = $view->render($templatePath, 'Email/html/landsite');
            return $body;
        }
    }

    /**
     * Send queue emails.
     *
     * @return void
     */
    public function sendEmailsFromQueue()
    {
        $this->loadModel('EmailQueue');
        $emails = $this->EmailQueue->findEmailsToSend();
        $this->out('<info>'.$emails->count().' records found.</info>');
        $this->out('<info>Sending...</info>');
        if (!empty($emails->toArray())) {
            foreach ($emails as $email) {
                $mailer = new Email('default');
                $mailer
                    ->setTo($email->email)
                    ->setSubject($email->subject)
                    ->setEmailFormat('html');
                if ($mailer->send($email->content)) {
                    $email->is_sent   = true;
                    $email->send_date = Time::now();
                    $this->EmailQueue->save($email);
                }
            }
            $this->out('<info>Everything went well.</info>');
        } else {
            $this->out('<info>There are no records.</info>');
        }
    }
}
