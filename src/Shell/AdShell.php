<?php

namespace App\Shell;

use App\Model\Entity\Property;
use App\Model\Entity\Service;
use App\Model\Enum\PropertyStatus;
use App\Model\Enum\ServiceStatus;
use App\Model\Table\PropertiesTable;
use App\Model\Table\ServicesTable;
use App\Shell\Task\PropertyTask;
use App\Shell\Task\ServiceTask;
use Cake\Console\Shell;

/**
 * @property PropertiesTable $Properties
 * @property ServicesTable   $Services
 * @property PropertyTask    $Property
 * @property ServiceTask     $Service
 *
 * @package App\Shell
 */
class AdShell extends Shell
{

    public $tasks = ['Property', 'Service'];

    /**
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Properties');
        $this->loadModel('Services');
    }

    /**
     * @return boolean|integer|void|null
     */
    public function main()
    {
        $this->abort('No task defined. Available tasks: [expired, expiring]');
    }

    /**
     * Set EXPIRED status on properties and services that were expired and notify owners.
     *
     * @return void
     */
    public function expired(): void
    {
		/*
        $query = $this->Properties->query();
        $this->Properties->find('all', [
            'conditions' => [
                'ad_expiry_date <' => $query->func()->now(),
                'status !=' => PropertyStatus::EXPIRED
            ],
            'contain' => 'Users'
        ])->each(function (Property $property) {
            $property->set('status', PropertyStatus::EXPIRED);
            if ($this->Properties->save($property)) {
                $this->Property->notify($property, 'expired');
            }
        });
		
        $query = $this->Services->query();
        $this->Services->find('all', [
            'conditions' => [
                'ad_expiry_date <' => $query->func()->now(),
                'status !=' => ServiceStatus::EXPIRED
            ],
            'contain' => 'Users'
        ])->each(function (Service $service) {
            $service->set('status', ServiceStatus::EXPIRED);
            if ($this->Services->save($service)) {
                $this->Service->notify($service, 'expired');
            }
        });
		*/
    }

    /**
     * Send notification on properties and services that are expiring soon (5 days before expiry).
     *
     * @return void
     */
    public function expiring(): void
    {
		/*
        $this->Properties->find('all', [
            'conditions' => [
                'DATEDIFF(ad_expiry_date, NOW()) =' => 5,
                'status !=' => PropertyStatus::EXPIRED
            ],
            'contain' => 'Users'
        ])->each(function (Property $property) {
            $this->Property->notify($property, 'expiring');
        });
		
        $this->Services->find('all', [
            'conditions' => [
                'DATEDIFF(ad_expiry_date, NOW()) =' => 5,
                'status !=' => ServiceStatus::EXPIRED
            ],
            'contain' => 'Users'
        ])->each(function (Service $service) {
            $this->Service->notify($service, 'expiring');
        });
		*/
    }
}
