<?php

namespace App\Http\Exception;

/**
 * Represents an HTTP 401 error.
 *
 * @author Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
class MethodNotAllowedException extends AjaxMethodNotAllowedException {

}
