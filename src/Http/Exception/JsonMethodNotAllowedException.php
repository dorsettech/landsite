<?php

namespace App\Http\Exception;

use Cake\Http\Exception\MethodNotAllowedException;

/**
 * Represents an HTTP 401 error.
 *
 * @author Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
class JsonMethodNotAllowedException extends MethodNotAllowedException {

    private $allowed = '';

    /**
     * Constructor
     *
     * @param string|null $message If no message is given 'Unauthorized' will be the message
     * @param int $code Status code, defaults to 401
     * @param string $allowed
     */
    public function __construct($message = null, $code = 401, $allowed = '') {
        parent::__construct($message, $code);

        $this->allowed = $allowed;
    }

    public function getAllowed() {
        return $this->allowed;
    }
}
