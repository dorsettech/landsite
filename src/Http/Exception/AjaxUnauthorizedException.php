<?php

namespace App\Http\Exception;

use Cake\Http\Exception\UnauthorizedException;

/**
 * Represents an HTTP 401 error.
 *
 * @author Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
class AjaxUnauthorizedException extends UnauthorizedException {
    /**
     * Constructor
     *
     * @param string|null $message If no message is given 'Unauthorized' will be the message
     * @param int $code Status code, defaults to 401
     */
    public function __construct($message = null, $code = 401) {
        parent::__construct($message, $code);
    }
}
