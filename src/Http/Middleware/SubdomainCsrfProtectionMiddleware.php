<?php

namespace App\Http\Middleware;

use Cake\Core\Configure;
use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\I18n\Time;

/**
 * Class SubdomainCsrfProtectionMiddleware
 * @package App\Http\Middleware
 *
 * @author Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
class SubdomainCsrfProtectionMiddleware extends CsrfProtectionMiddleware
{
    /**
     * It is worth to know about http only flag considerations.
     *
     * @see https://security.stackexchange.com/questions/175536/does-a-csrf-cookie-need-to-be-httponly
     * @param string $token
     * @param ServerRequest $request
     * @param Response $response
     * @return Response
     */
    protected function _addTokenCookie($token, ServerRequest $request, Response $response): Response
    {
        $expiry = new Time($this->_config['expiry']);

        $subdomains = Configure::read('Domain.subdomains');

        $domain = Configure::read('Domain.root');
        $httpOnly = $this->_config['httpOnly'];
        if ($subdomains) {
            $domain = ".$domain";
            $httpOnly = false;
        }

        return $response->withCookie($this->_config['cookieName'], [
            'value' => $token,
            'expire' => $expiry->format('U'),
            'path' => $request->getAttribute('webroot'),
            'domain' => $domain,
            'secure' => $this->_config['secure'],
            'httpOnly' => $httpOnly
        ]);
    }
}
