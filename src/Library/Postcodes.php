<?php

namespace App\Library;

/**
 * Postcodes
 */
class Postcodes
{
    /**
     * Get postcode beginning
     *
     * @param string       $postcode     UK Postcode.
     * @param boolean|null $returnString Should the method return result as a string.
     * @return mixed Returns beginning of postcode (Outward Code + Postcode Area) as an array, null if not matching.
     */
    public static function getBeginning(string $postcode, $returnString = false)
    {
        $pattern = '#^([\D]+)([\d]+)\s*([0-9]{1}[A-Z]{2})$#i';

        if (preg_match($pattern, $postcode, $matches)) {
            if ($returnString) {
                return $matches[1] . $matches[2];
            }

            return [$matches[1], $matches[2]];
        }

        return null;
    }
}
