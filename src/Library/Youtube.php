<?php

namespace App\Library;

/**
 * Youtube
 */
class Youtube
{
    /**
     * Get id from URL
     *
     * @param string $url Full Youtube URL.
     * @return string|boolean Returns Youtube ID if matching, false otherwise.
     */
    public static function getId(string $url)
    {
        if (!preg_match_all('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match)) {
            return false;
        }
        
        return $match[1][0];
    }

    /**
     * Get video URL
     *
     * Video can be used in iframe:
     * <iframe src="$url" class="embed-responsive-item" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"></iframe>
     *
     * @param string $videoId Video id.
     * @return string Returns full Youtube video URL.
     */
    public static function getUrl(string $videoId)
    {
        return 'https://www.youtube.com/embed/' . $videoId . '?controls=0&autoplay=0&modestbranding=1&showinfo=0';
    }
}
