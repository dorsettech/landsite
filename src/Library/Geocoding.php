<?php
/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */

namespace App\Library;

use Cake\Http\Client;

/**
 * Geocoding
 */
class Geocoding
{
    /**
     * Http Client
     *
     * @var Client
     */
    private $client = '';

    /**
     * Geocoding API url
     *
     * @var string
     */
    private $url = 'https://maps.googleapis.com/maps/api/geocode/json';

    /**
     * Geocoding API key
     *
     * @var string
     */
    private $apiKey = '';

    /**
     * Address to geocode
     *
     * @var string
     */
    private $address = '';

    /**
     * Default country
     *
     * @var string
     */
    private $country = 'UK';

    /**
     * Google Geocoding API response
     *
     * @var \Cake\Http\Client\Response
     */
    private $response = '';

    /**
     * Response body
     *
     * @var object
     */
    private $body = '';

    /**
     * Construct
     * @param array|null $options Additional options. Available keys: api_key, address, country.
     * @return void
     */
    public function __construct($options = [])
    {
        $this->setClient();

        if (!empty($options['api_key'])) {
            $this->setApiKey($options['api_key']);
        }
        if (!empty($options['country'])) {
            $this->setCountry($options['country']);
        }
        if (!empty($options['address'])) {
            $this->setAddress($options['address']);
        }
    }

    /**
     * Set API key
     *
     * @param string $apiKey API key.
     * @return void
     */
    public function setApiKey(string $apiKey): void
    {
        $this->apiKey = $apiKey;
    }

    /**
     * Set country
     *
     * @param string $country Country name or short.
     * @return void
     */
    public function setCountry(string $country): void
    {
        $this->country = $country;
    }

    /**
     * Set address
     *
     * @param string $address Address to lookup. Country will be added automatically if it's missing in address.
     * @return void
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;

        if (!preg_match('#' . $this->country . '#', $this->address)) {
            $this->address .= ', ' . $this->country;
        }

        $this->setResponse();
        $this->setBody();
    }

    /**
     * Set client
     *
     * @return void
     */
    private function setClient(): void
    {
        $this->client = new Client();
    }

    /**
     * Set response
     *
     * @return void
     */
    private function setResponse(): void
    {
        if (!empty($this->address) && !empty($this->apiKey)) {
            $this->response = $this->client->get($this->url, [
                'address' => $this->address,
                'key'     => $this->apiKey
            ]);
        }
    }

    /**
     * Set response data
     *
     * @return void
     */
    private function setBody(): void
    {
        if (!empty($this->response) && $this->response->isOk()) {
            $this->body = $this->response->body('json_decode');
        }
    }

    /**
     * Get coordinates
     *
     * @param string|null $address Address to lookup. Can be left empty if passed as an option when creating instance.
     * @return array|boolean Returns array with lat|lng or false if something goes wrong.
     */
    public function getCoordinates($address = '')
    {
        if (!empty($address)) {
            $this->setAddress($address);
        }

        if (!empty($this->body->results[0]->geometry->location)) {
            return (array) $this->body->results[0]->geometry->location;
        }

        return false;
    }

    /**
     * Geo coords - short for getCoordinates
     *
     * @param string|null $address Address to lookup. Can be left empty if passed as an option when creating instance.
     * @return array|boolean Returns array with lat|lng or false if something goes wrong.
     */
    public function getCoords($address = '')
    {
        return $this->getCoordinates($address);
    }

    /**
     * Get postcode
     *
     * @param string|null $address Address to lookup. Can be left empty if passed as an option when creating instance.
     * @return array|boolean
     */
    public function getPostcode($address = '')
    {
        if (!empty($address)) {
            $this->setAddress($address);
        }

        $coords = $this->getCoords();

        if (!empty($coords)) {
            $response = $this->client->get($this->url, [
                'latlng'      => $coords['lat'] . ',' . $coords['lng'],
                'result_type' => 'postal_code',
                'key'         => $this->apiKey
            ]);
            $body     = $response->body('json_decode');
        }

        if (!empty($body->results[0]->address_components)) {
            foreach ($body->results[0]->address_components as $addressComponent) {
                if (in_array('postal_code', $addressComponent->types)) {
                    return $addressComponent->long_name;
                }
            }
        }

        return false;
    }
}
