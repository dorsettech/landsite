<?php
/**
 * @author Jakub Gałyga <jakub@ninjacode.pl>
 * @author Stefan <marcin@econnect4u.pl>
 * @date date(2019-01-07)
 * @version 2.0
 */

namespace App\Library;

use Cake\Filesystem\Folder;
use ReflectionClass;
use ReflectionMethod;

/**
 * Class ACL
 *
 * @package App\Library
 */
class ACL
{
    /**
     * Default access for root
     */
    const ROOT_ACCESS = true;

    /**
     * Default has access value
     */
    const HAS_ACCESS = false;

    /**
     * Default access for root
     */
    const PREFIX_ALLOWED = ['frontend'];

    /**
     * Namespaces
     *
     * @var array
     */
    protected $_namespaces = [
        'App\Controller\Panel',
        'FileManager\Controller\Panel'
    ];

    /**
     * Ignored controllers list
     *
     * @var array
     */
    protected $_ignoreListController = [
        '.',
        '..',
        '.DS_Store',
        'Component',
        'ErrorController.php',
        'AppController.php',
        'PanelController.php'
    ];

    /**
     * Ignore actions list
     *
     * @var array
     */
    protected $_ignoreListActions = [
        'beforeFilter',
        'afterFilter',
        'beforeRender',
        'initialize',
        'acl',
    ];

    /**
     * Resources method
     *
     * @return array
     */
    public function getResources(): array
    {
        $allControllers = $this->getControllers();
        $resources      = [];
        foreach ($allControllers as $namespace => $controllers) {
            foreach ($controllers as $controller) {
                $resources[$namespace][$controller] = $this->getActions($namespace, $controller)[$controller];
            }
        }

        return $resources;
    }

    /**
     * Controller list method
     *
     * @return array
     */
    public function getControllers(): array
    {
        $controllers = [];

        foreach ($this->_namespaces as $namespace) {
            $folder = new Folder($this->getNamespaceDir($namespace));
            $read   = $folder->read(true, $this->_ignoreListController);

            if (!empty($read[1])) {
                foreach ($read[1] as $controller) {
                    $controllers[$namespace][] = str_replace('Controller', '', explode('.', $controller)[0]);
                }
            }
        }

        return $controllers;
    }

    /**
     * Get namespace dir
     *
     * @param string $namespace Namespace.
     * @return string Returns path for specific namespace.
     */
    private function getNamespaceDir(string $namespace): string
    {
        if (preg_match('#^App#', $namespace)) {
            $dir = str_replace('App\\', '', $namespace);
        } else {
            $dir = 'plugins\\' . $namespace;
        }

        $dir = str_replace('Controller\\', 'src\\Controller\\', $dir);
        $dir = str_replace('\\', DS, $dir);

        return ROOT . DS . $dir;
    }

    /**
     * Action list method
     *
     * @param string $namespace      Namespace.
     * @param string $controllerName Controller name.
     * @return array
     */
    public function getActions(string $namespace, string $controllerName): array
    {
        $className = $namespace . '\\' . $controllerName . 'Controller';
        $class     = new ReflectionClass($className);
        $actions   = $class->getMethods(ReflectionMethod::IS_PUBLIC);
        $results   = [$controllerName => []];
        
        foreach ($actions as $action) {
            if ($action->class == $className && !in_array($action->name, $this->_ignoreListActions)) {
                array_push($results[$controllerName], $action->name);
            }
        }
        
        return $results;
    }
}
