<?php
/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */

namespace App\Library;

/**
 * Search
 */
final class Search
{
    const RANGES = [
        1  => '1 mile',
        5  => '5 miles',
        10 => '10 miles',
        15 => '15 miles',
        20 => '20 miles',
        50 => '50 miles'
    ];
    const TYPES = [
        'SALE' => 'For Sale',
        'RENT' => 'To Rent'
    ];
    const PRICES = [
        50000,
        60000,
        70000,
        80000,
        90000,
        100000,
        110000,
        120000,
        125000,
        130000,
        140000,
        150000,
        160000,
        170000,
        175000,
        180000,
        190000,
        200000,
        210000,
        220000,
        230000,
        240000,
        250000,
        260000,
        270000,
        280000,
        290000,
        300000,
        325000,
        350000,
        375000,
        400000,
        425000,
        450000,
        475000,
        500000,
        550000,
        600000,
        650000,
        700000,
        800000,
        900000,
        1000000,
        1250000,
        1500000,
        1750000,
        2000000,
        2500000,
        3000000,
        4000000,
        5000000,
        7500000,
        10000000,
        15000000,
        20000000
    ];

    /**
     * Get prices
     *
     * @return array
     */
    public static function getPrices(): array
    {
        $prices = [];
        
        foreach (self::PRICES as $price) {
            $prices[$price] = number_format($price, 0, '.', ',');
        }

        return $prices;
    }
}
