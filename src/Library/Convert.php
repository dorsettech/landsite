<?php
/**
 * @author Stefan <marcin@econnect4u.pl>
 * @date date(2019-02-09)
 * @version 1.1
 */
namespace App\Library;

use Cake\I18n\FrozenDate;

/**
 * Convert
 */
class Convert
{
    /**
     * Convert to decimal
     *
     * Use for calculations
     *
     * @param float $value Converts given value to float with four digits by default.
     * @return float
     */
    public static function toDecimal(float $value)
    {
        return number_format($value, 4, '.', '');
    }
    
    /**
     * Convert array to list array
     *
     * @param array $data    Data to convert.
     * @param array $options Optional options: keyField, valueField.
     * @return array         Converted data.
     */
    public static function toList(array $data, array $options = [])
    {
        $keyField   = isset($options['keyField']) ? $options['keyField'] : 'id';
        $valueField = isset($options['valueField']) ? $options['valueField'] : 'name';

        $converted = [];

        foreach ($data as $item) {
            if (isset($item[$keyField]) && is_array($valueField)) {
                $tempName = [];
                foreach ($valueField as $key => $value) {
                    $key = is_string($value) ? $value : $key;
                    if (isset($item[$key])) {
                        $before     = (is_array($value) && isset($value['before'])) ? $value['before'] : '';
                        $after      = (is_array($value) && isset($value['after'])) ? $value['after'] : '';
                        $tempName[] = $before.$item[$key].$after;
                    }
                }
                $converted[$item[$keyField]] = implode(' ', $tempName);
            } elseif (isset($item[$keyField], $item[$valueField])) {
                $converted[$item[$keyField]] = $item[$valueField];
            }
        }

        return $converted;
    }

    /**
     * Convert given string to date
     * @param mixed $date Date.
     * @return string Returns formated date string.
     */
    public static function toDate($date)
    {
        $newDate = new FrozenDate($date);

        return $newDate->format('Y-m-d');
    }
}
