<?php

/**
 * @author Michal Skrzypek <dawid.katarzynski@econnect4u.pl>
 * @date (2018-12-15)
 * @version 1.0
 */
use Migrations\AbstractSeed;

/**
 * Analytics seed.
 */
class AnalyticsSeed extends AbstractSeed
{
    /**
     * Run Method. Calls required seeders.
     *
     * @return void
     */
    public function run(): void
    {
        $this->call('AnalyticsGroupsSeed');
        $this->call('AnalyticsVariablesSeed');
    }
}
