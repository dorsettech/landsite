<?php

/**
 * @author Michal Skrzypek <dawid.katarzynski@econnect4u.pl>
 * @date (2018-12-15)
 * @version 1.0
 */

use Migrations\AbstractSeed;

/**
 * AnalyticsGroups seed.
 */
class AnalyticsGroupsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * @return void
     */
    public function run(): void
    {
        $data = [
            [
                'name' => 'Google Analytics Reports',
            ],
        ];

        $table = $this->table('static_content_groups');
        $table->insert($data)->save();
    }
}
