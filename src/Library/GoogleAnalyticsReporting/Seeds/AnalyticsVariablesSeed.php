<?php

/**
 * @author Michal Skrzypek <dawid.katarzynski@econnect4u.pl>
 * @date (2018-12-15)
 * @version 1.0
 */
use Cake\ORM\TableRegistry;
use Migrations\AbstractSeed;

/**
 * AnalyticsVariables seed.
 */
class AnalyticsVariablesSeed extends AbstractSeed
{

    /**
     * Run Method.
     *
     * @return void
     */
    public function run(): void
    {
        $staticContentGroupsTable   = TableRegistry::getTableLocator()->get('StaticContentGroups');
        $googleAnalyticsReportingId = $staticContentGroupsTable
            ->find('all', ['order' => ['id' => 'DESC']])
            ->where(['name' => 'Google Analytics Reports'])
            ->first();
        $id                         = $googleAnalyticsReportingId->id;

        $data = [
            [
                'static_group_id' => $id,
                'var_name'        => 'ga_view_id',
                'value'           => '59426091',
                'type'            => '0',
                'description'     => 'Can be found at the bottom of the page (under "view") here: https://ga-dev-tools.appspot.com/account-explorer/. Remember to select the correct account! Leave blank to disable google analytics widget.',
                'created'         => date('Y-m-d H:i:s'),
                'modified'        => date('Y-m-d H:i:s'),
            ],
            [
                'static_group_id' => $id,
                'var_name'        => 'ga_days_back',
                'value'           => '30',
                'type'            => '1',
                'description'     => 'How many days back from today should the analytics display?',
                'created'         => date('Y-m-d H:i:s'),
                'modified'        => date('Y-m-d H:i:s'),
            ],
        ];

        $table = $this->table('static_contents');
        $table->insert($data)->save();
    }
}
