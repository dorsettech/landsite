# Google Analytics Reporting Library
*by Michal Skrzypek (michal.skrzypek@econnect4u.pl)*

Allows to fetch google analytics data for a given website using Google API v4.

## Requirements

The library requires a JSON file with your company **(not client's)** application credentials.

## First time-usage (with account creation)
### You can skip it if you already have a valid service-account-credentials.json file. It is crucial for the library to work, but you can use the same file for all your clients.

1. Go to https://console.developers.google.com/flows/enableapi?apiid=analyticsreporting.googleapis.com&credential=client_key and create a new project if you don't have one already.
2. Click 'go to credentials'
3. As an API you want to use choose Google Analytics Reporting API. As for "Where you will be calling the API from?" choose "Web server". Click "User data" under "What data will you be accessing?"
4. Click "What credentials do I need?". Specify any name you like, leave two other fields empty and click on "Create OAuth client ID".
5. Choose an email address you would like to have associated with the service (most likely the default selection will be ok).
6. Type anything you like in Product name shown to users, but try to choose a company name to be more trustworthy. Click "continue".
7. A JSON file will be created, but you don't need this data right now. If you ever need it, you can access it from your account.
8. Click "done".
8a. It's good to check if Googole Analytics Reporting API has been actually turned on so go to project Dashboard (hamburger icon next to Google APIs > API Interfaces and services > Dashboard) and check if it's on the Interfaces API list. You have to enable it manually if it's not by clicking on Turn on API interfaces and services. On the library API search for Google Analytics Reporting API and turn it on.
9. Go to https://console.developers.google.com/projectselector/iam-admin/serviceaccounts?supportedpurview=project
10. Create a new project. The previous one was for an API to work, this one will give you actual credentials.
11. Name it whatever you want, but keep in mind you will give this name to clients (or at least their analytics accounts) and it cannot be changed later. For the sake of this instructions we will call it Bespoke API.
12. Choose a localization if you have one and want to use it. Otherwise skip it. Click "CREATE".
13. On project panel click "Project settings" (or similar). Make sure you are on the correct project (it changed automatically for me for some reason). You can choose the corect one from the dropdown next to Google logo.
14. Choose "Service account" and and then "+CREATE SERVICE ACCOUNT".
15. Give it a name you like (but again, remember this will go to you client or at least their analytics account). I will call mine Bespoke in this example.
16. Service account ID will be created automatically, description is optional. Click "CREATE".
17. You will be prompted to choose a role. You can skip it, we will not need it.
18. On the next screen Leave the fields blank, but DO click on "+CREATE KEY", select JSON and click "CREATE".
19. A download should start automatically. Find the file that was downloaded, rename it to 'service-account-credentials.json' and give it to a dev that is installing the library. It is crucial for the library to work. Do not lose the file, it is the only copy.
20. Close the pop-up and click "done". Your account service is created and should look somewhat like this: bespoke@bespoke-api.iam.gserviceaccount.com. You will use this address to verify your app on clients' analytics pages.

## Steps to take with any new project/client
### Make sure the dev installing the library was already given the file created in step #19 from the instruction above before taking further steps.

1. In the previous section you created a service account (something like bespoke@bespoke-api.iam.gserviceaccount.com). Copy it.
2. If you have access to the client's analytics account, go there. Otherwise ask your client to do that. For the sake of simplicity we will assume you have the access.
3. Choose the analytics account you want to track on in your dashboard.
4. Go to ADMIN section.
5. Click User Management.
6. Click the "+" button to add a new user, paste the account (bespoke@bespoke-api.iam.gserviceaccount.com) in our case, in permissions choose Read & Analyze only. Uncheck "notify user by email".
7. Now in the ADMIN section go to selected project Property > View (last column) and click on the View settings
8. Copy the View ID number
9. If the project is already set up, go to CMS panel of our app and go to CMS->Layout Settings->Google Analytics Reports. If there is no such key in the menu, the project was not created yet.
10. Insert view_id as a value of view_id variable. Set other settings as you see fit, but DO NOT CHANGE ANY VARIABLE'S NAME. Most of them are the actual Google API keys so options with changed name will not work. Only change values (and descriptions if you feel like doing it).
