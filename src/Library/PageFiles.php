<?php

namespace App\Library;

/**
 * PageFiles Lib
 */
final class PageFiles
{
    /**
     * Slider image type
     */
    const TYPE_ID_SLIDER   = 1;

    /**
     * Gallery image type
     */
    const TYPE_ID_GALLERY  = 2;

    /**
     * Document file type
     */
    const TYPE_ID_DOCUMENT = 3;
}
