<?php

namespace App\Routing;

use Cake\Routing\Router;

/**
 * Class SubdomainRouter
 *
 * @see SubdomainRouterBuilder
 * @package App\Routing
 * @author Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
class SubdomainRouter extends Router
{
    public static function scope($path, $params = [], $callback = null)
    {
        $builder = new SubdomainRouterBuilder(static::$_collection, '/', [], static::$_collection->getExtensions());
        $builder->scope($path, $params, $callback);
    }
}
