<?php

namespace App\Routing;

use Cake\Routing\RouteBuilder;

/**
 * Class SubdomainRouterBuilder
 *
 * It is pretty useful when you need route many of domains (subdomains)
 *
 * @package App\Routing
 * @author Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
class SubdomainRouterBuilder extends RouteBuilder
{
    protected $_host = '';

    public function __construct($template, $path = '/', $defaults = [], array $options = [])
    {
        return parent::__construct($template, $path, $defaults, $options);
    }

    public function setHost($host)
    {
        $this->_host = $host;

        return $this;
    }

    /**
     * @param string $route
     * @param array $defaults
     * @param array $options
     * @return \Cake\Routing\Route\Route
     */
    public function connect($route, $defaults = [], array $options = [])
    {
        if ($this->_host !== '' && empty($options['_host'])) {
            $options['_host'] = $this->_host;
        }

        return parent::connect($route, $defaults, $options);
    }
}
