<?php

namespace App\Auth;

use Cake\Auth\BaseAuthenticate;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\Network\Request;
use Cake\Utility\Security;
use Exception;
use Firebase\JWT\JWT;

/**
 * Class for authentication by JWT (JSON Web Tokens) with
 * support for login and password authorization.
 *
 * @see    http://jwt.io
 * @see    http://tools.ietf.org/html/draft-ietf-oauth-json-web-token
 * @author Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
class JwtAuthenticate extends BaseAuthenticate
{

    /**
     * Parsed token.
     *
     * @var string|null
     */
    protected $_token;

    /**
     * Payload data.
     *
     * @var object|null
     */
    protected $_payload;

    /**
     * Exception.
     *
     * @var Exception
     */
    protected $_error;

    /**
     * Constructor.
     *
     * Settings for this object.
     *
     * - `header` - Header name to check. Defaults to `'authorization'`.
     * - `prefix` - Token prefix. Defaults to `'bearer'`.
     * - `parameter` - The url parameter name of the token. Defaults to `token`.
     *   First $_SERVER['HTTP_AUTHORIZATION'] is checked for token value.
     *   Its value should be of form "Bearer <token>". If empty this query string
     *   paramater is checked.
     * - `allowedAlgs` - List of supported verification algorithms.
     *   Defaults to ['HS256']. See API of JWT::decode() for more info.
     * - `userModel` - The model name of users, defaults to `Users`.
     * - `fields` - `username` and `password` are required for authorization. Key
     *  `username` denotes the identifier field for fetching user
     *   record. The `sub` claim of JWT must contain identifier value.
     *   Defaults to ['username' => 'username', 'password' => 'password'].
     * - `finder` - Finder method.
     * - `unauthenticatedException` - Fully namespaced exception name. Exception to
     *   throw if authentication fails. Set to false to do nothing.
     *   Defaults to '\Cake\Network\Exception\UnauthorizedException'.
     * - `key` - The key, or map of keys used to decode JWT. If not set, value
     *   of Security::salt() will be used.
     * - `loginAction` - denoted the action in controller which is used for user authorization
     * - `tokenExpiration` - how long token is valid, time() + token expiration in seconds
     * - `algorithm` - algorithm that will be used to encode JWT token
     *
     * @param ComponentRegistry $registry The Component registry used on this request.
     * @param array             $config   Array of config to use.
     */
    public function __construct(ComponentRegistry $registry, array $config)
    {
        $this->setConfig([
            'header' => 'authorization',
            'prefix' => 'bearer',
            'parameter' => 'token',
            'fields' => [
                'username' => 'username',
                'password' => 'password'
            ],
            'unauthenticatedException' => '\Cake\Network\Exception\UnauthorizedException',
            'key' => null,
            'loginAction' => 'login',
            'tokenExpiration' => 604800,
            'algorithm' => 'HS256'
        ]);

        if (empty($config['allowedAlgs'])) {
            $config['allowedAlgs'] = ['HS256'];
        }

        parent::__construct($registry, $config);
    }

    /**
     * Get user record based on info available in JWT.
     *
     * @param ServerRequest $request  The request object.
     * @param Response      $response Response object.
     *
     * @return boolean|array User record array or false on failure.
     * @throws Exception Inherited.
     */
    public function authenticate(ServerRequest $request, Response $response)
    {
        if ($request->getParam('action') === $this->getConfig('loginAction')) {
            $fields = $this->_config['fields'];
            if (!$this->_checkFields($request, $fields)) {
                return false;
            }

            return $this->_findUser(
                $request->is('get') ? $request->getQuery($fields['username']) : $request->getData($fields['username']),
                $request->is('get') ? $request->getQuery($fields['password']) : $request->getData($fields['password'])
            );
        }

        return $this->getUser($request);
    }

    /**
     * Checks the fields to ensure they are supplied.
     *
     * @param ServerRequest $request The request that contains login information.
     * @param array         $fields  The fields to be checked.
     *
     * @return boolean False if the fields have not been supplied. True if they exist.
     */
    protected function _checkFields(ServerRequest $request, array $fields)
    {
        foreach ([$fields['username'], $fields['password']] as $field) {
            $value = $request->is('get') ? $request->getQuery($field) : $request->getData($field);
            if (empty($value) || !is_string($value)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get user record based on info available in JWT.
     *
     * @param ServerRequest $request Request object.
     *
     * @return boolean|array User record array or false on failure.
     * @throws Exception Inherited.
     */
    public function getUser(ServerRequest $request)
    {
        $payload = $this->getPayload($request);
        if (empty($payload)) {
            return false;
        }

        if (!isset($payload->sub)) {
            return false;
        }

        $user = $this->_findUser($payload->sub);

        Configure::write('Auth.User', $user ?: null);

        if (!$user) {
            return false;
        }

        unset($user[$this->_config['fields']['password']]);

        return $user;
    }

    /**
     * Get payload data.
     *
     * @param Request|null $request Request instance or null.
     *
     * @return object|null Payload object on success, null on failure.
     * @throws Exception Inherited.
     */
    public function getPayload($request = null)
    {
        if (!$request) {
            return $this->_payload;
        }

        $payload = null;

        $token = $this->getToken($request);
        if ($token) {
            $payload = $this->_decode($token);
        }

        return $this->_payload = $payload;
    }

    /**
     * Get token from header or query string.
     *
     * @param Request|null $request Request object.
     *
     * @return string|null Token string if found else null.
     */
    public function getToken($request = null)
    {
        $config = $this->_config;

        if (!$request) {
            return $this->_token;
        }

        $header = $request->getHeader($config['header']);

        if (empty($header)) {
            $header = apache_request_headers()[$config['header']] ?? null;
        }

        if ($header && stripos($header[0], $config['prefix']) === 0) {
            return $this->_token = str_ireplace($config['prefix'] . ' ', '', $header[0]);
        }

        if (!empty($config['parameter'])) {
            $this->_token = $request->is('get') ? $request->getQuery($config['parameter']) : $request->getData($config['parameter']);
        }

        return $this->_token;
    }

    /**
     * Decode JWT token.
     *
     * @param string $token JWT token to decode.
     *
     * @return object|null The JWT's payload as a PHP object, null on failure.
     * @throws Exception Inherited.
     */
    protected function _decode(string $token)
    {
        $config = $this->_config;
        try {
            $payload = JWT::decode($token, $config['key'] ?: Security::salt(), $config['allowedAlgs']);

            return $payload;
        } catch (Exception $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            $this->_error = $e;
        }

        return null;
    }

    /**
     * @param array $user User array.
     *
     * @return string
     */
    public function generateToken(array $user): string
    {
        $config = $this->_config;
        if (!isset($user[$config['fields']['username']])) {
            return '';
        }

        return JWT::encode(
            [
                'sub' => $user[$config['fields']['username']],
                'exp' => time() + $config['tokenExpiration']
            ],
            $config['key'] ?: Security::getSalt(),
            $config['algorithm']
        );
    }

    /**
     * Handles an unauthenticated access attempt. Depending on value of config
     * `unauthenticatedException` either throws the specified exception or returns
     * null.
     *
     * @param ServerRequest $request  A request object.
     * @param Response      $response A response object.
     *
     * @return void
     */
    public function unauthenticated(ServerRequest $request, Response $response): void
    {
        if (!$this->_config['unauthenticatedException']) {
            return;
        }

        $message = $this->_error ? $this->_error->getMessage() : $this->_registry->Auth->_config['authError'];

        $exception = new $this->_config['unauthenticatedException']($message);
        throw $exception;
    }
}
