<?php

namespace App\Auth\Storage;

use Cake\Auth\Storage\SessionStorage;

/**
 * Extended Session Storage
 *
 * @package App\Auth\Storage
 * @author Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
class AuthSessionStorage extends SessionStorage {

    public function write($user) {

        $engine = $this->_session->engine('Session');
        $this->_user = $user;

        $this->_session->write($this->_config['key'], $user);
        $engine->update($this->_session->id(), $user);
    }
}
