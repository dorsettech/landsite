<?php

namespace App\Auth\Storage;

use Cake\Auth\Storage\SessionStorage;

/**
 * SessionStorage
 *
 * @author Stefan
 */
class SessionExStorage extends SessionStorage
{
    public function initialize()
    {
        parent::initialize();
    }
    
    /**
     * Write user record to session.
     *
     * @param array $user User record.
     * @return void
     */
    public function write($user)
    {
        $this->_user = $user;

        /**
         * Uncomment line below if you need to renew session everytime session 
         * is saved
         */
        // $this->_session->renew();
        $this->_session->write($this->_config['key'], $user);
    }
}
