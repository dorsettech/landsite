<?php
return [
    'title' => 'Title',
    'title2' => 'Title 2',
    'title3' => 'Title 3',
    'title4' => 'Title 4',
    'title5' => 'Title 5',
    'content' => 'Content',
    'content2' => 'Content2',
    'content3' => 'Content3',
    'content4' => 'Content4',
    'content5' => 'Content5',
    'file' => 'File 1',
    'file2' => 'File 2',
    'file3' => 'File 3',
    'file4' => 'File 4',
    'file5' => 'File 5',
    'gallery' => 'Gallery',
    'redirect_url' => 'Url'
    ]

?>