<section class="properties">
    <div class="container">
        <div class="row">
            <div class="col-12 col-xl-7">
                <?php if (isset($statics['professional_services_listing_intro']) && !empty($statics['professional_services_listing_intro'])): ?>
                    <div class="intro">
                        <?= $statics['professional_services_listing_intro'] ?>
                    </div>
                <?php endif; ?>
                <div class="adverts">
                    <?= $this->element('ProfessionalServices/Ads/ad1') ?>
                </div>
                <?php if (count($services) > 0): ?>
                    <?php $servicesCounter = 0; ?>
                    <?php $adsCounter      = 1; ?>
                    <?php foreach ($services as $service): ?>
                        <?php $servicesCounter++; ?>
                        <?= $this->element('Boxes/service-box', ['service' => $service]) ?>
                        <?php if ($servicesCounter % 3 === 0): ?>
                            <?php if ($adsCounter < 5): ?>
                                <?php $adsCounter++; ?>
                            <?php else: ?>
                                <?php $adsCounter = 1; ?>
                            <?php endif; ?>
                            <div class="d-md-block adverts">
                                <?= $this->element('ProfessionalServices/Ads/ad'.$adsCounter) ?>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <?= $this->element('Sections/pagination') ?>
                <?php else: ?>
                    <?= $this->element('ProfessionalServices/no-items') ?>
                <?php endif; ?>
            </div>
            <div class="col-12 col-xl-5">
                <?= $this->element('ProfessionalServices/listing-sidebar') ?>
                <?= $this->element('ProfessionalServices/sidebar-ads') ?>
            </div>
        </div>
    </div>
</section>
<?php if (!empty($this->request->getSession()->read('Auth.User'))): ?>
    <?= $this->element('Modals/professional-service-enquiry-clear'); ?>
<?php endif; ?>