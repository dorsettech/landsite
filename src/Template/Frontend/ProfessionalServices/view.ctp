<?php

use App\Model\Enum\CredentialType;
use App\Library\Youtube;
?>
<section class="profile">
    <div class="container">
        <div class="row" itemscope itemtype="https://schema.org/LocalBusiness">
            <div class="d-none">
                <span itemprop="url"><?= $this->Url->build('/', true).$this->request->url ?></span>
                <span itemprop="address"><?= $service->location ?></span>
                <span itemprop="email"><?= $service->user->email ?></span>
                <span itemprop="speakable"></span>
                <span itemprop="image"><?= $this->Url->build('/', true).$service->user->image_path ?></span>
            </div>
            <div class="col-12 adverts d-md-none">
                <img src="https://storage.googleapis.com/support-kms-prod/SNP_3094702_en_v0" class="img-fluid" alt="Advert">
            </div>
            <div class="col-12">
                <div class="row info">
                    <div class="col-12 col-lg-8 col-xxl-7">
                        <?= $this->element('ProfessionalServices/main-info') ?>
                    </div>
                    <div class="col-12 col-lg-4 col-xxl-5">
                        <?= $this->element('ProfessionalServices/service-map') ?>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4 col-lg-3">
                <?= $this->element('ProfessionalServices/sidebar') ?>
            </div>
            <div class="col-12 col-md-8 col-lg-9">
                <nav>
                    <?php if (isset($service->description) && !empty($service->description)): ?>
                        <button href="#profile-1" class="scrollto"><i class="customicons icon8"></i><span><?= __('Business Overview') ?></span></button>
                    <?php endif; ?>
                    <?php if (isset($service->products) && !empty($service->products)): ?>
                        <button href="#profile-2" class="scrollto"><i class="customicons icon9"></i><span><?= __('Products & Services') ?></span></button>
                    <?php endif; ?>
                    <?php if (isset($service->credentials) && !empty($service->credentials)): ?>
                        <button href="#profile-3" class="scrollto"><i class="customicons icon10"></i><span><?= __('Credentials') ?></span></button>
                    <?php endif; ?>
                    <?php if (isset($service->service_media) && !empty($service->service_media)): ?>
                        <button href="#profile-4" class="scrollto"><i class="customicons icon11"></i><span><?= __('Images') ?></span></button>
                    <?php endif; ?>
                    <?php if ($service->case_studies->count() > 0): ?>
                        <button href="#profile-5" class="scrollto"><i class="customicons icon12"></i><span><?= __('Case Studies') ?></span></button>
                    <?php endif; ?>
                    <?php if ($service->insights->count() > 0): ?>
                        <button href="#profile-6" class="scrollto"><i class="customicons icon13"></i><span><?= __('News') ?></span></button>
                    <?php endif; ?>
                </nav>
                <div class="detail">
                    <?php if (isset($service->description) && !empty($service->description)): ?>
                        <div id="profile-1">
                            <div class="heading"><?= __('Business Overview') ?></div>
                            <div itemprop="description">
                                <?= $service->description ?>
                            </div>
                            <?php if (isset($service->videos) && !empty($service->videos)): ?>
                                <?php foreach ($service->videos as $video): ?>
                                    <p>
                                        <iframe src="<?= Youtube::getUrl($video->source) ?>" class="embed-responsive-item" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" width="100%" height="400px"></iframe>
                                    </p>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                    <?php if (isset($service->products) && !empty($service->products)): ?>
                        <div id="profile-2" class="services">
                            <div class="heading"><?= __('Products & Services') ?></div>
                            <ul class="list-unstyled">
                                <?php foreach ($service->products as $product): ?>
                                    <li><?= $product->name ?></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                    <?php if (isset($service->credentials) && !empty($service->credentials)): ?>
                        <div id="profile-3" class="credentials">
                            <div class="row">
                                <?php if (isset($service->credentials[CredentialType::ACC]) && !empty($service->credentials[CredentialType::ACC])): ?>
                                    <div class="col-12 col-lg-6">
                                        <div class="heading"><?= __('Accreditations') ?></div>
                                        <ul class="list-unstyled">
                                            <?php foreach ($service->credentials[CredentialType::ACC] as $accreditation): ?>
                                                <li><?= $accreditation->name ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                <?php endif; ?>
                                <?php if (isset($service->credentials[CredentialType::ASS]) && !empty($service->credentials[CredentialType::ASS])): ?>
                                    <div class="col-12 col-lg-6">
                                        <div class="heading"><?= __('Associations') ?></div>
                                        <ul class="list-unstyled">
                                            <?php foreach ($service->credentials[CredentialType::ASS] as $association): ?>
                                                <li><?= $association->name ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?= $this->element('ProfessionalServices/media-carousel') ?>

                    <?php if ($service->case_studies->count() > 0): ?>
                        <div id="profile-5">
                            <div class="heading">
                                <?= __('Case Studies') ?>
                                <div class="text-right">
                                    <?= $this->Html->link('View all', ['_name' => 'case-studies-listing', '?' => ['author' => $service->user->id]], ['class' => 'btn btn-primary']) ?>
                                </div>
                            </div>
                            <div class="row posts carousel">
                                <?php foreach ($service->case_studies as $caseStudy): ?>
                                    <div class="col-12">
                                        <?= $this->element('Boxes/case-study-box', ['caseStudy' => $caseStudy]) ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($service->insights->count() > 0): ?>
                        <div id="profile-6">
                            <div class="row heading">
                                <div class="col-6">
                                    <?= __('News') ?>
                                </div>
                                <div class="col-6 text-right">
                                    <?= $this->Html->link('View all', ['_name' => 'news-listing', '?' => ['author' => $service->user->id]], ['class' => 'btn btn-primary']) ?>
                                </div>
                            </div>
                            <div class="row posts carousel">
                                <?php foreach ($service->insights as $insight): ?>
                                    <div class="col-12">
                                        <?= $this->element('Boxes/insight-box', ['article' => $insight]) ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-12">
                <?= $this->element('ProfessionalServices/sidebar-content'); ?>
            </div>
        </div>
    </div>
</section>
<?= $this->element('Modals/professional-service-enquiry'); ?>
