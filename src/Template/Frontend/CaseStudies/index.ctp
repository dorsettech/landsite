<section class="posts listing">
    <div class="container">
        <?php if (isset($contents->main->content) && !empty($contents->main->content)): ?>
            <div class="row head">
                <div class="col-12">
                    <?= $contents->main->content ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="row list">
            <?php if (isset($caseStudies) && !empty($caseStudies)): ?>
                <?php $caseStudiesCounter = 0; ?>
                <?php foreach ($caseStudies as $caseStudy): ?>
                    <?php $caseStudiesCounter++; ?>
                    <div class="col-12 col-sm-6 col-lg-4">
                        <?= $this->element('Boxes/case-study-box', ['caseStudy' => $caseStudy]) ?>
                    </div>
                    <?php if ($caseStudiesCounter === 6 || $caseStudies->count() === $caseStudiesCounter): ?>
                        <div class="col-12 adverts">
                            <?= $this->element('CaseStudies/ad') ?>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php else: ?>
                <?= $this->element('Sections/no-items') ?>
            <?php endif; ?>
        </div>
        <div>
            <?= $this->element('Sections/pagination') ?>
        </div>
    </div>
</section>