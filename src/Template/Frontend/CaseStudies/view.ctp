<section class="post">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-8 col-lg-9">
                <div itemscope itemtype="http://schema.org/Article">
                    <div class="d-none">
                        <span itemprop="url"><?= $this->Url->build('/', true).$this->request->url ?></span>
                        <?php if (!empty($caseStudy->publish_date)): ?>
                            <span itemprop="datePublished"><?= $caseStudy->publish_date->format('d-m-Y') ?></span>
                        <?php endif; ?>
                        <span itemprop="image"><?= $this->Url->build('/', true).$caseStudy->image_path ?></span>
                        <span itemprop="wordCount"><?= str_word_count(strip_tags($caseStudy->body)) ?></span>
                    </div>
                    <h1 itemprop="name headline"><?= $caseStudy->title ?></h1>
                    <p itemscope itemprop="publisher" itemtype="http://schema.org/Organization">
                        <i class="date">Published by <span itemprop="name"><?= $this->Page->articlePostedBy($caseStudy) ?></span>
                            <?php if (!empty($caseStudy->publish_date)): ?>
                                on <?= $caseStudy->publish_date->format('jS F Y') ?>
                            <?php endif; ?>
                        </i>
                        <span class="d-none" itemprop="logo">-</span>
                    </p>
                    <p><img src="<?= $this->Images->scale($caseStudy->image_path, 1140, 511) ?>" class="img-fluid" alt="title"></p>
                    <p itemprop="articleBody">
                        <?= $caseStudy->body ?>
                    </p>
                    <?php if (isset($caseStudy->user) && !empty($caseStudy->user)): ?>
                        <?= $this->element('Articles/author-info', ['author' => $caseStudy->user]) ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="sidebar col-12 col-md-4 col-lg-3">
                <?php if (isset($statics['want_to_contribute']) && !empty($statics['want_to_contribute'])): ?>
                    <hr>
                    <?= $statics['want_to_contribute'] ?>
                <?php endif; ?>
                <hr>
                <div class="box">
                    <div class="heading">Related Services Categories</div>
                    <ul class="list-unstyled columns">
                        <li>Architects</li>
                        <li>Builders</li>
                        <li>Estate Agents</li>
                        <li>Property Management</li>
                        <li>Planning Consultants</li>
                        <li>Solicitors</li>
                    </ul>
                </div>
                <?php if (isset($statics['get_your_business']) && !empty($statics['get_your_business'])): ?>
                    <hr>
                    <?= $statics['get_your_business'] ?>
                <?php endif; ?>
                <hr>
                <div class="form-row justify-content-center">
                    <div class="adverts col-6 col-md-12">
                        <a href="https://members.thelandsite.co.uk/register"><img src="/img/landsite-ad.png" class="img-fluid"/></a>
                    </div>
                    <div class="adverts col-6 col-md-12">
                        <a href="https://members.thelandsite.co.uk/register"><img src="/img/landsite-ad.png" class="img-fluid"/></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?= $this->element('Sections/recent-insights-subpage', ['recentInsights' => $relatedInsights]); ?>