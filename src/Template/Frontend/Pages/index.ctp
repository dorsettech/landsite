<?php
// layout: Homepage
/*
  {content.home_intro|default}
  {content.home_about|title_content_file}
  {content.home_help_tab_ampro|double_content}
  {content.home_help_tab_needpro|title_content_file}
  {content.home_help_title|title}
 */
?>

<?= $this->element('Sections/homepage-slider') ?>

<?php if (isset($contents->home_intro->content) && !empty($contents->home_intro->content)): ?>
    <div class="home-intro">
        <div class="container-fluid-slim">
            <div class="row">
                <div class="col-12 col-lg-7 col-xxl-8">
                    <div class="content box">
                        <?= $contents->home_intro->content ?>
                    </div>
                </div>
                <div class="col-12 col-lg-5 col-xxl-4 text-center">
                    <div class="sign-up">
                        <div class="sign-up-inner">
                            <h2 class="sign-up-title"><strong>Connect for free</strong></h2>
                            <p class="sign-up-sub">&hellip;and discover more today</p>
                            <?= $this->Html->link(__('Sign up today'), ['prefix' => 'members', 'controller' => '', 'action' => 'register'], ['class' => 'sign-up-btn btn btn-primary', 'escape' => false]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?= $this->element('Sections/thought-leadership', ['channels' => $channels]) ?>

<?= $this->element('Sections/pro-help', ['contents' => $contents]) ?>

<?php if (isset($contents->home_about->content) && !empty($contents->home_about->content)): ?>
    <?php if (isset($contents->home_about->file) && !empty($contents->home_about->file)): ?>
        <div class="home-about" style="background-image: url(<?= $this->Images->scale($contents->home_about->file, 1920, 1280) ?>);">
    <?php else: ?>
        <div class="home-about">
    <?php endif; ?>
        <div class="container-fluid-slim">
            <div class="row align-items-center justify-content-center">
                <div class="col-12 col-md-10 box">
                    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker&display=swap" rel="stylesheet">
                    <div class="content">
                        <?php if (isset($contents->home_about->title) && !empty($contents->home_about->title)): ?>
                            <h2><?= $contents->home_about->title ?></h2>
                        <?php endif; ?>
                        <?= $contents->home_about->content ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?= $this->element('Sections/recent-casestudies-home') ?>

<?= $this->element('Sections/recent-insights-home') ?>

<?= $this->element('Sections/testimonials') ?>

<?= $this->element('Sections/newsletter'); ?>
