<?php
// layout: Contact
/*
  {content.banner|file_content}
  {content.address_details}
 */
?>
<?php if (isset($contents->banner) && !empty($contents->banner)): ?>
    <?= $this->element('Sections/subpage-banner', ['content' => $contents->banner]) ?>
<?php endif; ?>

<section class="contact">
    <div class="container">
        <div class="contact-in">
            <div class="row">
                <div class="col-12 col-lg-8 pr-lg-0">
                    <?= $this->Captcha->replaceStart('contact'); ?>
                    <div class="form-row">
                        <div class="form-group col-12 col-sm-6 col-md-4">
                            {form.contact.field.first_name}
                        </div>
                        <div class="form-group col-12 col-sm-6 col-md-4">
                            {form.contact.field.surname}
                        </div>
                        <div class="form-group col-12 col-sm-6 col-md-4">
                            {form.contact.field.email}
                        </div>
                        <div class="form-group col-12 col-sm-6 col-md-4">
                            {form.contact.field.phone}
                        </div>
                        <div class="form-group col-12 col-sm-6 col-md-4">
                            {form.contact.field.postcode}
                        </div>
                        <div class="form-group col-12 col-sm-6 col-md-4">
                            {form.contact.field.company}
                        </div>
                        <div class="form-group col-12">
                            {form.contact.field.message}
                        </div>
                        <div class="col-auto ml-auto">
                            {form.contact.field.submit}
                        </div>
                    </div>
                    <?= $this->Captcha->formEnd(); ?>
                </div>
                <div class="col-12 col-lg pl-lg-0">
                    <div class="info">
                        <?php if (isset($contents->address_details->content) && !empty($contents->address_details->content)): ?>
                            <div class="data">
                                <?= $contents->address_details->content ?>
                            </div>
                        <?php endif; ?>
                        <div class="socials">
                            <?= $this->element('Sections/social-links') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?= $this->element('Sections/recent-insights-subpage'); ?>