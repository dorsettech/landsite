<?php
// layout: Subpage
/*
  {content.main}
  {content.sidebar}
 */
?>

<?= $this->element('Sections/professional-search') ?>

<?= $this->element('Sections/breadcrumbs') ?>

<section class="post">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-8 col-lg-9">
                <?php if (isset($contents->main->content) && !empty($contents->main->content)): ?>
                    <?= $contents->main->content ?>
                <?php endif; ?>
            </div>
            <div class="sidebar col-12 col-md-4 col-lg-3">
                <?php if (isset($statics['get_your_business']) && !empty($statics['get_your_business'])): ?>
                    <?= $statics['get_your_business'] ?>
                <?php endif; ?>
                <hr>
                <?php if (isset($contents->sidebar->content) && !empty($contents->sidebar->content)): ?>
                    <?= $contents->sidebar->content ?>
                <?php endif; ?>
                <?php if (isset($statics['sidebar_ads']) && !empty($statics['sidebar_ads'])): ?>
                    <div class="form-row justify-content-center">
                        <?= $statics['sidebar_ads'] ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?= $this->element('Sections/recent-insights-subpage') ?>
