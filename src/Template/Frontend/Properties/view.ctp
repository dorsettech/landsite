<section class="properties">
    <div class="container">
        <div class="row">
            <div class="details col-12 col-xl-7" itemscope itemtype="https://schema.org/Residence">
                <div class="d-none">
                    <span itemprop="url"><?= $this->Url->build('/', true).$this->request->url ?></span>
                    <span itemprop="image"><?= $this->Url->build('/', true).$property->main_image ?></span>
                </div>
                <div class="row">
                    <div class=" col-12 col-md">
                        <div class="heading" itemprop="name"><?= $property->title ?></div>
                        <span class="location" itemprop="address"><?= $property->location ?></span>
                        <?php if (!empty($property->reference)): ?>
                            | Ref: <span class="reference"><?= $property->reference ?></span>
                        <?php endif; ?>
                        <?php if (!empty($property->property_attributes)): ?>
                            <div class="icons">
                                <?php foreach ($property->property_attributes as $attribute): ?>
                                    <?php if ($attribute->_joinData->attribute_value == true) : ?>
                                        <i class="customicons icon1" data-toggle="tooltip" data-placement="top" title="<?= $attribute->name ?>" style="background: url(<?= $attribute->image_path ?>);"></i>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="prices col-12 col-md-auto">
                        <?= $this->Properties->price($property) ?>
                    </div>
                </div>
                <ul class="nav">
                    <li><button href="#detail-photos" class="active" data-toggle="tab" role="tab"><i class="fas fa-camera"></i><span>Photos</button></li>
                    <li><button href="#detail-map" class="" data-toggle="tab" role="tab"><i class="fas fa-map-marker-alt"></i><span>Map</span></button></li>
                    <?php if (!empty($property->lat) && !empty($property->lng)): ?>
                        <li id="street-view-li"><button href="#detail-streetview" class="detail-streetview" data-toggle="tab" role="tab"><i class="fas fa-street-view"></i><span>Streetview</span></button></li>
                    <?php endif; ?>
                    <li><button href="#detail-description" class="scrollto"><i class="far fa-file-alt"></i><span>Description</span></button></li>
                </ul>

                <div id="pills-tabContent" class="tab-content">
                    <div id="detail-photos" class="tab-pane fade show active" role="tabpanel" aria-labelledby="pills-home-tab">
                        <?= $this->element('Properties/media-carousel') ?>
                    </div>
                    <div id="detail-map" class="tab-pane fade" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <?= $this->element('Properties/map') ?>
                    </div>
                    <div id="detail-streetview" class="tab-pane fade" role="tabpanel" aria-labelledby="pills-contact-tab">
                        <?= $this->element('Properties/street-view') ?>
                    </div>
                </div>

                <div class="share">
                    <div class="heading">Share this plot</div>
                    <div class="icons">
                        <?= $this->Html->link('<i class="far fa-envelope"></i>', 'mailto:?&subject=I wanted you to see this property on The Landsite&body=Check%20out%20this%20site%20'.$this->Url->build('/', true).$this->request->url, ['escape' => false]) ?>
                        <?= $this->Html->link('<i class="fab fa-facebook-square"></i>', 'https://www.facebook.com/sharer/sharer.php?u='.$this->Url->build('/', true).$this->request->url, ['escape' => false, 'target' => '_blank']) ?>
                        <?= $this->Html->link('<i class="fab fa-twitter-square"></i>', 'https://twitter.com/home?status='.$this->Url->build('/', true).$this->request->url, ['escape' => false, 'target' => '_blank']) ?>
                        <?= $this->Html->link('<i class="fab fa-linkedin"></i>', 'https://www.linkedin.com/shareArticle?mini=true&url='.$this->Url->build('/', true).$this->request->url, ['escape' => false, 'target' => '_blank']) ?>
                    </div>
                </div>

                <div id="detail-description" itemprop="description">
                    <?php if ($property->auction): ?>
                        <span class="auction">AUCTION PROPERTY</span>
                    <?php endif; ?>
                    <?php if (!empty($property->common_use_classes_names)): ?>
                        <p class="common-use-class"><span>COMMON USE CLASS:</span> <?= $property->common_use_classes_names ?></p>
                    <?php endif; ?>
                    <?= $property->description ?>
                    <?php if (!empty($property->documents)): ?>
                        <ul class="list-unstyled">
                            <?php foreach ($property->documents as $document): ?>
                                <li>
                                    <a target="_blank" download href="<?= $document->source_path ?>"><?= $document->extra ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
                <div class="bottom">
                    <?php if (!empty($this->request->getSession()->read('Auth.User'))): ?>
                        <button data-toggle="modal" data-target="#propertymodal">
                            <i class="fas fa-envelope"></i> <?= __('Enquire about this property today!') ?>
                            <?php if (!empty($statics['property_enquire_extra_label'])): ?>
                                <span><?= $statics['property_enquire_extra_label'] ?></span>
                            <?php endif; ?>
                        </button>
                    <?php else: ?>
                        <?= $this->Html->link('Log in or register to contact today!', ['prefix' => 'members', 'controller' => '', 'action' => 'login', '?' => ['redirectTo' => $this->Url->build('/', true).$this->request->url]]) ?>
                    <?php endif; ?>
                    <?= $this->element('Sections/call-to-action-boxes') ?>
                </div>
            </div>
            <div class="col-12 col-xl-5">
                <div class="sidebar shadow-none">
                    <div class="top">
                        <?php if (!empty($this->request->getSession()->read('Auth.User'))): ?>
                            <button class="d-none d-xl-block" data-toggle="modal" data-target="#propertymodal"><i class="fas fa-envelope"></i> <?= __('Enquire about this property today!') ?> </button>
                        <?php else: ?>
                            <?= $this->Html->link('Log in or register to contact today!', ['prefix' => 'members', 'controller' => '', 'action' => 'login', '?' => ['redirectTo' => $this->Url->build('/', true).$this->request->url]]) ?>
                        <?php endif; ?>
                    </div>
                    <?php if ($property->has('user') && $property->user->has('service')): ?>
                        <div class="companyinfo">
                            
                            <div class="markedby mr-auto">
                                <a href="<?= $this->Url->build(['_name' => 'professional-services-individual', $property->user->service->urlname]) ?>" class="img">
                                    <img src="<?= !empty($property->user->image) ? $property->user->image_path : '/img/placeholder.png' ?>" alt="<?= $property->user->service->slug ?>">
                                </a>
                                <div class="">
                                    <?= __('This property is marketed by:') ?><br>
                                    <?= $this->Html->link($property->user->service->company, ['_name' => 'professional-services-individual', $property->user->service->urlname]) ?>
                                </div>
                            </div>
                            <?php if (!empty($this->request->getSession()->read('Auth.User'))): ?>
                                <a href="#" class="contact" data-toggle="modal" data-target="#servicesmodal">
                                    <i class="far fa-envelope"></i>
                                    <p>Contact<br>company</p>
                                </a>
                            <?php else: ?>
                                <?= $this->Html->link('<i class="far fa-envelope"></i><p>Contact<br>company</p>', ['prefix' => 'members', 'controller' => '', 'action' => 'login', '?' => ['redirectTo' => $this->Url->build('/', true).$this->request->url]], ['class' => 'contact ml-auto', 'escape' => false]) ?>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>
                <?= $this->element('Properties/services-sidebar') ?>
                <?php if (!empty($statics['properties_text_under_sidebar'])): ?>
                    <?= $statics['properties_text_under_sidebar'] ?>
                <?php endif; ?>
                <?php if (isset($statics['properties_sidebar_ads']) && !empty($statics['properties_sidebar_ads'])): ?>
                    <div class="form-row justify-content-center">
                        <?= $statics['properties_sidebar_ads'] ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php if (!empty($this->request->getSession()->read('Auth.User'))): ?>
    <?= $this->element('Modals/property-enquiry') ?>
    <?php if ($property->has('user') && $property->user->has('service')): ?>
        <?= $this->element('Modals/professional-service-enquiry', ['service' => $property->user->service]); ?>
    <?php endif; ?>
<?php endif; ?>
