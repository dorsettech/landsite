<section class="properties">
    <div class="container">
        <div class="row">
            <div class="col-12 col-xl-7">
                <?php if (isset($statics['properties_listing_intro']) && !empty($statics['properties_listing_intro'])): ?>
                    <div class="intro">
                        <?= $statics['properties_listing_intro'] ?>
                    </div>
                <?php endif; ?>
                <div class="adverts">
                    <?= $this->element('Properties/ListingAds/ad1') ?>
                </div>
                <?php if ($properties->count() > 0): ?>
                    <?= $this->cell('PropertiesSort') ?>
                    <?php $propertiesCounter = 0; ?>
                    <?php $adsCounter        = 1; ?>
                    <?php foreach ($properties as $property): ?>
                        <?php $propertiesCounter++; ?>
                        <?= $this->element('Boxes/property-box', ['property' => $property]) ?>
                        <?php if ($propertiesCounter % 3 === 0): ?>
                            <?php if ($adsCounter < 5): ?>
                                <?php $adsCounter++; ?>
                            <?php else: ?>
                                <?php $adsCounter = 1; ?>
                            <?php endif; ?>
                            <div class="d-md-block adverts">
                                <?= $this->element('Properties/ListingAds/ad'.$adsCounter) ?>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <?= $this->element('Sections/pagination') ?>
                <?php else: ?>
                    <?= $this->element('Properties/no-items') ?>
                <?php endif; ?>
            </div>
            <div class="col-12 col-xl-5">
                <?= $this->element('Properties/services-sidebar') ?>
                <?= $this->element('Properties/sidebar-ads') ?>
            </div>
        </div>
    </div>
</section>
<?php if (!empty($this->request->getSession()->read('Auth.User'))): ?>
    <?= $this->element('Modals/professional-service-enquiry-clear'); ?>
<?php endif; ?>
