<section class="posts listing">
    <div class="container">
        <?php if (isset($contents->main->content) && !empty($contents->main->content)): ?>
            <div class="row head">
                <div class="col-12">
                    <?= $contents->main->content ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="row list">
            <?php if (isset($articles) && !empty($articles)): ?>
                <?php $articlesCounter = 0; ?>
                <?php foreach ($articles as $article): ?>
                    <?php $articlesCounter++; ?>
                    <div class="col-12 col-sm-6 col-lg-4">
                        <?= $this->element('Boxes/insight-box', ['article' => $article]) ?>
                    </div>
                    <?php if ($articlesCounter === 6 || $articles->count() === $articlesCounter): ?>
                        <div class="adverts col-12">
                            <?= $this->element('Articles/ad') ?>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php else: ?>
                <?= $this->element('Sections/no-items') ?>
            <?php endif; ?>
        </div>
        <div>
            <?= $this->element('Sections/pagination') ?>
        </div>
    </div>
</section>
