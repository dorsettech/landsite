<?php

use App\Model\Entity\Group;
?>
<section class="post">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-8 col-lg-9">
                <div itemscope itemtype="http://schema.org/Article">
                    <div class="d-none">
                        <span itemprop="url"><?= $this->Url->build('/', true).$this->request->url ?></span>
                        <?php if (!empty($article->publish_date)): ?>
                            <span itemprop="datePublished"><?= $article->publish_date->format('d-m-Y') ?></span>
                        <?php endif; ?>
                        <span itemprop="image"><?= $this->Url->build('/', true).$article->image_path ?></span>
                        <span itemprop="wordCount"><?= str_word_count(strip_tags($article->body)) ?></span>
                    </div>
                    <h1 itemprop="name headline"><?= $article->title ?></h1>
                    <p itemscope itemprop="publisher" itemtype="http://schema.org/Organization">
                        <i class="date">Published by <span itemprop="name"><?= $this->Page->articlePostedBy($article, ['link' => true]) ?></span>
                            <?php if (!empty($article->publish_date)): ?>
                                on <?= $article->publish_date->format('jS F Y') ?>
                            <?php endif; ?>
                        </i>
                        <span class="d-none" itemprop="logo">-</span>
                    </p>
                    <?php if (!empty($article->image)) : ?><p><img src="<?= $this->Images->scale($article->image_path, 1140, 511) ?>" class="img-fluid" alt="title"></p><?php endif; ?>
                    <p itemprop="articleBody">
                        <?= $article->body ?>
                    </p>
                    <?php if (!empty($article->user) && $article->user->group_id === Group::MEMBERS): ?>
                        <?= $this->element('Articles/author-info', ['author' => $article->user]) ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="sidebar col-12 col-md-4 col-lg-3">
                <?php if (isset($statics['want_to_contribute']) && !empty($statics['want_to_contribute'])): ?>
                    <hr>
                    <?= $statics['want_to_contribute'] ?>
                <?php endif; ?>
                <hr>
                <div class="box">
                    <div class="heading">Related Services Categories</div>
                    <ul class="list-unstyled columns">
                        <li>Architects</li>
                        <li>Builders</li>
                        <li>Estate Agents</li>
                        <li>Property Management</li>
                        <li>Planning Consultants</li>
                        <li>Solicitors</li>
                    </ul>
                </div>
                <?php if (isset($statics['get_your_business']) && !empty($statics['get_your_business'])): ?>
                    <hr>
                    <?= $statics['get_your_business'] ?>
                <?php endif; ?>
                <hr>
                <?php if (isset($statics['sidebar_ads']) && !empty($statics['sidebar_ads'])): ?>
                    <div class="form-row justify-content-center">
                        <?= $statics['sidebar_ads'] ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?= $this->element('Sections/recent-insights-subpage', ['recentInsights' => $relatedInsights]); ?>