<?php if (isset($channel->image) && !empty($channel->image)): ?>
    <section class="titlebanner image" style="background-image: url('<?= $this->Images->scale($channel->image_path, 1920, 510) ?>');">
<?php else: ?>
    <section class="titlebanner">
<?php endif; ?>
    <div class="titlebanner-content">
        <h1 class="titlebanner-title"><?= $channel->name ?></h1>
    </div>
</section>

<?php if (isset($channel->sponsors) && !empty($channel->sponsors)): ?>
    <div class="channel sponsors">
        <div class="container-fluid-slim">
            <h2 class="sponsors-header">Sponsored by</h2>
            <div class="sponsors-content">
                <?= $channel->sponsors ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="channel view">
    <div class="container-fluid-slim">
        <div class="view-header">
            <h1 class="view-title"><?= $channel->name ?></h1>
            <h2 class="view-sub">on The Landsite</h2>
        </div>
        <div class="row">
            <?php if (isset($channel->description) && !empty($channel->description)): ?>
                <?php if (isset($channel->description2) && !empty($channel->description2)): ?>
                    <div class="col-sm-6">
                <?php else : ?>
                    <div class="col-sm-12">
                <?php endif; ?>
                    <div class="view-description">
                        <?= $channel->description ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (isset($channel->description2) && !empty($channel->description2)): ?>
                <div class="col-sm-6">
                    <div class="view-description">
                        <?= $channel->description2 ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <?php if (isset($channel->advert1) && !empty($channel->advert1)): ?>
        <div class="channel advert">
            <?= $channel->advert1 ?>
        </div>
    <?php endif; ?>
    <div class="container-fluid-slim">
        <div class="view-signup">
            <h2 class="view-signup-title"><strong>Connect for free</strong></h2>
            <p class="view-signup-sub">&hellip;and discover more today</p>
            <?= $this->Html->link(__('Sign up today'), ['prefix' => 'members', 'controller' => '', 'action' => 'register'], ['class' => 'view-signup-btn btn btn-ghost-primary', 'escape' => false]) ?>
        </div>
    </div>
</div>

<?php if (isset($insights) && !empty($insights)): ?>
    <section class="channel articles bgcolor">
        <div class="container-fluid-slim">
            <div class="articles-header">
                <h3 class="articles-title"><?= __('Latest News') ?></h3>
                <p class="articles-sub">
                    <?= __('Latest news related to ') ?><strong><?= $channel->name ?></strong>
                </p>
            </div>
            <div class="carousel-overflow">
                <div class="row carousel">
                    <?php foreach ($insights as $insight): ?>
                        <div class="col-12">
                            <?= $this->element('Boxes/insight-box', ['article' => $insight]) ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php if (isset($channel->advert2) && !empty($channel->advert2)): ?>
                <div class="channel advert">
                    <?= $channel->advert2 ?>
                </div>
            <?php endif; ?>
        </div>
    </section>
<?php endif; ?>

<?php if (isset($caseStudies) && !empty($caseStudies)): ?>
    <section class="channel articles bgwhite">
        <div class="container-fluid-slim">
            <div class="articles-header">
                <h3 class="articles-title"><?= __('Case Studies') ?></h3>
                <p class="articles-sub">
                    <?= __('Case Studies related to ') ?>
                    <strong><?= $channel->name ?></strong>
                </p>
            </div>
            <div class="carousel-overflow">
                <div class="row carousel">
                    <?php foreach ($caseStudies as $caseStudy): ?>
                        <div class="col-12">
                            <?= $this->element('Boxes/case-study-box', ['caseStudy' => $caseStudy]) ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php if (isset($channel->advert3) && !empty($channel->advert3)): ?>
                <div class="channel advert">
                    <?= $channel->advert3 ?>
                </div>
            <?php endif; ?>
        </div>
    </section>
<?php endif; ?>

<section class="channel forumlatest bgcolor">
    <div class="container-fluid-slim">
        <div class="forumlatest-box">
            <div class="forumlatest-header">
                <h3 class="forumlatest-title"><?= __('Join us on The Landsite forums') ?></h3>
                <p class="forumlatest-sub">Go to the forum to engage and share your expertise.</p>
            </div>
            <div class="forumlatest-action">
                <?= $this->Html->link(__('Go to the forums').' <i class="fas fa-arrow-right"></i>', ['prefix' => 'members', 'controller' => 'Dashboard', 'action' => 'forumredirect'], ['class' => 'btn btn-large btn-primary', 'escape' => false]) ?>
            </div>
        </div>
    </div>
</section>
