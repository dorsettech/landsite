<?php if (isset($page->slider) && !empty($page->slider) && isset($page->slider[0]['path'])): ?>
    <section class="titlebanner image" style="background-image: url('<?= $this->Images->scale($page->slider[0]['path'], 1920, 480) ?>');">
<?php else: ?>
    <section class="titlebanner">
<?php endif; ?>
    <div class="titlebanner-content">
        <h1 class="titlebanner-title"><?= $page->name ?? __('All Channels') ?></h1>
    </div>
</section>

<?php if (isset($contents->main->content) && !empty($contents->main->content)): ?>
    <section class="channel index mt-3">
        <div class="container-fluid-slim">
            <div class="row justify-content-center">
                <div class="col-12 col-sm-8">
                    <?= $contents->main->content ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<section class="channel index">
    <div class="container-fluid-slim">
        <div class="row list">
            <?php if (isset($channels) && !empty($channels)): ?>
                <?php foreach ($channels as $channel): ?>
                    <div class="col-12 col-sm-6 col-lg-3 mb-4">
                        <?= $this->element('Boxes/channel-box', ['channel' => $channel]) ?>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <?= $this->element('Sections/no-items') ?>
            <?php endif; ?>
        </div>
        <div class="pager">
            <?= $this->element('Sections/pagination') ?>
        </div>
    </div>
</section>
