<?php echo '<?xml version="1.0" encoding="utf-8"?>' ?>
<?php if (!empty($files)): ?>
    <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
        <?php foreach ($files as $file): ?>
            <?= $this->Sitemap->generateMainSitemap($file) ?>
        <?php endforeach; ?>
    </sitemapindex>
<?php endif; ?>