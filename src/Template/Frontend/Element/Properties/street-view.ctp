<?php if (!empty($property->lat) && !empty($property->lng)): ?>
    <script>
        var streetViewService = new google.maps.StreetViewService();
        var STREETVIEW_MAX_DISTANCE = 50;
        var latLng = new google.maps.LatLng(<?= $property->lat ?>, <?= $property->lng ?>);
        streetViewService.getPanoramaByLocation(latLng, STREETVIEW_MAX_DISTANCE, function (streetViewPanoramaData, status) {
            if (status !== google.maps.StreetViewStatus.OK) {
                $('#street-view-li').remove();
                $('#street-view').remove();
            }
        });
    </script>
    <div id="map-property"></div>
    <div id="street-view" style="min-height:400px"></div>
    <script>
        function initialize() {
            var fenway = {lat: <?= $property->lat ?>, lng: <?= $property->lng ?>};
            var map = new google.maps.Map(document.getElementById('map-property'), {
                center: fenway,
                zoom: 10
            });
            var panorama = new google.maps.StreetViewPanorama(
                    document.getElementById('street-view'), {
                position: fenway,
                pov: {
                    heading: 34,
                    pitch: 10
                }
            });
            map.setStreetView(panorama);
        }
        $(document).on('click', '.detail-streetview', initialize);
    </script>
<?php endif; ?>
