<?php if (!empty($property->gallery)): ?>
    <div class="carousel big lightbox">
        <?php foreach ($property->gallery as $image): ?>
            <div class="box">
                <div class="image">
                    <img src="<?= $image->source_path ?>" alt="title">
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="row small carousel">
        <?php foreach ($property->gallery as $image): ?>
            <div class="box col-12">
                <div class="image">
                    <img src="<?= $this->Images->scale($image->source_path, 215, 130) ?>" alt="title">
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>