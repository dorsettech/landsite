<div id="properties-sidebar1" class="sidebar sidebar1">
    <?php if (!empty($servicesGroupedByCategories)): ?>
        <div class="top">
            <div class="heading">
                <?= __('Featured Professionals') ?><br>
                <?= __('Select from the options below') ?>
            </div>
        </div>
        <?php foreach ($servicesGroupedByCategories as $rowKey => $row): ?>
            <div class="row">
                <?php $categoryCount = 0; ?>
                <?php foreach ($row['categories'] as $categoryKey => $category): ?>
                    <div class="control col-6 col-sm-3 order-0">
                        <div type="button" class="<?= $rowKey === 0 && $categoryCount === 0 ? '' : 'collapsed' ?>" data-toggle="collapse" data-target="#collapse<?= $rowKey.str_replace(' ', '-', $categoryKey) ?>">
                            <?php if (!empty($category->image)): ?>
                                <img src="<?= $this->Images->scale($category->image_path, 150, 100) ?>" alt="<?= $category->name ?>">
                            <?php endif; ?>
                            <span><?= $category->name ?></span>
                        </div>
                    </div>
                    <?php $categoryCount++; ?>
                <?php endforeach; ?>
                <?php $tabCount = 0; ?>
                <?php foreach ($row['services'] as $tabKey => $servicesTab): ?>
                    <div id="collapse<?= $rowKey.str_replace(' ', '-', $tabKey) ?>" class="col-12 order-0 collapse <?= $rowKey === 0 && $tabCount === 0 ? 'show' : '' ?>" data-parent="#properties-sidebar1">
                        <div class="content">
                            <div class="heading">Featured <?= $tabKey ?></div>
                            <ul class="list-unstyled">
                                <?php foreach ($servicesTab as $service): ?>
                                    <li>
                                        <?= $this->Html->link($service->company, ['_name' => 'professional-services-individual', $service->urlname]) ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                            <?php /*
                              <a href="">View all Architects in Wickham Bishops</a>
                             */ ?>
                        </div>
                    </div>
                    <?php $tabCount++; ?>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>