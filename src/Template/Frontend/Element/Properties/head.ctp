<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?php if (isset($metaTags['title']) && !empty($metaTags['title'])): ?>
                <?= $metaTags['title'] ?>
            <?php elseif (!empty($property->title) && (!empty($property->type_id)) && !empty($property->location)): ?>
                <?= $property->title.' | '.$property->property_type->name.' '.$property->location ?>
            <?php else: ?>
                <?= $_service; ?>
            <?php endif; ?>
        </title>
        <?php if (isset($metaTags['keywords']) && !empty($metaTags['keywords'])) : ?><meta name="keywords" content="<?= $metaTags['keywords'] ?>"><?php endif; ?>
        <?php if (isset($metaTags['description']) && !empty($metaTags['description'])) : ?>
            <meta name="description" content="<?= $metaTags['description'] ?>">
        <?php elseif (!empty($property->description)): ?>
            <meta name="description" content="<?= $this->Text->truncate(strip_tags($property->description), 165, ['ellipsis' => '...', 'exact' => false]) ?>">
        <?php endif; ?>
        <meta property="og:url" content="<?= $this->Url->build('/', true).substr($this->request->getPath(), 1) ?>">
        <meta property="og:type" content="website">
        <?php if (isset($property->title) && !empty($property->title)): ?>
            <meta property="og:title" content="<?= $property->title ?>">
            <meta name="twitter:title" content="<?= $property->title ?>">
        <?php elseif (isset($metaTags['og_title']) && !empty($metaTags['og_title'])) : ?>
            <meta property="og:title" content="<?= $metaTags['og_title'] ?>">
            <meta name="twitter:title" content="<?= $metaTags['og_title'] ?>">
        <?php endif; ?>
        <?php if (isset($property->main_image) && !empty($property->main_image)): ?>
            <meta property="og:image" content="<?= $this->Url->build('/', true).$property->main_image ?>">
            <meta name="twitter:image" content="<?= $this->Url->build('/', true).$property->main_image ?>">
        <?php elseif (isset($statics['og_image']) && !empty($statics['og_image'])) : ?>
            <meta property="og:image" content="<?= $this->Url->build('/', true).$statics['og_image'] ?>">
            <meta name="twitter:image" content="<?= $this->Url->build('/', true).$statics['og_image'] ?>">
        <?php endif; ?>
        <?php if (isset($property->description) && !empty($property->description)): ?>
            <meta property="og:description" content="<?= $this->Text->truncate(strip_tags($property->description), 255, ['ellipsis' => '...', 'exact' => false]) ?>">
            <meta name="twitter:description" content="<?= $this->Text->truncate(strip_tags($property->description), 255, ['ellipsis' => '...', 'exact' => false]) ?>">
        <?php elseif (isset($metaTags['og_description']) && !empty($metaTags['og_description'])) : ?>
            <meta property="og:description" content="<?= $metaTags['og_description'] ?>">
            <meta name="twitter:description" content="<?= $metaTags['og_description'] ?>">
        <?php endif; ?>
        <meta property="og:site_name" content="<?= $_service ?>">
        <?php if (isset($property->modified) && !empty($property->modified)): ?>
            <meta property="og:updated_time" content="<?= $property->modified->format('d-m-Y') ?>">
        <?php endif; ?>
        <meta property="og:locale" content="en_GB">
        <meta property="twitter:card" content="summary_large_image">
        <meta property="twitter:site" content="@thelandsite">
        <?= $this->element('head-scripts') ?>
    </head>
    <body>
        <?php if (!empty($statics['scripts_body_top'])) : ?>
            <?= $statics['scripts_body_top']; ?>
        <?php endif; ?>