<?php if ($this->request->getSession()->read('Auth.User.id') !== null): ?>
    <?php if ($property->is_saved !== true): ?>
        <?= $this->Form->create(null, ['class' => 'save-property', 'data-id' => $property->id]) ?>
        <?= $this->Form->conrol('user_id', ['type' => 'hidden', 'value' => $this->request->getSession()->read('Auth.User.id')]) ?>
        <?= $this->Form->conrol('property_id', ['type' => 'hidden', 'value' => $property->id]) ?>
        <button type="submit" class="saveproperty" id="save-button-<?= $property->id ?>">Save property</button>
        <span id="saved-info-<?= $property->id ?>" class="d-none">Saved</span>
        <?= $this->Form->end(); ?>
    <?php else: ?>
        <span id="saved-info-<?= $property->id ?>">Saved</span>
    <?php endif; ?>
<?php else: ?>
    <?= $this->Html->link('Save property', ['prefix' => 'members', 'controller' => '', 'action' => 'login', '?' => ['redirectTo' => $this->Url->build('/', true).substr($this->request->getPath(), 1)]], ['class' => 'saveproperty', 'escape' => false]) ?>
<?php endif; ?>
