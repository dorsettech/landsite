<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?= (isset($metaTags['title']) && !empty($metaTags['title'])) ? $metaTags['title'] : $_service; ?></title>
        <meta property="og:title" content="<?= (isset($metaTags['og_title']) && !empty($metaTags['og_title'])) ? $metaTags['og_title'] : $_service; ?>">
        <meta property="og:site_name" content="<?= $_service ?>">
        <?php if (isset($metaTags['keywords']) && !empty($metaTags['keywords'])) : ?><meta name="keywords" content="<?= $metaTags['keywords'] ?>"><?php endif; ?>
        <?php if (isset($metaTags['description']) && !empty($metaTags['description'])) : ?><meta name="description" content="<?= $metaTags['description'] ?>"><?php endif; ?>
        <?php if (isset($metaTags['og_description']) && !empty($metaTags['og_description'])) : ?><meta property="og:description" content="<?= $metaTags['og_description'] ?>"><?php endif; ?>
        <?php if (isset($metaTags['og_image']) && !empty($metaTags['og_image'])) : ?>
            <meta property="og:image" content="<?= $this->Url->build('/', true).$metaTags['og_image'] ?>">
        <?php elseif (isset($statics['og_image']) && !empty($statics['og_image'])) : ?>
            <meta property="og:image" content="<?= $this->Url->build('/', true).$statics['og_image'] ?>">
        <?php endif; ?>
        <meta property="og:locale" content="en_GB">
        <?= $this->element('head-scripts') ?>
    </head>
    <body>
        <?php if (!empty($statics['scripts_body_top'])) : ?>
            <?= $statics['scripts_body_top']; ?>
        <?php endif; ?>