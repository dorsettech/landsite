<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td align="center">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <?php if (!empty($this->Page->statics('social_facebook'))): ?>
                        <td>
                            <a href="<?= $this->Page->statics('social_facebook') ?>">
                                <img src="<?= $this->Url->build('/', true); ?>img/email/facebook.png" alt="Facebook" width="35" height="35" style="display: block;" border="0" />
                            </a>
                        </td>
                    <?php endif; ?>
                    <td style="font-size: 0; line-height: 0;" width="50">&nbsp;</td>
                    <?php if (!empty($this->Page->statics('social_twitter'))): ?>
                        <td>
                            <a href="<?= $this->Page->statics('social_twitter') ?>">
                                <img src="<?= $this->Url->build('/', true); ?>img/email/twitter.png" alt="Twitter" width="35" height="35" style="display: block;" border="0" />
                            </a>
                        </td>
                    <?php endif; ?>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 30px 0 0 0;">
    <tr>
        <td align="center" style="color: #ffffff; font-family: 'Raleway', sans-serif;">
            <a href="<?= $this->Url->build('/privacy-policy', true); ?>" style="color: #ffffff; font-size: 14px;">Privacy Policy</a>
            |
            <a href="<?= $this->Url->build('/', true); ?>" style="color: #ffffff; font-size: 14px;">Unsubscribe</a>
        </td>
    </tr>
</table>