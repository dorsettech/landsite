<div id="properties-sidebar2" class="sidebar sidebar2">
    <?php if (isset($recentProperties) && !empty($recentProperties)): ?>
        <div class="top">
            <?php if (isset($searchData['location']) && !empty($searchData['location'])): ?>
                <div class="heading"><?= __('Properties in').' '.$searchData['location'] ?></div>
            <?php else: ?>
                <div class="heading"><?= __('Featured Properties') ?></div>
            <?php endif; ?>
        </div>
        <div class="carousel-overflow">
            <div class="row carousel">
                <?php foreach ($recentProperties as $property): ?>
                    <?= $this->element('Boxes/property-sidebar-box', ['property' => $property]) ?>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="moreproperties">
            <?php if (isset($searchData['location']) && !empty($searchData['location'])): ?>
                <?= $this->Html->link('For more properties in '.$searchData['location'].' click here', ['_name' => 'properties-listing', '?' => ['location' => $searchData['location']]]) ?>
            <?php else: ?>
                <?= $this->Html->link('Click here to see more properties', ['_name' => 'properties-listing']) ?>
            <?php endif; ?>
        </div>
    <?php endif; ?>
</div>