<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?php if (!empty($categoryNameForSeoTitle)): ?>
                <?= $categoryNameForSeoTitle.' | Find Professional Property Services' ?>
            <?php elseif (isset($metaTags['title']) && !empty($metaTags['title'])): ?>
                <?= $metaTags['title'] ?>
            <?php elseif (!empty($service->company) && (!empty($service->category_id))): ?>
                <?= $service->company.' | '.$service->category->name ?>
            <?php else: ?>
                <?= $_service; ?>
            <?php endif; ?>
        </title>

        <?php if (isset($metaTags['keywords']) && !empty($metaTags['keywords'])) : ?>
            <meta name="keywords" content="<?= $metaTags['keywords'] ?>">
        <?php endif; ?>
        <?php if (!empty($categoryNameForSeoTitle)) : ?>
            <meta name="description" content="Search our online database for professional property services such as <?= $categoryNameForSeoTitle ?> in your area and engage with them to aid your commercial property transaction.">
        <?php elseif (isset($metaTags['description']) && !empty($metaTags['description'])) : ?>
            <meta name="description" content="<?= $metaTags['description'] ?>">
        <?php elseif (!empty($service->description)): ?>
            <meta name="description" content="<?= $this->Text->truncate(strip_tags($service->description), 165, ['ellipsis' => '...', 'exact' => false]) ?>">
        <?php endif; ?>
        <meta property="og:url" content="<?= $this->Url->build('/', true).substr($this->request->getPath(), 1) ?>">
        <meta property="og:type" content="business.business">
        <?php if (isset($service->company) && !empty($service->company)): ?>
            <meta property="og:title" content="<?= $service->company ?>">
            <meta name="twitter:title" content="<?= $service->company ?>">
        <?php elseif (isset($metaTags['og_title']) && !empty($metaTags['og_title'])) : ?>
            <meta property="og:title" content="<?= $metaTags['og_title'] ?>">
            <meta name="twitter:title" content="<?= $metaTags['og_title'] ?>">
        <?php endif; ?>
        <?php if (isset($service->lat) && !empty($service->lat)): ?>
            <meta property="place:location:latitude" content="<?= $service->lat ?>">
        <?php endif; ?>
        <?php if (isset($service->lng) && !empty($service->lng)): ?>
            <meta property="place:location:longitude" content="<?= $service->lng ?>">
        <?php endif; ?>
        <?php if (isset($service->location) && !empty($service->location)): ?>
            <meta property="business:contact_data:street_address" content="<?= $service->location ?>">
            <meta property="business:contact_data:country" content="UK">
        <?php endif; ?>
        <?php if (isset($service->postcode) && !empty($service->postcode)): ?>
            <meta property="business:contact_data:postal_code" content="<?= $service->postcode ?>">
        <?php endif; ?>
        <?php if (isset($service->user->email) && !empty($service->user->email)): ?>
            <meta property="business:contact_data:email" content="<?= $service->user->email ?>">
        <?php endif; ?>
        <?php if (isset($service->user->phone) && !empty($service->user->phone)): ?>
            <meta property="business:contact_data:phone_number" content="<?= $service->user->phone ?>">
        <?php endif; ?>
        <?php if (isset($service->website_url) && !empty($service->website_url)): ?>
            <meta property="business:contact_data:website" content="<?= $service->website_url ?>">
        <?php endif; ?>
        <?php if (isset($service->opening_days_information) && !empty($service->opening_days_information)): ?>
            <meta property="business:hours:day" content="<?= $service->opening_days_information ?>">
        <?php endif; ?>
        <?php if (isset($service->opening_time_from) && !empty($service->opening_time_from)): ?>
            <meta property="business:hours:start" content="<?= $service->opening_time_from->format('g:ia') ?>">
        <?php endif; ?>
        <?php if (isset($service->opening_time_to) && !empty($service->opening_time_to)): ?>
            <meta property="business:hours:end" content="<?= $service->opening_time_to->format('g:ia') ?>">
        <?php endif; ?>
        <?php if (isset($service->description_short) && !empty($service->description_short)): ?>
            <meta property="og:description" content="<?= $service->description_short ?>">
            <meta name="twitter:description" content="<?= $service->description_short ?>">
        <?php elseif (isset($metaTags['og_description']) && !empty($metaTags['og_description'])) : ?>
            <meta property="og:description" content="<?= $metaTags['og_description'] ?>">
            <meta name="twitter:description" content="<?= $metaTags['og_description'] ?>">
        <?php endif; ?>
        <?php if (isset($service->user->image_path) && !empty($service->user->image_path)): ?>
            <meta property="og:image" content="<?= $this->Url->build('/', true).$service->user->image_path ?>">
            <meta name="twitter:image" content="<?= $this->Url->build('/', true).$service->user->image_path ?>">
        <?php elseif (isset($statics['og_image']) && !empty($statics['og_image'])) : ?>
            <meta property="og:image" content="<?= $this->Url->build('/', true).$statics['og_image'] ?>">
            <meta name="twitter:image" content="<?= $this->Url->build('/', true).$statics['og_image'] ?>">
        <?php endif; ?>
        <meta property="og:locale" content="en_GB">
        <meta property="twitter:card" content="summary">
        <meta property="twitter:site" content="@thelandsite">
        <?= $this->element('head-scripts') ?>
    </head>
    <body>
        <?php if (!empty($statics['scripts_body_top'])) : ?>
            <?= $statics['scripts_body_top']; ?>
        <?php endif; ?>