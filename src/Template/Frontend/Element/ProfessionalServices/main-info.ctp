<div class="top">
    <div class="image">
        <img src="<?= !empty($service->user->image) ? $service->user->image_path : '/img/placeholder.png' ?>" alt="<?= $service->company ?>">
    </div>
    <div>
        <div class="heading" itemprop="name">
            <?= $service->company ?>
            <?php if (!empty($service->phone)): ?>
                <a href="tel:<?= $service->phone_number ?>" data-service-id="<?= $service->id ?>" data-element-id="1" class="service-clicks phone"><i class="fas fa-phone"></i> <span><?= $service->phone ?></span></a>
            <?php endif; ?>
        </div>
        <div class="dsc"><?= $service->description_short ?></div>
    </div>
</div>

<div class="middle">
    <div class="buttons">
        <?php if (!empty($this->request->getSession()->read('Auth.User'))): ?>
            <?= $this->Html->link('Enquire <i class="fas fa-envelope"></i>', '#', ['class' => 'btn btn-primary', 'escape' => false, 'data-toggle' => 'modal', 'data-target' => '#servicesmodal']) ?>
        <?php else: ?>
            <?= $this->Html->link('Enquire <i class="fas fa-envelope"></i>', ['prefix' => 'members', 'controller' => '', 'action' => 'login', '?' => ['redirectTo' => $this->Url->build('/', true) . $this->request->url]], ['class' => 'btn btn-primary', 'escape' => false]) ?>
        <?php endif; ?>
        <?php if (isset($service->website_url) && !empty($service->website_url)): ?>
            <?= $this->Html->link('Website <i class="fas fa-link"></i>', $service->website_url, ['class' => 'btn btn-secondary service-clicks', 'escape' => false, 'target' => '_blank', 'data-element-id' => 0, 'data-service-id' => $service->id]) ?>
        <?php endif; ?>
    </div>

    <?= $this->element('ProfessionalServices/socials') ?>

    <?php if (!empty($this->request->getSession()->read('Auth.User'))): ?>
        <?php if ($isSaved === false): ?>
            <?= $this->Form->create(null, ['class' => 'save-professional']) ?>
            <?= $this->Form->conrol('user_id', ['type' => 'hidden', 'value' => $this->request->getSession()->read('Auth.User.id')]) ?>
            <?= $this->Form->conrol('service_id', ['type' => 'hidden', 'value' => $service->id]) ?>
            <button type="submit" class="save"><i class="fas fa-heart"></i>save <span>professional</span></button>
            <?= $this->Form->end(); ?>
        <?php endif; ?>
    <?php else: ?>
        <?= $this->Html->link('<i class="fas fa-heart"></i>save <span>professional</span>', ['prefix' => 'members', 'controller' => '', 'action' => 'login', '?' => ['redirectTo' => $this->Url->build('/', true) . substr($this->request->getPath(), 1)]], ['class' => 'save', 'escape' => false]) ?>
    <?php endif; ?>

</div>
<div class="row bottom">
    <?php if (isset($service->quick_win_1) && !empty($service->quick_win_1)): ?>
        <div class="col-12 col-sm">
            <div class="box">
                <?= $service->quick_win_1 ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (isset($service->quick_win_2) && !empty($service->quick_win_2)): ?>
        <div class="col-12 col-sm">
            <div class="box">
                <?= $service->quick_win_2 ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (isset($service->quick_win_3) && !empty($service->quick_win_3)): ?>
        <div class="col-12 col-sm">
            <div class="box">
                <?= $service->quick_win_3 ?>
            </div>
        </div>
    <?php endif; ?>
</div>
