<p>
    <?= __("We're sorry, there are currently no professionals on our portal that match your search criteria. Our community is continuously growing though, so please bear with us and check back soon to find what you're looking for!") ?>
</p>
<p>
    <?= __("You can try to extend the search range and find results close to your chosen location or you can explore the rest of our site...") ?>
</p>

<?= $this->element('Sections/recent-insights-white', ['title' => 'News & Information']) ?>

