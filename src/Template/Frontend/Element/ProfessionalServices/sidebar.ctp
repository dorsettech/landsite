<div class="sidebar sidebar1">
    <?php if ($service->properties_count > 0) : ?>
        <?= $this->Html->link('<i class="customicons icon5"></i> <span>' . __('See Uploaded Properties') . '</span>', ['_name' => 'service-properties', $service->id, $service->slug], ['class' => 'button', 'escape' => false]) ?>
    <?php endif; ?>
    <?php if (!empty($service->opening_hours)): ?>
        <div class="box">
            <div class="heading"><?= __('Opening Hours') ?></div>
            <p><?= __('Opening hours are') ?>:</p>
            <div class="row">
                <?php foreach ($service->opening_hours_list as $day) : ?>
                <div class="col-6">
                    <?= $day['day_name'] ?>
                </div>
                <div class="col-6">
                    <span itemprop="openingHours"><?= $day['from']->format('g.ia') . '-' . $day['to']->format('g.ia')?></span>
                </div>
                <?php endforeach; ?>
            </div>
    </div>
    <?php endif; ?>
    <?php if (isset($service->areas) && !empty($service->areas)): ?>
        <hr>
        <div class="box">
            <div class="heading"><?= __('Areas We Cover') ?></div>
            <ul class="list-unstyled columns">
                <?php foreach ($service->areas as $area): ?>
                    <li><?= $area->name ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
</div>
<?= $this->element('ProfessionalServices/sidebar-content'); ?>