<div id="map"></div>
<script>
    // Snazzy Maps footer
    google.maps.event.addDomListener(window, 'load', init);
    function init() {
        // opcje mapy
        var mapOptions = {
            zoom: 14,
            mapTypeControl: false, // map type
            streetViewControl: false, // street view
            fullscreenControl: false, // full screen control
            disableDoubleClickZoom: true, // zoom on double click
            center: new google.maps.LatLng(<?= $service->lat ?>, <?= $service->lng ?>), // współrzędne wyśrodkowana
            styles: [],
        };
        // pobranie mapy do zmiennej
        var mapElement = document.getElementById('map');
        // Utworzenie mapy Google używając elementu #map i opcji zdefiniowanych w tablicy
        var map = new google.maps.Map(mapElement, mapOptions);
        // dodanie znacznika
        var marker1 = new google.maps.Marker({
            position: map.getCenter(),
            icon: {
                path: 'M11 2c-3.9 0-7 3.1-7 7 0 5.3 7 13 7 13 0 0 7-7.7 7-13 0-3.9-3.1-7-7-7Zm0 9.5c-1.4 0-2.5-1.1-2.5-2.5 0-1.4 1.1-2.5 2.5-2.5 1.4 0 2.5 1.1 2.5 2.5 0 1.4-1.1 2.5-2.5 2.5Z',
                scale: 2.2727272727272727272727272727,
                anchor: new google.maps.Point(11, 22),
                fillOpacity: 1,
                fillColor: '#e52338',
                strokeOpacity: 0
            },
            map: map,
        });
    }
</script>