<div class="sidebar sidebar2">
    <?php if (isset($statics['get_your_business']) && !empty($statics['get_your_business'])): ?>
        <?= $statics['get_your_business'] ?>
    <?php endif; ?>
    <hr>
    <?php if (isset($otherServices) && !empty($otherServices)): ?>
        <div class="box">
            <div class="heading"><?= __('Other Listings') ?></div>
            <ul class="list-unstyled">
                <?php foreach ($otherServices as $other): ?>
                    <li><?= $this->Html->link($other->company, ['_name' => 'professional-services-individual', $other->urlname]) ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <hr>
    <?php endif; ?>
    <div class="box">
        <div class="heading"><?= __('Related Services Categories') ?></div>
        <ul class="list-unstyled">
            <li>Architects</li>
            <li>Builders</li>
            <li>Estate Agents</li>
            <li>Property Management</li>
            <li>Planning Consultants</li>
            <li>Solicitors</li>
        </ul>
    </div>
    <hr>
    <?php if (isset($statics['sidebar_ads']) && !empty($statics['sidebar_ads'])): ?>
        <div class="form-row justify-content-center">
            <?= $statics['sidebar_ads'] ?>
        </div>
    <?php endif; ?>
</div>