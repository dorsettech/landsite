<div class="socials">
    <?php if (isset($service->facebook_url) && !empty($service->facebook_url)): ?>
        <?= $this->Html->link('<i class="fab fa-facebook-square"></i>', $service->facebook_url, ['escape' => false, 'target' => '_blank']) ?>
    <?php endif; ?>
    <?php if (isset($service->twitter_url) && !empty($service->twitter_url)): ?>
        <?= $this->Html->link('<i class="fab fa-twitter-square"></i>', $service->twitter_url, ['escape' => false, 'target' => '_blank']) ?>
    <?php endif; ?>
    <?php if (isset($service->linkedin_url) && !empty($service->linkedin_url)): ?>
        <?= $this->Html->link('<i class="fab fa-linkedin"></i>', $service->linkedin_url, ['escape' => false, 'target' => '_blank']) ?>
    <?php endif; ?>
    <?php if (isset($service->google_url) && !empty($service->google_url)): ?>
        <?= $this->Html->link('<i class="fab fa-google-plus-square"></i>', $service->google_url, ['escape' => false, 'target' => '_blank']) ?>
    <?php endif; ?>
    <?php if (isset($service->hubspot_url) && !empty($service->hubspot_url)): ?>
        <?= $this->Html->link('<i class="fab fa-hubspot"></i>', $service->hubspot_url, ['escape' => false, 'target' => '_blank']) ?>
    <?php endif; ?>
</div>