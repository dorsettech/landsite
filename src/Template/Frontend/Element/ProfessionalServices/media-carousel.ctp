<?php if (isset($service->images) && !empty($service->images)): ?>
    <div id="profile-4">
        <div class="heading"><?= __('Images') ?></div>
        <div class="row posts carousel lightbox">
            <?php foreach ($service->images as $image): ?>
                <div class="col-12">
                    <div class="box">
                        <div class="image">
                            <img src="<?= $image->source_path ?>" alt="<?= strip_tags($image->extra) ?>">
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>