<?php

use Cake\Core\Configure;
use Cake\Error\Debugger;
use Cake\ORM\TableRegistry;

// <Debug: True>
if (Configure::read('debug') && isset($error)) {
    $this->layout = 'dev_error';
    $this->assign('title', $message);
    $this->start('file');
    ?>

    <div class="error-subheading">
        <strong>Error: </strong><?= h($error->getMessage()) ?><br />
        <strong>Code: </strong><?= h($error->getCode()) ?><br />
        <strong>File: </strong><?= h($error->getFile()) ?><br />
        <strong>Line: </strong><?= h($error->getLine()) ?><br />
    </div>

    <?php if (!empty($error->queryString)) { ?>
        <p class="notice">
            <strong>SQL Query: </strong>
            <?= h($error->queryString) ?>
        </p>
    <?php } ?>

    <?php if (!empty($error->params)) { ?>
        <strong>SQL Query Params: </strong>
        <?= Debugger::dump($error->params) ?>
    <?php } ?>

    <?php
    echo $this->element('auto_table_warning');

    if (extension_loaded('xdebug')) {
        xdebug_print_function_stack();
    }

    $this->end();
}
// </Debug: True>

$pagesReg = TableRegistry::get('Pages');
$code     = !isset($code) || $code === null ? $this->response->statusCode() : $code;
$message  = !isset($message) || $message === null ? $this->response->httpCodes($code)[$code] : $message;
$url      = !isset($url) || $url === null ? $this->request->url : $url;
switch ($code) {
    case 300:
        $finalmessage = 'The user can select a link and go to that location';
        break;
    case 301:
        $finalmessage = 'The requested page has moved to a new URL';
        break;
    case 302: case 307:
        $finalmessage = 'The requested page has moved temporarily to a new URL';
        break;
    case 303:
        $finalmessage = 'The requested page can be found under a different URL';
        break;
    case 304:
        $finalmessage = 'The requested page has not been modified since last requested';
        break;
    case 308:
        $finalmessage = 'Used in the resumable requests proposal to resume aborted PUT or POST requests';
        break;
    case 400:
        $finalmessage = 'The server cannot or will not process your request due to something that is perceived to be a client error (e.g., malformed request syntax, invalid request message framing, or deceptive request routing)';
        break;
    case 401:
        $finalmessage = 'Authentication is required and has failed or has not yet been provided';
        break;
    case 402:
        $finalmessage = 'Reserved for future use';
        break;
    case 403:
        $finalmessage = 'The request was a valid request, but the server is refusing to respond to it';
        break;
    case 404:
        $finalmessage = 'Page not found';
        break;
    case 405:
        $finalmessage = 'A request was made of a page using a request method not supported by that page';
        break;
    case 406:
        $finalmessage = 'The server can only generate a response that is not accepted by the client';
        break;
    case 407:
        $finalmessage = 'The client must first authenticate itself with the proxy';
        break;
    case 408:
        $finalmessage = 'The server timed out waiting for the request';
        break;
    case 409:
        $finalmessage = 'The request could not be completed because of a conflict in the request';
        break;
    case 410:
        $finalmessage = 'The requested page is no longer available';
        break;
    case 411:
        $finalmessage = 'The "Content-Length" is not defined. The server will not accept the request without it';
        break;
    case 412:
        $finalmessage = 'The precondition given in the request evaluated to false by the server';
        break;
    case 413:
        $finalmessage = 'The server will not accept the request, because the request entity is too large';
        break;
    case 414:
        $finalmessage = 'The server will not accept the request, because the URL is too long. Occurs when you convert a POST request to a GET request with a long query information';
        break;
    case 415:
        $finalmessage = 'The server will not accept the request, because the media type is not supported';
        break;
    case 416:
        $finalmessage = 'The client has asked for a portion of the file, but the server cannot supply that portion';
        break;
    case 417:
        $finalmessage = 'The server cannot meet the requirements of the Expect request-header field';
        break;
    case 451:
        $finalmessage = 'Unavailable for legal reasons';
        break;
    case 500:
        $finalmessage = 'Internal server error';
        break;
    case 501:
        $finalmessage = 'The server either does not recognize the request method, or it lacks the ability to fulfill the request';
        break;
    case 502:
        $finalmessage = 'The server was acting as a gateway or proxy and received an invalid response from the upstream server';
        break;
    case 503:
        $finalmessage = 'The server is currently unavailable (overloaded or down)';
        break;
    case 504:
        $finalmessage = 'The server was acting as a gateway or proxy and did not receive a timely response from the upstream server';
        break;
    case 505:
        $finalmessage = 'The server does not support the HTTP protocol version used in the request';
        break;
    case 511:
        $finalmessage = 'The client needs to authenticate to gain network access';
        break;
    default:
        $finalmessage = 'An error has occurred';
        break;
}
//@todo send message to slack on code > 500
//dump($error);
if ($code >= 500 && isset($error)) {
    $slackHook     = 'https://hooks.slack.com/services/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';
    $projectName   = 'CMS4';
    $slack_message = 'Project: '.$projectName."\r";
    $slack_message .= '-Error: '.$error->getMessage()."\r";
    $slack_message .= '-Line: '.$error->getLine()."\r";
    $slack_message .= '-In: '.$error->getFile()."\r";
    $slack_message .= '-URL: <'.$_SERVER["REQUEST_SCHEME"].'://'.$_SERVER["HTTP_HOST"].">\r";
    $slack_message .= '-Server address: '.$_SERVER['SERVER_ADDR']."\r";
    $slack_message .= '-Remote address: '.$_SERVER['REMOTE_ADDR']."\r";

    if (isset($slackHook)) {
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, $slackHook);
//        curl_setopt($ch, CURLOPT_POST, 1);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, 'payload={"text": "'.str_replace("'", "\\", mysql_escape_string($slack_message)).'", "username":"Project: '.htmlspecialchars($projectName).'"}');
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        curl_exec($ch);
//        curl_close($ch);
    }
}
?>
<?php //@todo do adminPrefix nie można się tutaj dostać(na panelu) -.-, więc ten pierwszy warunek nigdy się nie spełni, trzeba znaleźć na to sposób lub zostawić tylko drugi warunek ?>
<?php if (isset($this->request->params['prefix'], $_admin_prefix) && $this->request->params['prefix'] === $_admin_prefix) : ?>

    <div class="text-center animated fadeInDown" style="margin-top: 200px; margin-bottom: 20px;">
        <h1><?= $code ?></h1>
        <h3 class="font-bold"><?= $message ?></h3>
        <div class="error-desc">
            <?= $finalmessage ?><br />
            Your request URL: <b><?= strip_tags($url) ?></b><br /><br />
            You can go back to the dashboard: <br /><br />
            <?= $this->Html->link('Dashboard', ['controller' => '', 'action' => 'index'], ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
<?php else: ?>
    <section class="main-content">
        <div class="error">
            <div class="error-code"><?= $code ?>
                <!--<i class="fa fa-warning"></i>-->
            </div>
            <div class="error-content">
                <div class="error-message"><h1><?= $message ?></h1></div>
                <div class="error-desc">
                    <?= $finalmessage ?>
                    <div>
                        <?= __('Requested url')?> <b><?= strip_tags($url) ?></b>
                    </div>
                    <div class="error-button">
                        <?= $this->Html->link(__('Back to homepage'), ['controller' => '', 'action' => 'index'], ['class' => 'btn btn-info']) ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>