<?= $this->element('Sections/sticky-box') ?>
<footer class="footer">
    <div class="container-fluid-slim">
        <div id="footer_collapse" class="row align-items-end">
            <div class="col col-12 col-sm-12 col-md-6 col-lg">
                <?php if (isset($statics['footer_menu']) && !empty($statics['footer_menu'])): ?>
                    <?= $statics['footer_menu'] ?>
                <?php endif; ?>
            </div>

            <div class="col col-12 col-sm-12 col-md-6 col-lg">
                <h4 class="footer-title">Contact us</h4>
                <?php if (isset($statics['phone']) && !empty($statics['phone'])): ?>
                    <p>
                        <a class="footer-contact" href="tel:<?= $statics['phone'] ?>"><?= $statics['phone'] ?></a>
                    </p>
                <?php endif; ?>
                <?php if (isset($statics['email']) && !empty($statics['email'])): ?>
                    <p>
                        <a class="footer-contact" href="mailto:<?= $statics['email'] ?>"><?= $statics['email'] ?></a>
                    </p>
                <?php endif; ?>
            </div>
            <div class="col col-12 col-sm-12 col-md-12 col-lg-6">
                <div class="social-icons">
                    <?= $this->element('Sections/social-links') ?>
                </div>
                <?php if (isset($statics['footer_copyright']) && !empty($statics['footer_copyright'])): ?>
                    <div class="legal">
                        <?= $statics['footer_copyright'] ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</footer>
