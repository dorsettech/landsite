<header class="header <?= $page->urlname ?? (isset($channel) ? 'channel' : '') ?>">
    <div class="header-top">
        <div class="header-container">
            <div class="header-row">
                <a class="header-logo" href="/">
                    <?= $this->Html->image('/img/logo_white.png', ['alt' => 'logo', 'class' => 'img-fluid']) ?>
                </a>
                <div class="header-nav">
                    <ul class="navigation">
                        <li class="navigation-item">
                            <?= $this->Html->link('<span class="navigation-text">'.__('Professionals').'</span>', ['_name' => 'professional-services-listing'], ['class' => 'navigation-link', 'escape' => false]) ?>
                        </li>
                        <li class="navigation-item">
                            <?= $this->Html->link('<span class="navigation-text">'.__('Properties').'</span>', ['_name' => 'properties-listing'], ['class' => 'navigation-link', 'escape' => false]) ?>
                        </li>
                        <li class="navigation-item">
                            <?= $this->Html->link('<span class="navigation-text">'.__('News & Case Studies').'</span>', '/news-and-case-studies', ['class' => 'navigation-link', 'escape' => false]) ?>
                        </li>
                        <li class="navigation-item">
                            <?= $this->Html->link('<small class="badge badge-primary badge-pill ml-auto">New</small><span class="navigation-text">'.__('Forums').'</span>', ['prefix' => 'members', 'controller' => 'Dashboard', 'action' => 'forumredirect'], ['class' => 'navigation-link', 'escape' => false]) ?>
                        </li>
                    </ul>
                </div>
                <div class="header-controls">
                    <?php if ($this->request->getSession()->read('Auth.User') !== null): ?>
                            <?= $this->Html->link(__('My Account'), ['prefix' => 'members', 'controller' => '', 'action' => 'index'], ['class' => 'myaccount btn btn-ghost-primary', 'escape' => false]) ?>

                            <?= $this->Html->link('<i class="fas fa-sign-out-alt"></i>', ['prefix' => 'members', 'controller' => '', 'action' => 'logout'], ['class' => 'myaccount btn btn-ghost-secondary', 'escape' => false, 'data-toggle' => 'tooltip', 'title' => 'Log Out']) ?>

                    <?php else: ?>

                            <?= $this->Html->link(__('Login or Register'), ['prefix' => 'members', 'controller' => '', 'action' => 'login'], ['class' => 'myaccount btn btn-ghost-primary ', 'escape' => false]) ?>

                    <?php endif; ?>
                </div>
                <button class="menu fas fa-bars" data-toggle="modal" data-target="#headermenu"></button>
            </div>
        </div>
    </div>
</header>

<!-- Modal menu -->
<div id="headermenu" class="side-menu modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button class="menu fas fa-bars" data-toggle="modal" data-target="#headermenu"></button>
                <div class="socials">
                    <?= $this->element('Sections/social-links') ?>
                </div>
            </div>
            <div class="modal-body">
                <nav class="navbar">
                    <ul>
                        <?php foreach ($menus as $menu): ?>
                            <li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <?php if (!empty($menu->image)): ?>
                                        <img src="<?= $this->Images->scale($menu->image_path, 37, 28) ?>" alt="<?= $menu->urlname ?>">
                                    <?php endif; ?>
                                    <?= $menu->name ?></a>
                                <?php if (!empty($menu->pages)): ?>
                                    <?= $this->Menu->generateDefault($menu, ['className' => 'dropdown-menu']) ?>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </nav>
                <div class="buttons">
                    <?= $this->Html->link(__('Property search').' <i class="fas fa-search-location"></i>', ['_name' => 'properties-listing'], ['class' => 'btn btn-primary d-lg-none', 'escape' => false]) ?>
                    <?= $this->Html->link(__('Professionals search').' <i class="fas fa-search-location"></i>', ['_name' => 'professional-services-listing'], ['class' => 'btn btn-primary d-lg-none', 'escape' => false]) ?>
                    <?= $this->Html->link(__('News & Case Studies').' <i class="far fa-newspaper"></i>', '/news-and-case-studies', ['class' => 'btn btn-primary d-lg-none', 'escape' => false]) ?>
                    <?= $this->Html->link('<small class="badge badge-light badge-pill ml-auto" style="font-size: .6em; position: absolute; right: 0; top: -.4em;">New</small><span class="navigation-text">'.__('Forums').'</span> <i class="far fa-comment-dots"></i>', ['prefix' => 'members', 'controller' => 'Dashboard', 'action' => 'forumredirect'], ['class' => 'btn btn-primary d-lg-none', 'style' => 'overflow: visible;', 'escape' => false]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
