<div class="author form-row">
    <div class="col-12"><hr></div>
    <div class="col-auto">
        <?php if (!empty($author->image)) : ?>
        <div class="image">
            <img src="<?= $this->Images->scale($author->image_path, 100, 100) ?>" alt="<?= $author->full_name ?>">
        </div>
        <?php endif; ?>
    </div>
    <div class="col">
        <p class="heading" itemprop="author">
            <?php if (isset($author->service) && !empty($author->service)): ?>
                <?= $author->service->company ?>
            <?php else: ?>
                <?= $author->full_name ?>
            <?php endif; ?>
        </p>
        <?php if (isset($author->service) && !empty($author->service)): ?>
            <p><?= $author->service->description_short ?></p>
            <p><?= $this->Html->link(__('Link to {0} business profile', $author->service->company), ['_name' => 'professional-services-individual', $author->service->urlname]) ?></p>
        <?php endif; ?>
    </div>
</div>
