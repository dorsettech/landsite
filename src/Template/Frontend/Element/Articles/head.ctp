<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?php if (isset($metaTags['title']) && !empty($metaTags['title'])): ?>
                <?= $metaTags['title'] ?>
            <?php elseif (!empty($article->title)): ?>
                <?= $article->title.' | Property News'; ?>
            <?php else: ?>
                <?= $_service; ?>
            <?php endif; ?>
        </title>
        <?php if (isset($metaTags['keywords']) && !empty($metaTags['keywords'])) : ?><meta name="keywords" content="<?= $metaTags['keywords'] ?>"><?php endif; ?>
        <?php if (isset($metaTags['description']) && !empty($metaTags['description'])) : ?>
            <meta name="description" content="<?= $metaTags['description'] ?>">
        <?php elseif (!empty($article->body)): ?>
            <meta name="description" content="<?= $this->Text->truncate(strip_tags($article->body), 165, ['ellipsis' => '...', 'exact' => false]) ?>">
        <?php endif; ?>
        <meta property="og:url" content="<?= $this->Url->build('/', true).substr($this->request->getPath(), 1) ?>">
        <meta property="og:type" content="article">
        <?php if (isset($article->title) && !empty($article->title)): ?>
            <meta property="og:title" content="<?= $article->title ?>">
            <meta name="twitter:title" content="<?= $article->title ?>">
        <?php elseif (isset($metaTags['og_title']) && !empty($metaTags['og_title'])) : ?>
            <meta property="og:title" content="<?= $metaTags['og_title'] ?>">
            <meta name="twitter:title" content="<?= $metaTags['og_title'] ?>">
        <?php endif; ?>
        <?php if (isset($article->image) && !empty($article->image)): ?>
            <meta property="og:image" content="<?= $this->Url->build('/', true).$article->image_path ?>">
            <meta name="twitter:image" content="<?= $this->Url->build('/', true).$article->image_path ?>">
        <?php elseif (isset($statics['og_image']) && !empty($statics['og_image'])) : ?>
            <meta property="og:image" content="<?= $this->Url->build('/', true).$statics['og_image'] ?>">
            <meta name="twitter:image" content="<?= $this->Url->build('/', true).$statics['og_image'] ?>">
        <?php endif; ?>
        <?php if (isset($article->body) && !empty($article->body)): ?>
            <meta property="og:description" content="<?= $this->Text->truncate(strip_tags($article->body), 255, ['ellipsis' => '...', 'exact' => false]) ?>">
            <meta name="twitter:description" content="<?= $this->Text->truncate(strip_tags($article->body, 255), ['ellipsis' => '...', 'exact' => false]) ?>">
        <?php elseif (isset($metaTags['og_description']) && !empty($metaTags['og_description'])) : ?>
            <meta property="og:description" content="<?= $metaTags['og_description'] ?>">
            <meta name="twitter:description" content="<?= $metaTags['og_description'] ?>">
        <?php endif; ?>
        <meta property="og:site_name" content="<?= $_service ?>">
        <?php if (isset($article->modified) && !empty($article->modified)): ?>
            <meta property="og:updated_time" content="<?= $article->modified->format('d-m-Y') ?>">
        <?php endif; ?>
        <meta property="og:locale" content="en_GB">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="@thelandsite" />
        <?= $this->element('head-scripts') ?>
    </head>
    <body>
        <?php if (!empty($statics['scripts_body_top'])) : ?>
            <?= $statics['scripts_body_top']; ?>
        <?php endif; ?>
