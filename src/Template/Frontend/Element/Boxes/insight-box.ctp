<?php if (isset($article) && !empty($article)): ?>
    <div class="box">
        <div class="image">
            <img src="<?= $this->Images->scale($article->image_path, 490, 370) ?>" alt="<?= $article->name ?>"> <?php /* best image size: 500x500px */ ?>
            <?= $this->Html->link('', ['_name' => 'news-individual', $article->slug], ['class' => 'full-link']); ?>
            <?php if (!empty($article->publish_date)): ?>
                <div class="date"><?= $article->publish_date->format('jS F Y') ?></div>
            <?php endif; ?>
        </div>
        <?= $this->Html->link($article->title, ['_name' => 'news-individual', $article->slug], ['class' => 'title']); ?>
        <?php if (isset($article->user) && !empty($article->user)): ?>
            <div class="author">
                <span>Posted by </span> <?= $this->Page->articlePostedBy($article, ['link' => true]) ?>
            </div>
        <?php endif; ?>
        <div class="dsc"><?= $article->headline ?></div>
        <?= $this->Html->link(__('Read more').' <i class="fas fa-arrow-right"></i>', ['_name' => 'news-individual', $article->slug], ['class' => 'btn btn-primary', 'escape' => false]); ?>
    </div>
<?php endif; ?>