
<?php if (isset($channel->image) && !empty($channel->image)) : ?>
    <div class="channel listing image" style="background-image: url('<?= $this->Images->scale($channel->image_path, 490, 370) ?>');">
<?php else : ?>
    <div class="channel listing">
<?php endif; ?>
    <?= $this->Html->link($channel->label ?? $channel->name, ['_name' => 'channels-individual', $channel->slug], ['class' => 'label btn btn-ghost-primary']); ?>
</div>
