<?php if (isset($caseStudy) && !empty($caseStudy)): ?>
    <div class="box">
        <div class="image">
            <img src="<?= $this->Images->scale($caseStudy->image_path, 490, 370) ?>" alt="<?= $caseStudy->name ?>">
            <?= $this->Html->link('', ['_name' => 'case-studies-individual', $caseStudy->slug], ['class' => 'full-link']); ?>
            <?php if (!empty($caseStudy->publish_date)): ?>
                <div class="date"><?= $caseStudy->publish_date->format('jS F Y') ?></div>
            <?php endif; ?>
        </div>
        <?= $this->Html->link($caseStudy->title, ['_name' => 'case-studies-individual', $caseStudy->slug], ['class' => 'title']); ?>
        <?php if (isset($caseStudy->user) && !empty($caseStudy->user)): ?>
            <div class="author"><span>Posted by </span>
                <?= $this->Page->articlePostedBy($caseStudy, ['link' => true]) ?>
            </div>
        <?php endif; ?>
        <div class="dsc"><?= $caseStudy->headline ?></div>
        <?= $this->Html->link(__('Read more').' <i class="fas fa-arrow-right"></i>', ['_name' => 'case-studies-individual', $caseStudy->slug], ['class' => 'btn btn-primary', 'escape' => false]); ?>
    </div>
<?php endif; ?>