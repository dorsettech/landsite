<?php if (isset($content) && !empty($content)): ?>
    <div class="col-12">
        <div class="box">
            <?= $content ?>
        </div>
    </div>
<?php endif; ?>
