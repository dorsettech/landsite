<?php

use App\Model\Enum\AdType;
?>
<div class="col-12">
    <div class="property-sidebar-box property <?= ($property->ad_type !== AdType::STANDARD) ? 'featured' : '' ?>">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-5 col-xl-12 col-xxl-5 left">
                <div class="image">
                    <?php if ($property->ad_type !== AdType::STANDARD): ?>
                        <div class="featured-text"><?= $property->ad_type ?></div>
                    <?php endif; ?>
                    <img src="<?= $this->Images->scale($property->main_image, 240, 300) ?>" alt="<?= $property->title ?>">
                    <?= $this->Html->link('', ['_name' => 'properties-individual', $property->urlname], ['class' => 'full-link']) ?>
                    <div class="prices d-md-none">
                        <?= $this->Properties->price($property) ?>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm right">
                <?= $this->Html->link($property->title, ['_name' => 'properties-individual', $property->urlname], ['class' => 'heading']) ?>
                <?php if (!empty($property->property_attributes)): ?>
                    <div class="icons">
                        <?php foreach ($property->property_attributes as $attribute): ?>
                            <?php if ($attribute->_joinData->attribute_value == true) : ?>
                                <i class="customicons icon1" data-toggle="tooltip" data-placement="top" title="<?= $attribute->name ?>" style="background: url(<?= $attribute->image_path ?>);"></i>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
                <div class="location"><?= $property->location ?></div>
                <div class="description"><?= $property->headline ?></div>
                <?php if ($property->has('user') && $property->user->has('service')): ?>
                    <div class="companyinfo">
                        <div class="markedby mr-auto">
                            <a href="<?= $this->Url->build(['_name' => 'professional-services-individual', $property->user->service->urlname]) ?>" class="img">
                                <img src="<?= !empty($property->user->image) ? $property->user->image_path : '/img/placeholder.png' ?>" alt="<?= $property->user->service->slug ?>">
                            </a>
                            <div class="">
                                Added <?= (!empty($property->publish_date)) ? 'on '.$property->publish_date->format('d/m/Y').' ' : '' ?>by:<br>
                                <?= $this->Html->link($property->user->service->company, ['_name' => 'professional-services-individual', $property->user->service->urlname]) ?>
                            </div>
                        </div>
                        <?php if (!empty($this->request->getSession()->read('Auth.User'))): ?>
                            <a href="#"  class="service-contact-modal contact ml-auto" data-toggle="modal" data-target="#servicesmodalclear" data-title='Contact <?= $property->user->service->company ?>' data-service-id='<?= $property->user->service->id ?>' data-recipient-name='<?= $property->user->first_name ?>' data-recipient-email='<?= $property->user->email ?>' data-profile-link='<?= $this->Url->build(['_name' => 'professional-services-individual', $property->user->service->urlname], true) ?>' data-image="<?= (!empty($property->user->image) ? '/' . $this->Images->scale($property->user->image_path, 150, 150, 'ratio_fill') : '') ?>">
                                <i class="far fa-envelope"></i>
                            </a>
                        <?php else: ?>
                            <?= $this->Html->link('<i class="far fa-envelope"></i>', ['prefix' => 'members', 'controller' => '', 'action' => 'login', '?' => ['redirectTo' => $this->Url->build('/', true).$this->request->url]], ['class' => 'contact', 'escape' => false]) ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <div class="prices">
                    <?= $this->Properties->price($property) ?>
                </div>
            </div>
        </div>
    </div>
</div>