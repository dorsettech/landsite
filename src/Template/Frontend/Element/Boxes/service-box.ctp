<?php if (isset($service) && !empty($service)): ?>
    <div class="property <?= ($service->ad_type !== 'STANDARD') ? 'featured' : ''; ?>">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-5 col-xl-6 col-xxl-5 left">
                <div class="image logo">
                    <?php if ($service->ad_type !== 'STANDARD'): ?>
                        <div class="featured-text"><?= $service->ad_type ?></div>
                    <?php endif; ?>
                    <img src="<?= !empty($service->user->image) ? $service->user->image_path : '/img/placeholder.png' ?>" alt="<?= $service->slug ?>">
                    <?= $this->Html->link('', ['_name' => 'professional-services-individual', $service->urlname], ['class' => 'full-link']) ?>
                </div>
            </div>
            <div class="col-12 col-sm right">
                <?= $this->Html->link($service->company, ['_name' => 'professional-services-individual', $service->urlname], ['class' => 'heading']) ?>
                <div class="location"><?= $service->location ?></div>
                <div class="description"><?= $service->description_short ?></div>
                <div class="bottom">
                    <?= $this->Html->link(__('More details').' <i class="fas fa-arrow-right"></i>', ['_name' => 'professional-services-individual', $service->urlname], ['class' => 'btn btn-primary', 'escape' => false]) ?>
                    <?php if ($service->properties_count) : ?><?= $this->Html->link('<i class="customicons icon5"></i>' . __('See uploaded properties'), ['_name' => 'service-properties', $service->id, $service->slug], ['class' => 'properties-available', 'escape' => false]) ?><?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>