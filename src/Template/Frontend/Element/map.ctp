<div id="map" style="height: 300px; width: 100%;"></div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTohKpkpJQE-T1akTfYcR-L7VE6_xHl44"></script>
<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', init);
    function init() {
        var latlng = new google.maps.LatLng(<?= $statics['map_latitude'] ?>, <?= $statics['map_longitude'] ?>);
        var mapOptions = {
            zoom: <?= $statics['map_zoom'] ?>,
            scrollwheel: false,
            disableDefaultUI: true,
            center: latlng,
        };
        var map = new google.maps.Map(document.getElementById('map'), mapOptions);
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: '<?= $statics['map_title'] ?>'
        });
    }
</script>