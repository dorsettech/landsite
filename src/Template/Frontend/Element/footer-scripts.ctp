<?= $this->Html->script('/js/libraries/sweetalert/sweetalert.min.js') ?>
<?= $this->Html->script('/js/swal.js') ?>
<?= $this->Html->script('/js/formBuilderListener.js') ?>
<?= $this->Html->script('/js/csrf') ?>
<?= $this->Html->script('/js/app.min.js') ?>
<?= $this->element('cookies-info'); ?>
<?= $this->fetch('scriptBottom') ?>
<?= $this->Flash->render() ?>
<?php if (!empty($statics['scripts_body_end'])) : ?>
    <?= $statics['scripts_body_end']; ?>
<?php endif; ?>
</body>
</html>