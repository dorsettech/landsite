<div id="cookiecontrol" class="d-none">
    <div id="cookiewrapper">
        <span id="cookiepolicytxt"><?= $this->Page->statics('cookie_policy'); ?></span>
        <a id="cookieclose" href="<?= $this->request->here() ?>#cookieagree">×</a>
    </div>
</div>
<style type="text/css">
    #cookiecontrol {
        background: rgba(0, 0, 0, 0.8) none repeat scroll 0 0;
        bottom: 20px;
        color: #cccccc;
        left: 20px;
        position: fixed;
        z-index: 99999;
    }
    #cookiecontrol #cookiewrapper {
        margin: 0 auto;
        max-width: 280px;
        min-height: 30px;
        padding: 40px 20px 20px 20px;
        position: relative;
        text-align: left;
    }
    #cookiecontrol #cookiewrapper section, #cookiecontrol #cookiewrapper div, #cookiecontrol #cookiewrapper p, #cookiecontrol #cookiewrapper ul, #cookiecontrol #cookiewrapper li, #cookiecontrol #cookiewrapper a {
        margin: 0;
        padding: 0;
    }
    #cookiecontrol #cookiewrapper #cookiepolicytxt {
        font-size: 11px;
    }
    #cookiecontrol #cookiewrapper #cookieclose {
        display: block;
        font-size: 25px;
        position: absolute;
        right: 10px;
        top: 0;
    }
    #cookiecontrol #cookiewrapper #cookieclose:hover {
        color: #cccccc;
        cursor: pointer;
        text-decoration: none;
    }
    #cookiecontrol #cookiewrapper a {
        color: #ffffff;
        font-weight: bold;
    }
    #cookiecontrol #cookiewrapper a:hover {
        color: #cccccc;
        font-weight: bold;
    }
</style>
<?php
$this->Html->script('/js/cookie-widget.js', ['block' => 'scriptBottom']);
?>