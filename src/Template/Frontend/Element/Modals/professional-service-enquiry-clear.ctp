<div id="servicesmodalclear" class="modal custom fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fas fa-times"></i>
                </button>
                <div class="heading"></div>
                <?= $this->Captcha->formStart(null, ['url' => ['_name' => '/professional-services/send-enquiry'], 'class' => 'form-row justify-content-center align-items-center']) ?>
                <?= $this->Form->control('service_id', ['type' => 'hidden', 'label' => false]) ?>
                <?= $this->Form->control('user_id', ['type' => 'hidden', 'value' => $this->request->getSession()->read('Auth.User.id'), 'label' => false]) ?>
                <?= $this->Form->control('recipient_name', ['type' => 'hidden', 'label' => false]) ?>
                <?= $this->Form->control('recipient_email', ['type' => 'hidden', 'label' => false]) ?>
                <?= $this->Form->control('profile_link', ['type' => 'hidden', 'label' => false]) ?>
                <div class="col-12 col-sm-6">
                    <?= $this->Form->control('full_name', ['placeholder' => 'Full name', 'label' => false, 'required', 'value' => $this->request->getSession()->read('Auth.User.first_name').' '.$this->request->getSession()->read('Auth.User.last_name')]) ?>
                </div>
                <div class="col-12 col-sm-6">
                    <?= $this->Form->control('phone', ['placeholder' => 'Phone number', 'label' => false, 'required', 'value' => $this->request->getSession()->read('Auth.User.phone')]) ?>
                </div>
                <div class="col-12">
                    <?= $this->Form->control('email', ['placeholder' => 'Email', 'label' => false, 'required', 'value' => $this->request->getSession()->read('Auth.User.email')]) ?>
                </div>
                <div class="col-12">
                    <?= $this->Form->control('message', ['placeholder' => 'Message', 'rows' => 5, 'label' => false]) ?>
                </div>
                <div class="bottominfo form-group col-12 d-flex">
                    <span class="image"></span>
                    <?php if (isset($statics['terms_and_conditions_label']) && !empty($statics['terms_and_conditions_label'])): ?>
                        <?= $statics['terms_and_conditions_label'] ?>
                    <?php endif; ?>
                </div>
                <div class="col-auto ml-auto">
                    <?= $this->Form->submit('Enquire now', ['class' => 'btn btn-primary']) ?>
                </div>
                <?= $this->Captcha->formEnd(); ?>
            </div>
        </div>
    </div>
</div>