<section class="newsletter">
    <div class="container-fluid-slim">
        <?php if (isset($statics['newsletter_section_content']) && !empty($statics['newsletter_section_content'])): ?>
            <div class="row head">
                <div class="col">
                    <?= $statics['newsletter_section_content'] ?>
                </div>
            </div>
        <?php endif; ?>
        <?= $this->Captcha->replaceStart('newsletter'); ?>
            <div class="row">
                <div class="col-12 col-sm-6">
                    {form.newsletter.field.email}
                </div>
                <div class="col-12 col-sm-1">
                    {form.newsletter.field.submit}
                </div>
            </div>
        <?= $this->Captcha->formEnd(); ?>
    </div>
</section>
