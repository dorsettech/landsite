<?php if (isset($channels) && !empty($channels)): ?>
    <div class="thought-leadership">
        <div class="container-fluid-slim">
            <h2 class="heading">Thought Leadership</h2>
            <h4 class="subheading">Popular Channels</h4>
            <div class="row align-items-center">
                <div class="col-12 col-md-9 box text-center-sm d-flex flex-wrap justify-content-center justify-content-md-between">
                    <?php foreach ($channels as $channel): ?>
                        <?= $this->Html->link($channel->label ?? $channel->name, ['_name' => 'channels-individual', $channel->slug], ['class' => 'label btn btn-ghost-primary mx-1']); ?>
                    <?php endforeach; ?>
                </div>
                <div class="col-12 col-md-3 box text-center">
                    <?= $this->Html->link(__('View All Channels') . ' <i class="fas fa-chevron-circle-right"></i>', ['_name' => 'channels-listing'], ['class' => 'btn btn-ghost-invert', 'escape' => false]); ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
