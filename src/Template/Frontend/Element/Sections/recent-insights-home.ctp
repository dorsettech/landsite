<?php if (isset($recentInsights) && !empty($recentInsights)): ?>
    <section class="posts bgcolor">
        <div class="container">
            <div class="row head">
                <div class="col-12">
                    <div class="heading"><?= __('Stay up to date with our members news') ?></div>
                </div>
            </div>
            <div class="carousel-overflow">
                <div class="row carousel">
                    <?php foreach ($recentInsights as $insight): ?>
                        <div class="col-12">
                            <?= $this->element('Boxes/insight-box', ['article' => $insight]) ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="d-flex justify-content-center mt-5">
                <?= $this->Html->link(__('View All'), ['_name' => 'news-listing'], ['class' => 'btn btn-ghost-invert', 'escape' => false]); ?>
            </div>
        </div>
    </section>
<?php endif; ?>
