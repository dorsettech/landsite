<?php if (isset($recentInsights) && !empty($recentInsights)): ?>
    <section class="posts bgimage">
        <div class="container">
            <?php if (isset($statics['recent_insights_section_description']) && !empty($statics['recent_insights_section_description'])): ?>
                <div class="row head">
                    <div class="col-12">
                        <?= $statics['recent_insights_section_description'] ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row list">
                <?php foreach ($recentInsights as $insight): ?>
                    <div class="col-12 col-sm-6 col-lg-4">
                        <?= $this->element('Boxes/insight-box', ['article' => $insight]) ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif; ?>