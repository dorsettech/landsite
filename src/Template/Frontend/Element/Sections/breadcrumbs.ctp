<?php if (isset($breadcrumbs) && !empty($breadcrumbs)): ?>
    <section>
        <div class="container">
            <?= $this->Menu->generateBreadcrumbs($breadcrumbs) ?>
        </div>
    </section>
<?php endif; ?>