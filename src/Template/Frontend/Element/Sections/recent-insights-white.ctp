<?php if (isset($recentInsights) && !empty($recentInsights)): ?>
    <section class="posts bgwhite">
        <?php if (isset($title) && !empty($title)): ?>
            <div class="row head">
                <div class="col-12">
                    <div class="heading">
                        <h2><?= $title ?></h2>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="row list">
            <?php foreach ($recentInsights as $insight): ?>
                <div class="col-12 col-sm-6 col-lg-4">
                    <?= $this->element('Boxes/insight-box', ['article' => $insight]) ?>
                </div>
            <?php endforeach; ?>
        </div>
    </section>
<?php endif; ?>