<section class="beta-testing">
    <div class="container">
        <div class="details">
            <div class="button">
                <?= $this->Html->link(__('BETA TESTING'), '/contact', ['class' => 'btn btn-primary']) ?>
            </div>
            <div class="info">
                <?= __('We are in the final stages of testing The Landsite professional property portal. Please bear with us. If you find any glitches, please let us know.') ?>
            </div>
        </div>
    </div>
</section>