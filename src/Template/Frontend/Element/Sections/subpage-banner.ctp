<section class="slider contact">
    <div class="bgimage">
        <img src="<?= $this->Images->scale($content->file, 1920, 510) ?>" alt="<?= $this->Images->alt($content->file) ?>">
    </div>
    <div class="container">
        <div class="content slider">
            <?php if (isset($content->content) && !empty($content->content)): ?>
                <div class="row">
                    <div class="col-12 col-md-9 col-lg-6">
                        <?= $content->content ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>