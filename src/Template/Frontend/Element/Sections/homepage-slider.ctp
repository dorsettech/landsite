<?php if (isset($page->slider) && !empty($page->slider)): ?>
    <section class="slider slim">
        <div class="carousel">
            <?php foreach ($page->slider as $image): ?>
                <div class="item">
                    <div class="bgimage">
                        <img src="<?= $this->Images->scale($image['path'], 1920, 480) ?>" alt="<?= $image['alt'] ?>">
                    </div>
                    <?php if (isset($image['description']) && !empty($image['description'])): ?>
                        <div class="container-fluid-slim">
                            <div class="content slide row">
                                <div class="col-12 col-sm-8">
                                    <?= $image['description'] ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </section>
<?php endif; ?>
