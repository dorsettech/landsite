<div class="form-row justify-content-center">
    <?php if (isset($statics['property_call_to_action_box_1']) && !empty($statics['property_call_to_action_box_1'])): ?>
        <div class="adverts col-6 col-sm-4 col-xxl">
            <?= $statics['property_call_to_action_box_1'] ?>
        </div>
    <?php endif; ?>
    <?php if (isset($statics['property_call_to_action_box_2']) && !empty($statics['property_call_to_action_box_2'])): ?>
        <div class="adverts col-6 col-sm-4 col-xxl">
            <?= $statics['property_call_to_action_box_2'] ?>
        </div>
    <?php endif; ?>
    <?php if (isset($statics['property_call_to_action_box_3']) && !empty($statics['property_call_to_action_box_3'])): ?>
        <div class="adverts col-6 col-sm-4 col-xxl">
            <?= $statics['property_call_to_action_box_3'] ?>
        </div>
    <?php endif; ?>
    <?php if (isset($statics['property_call_to_action_box_4']) && !empty($statics['property_call_to_action_box_4'])): ?>
        <div class="adverts col-6 col-sm-4 col-xxl">
            <?= $statics['property_call_to_action_box_4'] ?>
        </div>
    <?php endif; ?>
    <?php if (isset($statics['property_call_to_action_box_5']) && !empty($statics['property_call_to_action_box_5'])): ?>
        <div class="adverts col-6 col-sm-4 col-xxl">
            <?= $statics['property_call_to_action_box_5'] ?>
        </div>
    <?php endif; ?>
</div>