<?php

use App\Model\Enum\PropertyPurpose;
?>

<section class="tab tab1">
    <?php if (isset($contents->home_help_title->title) && !empty($contents->home_help_title->title)): ?>
        <div class="question">
            <div class="container">
                <?= $contents->home_help_title->title ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="tab-navigation">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul class="nav">
                        <li><button href="#tab1-1" class="active" data-toggle="tab" role="tab">I'm a professional</button></li>
                        <li><button href="#tab1-2" class="" data-toggle="tab" role="tab">I need a professional</button></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-content">
        <div id="tab1-1" class="tab-pane fade show active">
            <div class=""> <?php /* add class bgimage if backgorund needed */ ?>
                <div class="container-fluid-slim">
                    <?php if (isset($contents->home_help_tab_ampro->content) && !empty($contents->home_help_tab_ampro->content)): ?>
                        <div class="row head">
                            <div class="col-12">
                                <div class="heading"><?= __('Let\'s help your business grow') ?></div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-12">
                                <div class="collapser">
                                    <div id="tab_collapses1" class="row">
                                        <?= $contents->home_help_tab_ampro->content ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (isset($contents->home_help_tab_ampro->content2) && !empty($contents->home_help_tab_ampro->content2)): ?>
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <?= $contents->home_help_tab_ampro->content2 ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div id="tab1-2" class="tab-pane fade">
            <div class="bgimage">
                <div class="container">
                    <div class="row head">
                        <div class="col-12">
                            <div class="heading"><?= __('Search for professionals') ?></div>
                        </div>
                    </div>
                    <?= $this->cell('ServicesSearch') ?>
                    <?= $this->element('Sections/services-categories') ?>
                </div>
            </div>
        </div>
    </div>
</section>
