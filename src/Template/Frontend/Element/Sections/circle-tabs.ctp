<section class="circletabs">
    <?php if (isset($contents->circle_background->file) && !empty($contents->circle_background->file)): ?>
        <div class="bgimage">
            <img src="<?= $this->Images->scale($contents->circle_background->file, 2000, 760) ?>" alt="slider1">
        </div>
    <?php endif; ?>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7">
                <div class="tab-nav background">
                    <div class="embed-responsive embed-responsive-1by1">
                        <div class="embed-responsive-item">
                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                <?php if (isset($contents->circle_tab_center->file) && !empty($contents->circle_tab_center->file)) : ?>
                                    <li>
                                        <div class="link active" data-toggle="tab" href="#pills-center" role="tab">
                                            <img src="<?= $contents->circle_tab_center->file ?>" alt="Icon" class="">
                                        </div>
                                    </li>
                                <?php endif; ?>
                                <?php if (isset($contents->circle_tab_1->title) && !empty($contents->circle_tab_1->title)) : ?>
                                    <li>
                                        <div class="link" data-toggle="tab" href="#pills-1" role="tab">
                                            <?php if (isset($contents->circle_tab_1->file) && !empty($contents->circle_tab_1->file)) : ?>
                                                <img src="<?= $contents->circle_tab_1->file ?>" alt="Icon" class="customicons">
                                            <?php endif; ?>
                                            <span><?= $contents->circle_tab_1->title ?></span>
                                        </div>
                                    </li>
                                <?php endif; ?>
                                <?php if (isset($contents->circle_tab_2->title) && !empty($contents->circle_tab_2->title)) : ?>
                                    <li>
                                        <div class="link" data-toggle="tab" href="#pills-2" role="tab">
                                            <?php if (isset($contents->circle_tab_2->file) && !empty($contents->circle_tab_2->file)) : ?>
                                                <img src="<?= $contents->circle_tab_2->file ?>" alt="Icon" class="customicons">
                                            <?php endif; ?>
                                            <span><?= $contents->circle_tab_2->title ?></span>
                                        </div>
                                    </li>
                                <?php endif; ?>
                                <?php if (isset($contents->circle_tab_3->title) && !empty($contents->circle_tab_3->title)) : ?>
                                    <li>
                                        <div class="link" data-toggle="tab" href="#pills-3" role="tab">
                                            <?php if (isset($contents->circle_tab_3->file) && !empty($contents->circle_tab_3->file)) : ?>
                                                <img src="<?= $contents->circle_tab_3->file ?>" alt="Icon" class="customicons">
                                            <?php endif; ?>
                                            <span><?= $contents->circle_tab_3->title ?></span>
                                        </div>
                                    </li>
                                <?php endif; ?>
                                <?php if (isset($contents->circle_tab_4->title) && !empty($contents->circle_tab_4->title)) : ?>
                                    <li>
                                        <div class="link" data-toggle="tab" href="#pills-4" role="tab">
                                            <?php if (isset($contents->circle_tab_4->file) && !empty($contents->circle_tab_4->file)) : ?>
                                                <img src="<?= $contents->circle_tab_4->file ?>" alt="Icon" class="customicons">
                                            <?php endif; ?>
                                            <span><?= $contents->circle_tab_4->title ?></span>
                                        </div>
                                    </li>
                                <?php endif; ?>
                                <?php if (isset($contents->circle_tab_5->title) && !empty($contents->circle_tab_5->title)) : ?>
                                    <li>
                                        <div class="link" data-toggle="tab" href="#pills-5" role="tab">
                                            <?php if (isset($contents->circle_tab_5->file) && !empty($contents->circle_tab_5->file)) : ?>
                                                <img src="<?= $contents->circle_tab_5->file ?>" alt="Icon" class="customicons">
                                            <?php endif; ?>
                                            <span><?= $contents->circle_tab_5->title ?></span>
                                        </div>
                                    </li>
                                <?php endif; ?>
                                <?php if (isset($contents->circle_tab_6->title) && !empty($contents->circle_tab_6->title)) : ?>
                                    <li>
                                        <div class="link" data-toggle="tab" href="#pills-6" role="tab">
                                            <?php if (isset($contents->circle_tab_6->file) && !empty($contents->circle_tab_6->file)) : ?>
                                                <img src="<?= $contents->circle_tab_6->file ?>" alt="Icon" class="customicons">
                                            <?php endif; ?>
                                            <span><?= $contents->circle_tab_6->title ?></span>
                                        </div>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-5">
                <div class="tab-content" id="pills-tabContent">
                    <?php if (isset($contents->circle_tab_center->content) && !empty($contents->circle_tab_center->content)) : ?>
                        <div class="tab-pane fade show active" id="pills-center" role="tabpanel">
                            <?php if (isset($contents->circle_tab_center->title) && !empty($contents->circle_tab_center->title)) : ?>
                                <div class="heading"><?= $contents->circle_tab_center->title ?></div>
                            <?php endif; ?>
                            <?= $contents->circle_tab_center->content ?>
                        </div>
                    <?php endif; ?>
                    <?php if (isset($contents->circle_tab_1->title) && !empty($contents->circle_tab_1->title)) : ?>
                        <div class="tab-pane fade" id="pills-1" role="tabpanel">
                            <?php if (isset($contents->circle_tab_1->file) && !empty($contents->circle_tab_1->file)) : ?>
                                <img src="<?= $contents->circle_tab_1->file ?>" alt="Icon" class="customicons">
                            <?php endif; ?>
                            <div class="heading"><?= $contents->circle_tab_1->title ?></div>
                            <?php if (isset($contents->circle_tab_1->content) && !empty($contents->circle_tab_1->content)) : ?>
                                <?= $contents->circle_tab_1->content ?>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                    <?php if (isset($contents->circle_tab_2->title) && !empty($contents->circle_tab_2->title)) : ?>
                        <div class="tab-pane fade" id="pills-2" role="tabpanel">
                            <?php if (isset($contents->circle_tab_2->file) && !empty($contents->circle_tab_2->file)) : ?>
                                <img src="<?= $contents->circle_tab_2->file ?>" alt="Icon" class="customicons">
                            <?php endif; ?>
                            <div class="heading"><?= $contents->circle_tab_2->title ?></div>
                            <?php if (isset($contents->circle_tab_2->content) && !empty($contents->circle_tab_2->content)) : ?>
                                <?= $contents->circle_tab_2->content ?>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                    <?php if (isset($contents->circle_tab_3->title) && !empty($contents->circle_tab_3->title)) : ?>
                        <div class="tab-pane fade" id="pills-3" role="tabpanel">
                            <?php if (isset($contents->circle_tab_3->file) && !empty($contents->circle_tab_3->file)) : ?>
                                <img src="<?= $contents->circle_tab_3->file ?>" alt="Icon" class="customicons">
                            <?php endif; ?>
                            <div class="heading"><?= $contents->circle_tab_3->title ?></div>
                            <?php if (isset($contents->circle_tab_3->content) && !empty($contents->circle_tab_3->content)) : ?>
                                <?= $contents->circle_tab_3->content ?>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                    <?php if (isset($contents->circle_tab_4->title) && !empty($contents->circle_tab_4->title)) : ?>
                        <div class="tab-pane fade" id="pills-4" role="tabpanel">
                            <?php if (isset($contents->circle_tab_4->file) && !empty($contents->circle_tab_4->file)) : ?>
                                <img src="<?= $contents->circle_tab_4->file ?>" alt="Icon" class="customicons">
                            <?php endif; ?>
                            <div class="heading"><?= $contents->circle_tab_4->title ?></div>
                            <?php if (isset($contents->circle_tab_4->content) && !empty($contents->circle_tab_4->content)) : ?>
                                <?= $contents->circle_tab_4->content ?>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                    <?php if (isset($contents->circle_tab_5->title) && !empty($contents->circle_tab_5->title)) : ?>
                        <div class="tab-pane fade" id="pills-5" role="tabpanel">
                            <?php if (isset($contents->circle_tab_5->file) && !empty($contents->circle_tab_5->file)) : ?>
                                <img src="<?= $contents->circle_tab_5->file ?>" alt="Icon" class="customicons">
                            <?php endif; ?>

                            <div class="heading"><?= $contents->circle_tab_5->title ?></div>
                            <?php if (isset($contents->circle_tab_5->content) && !empty($contents->circle_tab_5->content)) : ?>
                                <?= $contents->circle_tab_5->content ?>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                    <?php if (isset($contents->circle_tab_6->title) && !empty($contents->circle_tab_6->title)) : ?>
                        <div class="tab-pane fade" id="pills-6" role="tabpanel">
                            <?php if (isset($contents->circle_tab_6->file) && !empty($contents->circle_tab_6->file)) : ?>
                                <img src="<?= $contents->circle_tab_6->file ?>" alt="Icon" class="customicons">
                            <?php endif; ?>
                            <div class="heading"><?= $contents->circle_tab_6->title ?></div>
                            <?php if (isset($contents->circle_tab_6->content) && !empty($contents->circle_tab_6->content)) : ?>
                                <?= $contents->circle_tab_6->content ?>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>