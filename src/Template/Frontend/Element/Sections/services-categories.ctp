<?php if (!empty($categories)) : ?>
    <div class="form-row justify-content-center align-items-center">
        <div class="col-12 col-lg-10 more">
            <ul class="columns">
            <?php foreach ($categories as $category) : ?>
                <li><?= $this->Html->link($category->name, ['_name' => 'professional-services-listing', '?' => ['service_category' => $category->id]]) ?></li>
            <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
</div>
