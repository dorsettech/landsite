<?php

use App\Model\Enum\PropertyPurpose;
?>

<section class="tab tab1">
    <?php if (isset($contents->red_section->title) && !empty($contents->red_section->title)): ?>
        <div class="question">
            <div class="container">
                <?= $contents->red_section->title ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="tab-navigation">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul class="nav">
                        <li><button href="#tab1-1" class="active" data-toggle="tab" role="tab">Sell</button></li>
                        <li><button href="#tab1-2" class="" data-toggle="tab" role="tab">Buy</button></li>
                        <li><button href="#tab1-3" class="" data-toggle="tab" role="tab">Let</button></li>
                        <li><button href="#tab1-4" class="" data-toggle="tab" role="tab">Rent</button></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-content">
        <div id="tab1-1" class="tab-pane fade show active">
            <div class=""> <?php /* add class bgimage if backgorund needed */ ?>
                <div class="container">
                    <?php if (isset($contents->sell_tab->content) && !empty($contents->sell_tab->content)): ?>
                        <div class="row head">
                            <div class="col-12">
                                <div class="heading"><?= __('Sell') ?></div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-12 col-lg-10 col-xxl-8">
                                <div class="collapser">
                                    <div id="tab_collapses1" class="row">
                                        <?= $contents->sell_tab->content ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (isset($contents->sell_tab->content2) && !empty($contents->sell_tab->content2)): ?>
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <?= $contents->sell_tab->content2 ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div id="tab1-2" class="tab-pane fade">
            <div class="bgimage">
                <div class="container">
                    <div class="row head">
                        <div class="col-12">
                            <div class="heading"><?= __('Buy') ?></div>
                        </div>
                    </div>
                    <?= $this->cell('PropertiesSearch', [PropertyPurpose::SALE]) ?>
                </div>
            </div>
            <div class="">
                <div class="container">
                    <?php if (isset($contents->buy_tab->title) && !empty($contents->buy_tab->title)): ?>
                        <div class="row head">
                            <div class="col-12">
                                <div class="heading"><?= $contents->buy_tab->title ?></div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="row content">
                        <div class="col-12 col-lg-10 col-xxl-8">
                            <div class="row">
                                <div class="col-12 col-md-6 order-1 order-md-2">
                                    <img src="<?= $this->Images->scale($contents->buy_tab->file, 490, 220) ?>" alt="<?= $this->Images->alt($contents->buy_tab->file) ?>" class="img-fluid">
                                </div>
                                <div class="col-12 col-md-6 order-2 order-md-1">
                                    <?php if (isset($contents->buy_tab->content) && !empty($contents->buy_tab->content)): ?>
                                        <?= $contents->buy_tab->content ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="tab1-3" class="tab-pane fade">
            <div class="">
                <div class="container">
                    <?php if (isset($contents->let_tab->content) && !empty($contents->let_tab->content)): ?>
                        <div class="row head">
                            <div class="col-12">
                                <div class="heading"><?= __('Let') ?></div>
                            </div>
                        </div>
                        <?php /* the same as "tabs1-collapse" but there must be another ID -> tab_collapses2 -< and data-target */ ?>
                        <div class="row justify-content-center">
                            <div class="col-12 col-lg-10 col-xxl-8">
                                <div class="collapser">
                                    <div id="tab_collapses2" class="row">
                                        <?= $contents->let_tab->content ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (isset($contents->let_tab->content2) && !empty($contents->let_tab->content2)): ?>
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <?= $contents->let_tab->content2 ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div id="tab1-4" class="tab-pane fade">
            <div class="bgimage">
                <div class="container">
                    <div class="row head">
                        <div class="col-12">
                            <div class="heading"><?= __('Rent') ?></div>
                        </div>
                    </div>
                    <?= $this->cell('PropertiesSearch', [PropertyPurpose::RENT]) ?>
                </div>
            </div>
            <div class="">
                <div class="container">
                    <?php if (isset($contents->rent_tab->title) && !empty($contents->rent_tab->title)): ?>
                        <div class="row head">
                            <div class="col-12">
                                <div class="heading"><?= $contents->rent_tab->title ?></div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="row content">
                        <div class="col-12 col-lg-10 col-xxl-8">
                            <div class="row">
                                <div class="col-12 col-md-6 order-1 order-md-2">
                                    <img src="<?= $this->Images->scale($contents->rent_tab->file, 490, 220) ?>" alt="<?= $this->Images->alt($contents->rent_tab->file) ?>" class="img-fluid">
                                </div>
                                <div class="col-12 col-md-6 order-2 order-md-1">
                                    <?php if (isset($contents->rent_tab->content) && !empty($contents->rent_tab->content)): ?>
                                        <?= $contents->rent_tab->content ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
