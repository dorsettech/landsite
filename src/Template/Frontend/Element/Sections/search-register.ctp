<section class="tab tab2">
    <div class="tab-navigation">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul class="nav">
                        <li><button href="#tab2-1" class="active" data-toggle="tab" role="tab"><?= __('Search for a Professional') ?></button></li>
                        <li><button href="#tab2-2" class="" data-toggle="tab" role="tab"><?= __('Register as a Professional') ?></button></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-content">
        <div id="tab2-1" class="tab-pane fade show active">
            <div class="bgimage">
                <div class="container">
                    <?php if (isset($contents->search_tab->title) && !empty($contents->search_tab->title)): ?>
                        <div class="row head">
                            <div class="col-12">
                                <div class="heading"><?= $contents->search_tab->title ?></div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (isset($contents->search_tab->content) && !empty($contents->search_tab->content)): ?>
                        <div class="row">
                            <div class="col-12">
                                <?= $contents->search_tab->content ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?= $this->cell('ServicesSearch') ?>
                    <?= $this->element('Sections/services-categories') ?>
                </div>
            </div>
        </div>
        <div id="tab2-2" class="tab-pane fade">
            <div class="bgimage">
                <div class="container">
                    <?php if (isset($contents->register_tab->title) && !empty($contents->register_tab->title)): ?>
                        <div class="row head">
                            <div class="col-12">
                                <div class="heading"><?= $contents->register_tab->title ?></div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="row content">
                        <div class="col-12 col-lg-10 col-xxl-8">
                            <div class="row">
                                <div class="col-12 col-md-6 order-1 order-md-2">
                                    <img src="<?= $this->Images->scale($contents->register_tab->file, 490, 220) ?>" alt="<?= $this->Images->alt($contents->register_tab->file) ?>" class="img-fluid">
                                </div>
                                <div class="col-12 col-md-6 order-2 order-md-1">
                                    <?php if (isset($contents->register_tab->content) && !empty($contents->register_tab->content)): ?>
                                        <?= $contents->register_tab->content ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>