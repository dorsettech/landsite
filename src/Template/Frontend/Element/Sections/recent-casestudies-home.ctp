<?php if (isset($recentCaseStudies) && !empty($recentCaseStudies)): ?>
    <section class="posts bgwhite">
        <div class="container">
            <div class="row head">
                <div class="col-12">
                    <div class="heading"><?= __('Case Studies') ?></div>
                </div>
            </div>
            <div class="carousel-overflow">
                <div class="row carousel">
                    <?php foreach ($recentCaseStudies as $caseStudy): ?>
                        <div class="col-12">
                            <?= $this->element('Boxes/case-study-box', ['caseStudy' => $caseStudy]) ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="d-flex justify-content-center mt-5">
                <?= $this->Html->link(__('View All'), ['_name' => 'case-studies-listing'], ['class' => 'btn btn-ghost', 'escape' => false]); ?>
            </div>
        </div>
    </section>
<?php endif; ?>
