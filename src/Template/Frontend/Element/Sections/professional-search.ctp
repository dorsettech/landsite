<section id="search_section" class="search accordion">
    <div id="search_button" class="d-xxl-none collapse show" data-parent="#search_section">
        <button class="btn btn-primary collapsed" data-toggle="collapse" data-target="#search_form"><i class="fas fa-search"></i><?= __('Search professionals') ?></button>
    </div>
    <div id="search_form" class="d-xxl-block collapse" data-parent="#search_section">
        <div class="bgimage">
            <div class="container">
                <?= $this->cell('ServicesSearch') ?>
            </div>
        </div>
    </div>
</section>
<?= $this->cell('SaveSearch', ['services']) ?>