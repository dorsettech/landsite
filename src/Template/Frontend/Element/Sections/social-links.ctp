<?php if (isset($statics['social_facebook']) && !empty($statics['social_facebook'])): ?>
    <?= $this->Html->link('<i class="fab fa-facebook-f fa-fw"></i>', $statics['social_facebook'], ['target' => '_blank', 'class' => 'social-icon', 'escape' => false]) ?>
<?php endif; ?>
<?php if (isset($statics['social_twitter']) && !empty($statics['social_twitter'])): ?>
    <?= $this->Html->link('<i class="fab fa-twitter fa-fw"></i>', $statics['social_twitter'], ['target' => '_blank', 'class' => 'social-icon', 'escape' => false]) ?>
<?php endif; ?>
<?php if (isset($statics['social_linkedin']) && !empty($statics['social_linkedin'])): ?>
    <?= $this->Html->link('<i class="fab fa-linkedin-in fa-fw"></i>', $statics['social_linkedin'], ['target' => '_blank', 'class' => 'social-icon', 'escape' => false]) ?>
<?php endif; ?>
<?php if (isset($statics['social_instagram']) && !empty($statics['social_instagram'])): ?>
    <?= $this->Html->link('<i class="fab fa-instagram fa-fw"></i>', $statics['social_instagram'], ['target' => '_blank', 'class' => 'social-icon', 'escape' => false]) ?>
<?php endif; ?>
<?php if (isset($statics['social_pinterest']) && !empty($statics['social_pinterest'])): ?>
    <?= $this->Html->link('<i class="fab fa-pinterest fa-fw"></i>', $statics['social_pinterest'], ['target' => '_blank', 'class' => 'social-icon', 'escape' => false]) ?>
<?php endif; ?>
<?php if (isset($statics['social_youtube']) && !empty($statics['social_youtube'])): ?>
    <?= $this->Html->link('<i class="fab fa-youtube fa-fw"></i>', $statics['social_youtube'], ['target' => '_blank', 'class' => 'social-icon','escape' => false]) ?>
<?php endif; ?>
<?php if (isset($statics['social_gplus']) && !empty($statics['social_gplus'])): ?>
    <?= $this->Html->link('<i class="fab fa-gplus fa-fw"></i>', $statics['social_gplus'], ['target' => '_blank', 'class' => 'social-icon', 'escape' => false]) ?>
<?php endif; ?>
