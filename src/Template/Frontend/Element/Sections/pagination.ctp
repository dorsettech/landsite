<?php if ($this->Paginator->total() > 1) : ?>
    <ul class="pagination">
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next() ?>
    </ul>
<?php endif; ?>
