<?php

use App\Settings;
?>
<base href="<?= $this->Url->build('/', true); ?>">
<link rel="icon" href="img/favicon.ico">
<link rel="apple-touch-icon" href="img/apple-touch-icon.png">
<?= $this->fetch('css') ?>
<?= $this->fetch('script') ?>
<?= $this->Html->script('/js/jquery-3.1.1.min.js') ?>
<?= $this->Html->script('https://maps.googleapis.com/maps/api/js?libraries=places&key='.Settings::get('google-api.maps')) ?>
<?= $this->Html->css('/js/libraries/sweetalert/sweetalert.min.css') ?>
<?= $this->Html->css('/css/style.min.css') ?>
<?= $this->Html->css('/css/custom.css?v='.time()) ?>
<?php if (isset($page) && !empty($page->css)): ?>
    <style type="text/css">
    <?= $page->css ?>
    </style>
<?php endif; ?>
<?php if (!empty($statics['scripts_head'])) : ?>
    <?= $statics['scripts_head']; ?>
<?php endif; ?>