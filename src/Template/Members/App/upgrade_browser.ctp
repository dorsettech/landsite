<?php
    $this->assign('title', "The Land Site - verify your email address");

    $this->start('css-head');
        echo $this->Html->css('/css/style.min.css') . "\n\t";
    $this->end('css-head');
?>
<div class="account">
    <div class="container">
        <div class="row justify-content-center">
            <div class="logo col">
                <a href="/">
                    <img src="/members/assets/logo.png" alt="logo" class="img-fluid">
                </a>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-md-8 col-xxl-6">
                <div class="login accordion">
                    <div class="content">
                        <div class="heading">
                            Please upgrade your browser to use The Landsite
                        </div>
                        <div class="row">
                            <div class="col-12 text-center">
                                <p>We built The Landsite using the latest techniques and technologies. This makes The Landsite faster and easier to use. Unfortunately, your browser doesn't support those technologies.</p>
                                <p>Download one of these great browsers and you'll be on your way:</p>
                                <br/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col text-center" style="margin-bottom:20px">
                                <img src="/members/assets/edge_128x128.png" alt="Microsoft Edge" class="d-inline-block" style="max-width:65px;margin-bottom:10px"/>
                                <a href="https://www.microsoft.com/en-US/windows/microsoft-edge" class="d-block">Microsoft Edge</a>
                                <small>Version 44.0+</small>
                            </div>
                            <div class="col text-center" style="margin-bottom:20px">
                                <img src="/members/assets/firefox_128x128.png" alt="Mozilla Firefox" class="d-inline-block" style="max-width:65px;margin-bottom:10px"/>
                                <a href="https://www.mozilla.org/en-US/firefox/new/" class="d-block">Mozilla Firefox</a>
                                <small>Version 66.0+</small>
                            </div>
                            <div class="col text-center" style="margin-bottom:20px">
                                <img src="/members/assets/chrome_128x128.png" alt="Google Chrome" class="d-inline-block" style="max-width:65px;margin-bottom:10px"/>
                                <a href="https://www.google.com/intl/en_ALL/chrome/" class="d-block">Google Chrome</a>
                                <small>Version 74.0+</small>
                            </div>
                            <div class="col text-center" style="margin-bottom:20px">
                                <img src="/members/assets/opera_128x128.png" alt="Opera" class="d-inline-block" style="max-width:65px;margin-bottom:10px"/>
                                <a href="https://www.opera.com/en/download" class="d-block">Opera</a>
                                <small>Version 60.0+</small>
                            </div>
                            <div class="col text-center" style="margin-bottom:20px">
                                <img src="/members/assets/safari_128x128.png" alt="Apple Safari" class="d-inline-block" style="max-width:65px;margin-bottom:10px"/>
                                <a href="https://www.apple.com/safari/" class="d-block">Apple Safari</a>
                                <small>Version 12.1+</small>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 text-center">
                                <br/>
                                <small>Already upgraded but still having problems? Contact us <a href="mailto:info@thelandsite.co.uk">info@thelandsite.co.uk</a></small>
                            </div>
                        </div>
                        <img src="/members/assets/image.jpg" class="step1image">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
