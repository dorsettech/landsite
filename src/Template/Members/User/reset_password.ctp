<?php
    $this->assign('title', "The Land Site - reset your password");

    $this->start('css-head');
        echo $this->Html->css('/css/style.min.css') . "\n\t";
        echo $this->Html->css('/members/css/common.css') . "\n";
        echo $this->Html->css('/members/css/login.css') . "\n";
    $this->end('css-head');

    $this->start('script-appended');

        /*
         * On production server (not only mode), it is possible to combine
         * multiple files to final one directly from CDN
         */
        echo $this->Html->script(
            $isProduction ? [
                '//cdn.jsdelivr.net/combine/npm/vue@2.6.8/dist/vue.min.js,npm/axios@0.18.0/dist/axios.min.js,npm/js-cookie@2.2.0/src/js.cookie.min.js'
            ] : [
                '//cdn.jsdelivr.net/npm/vue/dist/vue.js',
                '//cdn.jsdelivr.net/npm/axios@0.18.0/dist/axios.min.js',
                '//cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js'
            ]
        );

        if ($isProduction) {
            echo $this->Html->script('/statics/map/0b0001') . "\n\t";
            echo $this->Html->script('/members/dist/login-build.js') . "\n\t";
        } else {
            echo $this->Html->script('/statics/map/0b0001?' . rand()) . "\n\t";
            echo $this->Html->script('/members/login.js', ['type' => 'module']) . "\n\t";
            echo $this->Html->script('/members/dist/login-build.js', ['nomodule']) . "\n\t";
        }
    $this->end('script-appended');
?>
<loader ref="PageLoader" :active=true></loader>
<reset-password-form token="<?=$token?>"></reset-password-form>
