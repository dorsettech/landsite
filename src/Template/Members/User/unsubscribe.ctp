<?php
    $this->assign('title', "The Land Site - unsubscribe");

    $this->start('css-head');
        echo $this->Html->css('/css/style.min.css') . "\n\t";
        echo $this->Html->css('/members/css/common.css') . "\n";
        echo $this->Html->css('/members/css/login.css') . "\n";
    $this->end('css-head');

    $this->start('script-appended');

        /*
         * On production server (not only mode), it is possible to combine
         * multiple files to final one directly from CDN
         */
        echo $this->Html->script(
            $isProduction ? [
                '//cdn.jsdelivr.net/combine/npm/vue@2.6.8/dist/vue.min.js,npm/axios@0.18.0/dist/axios.min.js,npm/js-cookie@2.2.0/src/js.cookie.min.js'
            ] : [
                '//cdn.jsdelivr.net/npm/vue/dist/vue.js',
                '//cdn.jsdelivr.net/npm/axios@0.18.0/dist/axios.min.js',
                '//cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js'
            ]
        );

        if ($isProduction) {
            echo $this->Html->script('/statics/map/0b0001') . "\n\t";
            echo $this->Html->script('/members/dist/login-build.js') . "\n\t";
        } else {
            echo $this->Html->script('/statics/map/0b0001?' . rand()) . "\n\t";
            echo $this->Html->script('/members/login.js', ['type' => 'module']) . "\n\t";
            echo $this->Html->script('/members/dist/login-build.js', ['nomodule']) . "\n\t";
        }
    $this->end('script-appended');
?>
<loader ref="PageLoader" :active=true></loader>
<unsubscribe-form></unsubscribe-form>

<div class="account">
	<div class="container">
		<div class="row justify-content-center">
			<div class="logo col">
				<a href="/"><img src="/members/assets/logo.png" alt="logo" class="img-fluid"></a>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-12 col-md-6 col-xxl-4">
				<div class="login accordion">
					<div class="content">
						<div class="heading">Unsubscribe</div>
						<form class="v-comp-forgot-password-form" method="get">
							<div class="form-row">
								<p class="info">Enter your email address to be removed from our mailing list.</p>
							</div>
							<div class="form-row">
								<div class="form-group col-12">
									<input type="email" name="email" placeholder="Email *" required="required" class="form-control" value="<?php echo (isset($_GET['email']) ? $_GET['email'] : '' ); ?>">
								</div>
								<div class="form-group col-12">
									<button type="submit" class="unsubscribe btn btn-primary">Unsubscribe</button>
								</div>
								
								<?php if (isset($_GET['email'])) : ?>
									<div class="form-group col-12">
										<div role="alert" class="alert text-center text-success">
											Your email address '<?php echo htmlentities($_GET['email']); ?>' has been successfully removed from our mailing list.
										</div>
									</div>
								<?php endif; ?>
								
								<div class="signin col-12 mt-3">
									Access your account <a href="/login">Sign-in now</a>
								</div>
							</div>
						</form>
						<img src="/members/assets/image.jpg" class="step1image">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

