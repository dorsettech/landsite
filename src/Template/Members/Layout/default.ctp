<?= $this->Html->docType() ?>
<html>
<head>
    <!-- prevent caching -->
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Expires" content="0"/>
    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <?php
        echo $this->Html->meta('description', $appDescription) . "\n\t";
        echo $this->Html->meta('keywords', $appKeywords) . "\n\t";
        echo $this->Html->meta('author', $appAuthor) . "\n\t";
        echo $this->Html->meta('application-name', $appName) . "\n\t";
        echo $this->Html->meta('application-tooltip', $appDescription);
        echo $this->Html->meta('csrf-token', $csrfToken);
    ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <?php
        echo $this->Html->meta(['http-equiv' => 'Content-Language', 'content' => $appLanguage]);
    ?>
    <meta name="robots" content="noindex"/>
    <link rel="dns-prefetch" href="//fonts.gstatic.com" />
    <link rel="dns-prefetch" href="//fonts.googleapis.com" />
    <?= $this->Html->meta('icon') ?>
    <title><?= $this->fetch('title') ?></title>
    <?= $this->fetch('css-head') ?>
    <?= $this->fetch('script-head') ?>

    <script>dataLayer = [];</script>
    <?php if ($isProduction) { ?>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-KLCCG6Q');</script>
    <?php } ?>
</head>
<body>
<?php if ($isProduction) { ?>
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KLCCG6Q" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<?php } ?>

<?php
    echo $this->Html->tag('div', null, [
        'id' => 'app',
        'class' => 'app',
        'v-bind' => json_encode([
        'data-is-production' => $isProduction ? 'true' : 'false',
        'data-name' => $appName,
        'data-version' => $appVersion,
        'data-build' => $appBuildVersion,
        'data-revision' => $appRevisionString,
        'data-language' => $appLanguage,
        'data-company' => $appCompany
        ])
    ]) . "\n";
?>
        <?= $this->fetch('content') ?>
    </div>

    <?= $this->fetch('script-appended') ?>
</body>
</html>
