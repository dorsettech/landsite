<?php
    $this->assign('title', "The Land Site - Dashboard");

    $this->start('css-head');
        echo $this->Html->css('//fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic,900,900italic') . "\n\t";
        echo $this->Html->css('//fonts.googleapis.com/icon?family=Material+Icons') . "\n\t";
        echo $this->Html->css('//use.fontawesome.com/releases/v5.7.2/css/all.css') . "\n\t";

        echo $this->Html->css(
            $isProduction ? [
                '//cdn.jsdelivr.net/combine/npm/vue-snotify@3.2.1/styles/material.min.css,npm/flatpickr@4.5.7/dist/flatpickr.min.css,npm/@riophae/vue-treeselect@0.0.38/dist/vue-treeselect.min.css,npm/vue-directive-tooltip@1.5.1/css/index.min.css'
            ] : [
                '//cdn.jsdelivr.net/npm/vue-snotify@3.2.1/styles/material.min.css',
                '//cdn.jsdelivr.net/npm/flatpickr@4.5.7/dist/flatpickr.min.css',
                '//cdn.jsdelivr.net/npm/@riophae/vue-treeselect@0.0.38/dist/vue-treeselect.min.css',
                '//cdn.jsdelivr.net/npm/vue-directive-tooltip@1.5.1/css/index.min.css'
            ]
        ) . "\n\t";

        echo $this->Html->css('/members/css/ca/bootstrap.min.css') . "\n";
        echo $this->Html->css('/members/css/ca/animate.css') . "\n";
        echo $this->Html->css('/members/css/ca/material/style.min.css') . "\n";
        echo $this->Html->css('/members/css/ca/material/style-responsive.min.css') . "\n";
        echo $this->Html->css('/members/css/ca/material/theme/default.css') . "\n";
        echo $this->Html->css('/members/css/common.css') . "\n";
        echo $this->Html->css('/members/css/app.css') . "\n";
    $this->end('css-head');

    $this->start('script-appended');
        /*
         * On production server (not only mode), it is possible to combine
         * multiple files to final one directly from CDN
         */
        echo $this->Html->script(
            $isProduction ? [
                '//cdn.jsdelivr.net/combine/npm/vue@2.6.8,npm/vue-router@3.0.2,npm/axios@0.18.0/dist/axios.min.js,npm/vue-snotify@3.2.1/vue-snotify.min.js,npm/flatpickr@4.5.7,npm/vue-flatpickr-component@8.1.1,npm/js-cookie@2.2.0,npm/vue2-editor@2.6.6,npm/vue-simple-suggest@1.8.3/dist/iife.min.js,npm/vuetable-2@2.0.0-beta.4/dist/vuetable-2-full.min.js,npm/chart.js@2.8.0,npm/vue-chartjs@3.4.0,npm/@riophae/vue-treeselect@0.0.38,npm/v-money@0.8.1,npm/vue-directive-tooltip@1.5.1,npm/animated-number-vue@0.1.4',
                '//js.stripe.com/v3/'
            ] : [
                '//cdn.jsdelivr.net/npm/vue/dist/vue.js',
                '//cdn.jsdelivr.net/npm/vue-router@3.0.2/dist/vue-router.min.js',
                '//cdn.jsdelivr.net/npm/axios@0.18.0/dist/axios.min.js',
                '//cdn.jsdelivr.net/npm/vue-snotify@3.2.1/vue-snotify.min.js',
                '//cdn.jsdelivr.net/npm/vuetable-2@2.0.0-beta.4/dist/vuetable-2-full.min.js',
                '//cdn.jsdelivr.net/npm/flatpickr@4.5.7/dist/flatpickr.min.js',
                '//cdn.jsdelivr.net/npm/vue-flatpickr-component@8.1.1/dist/vue-flatpickr.min.js',
                '//cdn.jsdelivr.net/npm/js-cookie@2.2.0/src/js.cookie.min.js',
                '//cdn.jsdelivr.net/npm/vue2-editor@2.6.6/dist/vue2-editor.min.js',
                '//cdn.jsdelivr.net/npm/vue-simple-suggest@1.8.3/dist/iife.min.js',
                '//cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js',
                '//cdn.jsdelivr.net/npm/vue-chartjs@3.4.0/dist/vue-chartjs.min.js',
                '//cdn.jsdelivr.net/npm/@riophae/vue-treeselect@0.0.38/dist/vue-treeselect.min.js',
                '//cdn.jsdelivr.net/npm/v-money@0.8.1/dist/v-money.min.js',
                '//cdn.jsdelivr.net/npm/vue-directive-tooltip@1.5.1/dist/vueDirectiveTooltip.min.js',
                '//cdn.jsdelivr.net/npm/animated-number-vue@0.1.4/dist/AnimatedNumber.umd.min.js',
                '//js.stripe.com/v3/'
            ]
        );

        if ($isProduction) {
            echo $this->Html->script('/statics/map/0b0110') . "\n\t";
            echo $this->Html->script('/members/dist/app-build.js') . "\n\t";
        } else {
            echo $this->Html->script('/statics/map/0b0110?' . rand()) . "\n\t";
            echo $this->Html->script('/members/app.js', ['type' => 'module']) . "\n\t";
            echo $this->Html->script('/members/dist/app-build.js', ['nomodule']) . "\n\t";
        }
    $this->end('script-appended');
?>
<loader ref="PageLoader" :active=true></loader>
<dashboard ref="Dashboard" v-cloak raw-config="<?=base64_encode(json_encode($config))?>"></dashboard>
