<?= $this->Form->create(null, ['id' => 'report-settings']) ?>
<div class="row">
    <div class="col-sm-6 col-lg-2"><?= $this->Form->control('type', ['options' => $reports, 'label' => 'Report type', 'empty' => __('Select report type'), 'required' => true]); ?></div>
    <div class="col-sm-6 col-lg-1"><?= $this->Form->control('from', ['type' => 'text', 'empty' => true, 'class' => 'date', 'required' => true]); ?></div>
    <div class="col-sm-6 col-lg-1"><?= $this->Form->control('to', ['type' => 'text', 'empty' => true, 'class' => 'date', 'required' => true]); ?></div>
    <div class="col-auto"><button class="btn btn-primary m-t-25" type="submit" id="report-submit"><i class="fas fa-cog fa-spin m-r-5 s hide"></i> <span>Generate and download</span></button></div>
</div>
<?= $this->Form->end() ?>
