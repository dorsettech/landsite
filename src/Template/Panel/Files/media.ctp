<div class="col-xs-12">
    <div class="h4"><?= __('Select file you want to add to editor'); ?></small></div>
    <div class="superbox" data-offset="50">
        <?php
        foreach ($files as $file):
            if (file_exists($file) && (strpos(mime_content_type($file), 'image') === 0)):
                ?>
                <div class="superbox-list media-file-select" data-ckeditor="<?= $funcNum ?>" data-url="<?= $file ?>" >
                    <img src="<?= $this->Images->scale($file, 150, 150, 'ratio_crop') ?>" data-img="<?= $this->Images->scale($file, 640, 640, 'ratio') ?>" class="superbox-img" alt="<?= $this->Images->alt($file) ?>"/>
                </div>
            <?php elseif (file_exists($file)): ?> 
                <div class="text-center media-file-select" style="width: 12.5%; display: inline-block;" data-ckeditor="<?= $funcNum ?>" data-url="<?= $file ?>" >
                    <i class="fa fa-4x fa-file"></i>
                    <p><?= $file ?></p>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
</div>