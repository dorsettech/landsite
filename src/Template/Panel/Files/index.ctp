<div class="row">
    <?= $this->Form->create(null, ['url' => ['prefix' => 'panel', 'controller' => 'Files', 'action' => 'uploadFiles'], 'type' => 'file']) ?>
    <div class="col-xs-12">
        <?= $this->Form->control('images[]', ['type' => 'file', 'label' => 'Add files', 'multiple' => true]) ?>
        <?= $this->Form->button(__('Upload files'), ['bootstrap-type' => 'primary', 'bootstrap-size' => 'md', 'class' => 'pull-right', 'style' => 'margin-bottom: 15px;']); ?>
    </div>
    <?= $this->Form->end() ?>
</div>

<div class="clearfix">
    <div class="superbox" data-offset="50">
        <?php
        foreach ($files as $file):
            if (file_exists($file) && (strpos(mime_content_type($file), 'image') === 0)) : ?>
                <div class="superbox-list">
                    <img src="<?= $this->Images->scale($file, 150, 150, 'ratio_crop') ?>" data-img="<?= $this->Images->scale($file, 640, 640, 'ratio') ?>" class="superbox-img" alt="<?= $this->Images->alt($file) ?>"/>
                </div>
            <?php elseif (file_exists($file)): ?>
                <div class="text-center" style="width: 12.5%; display: inline-block;">
                    <a href="<?= $file ?>" target="_blank"><i class="fa fa-4x fa-file"></i></a>
                    <p><?= $file ?></p>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
</div>
