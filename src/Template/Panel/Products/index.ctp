<div class="table-responsive">
    <?php if ($products->count() > 0) : ?>
        <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th class="width-200"><?= $this->Paginator->sort('name') ?></th>
                    <th><?= $this->Paginator->sort('description') ?></th>
                    <th><?= $this->Paginator->sort('created') ?> / <?= $this->Paginator->sort('modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($products as $product): ?>
                    <tr>
                        <td><?= $this->Number->format($product->id) ?></td>
                        <td><?= h($product->name) ?></td>
                        <td><?= h($product->description) ?></td>
                        <td><?= $this->Html->badge('<i class="far fa-clock"></i> ' . h($this->Transform->date($product->created)), 'light') ?><br><?= $this->Html->badge('<i class="fa fa-clock"></i> ' . h($this->Transform->date($product->modified)), 'light') ?></td>
                        <td class="actions">
                            <div class="btn-group btn-group-xs">
                                <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $product->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Edit'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                                <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $product->id], ['class' => 'btn btn-danger', 'confirm' => __('Please confirm removal Product #{0}', $product->id), 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else : ?>
        <div class="well text-center text-warning"><?= __('There are no Products yet.') ?> <?= $this->Html->link(__('Add first Product'), ['action' => 'add']) ?></div>
    <?php endif; ?>
</div>
<div class="row action-buttons">
    <div class="col text-right">
        <?= $this->Html->link(__('Add Product'), ['action' => 'add'], ['class' => 'btn btn-primary']); ?>
    </div>
</div>
<?= $this->element('pagination') ?>
