<div class="table-responsive">
    <?php if ($areas->count() > 0) : ?>
        <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('name') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($areas as $area): ?>
                    <tr>
                        <td><?= $this->Number->format($area->id) ?></td>
                        <td><?= h($area->name) ?></td>
                        <td><?= $this->Html->badge('<i class="far fa-clock"></i> ' . h($this->Transform->date($area->created)), 'light') ?></td>
                        <td class="actions">
                            <div class="btn-group btn-group-xs">
                                <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $area->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Edit'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                                <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $area->id], ['class' => 'btn btn-danger', 'confirm' => __('Please confirm removal Area #{0}', $area->id), 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else : ?>
        <div class="well text-center text-warning"><?= __('There are no Areas yet.') ?> <?= $this->Html->link(__('Add first Area'), ['action' => 'add']) ?></div>
    <?php endif; ?>
</div>
<div class="row action-buttons">
    <div class="col text-right">
        <?= $this->Html->link(__('Add Area'), ['action' => 'add'], ['class' => 'btn btn-primary']); ?>
    </div>
</div>
<?= $this->element('pagination') ?>
