<?php if ($property->deleted) : ?><div class="alert alert-warning"><?= __('This property has been marked as deleted.') ?> <?= __('Reason: {0}', $property->deletion_reason) ?></div><?php endif; ?>
<ul class="nav nav-tabs" id="page-tabs" role="tablist">
    <li class="nav-item">
        <?= $this->Html->link(__('Property Details'), '#tab-property-details-content', ['class' => 'nav-link settings active', 'id' => 'tab-property-details', 'aria-controls' => 'tab-property-details-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
    </li>
    <li class="nav-item">
        <?= $this->Html->link(__('Additional Options'), '#tab-additional-options-content', ['class' => 'nav-link settings', 'id' => 'tab-other-details', 'aria-controls' => 'tab-additional-options-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
    </li>
    <li class="nav-item">
        <?= $this->Html->link(__('Stats'), '#tab-stats-content', ['class' => 'nav-link settings', 'id' => 'tab-stats', 'aria-controls' => 'tab-stats-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
    </li>
</ul>
<div class="tab-content" id="tabs-content">
    <div class="tab-pane fade show active" id="tab-property-details-content" role="tabpanel" aria-labelledby="tab-property-details">
        <div class="p-15">

            <div class="row view m-b-15">
                <div class="col-sm-6 col-lg-3">
                    <label><i class="far fa-building"></i> <?= __('Company Name') ?></label>
                    <?= $property->company ?: '-' ?>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <label><i class="far fa-user"></i> <?= __('First Name, Last Name') ?></label>
                    <?= $property->user->full_name ?>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <label><i class="far fa-envelope"></i> <?= __('Email Address') ?></label>
                    <a href="<?= $this->Url->build('mailto:' . $property->user->email) ?>"><?= $property->user->email ?></a>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <label><i class="fas fa-phone"></i> <?= __('Phone') ?></label>
                    <?= $property->user->phone ?>
                </div>
            </div>

            <hr>

            <div class="row view m-b-15">
                <div class="col-sm-6 col-lg-4">
                    <label><i class="fas fa-tag"></i> <?= __('Title') ?></label>
                    <?= $property->title ?>
                </div>
                <div class="col-sm-6 col-lg-2">
                    <label><i class="fas fa-tag"></i> <?= __('Type') ?></label>
                    <?= $property->property_type->name ?>
                </div>
                <div class="col-sm-6 col-lg-2">
                    <label><i class="fas fa-dot-circle"></i> <?= __('Purpose') ?></label>
                    <?= $property->purpose_name ?>
                </div>
                <div class="col-sm-6 col-lg-2">
                    <label><i class="fas fa-pound-sign"></i> <?= __('Price Sale') ?></label>
                    <?= $property->price_sale ?>
                </div>
                <div class="col-sm-6 col-lg-2">
                    <label><i class="fas fa-pound-sign"></i> <?= __('Price Rent') ?></label>
                    <?= $property->price_rent ?>
                </div>
            </div>

            <hr>

            <div class="row view m-b-15">
                <div class="col-sm-6 col-lg-3">
                    <label><i class="fas fa-map-marker"></i> <?= __('Location, Postcode') ?></label>
                    <?= $property->location ?>, <?= $property->postcode ?>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <label><i class="fas fa-map"></i> <?= __('Lat, Lng') ?></label>
                    <?= $property->lat ?><?= (!empty($property->lat) && !empty($property->lat) ? ',' : '') ?> <?= $property->lng ?>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <label><i class="fas fa-tag"></i> <?= __('Status') ?></label>
                    <?= $property->status_name ?>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <label><i class="fas fa-clock"></i> <?= __('Publish Date') ?></label>
                    <?= $this->Transform->date($property->publish_date, 'datetimeuk') ?>
                </div>
            </div>

            <hr>

            <div class="row view m-b-15">
                <div class="col-sm-6">
                    <label><i class="far fa-plus-square"></i> <?= __('Headline') ?></label>
                    <?= $property->headline ?>
                </div>
                <div class="col-sm-6">
                    <label><i class="fas fa-globe"></i> <?= __('Website URL') ?></label>
                    <?= $property->website_url ?>
                </div>
            </div>

            <hr>

            <h4><?= __('Description') ?></h4>
            <?= $property->description ?>
        </div>
    </div>
    <div class="tab-pane fade" id="tab-additional-options-content" role="tabpanel" aria-labelledby="tab-additional-options">
        <div class="p-15">

            

        </div>
    </div>
    <div class="tab-pane fade" id="tab-stats-content" role="tabpanel" aria-labelledby="tab-stats">
        <div class="p-15">

            <div class="row view m-b-15">
                <div class="col-sm-6">
                    <label><i class="fab fa-adversal"></i> <?= __('Ad Type') ?></label>
                    <?= $property->ad_type_name ?>
                </div>
                <div class="col-sm-6">
                    <label><i class="far fa-clock"></i> <?= __('Ad Expiry Date') ?></label>
                    <?= $this->Transform->date($property->ad_expiry_date, 'datetime') ?>
                </div>
            </div>

            <hr>

            <div class="row view m-b-15">
                <div class="col-sm-6 col-lg-3">
                    <label><i class="fas fa-pound-sign"></i> <?= __('Ad Cost') ?></label>
                    <?= $property->ad_cost ?>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <label><i class="fas fa-pound-sign"></i> <?= __('Ad Total Cost') ?></label>
                    <?= $property->ad_total_cost ?>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <label><i class="fas fa-clipboard-list"></i> <?= __('Enquiries') ?></label>
                    <?= $property->enquiries ?>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <label><i class="fas fa-eye"></i> <?= __('Views') ?></label>
                    <?= $property->views ?>
                </div>
            </div>

        </div>
    </div>
</div>

<?= $this->element('Structure/view-actions', ['id' => $property->id, 'disableEdit' => $property->deleted]) ?>