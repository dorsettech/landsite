<?php

use App\Model\Enum\State;

?>
<?php if ($properties->count() > 0) : ?>
    <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('title') ?></th>
                    <th><?= __('Image') ?></th>
                    <th><?= $this->Paginator->sort('purpose') ?></th>
                    <th><?= $this->Paginator->sort('type_id') ?></th>
                    <th><?= $this->Paginator->sort('status') ?></th>
                    <th><?= $this->Paginator->sort('state') ?></th>
                    <th><?= $this->Paginator->sort('user_id') ?></th>
                    <th><?= __('Company') ?></th>
                    <th><?= $this->Paginator->sort('ad_expiry_date', ['title' => __('Expiry Date')]) ?></th>
                    <th><?= $this->Paginator->sort('enquiries') ?></th>
                    <th><?= $this->Paginator->sort('views') ?></th>
                    <th><?= $this->Paginator->sort('visibility', ['label' => 'Visible']) ?></th>
                    <th><?= $this->Paginator->sort('send_in_email', ['label' => 'In email']) ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($properties as $property): ?>
                    <tr<?= $property->deleted ? ' class="deleted"' : '' ?>>
                        <td><?= $this->Number->format($property->id) ?></td>
                        <td>
                            <?php if ($property->under_offer) : ?><?= $this->Html->badge(__('Under Offer'), 'info', ['class' => 'float-right']) ?><?php endif; ?>
                            <?= h($property->title) ?>
                        </td>
                        <td><?php if (!empty($property->main_image)) : ?><?= $this->Html->link($this->Html->image('/' . $this->Images->scale($property->main_image, 30, 30), ['class' => 'img-thumbnail']), $property->main_image, ['class' => 'magnific-image', 'escape' => false]) ?><?php endif; ?></td>
                        <td><?= $property->purpose ?></td>
                        <td><?= $property->has('property_type') ? $this->Html->link($property->property_type->name, ['controller' => 'PropertyTypes', 'action' => 'edit', $property->property_type->id]) : '' ?></td>
                        <td><?= h($property->status) ?></td>
                        <td><?= h($property->state) ?></td>
                        <td><?= $property->has('user') ? $this->Html->link($property->user->full_name, ['controller' => 'Users', 'action' => 'edit', $property->user->id]) : '' ?></td>
                        <td><?= $property->has('user') && $property->user->has('user_detail') ? $this->Html->link($property->user->user_detail->company, ['controller' => 'Users', 'action' => 'edit', $property->user->id]) : '' ?></td>
                        <td><?= $this->Html->badge('<i class="far fa-clock"></i> ' . h($this->Transform->date($property->ad_expiry_date)), 'light') ?></td>
                        <td><?= $this->Number->format($property->enquiries) ?></td>
                        <td><?= $this->Number->format($property->views) ?></td>
                        <td><?= $this->Switcher->create('visibility', ['val' => $property->visibility, 'data-model' => 'Properties', 'data-id' => $property->id]) ?></td>
                        <td><?= $this->Switcher->create('send_in_email', ['val' => $property->send_in_email, 'data-model' => 'Properties', 'data-id' => $property->id]) ?></td>
                        <td class="actions">
                            <div class="btn-group btn-group-xs">
                                <?php if ($property->state === State::PENDING && !$property->deleted && $property->visibility): ?>
                                    <?= $this->Html->link('<i class="fa fa-check-circle"></i>', '#', ['class' => 'btn btn-default approve-reject-property-button', 'escape' => false, 'title' => __('Approve/Reject'), 'data-toggle' => 'modal', 'data-target' => '#approve-reject-property-modal', 'data-id' => $property->id]) ?>
                                <?php endif; ?>
                                <?php if ($property->deleted) : ?>
                                <?= $this->Html->link('<i class="fa fa-eye"></i> Deleted', ['action' => 'view', $property->id], ['class' => 'btn btn-default', 'escape' => false, 'title' => __('View'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                                <?php else : ?>
                                <?= $this->Html->link('<i class="fa fa-eye"></i>', ['action' => 'view', $property->id], ['class' => 'btn btn-default', 'escape' => false, 'title' => __('View'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                                <?= $this->Html->link('<i class="fa fa-images"></i>', ['controller' => 'PropertyMedia', 'action' => 'list', $property->id], ['class' => 'btn btn-info', 'escape' => false, 'title' => __('Media'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                                <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $property->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Edit'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                                <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $property->id], ['class' => 'btn btn-danger', 'confirm' => __('Please confirm removal Property #{0}', $property->id), 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                                <?php endif; ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php else : ?>
    <div class="well text-center text-warning"><?= __('There are no Properties yet.') ?> <?= $this->Html->link(__('Add first Property'), ['action' => 'add']) ?></div>
<?php endif; ?>
<div class="row action-buttons">
    <div class="col text-right">
        <?= $this->Html->link(__('Add Property'), ['action' => 'add'], ['class' => 'btn btn-primary']); ?>
    </div>
</div>
<?= $this->element('pagination') ?>
<?= $this->element('Structure/approve-reject-property-modal') ?>
