<?= $this->Form->create($property) ?>
<?= $this->element('Structure/action-buttons') ?>

<hr>

<div class="row">
    <div class="col-md-8 col-lg-9">

        <h4><?= __('Details') ?></h4>
        <div class="row">
            <div class="col-sm-6 col-lg-5"><?= $this->Form->control('title'); ?></div>
            <div class="col-sm-6 col-lg-5"><?= $this->Form->control('slug'); ?></div>
            <div class="col-sm-12 col-lg-2"><?= $this->Form->control('reference'); ?></div>
            <div class="col-sm-6 col-lg-2"><?= $this->Form->control('user_id', ['options' => $users, 'empty' => __('Please choose')]); ?></div>
            <div class="col-sm-6 col-lg-2"><?= $this->Form->control('type_id', ['options' => $propertyTypes, 'empty' => __('Please choose'), 'class' => 'property-type']); ?></div>
            <div class="col-12 col-lg-2"><?= $this->Form->control('purpose', ['options' => $purposes, 'empty' => __('Please choose')]); ?></div>
            <div class="col-12 col-lg-6"><?= $this->Form->control('comm_use_class', ['multiple' => true, 'options' => $commonUseClass, 'class' => 'select2', 'label' => __('Common Use Class')]); ?></div>
            <div class="col-6 col-sm-2"><?= $this->Form->control('price_sale'); ?></div>
            <div class="col-6 col-sm-2"><?= $this->Form->control('price_sale_per', ['type' => 'select', 'options' => $priceSaleTypes]); ?></div>
            <div class="col-6 col-sm-2"><?= $this->Form->control('price_sale_qualifier', ['type' => 'select', 'options' => $priceSaleQualifiers]); ?></div>
            <div class="col-6 col-sm-2"><?= $this->Form->control('price_rent'); ?></div>
            <div class="col-6 col-sm-2"><?= $this->Form->control('price_rent_per', ['type' => 'select', 'options' => $priceRentTypes]); ?></div>
            <div class="col-6 col-sm-2"><?= $this->Form->control('price_rent_qualifier', ['type' => 'select', 'options' => $priceRentQualifiers]); ?></div>
            <div class="col-sm-6 col-md-4"><?= $this->Form->control('location'); ?></div>
            <div class="col-sm-6 col-md-2"><?= $this->Form->control('town'); ?></div>
            <div class="col-sm-6 col-md-2"><?= $this->Form->control('postcode'); ?></div>
            <div class="col-6 col-sm-2"><?= $this->Form->control('lat'); ?></div>
            <div class="col-6 col-sm-2"><?= $this->Form->control('lng'); ?></div>

            <div class="col-12"><?= $this->Form->control('headline'); ?></div>
            <div class="col-12"><?= $this->Form->control('description', ['type' => 'textarea', 'class' => 'ckeditor']); ?></div>
            <div class="col-12"><?= $this->Form->control('website_url'); ?></div>

            <div class="col-sm-6 col-lg-3"><?= $this->Form->control('status', ['options' => $statuses, 'empty' => __('Please choose')]); ?></div>
            <div class="col-sm-6 col-lg-3"><?= $this->Form->control('publish_date', ['type' => 'text', 'empty' => true, 'class' => 'datetime', 'val' => $this->Transform->date($property->publish_date)]); ?></div>

            <div class="col-sm-6 col-lg-2"><?= $this->Form->control('under_offer', ['type' =>'checkbox']); ?></div>
            <div class="col-sm-6 col-lg-2"><?= $this->Form->control('auction', ['type' =>'checkbox']); ?></div>
            <div class="col-sm-6 col-lg-2"><?= $this->Form->control('visibility', ['type' =>'checkbox']); ?></div>
        </div>

        <hr>

        <h4><?= __('SEO') ?></h4>

        <div class="row">
            <div class="col-12"><?= $this->Form->control('seo_title', ['label' => ['text' => __('Title SEO'), 'escape' => false], 'id' => 'seo_title']); ?></div>
            <div class="col-12"><?= $this->Form->control('seo_keywords', ['label' => ['text' => __('Keywords SEO'), 'escape' => false], 'id' => 'seo_keywords']); ?></div>
            <div class="col-12"><?= $this->Form->control('seo_description', ['label' => ['text' => __('Description SEO'), 'escape' => false], 'id' => 'seo_description']); ?></div>
        </div>

        <hr>

        <h4><?= __('Ads') ?></h4>
        <div class="row">
            <div class="col-sm-6"><?= $this->Form->control('ad_type'); ?></div>
            <div class="col-sm-6"><?= $this->Form->control('ad_expiry_date', ['type' => 'text', 'empty' => true, 'class' => 'datetime', 'val' => $this->Transform->date($property->ad_expiry_date)]); ?></div>
            <div class="col-sm-6"><?= $this->Form->control('ad_cost'); ?></div>
            <div class="col-sm-6"><?= $this->Form->control('ad_total_cost'); ?></div>
        </div>

    </div>
    <div class="col-md-4 col-lg-3">

        <h4><?= __('Additional Options') ?></h4>
        <div class="attributes loading-wrapper">
            <?php if (!empty($propertyAttributes)) : ?>
                <?php foreach ($propertyAttributes as $key => $attribute) : ?>
                    <?= $this->Form->control('property_attributes.' . $key . '.id', ['type' => 'hidden', 'val' => $attribute['id']]) ?>
                    <?= $this->Form->control('property_attributes.' . $key . '._joinData.attribute_value', ['type' => 'checkbox', 'label' => $attribute['name']]) ?>
                <?php endforeach; ?>
            <?php elseif (!empty($property->type_id)) : ?>
                <?= $this->Form->control('property_attributes[]', ['type' => 'hidden', 'val' => '']) ?>
                <p class="alert alert-warning"><?= __('There are no additional options for selected property type.') ?></p>
            <?php else : ?>
                <?= $this->Form->control('property_attributes[]', ['type' => 'hidden', 'val' => '']) ?>
                <p class="alert alert-info"><?= __('Please choose property type to display additional options.') ?></p>
            <?php endif; ?>
        </div>

    </div>
</div>
<hr>

<?= $this->element('Structure/action-buttons') ?>
<?= $this->Form->end() ?>
