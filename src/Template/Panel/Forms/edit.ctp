<?= $this->Form->create($form) ?>
<div class="row">
    <div class="col-sm-12"><?= $this->Form->control('name') ?></div>
    <div class="col-sm-12 <?= (!$_user->is_root) ? 'd-none' : '' ?>"><?= $this->Form->control('alias') ?></div>
    <div class="col-sm-12"><?= $this->Form->control('html_class', ['label' => __('HTML classes')]) ?></div>
    <div class="col-sm-12 mb-3"><?= $this->Form->control('thank_you', ['label' => __('After submit redirect to thank you page')]) ?></div>
    <div class="col-sm-6 mb-3"><?= $this->Form->control('redirect_url', ['class' => 'redirect']) ?></div>
</div>
<div class="row">
    <div class="col-sm-12">
        <label class="control-label col-xs-12" ><?= __('Emails') ?>: </label>
    </div>
</div>
<div class="row">
    <div class="col-sm-4 col-xs-12">
        <div class="row">
            <div class="col-sm-10">
                <?= $this->Form->control('main_email', ['label' => false, 'placeholder' => 'Mail to...']) ?>
            </div>
            <div class="col-sm-2">
                <button data-email="mainEmails" type="button" class="btn btn-primary pull-right addMainEmail"><i class="fa fa-plus"></i></button>
            </div>
        </div>
    </div>
    <div class="col-sm-8 col-xs-12 mainEmails">
        <?php if (isset($emails->main) && is_array($emails->main)): ?>
            <?php foreach ($emails->main as $email): ?>
                <button type="button" class="btn btn-default btn-md emailElement" data-selector="mainEmails" data-email="<?= $email ?>"><?= $email ?> <i class="removeEmail fa fa-times text-danger"></i></button>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-4 col-xs-12">
        <div class="row">
            <div class="col-sm-10">
                <?= $this->Form->control('cc_email', ['label' => false, 'placeholder' => 'CC...']) ?>
            </div>
            <div class="col-sm-2">
                <button data-email="ccEmails" type="button" class="btn btn-primary pull-right addCCEmail"><i class="fa fa-plus"></i></button>
            </div>
        </div>
    </div>
    <div class="col-sm-8 col-xs-12 ccEmails">
        <?php if (isset($emails->cc) && is_array($emails->cc)): ?>
            <?php foreach ($emails->cc as $email): ?>
                <button type="button" class="btn btn-default btn-md emailElement" data-selector="ccEmails" data-email="<?= $email ?>"><?= $email ?> <i class="removeEmail fa fa-times text-danger"></i></button>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-4 col-xs-12">
        <div class="row">
            <div class="col-sm-10">
                <?= $this->Form->control('bcc_email', ['label' => false, 'placeholder' => 'BCC...']) ?>
            </div>
            <div class="col-sm-2">
                <button data-email="bccEmails" type="button" class="btn btn-primary pull-right addBCCEmail"><i class="fa fa-plus"></i></button>
            </div>
        </div>
    </div>
    <div class="col-sm-8 col-xs-12 bccEmails">
        <?php if (isset($emails->bcc) && is_array($emails->bcc)): ?>
            <?php foreach ($emails->bcc as $email): ?>
                <button type="button" class="btn btn-default btn-md emailElement" data-selector="bccEmails" data-email="<?= $email ?>"><?= $email ?> <i class="removeEmail fa fa-times text-danger"></i></button>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
<?= $this->Form->control('mainEmails', ['type' => 'hidden', 'value' => isset($emails->main) && is_array($emails->main) ? implode(',', $emails->main) : '', 'class' => 'mainEmailsHidden']) ?>
<?= $this->Form->control('ccEmails', ['type' => 'hidden', 'value' => isset($emails->cc) && is_array($emails->cc) ? implode(',', $emails->cc) : '', 'class' => 'ccEmailsHidden']) ?>
<?= $this->Form->control('bccEmails', ['type' => 'hidden', 'value' => isset($emails->bcc) && is_array($emails->bcc) ? implode(',', $emails->bcc) : '', 'class' => 'bccEmailsHidden']) ?>
<div class="row mb-3">
    <div class="col-sm-2"><?= $this->Form->control('send_email') ?></div>
    <div class="col-sm-2"><?= $this->Form->control('active') ?></div>
</div>
<div class="row" id="json">
    <div class="col-sm-12 mb-3">
        <button type="button" id="addField" class="btn btn-primary"><?= 'Add field' ?></button>
    </div>
    <div class="col-sm-12 formBuilderTable">
        <div class="table-responsive">
            <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered marg-t">
                <thead>
                    <tr>
                        <th><?= 'Label'; ?></th>
                        <th><?= 'Name'; ?></th>
                        <th><?= 'Validation text'; ?></th>
                        <th><?= 'Css class'; ?></th>
                        <th><?= 'Placeholder'; ?></th>
                        <th><?= 'Type'; ?></th>
                        <th><?= 'Value'; ?></th>
                        <th><?= 'Validation'; ?></th>
                        <th><?= 'Rule'; ?></th>
                        <th><?= 'Required'; ?></th>
                        <th><?= 'Actions'; ?></th>
                    </tr>
                </thead>
                <tbody class="formBuilderBody">
                    <?php if (isset($data[0])): ?>
                        <?php foreach ($data[0] as $key => $field): ?>
                            <tr>
                                <td><?= $this->Form->control('label[]', ['label' => false, 'value' => $field->label, 'id' => false]) ?></div></td>
                                <td><?= $this->Form->control('input_name[]', ['label' => false, 'value' => $key, 'required' => true, 'class' => 'inputName', 'id' => false]) ?></div></td>
                                <td><?= $this->Form->control('validation_text[]', ['label' => false, 'value' => $field->validation_text, 'id' => false]) ?></div></td>
                                <td><?= $this->Form->control('class[]', ['label' => false, 'value' => $field->class, 'id' => false]) ?></div></td>
                                <td><?= $this->Form->control('placeholder[]', ['label' => false, 'value' => $field->placeholder, 'id' => false]) ?></div></td>
                                <td>
                                    <div class="form-group">
                                        <?= $this->Form->select('type[]', $types, ['escape' => false, 'class' => 'optionsSelect', 'empty' => 'Choose...', 'value' => $field->type, 'id' => false]); ?>
                                        <button style="<?php if (isset($field->options)): ?>display:inline-block;<?php endif; ?>" type="button" data-toggle="modal" data-target=".bs-example-modal-sm" class="btn btn-default btn-xs optionsButton"><i class="fa fa-edit"></i></button>
                                        <?= $this->Form->control('options[]', ['type' => 'hidden', 'value' => '', 'class' => 'options', 'id' => false, 'value' => isset($field->options) ? json_encode($field->options) : '']); ?>
                                    </div>
                                </td>
                                <td><?= $this->Form->control('value[]', ['label' => false, 'value' => $field->value, 'id' => false]) ?></div></td>
                                <td><div class="form-group"><?= $this->Form->select('validation[]', $validations, ['escape' => false, 'class' => 'validationSelect', 'empty' => 'Choose...', 'value' => $field->validation, 'id' => false])
                                        ?></div></td>
                                <td><?= $this->Form->control('rule[]', ['label' => false, 'class' => 'rule', 'value' => $field->rule, 'id' => false]) ?></td>
                                <td><div class="form-group"><?= $this->Form->checkbox('required['.$key.']', ['checked' => $field->required, 'class' => 'requiredField', 'id' => false]) ?></div></td>
                                <td style="width: 100px;"><span class="btn btn-xs btn-default moveUp"><i class="fa fa-chevron-up"></i></span><span class="btn btn-xs btn-default moveDown"><i class="fa fa-chevron-down"></i></span><button class="remove btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<hr>
<?= $this->element('Structure/action-buttons') ?>

<?= $this->Form->end() ?>

<?= $this->element('FormBuilder/new-field-row') ?>

<?= $this->element('FormBuilder/options-modal') ?>

<?= $this->Html->script('/panel/js/form-builder.js', ['block' => 'scriptBottom']) ?>
