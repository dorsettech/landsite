<?= $this->Form->create($form) ?>
<?= $this->element('Structure/action-buttons') ?>
<hr>
<div class="row">
    <div class="col-sm-12"><?= $this->Form->control('name') ?></div>
    <div class="col-sm-12 <?= (!$_user->is_root) ? 'd-none' : '' ?>"><?= $this->Form->control('alias') ?></div>
    <div class="col-sm-12"><?= $this->Form->control('html_class', ['label' => __('HTML classes')]) ?></div>
    <div class="col-sm-12 mb-3"><?= $this->Form->control('thank_you', ['label' => __('After submit redirect to thank you page')]) ?></div>
    <div class="col-sm-6 mb-3"><?= $this->Form->control('redirect_url', ['class' => 'redirect']) ?></div>
</div>
<div class="row">
    <div class="col-sm-12">
        <label class="control-label col-xs-12" ><?= __('Emails') ?>: </label>
    </div>
</div>
<div class="row">
    <div class="col-sm-4 col-xs-12">
        <div class="row">
            <div class="col-sm-10">
                <?= $this->Form->control('main_email', ['label' => false, 'placeholder' => __('Mail to...')]) ?>
            </div>
            <div class="col-sm-2">
                <button data-email="mainEmails" type="button" class="btn btn-primary pull-right addMainEmail"><i class="fa fa-plus"></i></button>
            </div>
        </div>
    </div>
    <div class="col-md-8 col-xs-12 mainEmails"></div>
</div>
<div class="row">
    <div class="col-sm-4 col-xs-12">
        <div class="row">
            <div class="col-sm-10">
                <?= $this->Form->control('cc_email', ['label' => false, 'placeholder' => 'CC...']) ?>
            </div>
            <div class="col-sm-2">
                <button data-email="ccEmails" type="button" class="btn btn-primary pull-right addCCEmail"><i class="fa fa-plus"></i></button>
            </div>
        </div>
    </div>
    <div class="col-md-8 col-xs-12 ccEmails"></div>
</div>
<div class="row">
    <div class="col-sm-4 col-xs-12">
        <div class="row">
            <div class="col-sm-10">
                <?= $this->Form->control('bcc_email', ['label' => false, 'placeholder' => __('BCC...')]) ?>
            </div>
            <div class="col-sm-2">
                <button data-email="bccEmails" type="button" class="btn btn-primary pull-right addBCCEmail"><i class="fa fa-plus"></i></button>
            </div>
        </div>
    </div>
    <div class="col-md-8 col-xs-12 bccEmails"></div>
</div>
<?= $this->Form->control('mainEmails', ['type' => 'hidden', 'value' => '', 'class' => 'mainEmailsHidden']) ?>
<?= $this->Form->control('ccEmails', ['type' => 'hidden', 'value' => '', 'class' => 'ccEmailsHidden']) ?>
<?= $this->Form->control('bccEmails', ['type' => 'hidden', 'value' => '', 'class' => 'bccEmailsHidden']) ?>
<div class="row mb-3">
    <div class="col-sm-2"><?= $this->Form->control('send_email', ['checked' => true]) ?></div>
    <div class="col-sm-2"><?= $this->Form->control('active') ?></div>
</div>
<div class="row" id="json">
    <div class="col-xs-12 mb-3">
        <button type="button" id="addField" class="btn btn-primary"><?= __('Add field') ?></button>
    </div>
    <div class="col-sm-12 formBuilderTable ">
        <div class="table-responsive">
            <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th><?= __('Label') ?></th>
                        <th><?= __('Name') ?></th>
                        <th><?= __('Validation text') ?></th>
                        <th><?= __('Css class') ?></th>
                        <th><?= __('Placeholder') ?></th>
                        <th><?= __('Type') ?></th>
                        <th><?= __('Value') ?></th>
                        <th><?= __('Validation') ?></th>
                        <th><?= __('Rule') ?></th>
                        <th><?= __('Required') ?></th>
                        <th><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody class="formBuilderBody"></tbody>
            </table>
        </div>
    </div>
</div>
<hr>
<?= $this->element('Structure/action-buttons') ?>
<?= $this->Form->end() ?>

<?= $this->element('FormBuilder/new-field-row') ?>

<?= $this->element('FormBuilder/options-modal') ?>

<?= $this->Html->script('/panel/js/form-builder.js', ['block' => 'scriptBottom']) ?>


