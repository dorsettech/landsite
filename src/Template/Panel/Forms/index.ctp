<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name', ['name' => 'Name']) ?></th>
                <th><?= __('Messages') ?></th>
                <th><?= $this->Paginator->sort('html_class', ['name' => 'HTML classes']) ?></th>
                <th><?= $this->Paginator->sort('active', ['name' => 'Active']) ?></th>
                <th><?= $this->Paginator->sort('created', ['name' => 'Created']) ?> / <?= $this->Paginator->sort('modified', ['name' => 'Modified']) ?></th>
                <th class="actions"><?= 'Actions' ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($forms as $form): ?>
                <tr>
                    <td><?= $this->Html->link($this->Number->format($form->id), ['controller' => 'FormsData', 'action' => 'listView', $form->id]) ?></td>
                    <td><?= $this->Html->link(h($form->name), ['controller' => 'FormsData', 'action' => 'listView', $form->id]) ?></td>
                    <td>
                        <?= $this->Html->link(__('All messages'), ['controller' => 'FormsData', 'action' => 'listView', $form->id]) ?>
                        <?= $this->Html->link(__('Unread: {0}', $form->getUnreadMessages()), ['controller' => 'FormsData', 'action' => 'listView', $form->id], ['class' => ($form->getUnreadMessages() > 0) ? 'badge badge-warning' : 'badge badge-light']) ?>
                    </td>
                    <td><?= h($form->html_class) ?></td>
                    <td>
                        <?= $this->Switcher->create('active', ['val' => $form->active, 'data-id' => $form->id]); ?>
                    </td>
                    <td>
                        <?= $this->Html->badge('<i class="far fa-clock"></i> '.h($this->Transform->date($form->created)), 'light') ?><br>
                        <?= $this->Html->badge('<i class="fa fa-clock"></i> '.h($this->Transform->date($form->modified)), 'light') ?>
                    </td>
                    <td class="actions">
                        <div class="btn-group btn-group-xs">
                            <?= $this->Html->link('<i class="fa fa-list"></i>', ['controller' => 'FormsData', 'action' => 'listView', $form->id], ['class' => 'btn btn-default', 'escape' => false, 'title' => __('Messages list'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $form->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Edit form'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            <?php if ($_user->is_root) : ?>
                                <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $form->id], ['class' => 'btn btn-danger', 'confirm' => 'Please confirm form removal '.h($form->name).'?', 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            <?php endif; ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div class="action-buttons">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-lg-6 pull-left">
            <?= $this->Form->create(null, ['url' => ['controller' => 'FormsData', 'action' => 'exportAllForms'], 'class' => 'form from-inline forms-list-filter']) ?>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">From: </span>
                </div>
                <?= $this->Form->input('from', ['type' => 'text', 'id' => 'datetimelogs', 'class' => 'form-control', 'label' => false]) ?>
                <div class="input-group-prepend">
                    <span class="input-group-text">To: </span>
                </div>
                <?= $this->Form->input('to', ['type' => 'text', 'id' => 'createdTo', 'class' => 'form-control', 'label' => false]) ?>
                <div class="input-group-prepend">
                    <span class="input-group-btn"><?= $this->Form->submit('Download All Forms CSV', ['class' => 'btn btn-primary']) ?></span>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg-6 text-right">
            <?php if ($_user->is_root): ?>
                <?= $this->Html->link(__('Add Form'), ['action' => 'add'], ['class' => 'btn btn-primary']); ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<?= $this->element('pagination'); ?>