<?php

use App\Model\Enum\MediaType;
?>
<div class="upload">
    <?= $this->Form->create($propertyMedia, ['url' => ['action' => 'add', $property->id . '.json'], 'id' => 'propertyMediaUpload', 'class' => 'dropzone', 'data-param-name' => 'source']) ?>
    <div class="fallback">
        <?= $this->Form->control('source', ['type' => 'file', 'multiple' => true]) ?>
    </div>
    <?= $this->Form->end() ?>
</div>
<div class="table-responsive">
    <?php if ($propertyMedia->count() > 0) : ?>
        <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('position') ?></th>
                    <th><?= $this->Paginator->sort('type') ?></th>
                    <th></th>
                    <th><?= $this->Paginator->sort('source') ?></th>
                    <th><?= $this->Paginator->sort('created') ?> / <?= $this->Paginator->sort('modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($propertyMedia as $media): ?>
                    <tr>
                        <td><?= $this->Number->format($media->id) ?></td>
                        <td><?= $this->Number->format($media->position) ?></td>
                        <td><?= h($media->type) ?></td>
                        <td class="text-center">
                            <?php if ($media->type == MediaType::IMAGE) : ?>
                                <?= $this->Html->link($this->Html->image('/' . $this->Images->scale($media->source_path, 40, 40, 'ratio_crop'), ['alt' => $property->company]), '/' . $this->Images->scale($media->source_path, 1280, 1280, 'ratio'), ['class' => 'magnific-image', 'escape' => false]) ?>
                            <?php elseif ($media->type == MediaType::VIDEO) : ?>
                                <i class="fab fa-youtube fa-3x"></i>
                            <?php elseif ($media->type == MediaType::DOCUMENT) : ?>
                                <i class="far fa-file fa-3x"></i>
                            <?php endif; ?>
                        </td>
                        <td><?= h($media->source) ?></td>
                        <td>
                            <?= $this->Html->badge('<i class="far fa-clock"></i> ' . h($this->Transform->date($media->created)), 'light', ['title' => __('Created'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?><br>
                            <?= $this->Html->badge('<i class="fa fa-clock"></i> ' . h($this->Transform->date($media->modified)), 'light', ['title' => __('Modified'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                        </td>
                        <td class="actions">
                            <div class="btn-group btn-group-xs">
                                <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $property->id, $media->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Edit'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                                <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $property->id, $media->id], ['class' => 'btn btn-danger', 'confirm' => __('Please confirm removal Property Media #{0}', $media->id), 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else : ?>
        <div class="well text-center text-warning"><?= __('There are no media files for {0} yet.', $property->title) ?> <?= __('Add files by dragging them into area above.') ?></div>
    <?php endif; ?>
</div>
<?= $this->element('pagination') ?>
<div class="row action-buttons">
    <div class="col-3 text-left"><?= $this->Html->link(__('Properties List'), $heading['link_close'], ['class' => 'btn btn-light']) ?></div>
</div>
<?php
$this->Html->css('/plugins/dropzone/min/dropzone.min.css', ['block' => 'cssHead']);
$this->Html->script('/plugins/dropzone/min/dropzone.min.js', ['block' => 'scriptBottom']);
?>
