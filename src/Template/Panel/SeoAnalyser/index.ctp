<div class="row">
    <div class="col-3 col-lg-offset-6">
        <?= $this->Form->control('switch', ['prepend' => __('Change type'), 'label' => false, 'options' => $options]) ?>
    </div>
</div>
<?php foreach ($data as $entity) : ?>
    <?= $this->element('SeoAnalyser/section', ['entity' => $entity]) ?>
<?php endforeach; ?>
<div class="paginator text-center">
    <?= $this->Paginator->numbers() ?>
    <p><?= $this->Paginator->counter() ?></p>
</div>
<?php
    $this->Html->css('/plugins/dropzone/min/dropzone.min.css', ['block' => 'cssHead']);
    $this->Html->script('/plugins/dropzone/min/dropzone.min.js', ['block' => 'scriptBottom']);
?>
