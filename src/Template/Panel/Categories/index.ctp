<div class="table-responsive">
    <?php if ($categories->count() > 0) : ?>
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('slug') ?></th>
                <th><?= $this->Paginator->sort('image') ?></th>
                <th><?= $this->Paginator->sort('position') ?></th>
                <th><?= $this->Paginator->sort('is_recommended') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($categories as $category): ?>
                <tr>
                    <td><?= $this->Number->format($category->id) ?></td>
                    <td><?= h($category->name) ?></td>
                    <td><?= h($category->slug) ?></td>
                    <td>
                        <?php if(!empty($category->image)): ?>
                            <a href="<?= $category->image_path ?>" class="magnific-image"><img src="<?= $this->Images->scale($category->image_path, 50, 50) ?>" class="img-responsive"></a>
                        <?php endif; ?>
                    </td>
                    <td><?= $this->Number->format($category->position) ?></td>
                    <td>
                        <?= $this->Switcher->create('is_recommended', ['val' => $category->is_recommended, 'data-id' => $category-> id]); ?>
                    </td>
                    <td><?= $this->Html->badge('<i class="far fa-clock"></i> ' . h($this->Transform->date($category->created)), 'light') ?></td>
                    <td><?= $this->Html->badge('<i class="fa fa-clock"></i> ' . h($this->Transform->date($category->modified)), 'light') ?></td>
                    <td class="actions">
                        <div class="btn-group btn-group-xs">
                            <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $category->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Edit'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $category->id], ['class' => 'btn btn-danger', 'confirm' => __('Please confirm removal Category #{0}', $category->id), 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php else : ?>
    <div class="well text-center text-warning"><?= __('There are no Categories yet.') ?> <?= $this->Html->link(__('Add first Category'), ['action' => 'add']) ?></div>
    <?php endif; ?>
</div>
<div class="row action-buttons">
    <div class="col text-right">
    <?= $this->Html->link(__('Add Category'), ['action' => 'add'], ['class' => 'btn btn-primary']); ?>
    </div>
</div>
<?= $this->element('pagination') ?>
