<?= $this->Form->create($category, ['type' => 'file']); ?>
<?= $this->element('Structure/action-buttons') ?>
<hr>
<div class="row">
    <div class="col-sm-6"><?= $this->Form->control('name'); ?></div>
    <div class="col-sm-6"><?= $this->Form->control('slug'); ?></div>
    <div class="col-sm-12"><?= $this->Form->control('description', ['class' => 'ckeditor']); ?></div>
    <div class="col-sm-8"><?= $this->Form->control('image', ['type' => 'file', 'required' => false]); ?></div>
    <?php if (!empty($category->image)): ?>
        <div class="col-sm-4">
            <div class="pull-left mr-sm-2">
                <h6><?= __('Current image') ?></h6>
                <img src="<?= $this->Images->scale($category->image_path, 60, 60) ?>" class="img-responsive m-b-20">
            </div>
            <div class="pull-left">
                <label class="d-block">&nbsp;</label>
                <?= $this->Form->control('image-clear', ['type' => 'checkbox', 'label' => __('Delete image')]); ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="col-sm-6 col-lg-2"><?= $this->Form->control('position'); ?></div>
    <div class="col-sm-6 col-lg-2 d-flex align-items-center"><?= $this->Form->control('is_recommended'); ?></div>
</div>
<hr><?= $this->element('Structure/action-buttons') ?>
<?= $this->Form->end() ?>

