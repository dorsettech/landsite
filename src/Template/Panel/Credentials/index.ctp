<div class="table-responsive">
    <?php if ($credentials->count() > 0) : ?>
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($credentials as $credential): ?>
                <tr>
                    <td><?= $this->Number->format($credential->id) ?></td>
                    <td><?= h($credential->name) ?></td>
                    <td><?= $this->Html->badge('<i class="far fa-clock"></i> ' . h($this->Transform->date($credential->created)), 'light') ?></td>
                    <td class="actions">
                        <div class="btn-group btn-group-xs">
                            <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $credential->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Edit'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $credential->id], ['class' => 'btn btn-danger', 'confirm' => __('Please confirm removal Credential #{0}', $credential->id), 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php else : ?>
    <div class="well text-center text-warning"><?= __('There are no {0} yet.', \Cake\Utility\Inflector::pluralize($credentialName)) ?> <?= $this->Html->link(__('Add first {0}', $credentialName), ['action' => 'add']) ?></div>
    <?php endif; ?>
</div>
<div class="row action-buttons">
    <div class="col text-right">
    <?= $this->Html->link(__('Add {0}', $credentialName), ['action' => 'add'], ['class' => 'btn btn-primary']); ?>
    </div>
</div>
<?= $this->element('pagination') ?>
