<div class="table-responsive">
    <?php if ($propertySearches->count() > 0) : ?>
        <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('user_id') ?></th>
                    <th><?= $this->Paginator->sort('location') ?></th>
                    <th><?= $this->Paginator->sort('purpose') ?></th>
                    <th><?= $this->Paginator->sort('type_id', ['label' => __('Property Type')]) ?></th>
                    <th><?= $this->Paginator->sort('min_price') ?></th>
                    <th><?= $this->Paginator->sort('max_price') ?></th>
                    <th><?= $this->Paginator->sort('radius') ?></th>
                    <th><?= __('Attributes') ?></th>
                    <th><?= $this->Paginator->sort('alerts', ['label' => __('Receive Alerts')]) ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($propertySearches as $propertySearch): ?>
                    <tr>
                        <td><?= $this->Number->format($propertySearch->id) ?></td>
                        <td><?= $propertySearch->has('user') ? $this->Html->link($propertySearch->user->full_name, ['controller' => 'Members', 'action' => 'view', $propertySearch->user->id]) : '' ?></td>
                        <td><?= h($propertySearch->location) ?></td>
                        <td><?= h($propertySearch->purpose) ?></td>
                        <td><?= $propertySearch->has('property_type') ? $propertySearch->property_type->name : '' ?></td>
                        <td><?= ($propertySearch->min_price > 0 ) ? $this->Number->format($propertySearch->min_price) : '' ?></td>
                        <td><?= ($propertySearch->max_price > 0) ? $this->Number->format($propertySearch->max_price) : '' ?></td>
                        <td><?= ($propertySearch->radius > 0) ? $this->Number->format($propertySearch->radius) : '' ?></td>
                        <td><?= $propertySearch->attribute_names ?></td>
                        <td>
                            <?= $this->Switcher->create('alerts', ['val' => $propertySearch->alerts, 'data-id' => $propertySearch->id]); ?>
                        </td>
                        <td><?= $this->Html->badge('<i class="far fa-clock"></i> ' . h($this->Transform->date($propertySearch->created)), 'light') ?></td>
                        <td class="actions">
                            <div class="btn-group btn-group-xs">
                                <?= $this->Html->link('<i class="fas fa-search"></i>', ['_name' => 'properties-listing', '?' => $propertySearch->query_array], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Search'), 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'target' => '_blank']) ?>
                                <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $propertySearch->id], ['class' => 'btn btn-danger', 'confirm' => __('Please confirm removal Property Search #{0}', $propertySearch->id), 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else : ?>
        <div class="well text-center text-warning"><?= __('There are no Property Searches yet.') ?></div>
    <?php endif; ?>
</div>
<?= $this->element('pagination') ?>
