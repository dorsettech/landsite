<?= $this->element('head'); ?>
<body class="gray-bg">

    <div id="wrapper">
        <div class="gray-bg">
            <div class="row">
                <div class="col-xs-12">
                    <?= $this->element('clear-content-media'); ?>
                </div>
            </div>
        </div>

    </div>
    <?= $this->element('footer-scripts'); ?>
</body>
