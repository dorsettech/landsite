<?= $this->element('head'); ?>
<body class="pace-top bg-white">
    <?= $this->element('page-loader') ?>
    <div id="page-container" class="fade">
        <div class="login login-with-news-feed">
            <div class="news-feed">
                <div class="news-image" style="background-image: url(/panel/img/login-bg/login-bg-1.jpg)"></div>
                <div class="news-caption">
                    <h4 class="caption-title"><?= $loginTitle ?></h4>
                    <p>
                        <?= $loginSlogan ?>
                    </p>
                </div>
            </div>
            <div class="right-content">
                <?= $this->fetch('content'); ?>
                <hr />
                <p class="text-center text-grey-darker">
                    &copy; <?= date('Y') ?> <?= $_copy; ?>
                </p>
            </div>
        </div>
    </div>
    <?= $this->element('footer-scripts'); ?>
</body>
</html>
