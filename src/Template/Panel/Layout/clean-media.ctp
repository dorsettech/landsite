<?= $this->element('head'); ?>
<body class="media">
    <div id="page-container" class="fade">
        <div id="content" class="content-media">
            <?= $this->Flash->render(); ?>
            <?= $this->element('breadcrumb') ?>
            <?= $this->element('heading'); ?>
            <?= $this->fetch('content'); ?>
        </div>
        <?= $this->element('footer'); ?>
    </div>
    <?= $this->element('footer-scripts') ?>
</body>
</html>