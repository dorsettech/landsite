<?= $this->element('head'); ?>
<body class="fixed-sidebar no-skin-config full-height-layout">
    <div id="wrapper">
        <?= $this->element('sidebar'); ?>

        <div id="page-wrapper" class="gray-bg dashbard-1">
            <?= $this->element('header'); ?>
            <div class="row">
                <div class="col-lg-12">
                    <?= $this->element('content-media'); ?>
                    <?= $this->element('footer'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->element('locked-screen'); ?>
<?= $this->element('footer-scripts'); ?>
</body>
</html>
