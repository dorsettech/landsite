<?= $this->element('head-min'); ?>
    <body class="error-page">
        <?= $this->element('page-loader') ?>
        <div id="page-container" class="fade">
            <?= $this->fetch('content') ?>
        </div>
        
        <?php
        $errorMessage = $this->fetch('errorMessage');
        $errorTrace = $this->fetch('errorTrace');
        $errorInfo = $this->fetch('errorInfo');
        if ($errorMessage || $errorTrace || $errorInfo) : ?>
        <section class="debug animated fadeInUp">
            <div class="container">
                <div class="error-message">
                    <?= $errorMessage ?>
                </div>
                <div class="error-trace">
                    <?= $errorTrace ?>
                </div>
                <div class="error-info">
                    <?= $errorInfo ?>
                </div>
            </div>
        </section>
        <?php endif; ?>
        <?= $this->element('footer-scripts-min') ?>
    </body>
</html>