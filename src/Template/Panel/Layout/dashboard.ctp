<?= $this->element('head'); ?>
<body>
    <?= $this->element('locked-screen'); ?>
    <?= $this->element('page-loader') ?>
    <div id="page-container" class="fade page-sidebar-fixed page-header-fixed page-with-wide-sidebar">
        <?= $this->element('header'); ?>
        <?= $this->element('sidebar'); ?>
        <div id="content" class="content">
            <?= $this->Flash->render(); ?>
            <?= $this->element('breadcrumb') ?>
            <?= $this->element('dashboard-content'); ?>
        </div>
        <?= $this->element('footer'); ?>
    </div>
    <?= $this->element('footer-scripts') ?>
</body>
</html>
