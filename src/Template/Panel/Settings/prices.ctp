<?= $this->Form->create($settings) ?>
<?= $this->element('Structure/action-buttons') ?>

<div class="row">
    <?php $type = 'property'; ?>
    <div class="col-sm-6">
        <h3><?= __('Property Prices') ?></h3>
    <?php foreach ($settings as $key => $setting) : ?>
        <?php $currentType = explode('-', $setting->key)[0] ?>
        <?php if ($currentType != $type) : ?>
            </div>
            <div class="col-sm-6">
                <h3>
                    <?php
                        if ($currentType == 'service') {
                            echo __('Business Listing Prices');
                        } else if ($currentType == 'service_2') {
                            echo __('Business Listing Prices (180 days)');
                        } else if ($currentType == 'vat') {
                            echo __('VAT rate');
                        }
                    ?>
                </h3>
        <?php endif; ?>
            <?= $this->Form->control('services[' . $key . '][id]', ['type' => 'hidden', 'value' => $setting->id]) ?>
            <?php if ($setting->type == 'FLOAT'): ?>
                <?= $this->Form->control('services[' . $key . '][value]', ['type' => 'number', 'label' => $setting->description, 'value' => $setting->value, 'step' => '0.01']) ?>
            <?php elseif ($setting->type == 'INT'): ?>
                <?= $this->Form->control('services[' . $key . '][value]', ['type' => 'number', 'label' => $setting->description, 'value' => $setting->value, 'step' => '1']) ?>
            <?php else : ?>
                <?= $this->Form->control('services[' . $key . '][value]', ['type' => 'text', 'label' => $setting->description, 'value' => $setting->value]) ?>
            <?php endif; ?>
        <?php $type = $currentType ?>
    <?php endforeach; ?>
    </div>
</div>

<?= $this->element('Structure/action-buttons') ?>
<?= $this->Form->end() ?>
