<?= $this->Form->create($user, ['type' => 'file']) ?>
<?= $this->element('Structure/action-buttons') ?>
<hr>

<h4><?= __('User details') ?></h4>
<div class="row">
    <div class="col-sm-6"><?= $this->Form->control('first_name', ['label' => __('Name')]); ?></div>
    <div class="col-sm-6"><?= $this->Form->control('last_name', ['label' => __('Surname')]); ?></div>
</div>
<div class="row">
    <div class="col-sm-6"><?= $this->Form->control('email', ['label' => __('Email address')]); ?></div>
    <div class="col-sm-6"><?= $this->Form->control('phone', ['label' => __('Phone Number')]); ?></div>
</div>
<div class="row">
    <div class="col-sm-6"><?= $this->Form->control('user_detail.company', ['label' => __('Company')]); ?></div>
    <div class="col-sm-6"><?= $this->Form->control('user_detail.postcode', ['label' => __('Postcode')]); ?></div>
</div>
<div class="row">
    <div class="col-sm-6"><?= $this->Form->control('user_detail.company_description', ['label' => __('Company Description')]); ?></div>
    <div class="col-sm-6"><?= $this->Form->control('user_detail.location', ['type' => 'textarea', 'label' => __('Location')]); ?></div>
</div>
<div class="row">
    <div class="col-sm-6"><?= $this->Form->control('password', ['type' => 'password', 'val' => '']); ?></div>
    <div class="col-sm-6"><?= $this->Form->control('confirm_password', ['type' => 'password', 'val' => '']); ?></div>
</div>

<hr>

<h4><?= __('Billing') ?></h4>
<div class="mb-3">
    <?= $this->Form->input('user_detail.use_billing_details', ['type' => 'checkbox', 'checked' => (!empty($user->user_detail) ? $user->user_detail->use_billing_details : 'true'), 'label' => __('Billing details are the same as above'), 'data-toggle' => 'collapse', 'data-target' => '#billing-details', 'aria-expanded' => 'false', 'aria-controls' => 'billing-details']) ?>
</div>
<div class="collapse<?= !empty($user->user_detail) && !$user->user_detail->use_billing_details ? ' show' : ''  ?>" id="billing-details">
    <?= $this->Form->control('user_billing_addresses.0.id') ?>
    <?= $this->Form->control('user_billing_addresses.0.is_default', ['type' => 'hidden', 'value' => 1]) ?>
    <div class="row">
        <div class="col-sm-6"><?= $this->Form->control('user_billing_addresses.0.first_name', ['label' => __('Name')]); ?></div>
        <div class="col-sm-6"><?= $this->Form->control('user_billing_addresses.0.last_name', ['label' => __('Surname')]); ?></div>
    </div>
    <div class="row">
        <div class="col-sm-6"><?= $this->Form->control('user_billing_addresses.0.email', ['label' => __('Email address')]); ?></div>
        <div class="col-sm-6"><?= $this->Form->control('user_billing_addresses.0.phone', ['label' => __('Phone Number')]); ?></div>
    </div>
    <div class="row">
        <div class="col-sm-6"><?= $this->Form->control('user_billing_addresses.0.company', ['label' => __('Company')]); ?></div>
        <div class="col-sm-6"><?= $this->Form->control('user_billing_addresses.0.postcode', ['label' => __('Postcode')]); ?></div>
    </div>
</div>

<hr>

<h4><?= __('Interested in') ?></h4>
<div class="row">
    <div class="col-sm-6 col-md-3"><?= $this->Form->control('user_detail.pref_buying', ['type' => 'checkbox', 'label' => __('Buying')]); ?></div>
    <div class="col-sm-6 col-md-3"><?= $this->Form->control('user_detail.pref_selling', ['type' => 'checkbox', 'label' => __('Selling')]); ?></div>
    <div class="col-sm-6 col-md-3"><?= $this->Form->control('user_detail.pref_professional', ['type' => 'checkbox', 'label' => __('Professional Services')]); ?></div>
    <div class="col-sm-6 col-md-3"><?= $this->Form->control('user_detail.pref_insights', ['type' => 'checkbox', 'label' => __('Insights')]); ?></div>
</div>

<hr>

<h4><?= __('Marketing Preferences') ?></h4>
<div class="row">
    <div class="col-sm-6 col-md-3"><?= $this->Form->control('user_detail.marketing_agreement_1', ['type' => 'checkbox', 'label' => __('Contact by email')]); ?></div>
    <div class="col-sm-6 col-md-3"><?= $this->Form->control('user_detail.marketing_agreement_2', ['type' => 'checkbox', 'label' => __('Contact by phone')]); ?></div>
    <div class="col-sm-6 col-md-3"><?= $this->Form->control('user_detail.marketing_agreement_3', ['type' => 'checkbox', 'label' => __('Other Contact')]); ?></div>
</div>

<hr>

<h4><?= __('Other') ?></h4>
<div class="row">
    <div class="col-sm-6 clearfix">
        <?= $this->Form->control('image', ['type' => 'file', 'label' => __('Profile Image/Company Logo'), 'button-label' => __('Select file')]); ?>
    </div>
    <?php if (!empty($user->image)) : ?>
        <div class="col-sm-2">
            <img src="<?= $this->Images->scale($user->image_path, 60, 60, 'ratio_fill') ?>" alt="" class="img-responsive">
        </div>
        <div class="col-sm-4">
            <?= $this->Form->control('remove_image', ['type' => 'checkbox', 'label' => __('Delete image')]); ?>
        </div>
    <?php endif; ?>
</div>
<?php if ($_user->is_root || $_user->is_admin) : ?>
    <hr>
    <div class="row align-items-center">
        <div class="col-sm-6"><?= $this->Form->control('group_id', ['empty' => __('select group'), 'options' => $groups]); ?></div>
        <div class="col-sm-6 col-md-3 col-lg-4"><?= $this->Form->control('job_role', ['label' => __('Job Role')]); ?></div>
        <div class="col-sm-6 col-md-3 col-lg-2"><?= $this->Form->control('active', ['type' => 'checkbox']); ?></div>
    </div>
<?php endif; ?>
<?= $this->element('Structure/action-buttons') ?>
<?= $this->Form->end() ?>
