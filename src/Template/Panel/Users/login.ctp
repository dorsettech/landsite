<div class="login-header">
    <div class="brand">
        <div class="logo">
            <?= $this->Html->image('/' . $this->Images->scale('panel/img/logo.png', 380, 190, 'ratio'), ['alt' => $_service, 'class' => 'img-fluid']) ?>
        </div>
        <?= $_service ?>
        <small><?= __('Please login to service panel.') ?></small>
    </div>
    <div class="icon">
        <i class="fa fa-sign-in"></i>
    </div>
</div>
<div class="login-content">
    <?= $this->Form->create(null, ['class' => 'login_form margin-bottom-0']) ?>
    <?= $this->Flash->render() ?>
    <?= $this->Form->control('email', ['label' => false, 'class' => 'form-control form-control-lg', 'type' => 'email', 'placeholder' => __('Email address')]); ?>
    <?= $this->Form->control('password', ['label' => false, 'class' => 'form-control form-control-lg', 'type' => 'password', 'placeholder' => __('Password')]); ?>
    <div class="login-buttons">
        <?= $this->Form->button(__('Sign me in'), ['type' => 'submit', 'btype' => 'primary', 'size' => 'lg', 'block' => true]); ?>
    </div>
    <div class="m-t-20 m-b-40 p-b-40 text-inverse">
        <?= __('Do you need help? Please ') ?> <?= $this->Html->link(__('contact us'), 'https://www.bespoke4business.com/contact', ['class' => 'text-primary', 'target' => '_blank']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>