<div class="login-header">
    <div class="brand">
        <div class="logo">
            <?= $this->Html->image('/' . $this->Images->scale('panel/img/logo.png', 380, 190, 'ratio'), ['alt' => $_service, 'class' => 'img-fluid']) ?>
        </div>
        <?= $_service ?>
        <small><?= __('Your token has expired.') ?></small>
    </div>
    <div class="icon">
        <i class="fa fa-sign-in"></i>
    </div>
</div>
<div class="login-content">
    <?= $this->Form->create(null, ['class' => 'login_form margin-bottom-0']) ?>
    <?= $this->Flash->render() ?>
    <?= $this->Form->control('token', ['type' => 'text', 'label' => __('Please enter new token to continue'), 'class' => 'form-control form-control-lg', 'placeholder' => __('Token')]); ?>
    <div class="login-buttons">
        <?= $this->Form->button(__('Confirm'), ['type' => 'submit', 'btype' => 'warning', 'size' => 'lg', 'block' => true]); ?>
    </div>
    <div class="m-t-20 m-b-40 p-b-40 text-inverse">
        <?= $this->Html->link(__('Regenerate token'), ['controller' => 'Users', 'action' => 'generate-token'], ['class' => 'btn btn-block btn-link']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>