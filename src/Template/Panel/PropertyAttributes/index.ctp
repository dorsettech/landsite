<div class="table-responsive">
    <?php if ($propertyAttributes->count() > 0) : ?>
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('position') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('type_id') ?></th>
                <th><?= $this->Paginator->sort('image') ?></th>
                <th><?= $this->Paginator->sort('visibility') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($propertyAttributes as $propertyAttribute): ?>
                <tr>
                    <td><?= $this->Number->format($propertyAttribute->id) ?></td>
                    <td><?= $this->Number->format($propertyAttribute->position) ?></td>
                    <td><?= h($propertyAttribute->name) ?></td>
                    <td><?= $propertyAttribute->has('property_type') ? $this->Html->link($propertyAttribute->property_type->name, ['controller' => 'PropertyTypes', 'action' => 'edit', $propertyAttribute->property_type->id]) : '' ?></td>
                    <td>
                        <?php if(!empty($propertyAttribute->image)): ?>
                            <a href="<?= $propertyAttribute->image_path ?>" class="magnific-image"><img src="<?= $this->Images->scale($propertyAttribute->image_path, 50, 50) ?>" class="img-responsive"></a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?= $this->Switcher->create('visibility', ['val' => $propertyAttribute->visibility, 'data-id' => $propertyAttribute-> id]); ?>
                    </td>
                    <td><?= $this->Html->badge('<i class="far fa-clock"></i> ' . h($this->Transform->date($propertyAttribute->created)), 'light') ?></td>
                    <td><?= $this->Html->badge('<i class="fa fa-clock"></i> ' . h($this->Transform->date($propertyAttribute->modified)), 'light') ?></td>
                    <td class="actions">
                        <div class="btn-group btn-group-xs">
                            <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $propertyAttribute->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Edit'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $propertyAttribute->id], ['class' => 'btn btn-danger', 'confirm' => __('Please confirm you would like to remove property attribute {0}', $propertyAttribute->name), 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php else : ?>
    <div class="well text-center text-warning"><?= __('There are no Property Attributes yet.') ?> <?= $this->Html->link(__('Add first Property Attribute'), ['action' => 'add']) ?></div>
    <?php endif; ?>
</div>
<div class="row action-buttons">
    <div class="col text-right">
    <?= $this->Html->link(__('Add Property Attribute'), ['action' => 'add'], ['class' => 'btn btn-primary']); ?>
    </div>
</div>
<?= $this->element('pagination') ?>
