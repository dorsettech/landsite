<?= $this->Form->create($propertyAttribute, ['type' => 'file']); ?>
<?= $this->element('Structure/action-buttons') ?>

<hr>

<div class="row">
    <div class="col-sm-6"><?= $this->Form->control('name'); ?></div>
    <div class="col-sm-6"><?= $this->Form->control('type_id', ['options' => $propertyTypes, 'empty' => __('Please choose')]); ?></div>
    <div class="col-sm-12"><?= $this->Form->control('description', ['class' => 'ckeditor']); ?></div>
    <div class="col-sm-6"><?= $this->Form->control('image', ['type' => 'file', 'required' => false]); ?></div>
    <?php if (!empty($propertyAttribute->image)): ?>
        <div class="col-sm-6">
            <h6><?= __('Current image') ?></h6>
            <img src="<?= $this->Images->scale($propertyAttribute->image, 100, 100) ?>" class="img-responsive m-b-20">
        </div>
    <?php endif; ?>
    <div class="col-sm-2"><?= $this->Form->control('position'); ?></div>
    <div class="col-sm-2 d-flex align-items-center"><?= $this->Form->control('visibility'); ?></div>
</div>

<hr>

<?= $this->element('Structure/action-buttons') ?>
<?= $this->Form->end() ?>
