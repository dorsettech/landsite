<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('position', ['name' => 'Position']) ?></th>
                <th><?= $this->Paginator->sort('title', ['name' => 'Title']) ?></th>
                <th><?= $this->Paginator->sort('page_id', ['name' => 'Page']) ?></th>
                <th><?= $this->Paginator->sort('file_id', ['name' => 'File']) ?></th>
                <th><?= $this->Paginator->sort('visible', ['name' => 'Visible']) ?></th>
                <th><?= $this->Paginator->sort('created', ['name' => 'Created']) ?></th>
                <th><?= $this->Paginator->sort('modified', ['name' => 'Updated']) ?></th>
                <th class="actions"><?= 'Actions' ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($contents as $content): ?>
                <tr>
                    <td><?= $this->Number->format($content->id) ?></td>
                    <td><?= $this->Number->format($content->position) ?></td>
                    <td><?= h($content->title) ?></td>
                    <td><?= $content->has('page') ? h($content->page->name) : '' ?></td>
                    <td><?= $content->has('file') ? $this->Common->displayIcoImage($content->file) : '' ?></td>
                    <td><?= ($content->visible ? '<i class="fa fa-check-square-o"></i>' : '<i class="fa fa-check-o"></i>') ?></td>
                    <td><?= h($this->Transform->date($content->created)) ?></td>
                    <td><?= h($this->Transform->date($content->modified)) ?></td>
                    <td class="actions">
                        <div class="btn-group btn-group-xs">
                            <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $content->id], ['class' => 'btn btn-danger', 'confirm' => 'Please confirm removal site '.h($content->title).'?', 'escape' => false]) ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="paginator text-center">
    <?= $this->Paginator->numbers() ?>
    <p><?= $this->Paginator->counter() ?></p>
</div>