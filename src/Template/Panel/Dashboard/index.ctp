<h1 class="page-header"><?= __('Welcome', $_service) ?> <small><?= __('{0} Dashboard', $_service) ?></small></h1>

<?= $this->GoogleAnalyticsReporting->generateWidgets($analyticsApi) ?>

<?php if (!$this->GoogleAnalyticsReporting->hasErrors($analyticsApi)) : ?>
<div class="row">
    <div class="col-lg-8 align-items-stretch pb-4">
        <div class="panel panel-inverse h-100" data-sortable-id="index-1">
            <div class="panel-heading ui-sortable-handle">
                <h4 class="panel-title"><?= __('Website Analytics (Last {0} Days)', $analyticsTimeSpan) ?></h4>
            </div>
            <div class="panel-body">
                <div id="website-analytics" class="height-sm"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 align-items-stretch pb-4">
        <div class="panel panel-inverse h-100" data-sortable-id="index-6">
            <div class="panel-heading ui-sortable-handle">
                <h4 class="panel-title"><?= __("Analytics Details"); ?></h4>
            </div>
            <div class="panel-body p-t-0">
                <div id="detail-analytics" class="height-sm"></div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-12 col-sm-6 col-lg-4 align-items-stretch pb-4">
        <div class="panel panel-inverse h-100">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <?= $this->Html->link('<i class="fa fa-plus"></i>', ['prefix' => $_admin_prefix, 'controller' => 'Pages', 'action' => 'add'], ['class' => 'btn btn-xs btn-icon btn-circle btn-success', 'escape' => false]) ?>
                </div>
                <h4 class="panel-title"><?= __('Recently Updated Pages') ?></h4>
            </div>
            <div class="panel-body">

                <?php if (iterator_count($Pages) > 0): ?>
                    <div class="table-responsive">
                        <table class="table table-valign-middle">
                            <thead>
                                <tr>
                                    <th><?= __('Modified Page') ?></th>
                                    <th><?= __('Menu') ?></th>
                                    <th class="timestamp"><?= __('Modified') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($Pages as $page): ?>
                                    <tr>
                                        <td><?= $this->Html->link(h($page->name), ['prefix' => $_admin_prefix, 'controller' => 'Pages', 'action' => 'editcontents', $page->id]) ?><br></td>
                                        <td><?= $this->Html->link(h($page->menu->name), ['prefix' => $_admin_prefix, 'controller' => 'Menus', 'action' => 'view', $page->menu->urlname]) ?></td>
                                        <td class="timestamp"><?= $this->Html->badge('<i class="fas fa-clock"></i> ' . $this->Transform->date($page->modified), 'light') ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php else: ?>
                    <hr>
                    <p class="text-center"><?= __('There are no pages yet.') ?></p>
                    <hr>
                <?php endif; ?>
                <div class="text-right">
                    <?php if (iterator_count($Pages) > 0) : ?><?= $this->Html->link(__('View All'), ['prefix' => $_admin_prefix, 'controller' => 'Pages', 'action' => 'index'], ['class' => 'btn btn-info']) ?><?php endif; ?>
                    <?= $this->Html->link(__('Add Page'), ['prefix' => $_admin_prefix, 'controller' => 'Pages', 'action' => 'add'], ['class' => 'btn btn-primary']) ?>
                </div>

            </div>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-lg-4 align-items-stretch pb-4">
        <div class="panel panel-inverse h-100">
            <div class="panel-heading">
                <h4 class="panel-title"><?= __('Recent Messages') ?></h4>
            </div>
            <div class="panel-body">

                <?php if (iterator_count($FormsData) > 0): ?>
                    <div class="table-responsive">
                        <table class="table table-valign-middle">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?= __('IP') ?></th>
                                    <th><?= __('Form') ?></th>
                                    <th class="timestamp"><?= __('Created') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($FormsData as $message): ?>
                                    <tr>
                                        <td><?= $this->Html->link('#' . $message->id, ['prefix' => $_admin_prefix, 'controller' => 'FormsData', 'action' => 'view', $message->id]) ?><?php if (!$message->view) : ?><?= $this->Html->badge(__('New'), 'primary', ['class' => 'pull-right']) ?><?php endif; ?></td>
                                        <td><?= $this->Html->link($message->ip, ['prefix' => $_admin_prefix, 'controller' => 'FormsData', 'action' => 'view', $message->id]) ?></td>
                                        <td><?= $this->Html->link(h($message->form->name), ['prefix' => $_admin_prefix, 'controller' => 'FormsData', 'action' => 'ListView', $message->id_forms]) ?></td>
                                        <td class="timestamp"><?= $this->Html->badge('<i class="far fa-clock"></i> ' . $this->Transform->date($message->created), 'light') ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php else: ?>
                    <hr>
                    <p class="text-center"><?= __('There are no messages yet.') ?></p>
                    <hr>
                <?php endif; ?>
                <div class="text-right">
                    <?= $this->Html->link('View All', ['prefix' => $_admin_prefix, 'controller' => 'Forms'], ['class' => 'btn btn-info']) ?>
                </div>
            </div>
        </div>
    </div>
    <?php if ($_user['is_root']): ?>
        <div class="col-12 col-sm-6 col-lg-4 align-items-stretch pb-4">
            <div class="panel panel-inverse h-100">
                <div class="panel-heading">
                    <h4 class="panel-title"><?= __('Recent Logs') ?></h4>
                </div>
                <div class="panel-body">
                    <?php if (iterator_count($Logs) > 0): ?>
                        <ul class="media-list media-list-with-divider media-messaging">
                            <?php foreach ($Logs as $log): ?>
                                <li class="media media-xs">
                                    <?= $this->Html->link($this->Html->image('/' . $this->Images->scale($log->user->image, 70, 70, 'ratio_crop'), ['alt' => $log->user->full_name, 'class' => 'media-object rounded-corner']), ['controller' => 'Users', 'action' => 'edit', $log->user->id], ['class' => 'pull-left', 'escape' => false]) ?>
                                    <div class="media-body">
                                        <h5 class="media-heading"><?= $log->model ?> <?= $this->Html->badge('<i class="far fa-clock"></i> ' . $this->Transform->date($log->created), 'light', ['class' => 'pull-right']) ?></h5>
                                        <p><?= h($log->content) ?></p>

                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php else: ?>
                        <hr>
                        <p class="text-center"><?= __('There are no logs yet.') ?></p>
                        <hr>
                    <?php endif; ?>
                    <div class="text-right">
                        <?= $this->Html->link(__('View All'), ['controller' => 'Logs', 'action' => 'index'], ['class' => 'btn btn-info']) ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?= $this->element('DashboardWidgets/server-settings') ?>
</div>
<?= $this->Html->script('/plugins/flot/jquery.flot.min.js', ['block' => 'scriptBottom']) ?>
<?= $this->Html->script('/plugins/flot/jquery.flot.time.min.js', ['block' => 'scriptBottom']) ?>
<?= $this->Html->script('/plugins/flot/jquery.flot.resize.min.js', ['block' => 'scriptBottom']) ?>
<?= $this->Html->script('/plugins/flot/jquery.flot.pie.min.js', ['block' => 'scriptBottom']) ?>
<?= $this->Html->script('/plugins/sparkline/jquery.sparkline.js', ['block' => 'scriptBottom']) ?>
<?= $this->Html->script('/panel/js/dashboard.js', ['block' => 'scriptBottom']) ?>
