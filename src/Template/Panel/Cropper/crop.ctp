<div class="row cropper">
    <div class="col-sm-8">
        <h4 class="title-original"><?= __('Orginal image') ?></h4>
        <div class="buttons">
            <div class="btn-group">
                <?= $this->Form->button('<i class="fas fa-search-plus"></i>', ['type' => 'button', 'class' => 'btn btn-light btn-zoom-in', 'escape' => false, 'title' => __('Zoom in'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                <?= $this->Form->button('<i class="fas fa-search-minus"></i>', ['type' => 'button', 'class' => 'btn btn-light btn-zoom-out', 'escape' => false, 'title' => __('Zoom in'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                <?= $this->Form->button('<i class="fas fa-redo"></i>', ['type' => 'button', 'class' => 'btn btn-light btn-rotate-right', 'escape' => false, 'title' => __('Rotate right'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                <?= $this->Form->button('<i class="fas fa-undo"></i>', ['type' => 'button', 'class' => 'btn btn-light btn-rotate-left', 'escape' => false, 'title' => __('Rotate left'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                
            </div>
            <div class="btn-group">
                <span class="btn btn-light"><?= __('Crop Ratio') ?>:</span>
                <?= $this->Form->button('<span>16:9</span>', ['type' => 'button', 'class' => 'btn btn-light btn-ratio' . ($aspectRatio == (string)(16/9) ? ' active' : ''), 'data-ratio' => 16/9, 'escape' => false, 'title' => __('16:9'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                <?= $this->Form->button('<span>4:3</span>', ['type' => 'button', 'class' => 'btn btn-light btn-ratio' . ($aspectRatio == (string)(4/3) ? ' active' : ''), 'data-ratio' => 4/3, 'escape' => false, 'title' => __('4:3'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                <?= $this->Form->button('<span>3:2</span>', ['type' => 'button', 'class' => 'btn btn-light btn-ratio' . ($aspectRatio == (string)(3/2) ? ' active' : ''), 'data-ratio' => 3/2, 'escape' => false, 'title' => __('3:2'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                <?= $this->Form->button('<span>1:1</span>', ['type' => 'button', 'class' => 'btn btn-light btn-ratio' . ($aspectRatio == (string)(1) ? ' active' : ''), 'data-ratio' => 1, 'escape' => false, 'title' => __('1:1'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                <?= $this->Form->button('<span>Free</span>', ['type' => 'button', 'class' => 'btn btn-light btn-ratio' . ($aspectRatio == 'NaN' ? ' active' : ''), 'data-ratio' => 'NaN', 'escape' => false, 'title' => __('Free'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
            </div>
        </div>
        <div class="img-source"><?= $this->Html->image('/' . $this->Images->scale($path, 1920, 1920, 'ratio'), ['class' => 'img-responsive img-crop', 'height' => '700']) ?></div>
    </div>
    <div class="col-sm-4">
        <h4 class="title-preview"><?= __('Preview image') ?></h4>
        <div class="img-preview preview-lg"></div>
    </div>
</div>
<hr>
<?= $this->Form->create(null, ['class' => 'form-crop']); ?>
<?= $this->Form->control('image', ['type' => 'hidden', 'class' => 'input-image']); ?>
<div class="row actionbuttons">
    <div class="col-6">
        <?= (!empty($_referer) ? $this->Html->link(__('Back'), $_referer, ['class' => 'btn btn-warning']) : '' ) ?>
        <?= (!empty($restoreUrl) ? $this->Html->link(__('Restore orginal image'), $restoreUrl, ['class' => 'btn btn-danger']) : '') ?>
    </div>
    <div class="col-6 text-right">
        <?= $this->Form->button(__('Save'), ['type' => 'button', 'class' => 'btn btn-primary btn-save']) ?>
    </div>
</div>
<?= $this->Form->end(); ?>
<?php
$this->Html->css('/plugins/cropper/cropper.min.css', ['block' => 'cssHead']);
$this->Html->script('/plugins/cropper/cropper.min.js', ['block' => 'scriptBottom']);
?>
<script>
    var aspectRatioView = <?= $aspectRatio; ?>;
</script>

