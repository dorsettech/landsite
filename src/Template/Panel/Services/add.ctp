<?= $this->Form->create($service) ?>
<?= $this->element('Structure/action-buttons') ?>

<hr>

<h4><?= __('Details') ?></h4>
<div class="row">
    <div class="col-sm-6"><?= $this->Form->control('company'); ?></div>
    <div class="col-sm-6"><?= $this->Form->control('slug'); ?></div>
    <div class="col-sm-6"><?= $this->Form->control('user_id', ['type' => 'select', 'empty' => __('Please choose'), 'options' => $users, 'class' => 'select2', 'data-type' => 'members', 'min-length' => 0]); ?></div>
    <div class="col-sm-6"><?= $this->Form->control('category_id', ['type' => 'select', 'empty' => __('Please choose'), 'options' => $categories, 'class' => 'select2', 'data-type' => 'categories', 'min-length' => 0]); ?></div>
    <div class="col-sm-3 col-md-4"><?= $this->Form->control('location'); ?></div>
    <div class="col-sm-3 col-md-2"><?= $this->Form->control('postcode'); ?></div>
    <div class="col-sm-2"><?= $this->Form->control('phone'); ?></div>
    <div class="col-sm-2"><?= $this->Form->control('lat'); ?></div>
    <div class="col-sm-2"><?= $this->Form->control('lng'); ?></div>
    <div class="col-sm-6"><?= $this->Form->control('opening_days', ['type' => 'select', 'options' => $openingDays, 'multiple' => true, 'class' => 'select2']); ?></div>
    <div class="col-sm-3"><?= $this->Form->control('opening_time_from', ['type' => 'text', 'empty' => true, 'class' => 'time-from', 'val' => $this->Transform->date($service->opening_time_from, 'time')]); ?></div>
    <div class="col-sm-3"><?= $this->Form->control('opening_time_to', ['type' => 'text', 'empty' => true, 'class' => 'time-to', 'val' => $this->Transform->date($service->opening_time_to, 'time')]); ?></div>
    <div class="col-12"><?= $this->Form->control('description_short'); ?></div>
    <div class="col-12"><?= $this->Form->control('description', ['type' => 'textarea', 'class' => 'ckeditor']); ?></div>
    <div class="col-sm-6 col-md-4"><?= $this->Form->control('quick_win_1', ['label' => __('Business advantage 1')]); ?></div>
    <div class="col-sm-6 col-md-4"><?= $this->Form->control('quick_win_2', ['label' => __('Business advantage 2')]); ?></div>
    <div class="col-sm-6 col-md-4"><?= $this->Form->control('quick_win_3', ['label' => __('Business advantage 3')]); ?></div>
    <div class="col-sm-6"><?= $this->Form->control('status', ['type' => 'select', 'options' => $statuses, 'default' => 'DRAFT']); ?></div>
    <div class="col-sm-6"><?= $this->Form->control('publish_date', ['type' => 'text', 'empty' => true, 'class' => 'datetime', 'val' => $this->Transform->date($service->publish_date)]); ?></div>
</div>

<hr>

<h4><?= __('Areas We Cover') ?></h4>
<div class="row">
    <div class="col-12"><?= $this->Form->control('areas._ids', ['type' => 'select', 'label' => false, 'options' => $areas, 'class' => 'select2', 'multiple' => true, 'data-type' => 'areas', 'min-length' => 0]) ?></div>
</div>

<hr>

<h4><?= __('Products & Services') ?></h4>
<div class="row">
    <div class="col-12"><?= $this->Form->control('products._ids', ['type' => 'select', 'label' => false, 'options' => $products, 'class' => 'select2', 'multiple' => true, 'data-type' => 'products', 'min-length' => 0]) ?></div>
</div>

<hr>

<div class="row">
    <div class="col-12 col-sm-6">
        <h4><?= __('Accreditations') ?></h4>
        <?= $this->Form->control('accreditations._ids', ['type' => 'select', 'label' => false, 'options' => $accreditations, 'id' => 'accreditations', 'class' => 'select2', 'multiple' => true, 'data-type' => 'accreditations', 'min-length' => 0]) ?>
    </div>
    <div class="col-12 col-sm-6">
        <h4><?= __('Associations') ?></h4>
        <?= $this->Form->control('associations._ids', ['type' => 'select', 'label' => false, 'options' => $associations, 'id' => 'associations', 'class' => 'select2', 'multiple' => true, 'data-type' => 'associations', 'min-length' => 0]) ?>
    </div>
</div>

<hr>

<h4><?= __('Other') ?></h4>
<div class="row">
    <div class="col-sm-6"><?= $this->Form->control('website_url'); ?></div>
    <div class="col-sm-6"><?= $this->Form->control('google_url'); ?></div>
    <div class="col-sm-6"><?= $this->Form->control('facebook_url'); ?></div>
    <div class="col-sm-6"><?= $this->Form->control('linkedin_url'); ?></div>
    <div class="col-sm-6"><?= $this->Form->control('twitter_url'); ?></div>
    <div class="col-sm-6"><?= $this->Form->control('hubspot_url'); ?></div>
</div>

<hr>

<h4><?= __('Contacts') ?></h4>
<div class="contains" data-max-rows="5">
    <?php if (!empty($service->service_contacts)) : ?>
        <?php foreach ($service->service_contacts as $i => $contact) : ?>
            <div class="row">
                <?= $this->Form->control('service_contacts.' . $i . '.id') ?>
                <div class="col-5"><?= $this->Form->control('service_contacts.' . $i . '.name', ['label' => __('Person Name')]) ?></div>
                <div class="col-5"><?= $this->Form->control('service_contacts.' . $i . '.email', ['label' => __('Email')]) ?></div>
                <div class="col-2">
                    <label class="d-block">&nbsp;</label>
                    <?php if ($i == 0) : ?>
                    <?= $this->Form->button('<i class="fa fa-plus"></i>', ['type' => 'button', 'bootstrap-type' => 'success', 'disabled' => false, 'escape' => false, 'class' => 'add-contain']) ?>
                    <?php else : ?>
                    <?= $this->Form->button('<i class="fa fa-times"></i>', ['type' => 'button', 'bootstrap-type' => 'warning', 'disabled' => false, 'escape' => false, 'class' => 'remove-contain']) ?>
                    <?php endif;  ?>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="row">
            <div class="col-5"><?= $this->Form->control('service_contacts.0.name', ['label' => __('Person Name')]) ?></div>
            <div class="col-5"><?= $this->Form->control('service_contacts.0.email', ['label' => __('Email')]) ?></div>
            <div class="col-2">
                <label class="d-block">&nbsp;</label>
                <?= $this->Form->button('<i class="fa fa-plus"></i>', ['type' => 'button', 'bootstrap-type' => 'success', 'disabled' => false, 'escape' => false, 'class' => 'add-contain']) ?>
            </div>
        </div>
    <?php endif; ?>
</div>

<hr>

<?= $this->element('Structure/action-buttons') ?>
<?= $this->Form->end() ?>

