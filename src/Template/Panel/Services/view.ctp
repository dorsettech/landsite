<ul class="nav nav-tabs" id="page-tabs" role="tablist">
    <li class="nav-item">
        <?= $this->Html->link(__('Professional Service Details'), '#tab-service-details-content', ['class' => 'nav-link settings active', 'id' => 'tab-service-details', 'aria-controls' => 'tab-service-details-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
    </li>
    <li class="nav-item">
        <?= $this->Html->link(__('Other Details'), '#tab-other-details-content', ['class' => 'nav-link settings', 'id' => 'tab-other-details', 'aria-controls' => 'tab-other-details-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
    </li>
    <li class="nav-item">
        <?= $this->Html->link(__('Stats'), '#tab-stats-content', ['class' => 'nav-link settings', 'id' => 'tab-stats', 'aria-controls' => 'tab-stats-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
    </li>
</ul>
<div class="tab-content" id="tabs-content">
    <div class="tab-pane fade show active" id="tab-service-details-content" role="tabpanel" aria-labelledby="tab-service-details">
        <div class="p-15">

            <div class="row view m-b-15">
                <div class="col-sm-6 col-lg-3">
                    <label><i class="far fa-building"></i> <?= __('Company Name') ?></label>
                    <?= $service->company ?>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <label><i class="far fa-user"></i> <?= __('First Name, Last Name') ?></label>
                    <?= $service->user->full_name ?>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <label><i class="far fa-envelope"></i> <?= __('Email Address') ?></label>
                    <a href="<?= $this->Url->build('mailto:' . $service->user->email) ?>"><?= $service->user->email ?></a>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <label><i class="fas fa-phone"></i> <?= __('Phone') ?></label>
                    <?= $service->user->phone ?>
                </div>
            </div>

            <hr>

            <div class="row view m-b-15">
                <div class="col-sm-6 col-lg-3">
                    <label><i class="fas fa-list"></i> <?= __('Category') ?></label>
                    <?= $service->category->name ?>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <label><i class="fas fa-map-marker"></i> <?= __('Location, Postcode') ?></label>
                    <?= $service->location ?>, <?= $service->postcode ?>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <label><i class="fas fa-map"></i> <?= __('Lat, Lng') ?></label>
                    <?= $service->lat ?><?= (!empty($service->lat) && !empty($service->lat) ? ',' : '') ?> <?= $service->lng ?>
                </div>
            </div>

            <hr>

            <div class="row view m-b-15">
                <div class="col-sm-6 col-lg-4">
                    <label><i class="far fa-calendar-alt"></i> <?= __('Opening Days') ?></label>
                    <?= $service->opening_day_names ?>
                </div>
                <div class="col-sm-6 col-lg-2">
                    <label><i class="far fa-clock"></i> <?= __('Opening Times') ?></label>
                    <?= $this->Transform->date($service->opening_time_from, 'time') ?> - <?= $this->Transform->date($service->opening_time_to, 'time') ?>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <label><i class="fas fa-tag"></i> <?= __('Status') ?></label>
                    <?= $service->status_name ?>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <label><i class="fas fa-clock"></i> <?= __('Publish Date') ?></label>
                    <?= $this->Transform->date($service->publish_date, 'datetimeuk') ?>
                </div>
            </div>

            <hr>

            <div class="row view m-b-15">
                <div class="col-sm-6 col-lg-3">
                    <label><i class="far fa-plus-square"></i> <?= __('Business Advantage 1') ?></label>
                    <?= $service->quick_win_1 ?>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <label><i class="far fa-plus-square"></i> <?= __('Business Advantage 2') ?></label>
                    <?= $service->quick_win_2 ?>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <label><i class="far fa-plus-square"></i> <?= __('Business Advantage 3') ?></label>
                    <?= $service->quick_win_3 ?>
                </div>
            </div>

            <hr>

            <h4><?= __('Description') ?></h4>
            <?= $service->description ?>
            
            <hr>

            <h4><?= __('Contacts') ?></h4>
            <div class="row view">
            <?php foreach ($service->service_contacts as $key => $contact) : ?>
                <div class="col-12"><?= $key + 1 ?>. <?= $contact->name ?> (<a href="mailto:<?= $contact->email ?>"><?= $contact->email ?></a>)</div>
            <?php endforeach; ?>
            </div>
            
        </div>
    </div>
    <div class="tab-pane fade" id="tab-other-details-content" role="tabpanel" aria-labelledby="tab-other-details">
        <div class="p-15">

            <div class="row view m-b-15">
                <div class="col-sm-6">
                    <label><i class="fas fa-globe"></i> <?= __('Website Url') ?></label>
                    <?= $service->website_url ?>
                </div>
                <div class="col-sm-6">
                    <label><i class="fab fa-google"></i> <?= __('Google Url') ?></label>
                    <?= $service->google_url ?>
                </div>
                <div class="col-sm-6">
                    <label><i class="fab linkedin-in"></i> <?= __('Facebook Url') ?></label>
                    <?= $service->facebook_url ?>
                </div>
                <div class="col-sm-6">
                    <label><i class="fab fa-linkedin-in"></i> <?= __('LinkedIn Url') ?></label>
                    <?= $service->linkedin_url ?>
                </div>
                <div class="col-sm-6">
                    <label><i class="fab fa-twitter"></i> <?= __('Twitter Url') ?></label>
                    <?= $service->twitter_url ?>
                </div>
                <div class="col-sm-6">
                    <label><i class="fab fa-hubspot"></i> <?= __('Hubspot Url') ?></label>
                    <?= $service->hubspot_url ?>
                </div>
            </div>

        </div>
    </div>
    <div class="tab-pane fade" id="tab-stats-content" role="tabpanel" aria-labelledby="tab-stats">
        <div class="p-15">

            <div class="row view m-b-15">
                <div class="col-sm-6 col-lg-4">
                    <label><i class="fab fa-adversal"></i> <?= __('Ad Type') ?></label>
                    <?= $service->ad_type_name ?>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <label><i class="far fa-clock"></i> <?= __('Ad Expiry Date') ?></label>
                    <?= $this->Transform->date($service->ad_expiry_date, 'datetime') ?>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <label><i class="fa fa-users"></i> <?= __('Properties Visitors') ?></label>
                    <?= $propertiesViews ?>
                </div>
            </div>

            <hr>

            <div class="row view m-b-15">
                <div class="col-sm-6 col-lg-2">
                    <label><i class="fas fa-pound-sign"></i> <?= __('Ad Cost') ?></label>
                    <?= $service->ad_cost ?>
                </div>
                <div class="col-sm-6 col-lg-2">
                    <label><i class="fas fa-pound-sign"></i> <?= __('Ad Total Cost') ?></label>
                    <?= $service->ad_total_cost ?>
                </div>
                <div class="col-sm-6 col-lg-2">
                    <label><i class="fas fa-clipboard-list"></i> <?= __('Enquiries') ?></label>
                    <?= $service->enquiries ?>
                </div>
                <div class="col-sm-6 col-lg-2">
                    <label><i class="fas fa-eye"></i> <?= __('Views') ?></label>
                    <?= $service->views ?>
                </div>
                <div class="col-sm-6 col-lg-2">
                    <label><i class="fas fa-hand-pointer"></i> <?= __('Website button clicks') ?></label>
                    <?= $websiteClicks ?>
                </div>
                <div class="col-sm-6 col-lg-2">
                    <label><i class="fas fa-phone"></i> <?= __('Phone number clicks') ?></label>
                    <?= $phoneClicks ?>
                </div>
            </div>

        </div>
    </div>
</div>

<?= $this->element('Structure/view-actions', ['id' => $service->id]) ?>
