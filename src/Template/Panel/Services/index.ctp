<?php

use App\Model\Enum\State;

?>
<?php if ($services->count() > 0) : ?>
    <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('company') ?></th>
                    <th><?= $this->Paginator->sort('category_id', ['label' => __('Business Category')]) ?></th>
                    <th><?= $this->Paginator->sort('status') ?></th>
                    <th><?= $this->Paginator->sort('state') ?></th>
                    <th><?= $this->Paginator->sort('postcode') ?></th>
                    <th><?= $this->Paginator->sort('user_id', ['label' => __('Member')]) ?></th>
                    <th><?= $this->Paginator->sort('ad_total_cost') ?></th>
                    <th><?= $this->Paginator->sort('created') ?> / <?= $this->Paginator->sort('modified') ?></th>
                    <th><?= $this->Paginator->sort('visibility', ['label' => 'Visible']) ?></th>
                    <th><?= $this->Paginator->sort('send_in_email', ['label' => 'In email']) ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($services as $service): ?>
                    <tr>
                        <td><?= $this->Number->format($service->id) ?></td>
                        <td><?= h($service->company) ?><br><small><?= h($service->slug) ?></small></td>
                        <td><?= $service->has('category') ? $this->Html->link($service->category->name, ['controller' => 'Categories', 'action' => 'edit', $service->category->id]) : '' ?></td>
                        <td><?= h($service->status) ?></td>
                        <td><?= h($service->state) ?></td>
                        <td><?= h($service->postcode) ?></td>
                        <td>
                            <?php if ($service->has('user')) : ?>
                                <?= $this->Html->link($service->user->full_name, ['controller' => 'Users', 'action' => 'edit', $service->user->id]) ?><br>
                                <?php if (!empty($service->user->email)) : ?><i class="far fa-envelope"></i> <a href="mailto:<?= $service->user->email ?>"><?= $service->user->email ?></a><br><?php endif; ?>
                                <?php if (!empty($service->user->phone)) : ?><i class="fas fa-phone"></i> <?= $service->user->phone ?><?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td><?= $this->Number->format($service->ad_total_cost) ?></td>
                        <td>
                            <?= $this->Html->badge('<i class="far fa-clock"></i> ' . h($this->Transform->date($service->created)), 'light') ?><br>
                            <?= $this->Html->badge('<i class="fa fa-clock"></i> ' . h($this->Transform->date($service->modified)), 'light') ?>
                        </td>
                        <td><?= $this->Switcher->create('visibility', ['val' => $service->visibility, 'data-model' => 'Services', 'data-id' => $service->id]) ?></td>
                        <td><?= $this->Switcher->create('send_in_email', ['val' => $service->send_in_email, 'data-model' => 'Services', 'data-id' => $service->id]) ?></td>
                        <td class="actions">
                            <div class="btn-group btn-group-xs">
                                <?php if ($service->state === State::PENDING && $service->visibility): ?>
                                    <?= $this->Html->link('<i class="fa fa-check-circle"></i>', '#', ['class' => 'btn btn-default approve-reject-service-button', 'escape' => false, 'title' => __('Approve/Reject'), 'data-toggle' => 'modal', 'data-target' => '#approve-reject-service-modal', 'data-id' => $service->id]) ?>
                                <?php endif; ?>
                                <?= $this->Html->link('<i class="fa fa-eye"></i>', ['action' => 'view', $service->id], ['class' => 'btn btn-default', 'escape' => false, 'title' => __('View'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                                <?= $this->Html->link('<i class="fa fa-images"></i>', ['controller' => 'ServiceMedia', 'action' => 'list', $service->id], ['class' => 'btn btn-info', 'escape' => false, 'title' => __('Media'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                                <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $service->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Edit'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                                <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $service->id], ['class' => 'btn btn-danger', 'confirm' => __('Please confirm removal Service #{0}', $service->id), 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php else : ?>
    <div class="well text-center text-warning"><?= __('There are no Services yet.') ?> <?= $this->Html->link(__('Add first Service'), ['action' => 'add']) ?></div>
<?php endif; ?>
<div class="row action-buttons">
    <div class="col text-right">
        <?= $this->Html->link(__('Add Service'), ['action' => 'add'], ['class' => 'btn btn-primary']); ?>
    </div>
</div>
<?= $this->element('pagination') ?>
<?= $this->element('Structure/approve-reject-service-modal') ?>
