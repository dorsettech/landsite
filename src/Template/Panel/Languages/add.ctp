<?= $this->Form->create($language) ?>
<?= $this->element('Structure/action-buttons') ?>
<hr>
<div class="row">
    <div class="col-sm-8"><?= $this->Form->control('name'); ?></div>
    <div class="col-sm-4"><?= $this->Form->control('locale'); ?></div>
</div>
<div class="row">
    <div class="col-sm-2"><?= $this->Form->control('position'); ?></div>
    <div class="col-sm-2 d-flex align-items-center"><?= $this->Form->control('is_default', ['type' => 'checkbox', 'label' => __('Default')]); ?></div>
    <div class="col-sm-2 d-flex align-items-center"><?= $this->Form->control('active', ['type' => 'checkbox']); ?></div>
</div>
<hr>
<?= $this->element('Structure/action-buttons') ?>
<?= $this->Form->end() ?>
