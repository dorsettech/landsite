<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('position', ['name' => 'Position']) ?></th>
                <th><?= $this->Paginator->sort('name', ['name' => 'Name']) ?></th>
                <th><?= $this->Paginator->sort('locale', ['name' => 'Locale']) ?></th>
                <th><?= $this->Paginator->sort('is_default', ['name' => 'Default']) ?></th>
                <th><?= $this->Paginator->sort('active', ['name' => 'Active']) ?></th>
                <th><?= $this->Paginator->sort('created', ['name' => 'Created']) ?> / <?= $this->Paginator->sort('modified', ['name' => 'Modified']) ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($languages as $language): ?>
                <tr>
                    <td><?= $this->Number->format($language->id) ?></td>
                    <td><?= $this->Number->format($language->position) ?></td>
                    <td><?= $this->Html->link(h($language->name), ['action' => 'edit', $language->id], ['escape' => false]); ?></td>
                    <td><?= h($language->locale) ?></td>
                    <td><?= $this->Switcher->create('is_default', ['val' => $language->is_default, 'data-id' => $language->id]); ?></td>
                    <td><?= $this->Switcher->create('active', ['val' => $language->active, 'data-id' => $language->id]); ?></td>
                    <td>
                        <?= $this->Html->badge('<i class="far fa-clock"></i> '.h($this->Transform->date($language->created)), 'light') ?><br>
                        <?= $this->Html->badge('<i class="fa fa-clock"></i> '.h($this->Transform->date($language->modified)), 'light') ?>
                    </td>
                    <td class="actions">
                        <div class="btn-group btn-group-xs">
                            <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $language->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Edit'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $language->id], ['class' => 'btn btn-danger', 'confirm' => __('Please confirm removal language {0}', h($language->name)), 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?= $this->element('Structure/add-button') ?>
<?= $this->element('pagination') ?>