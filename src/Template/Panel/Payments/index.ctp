<div class="table-responsive">
    <?php if ($payments->count() > 0) : ?>
        <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('user_id') ?></th>
                    <th><?= $this->Paginator->sort('email') ?></th>
                    <th><?= $this->Paginator->sort('phone') ?></th>
                    <th><?= $this->Paginator->sort('service', ['label' => 'Company']) ?></th>
                    <th><?= $this->Paginator->sort('title', ['label' => 'Item']) ?></th>
                    <th><?= $this->Paginator->sort('amount') ?></th>
                    <th><?= $this->Paginator->sort('created', ['name' => 'Created']) ?> / <?= $this->Paginator->sort('modified', ['name' => 'Modified']) ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($payments as $payment): ?>
                    <tr>
                        <td><?= $this->Number->format($payment->id) ?></td>
                        <td><?= $payment->has('user') ? $payment->user->full_name : '' ?></td>
                        <td><?= $payment->has('user') ? $payment->user->email : '' ?></td>
                        <td><?= $payment->has('user') ? $payment->user->phone : '' ?></td>
                        <td><?= ($payment->has('user') && $payment->user->has('service')) ? $payment->user->service->company : '' ?></td>
                        <td><?= $payment->title ?></td>
                        <td><?= $this->Number->currency($payment->amount, $payment->currency) ?></td>
                        <td>
                            <?= $this->Html->badge('<i class="far fa-clock"></i> '.h($this->Transform->date($payment->created)), 'light') ?><br>
                            <?= $this->Html->badge('<i class="fa fa-clock"></i> '.h($this->Transform->date($payment->modified)), 'light') ?>
                        </td>
                        <td class="actions">
                            <div class="btn-group btn-group-xs">
                                <?= $this->Html->link('<i class="fa fa-file-pdf"></i>', ['controller' => 'ExportPdf', 'action' => 'invoice', $payment->id], ['class' => 'btn btn-default', 'escape' => false, 'title' => __('Generate Invoice'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                                <?= $this->Html->link('<i class="fa fa-file-pdf"></i>', ['controller' => 'ExportPdf', 'action' => 'order', $payment->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Generate Order PDF'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else : ?>
        <div class="well text-center text-warning"><?= __('There are no Payments yet.') ?> <?= $this->Html->link(__('Add first Payment'), ['action' => 'add']) ?></div>
    <?php endif; ?>
</div>
<?= $this->element('pagination') ?>