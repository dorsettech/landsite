<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('redirection_from') ?></th>
                <th><?= $this->Paginator->sort('redirect_to') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($redirections as $redirection): ?>
                <tr>
                    <td><?= $this->Number->format($redirection->id) ?></td>
                    <td><?= h($redirection->redirection_from) ?></td>
                    <td><?= h($redirection->redirect_to) ?></td>
                    <td class="actions">
                        <div class="btn-group btn-group-xs">
                            <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $redirection->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Edit'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $redirection->id], ['class' => 'btn btn-danger', 'confirm' => __('Please confirm removal redirection {0}', h($redirection->id)), 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?= $this->element('Structure/add-button') ?>
<?= $this->element('pagination') ?>
