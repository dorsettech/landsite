<?= $this->Form->create($redirection) ?>
<?= $this->element('Structure/action-buttons') ?>
<div class="row">
    <div class="col-sm-6"><?= $this->Form->control('redirection_from', ['templateVars' => ['help' => __('Redirection should start with "/" sign at beginning.')]]); ?></div>
    <div class="col-sm-6"><?= $this->Form->control('redirect_to'); ?></div>
</div>
<?= $this->element('Structure/action-buttons') ?>
<?= $this->Form->end() ?>
