<?php if (!empty($menu->pages)) : ?>
    <?= $this->Page->getPageActions(); ?>
    <div class="dd nestable mb-3" id="menu-page">
        <?= $this->Page->getList($menu->pages, ['is_root' => $_user['is_root']]); ?>
    </div>
    <?= $this->Page->getPageActions(); ?>
<?php else : ?>
    <div class="alert alert-danger text-center marg-b"><?= __('No pages.') ?></div>
<?php endif; ?>
<?php
$this->Html->script('/plugins/nestable/jquery.nestable', ['block' => 'scriptBottom']);
?>