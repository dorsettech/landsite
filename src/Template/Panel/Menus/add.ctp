<?= $this->Form->create($menu, ['type' => 'file']) ?>
<?= $this->element('Structure/action-buttons') ?>
<div class="row">
    <div class="col-12"><?= $this->Form->control('name'); ?></div>
    <div class="col-12"><?= $this->Form->control('urlname'); ?></div>
    <div class="col-12"><?= $this->Form->control('image', ['type' => 'file', 'required' => false]); ?></div>
    <div class="col-12"><?= $this->Form->control('position'); ?></div>
    <div class="col-6 col-sm-3 col-lg-2"><?= $this->Form->control('is_visible'); ?></div>
</div>
<?= $this->element('Structure/action-buttons') ?>
<?= $this->Form->end() ?>
