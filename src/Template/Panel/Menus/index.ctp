<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('position', ['name' => 'Position']) ?></th>
                <th><?= $this->Paginator->sort('name', ['name' => 'Name']) ?></th>
                <th><?= $this->Paginator->sort('urlname', ['name' => 'URL menu']) ?></th>
                <th><?= $this->Paginator->sort('image', ['name' => 'Icon']) ?></th>
                <th><?= $this->Paginator->sort('is_visible', ['name' => 'Is visible']) ?></th>
                <th><?= $this->Paginator->sort('created', ['name' => 'Created']) ?> / <?= $this->Paginator->sort('modified', ['name' => 'Modified']) ?></th>
                <th class="actions text-right"><?= 'Actions' ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($menus as $menu): ?>
                <tr>
                    <td><?= $this->Number->format($menu->id) ?></td>
                    <td><?= $this->Number->format($menu->position) ?></td>
                    <td><?= h($menu->name) ?></td>
                    <td><?= h($menu->urlname) ?></td>
                    <td>
                        <?php if (!empty($menu->image)): ?>
                            <a href="<?= $menu->image_path ?>" class="magnific-image"><img src="<?= $this->Images->scale($menu->image_path, 50, 50) ?>" class="img-responsive"></a>
                        <?php endif; ?>
                    </td>
                    <td><?= $this->Switcher->create('is_visible', ['val' => $menu->is_visible, 'data-id' => $menu->id]); ?></td>
                    <td>
                        <?= $this->Html->badge('<i class="far fa-clock"></i> '.h($this->Transform->date($menu->created)), 'light') ?><br>
                        <?= $this->Html->badge('<i class="fa fa-clock"></i> '.h($this->Transform->date($menu->modified)), 'light') ?>
                    </td>
                    <td class="actions">
                        <div class="btn-group btn-group-xs">
                            <?= $this->Html->link('<i class="fas fa-stream"></i>', ['action' => 'view', $menu->urlname], ['class' => 'btn btn-default', 'escape' => false, 'title' => __('View pages tree'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $menu->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Edit menu'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $menu->id], ['class' => 'btn btn-danger', 'confirm' => 'Please confirm removal menu '.h($menu->name).'?', 'escape' => false, 'title' => __('Delete menu'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?= $this->element('Structure/add-button') ?>
<?= $this->element('pagination') ?>