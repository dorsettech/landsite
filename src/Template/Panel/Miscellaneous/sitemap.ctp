<div class="marg-b-sm">
    <?php
    echo $this->Form->create(null, ['type' => 'file']);
    echo $this->Form->control('sitemap', ['type' => 'file', 'label' => __('Upload sitemap')]);
    ?>
    <div class="text-right">
        <?= $this->Form->submit(__('Upload'), ['class' => 'btn btn-primary']); ?>
    </div>
    <?= $this->Form->end(); ?>
</div>
<?php if (!$sitemap): ?>
    <div class="alert alert-info"><?= __('Sitemap file not been uploaded yet. It\' generated automatically based on pages structure.') ?> <?= $this->Html->link(__('View sitemap.xml'), '/sitemap.xml', ['target' => '_blank']) ?></div>
<?php else: ?>
    <h3><?= __('Uploaded sitemap') ?>:</h3>
    <?= $this->Form->control('sitemap', ['type' => 'textarea', 'class' => 'code-mirror', 'value' => $sitemap, 'data-readonly' => 'true', 'data-mode' => 'xml']) ?>
<?php endif; ?>
<?php
$this->Html->css('/plugins/codemirror/codemirror.css', ['block' => 'cssHead']);
$this->Html->script('/plugins/codemirror/codemirror.js', ['block' => 'scriptBottom']);
$this->Html->script('/plugins/codemirror/mode/xml/xml.js', ['block' => 'scriptBottom']);
?>