<?= $this->Form->create(); ?>
<div class="row diffWrapper">
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <h4><?= __('Current CSS') ?></h4>
        <?= $this->Form->control('source', ['type' => 'textarea', 'label' => '', 'id' => 'css-current', 'class' => 'code-mirror css-current', 'required' => false, 'value' => $content, 'escape' => true]); ?>
        <?= $this->Html->link(__('Cancel'), ['action' => 'index'], ['class' => 'btn btn-warning', 'escape' => false]); ?>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <h4><?= __('Diff result') ?></h4><br />
        <div class="diff"></div><br />
        <?= $this->Form->button(__('Save this version'), ['bootstrap-type' => 'primary', 'class' => 'pull-right']); ?>
        <?= $this->Form->control('content', ['type' => 'textarea', 'label' => '', 'class' => 'd-none css-diff', 'id' => 'css-diff', 'required' => true, 'escape' => false]); ?>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <?= $this->Html->link(__('Restore'), ['action' => 'restore', $id], ['class' => 'btn btn-primary pull-right']); ?>
        <h4><?= __($this->Transform->date($version->created).' / CSS Version'); ?></h4>
        <?= $this->Form->control('restored', ['type' => 'textarea', 'label' => '', 'id' => 'css-version', 'class' => 'code-mirror css-version', 'required' => false, 'value' => $restored]); ?>
    </div>
</div>
<?= $this->Form->end() ?>

<?php
$this->Html->css('/plugins/codemirror/codemirror.css', ['block' => 'cssHead']);
$this->Html->script('/plugins/codemirror/codemirror.js', ['block' => 'scriptBottom']);
$this->Html->script('/plugins/codemirror/mode/css/css.js', ['block' => 'scriptBottom']);

$this->Html->script('/plugins/diff_match_patch/javascript/diff_match_patch.js', ['block' => 'scriptBottom']);
$this->Html->script('/plugins/preetyTextDiff/jquery.pretty-text-diff.min.js', ['block' => 'scriptBottom']);
?>

