<?= $this->Form->create(); ?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="alert alert-warning">
            <?= __('Please note that you should be familar with CSS language to make any changes in custom file. By editing Custom CSS styles <strong>you may break the website layout</strong> if you\'re not sure what are you doing or by lack of knowledge.') ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-10 col-md-10">
        <?= $this->Form->control('content', ['type' => 'textarea', 'label' => __('Custom CSS'), 'class' => 'code-mirror', 'required' => false, 'value' => $content]); ?>
        <?= $this->Form->button(__('Save'), ['bootstrap-type' => 'primary', 'class' => 'pull-right']); ?>
    </div>
    <div class="col-sm-2 col-md-2">
        <h4><?= __('Version to restore') ?></h4>
        <div class="restored-size">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?= __('Date') ?></th>
                        <th><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($versions->count() > 0): ?>
                        <?php foreach ($versions as $version): ?>
                            <tr>
                                <td>
                                    <?= $this->Html->link($version->id, ['action' => 'edit', $version->id]); ?>
                                </td>
                                <td>
                                    <?= $this->Html->link($this->Transform->date($version->created, 'human'), ['action' => 'edit', $version->id]); ?>
                                </td>
                                <td class="text-center">
                                    <div class="btn-group btn-group-xs">
                                        <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $version->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Edit'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']); ?>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="3"><?= __('No data') ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>
<?php
$this->Html->css('/plugins/codemirror/codemirror.css', ['block' => 'cssHead']);
$this->Html->script('/plugins/codemirror/codemirror.js', ['block' => 'scriptBottom']);
$this->Html->script('/plugins/codemirror/mode/css/css.js', ['block' => 'scriptBottom']);
?>