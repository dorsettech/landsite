<div class="table-responsive">
    <?php if ($serviceSearches->count() > 0) : ?>
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= $this->Paginator->sort('location') ?></th>
                <th><?= $this->Paginator->sort('keywords') ?></th>
                <th><?= $this->Paginator->sort('category_id', ['label' => __('Category')]) ?></th>
                <th><?= $this->Paginator->sort('radius') ?></th>
                <th><?= $this->Paginator->sort('alerts', ['label' => __('Receive Alerts')]) ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($serviceSearches as $serviceSearch): ?>
                <tr>
                    <td><?= $this->Number->format($serviceSearch->id) ?></td>
                    <td><?= $serviceSearch->has('user') ? $this->Html->link($serviceSearch->user->full_name, ['controller' => 'Members', 'action' => 'view', $serviceSearch->user->id]) : '' ?></td>
                    <td><?= h($serviceSearch->location) ?></td>
                    <td><?= h($serviceSearch->keywords) ?></td>
                    <td><?= $serviceSearch->has('category') ? $serviceSearch->category->name : '' ?></td>
                    <td><?= ($serviceSearch->radius > 0) ? $this->Number->format($serviceSearch->radius) : '' ?></td>
                    <td>
                        <?= $this->Switcher->create('alerts', ['val' => $serviceSearch->alerts, 'data-id' => $serviceSearch-> id]); ?>
                    </td>
                    <td><?= $this->Html->badge('<i class="far fa-clock"></i> ' . h($this->Transform->date($serviceSearch->created)), 'light') ?></td>
                    <td class="actions">
                        <div class="btn-group btn-group-xs">
                            <?= $this->Html->link('<i class="fas fa-search"></i>', ['_name' => 'professional-services-listing', '?' => $serviceSearch->query_array], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Search'), 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'target' => '_blank']) ?>
                            <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $serviceSearch->id], ['class' => 'btn btn-danger', 'confirm' => __('Please confirm removal Service Search #{0}', $serviceSearch->id), 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php else : ?>
    <div class="well text-center text-warning"><?= __('There are no Service Searches yet.') ?></div>
    <?php endif; ?>
</div>
<?= $this->element('pagination') ?>
