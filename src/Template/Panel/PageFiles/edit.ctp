<?php

use App\Library\PageFiles;

?>
<?= $this->Form->create($pageFile, ['type' => 'file']) ?>
<?= $this->element('Structure/action-buttons', ['cancelUrl' => ['controller' => 'Pages', 'action' => 'edit-contents', $pageFile->page_id]]) ?>
<div class="row">
    <div class="col-12 col-md-6">
        <?= $this->Form->control('position', ['type' => 'number', 'label' => 'Position']); ?>
    </div>
    <?php if (PageFiles::TYPE_ID_SLIDER || PageFiles::TYPE_ID_GALLERY) : ?>
        <div class="col-12 col-md-6">
            <a href="<?= $this->Images->scale($pageFile->path, 900, 900, 'ratio') ?>" class="magnific-image">
                <?= $this->Html->image('/' . $this->Images->scale($pageFile->path, 100, 50, 'ratio_crop'), ['class' => 'img-thumbnail']) ?>
            </a>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-12 col-md-6">
        <?= $this->Form->control('name', ['type' => 'text', 'label' => 'File name']); ?>
    </div>
    <div class="col-12 col-md-6">
        <?= $this->Form->control('urlname', ['type' => 'text', 'label' => 'URL / Redirection']); ?>
    </div>
    <div class="col-12 col-md-6">
        <?= $this->Form->control('title', ['type' => 'text', 'label' => 'Title']); ?>
    </div>
    <div class="col-12 col-md-6">
        <?= $this->Form->control('alt', ['type' => 'text', 'label' => 'Alt attribute']); ?>
    </div>
    <div class="col-12">
        <?= $this->Form->control('description', ['type' => 'textarea', 'class' => 'ckeditor', 'label' => 'Description']); ?>
    </div>
</div>
<?= $this->element('Structure/action-buttons',['cancelUrl' => ['controller' => 'Pages', 'action' => 'edit-contents', $pageFile->page_id]]) ?>
<?= $this->Form->end() ?>