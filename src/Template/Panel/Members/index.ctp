<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('first_name') ?> / <?= $this->Paginator->sort('last_name') ?></th>
                <th><?= __('Image') ?></th>
                <th><?= $this->Paginator->sort('email') ?></th>
                <th><?= $this->Paginator->sort('phone') ?></th>
                <th><?= $this->Paginator->sort('user_detail.company', ['label' => __('Company')]) ?></th>
                <th><?= $this->Paginator->sort('user_detail.postcode', ['label' => __('Postcode')]) ?></th>
                <th><?= $this->Paginator->sort('active') ?></th>
                <th><?= $this->Paginator->sort('email_verified') ?></th>
                <th><?= $this->Paginator->sort('user_detail.profile_completion', ['label' => __('Profile Completion')]) ?></th>
                <th><?= $this->Paginator->sort('created', ['name' => 'Created']) ?> / <?= $this->Paginator->sort('modified', ['name' => 'Modified']) ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
                <tr>
                    <td><?= $this->Number->format($user->id) ?></td>
                    <td>
                        <?= h($user->first_name) ?> <?= h($user->last_name) ?>
                    </td>
                    <td><?php if (!empty($user->image)) : ?><?= $this->Html->image('/'.$this->Images->scale($user->image_path, 40, 40, 'ratio_fill'), ['alt' => h($user->full_name)]) ?><?php endif; ?></td>
                    <td><?= $this->Html->link(h($user->email), ['controller' => 'Users', 'action' => 'edit', $user->id], ['escape' => false]) ?></td>
                    <td><?= h($user->phone); ?></td>
                    <td><?= !empty($user->user_detail) ? h($user->user_detail->company) : ''; ?></td>
                    <td><?= !empty($user->user_detail) ? h($user->user_detail->postcode) : ''; ?></td>
                    <td><?= $this->Switcher->create('active', ['val' => $user->active, 'data-model' => 'Users', 'data-id' => $user->id]) ?></td>
                    <td><?= $this->Switcher->create('email_verified', ['val' => $user->email_verified, 'data-model' => 'Users', 'data-id' => $user->id]) ?></td>
                    <td>
                        <?php if ($user->has('user_detail')): ?>
                            <?php if ($user->user_detail->profile_completion === true): ?>
                                <?= $this->Html->badge('Yes', 'success') ?>
                            <?php else: ?>
                                <?= $this->Html->badge('No', 'danger') ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <?= $this->Html->badge('No', 'danger') ?>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?= $this->Html->badge('<i class="far fa-clock"></i> '.h($this->Transform->date($user->created)), 'light') ?><br>
                        <?= $this->Html->badge('<i class="fa fa-clock"></i> '.h($this->Transform->date($user->modified)), 'light') ?>
                    </td>
                    <td class="actions">
                        <div class="btn-group btn-group-xs">
                            <?php if (($_user->is_root || $_user->is_admin) && in_array($_user->group->id, $_allowedGroups) && in_array($user->group->id, $_allowedGroups) && ($user->email_verified && $user->active === false && empty($user->reject_reason))): ?>
                                <?= $this->Html->link('<i class="fa fa-check-circle"></i>', '#', ['class' => 'btn btn-default approve-reject-member-button', 'escape' => false, 'title' => __('Approve/Reject'), 'data-toggle' => 'modal', 'data-target' => '#approve-reject-modal', 'data-id' => $user->id]) ?>
                            <?php endif; ?>
                            <?= $this->Html->link('<i class="fa fa-eye"></i>', ['action' => 'view', $user->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('View'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            <?php if (($_user->is_root || $_user->is_admin) && in_array($_user->group->id, $_allowedGroups) && in_array($user->group->id, $_allowedGroups)): ?>
                                <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $user->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Edit'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            <?php endif; ?>
                            <?php if ($_user->id !== $user->id) : ?>
                                <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $user->id], ['class' => 'btn btn-danger', 'confirm' => 'Please confirm removal of user '.h($user->full_name.' ('.h($user->email).')?'), 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            <?php endif; ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?= $this->element('Structure/add-button') ?>
<?= $this->element('pagination') ?>
<?= $this->element('Structure/approve-reject-member-modal') ?>

