<ul class="nav nav-tabs" id="page-tabs" role="tablist">
    <li class="nav-item">
        <?= $this->Html->link(__('Member Details'), '#tab-member-details-content', ['class' => 'nav-link settings active', 'id' => 'tab-member-details', 'aria-controls' => 'tab-member-details-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
    </li>
    <li class="nav-item">
        <?= $this->Html->link(__('Billing Details'), '#tab-billing-details-content', ['class' => 'nav-link settings', 'id' => 'tab-billing-details', 'aria-controls' => 'tab-billing-details-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
    </li>
    <li class="nav-item">
        <?= $this->Html->link(__('Properties List'), '#tab-properties-list-content', ['class' => 'nav-link settings', 'id' => 'tab-properties-list', 'aria-controls' => 'tab-properties-list-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
    </li>
    <li class="nav-item">
        <?= $this->Html->link(__('Business Listing'), '#tab-business-listing-content', ['class' => 'nav-link settings', 'id' => 'tab-business-listing', 'aria-controls' => 'tab-business-listing-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
    </li>
    <li class="nav-item">
        <?= $this->Html->link(__('Articles List'), '#tab-insights-list-content', ['class' => 'nav-link settings', 'id' => 'tab-insights-list', 'aria-controls' => 'tab-insights-list-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
    </li>
    <li class="nav-item">
        <?= $this->Html->link( __('Alerts and Searches'), '#tab-alerts-searches-content', ['class' => 'nav-link settings', 'id' => 'tab-alerts-searches', 'aria-controls' => 'tab-alerts-searches-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
    </li>
    <li class="nav-item">
        <?= $this->Html->link(__('Other'), '#tab-other-content', ['class' => 'nav-link settings', 'id' => 'tab-other', 'aria-controls' => 'tab-other-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
    </li>
</ul>
<div class="tab-content" id="tabs-content">
    <div class="tab-pane fade show active" id="tab-member-details-content" role="tabpanel" aria-labelledby="tab-member-details">
        <div class="p-15">
            <?= $this->element('Members/details') ?>
        </div>
    </div>
    <div class="tab-pane fade" id="tab-billing-details-content" role="tabpanel" aria-labelledby="tab-billing-details">
        <div class="p-15">
            <?= $this->element('Members/billing_details') ?>
        </div>
    </div>
    <div class="tab-pane fade" id="tab-properties-list-content" role="tabpanel" aria-labelledby="tab-properties-list">
        <div class="p-15">
            <?= $this->element('Members/properties') ?>
        </div>
    </div>
    <div class="tab-pane fade" id="tab-business-listing-content" role="tabpanel" aria-labelledby="tab-business-listing">
        <div class="p-15">
            <?= $this->element('Members/services') ?>
        </div>
    </div>
    <div class="tab-pane fade" id="tab-insights-list-content" role="tabpanel" aria-labelledby="tab-insights-list">
        <div class="p-15">
            <?= $this->element('Members/articles') ?>
        </div>
    </div>
    <div class="tab-pane fade" id="tab-alerts-searches-content" role="tabpanel" aria-labelledby="tab-alerts-searches">
        <div class="p-15">
            <?= $this->element('Members/searches', ['propertySearches' => $user->property_searches, 'serviceSearches' => $user->service_searches]) ?>
        </div>
    </div>
    <div class="tab-pane fade" id="tab-other-content" role="tabpanel" aria-labelledby="tab-other">
        <div class="p-15">
            <?= $this->element('Members/other') ?>
        </div>
    </div>
</div>

<?= $this->element('Structure/view-actions', ['id' => $user->id]) ?>

