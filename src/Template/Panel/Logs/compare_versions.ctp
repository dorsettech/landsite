<div class="row">
    <div class="col-md-6">
        <h3>
            CURRENT VERSION
            <?= !empty($currentVersion) && $currentVersion->has('modified') ? '<small>created: '.$this->Transform->date($currentVersion->created).'</small>' : '' ?>
        </h3>
        <?php
        if (!empty($currentVersion)) {
            debug($currentVersion->toArray());
        }
        ?>
    </div>
    <div class="col-md-6">
        <h3>
            OLD VERSION
            <?= !empty($oldVersion) && $oldVersion->has('modified') ? '<small>created: '.$this->Transform->date($oldVersion->created).'</small>' : '' ?>
        </h3>
        <?php
        if (!empty($oldVersion)) {
            debug($oldVersion->toArray());
        }
        ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-6 text-left"><?= $this->Html->link(__('Back'), ['controller' => 'Logs', 'action' => 'show-versions', $model, $oldVersion->id], ['class' => 'btn btn-warning']) ?></div>
    <div class="col-xs-6 text-right"><?= $this->Html->link(__('Revert old version'), ['action' => 'revertVersion', $versionId], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Revert version')]) ?></div>
</div>