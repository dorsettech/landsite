<?php if (!empty($logs->toArray()) && $_user['is_root']) : ?>
    <div class="row">
        <div class="col-12 col-md-6">
            <?= $this->Form->create(null, ['url' => ['controller' => 'Logs', 'action' => 'index'], 'class' => 'form form-inline logs-filter', 'type' => 'GET']); ?>
            <?= $this->Form->control('search', ['type' => 'hidden', 'val' => '1', 'id' => 'search']); ?>
            <div class="input-group-prepend">
                <span class="input-group-text">From:</span>
            </div>
            <?= $this->Form->control('createdFrom', ['type' => 'text', 'label' => false, 'class' => 'picker', 'id' => 'datetimelogs', 'val' => $createdFrom]); ?>
            <div class="input-group-prepend">
                <span class="input-group-text">To:</span>
            </div>
            <?= $this->Form->control('createdTo', ['type' => 'text', 'label' => false, 'class' => 'picker', 'id' => 'createdTo', 'val' => $createdTo]); ?>
            <div class="input-group-append">
                <span class="input-group-btn">
                    <?= $this->Form->submit('Filter', ['class' => 'btn btn-primary']) ?>
                </span>
            </div>
            <?= $this->Form->end() ?>
        </div>
        <div class="col-12 col-md-6">
            <?= $this->Form->postLink(__('Truncate'), ['action' => 'truncate'], ['class' => 'btn btn-danger ml-2 pull-right', 'confirm' => __('Are you sure?'), 'data-text' => __('Are you sure you want to remove all logs? This operation cannot be undone. Please backup your database first before doing that operation.'), 'escape' => false]) ?>
            
            <?= $this->Form->create(null, ['url' => $this->request->here, 'type' => 'GET', 'class' => 'form-inline delete-logs pull-right']) ?>
            <?= $this->Form->control('search', ['type' => 'hidden', 'val' => '1', 'id' => 'filter']); ?>
            <div class="input-group-prepend">
                <span class="input-group-text">From:</span>
            </div>
            <?= $this->Form->control("date_from", ['class' => 'datetime picker', 'label' => false]) ?>
            <div class="input-group-prepend">
                <span class="input-group-text">To:</span>
            </div>
            <?= $this->Form->control("date_to", ['class' => 'picker', 'id' => 'dateTo', 'label' => false]) ?>
            <div class="input-group-append">
                <span class="input-group-btn">
                    <?= $this->Form->button(__('Delete'), ['bootstrap-type' => 'warning', 'style' => 'margin-bottom:0;']); ?>
                </span>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
    <hr>
<?php endif; ?>
<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('created', ['name' => __('Date')]); ?></th>
                <th><?= __('Event'); ?></th>
                <th><?= __('URL'); ?></th>
                <th><?= $this->Paginator->sort('User.first_name', ['name' => __('User details')]); ?></th>
                <th><?= $this->Paginator->sort('User.email', ['name' => __('Email address')]); ?></th>
                <th><?= $this->Paginator->sort('host'); ?></th>
                <th><?= $this->Paginator->sort('ip', ['name' => __('IP')]); ?></th>
                <th style="width:450px;"><?= __('User Agent'); ?></th>
                <?php if ($_user['is_root'] === true) : ?>
                    <th class="actions"><?= __('Actions') ?></th>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($logs as $log) : ?>
                <tr class="odd gradeX">
                    <td><?= $this->Html->badge('<i class="far fa-clock"></i> '.h($this->Transform->date($log->created)), 'light') ?></td>
                    <td><?= $log->content ?></td>
                    <td><?= $log->url ?></td>
                    <td><?= $log->user->full_name ?></td>
                    <td><?= ($_user['is_root']) ? $this->Html->link($log->user->email, ['controller' => 'Logs', 'action' => 'viewUser', $log->user_id]) : $log->user->email ?></td>
                    <td><?= $log->host ?></td>
                    <td><?= $log->ip ?></td>
                    <td><?= $log->agent ?></td>
                    <?php if ($_user['is_root'] === true) : ?>
                        <td class="actions" style="min-width: 85px;">
                            <div class="btn-group btn-group-xs">
                                <?php if (!in_array($log->url, $urls)): ?>
                                    <?= $this->Html->link('<i class="fa fa-eye"></i>', ['action' => 'showVersions', $log->model, $log->foreign_key], ['class' => 'btn btn-default', 'escape' => false, 'title' => __('View details'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                                <?php endif; ?>
                            </div>
                        </td>
                    <?php endif; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?= $this->element('pagination') ?>