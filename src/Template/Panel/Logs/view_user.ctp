<?php if (iterator_count($logs)): ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="input-group">
                    <?= $this->Form->create(null, ['url' => ['controller' => 'Logs', 'action' => 'index'], 'class' => 'form form-inline logs-filter', 'type' => 'GET']); ?>
                    <?= $this->Form->control('search', ['type' => 'hidden', 'val' => '1']); ?>
                    <div class="input-group-prepend">
                        <span class="input-group-text">From:</span>
                    </div>
                    <?= $this->Form->control('createdFrom', ['type' => 'text', 'label' => false, 'class' => 'picker', 'id' => 'datetimelogs']); ?>
                    <div class="input-group-prepend">
                        <span class="input-group-text">To:</span>
                    </div>
                    <?= $this->Form->control('createdTo', ['type' => 'text', 'label' => false, 'class' => 'picker', 'id' => 'createdTo']); ?>
                    <div class="input-group-append">
                        <span class="input-group-btn">
                            <?= $this->Form->submit('Filter', ['class' => 'btn btn-primary']) ?>
                        </span>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="table-responsive">
        <table id="data-table" cellpadding="0" cellspacing="0" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th><?= __('User Name'); ?></th>
                    <th><?= $this->Paginator->sort('user_host', 'User Host'); ?></th>
                    <th><?= $this->Paginator->sort('user_ip', 'User Ip'); ?></th>
                    <th style="width:450px;"><?= __('User Agent'); ?></th>
                    <th><?= $this->Paginator->sort('created', 'Date'); ?></th>
                    <th><?= __('Event'); ?></th>
                    <th><?= __('Url'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($logs as $log) : ?>
                    <tr class="odd gradeX">
                        <td><?= $log->user->first_name.' '.$log->user->last_name ?>
                            <br>
                            <small><?= $log->user->email ?></small>
                        </td>
                        <td><?= $log->host ?></td>
                        <td><?= $log->ip ?></td>
                        <td><?= $log->agent ?></td>
                        <td><?= $this->Html->badge('<i class="far fa-clock"></i> '.h($this->Transform->date($log->created)), 'light') ?></td>
                        <td><?= $log->content ?></td>
                        <td><?= $log->url ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?= $this->element('pagination') ?>
<?php else : ?>
    <h3 class="text-center"><?= __('This user has no logs to show.') ?></h3>
<?php endif; ?>
<hr>
<div class="row">
    <div class="col-xs-6 text-left"><?= $this->Html->link('Back', ['action' => 'index'], ['class' => 'btn btn-warning']) ?></div>
    <div class="col-xs-6 text-right"></div>
</div>



