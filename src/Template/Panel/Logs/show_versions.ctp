<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('content') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($versions as $version): ?>
                <tr>
                    <td><?= $this->Number->format($version->id) ?></td>
                    <td><?= h($version->content) ?></td>
                    <td><?= $this->Html->badge('<i class="far fa-clock"></i> '.h($this->Transform->date($version->created)), 'light') ?></td>
                    <td class="actions" style="min-width: 85px;">
                        <div class="btn-group btn-group-xs">
                            <?= $this->Html->link('<i class="fa fa-arrows-alt-h"></i>', ['action' => 'compareVersions', $version->id], ['class' => 'btn btn-default', 'escape' => false, 'title' => __('Compare versions'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            <?= $this->Html->link('<i class="fa fa-history"></i>', ['action' => 'revertVersion', $version->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Revert version'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?= $this->element('pagination') ?>
<div class="row">
    <div class="col-xs-6 text-left"><?= $this->Html->link('Back', ['action' => 'index'], ['class' => 'btn btn-warning']) ?></div>
    <div class="col-xs-6 text-right"></div>
</div>
