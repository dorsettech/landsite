<?= $this->Form->create($staticContent, ['type' => 'get']) ?>
<div class="row">
    <div class="col-12">
        <?= $this->Form->control('type', ['type' => 'select', 'options' => $staticContent->types, 'empty' => __('Select type'), 'label' => __('Type'), 'id' => 'staticContentsSelect', 'onchange' => 'this.form.submit()']) ?>
    </div>
</div>
<?= $this->Form->end() ?>
<?= $this->Form->create($staticContent, ['type' => 'file']) ?>
<?= $this->element('Structure/action-buttons', ['cancelUrl' => ['controller' => 'StaticContentGroups', 'action' => 'view', $staticContentGroup['id']]]) ?>
<div class="row">
    <div class="col-12"><?= $this->Form->control('var_name', ['type' => 'text']); ?></div>
    <div class="col-12"><?= $this->Form->control('description', ['type' => 'textarea']); ?></div>
    <?php
    switch ($type):
        case $staticContent::TYPE_TEXT:
            ?>
            <div class="col-12"><?= $this->Form->control('value', ['type' => 'text']); ?></div>
            <?php
            break;
        case $staticContent::TYPE_NUMBER:
            ?>
            <div class="col-12"><?= $this->Form->control('value', ['type' => 'number']); ?></div>
            <?php
            break;
        case $staticContent::TYPE_EMAIL:
            ?>
            <div class="col-12"><?= $this->Form->control('value', ['type' => 'email']); ?></div>
            <?php
            break;
        case $staticContent::TYPE_TEXTAREA:
            ?>
            <div class="col-12 "><?= $this->Form->control('value', ['type' => 'textarea']); ?></div>
            <?php
            break;
        case $staticContent::TYPE_CKEDITOR:
            ?>
            <div class="col-12"><?= $this->Form->control('value', ['type' => 'textarea', 'rows' => 10, 'class' => 'ckeditor']); ?></div>
            <?php
            break;
        case $staticContent::TYPE_CODEEITOR:
            ?>
            <div class="col-12"><?= $this->Form->control('value', ['type' => 'textarea', 'class' => 'code-mirror', 'required' => false]); ?></div>
            <?php
            break;
        case $staticContent::TYPE_CHECKBOX:
            ?>
            <div class="col-12">
                <?= $this->Form->control('value', ['type' => 'checkbox', 'label' => __('Active'), 'required' => false]) ?>
            </div>
            <?php
            break;
        case $staticContent::TYPE_FILE:
            ?>
            <div class="col-12"><?= $this->Form->control('value', ['type' => 'file', 'required' => false, 'label' => false]); ?></div>
            <?php
            break;
    endswitch;
    ?>
    <?= $this->Form->control('type', ['type' => 'hidden', 'value' => $type]); ?>
</div>
<?= $this->element('Structure/action-buttons', ['cancelUrl' => ['controller' => 'StaticContentGroups', 'action' => 'view', $staticContentGroup['id']]]) ?>
<?= $this->Form->end() ?>
<?php
$this->Html->css('/plugins/codemirror/codemirror.css', ['block' => 'cssHead']);
$this->Html->script('/plugins/codemirror/codemirror.js', ['block' => 'scriptBottom']);
$this->Html->script('/plugins/codemirror/mode/css/css.js', ['block' => 'scriptBottom']);
?>