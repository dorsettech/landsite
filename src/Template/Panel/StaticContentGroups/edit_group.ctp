<?= $this->Form->create($group, ['type' => 'file']) ?>
<?= $this->element('Structure/action-buttons', ['cancelUrl' => ['controller' => 'StaticContentGroups', 'action' => 'view', $group->id]]) ?>
<?php foreach ($group->static_contents as $content): ?>
    <?php
    switch ($content->type):
        case $content::TYPE_TEXT:
            ?>
            <?= $this->Form->control("content[$content->id][value]", ['type' => 'text', 'label' => $content->description. ' ('.$content->var_name.')', 'value' => $content['value']]); ?>
            <?php
            break;
        case $content::TYPE_NUMBER:
            ?>
            <?= $this->Form->control("content[$content->id][value]", ['type' => 'number', 'label' => $content->description. ' ('.$content->var_name.')', 'value' => $content['value']]); ?>
            <?php
            break;
        case $content::TYPE_EMAIL:
            ?>
            <?= $this->Form->control("content[$content->id][value]", ['type' => 'email', 'label' => $content->description. ' ('.$content->var_name.')', 'value' => $content['value']]); ?>
            <?php
            break;
        case $content::TYPE_TEXTAREA:
            ?>
            <?= $this->Form->control("content[$content->id][value]", ['type' => 'textarea', 'label' => $content->description. ' ('.$content->var_name.')', 'value' => $content['value']]); ?>
            <?php
            break;
        case $content::TYPE_CKEDITOR:
            ?>
            <?= $this->Form->control("content[$content->id][value]", ['type' => 'textarea', 'rows' => 10, 'class' => 'ckeditor', 'label' => $content->description. ' ('.$content->var_name.')', 'value' => $content['value']]); ?>

            <?php
            break;
        case $content::TYPE_CODEEITOR:
            ?>
            <?= $this->Form->control("content[$content->id][value]", ['type' => 'textarea', 'label' => $content->description. ' ('.$content->var_name.')', 'id' => 'codeMirror', 'required' => false, 'value' => $content['value']]);
            ?>

            <?php
            break;
        case $content::TYPE_CHECKBOX:
            ?>
            <div class="form-group">
                <?= $this->Form->label("content[$content->id][value]", ['name' => $content->description. ' ('.$content->var_name.')']); ?>
                <?= $this->Form->checkbox("content[$content->id][value]", ['required' => false, 'checked' => $content->value]); ?>
            </div>
            <?php
            break;
        case $content::TYPE_FILE:
            ?>
            <div class="form-group">
                <?= $this->Form->control("content[$content->id][value]", ['type' => 'file', 'required' => false, 'label' => $content->description. ' ('.$content->var_name.')']); ?>
                <?php
                if (!empty($content['value'])) {
                    echo $this->Html->link(__('Preview {0}', $content->description), $content['value'], ['target' => '_blank', 'class' => 'btn btn-success']);
                }
                ?>
            </div>
            <?php
            break;
    endswitch;
    ?>
<?php endforeach; ?>
<?= $this->element('Structure/action-buttons', ['cancelUrl' => ['controller' => 'StaticContentGroups', 'action' => 'view', $group->id]]) ?>
<?= $this->Form->end() ?>
