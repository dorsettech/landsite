<?php

use App\Model\Entity\StaticContent;

?>
<div class="row action-buttons">
    <div class="col-12 col-sm-6 text-left">
        <div class="btn-group">
            <?= $this->Html->link(__('Edit Group Name'), ['controller' => 'StaticContentGroups', 'action' => 'edit', $group->id], ['class' => 'btn btn-outline-dark']) ?>
            <?= $this->Form->postLink(__('Delete Group'), ['controller' => 'StaticContentGroups', 'action' => 'delete', $group->id], ['class' => 'btn btn-outline-'
                . 'danger', 'confirm' => __('Are you sure you want to remove {0} group?', h($group->name)), 'escape' => false, 'title' => __('Delete configuration group'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
        </div>
    </div>
    <div class="col-12 col-sm-6 text-right">
        <div class="btn-group">
            <?= $this->Html->link(__('Add Configuration Variable'), ['controller' => 'StaticContents', 'action' => 'add', $group->id], ['class' => 'btn btn-info']) ?>
            <?= $this->Html->link(__('Edit All Variables'), ['controller' => 'StaticContentGroups', 'action' => 'editGroup', $group->id], ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
</div>
<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
        <thead>
            <tr>
                <th><?= __('Name') ?></th>
                <th><?= __('Value') ?></th>
                <th><?= __('Description') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($group->static_contents as $content): ?>
                <tr>
                    <td><?= $this->Html->link(h($content['var_name']), ['controller' => 'StaticContents', 'action' => 'edit', $content['id']]); ?></td>
                    <?php if ($content['type'] === StaticContent::TYPE_CHECKBOX): ?>
                        <td><?= $this->Html->link(($content['value'] ? 'active' : 'inactive'), ['controller' => 'StaticContents', 'action' => 'edit', $content['id']]); ?></td>
                    <?php else: ?>
                        <td><?= $this->Html->link($this->Text->truncate(h($content['value']), 50), ['controller' => 'StaticContents', 'action' => 'edit', $content['id']]); ?></td>
                    <?php endif; ?>
                    <td><?= $this->Html->link(h($content['description']), ['controller' => 'StaticContents', 'action' => 'edit', $content['id']], ['escape' => false]) ?></td>
                    <td class="actions">
                        <div class="btn-group btn-group-xs">
                            <?=
                            $this->Html->link('<i class="fa fa-edit"></i>', ['controller' => 'StaticContents', 'action' => 'edit', $content['id']], ['class' => 'btn btn-default', 'escape' => false, 'title' => __('Edit'), 'data-toggle' => 'tooltip', 'data-placement' => 'top'])
                            ?>
                            <?php if ($_user->is_root) : ?>
                                <?=
                                $this->Form->postLink('<i class="fa fa-times"></i>', ['controller' => 'StaticContents', 'action' => 'delete', $content['id']], ['class' => 'btn btn-danger', 'confirm' => __('Are you sure you want to remove {0} content?', h($content['description'])), 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top'])
                                ?>
                            <?php endif; ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="row action-buttons">
    <div class="col-12 col-sm-6 text-left">
        <div class="btn-group">
            <?= $this->Html->link(__('Edit Group Name'), ['controller' => 'StaticContentGroups', 'action' => 'edit', $group->id], ['class' => 'btn btn-outline-dark']) ?>
            <?= $this->Form->postLink(__('Delete Group'), ['controller' => 'StaticContentGroups', 'action' => 'delete', $group->id], ['class' => 'btn btn-outline-'
                . 'danger', 'confirm' => __('Are you sure you want to remove {0} group?', h($group->name)), 'escape' => false, 'title' => __('Delete configuration group'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
        </div>
    </div>
    <div class="col-12 col-sm-6 text-right">
        <div class="btn-group">
            <?= $this->Html->link(__('Add Configuration Variable'), ['controller' => 'StaticContents', 'action' => 'add', $group->id], ['class' => 'btn btn-info']) ?>
            <?= $this->Html->link(__('Edit All Variables'), ['controller' => 'StaticContentGroups', 'action' => 'editGroup', $group->id], ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
</div>