<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('name', ['name' => 'Group name']) ?></th>
                <?php if ($_user->is_root) : ?>
                    <th class="actions"><?= 'Actions' ?></th>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($staticContentGroups as $group): ?>
                <tr>
                    <td><?= $this->Html->link(h($group->name), ['action' => 'view', $group->id], ['escape' => false]) ?></td>
                    <?php if ($_user->is_root) : ?>
                        <td class="actions">
                            <div class="btn-group btn-group-xs">
                                <?= $this->Html->link('<i class="fa fa-list"></i>', ['action' => 'view', $group->id], ['class' => 'btn btn-default', 'escape' => false, 'title' => __('View configuration variables'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                                <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $group->id], ['class' => 'btn btn-default', 'escape' => false, 'title' => __('Edit configuration group'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                                <?=
                                $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $group->id], ['class' => 'btn btn-danger', 'confirm' => 'Please confirm that You want to remove ' . h($group->name) . ' group?', 'escape' => false, 'title' => __('Delete configuration group'), 'data-toggle' => 'tooltip', 'data-placement' => 'top'])
                                ?>
                            </div>
                        </td>
                    <?php endif; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?= $this->element('Structure/add-button') ?>
<?= $this->element('pagination') ?>