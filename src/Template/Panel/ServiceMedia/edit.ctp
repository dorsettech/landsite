<?php

use App\Model\Enum\MediaType;

?>
<?= $this->Form->create($serviceMedia) ?>
<?= $this->element('Structure/action-buttons', ['cancelUrl' => $heading['link_close']]) ?>
<hr>
<div class="row">
    <div class="col-sm-6 col-lg-2"><?= $this->Form->control('position'); ?></div>
    <div class="col-sm-6 col-lg-10 text-right">
        <?php if ($serviceMedia->type == MediaType::IMAGE) : ?>
        <?= $this->Html->link($this->Html->image('/' . $this->Images->scale($serviceMedia->source_path, 100, 100, 'ratio'), ['alt' => $serviceMedia->service->company, 'class' => 'img-thumbnail']), '/' . $this->Images->scale($serviceMedia->source_path, 1280, 1280, 'ratio'), ['class' => 'magnific-image', 'escape' => false]) ?>
        <?php elseif ($serviceMedia->type == MediaType::VIDEO) : ?>
        <i class="fab fa-youtube fa-3x"></i>
        <?php endif; ?>
    </div>
    <div class="col-sm-12"><?= $this->Form->control('extra'); ?></div>
</div>
<hr><?= $this->element('Structure/action-buttons', ['cancelUrl' => $heading['link_close']]) ?>
<?= $this->Form->end() ?>

