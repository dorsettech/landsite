<?= $this->Form->create($group) ?>
<?= $this->element('Structure/action-buttons') ?>
<hr>
<div class="row">
    <div class="col-sm-6"><?= $this->Form->control('parent_id', ['type' => 'select', 'label' => __('Parent group'), 'options' => $groups, 'empty' => 'none']); ?></div>
    <div class="col-sm-6"><?= $this->Form->control('name', ['label' => 'Name']); ?></div>
</div>
<hr>
<?= $this->element('Structure/action-buttons') ?>
<?= $this->Form->end() ?>
