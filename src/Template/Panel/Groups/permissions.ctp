<?= $this->Form->create($group) ?>
<?= $this->element('Structure/acl-action-buttons') ?>
<hr>
<div class="row">
    <?php foreach ($resources as $namespace => $controllers): ?>
        <?php foreach ($controllers as $key => $controller) : ?>
            <div class="col-12 col-md-4 col-lg-3 align-items-stretch pb-4">
                <div class="panel panel-inverse item h-100">
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= $key ?> <span class="pull-right text-muted"><?= $namespace ?></span></h4>
                    </div>
                    <div class="panel-body">
                        <div class="alert alert-info">
                            <?= $this->ACL->controllerDescription($namespace, $key); ?>
                        </div>
                        <?php foreach ($controller as $action): ?>
                            <?= @$this->Form->control($key . '.' . $action, ['type' => 'checkbox', 'class' => 'acl-check acl-check-' . $action, 'data-related' => $this->ACL->actionRelated($namespace, $key, $action), 'label' => '<b>' . $this->ACL->actionDescription($namespace, $key, $action) . '</b> <small>(' . $action . ')</small>', 'escape' => false, (isset($group->permissions[$key][$action]) && $group->permissions[$key][$action]) ? 'checked' : null]); ?>
                        <?php endforeach; ?>
                        <hr>
                        <?= $this->Form->control('checkAll' . $key, ['type' => 'checkbox', 'label' => __('Toggle All'), 'class' => 'acl-toggle-module', 'hiddenField' => false]); ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endforeach; ?>
</div>
<hr>
<?= $this->element('Structure/acl-action-buttons') ?>
<?= $this->Form->end() ?>
