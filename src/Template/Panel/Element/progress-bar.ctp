<div class="progress progress-session">
    <div id="session-time-bar" aria-valuemax="100" aria-valuemin="0" role="regressbar" class="progress-bar progress-bar-info">
        <span class="sr-only"></span>
    </div>
</div>
