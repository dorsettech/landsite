<div id="lock-screen" class="lock-screen vis invisible">
    <div class="lock-screen-header" style="background-image: url(/panel/img/login-bg/login-bg-1.jpg)">
        <div class="bg-cover"></div>
        <div class="title">
            <?= __('Screen') ?> <b><?= __('Locked') ?></b>
        </div>
        <div class="desc">
            <p><?= __('Your screen has been locked because of long inactivity.') ?><br>
            <?= __('No worries &mdash; <strong class="text-white">your data is safe</strong> &mdash; you just have to unlock this screen.') ?></p>
            <p><?= __('Please note you will lost all unsaved data if you will refresh this screen without unlocking it.') ?></p>
        </div>
    </div>
    <div class="lock-screen-content">
        <div class="desc">
            <h3><?= __('Unlock Screen') ?></h3>
            <?= __('Please enter your password to unlock it and get back to where you were before.') ?>
        </div>
        <?= $this->Form->create(null, ['class' => 'form locked-screen-form', 'id' => 'locked-screen-form']); ?>
        <?= $this->Form->input('password', ['type' => 'password', 'label' => false, 'id' => 'locked-screen-password', 'append' => [
                $this->Form->button('Unlock', ['type' => 'submit', 'btype' => 'info'])
        ]]) ?>
        <?= $this->Form->end(); ?>
    </div>
</div>
<?= $this->element('progress-bar') ?>
