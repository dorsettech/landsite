<?php

use App\Library\PageFiles;

$cropActive = true;
switch ($type) {
    case PageFiles::TYPE_ID_SLIDER:
        $title = 'Slider';
        break;
    case PageFiles::TYPE_ID_GALLERY:
        $title = 'Gallery';
        break;
    case PageFiles::TYPE_ID_DOCUMENT:
        $title = 'Documents';
        $cropActive = false;
        break;
}

?>
<?= $this->Form->create(null, ['url' => ['prefix' => $_admin_prefix, 'controller' => 'PageFiles', 'action' => 'addFilesToPage', $pageId, $type], 'type' => 'file', 'class' => 'form-article']) ?>
<div><?= $this->Form->control('images[]', ['value' => '', 'multiple' => 'true', 'type' => 'file', 'label' => 'Add files to ' . $title, 'button-label' => __('Select file(s)'), 'id' => "image_$type"]); ?></div>
<div class="text-right"><?= $this->Form->button('Upload images', ['bootstrap-type' => 'primary', 'bootstrap-size' => 'sm']); ?></div>
<?= $this->Form->end() ?>
<hr>
<?php if ($files && $files->count()): ?>
    <h4><?= __('Manage') ?> <?= $title ?></h4>
    <div class="row row-space-2 page-files<?= ($type < PageFiles::TYPE_ID_DOCUMENT) ? ' magnific-gallery' : '' ?>" data-page-id="<?= $pageId ?>">
        <?php foreach ($files as $file): ?>
            <div class="col-6 col-lg-2 widget-card-box" data-id="<?= $file->id ?>" id="file-<?= $file->id ?>">
                <?php if ($file->type === PageFiles::TYPE_ID_DOCUMENT): ?>
                    <a href="<?= $file->path ?>" class="widget-card widget-card-rounded square m-b-2" target="_blank">
                        <div class="widget-card-cover icon">
                            <div class="vertical-box vertical-box-widget">
                                <div class="vertical-box-column valign-middle text-center">
                                    <div><i class="fas fa-3x fa-file"></i></div>
                                    <div class="file-name"><?= $file->name ?></div>
                                </div>
                            </div>
                        </div>
                    </a>
                <?php else: ?>
                    <a href="<?= $this->Images->scale($file->path, 900, 900, 'ratio') ?>" class="widget-card widget-card-rounded square m-b-2">
                        <div class="widget-card-cover" style="background-image: url(<?= $this->Images->scale($file->path, 500, 340, 'ratio') ?>)"></div>
                    </a>
                <?php endif; ?>
                <div class="widget-card-options">
                    <div class="btn-group btn-group-xs">
                        <?= $this->Form->button('<i class="fa fa-' . ($file->active ? 'check' : 'times') . '"></i>', ['type' => false, 'class' => 'btn btn-default status', 'data-id' => $file->id, 'title' => ($file->active ? __('Deactivate') : __('Activate')), 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'escape' => false]) ?>
                        <?= $cropActive ? $this->Html->link('<i class="fa fa-crop"></i>', ['controller' => 'Cropper', 'action' => 'crop', 'PageFiles', 'path', $file->id], ['class' => 'btn btn-default', 'title' => __('Crop'), 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'escape' => false]) : '' ?>
                        <?= $this->Html->link('<i class="fa fa-edit"></i>', ['controller' => 'PageFiles', 'action' => 'edit', $file->id], ['class' => 'btn btn-primary', 'title' => __('Edit'), 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'escape' => false]) ?>
                        <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['controller' => 'PageFiles', 'action' => 'delete', $file->id], ['class' => 'btn btn-danger', 'confirm' => __('Please confirm file removal `{0}`', h($file->name)), 'title' => __('Remove'), 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'escape' => false]) ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php else: ?>
    <p class="text-center"><?= __('There are no files for {0} yet.', $title) ?></p>
<?php endif; ?>
