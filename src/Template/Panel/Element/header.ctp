<div id="header" class="header navbar-default<?= !empty($_filter_keyword) ? ' header-search-toggled' : '' ?>">

    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed navbar-toggle-left" data-click="sidebar-minify">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <?= $this->Html->link($_service, ['plugin' => null, 'prefix' => $_admin_prefix, 'controller' => $_admin_panel, 'action' => 'index'], ['class' => 'navbar-brand']) ?>
    </div>

    <ul class="navbar-nav navbar-right">
        <?php if ($_removeSearch == false && ($this->request->getParam('action') === 'index' || $this->request->getParam('action') === 'view')): ?><li><?= $this->Html->link('<i class="material-icons">search</i>', $this->request->getRequestTarget() . '#search', ['class' => 'icon', 'data-toggle' => 'navbar-search', 'escape' => false]) ?></li><?php endif; ?>
        <li class="dropdown">
            <?= $this->Html->link('<i class="material-icons">inbox</i><span class="label">' . $unreadMessages->count() . '</span>', $this->request->getRequestTarget() . '#search', ['class' => 'dropdown-toggle icon', 'data-toggle' => 'dropdown', 'escape' => false]) ?>
            <ul class="dropdown-menu media-list dropdown-menu-right">
                <li class="dropdown-header"><?= __('Messages ({0})', $unreadMessages->count()) ?></li>
                <?php if ($unreadMessages->count() > 0) : ?>
                    <?php foreach ($unreadMessages as $message) : ?>
                        <li class="media">
                            <a href="<?= $this->Url->build(['plugin' => null, 'prefix' => $_admin_prefix, 'controller' => 'FormsData', 'action' => 'view', $message->id]) ?>">
                                <div class="media-body">
                                    <p><?= __('Message #{0} send from {1}', [$message->id, $message->form->name]) ?></p>
                                    <div class="text-muted text-right"><?= $this->Html->badge('<i class="far fa-clock"></i> ' . $this->Transform->date($message->created), 'light') ?></div>
                                </div>
                            </a>
                        </li>
                    <?php endforeach; ?>
                <?php else : ?>
                    <li class="text-center width-300 p-b-10">
                        <?= __('There are no new messages yet.') ?>
                    </li>
                <?php endif; ?>
                <li class="dropdown-footer text-center">
                    <?= $this->Html->link(__('View Forms'), ['plugin' => null, 'prefix' => $_admin_prefix, 'controller' => 'Forms', 'action' => 'index']) ?>
                </li>
            </ul>
        </li>
        <li class="dropdown navbar-user">
            <?= $this->Html->link('<span class="d-none d-md-inline">' . __('Hi, {0}', $_user->first_name) . ' </span>' . $this->Html->image('/' . $this->Images->scale($_user->profile_image, 70, 70, 'ratio_crop'), ['alt' => $_user->full_name]) . '</span>', $this->request->getRequestTarget() . '#search', ['class' => 'dropdown-toggle', 'data-toggle' => 'dropdown', 'escape' => false]) ?>
            <div class="dropdown-menu dropdown-menu-right">
                <?= $this->Html->link(__('Edit Profile'), ['plugin' => false, 'prefix' => $_admin_prefix, 'controller' => 'Users', 'action' => 'edit'], ['class' => 'dropdown-item']) ?>
                <?= $this->Html->link(__('Messages') . $this->Html->badge($unreadMessages->count(), 'danger', ['class' => 'pull-right']), ['plugin' => false, 'prefix' => $_admin_prefix, 'controller' => 'Forms', 'action' => 'index'], ['class' => 'dropdown-item', 'escape' => false]) ?>
                <div class="dropdown-divider"></div>
                <?= $this->Html->link('Log out', ['plugin' => false, 'prefix' => $_admin_prefix, 'controller' => 'Users', 'action' => 'logout'], ['class' => 'dropdown-item']) ?>
            </div>
        </li>
    </ul>

    <?php if ($_removeSearch == false && ($this->request->getParam('action') === 'index' || $this->request->getParam('action') === 'view')): ?>
        <div class="search-form">
            <?= $this->Form->create(null, ['type' => 'get']) ?>
            <button class="search-btn" type="submit"><i class="material-icons">search</i></button>
            <?= $this->Form->control('filter_keyword', ['placeholder' => __('Search for...'), 'value' => $this->request->getQuery('filter_keyword')]) ?>
            <?= $this->Html->link('<i class="material-icons">close</i>', $this->request->getRequestTarget() . '#close-search', ['class' => 'close', 'data-dismiss' => 'navbar-search', 'escape' => false]) ?>
            <?= $this->Form->end() ?>
        </div>
    <?php endif; ?>
</div>

<?php /**
 * Left if needed
 *
  <?php if ($this->request->session()->check('lang') && count($_languages) > 1) : ?>
  <li dropdown="" class="dropdown">
  <a aria-expanded="false" aria-haspopup="true" data-toggle="dropdown" id="dropdownLang" class="dropdown-toggle">
  <i class="fa fa-language"></i>
  <span><?= $this->request->session()->read('lang') ?></span>
  <span class="caret"></span>
  </a>
  <ul aria-labelledby="dropdownLang" class="dropdown-menu fadeInDown m-t-xs">
  <?php foreach ($_languages as $_lang): ?>
  <li <?php if ($_lang->locale == $this->request->session()->read('lang')) echo 'class="active"'; ?>>
  <a href="/<?= $_admin_prefix ?>/languages/setLanguage/<?= $_lang->locale ?>"><?= $_lang->name ?></a>
  </li>
  <!--<li class="divider"></li>-->
  <?php endforeach; ?>
  </ul>
  </li>
  <?php endif; ?>
 *
 */ ?>
