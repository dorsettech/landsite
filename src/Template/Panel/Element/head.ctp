<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <?= $this->Html->charset() ?>
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <base href="<?= $this->Url->build('/', true); ?>">
        <title><?= $meta['title'] ?></title>
        <?= $this->Html->meta('favicon.ico', '/favicon.ico', ['type' => 'icon']) ?>
        <?= $this->Html->css('https://fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic,900,900italic'); ?>
        <?= $this->Html->css('https://fonts.googleapis.com/icon?family=Material+Icons'); ?>
        <?= $this->Html->css('/plugins/jquery-ui/jquery-ui.min.css'); ?>
        <?= $this->Html->css('/plugins/font-awesome/5.3/css/all.min.css'); ?>
        <?= $this->Html->css('/plugins/animate/animate.min.css'); ?>
        <?= $this->Html->css('/plugins/noty/noty.css'); ?>
        <?= $this->Html->css('/plugins/select2/dist/css/select2.min.css'); ?>
        <?= $this->Html->css('/plugins/noty/themes/metroui.css'); ?>
        <?= $this->Html->css('/plugins/bootstrap-eonasdan-datetimepicker/build/css/bootstrap-datetimepicker.min.css'); ?>
        <?= $this->Html->css('/panel/css/bootstrap.min.css'); ?>
        <?= $this->Html->css('/plugins/magnific-popup/magnific-popup.css', ['block' => 'cssHead']); ?>
        <?= $this->Html->css('/panel/css/app.min.css'); ?>
        <?= $this->fetch('cssHead') ?>
        <?= $this->Html->script('/plugins/pace/pace.min.js'); ?>
        <?= $this->fetch('scriptHead') ?>
        <script type="text/javascript">
            var sessionlifetime = <?= $_session_lifetime ?>;
            var userEmail = "<?= $_user['email'] ?? '' ?>";
            var adminPrefix = "<?= $_admin_prefix ?>";
        </script>
    </head>
