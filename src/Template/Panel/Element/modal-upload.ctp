<div class="modal inmodal" id="modal-upload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <?= $this->Form->create($file, ['type' => 'file']); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= __('Close') ?></span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title"><?= __('Add file') ?></h4>
                <small><?= __('Use form below to upload file into server.') ?></small>
            </div>
            <div class="modal-body">
                <?= $this->Flash->render(); ?>
                <?= $this->Form->control('file.', ['multiple' => true, 'type' => 'file', 'label' => false, 'button-label' => __('Select file')]); ?>
                <?= $this->Form->control('file_alt', ['label' => __('File description')]); ?>
                <?= $this->Form->control('file_title', ['label' => __('File title')]); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal"><?= __('Close') ?></button>
                <?= $this->Form->button(__('Save'), ['bootstrap-type' => 'primary']); ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
