<div class="row action-buttons">
    <div class="col text-right">
        <?= $this->Html->link(__('Add {0}', $heading['title_singular']), ((!empty($addUrl)) ? $addUrl : ['action' => 'add']), ['class' => 'btn btn-primary']); ?>
    </div>
</div>
