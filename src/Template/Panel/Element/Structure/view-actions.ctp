<div class="row action-buttons">
    <div class="col-3 text-left"><?= $this->Html->link(__('Back to List'), ((!empty($cancelUrl)) ? $cancelUrl : ['action' => 'index']), ['class' => 'btn btn-default']) ?></div>
    <?php if (!isset($disableEdit) || $disableEdit == 0) : ?><div class="col-9 text-right"><?= $this->Html->link(__('Edit'), ((!empty($editUrl)) ? $editUrl : ['action' => 'edit', $id]), ['class' => 'btn btn-primary']) ?></div><?php endif; ?>
</div>