<div class="row action-buttons">
    <div class="col-3 text-left"><?= $this->Html->link(__('Cancel'), ((!empty($cancelUrl)) ? $cancelUrl : ['action' => 'index']), ['class' => 'btn btn-warning']) ?></div>
    <div class="col-9 text-right">
        <?php if (isset($enableClose) && $enableClose) : ?><?= $this->Form->button(__('Save and close'), ['btype' => 'info', 'name' => 'close', 'value' => true]) ?><?php endif; ?>
        <?= $this->Form->button(__('Save'), ['btype' => 'primary']); ?>
    </div>
</div>