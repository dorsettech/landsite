<div class="row action-buttons">
    <div class="col-3 col-xl-2 text-left"><?= $this->Html->link(__('Cancel'), ((!empty($cancelUrl)) ? $cancelUrl : ['action' => 'index']), ['class' => 'btn btn-warning']) ?></div>
    <div class="col-6 col-xl-8 text-center">
        <div class="btn-group">
            <?= $this->Form->button(__('Toggle'), ['type' => 'button', 'btype' => 'light', 'disabled' => true]) ?>
            <?= $this->Form->button(__('All'), ['type' => 'button', 'btype' => 'light', 'class' => 'acl-toggle-all', 'data-value' => true]) ?>
            <?= $this->Form->button(__('Lists'), ['type' => 'button', 'btype' => 'light', 'class' => 'acl-toggle-index', 'data-value' => true]) ?>
            <?= $this->Form->button(__('Create'), ['type' => 'button', 'btype' => 'light', 'class' => 'acl-toggle-add', 'data-value' => true]) ?>
            <?= $this->Form->button(__('Update'), ['type' => 'button', 'btype' => 'light', 'class' => 'acl-toggle-edit', 'data-value' => true]) ?>
            <?= $this->Form->button(__('Delete'), ['type' => 'button', 'btype' => 'light', 'class' => 'acl-toggle-delete', 'data-value' => true]) ?>
        </div>
    </div>
    <div class="col-3 col-xl-2 text-right">
        <?php if (isset($enableClose) && $enableClose) : ?><?= $this->Form->button(__('Save and close'), ['btype' => 'info', 'name' => 'close', 'value' => true]) ?><?php endif; ?>
        <?= $this->Form->button(__('Save'), ['btype' => 'primary']); ?>
    </div>
</div>