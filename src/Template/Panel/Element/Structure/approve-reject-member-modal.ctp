<div class="modal fade" id="approve-reject-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?= __('Approve/Reject member') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= $this->Form->create(null, ['url' => ['controller' => 'Members', 'action' => 'approveOrReject']]); ?>
            <div class="modal-body">
                <?= $this->Form->control('id', ['type' => 'hidden', 'class' => 'user-id']); ?>
                <?= $this->Form->radio('state', [['value' => 'APPROVED', 'text' => 'APPROVED'], ['value' => 'REJECTED', 'text' => 'REJECTED']],['required'=>true]); ?>
                <div class="d-none reject-reason">
                    <hr>
                    <?= $this->Form->control('reject_reason') ?>
                </div>
            </div>
            <div class="modal-footer">
                <?= $this->Form->button('Save', ['type' => 'submit']) ?>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>