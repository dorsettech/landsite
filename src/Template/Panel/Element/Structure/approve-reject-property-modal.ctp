<?php

use App\Model\Enum\State;

?>
<div class="modal fade" id="approve-reject-property-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?= __('Approve/Reject property') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= $this->Form->create(null, ['url' => ['controller' => $controller, 'action' => 'approveOrReject']]); ?>
            <div class="modal-body">
                <?= $this->Form->control('id', ['type' => 'hidden', 'class' => 'property-id']); ?>
                <?= $this->Form->radio('state', [['value' => State::APPROVED, 'text' => State::APPROVED], ['value' => State::REJECTED, 'text' => State::REJECTED]]); ?>
                <div class="d-none reject-reason">
                    <hr>
                    <?= $this->Form->control('rejection_reason') ?>
                </div>
            </div>
            <div class="modal-footer">
                <?= $this->Form->button('Save', ['type' => 'submit']) ?>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
