<div class="row wrapper border-bottom white-bg media">
    <div class="col-sm-4">
        <h2>Files library</h2>
        <?= $this->element('breadcrumb'); ?>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
        </div>
    </div>
</div>
<div class="wrapper wrapper-content media">
    <div class="row">
        <div class="col-lg-12 animated fadeInUp">

            <?= $this->Flash->render(); ?>
            <?= $this->fetch('content'); ?>
            <?php // $this->element('modal-upload'); ?>

        </div>
    </div>
</div>
