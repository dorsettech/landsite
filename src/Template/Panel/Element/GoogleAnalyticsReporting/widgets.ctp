<?php
    $stylingElements = [
        'backgrounds' => ['green-darker', 'green', 'info', 'orange'],
        'icons' => ['users', 'users', 'clock', 'sign-out-alt']
    ];
    $bg = ['red', 'orange', 'grey-darker', 'black-lighter'];
    $icons = ['desktop', 'link', 'users', 'clock'];
?>
<div class="col-lg-3 col-md-6">
    <div class="widget widget-stats bg-<?= $stylingElements['backgrounds'][$number]; ?>">
        <div class="stats-icon"><i class="fa fa-<?= $stylingElements['icons'][$number]; ?>"></i></div>
        <div class="stats-info">
            <h4><?= $data['name']; ?> <span>(<?= __('Last {0} days', $analyticsTimeSpan) ?>)</span></h4>
            <p><?= $data['value']; ?></p>
        </div>
    </div>
</div>