<h1><?= __('Gallery') ?></h1>
<?php if (!empty($data)): ?>
    <form id="galleryForm">
        <div class="row" id="sortable-view">
            <?php foreach ($data as $key => $image): ?>
                <div class="col-sm-2 gallery-item marg-b-sm" data-key="<?= $key ?>">
                    <div style="background:url(<?= $image['path'] ?>) center center no-repeat; background-size: cover; height: 200px; width: 100%; position: relative">
                        <div style="position: absolute; right: 0; bottom: 0;">
                            <a class="btn btn-default editSingleImage">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a class="btn btn-danger deleteSingleImage">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                        <input type="hidden" name="position[<?= $key ?>]" value="<?= $key ?>">
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </form>
<?php else: ?>
    <div class="alert alert-danger text-center">
        <?= __('There are no images yet.') ?>
    </div>
<?php endif; ?>
<?php $uuid = \Cake\Utility\Text::uuid(); ?>
<?= $this->Form->create(null, ['type' => 'file', 'url' => 'panel/Gallery/addFiles']) ?>
<?= $this->Form->control('model',['type'=>'hidden','value'=>$model]) ?>
<?= $this->Form->control('entityId',['type'=>'hidden','value'=>$entityId]) ?>
<?= $this->Form->control('field',['type'=>'hidden','value'=>$field]) ?>
<div class="form-group">
    <input type="file" onchange="document.getElementById('<?= $uuid ?>-input').value = (this.files.length <= 1) ? this.files[0].name : this.files.length + ' ' + 'files selected';" style="display: none;" id="<?= $uuid ?>" button-label="Select files for gallery" name="gallery" multiple="">
    <div class="input-group">
        <div class="input-group-btn">
            <button class="btn btn-default" onclick="document.getElementById('<?= $uuid ?>').click();" type="button"><?= __('Select files for gallery') ?></button>
        </div>
        <input type="text" value="" onclick="document.getElementById('<?= $uuid ?>').click();" id="<?= $uuid ?>-input" readonly="readonly" class="form-control" name="gallery">
    </div>
</div>
<?= $this->Form->control('images', ['type' => 'file', 'multiple' => true]) ?>
<?= $this->Form->end() ?>
<div class="modal fade" id="editImageModal" tabindex="-1" role="dialog" aria-labelledby="editImageModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= __('Edit image') ?></h4>
            </div>
            <div class="modal-body">
                <div class="image-handler marg-b-sm" style="background-repeat: no-repeat; background-position: center center; background-size: contain; height: 200px; width: 100%;"></div>
                <form id="updateSingleImageForm">
                    <div class="form-group">
                        <label><?= __('Image title attribute') ?></label>
                        <input class="form-control" type="text" name="title">
                    </div>
                    <div class="form-group">
                        <label><?= __('Image alt attribute') ?></label>
                        <input class="form-control" type="text" name="alt">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <?= $this->Form->button(__('Close'), ['bootstrap-type' => 'default', 'bootstrap-size' => 'sm', 'type' => 'button', 'data-dismiss' => 'modal']); ?>
                <?= $this->Form->button(__('Save changes'), ['bootstrap-type' => 'primary', 'bootstrap-size' => 'sm', 'type' => 'button', 'id' => 'saveSingleImage']); ?>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('/backend/js/gallery'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        Gallery._model = '<?= $model ?>';
        Gallery._entityId = '<?= $id ?>';
        Gallery._field = '<?= $field ?>';
    });
</script>
