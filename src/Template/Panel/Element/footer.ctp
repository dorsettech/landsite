<div id="footer" class="footer">
    <div class="pull-right">
        <?= __('CMS v.{0} powered by', $_version) ?> <a href="http://bespoke4business.com" target="_blank" title="<?= __('Bespoke 4 Business') ?>"><?= __('Bespoke 4 Business') ?></a>
    </div>
    <div>&copy; <?= date('Y') ?> <?= $_copy; ?></div>
</div>
