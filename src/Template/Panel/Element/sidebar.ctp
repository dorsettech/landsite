<div id="sidebar" class="sidebar sidebar-grid" data-disable-slide-animation="true">
    <div data-scrollbar="true" data-height="100%">
        <ul class="nav">
            <li class="nav-profile">
                <a href="javascript:;" data-toggle="nav-profile">
                    <div class="cover with-shadow"></div>
                    <div class="image image-icon bg-black text-grey-darker">
                        <?= $this->Html->image('/' . $this->Images->scale($_user->profile_image, 70, 70, 'ratio_crop'), ['alt' => $_user->full_name]) ?>
                    </div>
                    <div class="info">
                        <b class="caret pull-right"></b>
                        <?= $_user->full_name ?>
                        <small><?= $_user->group->name ?></small>
                    </div>
                </a>
            </li>
            <li>
                <ul class="nav nav-profile">
                    <li><?= $this->Html->link('<i class="fa fa-cog"></i> ' . __('Profile'), ['plugin' => false, 'prefix' => $_admin_prefix, 'controller' => 'Users', 'action' => 'edit'], ['escape' => false]) ?></li>
                    <li><?= $this->Html->link('<i class="fa fa-sign-out-alt"></i> ' . __('Sign Out'), ['plugin' => false, 'prefix' => $_admin_prefix, 'controller' => 'Users', 'action' => 'logout'], ['escape' => false]) ?></li>
                </ul>
            </li>
        </ul>
        <?= $this->Sidebar->generateMenu($sidebar_menu) ?>
    </div>
</div>
<div class="sidebar-bg"></div>

