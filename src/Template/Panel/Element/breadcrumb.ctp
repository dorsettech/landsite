<ol class="breadcrumb pull-right">
    <li class="breadcrumb-item"><?= $this->Html->link('Panel', ['plugin' => null, 'prefix' => 'panel', 'controller' => $_admin_panel, 'action' => 'index']) ?></li>
    <li class="breadcrumb-item"><?= $this->Html->link(isset($breadcrumb_controller) ? $breadcrumb_controller : $this->request->getParam('controller'), ['action' => 'index'], ['controller' => $this->request->getParam('controller'), 'prefix' => $this->request->getParam('prefix')]) ?></li>
    <?php if ($this->request->getParam('action') != 'index') : ?>
        <li class="breadcrumb-item"><?= isset($breadcrumb_action) ? $breadcrumb_action : ucfirst($this->request->getParam('action')) ?></li>
    <?php endif; ?>
</ol>