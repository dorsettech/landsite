<?= $this->Html->script("/plugins/jquery/jquery-3.3.1.min.js") ?>
<?= $this->Html->script("/plugins/jquery-ui/jquery-ui.min.js") ?>
<!--[if lt IE 9]>
<?= $this->Html->script("/panel/js/crossbrowser/html5shiv.js") ?>
<?= $this->Html->script("/panel/js/crossbrowser/respond.min.js") ?>
<?= $this->Html->script("/panel/js/crossbrowser/excanvas.min.js") ?>
<![endif]-->
<?= $this->Html->script("/plugins/slimscroll/jquery.slimscroll.min.js") ?>
<?= $this->Html->script("/plugins/js-cookie/js.cookie.js") ?>
<?= $this->Html->script("/panel/js/app.min.js") ?>

