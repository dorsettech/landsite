<?php if ($_user['is_root'] && !empty($serverSettings)): ?>
    <div class="col-12 col-sm-6 col-lg-4">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title"><?= __('Server Configuration') ?></h4>
            </div>
            <div class="panel-body">
                <?php if(!empty($serverSettings['error'])): ?>
                    <div class="alert alert-warning" role="alert">
                      <?= $serverSettings['error']; ?>
                    </div>
                <?php else: ?>
                    <div class="table-responsive">
                        <table class="table table-valign-middle">
                            <thead>
                                <tr>
                                    <th><?= __('Setting') ?></th>
                                    <th><?= __('Project') ?></th>
                                    <th><?= __('php.ini') ?></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($serverSettings as $setting => [$configValue, $phpIniValue, $acceptable]): ?>
                                    <tr>
                                        <td>
                                            <h5 class="media-heading"><?= $setting; ?></h5>
                                        </td>
                                        <td><?= $configValue; ?></td>
                                        <td><?= $phpIniValue; ?></td>
                                        <td>
                                            <?php if ($acceptable): ?>
                                                <i class="fas fa-check-circle fa-lg text-success"></i>
                                            <?php else: ?>
                                                <i class="fas fa-exclamation-circle fa-lg text-danger"></i>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endif; ?>