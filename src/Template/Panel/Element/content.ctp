<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <?php if (!isset($_removeAddButton) && $this->request->getParam('action') === 'index') : ?>
            <?= $this->Html->link('<i class="fa fa-plus"></i>', $heading['link_add'], ['class' => 'btn btn-xs btn-icon btn-circle btn-success', 'escape' => false]); ?>
            <?php endif; ?>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <?php if ($this->request->getParam('action') !== 'index') : ?>
            <?= $this->Html->link('<i class="fa fa-times"></i>', $heading['link_close'], ['class' => 'btn btn-xs btn-icon btn-circle btn-danger', 'escape' => false]); ?>
            <?php endif; ?>
        </div>
        <h4 class="panel-title"><?= $heading['panel'] ?></h4>
    </div>
    <div class="panel-body">
        <?= $this->fetch('content'); ?>
    </div>
</div>