<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Media</h2>
        <?= $this->element('breadcrumb'); ?>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <?= $this->Form->button('Add file', ['bootstrap-type' => 'primary', 'data-toggle' => 'modal', 'data-target' => '#modal-upload']); ?>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12 animated fadeInUp">
            
            <?= $this->Flash->render(); ?>
            <?= $this->fetch('content'); ?>
            <?= $this->element('modal-upload'); ?>
            
        </div>
    </div>
</div>
