<?= $this->Html->script("/plugins/jquery/jquery-3.3.1.min.js") ?>
<?= $this->Html->script("/plugins/jquery-ui/jquery-ui.min.js") ?>
<!--[if lt IE 9]>
    <?= $this->Html->script("/panel/js/crossbrowser/html5shiv.js") ?>
    <?= $this->Html->script("/panel/js/crossbrowser/respond.min.js") ?>
    <?= $this->Html->script("/panel/js/crossbrowser/excanvas.min.js") ?>
<![endif]-->
<?= $this->Html->script("/plugins/slimscroll/jquery.slimscroll.min.js") ?>
<?= $this->Html->script("/plugins/js-cookie/js.cookie.js") ?>
<?= $this->Html->script("/plugins/noty/noty.min.js") ?>
<?= $this->Html->script("/plugins/bootstrap-sweetalert/sweetalert.min.js") ?>
<?= $this->Html->script("/plugins/ckeditor/ckeditor.js") ?>
<?= $this->Html->script("/plugins/select2/dist/js/select2.min.js") ?>
<?= $this->Html->script("/plugins/bootstrap-daterangepicker/moment.js") ?>
<?= $this->Html->script("/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js") ?>
<?= $this->Html->script('/plugins/magnific-popup/jquery.magnific-popup.min.js', ['block' => 'scriptBottom']); ?>
<?= $this->Html->script("/panel/js/app.min.js") ?>
<?= $this->Html->script('FileManager.file-manager-ckeditor.js'); ?>
<?= $this->fetch('scriptBottom') ?>

<?php /*
<?= $this->Html->script("/backend/js/plugins/contentbuilder/saveimages.js") ?>
<?= $this->Html->script('/backend/js/plugins/daterangepicker/daterangepicker.js') ?>
<?= $this->Html->script("/backend/js/plugins/contentbuilder/contentbuilder.js") ?>
<?= $this->Html->script("/backend/js/plugins/ckeditor/ckeditor.js") ?>
<?= $this->Html->script("/backend/js/panel-footerscripts.js") ?>
<?= $this->Html->script("/js/csrf.js") ?>
<?= $this->fetch('scriptBottom') ?>
<?= $this->Html->script("/backend/js/main.js") ?>
<?= $this->Html->script("/backend/js/charsCounter.js") ?>
*/ ?>
