<?php if ($this->Paginator->total() > 1) : ?>
    <div class="paginator">
        <?= $this->Paginator->numbers() ?>
        <div class="counter"><?= $this->Paginator->counter() ?></div>
    </div>
<?php endif; ?>