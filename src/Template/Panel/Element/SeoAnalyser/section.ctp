<?php
$tab     = explode("\\", get_class($entity));
$model   = end($tab);
$append  = ['<div class="actions"><button type="button" class="btn btn-primary save"><i class="fa fa-check"></i></button><button type="button" class="btn btn-danger cancel"><i class="fa fa-times"></i></button></div>'];
$prepend = ['<button type="button" class="btn btn-primary dropzone-seo-analyser" id="btn_og_image_' . $entity->id . '" data-name="og_image" data-id="' . $entity->id . '" data-model="' . $model . '"><i class="fa fa-plus"></i></button>'];

/* add other logic on other modules */
switch ($model) {
    default:
    case 'Page':
        $urlname = $this->Url->build($entity->urlname, true);
        break;
}
?>
<div class="panel panel-<?= $entity->seo_analyzer['internal_ranking']['statusLevel'] ?> seo-analyser" id="sa-<?= $entity->id ?>">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript::void()" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title"><?= $model . ': ' . $urlname ?></h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-4">
                <?= $this->Form->control('urlname', ['prepend' => __('Urlname (<span class="limitFrom">{0}</span>/{1})', strlen($entity->urlname), $limits['urlname']['max']), 'maxlength' => $limits['urlname']['max'], 'label' => false, 'readonly' => true, 'id' => false, 'type' => 'text', 'value' => $entity->urlname, 'class' => 'x-editable', 'data-model' => $model, 'data-id' => $entity->id, 'data-name' => 'urlname', 'append' => $append, 'autocomplete' => 'off']); ?>
                <h4><?= __('Internal Ranking') . ' <span class="from">' . $entity->seo_analyzer['internal_ranking']['from'] . '</span> / <span class="to">' . $entity->seo_analyzer['internal_ranking']['to'] . '</span>' ?>
                    <i class="fas fa-chevron-down pull-right showHide"></i>
                </h4>
                <ul class="list-unstyled ranking-list hide">
                    <?php foreach ($entity->seo_analyzer['internal_ranking']['report'] as $status => $value) : ?>
                        <li class="text-<?= h($status) ?>"><?= implode('<br />', $value) ?></li>
                    <?php endforeach; ?>
                </ul>
                <?= $this->Html->link(__('Check on google insights'), 'https://developers.google.com/speed/pagespeed/insights/?hl=pl&url=' . $urlname, ['target' => 'blank', 'class' => 'btn btn-outline-info', 'title' => __('Check on google insights')]) ?>
                <?= $this->Html->link(__('Check on GT Metrix'), 'https://gtmetrix.com/', ['target' => 'blank', 'class' => 'btn btn-outline-info', 'title' => __('Check on GT Metrix')]) ?>
            </div>
            <div class="col-4">
                <?= $this->Form->control('seo_title', ['prepend' => __('SEO title (<span class="limitFrom">{0}</span>/{1})', strlen($entity->seo_title), $limits['seo_title']['max']), 'maxlength' => $limits['seo_title']['max'], 'label' => false, 'readonly' => true, 'id' => false, 'type' => 'text', 'value' => $entity->seo_title, 'class' => 'x-editable', 'data-model' => $model, 'data-id' => $entity->id, 'data-name' => 'seo_title', 'append' => $append, 'autocomplete' => 'off']); ?>
                <?= $this->Form->control('seo_keywords', ['prepend' => __('SEO keywords (<span class="limitFrom">{0}</span>/{1})', strlen($entity->seo_keywords), $limits['seo_keywords']['max']), 'maxlength' => $limits['seo_keywords']['max'], 'label' => false, 'readonly' => true, 'id' => false, 'type' => 'text', 'value' => $entity->seo_keywords, 'class' => 'x-editable', 'data-model' => $model, 'data-id' => $entity->id, 'data-name' => 'seo_keywords', 'append' => $append, 'autocomplete' => 'off']); ?>
                <?= $this->Form->control('seo_description', ['prepend' => __('SEO description (<span class="limitFrom">{0}</span>/{1})', strlen($entity->seo_description), $limits['seo_description']['max']), 'maxlength' => $limits['seo_description']['max'], 'label' => false, 'readonly' => true, 'id' => false, 'type' => 'text', 'value' => $entity->seo_description, 'class' => 'x-editable', 'data-model' => $model, 'data-id' => $entity->id, 'data-name' => 'seo_description', 'append' => $append, 'autocomplete' => 'off']); ?>
            </div>
            <div class="col-4">
                <?= $this->Form->control('og_title', ['prepend' => __('OG title (<span class="limitFrom">{0}</span>/{1})', strlen($entity->og_title), $limits['og_title']['max']), 'maxlength' => $limits['og_title']['max'], 'label' => false, 'readonly' => true, 'id' => false, 'type' => 'text', 'value' => $entity->og_title, 'class' => 'x-editable', 'data-model' => $model, 'data-id' => $entity->id, 'data-name' => 'og_title', 'append' => $append, 'autocomplete' => 'off']); ?>
                <?= $this->Form->control('og_description', ['prepend' => __('OG description (<span class="limitFrom">{0}</span>/{1})', strlen($entity->og_description), $limits['og_description']['max']), 'maxlength' => $limits['og_description']['max'], 'label' => false, 'readonly' => true, 'id' => false, 'type' => 'text', 'value' => $entity->og_description, 'class' => 'x-editable', 'data-model' => $model, 'data-id' => $entity->id, 'data-name' => 'og_descriptiony', 'append' => $append, 'autocomplete' => 'off']); ?>
                <div class="row">
                    <div class="col-10">
                        <?= $this->Form->control('og_image', ['prepend' => $prepend, 'id' => "og_image_" . $entity->id, 'readonly' => 'readonly', 'value' => $entity->og_image, 'label' => false, 'type' => 'text']); ?>
                    </div>
                    <div class="col-2">
                        <?php if (!empty($entity->og_image)) : ?>
                        <img src="<?= $this->Images->scale($entity->og_image_path, 30, 30, 'ratio_fill') ?>" width="30" height="30" alt="" id="og_image_thumb_<?= $entity->id ?>" />
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
