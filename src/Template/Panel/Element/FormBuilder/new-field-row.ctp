<!--Single row to new field-->
<table style="display:none;" id="row">
    <tbody>
        <tr>
            <td><?= $this->Form->control('label[]', ['label' => false]) ?></div></td>
            <td><?= $this->Form->control('input_name[]', ['label' => false, 'required' => true, 'class' => 'inputName']) ?></div></td>
            <td><?= $this->Form->control('validation_text[]', ['label' => false]) ?></div></td>
            <td><?= $this->Form->control('class[]', ['label' => false]) ?></div></td>
            <td><?= $this->Form->control('placeholder[]', ['label' => false]) ?></div></td>
            <td>
                <div class="form-group">
                    <?= $this->Form->select('type[]', $types, ['escape' => false, 'class' => 'optionsSelect', 'empty' => 'Choose...']) ?>
                    <button type="button" data-toggle="modal" data-target=".bs-example-modal-sm" class="btn btn-default btn-xs optionsButton"><i class="fa fa-edit"></i></button>
                    <?= $this->Form->control('options[]', ['type' => 'hidden', 'value' => '', 'class' => 'options']) ?>
                </div>
            </td>
            <td><?= $this->Form->control('value[]', ['label' => false]) ?></div></td>
            <td><div class="form-group"><?= $this->Form->select('validation[]', $validations, ['escape' => false, 'class' => 'validationSelect', 'empty' => 'Choose...']) ?></div></td>
            <td><?= $this->Form->control('rule[]', ['label' => false, 'class' => 'rule']) ?></td>
            <td><div class="form-group"><?= $this->Form->checkbox('required[]', ['class' => 'requiredField']) ?></div></td>
            <td style="width: 100px;"><span class="btn btn-xs btn-default moveUp"><i class="fa fa-chevron-up"></i></span><span class="btn btn-xs btn-default moveDown"><i class="fa fa-chevron-down"></i></span><button class="remove btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>
        </tr>
    </tbody>
</table>

<table id="option" style="display:none;">
    <tbody>
        <tr>
            <td><?= $this->Form->control('optionName[]', ['label' => false]) ?></td>
            <td><?= $this->Form->control('optionValue[]', ['label' => false]) ?></td>
            <td><button class="remove btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>
        </tr>
    </tbody>
</table>