<div id="optionModalDialog" class="modal fade bs-example-modal-sm in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="mySmallModalLabel"><?= __('Options generator') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body clearfix">
                <div class="col-xs-12 mb-3">
                    <button id="addOption" class="btn btn-primary"><?= __('Add option') ?></button>
                </div>
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered marg-t">
                            <thead>
                                <tr>
                                    <td><?= __('Name') ?></td>
                                    <td><?= __('Value') ?></td>
                                    <td><?= __('Actions') ?></td>
                                </tr>
                            </thead>
                            <tbody class="optionsBody"></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="saveOptions" class="btn btn-primary pull-right"><?= __('Save') ?></button>
            </div>
        </div>
    </div>
</div>