<?php if (!empty($user->service)) : ?>
    <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th><?= __('Id') ?></th>
                    <th><?= __('Company') ?></th>
                    <th><?= __('Business Category') ?></th>
                    <th><?= __('Postcode') ?></th>
                    <th><?= __('Ad Total Cost') ?></th>
                    <th><?= __('Created') ?> / <?= __('Modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?= $this->Number->format($user->service->id) ?></td>
                    <td><?= h($user->service->company) ?><br><small><?= h($user->service->slug) ?></small></td>
                    <td><?= $user->service->has('category') ? $user->service->category->name : '' ?></td>
                    <td><?= h($user->service->postcode) ?></td>
                    <td><?= $this->Number->format($user->service->ad_total_cost) ?></td>
                    <td>
                        <?= $this->Html->badge('<i class="far fa-clock"></i> ' . h($this->Transform->date($user->service->created)), 'light') ?><br>
                        <?= $this->Html->badge('<i class="fa fa-clock"></i> ' . h($this->Transform->date($user->service->modified)), 'light') ?>
                    </td>
                    <td class="actions">
                        <div class="btn-group btn-group-xs">
                            <?= $this->Html->link('<i class="fa fa-eye"></i>', ['controller' => 'Services', 'action' => 'view', $user->service->id], ['class' => 'btn btn-default', 'escape' => false, 'title' => __('View'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            <?= $this->Html->link('<i class="fa fa-images"></i>', ['controller' => 'ServiceMedia', 'action' => 'list', $user->service->id], ['class' => 'btn btn-info', 'escape' => false, 'title' => __('Media'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            <?= $this->Html->link('<i class="fa fa-edit"></i>', ['controller' => 'Services', 'action' => 'edit', $user->service->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Edit'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['controller' => 'Services', 'action' => 'delete', $user->service->id], ['class' => 'btn btn-danger', 'confirm' => __('Please confirm removal Service #{0}', $user->service->id), 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
<?php else : ?>
    <div class="well text-center text-warning"><?= __('There are no Services yet.') ?> <?= $this->Html->link(__('Add first Service'), ['controller' => 'Services', 'action' => 'add']) ?></div>
<?php endif; ?>