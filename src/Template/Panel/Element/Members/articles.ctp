<?php

use App\Model\Enum\State;

?>
<?php if (!empty($user->articles)) : ?>
    <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th><?= __('Id') ?></th>
                    <th><?= __('Type') ?></th>
                    <th><?= __('Title') ?></th>
                    <th><?= __('Image') ?></th>
                    <th><?= __('Status') ?></th>
                    <th><?= __('State') ?></th>
                    <th><?= __('Category') ?></th>
                    <th><?= __('Visibility') ?></th>
                    <th><?= __('Publish Date') ?></th>
                    <th><?= __('Created') ?> / <?= __('Modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($user->articles as $article): ?>
                    <?php $controller = ($article->type == \App\Model\Enum\ArticleType::CASE_STUDY) ? 'CaseStudies' : 'News'; ?>
                    <tr>
                        <td><?= $this->Number->format($article->id) ?></td>
                        <td><?= \App\Model\Enum\ArticleType::getNamesList()[$article->type] ?></td>
                        <td><?= h($article->title) ?></td>
                        <td>
                            <?php if (!empty($article->image)): ?>
                                <?= $this->Html->link($this->Html->image('/' . $this->Images->scale($article->image_path, 30, 30), ['class' => 'img-thumbnail']), $article->image_path, ['class' => 'magnific-image', 'escape' => false]) ?>
                            <?php endif; ?>
                        </td>
                        <td><?= h($article->status) ?></td>
                        <td><?= h($article->state) ?></td>
                        <td><?= $article->has('category') ? $article->category->name : '' ?></td>
                        <td>
                            <?= $article->visibility ? '<i class="far fa-check-square"></i>' : '<i class="far fa-square"></i>' ?>
                        </td>
                        <td><?= h($this->Transform->date($article->publish_date)) ?></td>
                        <td>
                            <?= $this->Html->badge('<i class="far fa-clock"></i> ' . h($this->Transform->date($article->created)), 'light') ?><br>
                            <?= $this->Html->badge('<i class="fa fa-clock"></i> ' . h($this->Transform->date($article->modified)), 'light') ?>
                        </td>

                        <td class="actions">
                            <div class="btn-group btn-group-xs">
                                <?php if ($article->state === State::PENDING): ?>
                                    <?= $this->Html->link('<i class="fa fa-check-circle"></i>', '#', ['class' => 'btn btn-default approve-reject-button', 'escape' => false, 'title' => __('Approve/Reject'), 'data-toggle' => 'modal', 'data-target' => '#approve-reject-modal', 'data-id' => $article->id]) ?>
                                <?php endif; ?>
                                <?= $this->Html->link('<i class="fa fa-edit"></i>', ['controller' => $controller, 'action' => 'edit', $article->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Edit'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                                <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['controller' => $controller, 'action' => 'delete', $article->id], ['class' => 'btn btn-danger', 'confirm' => __('Please confirm removal Article #{0}', $article->id), 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php else : ?>
    <div class="well text-center text-warning"><?= __('There are no Articles yet.') ?> <?= $this->Html->link(__('Add first Article'), ['action' => 'add']) ?></div>
<?php endif; ?>
<?= $this->element('Structure/approve-reject-modal') ?>
