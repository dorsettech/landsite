<?= $this->Html->image('/' . $this->Images->scale($user->image_path, 300, 200, 'ratio'), ['class' => 'img-thumbnail pull-right m-b-15']) ?>
<div class="row view m-b-15">
    <div class="col-sm-6 col-lg-4">
        <label><i class="far fa-user"></i> <?= __('First Name, Last Name') ?></label>
        <?= $user->full_name ?>
    </div>
    <div class="col-sm-6 col-lg-4">
        <label><i class="far fa-envelope"></i> <?= __('Email Address') ?></label>
        <a href="<?= $this->Url->build('mailto:' . $user->email) ?>"><?= $user->email ?></a>
    </div>
    <div class="col-sm-6 col-lg-4">
        <label><i class="fas fa-phone"></i> <?= __('Phone') ?></label>
        <?= $user->phone ?>
    </div>
</div>

<hr>

<div class="row view m-b-15">
    <div class="col-sm-6">
        <?php if (!empty($user->user_detail->company)) : ?>
            <label><i class="far fa-building"></i> <?= __('Company') ?></label>
            <?= $user->user_detail->company ?>
        <?php endif; ?>

        <?php if (!empty($user->user_detail->company_description)) : ?>
            <label class="m-t-15"><i class="fas fa-building"></i> <?= __('Company Description') ?></label>
            <p><?= nl2br($user->user_detail->company_description) ?></p>
        <?php endif; ?>
    </div>
    <div class="col-sm-6">
        <?php if (!empty($user->user_detail->postcode)) : ?>
            <label><i class="fas fa-map-marker-alt"></i> <?= __('Postcode') ?></label>
            <?= $user->user_detail->postcode ?>
        <?php endif; ?>

        <?php if (!empty($user->user_detail->location)) : ?>
            <label class="m-t-15"><i class="fas fa-map-marker"></i> <?= __('Location') ?></label>
            <p><?= nl2br($user->user_detail->location) ?></p>
        <?php endif; ?>
    </div>
</div>
<div class="clearfix"></div>

<div class="text-right view">
    <label><i></i> <?= __('Created / Modified') ?></label>
    <small><?= $this->Transform->date($user->created, 'human') ?> / <?= $this->Transform->date($user->modified, 'human') ?></small>
</div>