<h3><?= __('Recent Property Searches') ?></h3>
<div class="table-responsive">
    <?php if (!empty($propertySearches)) : ?>
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
        <thead>
            <tr>
                <th><?= __('Location') ?></th>
                <th><?= __('Purpose') ?></th>
                <th><?= __('Property Type') ?></th>
                <th><?= __('Min Price') ?></th>
                <th><?= __('Max Price') ?></th>
                <th><?= __('Radius') ?></th>
                <th><?= __('Attributes') ?></th>
                <th><?= __('Receive Alerts') ?></th>
                <th><?= __('Created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($propertySearches as $propertySearch): ?>
                <tr>
                    <td><?= h($propertySearch->location) ?></td>
                    <td><?= h($propertySearch->purpose) ?></td>
                    <td><?= $propertySearch->has('property_type') ? $propertySearch->property_type->name : '' ?></td>
                    <td><?= ($propertySearch->min_price > 0) ? $this->Number->format($propertySearch->min_price) : '' ?></td>
                    <td><?= ($propertySearch->max_price > 0 ) ? $this->Number->format($propertySearch->max_price) : '' ?></td>
                    <td><?= ($propertySearch->radius > 0) ? $this->Number->format($propertySearch->radius) : '' ?></td>
                    <td><?= $propertySearch->attribute_names ?></td>
                    <td>
                        <?= $this->Switcher->create('alerts', ['val' => $propertySearch->alerts, 'data-path' => '/' . $_admin_prefix . '/property-searches', 'data-model' => 'PropertySearches', 'data-id' => $propertySearch-> id]); ?>
                    </td>
                    <td><?= $this->Html->badge('<i class="far fa-clock"></i> ' . h($this->Transform->date($propertySearch->created)), 'light') ?></td>
                    <td class="actions">
                        <div class="btn-group btn-group-xs">
                            <?= $this->Html->link('<i class="fas fa-search"></i>', ['_name' => 'properties-listing', '?' => $propertySearch->query_array], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Search'), 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'target' => '_blank']) ?>
                            <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['controller' => 'PropertySearches', 'action' => 'delete', $propertySearch->id], ['class' => 'btn btn-danger', 'confirm' => __('Please confirm removal Property Search #{0}', $propertySearch->id), 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php else : ?>
    <div class="well text-center text-warning"><?= __('There are no Property searches yet.') ?></div>
    <?php endif; ?>
</div>

<hr>

<h3><?= __('Recent Business Searches') ?></h3>
<div class="table-responsive">
    <?php if (!empty($serviceSearches)) : ?>
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
        <thead>
            <tr>
                <th><?= __('Location') ?></th>
                <th><?= __('Keywords') ?></th>
                <th><?= __('Category') ?></th>
                <th><?= __('Radius') ?></th>
                <th><?= __('Receive Alerts') ?></th>
                <th><?= __('Created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($serviceSearches as $serviceSearch): ?>
                <tr>
                    <td><?= h($serviceSearch->location) ?></td>
                    <td><?= h($serviceSearch->keywords) ?></td>
                    <td><?= $serviceSearch->has('category') ? $serviceSearch->category->name : '' ?></td>
                    <td><?= ($serviceSearch->radius > 0 ) ? $this->Number->format($serviceSearch->radius) : '' ?></td>
                    <td>
                        <?= $this->Switcher->create('alerts', ['val' => $serviceSearch->alerts,  'data-path' => '/' . $_admin_prefix . '/service-searches', 'data-model' => 'ServiceSearches', 'data-id' => $serviceSearch-> id]); ?>
                    </td>
                    <td><?= $this->Html->badge('<i class="far fa-clock"></i> ' . h($this->Transform->date($serviceSearch->created)), 'light') ?></td>
                    <td class="actions">
                        <div class="btn-group btn-group-xs">
                            <?= $this->Html->link('<i class="fas fa-search"></i>', ['_name' => 'professional-services-listing', '?' => $serviceSearch->query_array], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Search'), 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'target' => '_blank']) ?>
                            <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['controller' => 'ServiceSearches', 'action' => 'delete', $serviceSearch->id], ['class' => 'btn btn-danger', 'confirm' => __('Please confirm removal Service Search #{0}', $serviceSearch->id), 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php else : ?>
    <div class="well text-center text-warning"><?= __('There are no Business searches yet.') ?></div>
    <?php endif; ?>
</div>