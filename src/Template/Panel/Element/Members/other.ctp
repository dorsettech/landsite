<?php if (!empty($user->user_detail)) : ?>
    <h3><?= __('Interested in') ?>:</h3>
    <div class="row view">
        <div class="col-sm-6 col-lg-3">
            <i class="far <?= $user->user_detail->pref_buying ? 'fa-check-square' : 'fa-square' ?>"></i> <?= __('Buying') ?>
        </div>
        <div class="col-sm-6 col-lg-3">
            <i class="far <?= $user->user_detail->pref_selling ? 'fa-check-square' : 'fa-square' ?>"></i> <?= __('Selling') ?>
        </div>
        <div class="col-sm-6 col-lg-3">
            <i class="far <?= $user->user_detail->pref_professional ? 'fa-check-square' : 'fa-square' ?>"></i> <?= __('Professional Services') ?>
        </div>
        <div class="col-sm-6 col-lg-3">
            <i class="far <?= $user->user_detail->pref_insights ? 'fa-check-square' : 'fa-square' ?>"></i> <?= __('Insights') ?>
        </div>
    </div>

    <hr>
<?php endif; ?>

<h3><?= __('Preferred Insights') ?>:</h3>
<ul class="list-inline view">
    <?php foreach ($relatedCategories as $category) : ?>
        <li><?= h($category->name) ?></li>
    <?php endforeach; ?>
</ul>

<hr>

<?php if (!empty($user->user_detail)) : ?>
    <h3><?= __('Marketing Preferences') ?>:</h3>
    <div class="row view">
        <div class="col-sm-6 col-lg-3">
            <i class="far <?= $user->user_detail->marketing_agreement_1 ? 'fa-check-square' : 'fa-square' ?>"></i> <?= __('Contact by email') ?>
        </div>
        <div class="col-sm-6 col-lg-3">
            <i class="far <?= $user->user_detail->marketing_agreement_2 ? 'fa-check-square' : 'fa-square' ?>"></i> <?= __('Contact by phone') ?>
        </div>
        <div class="col-sm-6 col-lg-3">
            <i class="far <?= $user->user_detail->marketing_agreement_3 ? 'fa-check-square' : 'fa-square' ?>"></i> <?= __('Other Contact') ?>
        </div>
    </div>
<?php endif; ?>
