<?php if (!empty($user->properties)) : ?>
    <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th><?= __('ID') ?></th>
                    <th><?= __('Title') ?></th>
                    <th><?= __('Image') ?></th>
                    <th><?= __('Type') ?></th>
                    <th><?= __('Status') ?></th>
                    <th><?= __('Created') ?></th>
                    <th><?= __('Expiry Date') ?></th>
                    <th><?= __('Enquiries') ?></th>
                    <th><?= __('Views') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($user->properties as $property): ?>
                    <tr>
                        <td><?= $this->Number->format($property->id) ?></td>
                        <td><?= $this->Html->link(h($property->title), ['controller' => 'Properties', 'action' => 'view', $property->id]) ?></td>
                        <td><?php if (!empty($property->main_image)) : ?><?= $this->Html->link($this->Html->image('/' . $this->Images->scale($property->main_image, 30, 30), ['class' => 'img-thumbnail']), $property->main_image, ['class' => 'magnific-image', 'escape' => false]) ?><?php endif; ?></td>
                        <td><?= $property->has('property_type') ? $property->property_type->name : '' ?></td>
                        <td><?= h($property->status) ?></td>
                        <td><?= $this->Html->badge('<i class="far fa-clock"></i> ' . h($this->Transform->date($property->created)), 'light') ?></td>
                        <td><?= h($this->Transform->date($property->ad_expiry_date)) ?></td>
                        <td><?= $this->Number->format($property->enquiries) ?></td>
                        <td><?= $this->Number->format($property->views) ?></td>
                        <td class="actions">
                            <div class="btn-group btn-group-xs">
                                <?= $this->Html->link('<i class="fa fa-eye"></i>', ['controller' => 'Properties', 'action' => 'view', $property->id], ['class' => 'btn btn-default', 'escape' => false, 'title' => __('View'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                                <?= $this->Html->link('<i class="fa fa-images"></i>', ['controller' => 'PropertyMedia', 'action' => 'list', $property->id], ['class' => 'btn btn-info', 'escape' => false, 'title' => __('Media'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                                <?= $this->Html->link('<i class="fa fa-edit"></i>', ['controller' => 'Properties', 'action' => 'edit', $property->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Edit'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                                <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['controller' => 'Properties', 'action' => 'delete', $property->id], ['class' => 'btn btn-danger', 'confirm' => __('Please confirm removal Property #{0}', $property->id), 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php else : ?>
    <div class="well text-center text-warning"><?= __('There are no Properties yet.') ?> <?= $this->Html->link(__('Add first Property'), ['controller' => 'Properties', 'action' => 'add']) ?></div>
<?php endif; ?>