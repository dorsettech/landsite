
<?php if (!empty($user->user_detail->use_billing_details) && $user->user_detail->use_billing_details) : ?>
    <div class="view"><i class="far fa-check-square"></i> <?= __('Billing details are the same') ?></div>

    <hr>

    <div class="row view m-b-15">
        <div class="col-sm-6 col-lg-4">
            <label><i class="far fa-user"></i> <?= __('First Name, Last Name') ?></label>
            <?= $user->full_name ?>
        </div>
        <div class="col-sm-6 col-lg-4">
            <label><i class="far fa-envelope"></i> <?= __('Email Address') ?></label>
            <a href="<?= $this->Url->build('mailto:' . $user->email) ?>"><?= $user->email ?></a>
        </div>
        <div class="col-sm-6 col-lg-4">
            <label><i class="fas fa-phone"></i> <?= __('Phone') ?></label>
            <?= $user->phone ?>
        </div>
    </div>

    <hr>

    <div class="row view m-b-15">
        <div class="col-sm-6">
            <label><i class="far fa-building"></i> <?= __('Company') ?></label>
            <?= $user->user_detail->company ?>
        </div>
        <div class="col-sm-6">
            <label><i class="fas fa-map-marker-alt"></i> <?= __('Postcode') ?></label>
            <?= $user->user_detail->postcode ?>
        </div>
    </div>
<?php elseif (!empty($user->user_billing_addresses[0])) : ?>
    <div class="view"><i class="far fa-square"></i> <?= __('Billing details are the same') ?></div>

    <hr>

    <div class="row view m-b-15">
        <div class="col-sm-6 col-lg-4">
            <label><i class="far fa-user"></i> <?= __('First Name, Last Name') ?></label>
            <?= $user->user_billing_addresses[0]->full_name ?>
        </div>
        <div class="col-sm-6 col-lg-4">
            <label><i class="far fa-envelope"></i> <?= __('Email Address') ?></label>
            <a href="<?= $this->Url->build('mailto:' . $user->user_billing_addresses[0]->email) ?>"><?= $user->user_billing_addresses[0]->email ?></a>
        </div>
        <div class="col-sm-6 col-lg-4">
            <label><i class="fas fa-phone"></i> <?= __('Phone') ?></label>
            <?= $user->user_billing_addresses[0]->phone ?>
        </div>
    </div>

    <hr>

    <div class="row view m-b-15">
        <div class="col-sm-6">
            <label><i class="far fa-building"></i> <?= __('Company') ?></label>
            <?= $user->user_billing_addresses[0]->company ?>
        </div>
        <div class="col-sm-6">
            <label><i class="fas fa-map-marker-alt"></i> <?= __('Postcode') ?></label>
            <?= $user->user_billing_addresses[0]->postcode ?>
        </div>
    </div>
<?php endif; ?>
