<div class="table-responsive">
    <?php if ($emailQueue->count() > 0) : ?>
        <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('email') ?></th>
                    <th><?= $this->Paginator->sort('type') ?></th>
                    <th><?= $this->Paginator->sort('send_date') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($emailQueue as $emailQueue): ?>
                    <tr>
                        <td><?= $this->Number->format($emailQueue->id) ?></td>
                        <td><?= h($emailQueue->email) ?></td>
                        <td><?= h($emailQueue->type) ?></td>
                        <td><?= $this->Html->badge('<i class="fa fa-clock"></i> '.h($this->Transform->date($emailQueue->send_date)), 'light') ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else : ?>
        <div class="well text-center text-warning"><?= __('There are no Email Queue yet.') ?> <?= $this->Html->link(__('Add first Email Queue'), ['action' => 'add']) ?></div>
    <?php endif; ?>
</div>
<div class="action-buttons">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-lg-12 pull-left">
            <?= $this->Form->create(null, ['url' => ['controller' => 'EmailQueue', 'action' => 'index'], 'type' => 'get', 'class' => 'form from-inline forms-list-filter']) ?>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">From: </span>
                </div>
                <?= $this->Form->input('from', ['type' => 'text', 'id' => 'datetimelogs', 'class' => 'form-control', 'label' => false, 'value' => $this->request->query('from')]) ?>
                <div class="input-group-prepend">
                    <span class="input-group-text">To: </span>
                </div>
                <?= $this->Form->input('to', ['type' => 'text', 'id' => 'createdTo', 'class' => 'form-control', 'label' => false, 'value' => $this->request->query('to')]) ?>
                <div class="input-group-prepend">
                    <span class="input-group-btn"><?= $this->Form->submit('Filter by date', ['class' => 'btn btn-primary']) ?></span>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->element('pagination') ?>
