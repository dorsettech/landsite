<?= $this->Form->create($propertyType) ?>
<?= $this->element('Structure/action-buttons') ?>
<hr>
<div class="row">
    <div class="col-sm-6"><?= $this->Form->control('name'); ?></div>
    <div class="col-sm-6"><?= $this->Form->control('parent_id', ['options' => $parentPropertyTypes, 'empty' => true]); ?></div>
    <div class="col-sm-2"><?= $this->Form->control('position'); ?></div>
    <div class="col-sm-2 d-flex align-items-center"><?= $this->Form->control('visibility', ['label' => __('Visible')]); ?></div>
</div>
<hr><?= $this->element('Structure/action-buttons') ?>
<?= $this->Form->end() ?>

