<div class="table-responsive">
    <?php if ($propertyTypes->count() > 0) : ?>
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('position') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('parent_id') ?></th>
                <th><?= $this->Paginator->sort('visibility') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($propertyTypes as $propertyType): ?>
                <tr>
                    <td><?= $this->Number->format($propertyType->id) ?></td>
                    <td><?= $this->Number->format($propertyType->position) ?></td>
                    <td><?= h($propertyType->name) ?></td>
                    <td><?= $propertyType->has('parent_property_type') ? $this->Html->link($propertyType->parent_property_type->name, ['controller' => 'PropertyTypes', 'action' => 'edit', $propertyType->parent_property_type->id]) : '' ?></td>
                    <td>
                        <?= $this->Switcher->create('visibility', ['val' => $propertyType->visibility, 'data-id' => $propertyType-> id]); ?>
                    </td>
                    <td><?= $this->Html->badge('<i class="far fa-clock"></i> ' . h($this->Transform->date($propertyType->created)), 'light') ?></td>
                    <td><?= $this->Html->badge('<i class="fa fa-clock"></i> ' . h($this->Transform->date($propertyType->modified)), 'light') ?></td>
                    <td class="actions">
                        <div class="btn-group btn-group-xs">
                            <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $propertyType->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Edit'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $propertyType->id], ['class' => 'btn btn-danger', 'confirm' => __('Please confirm you would like to remove property type {0}', $propertyType->name), 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php else : ?>
    <div class="well text-center text-warning"><?= __('There are no Property Types yet.') ?> <?= $this->Html->link(__('Add first Property Type'), ['action' => 'add']) ?></div>
    <?php endif; ?>
</div>
<div class="row action-buttons">
    <div class="col text-right">
    <?= $this->Html->link(__('Add Property Type'), ['action' => 'add'], ['class' => 'btn btn-primary']); ?>
    </div>
</div>
<?= $this->element('pagination') ?>
