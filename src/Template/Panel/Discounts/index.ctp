<?php

use App\Model\Enum\DiscountType;

?>
<div class="table-responsive">
    <?php if (!empty($discounts)) : ?>
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('code', ['label' => __('Coupon Code')]) ?></th>
                <th><?= $this->Paginator->sort('name', ['label' => __('Coupon Name')]) ?></th>
                <th><?= $this->Paginator->sort('type', ['label' => __('Discount Type')]) ?></th>
                <th><?= $this->Paginator->sort('value', ['label' => __('Discount Amount')]) ?></th>
                <th><?= $this->Paginator->sort('date_start', ['label' => __('Start Date')]) ?></th>
                <th><?= $this->Paginator->sort('date_end', ['label' => __('End Date')]) ?></th>
                <th><?= $this->Paginator->sort('per_customer', ['label' => __('Uses Per Customer')]) ?></th>
                <th><?= $this->Paginator->sort('per_code', ['label' => __('Uses Per Code')]) ?></th>
                <th><?= $this->Paginator->sort('user_id', ['label' => __('User')]) ?></th>
                <th><?= __('Times Used') ?></th>
                <th><?= $this->Paginator->sort('active', ['label' => __('Enabled')]) ?></th>
                <th><?= $this->Paginator->sort('created') ?> / <?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($discounts as $discount): ?>
                <tr>
                    <td><?= $this->Number->format($discount->id) ?></td>
                    <td><?= h($discount->code) ?></td>
                    <td><?= h($discount->name) ?></td>
                    <td>
                        <?= $this->Html->badge(h($discount->type), ($discount->type == DiscountType::FREE ? 'success' : ($discount->type == DiscountType::PERCENTAGE ? 'primary': 'info'))) ?>
                    </td>
                    <td><?= $discount->amount ?></td>
                    <td><?= h($this->Transform->date($discount->date_start, 'date')) ?></td>
                    <td><?= h($this->Transform->date($discount->date_end, 'date')) ?></td>
                    <td><?= is_numeric($discount->per_customer) ? $discount->per_customer : '<i class="fas fa-infinity"></i>' ?></td>
                    <td><?= is_numeric($discount->per_code) ? $discount->per_code : '<i class="fas fa-infinity"></i>' ?></td>
                    <td>
                        <?php if ($discount->has('user')) : ?>
                        <?= $this->Html->link($discount->user->full_name, ['controller' => 'Users', 'action' => 'edit', $discount->user->id]) ?><br>
                        <?= $discount->user->email ?>
                        <?php else : ?>
                        <?= __('All users') ?>
                        <?php endif; ?>
                    </td>
                    <td><?= $this->Number->format($discount->times_used) ?></td>
                    <td>
                        <?= $this->Switcher->create('active', ['val' => $discount->active, 'data-id' => $discount-> id]); ?>
                    </td>
                    <td>
                        <?= $this->Html->badge('<i class="far fa-clock"></i> ' . h($this->Transform->date($discount->created)), 'light') ?><br>
                        <?= $this->Html->badge('<i class="fa fa-clock"></i> ' . h($this->Transform->date($discount->modified)), 'light') ?>
                    </td>
                    <td class="actions">
                        <div class="btn-group btn-group-xs">
                            <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $discount->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Edit'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $discount->id], ['class' => 'btn btn-danger', 'confirm' => __('Please confirm removal Discount #{0}', $discount->id), 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php else : ?>
    <div class="well text-center text-warning"><?= __('There are no Discounts yet.') ?> <?= $this->Html->link(__('Add first Discount'), ['action' => 'add']) ?></div>
    <?php endif; ?>
</div>
<div class="row action-buttons">
    <div class="col text-right">
    <?= $this->Html->link(__('Add Discount'), ['action' => 'add'], ['class' => 'btn btn-primary']); ?>
    </div>
</div>
<?= $this->element('pagination') ?>
