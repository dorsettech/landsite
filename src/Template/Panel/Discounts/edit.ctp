<?= $this->Form->create($discount) ?>
<?= $this->element('Structure/action-buttons') ?>
<hr>
<div class="row">
    <div class="col-sm-6 col-lg-4"><?= $this->Form->control('code', ['label' => __('Coupon Code'), 'append' => $this->Form->button(__('Generate'), ['type' => 'button', 'class' => 'generate', 'data-target' => '#code'])]); ?></div>
    <div class="col-sm-6 col-lg-8"><?= $this->Form->control('name', ['label' => __('Coupon Name')]); ?></div>
    <div class="col-sm-6 col-lg-3"><?= $this->Form->control('type', ['type' => 'select', 'label' => __('Discount Type'), 'options' => $discountTypes, 'class' => 'discount-type']); ?></div>
    <div class="col-sm-6 col-lg-3"><?= $this->Form->control('value', ['label' => __('Discount Amount'), 'min' => 0, 'max' => '', 'step' => 1, 'class' => 'discount-amount']); ?></div>
    <div class="col-sm-6 col-lg-3"><?= $this->Form->control('per_customer', ['label' => __('Uses Per Customer')]); ?></div>
    <div class="col-sm-6 col-lg-3"><?= $this->Form->control('per_code', ['label' => __('Uses Per Code')]); ?></div>
    <div class="col-sm-6 col-lg-4"><?= $this->Form->control('date_start', ['type' => 'text', 'label' => __('Start Date'), 'val' => (!empty($discount->date_start) ? $discount->date_start->format('Y-m-d') : ''), 'class' => 'date']); ?></div>
    <div class="col-sm-6 col-lg-4"><?= $this->Form->control('date_end', ['type' => 'text', 'label' => __('End Date'), 'val' => (!empty($discount->date_end) ? $discount->date_end->format('Y-m-d') : ''), 'class' => 'date']); ?></div>
    <div class="col-sm-6 col-lg-4"><?= $this->Form->control('user_id', ['options' => $users, 'empty' => true, 'class' => 'select2', 'data-type' => 'Members', 'min-length' => 2]); ?></div>
    <div class="col-sm-2 d-flex align-items-center"><?= $this->Form->control('active', ['label' => __('Enabled')]); ?></div>
</div>
<hr>
<?= $this->element('Structure/action-buttons') ?>
<?= $this->Form->end() ?>
