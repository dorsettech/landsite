<div class="row">
    <div class="col-12">
        <?php if (isset($formsData) && !empty($formsData)): ?>
            <div class="pull-left">
                <?= $this->Html->link('Back', ['controller' => 'Forms', 'action' => 'index'], ['class' => 'btn btn-warning']) ?>
            </div>
            <div class="pull-right">
                <div class="input-group">
                    <?= $this->Form->create(null, ['class' => 'form-inline forms-list-filter']) ?>
                    <div class="input-group-prepend">
                        <span class="input-group-text">From:</span>
                    </div>
                    <?= $this->Form->input('from', ['type' => 'text', 'id' => 'datetimelogs', 'class' => 'form-control', 'label' => false]) ?>
                    <div class="input-group-prepend">
                        <span class="input-group-text">To:</span>
                    </div>
                    <?= $this->Form->input('to', ['type' => 'text', 'id' => 'createdTo', 'class' => 'form-control', 'label' => false]) ?>
                    <div class="input-group-append">
                        <span class="input-group-btn">
                            <?= $this->Form->submit('Filter', ['class' => 'btn btn-primary']) ?>
                        </span>
                    </div>
                    <?= $this->Form->end() ?>
                    <?= $this->Form->create(null, ['url' => ['controller' => 'FormsData', 'action' => 'exportMessages'], 'class' => 'form-inline']) ?>
                    <div class="input-group-append">
                        <span class="input-group-btn">
                            <?= $this->Form->submit('Export', ['class' => 'btn btn-info']) ?>
                        </span>
                    </div>
                    <?= $this->Form->input('form_id', ['type' => 'hidden', 'value' => $form->id]) ?>
                    <?= $this->Form->input('from', ['type' => 'hidden', 'value' => $from]) ?>
                    <?= $this->Form->input('to', ['type' => 'hidden', 'value' => $to]) ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        <?php else: ?>
            <div class="pull-left">
                <?= $this->Html->link('Back', ['controller' => 'Forms', 'action' => 'index'], ['class' => 'btn btn-warning']) ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<hr>
<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" class="table table-striped formsDataList">
        <thead>
            <tr>
                <th><?= __('ID') ?></th>
                <?php foreach ($formFields as $fieldName => $fieldValue): ?>
                    <?php if ($fieldName !== 'submit'): ?>
                        <th> <?= ucfirst(str_replace('_', ' ', $fieldName)) ?></th>
                    <?php endif; ?>
                <?php endforeach; ?>
                <th><?= __('Created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($formsData)): ?>
                <?php foreach ($formsData as $data): ?>
                    <tr class="<?= ($data->view === false) ? 'not_view' : '' ?>">
                        <td><?= $data->id ?></td>
                        <?php foreach ($data->values as $fieldName => $value): ?>
                            <?php if ($fieldName !== 'view'): ?>
                                <td><?= $value ?></td>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <td><?= $this->Html->badge('<i class="far fa-clock"></i> '.h($this->Transform->date($data->created)), 'light') ?></td>
                        <td class="actions">
                            <div class="btn-group btn-group-xs">
                                <?= $this->Html->link('<i class="fa fa-eye"></i>', ['controller' => 'FormsData', 'action' => 'view', $data->id], ['class' => 'btn btn-default', 'escape' => false, 'title' => __('View'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                                <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $data->id], ['class' => 'btn btn-danger', 'confirm' => 'Please confirm form removal '.h($data['id']).'?', 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
</div>
<hr>
<div class="row">
    <div class="col-xs-12 text-right">
        <?= $this->Html->link('Back', ['controller' => 'Forms', 'action' => 'index'], ['class' => 'btn btn-warning']) ?>
    </div>
</div>
