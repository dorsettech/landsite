<div class="row">
    <?php foreach ($messages as $message): ?>
        <div class="col-12 col-sm-6 col-lg-3 mb-3">
            <div class="card h-100 d-flex align-items-stretched <?= ($message->view === true) ? 'bg-grey-transparent-2' : 'bg-yellow-transparent-3' ?>">
                <div class="card-header <?= ($message->view === true) ? 'bg-grey-transparent-4' : 'bg-yellow-transparent-5' ?>">
                    <?= __('Form: {0}', $message->form->name) ?>
                </div>
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-3"><?= __('From') ?></div>
                        <div class="col-9"><?= $message->from ?></div>
                    </div>
                    <h6><?= __('Message') ?>:</h6>
                    <?= h($message->message) ?>
                </div>
                <div class="card-footer <?= ($message->view === true) ? 'bg-grey-transparent-2' : 'bg-yellow-transparent-3' ?>">
                    <div class="row d-flex align-items-center">
                        <div class="col-6"><?= $this->Html->badge('<i class="far fa-clock"></i> ' . $this->Transform->date($message->created), 'light', ['title' => __('Created'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?></div>
                        <div class="col-6 text-right"><?= $this->Html->link(__('View details'), ['action' => 'view', $message->id], ['class' => 'btn btn-light']) ?></div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<?= $this->element('pagination'); ?>