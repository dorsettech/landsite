<?php $fieldsRow = ''; ?>
<?php foreach ($formFields as $fieldName => $fieldValue): ?>
    <?php if ($fieldName !== 'submit'): ?>
        <?php $fieldsRow .= '"'.$fieldName.'",'; ?>
    <?php endif; ?>
<?php endforeach; ?>
<?= $fieldsRow."\n" ?>
<?php $valuesRow = ''; ?>
<?php foreach ($formsData as $data): ?>
    <?php foreach ($data->values as $fieldName => $value): ?>
        <?php $valuesRow.='"'.$value.'",' ?>
    <?php endforeach; ?>
    <?= $valuesRow."\n" ?>
    <?php $valuesRow = ''; ?>
<?php endforeach; ?>


