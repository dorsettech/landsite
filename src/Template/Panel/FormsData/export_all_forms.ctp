<?php foreach ($forms as $form): ?>
    <?php $fieldsRow = '"form_name",'; ?>
    <?php foreach ($form->fields as $fieldName => $fieldValue): ?>
        <?php if ($fieldName !== 'submit'): ?>
            <?php $fieldsRow .= '"'.$fieldName.'",'; ?>
        <?php endif; ?>
    <?php endforeach; ?>
    <?= $fieldsRow."\n" ?>
    <?php $fieldsRow = '"form_name",'; ?>
    <?php $valuesRow = '"'.$form->name.'",'; ?>
    <?php foreach ($form->data as $data): ?>
        <?php foreach ($data->values as $fieldName => $value): ?>
            <?php $valuesRow.='"'.$value.'",' ?>
        <?php endforeach; ?>
        <?= $valuesRow."\n" ?>
        <?php $valuesRow = '"'.$form->name.'",'; ?>
    <?php endforeach; ?>
<?php endforeach; ?>


