<div class="row page-wrapper">
    <div class="col-12 col-sm-4 col-lg-2">

        <div class="card bg-yellow-transparent-1">
            <div class="card-block">
                <h4 class="card-title m-t-5 m-b-10"><?= __('Message Details') ?></h4>
                <div class="card-text">
                    <div class="row mb-3">
                        <div class="col-4"><?= __('Form') ?></div>
                        <div class="col-8"><?= $formsData->form->name ?></div>
                    </div>
                    <div class="row">
                        <div class="col-4"><?= __('Created') ?></div>
                        <div class="col-8"><?= $this->Html->badge('<i class="far fa-clock"></i> ' . $this->Transform->date($formsData->created), 'light') ?></div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-4"><?= __('Modified') ?></div>
                        <div class="col-8"><?= $this->Html->badge('<i class="fas fa-clock"></i> ' . $this->Transform->date($formsData->modified), 'light') ?></div>
                    </div>
                    <div class="row">
                        <div class="col-4"><?= __('IP') ?></div>
                        <div class="col-8"><?= $formsData->ip ?></div>
                    </div>
                    <div class="row">
                        <div class="col-4"><?= __('Host') ?></div>
                        <div class="col-8"><?= $formsData->host ?></div>
                    </div>
                    <div class="row">
                        <div class="col-12"><?= __('Referer') ?></div>
                        <div class="col-12"><?= $formsData->referer ?></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="col-12 col-sm-8 col-lg-10">

        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <?= $this->Html->link('<i class="fa fa-times"></i>', ['action' => 'listView', $formsData->id_forms], ['class' => 'btn btn-xs btn-icon btn-circle btn-danger', 'escape' => false]); ?>
                </div>
                <h4 class="panel-title"><?= __('Message Details') ?></h4>
            </div>
            <div class="panel-body">

            <?php foreach (json_decode($formsData->data) as $key => $value): ?>
                <?php if ($key !== 'submit'): ?>
                    <div class="row mb-2">
                        <div class="col-12 col-md-4 col-lg-3"><?= ucfirst($key) ?>:</div>
                        <div class="col-12 col-md-8 col-lg-9"><?= $value ?></div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>

            <?php if (isset($formsData->form_data_files) && !empty($formsData->form_data_files)): ?>
                <div class="row marg-t">
                    <div class="col-md-6 col-xs-12"><b>Attachments:</b></div>
                </div>
                <?php foreach ($formsData->form_data_files as $file): ?>
                    <?php if (trim($file->path) !== ''): ?>
                        <div class="col-md-1 col-sm-4 col-xs-6 marg-t">
                            <?php if (getimagesize($file->path_path)): ?>
                                <a href="<?= $file->path_path ?>" class="image-link">
                                    <div style="background-image:url(<?= $file->path_path ?>);background-size:cover;width:100%;height:100px;background-size: contain; background-position: center center; background-repeat: no-repeat;" ></div>
                                </a>
                            <?php else: ?>
                                <a href="<?= $file->path_path ?>"  target="_blank" style="display: block; height: 100px; text-align: center;">
                                    <i class="fa fa-file fa-3x" style="margin-top: 30px;"></i>
                                </a>
                            <?php endif; ?>
                            <h4 style="height: 40px;"><?= h($file->filename) ?></h4>
                            <a href="<?= $file->path_path ?>" download="<?= $file->filename ?>" class="btn btn-primary">Download</a>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>

            <div class="row action-buttons mt-4">
                <div class="col-12 col-sm-6 text-left"><?= $this->Html->link(__('Back'), ['action' => 'listView', $formsData->id_forms], ['class' => 'btn btn-warning']) ?></div>
            </div>

            </div>
        </div>

    </div>
</div>
