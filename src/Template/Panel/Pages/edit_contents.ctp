<?php

use Cake\Utility\Inflector;

?>
<?= $this->Form->create($page, ['type' => 'file', 'id' => 'page-contents']) ?>
<div class="page-wrapper contents">

    <?= $this->element('Structure/action-buttons', ['enableClose' => true, 'cancelUrl' => ['controller' => 'Menus', 'action' => 'view', $page->menu->urlname]]) ?>
    <ul class="nav nav-tabs" id="page-tabs">
        <?php if (!empty($layoutContents)) : ?>
            <li class="nav-item dropdown">
                <?= $this->Html->link(__('Contents'), '#contents', ['class' => 'nav-link dropdown-toggle active', 'data-toggle' => 'dropdown', 'role' => 'button', 'aria-haspopup' => 'true', 'aria-expanded' => 'false']) ?>
                <div class="dropdown-menu">
                    <?php $listIndex = 0; ?>
                    <?php foreach ($layoutContents as $layout) : ?>
                        <?= $this->Html->link(Inflector::humanize($layout[0]), '#tab-' . $layout[0] . '-content', ['class' => 'dropdown-item' . ($listIndex === 0 ? ' active' : ''), 'id' => 'tab-' . $layout[0], 'aria-controls' => 'tab-' . $layout[0] . '-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
                        <?php $listIndex++; ?>
                    <?php endforeach; ?>
                </div>
            </li>
        <?php endif; ?>
        <li class="nav-item">
            <?= $this->Html->link(__('Page'), '#tab-page-content', ['class' => 'nav-link settings' . (empty($layoutContents) ? ' active' : ''), 'id' => 'tab-page', 'aria-controls' => 'tab-page-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link(__('SEO'), '#tab-seo-content', ['class' => 'nav-link settings', 'id' => 'tab-seo', 'aria-controls' => 'tab-seo-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link(__('Redirection'), '#tab-redirection-content', ['class' => 'nav-link settings', 'id' => 'tab-redirection', 'aria-controls' => 'tab-redirection-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link(__('Page CSS'), '#tab-css-content', ['class' => 'nav-link settings', 'id' => 'tab-css', 'aria-controls' => 'tab-css-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
        </li>
    </ul>
    <div class="tab-content" id="tabs-content">
        <?php if (!empty($layoutContents)) : ?>
            <?php $contentIndex = 0; ?>
            <?php foreach ($layoutContents as $layout) : ?>
                <?php
                if (isset($contentsLayout[$layout[0]])) {
                    $content = $page->contents[$contentsLayout[$layout[0]]];
                } else {
                    $content = $emptyContent;
                }
                ?>
                <div class="tab-pane fade<?= $contentIndex === 0 ? ' show active' : '' ?>" id="tab-<?= $layout[0] ?>-content" role="tabpanel" aria-labelledby="tab-<?= $layout[0] ?>">
                    <h4 class="tab-title"><?= Inflector::humanize($layout[0]) ?></h4>
                    <div class="row">
                        <?= $this->Form->control('contents.' . $layout[0] . '.content_name', ['type' => 'hidden', 'val' => $layout[0]]); ?>
                        <?= $this->Form->control('contents.' . $layout[0] . '.position', ['type' => 'hidden', 'val' => $content->position]); ?>
                        <?php if (isset($layoutForms[$layout[0]])): ?>
                            <?php foreach ($layoutForms[$layout[0]] as $inputKey => $inputValue): ?>
                                <?php
                                switch ($inputKey):
                                    case 'file':
                                        ?>
                                        <div class="col-12">
                                            <?= $this->Form->control('contents.' . $layout[0] . '.file_image', ['type' => 'file', 'label' => $inputValue, 'button-label' => 'Select file']); ?>
                                        </div>
                                        <?php if (!empty($content->file)) : ?>
                                            <div class="col-12 col-sm-6 col-md-3">
                                                <?= (!empty($content->file) ? '<img src="' . $this->Images->scale($content->file, 80, 80, 'ratio_fill') . '">' : ''); ?>
                                            </div>
                                            <?php if (!empty($content->file)) : ?>
                                                <div class="col-12 col-sm-6 col-md-3">
                                                    <?= $this->Form->control('contents.' . $layout[0] . '.remove_image', ['type' => 'checkbox', 'label' => __('Delete main image')]); ?>
                                                    <?= $this->Html->link('<i class="fa fa-crop"></i> ' . __('Crop image'), ['controller' => 'Cropper', 'action' => 'crop', 'Contents', 'file', $content->id], ['class' => 'btn btn-default', 'title' => __('Crop image'), 'escape' => false]) ?>
                                                </div>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                        <?php break; ?>
                                    <?php case 'file2':
                                        ?>
                                        <div class="col-12">
                                            <?= $this->Form->control('contents.' . $layout[0] . '.file_image2', ['type' => 'file', 'label' => $inputValue, 'button-label' => __('Select file')]); ?>
                                        </div>
                                        <?php if (!empty($content->file2)) : ?>
                                            <div class="col-12 col-sm-6 col-md-3">
                                                <?= (!empty($content->file2) ? '<img src="' . $this->Images->scale($content->file2, 80, 80, 'ratio_fill') . '">' : ''); ?>
                                            </div>
                                            <?php if (!empty($content->file2)) : ?>
                                                <div class="col-12 col-sm-6 col-md-3">
                                                    <?= $this->Html->link('<i class="fa fa-crop"></i> ' . __('Crop image'), ['controller' => 'Cropper', 'action' => 'crop', 'Contents', 'file2', $content->id], ['class' => 'btn btn-default', 'title' => __('Crop image'), 'escape' => false]) ?>
                                                </div>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                        <?php break; ?>
                                    <?php case 'file3':
                                        ?>
                                        <div class="col-12">
                                            <?= $this->Form->control('contents.' . $layout[0] . '.file_image3', ['type' => 'file', 'label' => $inputValue, 'button-label' => __('Select file')]); ?>
                                        </div>
                                        <?php if (!empty($content->file3)) : ?>
                                            <div class="col-12 col-sm-6 col-md-3">
                                                <?= (!empty($content->file3) ? '<img src="' . $this->Images->scale($content->file3, 80, 80, 'ratio_fill') . '">' : ''); ?>
                                            </div>
                                            <?php if (!empty($content->file3)) : ?>
                                                <div class="col-12 col-sm-6 col-md-3">
                                                    <?= $this->Html->link('<i class="fa fa-crop"></i> ' . __('Crop image'), ['controller' => 'Cropper', 'action' => 'crop', 'Contents', 'file3', $content->id], ['class' => 'btn btn-default', 'title' => __('Crop image'), 'escape' => false]) ?>
                                                </div>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                        <?php break; ?>
                                    <?php case 'file4':
                                        ?>
                                        <div class="col-12">
                                            <?= $this->Form->control('contents.' . $layout[0] . '.file_image4', ['type' => 'file', 'label' => $inputValue, 'button-label' => __('Select file')]); ?>
                                        </div>
                                        <?php if (!empty($content->file4)) : ?>
                                            <div class="col-12 col-sm-6 col-md-3">
                                                <?= (!empty($content->file4) ? '<img src="' . $this->Images->scale($content->file4, 80, 80, 'ratio_fill') . '">' : ''); ?>
                                            </div>
                                            <?php if (!empty($content->file4)) : ?>
                                                <div class="col-12 col-sm-6 col-md-3">
                                                    <?= $this->Html->link('<i class="fa fa-crop"></i> ' . __('Crop image'), ['controller' => 'Cropper', 'action' => 'crop', 'Contents', 'file4', $content->id], ['class' => 'btn btn-default', 'title' => __('Crop image'), 'escape' => false]) ?>
                                                </div>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                        <?php break; ?>
                                    <?php case 'file5':
                                        ?>
                                        <div class="col-12">
                                            <?= $this->Form->control('contents.' . $layout[0] . '.file_image5', ['type' => 'file', 'label' => $inputValue, 'button-label' => __('Select file')]); ?>
                                        </div>
                                        <?php if (!empty($content->file5)): ?>
                                            <div class="col-12 col-sm-6 col-md-3">
                                                <?= (!empty($content->file5) ? '<img src="' . $this->Images->scale($content->file5, 80, 80, 'ratio_fill') . '">' : ''); ?>
                                            </div>
                                            <?php if (!empty($content->file5)) : ?>
                                                <div class="col-12 col-sm-6 col-md-3">
                                                    <?= $this->Html->link('<i class="fa fa-crop"></i> ' . __('Crop image'), ['controller' => 'Cropper', 'action' => 'crop', 'Contents', 'file5', $content->id], ['class' => 'btn btn-default', 'title' => __('Crop image'), 'escape' => false]) ?>
                                                </div>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                        <?php break; ?>
                                    <?php case 'title': ?>
                                        <div class="col-sm-12 "><?= $this->Form->control('contents.' . $layout[0] . '.title', ['label' => $inputValue, 'val' => $content->title]); ?></div>
                                        <?php break; ?>
                                    <?php case 'title2': ?>
                                        <div class="col-sm-12 "><?= $this->Form->control('contents.' . $layout[0] . '.title2', ['label' => $inputValue, 'val' => $content->title2]); ?></div>
                                        <?php break; ?>
                                    <?php case 'title3': ?>
                                        <div class="col-sm-12 "><?= $this->Form->control('contents.' . $layout[0] . '.title3', ['label' => $inputValue, 'val' => $content->title3]); ?></div>
                                        <?php break; ?>
                                    <?php case 'title4': ?>
                                        <div class="col-sm-12 "><?= $this->Form->control('contents.' . $layout[0] . '.title4', ['label' => $inputValue, 'val' => $content->title4]); ?></div>
                                        <?php break; ?>
                                    <?php case 'title5': ?>
                                        <div class="col-sm-12 "><?= $this->Form->control('contents.' . $layout[0] . '.title5', ['label' => $inputValue, 'val' => $content->title5]); ?></div>
                                        <?php break; ?>
                                    <?php case 'content': ?>
                                        <div class="col-sm-12">
                                            <?= $this->Form->control('contents.' . $layout[0] . '.content', ['type' => 'textarea', 'rows' => 20, 'label' => $inputValue, 'class' => 'ckeditor', 'val' => $content->content]); ?>
                                        </div>
                                        <?php break; ?>
                                    <?php case 'content2': ?>
                                        <div class="col-sm-12">
                                            <?= $this->Form->control('contents.' . $layout[0] . '.content2', ['type' => 'textarea', 'rows' => 20, 'label' => $inputValue, 'class' => 'ckeditor', 'val' => $content->content2]); ?>
                                        </div>
                                        <?php break; ?>
                                    <?php case 'content3': ?>
                                        <div class="col-sm-12">
                                            <?= $this->Form->control('contents.' . $layout[0] . '.content3', ['type' => 'textarea', 'rows' => 20, 'label' => $inputValue, 'class' => 'ckeditor', 'val' => $content->content3]); ?>
                                        </div>
                                        <?php break; ?>
                                    <?php case 'content4': ?>
                                        <div class="col-sm-12">
                                            <?= $this->Form->control('contents.' . $layout[0] . '.content4', ['type' => 'textarea', 'rows' => 20, 'label' => $inputValue, 'class' => 'ckeditor', 'val' => $content->content4]); ?>
                                        </div>
                                        <?php break; ?>
                                    <?php case 'content5': ?>
                                        <div class="col-sm-12">
                                            <?= $this->Form->control('contents.' . $layout[0] . '.content5', ['type' => 'textarea', 'rows' => 20, 'label' => $inputValue, 'class' => 'ckeditor', 'val' => $content->content5]); ?>
                                        </div>
                                        <?php break; ?>
                                    <?php case 'redirect_url': ?>
                                        <div class="col-12"><?= $this->Form->control('contents.' . $layout[0] . '.redirect_url', ['label' => $inputValue, 'val' => $content->redirect_url]); ?></div>
                                        <?php break; ?>
                                    <?php case 'redirect_url2': ?>
                                        <div class="col-12"><?= $this->Form->control('contents.' . $layout[0] . '.redirect_url2', ['label' => $inputValue, 'val' => $content->redirect_url2]); ?></div>
                                        <?php break; ?>
                                    <?php case 'redirect_url3': ?>
                                        <div class="col-12"><?= $this->Form->control('contents.' . $layout[0] . '.redirect_url3', ['label' => $inputValue, 'val' => $content->redirect_url3]); ?></div>
                                        <?php break; ?>
                                    <?php case 'redirect_url4': ?>
                                        <div class="col-12"><?= $this->Form->control('contents.' . $layout[0] . '.redirect_url4', ['label' => $inputValue, 'val' => $content->redirect_url4]); ?></div>
                                        <?php break; ?>
                                    <?php case 'redirect_url5': ?>
                                        <div class="col-12"><?= $this->Form->control('contents.' . $layout[0] . '.redirect_url5', ['label' => $inputValue, 'val' => $content->redirect_url5]); ?></div>
                                        <?php break; ?>
                                    <?php case 'gallery': ?>
                                        <div class="col-12">
                                            <label><?= $inputValue ?></label><br>
                                            <?php
                                            if (is_numeric($content->id)) {
                                                echo $this->Html->link(__('Manage Gallery'), ['controller' => 'Gallery', 'action' => 'manage', 'Contents', $content->id, 'gallery', 'Pages', 'editcontents', $page->id], ['class' => 'btn btn-primary']);
                                            }
                                            ?>
                                        </div>
                                        <?php break; ?>
                                <?php endswitch; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <div class="col-sm-3 col-md-3 col-12"><?= $this->Form->control('contents.' . $layout[0] . '.visible', ['type' => 'checkbox', 'label' => __('Content visible'), 'val' => $content->visible]); ?></div>
                    </div>
                </div>
                <?php $contentIndex++; ?>
            <?php endforeach; ?>
        <?php endif; ?>
        <div class="tab-pane fade<?= empty($layoutContents) ? ' show active' : '' ?>" id="tab-page-content" role="tabpanel" aria-labelledby="tab-page">
            <h4 class="tab-title"><?= __('Page') ?></h4>
            <div class="row">
                <div class="col-12"><?= $this->Form->control('name'); ?></div>
                <div class="col-12"><?= $this->Form->control('urlname'); ?></div>
                <div class="col-12">
                    <?= $this->Form->control('menu_icon', ['label' => __('Menu icon image'), 'type' => 'file', 'required' => false]); ?>
                    <?php if (!empty($page->menu_icon)): ?>
                        <img src="<?= $this->Images->scale($page->menu_icon_path, 50, 50) ?>" alt="" />
                    <?php endif; ?>
                </div>
                <div class="col-sm-3"><?= $this->Form->control('position'); ?></div>
                <div class="col-sm-3 d-flex align-items-center">
                    <?= $this->Form->control('active', ['type' => 'checkbox']); ?>
                </div>
                <div class="col-sm-3 d-flex align-items-center">
                    <?= $this->Form->control('in_menu', ['type' => 'checkbox', 'label' => __('Visible in menu')]); ?>
                </div>
                <?php if ($_user->is_root): ?>
                    <div class="col-sm-3 d-flex align-items-center">
                        <?= $this->Form->control('protected', ['type' => 'checkbox', 'label' => __('Page protected')]); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="tab-pane fade" id="tab-seo-content" role="tabpanel" aria-labelledby="tab-seo">
            <h4 class="tab-title"><?= __('SEO') ?></h4>
            <div class="row">
                <div class="col-sm-12"><?= $this->Form->control('seo_title', ['label' => ['text' => __('Title SEO'), 'escape' => false], 'id' => 'seo_title']); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('seo_keywords', ['label' => ['text' => __('Keywords SEO'), 'escape' => false], 'id' => 'seo_keywords']); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('seo_description', ['label' => ['text' => __('Description SEO'), 'escape' => false], 'id' => 'seo_description']); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('seo_priority', ['label' => __('Priority SEO'), 'min' => 0, 'max' => 1, 'step' => 0.1]); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('og_title', ['label' => ['text' => __('OG Title'), 'escape' => false], 'required' => false]); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('og_description', ['label' => ['text' => 'OG Description', 'escape' => false], 'required' => false]); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('og_image', ['label' => __('OG Image'), 'type' => 'file', 'required' => false]); ?></div>
                <div class="col-sm-12">
                    <?php if (!empty($page->og_image)): ?>
                        <strong><?= __('Current OG Image') ?></strong>
                        <img src="<?= $this->Images->scale($page->og_image_path, 100, 50, 'ratio_fill') ?>" alt="<?= $this->Images->alt($page->og_image) ?>" class="img-thumbnail" style='margin-bottom:20px;'>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="tab-redirection-content" role="tabpanel" aria-labelledby="tab-redirection">
            <h4 class="tab-title"><?= __('Redirection') ?></h4>
            <div class="row">
                <div class="col-md-9"><?= $this->Form->control('redirection', ['label' => __('Redirection (URL address)')]); ?></div>
                <div class="col-md-3 d-flex align-items-center"><?= $this->Form->control('target_blank', ['type' => 'checkbox', 'label' => __('Redirection in new window')]); ?></div>
            </div>
        </div>
        <div class="tab-pane fade" id="tab-css-content" role="tabpanel" aria-labelledby="tab-css">
            <h4 class="tab-title"><?= __('Page CSS') ?></h4>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <?= $this->Form->control('css', ['type' => 'textarea', 'label' => __('Page CSS'), 'class' => 'code-mirror', 'required' => false]); ?>
                </div>
            </div>
        </div>
    </div>
    <?= $this->element('Structure/action-buttons', ['enableClose' => true, 'cancelUrl' => ['controller' => 'Menus', 'action' => 'view', $page->menu->urlname]]) ?>

</div>
<?= $this->Form->end() ?>

<div class="page-wrapper details">
    <div class="btn-group" role="group">
        <?= $this->Form->button(__('Page Details'), ['type' => false, 'class' => 'btn btn-secondary']) ?>
        <?php if (!empty($page->menu)) : ?>
            <div class="btn-group" role="group">
                <?= $this->Form->button(h($page->menu->name), ['type' => false, 'id' => 'settings-menu', 'class' => 'btn btn-light dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false']) ?>
                <?php if ($menus->count() > 0) : ?>
                    <div class="dropdown-menu" aria-labelledby="settings-menu">
                        <?php foreach ($menus as $menuId => $menuName) : ?>
                            <?= $this->Form->postLink(h($menuName), ['controller' => 'Pages', 'action' => 'update-element-field', $page->id, 'menu', $menuId], ['class' => 'dropdown-item' . (($page->menu_id == $menuId) ? ' active' : '')]) ?>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <div class="btn-group" role="group">
            <?= $this->Form->button(__('Parent: {0}', (!empty($page->parent_page) ? h($page->parent_page->name) : 'none')), ['type' => false, 'id' => 'settings-parent', 'class' => 'btn btn-light dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false']) ?>
            <?php if ($parentPages->count() > 0) : ?>
                <div class="dropdown-menu" aria-labelledby="settings-parent">
                    <?= $this->Form->postLink(__('none'), ['controller' => 'Pages', 'action' => 'update-element-field', $page->id, 'page', null], ['class' => 'dropdown-item' . (($page->parent_id == 'null') ? ' active' : '')]) ?>
                    <?php foreach ($parentPages as $pageId => $pageName) : ?>
                        <?= $this->Form->postLink(h($pageName), ['controller' => 'Pages', 'action' => 'update-element-field', $page->id, 'page', $pageId], ['class' => 'dropdown-item' . (($page->parent_id == $pageId) ? ' active' : '')]) ?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="btn-group" role="group">
            <?= $this->Form->button(__('Template: {0}', h((isset($layouts[$page->layout])) ? $layouts[$page->layout] : $page->layout)), ['type' => false, 'id' => 'settings-template', 'class' => 'btn btn-light dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false']) ?>
            <?php if (!empty($layouts)) : ?>
                <div class="dropdown-menu" aria-labelledby="settings-template">
                    <?php foreach ($layouts as $layout => $layoutName) : ?>
                        <?= $this->Form->postLink(h($layoutName), ['controller' => 'Pages', 'action' => 'update-element-field', $page->id, 'layout', $layout], ['class' => 'dropdown-item' . (($page->layout == $layout) ? ' active' : '')]) ?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="btn-group" role="group">
            <?= $this->Form->button(__('Type of page: {0}', h((isset($types[$page->page_type_id])) ? $types[$page->page_type_id] : 'Type of page')), ['type' => false, 'id' => 'settings-type', 'class' => 'btn btn-light dropdown-toggle', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false']) ?>
            <?php if (!empty($types)) : ?>
                <div class="dropdown-menu" aria-labelledby="settings-type">
                    <?php foreach ($types as $type => $typeName) : ?>
                        <?= $this->Form->postlink(h($typeName), ['controller' => 'Pages', 'action' => 'update-element-field', $page->id, 'page_type_id', $type], ['class' => 'dropdown-item' . (($page->page_type_id == $type) ? ' active' : '')]) ?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <?= $this->Form->button('<i class="far fa-clock"></i> ' . $this->Transform->date($page->created), ['type' => false, 'class' => 'btn btn-light', 'escape' => false, 'title' => __('Created'), 'data-toggle' => 'tooltip', 'data-position' => 'top']) ?>
        <?= $this->Form->button('<i class="fas fa-clock"></i> ' . $this->Transform->date($page->modified), ['type' => false, 'class' => 'btn btn-light', 'escape' => false, 'title' => __('Modified'), 'data-toggle' => 'tooltip', 'data-position' => 'top']) ?>
        <?= $this->Html->link(__('Preview {0}', '<i class="fa fa-external-link-alt"></i>'), $this->Urlname->generate($page->urlname), ['class' => 'btn btn-info preview', 'target' => '_blank', 'escape' => false]) ?>
    </div>
</div>

<div class="page-wrapper files">

    <h4><?= __('Page Files') ?></h4>

    <ul class="nav nav-tabs" id="page-tabs">
        <li class="nav-item">
            <?= $this->Html->link(__('Sliders'), '#tab-sliders-content', ['class' => 'nav-link settings active', 'id' => 'tab-sliders', 'aria-controls' => 'tab-sliders-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link(__('Gallery'), '#tab-galleries-content', ['class' => 'nav-link settings', 'id' => 'tab-galleries', 'aria-controls' => 'tab-galleries', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link(__('Documents'), '#tab-documents-content', ['class' => 'nav-link settings', 'id' => 'tab-documents', 'aria-controls' => 'tab-documents-content', 'data-toggle' => 'pill', 'role' => 'tab']) ?>
        </li>
    </ul>
    <div class="tab-content" id="tabs-content">
        <div class="tab-pane fade show active" id="tab-sliders-content" role="tabpanel" aria-labelledby="tab-sliders">
            <?=
            $this->element('page-files', [
                'files'  => $slider,
                'type'   => 1,
                'pageId' => $page->id
            ])
            ?>
        </div>
        <div class="tab-pane fade" id="tab-galleries-content" role="tabpanel" aria-labelledby="tab-galleries">
            <?=
            $this->element('page-files', [
                'files'  => $galleries,
                'type'   => 2,
                'pageId' => $page->id
            ])
            ?>
        </div>
        <div class="tab-pane fade" id="tab-documents-content" role="tabpanel" aria-labelledby="tab-documents">
            <?=
            $this->element('page-files', [
                'files'  => $attachments,
                'type'   => 3,
                'pageId' => $page->id
            ])
            ?>
        </div>
    </div>

</div>
<?php
$this->Html->css('/plugins/codemirror/codemirror.css', ['block' => 'cssHead']);
$this->Html->script('/plugins/codemirror/codemirror.js', ['block' => 'scriptBottom']);
$this->Html->script('/plugins/codemirror/mode/css/css.js', ['block' => 'scriptBottom']);
?>