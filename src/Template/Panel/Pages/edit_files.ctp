<?php

use App\Library\PageFiles;

switch ($type) {
    case PageFiles::TYPE_ID_SLIDER:
        $title = __('Slider');
        break;
    case PageFiles::TYPE_ID_GALLERY:
        $title = __('Gallery');
        break;
    case PageFiles::TYPE_ID_DOCUMENT:
        $title = __('Documents');
        break;
}

?>
<?= $this->Form->create(null, ['url' => 'panel/PageFiles/addFilesToPage/' . $pageId . '/' . $type, 'type' => 'file', 'class' => 'form-article']) ?>
<div class="row form-group">
    <div class="col-xs-6 text-left"><?= $this->Html->link(__('Cancel'), ['action' => 'editcontents', $pageId], ['class' => 'btn btn-sm btn-warning']) ?></div>
    <div class="col-xs-6 text-right"><?= $this->Form->button(__('Save'), ['bootstrap-type' => 'primary', 'bootstrap-size' => 'sm']); ?></div>
</div>
<?= $this->Form->control('images[]', ['value' => '', 'multiple' => 'true', 'type' => 'file', 'label' => __('Add files to {0}', $title), 'button-label' => 'Select file(s)']); ?>
<hr>
<h4>Manage <?= $title ?></h4>
<?php if ($files->count()): ?>
    <div class="row">
        <?php foreach ($files as $file): ?>
            <div class="col-md-2 text-center ">
                <?php if ($file->type === PageFiles::TYPE_ID_DOCUMENT): ?>
                    <a href="<?= $file->path ?>" class="block"><i class="fa fa-3x fa-file-pdf-o"></i></a>
                <?php else: ?>
                    <a href="<?= $file->path ?>" class="image-link">
                        <img src="<?= $this->Images->scale($file->path, 300, 300, 'ratio_fill') ?>" alt="<?= $this->Images->alt($file->path) ?>" class="img-responsive">
                    </a>
                <?php endif; ?>
                <div class="clearfix form-group">
                    <strong><?= __('Name') ?>:</strong>
                    <?= $file->name ?><br>
                    <strong><?= __('Position') ?>:</strong>
                    <?= $file->position ?><br>
                    <strong><?= __('Active') ?>:</strong>
                    <?= $file->active ? 'yes' : 'no' ?>
                </div>
                <?= $this->Html->link(__('Edit'), ['controller' => 'PageFiles', 'action' => 'edit', $file->id], ['class' => 'btn btn-sm btn-primary']) ?>
                <?= $this->Html->link(__('Delete'), ['controller' => 'PageFiles', 'action' => 'delete', $file->id], ['class' => 'btn btn-sm btn-danger']) ?>
            </div>
        <?php endforeach; ?>
    </div>
<?php else: ?>
    <?= __('There are no files for {0} yet.', $title) ?>
<?php endif; ?> 
<hr>
<div class="row">
    <div class="col-xs-6 text-left"><?= $this->Html->link(__('Cancel'), ['action' => 'editcontents', $pageId], ['class' => 'btn btn-sm btn-warning']) ?></div>
    <div class="col-xs-6 text-right"><?= $this->Form->button(__('Save'), ['bootstrap-type' => 'primary', 'bootstrap-size' => 'sm']); ?></div>
</div>
<?= $this->Form->end() ?>
