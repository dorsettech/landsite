<?= $this->Form->create($page, ['type' => 'file']) ?>
<div class="page-wrapper">
    <?= $this->element('Structure/action-buttons') ?>
    <ul class="nav nav-tabs" id="page-tabs" role="tablist">
        <li class="nav-item">
            <?= $this->Html->link(__('Page'), '#tab-page-content', ['class' => 'nav-link settings' . (empty($layoutContents) ? ' active' : ''), 'id' => 'tab-page', 'aria-controls' => 'tab-page-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link(__('SEO'), '#tab-seo-content', ['class' => 'nav-link settings', 'id' => 'tab-seo', 'aria-controls' => 'tab-seo-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link(__('Redirection'), '#tab-redirection-content', ['class' => 'nav-link settings', 'id' => 'tab-redirection', 'aria-controls' => 'tab-redirection-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
        </li>
    </ul>
    <div class="tab-content" id="tabs-content">
        <div class="tab-pane fade<?= empty($layoutContents) ? ' show active' : '' ?>" id="tab-page-content" role="tabpanel" aria-labelledby="tab-page">
            <div class="row">
                <div class="col-12"><?= $this->Form->control('name'); ?></div>
                <div class="col-12"><?= $this->Form->control('urlname'); ?></div>
                <div class="col-12">
                    <?= $this->Form->control('menu_icon', ['label' => __('Menu icon image'), 'type' => 'file', 'required' => false]); ?>
                    <?php if (!empty($page->menu_icon)): ?>
                        <img src="<?= $this->Images->scale($page->menu_icon_path, 50, 50) ?>" alt="" />
                    <?php endif; ?>
                </div>
                <div class="col-md-6 col-lg-3"><?= $this->Form->control('position'); ?></div>
                <div class="col-md-6 col-lg-3 d-flex align-items-center">
                    <?= $this->Form->control('active', ['type' => 'checkbox']); ?>
                </div>
                <div class="col-md-6 col-lg-3 d-flex align-items-center">
                    <?= $this->Form->control('in_menu', ['type' => 'checkbox', 'label' => __('Visible in menu')]); ?>
                </div>
                <?php if ($_user->is_root): ?>
                    <div class="col-md-6 col-lg-3 d-flex align-items-center">
                        <?= $this->Form->control('protected', ['type' => 'checkbox', 'label' => __('Page protected')]); ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-3"><?= $this->Form->control('menu_id', ['label' => __('Menu'), 'options' => $menus, 'default' => $currentMenu, 'class' => 'select-menu', 'data-model' => 'pages']); ?></div>
                <div class="col-md-6 col-lg-3"><?= $this->Form->control('parent_id', ['label' => __('Parent element'), 'options' => $parentPages, 'empty' => true, 'class' => 'pages']); ?></div>
                <div class="col-md-6 col-lg-3"><?= $this->Form->control('layout', ['label' => __('Template'), 'options' => $layouts, 'empty' => false]); ?></div>
                <div class="col-md-6 col-lg-3"><?= $this->Form->control('page_type_id', ['label' => __('Type of page'), 'options' => $types, 'empty' => false, 'class' => 'select-pagetype']); ?></div>
            </div>
        </div>
        <div class="tab-pane fade" id="tab-seo-content" role="tabpanel" aria-labelledby="tab-seo">
            <div class="row">
                <div class="col-sm-12"><?= $this->Form->control('seo_title', ['label' => ['text' => __('Title SEO'), 'escape' => false], 'id' => 'seo_title']); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('seo_keywords', ['label' => ['text' => __('Keywords SEO'), 'escape' => false], 'id' => 'seo_keywords']); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('seo_description', ['label' => ['text' => __('Description SEO'), 'escape' => false], 'id' => 'seo_description']); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('seo_priority', ['label' => __('Priority SEO'), 'min' => 0, 'max' => 1, 'step' => 0.1]); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('og_title', ['label' => ['text' => __('OG Title'), 'escape' => false], 'required' => false]); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('og_description', ['label' => ['text' => 'OG Description', 'escape' => false], 'required' => false]); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('og_image', ['label' => __('OG Image'), 'type' => 'file', 'required' => false]); ?></div>
                <div class="col-sm-12">
                    <?php if (!empty($page->og_image)): ?>
                        <strong><?= __('Current OG Image') ?></strong>
                        <img src="<?= $this->Images->scale($page->og_image_path, 100, 50, 'ratio_fill') ?>" alt="<?= $this->Images->alt($page->og_image) ?>" class="img-thumbnail" style='margin-bottom:20px;'>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="tab-redirection-content" role="tabpanel" aria-labelledby="tab-redirection">
            <div class="row">
                <div class="col-md-9"><?= $this->Form->control('redirection', ['label' => __('Redirection (URL address)')]); ?></div>
                <div class="col-md-3 d-flex align-items-center"><?= $this->Form->control('target_blank', ['type' => 'checkbox', 'label' => __('Redirection in new window')]); ?></div>
            </div>
        </div>
    </div>
    <?= $this->element('Structure/action-buttons') ?>

</div>
<?= $this->Form->end() ?>
