<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name', ['name' => 'Name']) ?><br><?= $this->Paginator->sort('urlname', ['name' => 'Page URL']) ?></th>
                <th><?= $this->Paginator->sort('parent_id', ['name' => 'Parent element']) ?></th>
                <th><?= $this->Paginator->sort('menu_id', ['name' => 'Menu']) ?></th>
                <th><?= $this->Paginator->sort('layout', ['name' => 'Template']) ?></th>
                <th><?= $this->Paginator->sort('type', ['name' => 'Type']) ?></th>
                <?php if ($_user['is_root']) : ?><th><?= $this->Paginator->sort('ip', ['name' => 'IP']) ?></th><?php endif; ?>
                <?php if ($_user['is_root']) : ?><th><?= $this->Paginator->sort('depth', ['name' => 'Depth']) ?></th><?php endif; ?>
                <th><?= $this->Paginator->sort('in_menu', ['name' => 'Visible in menu']) ?></th>
                <th><?= $this->Paginator->sort('active', ['name' => 'Active']) ?></th>
                <th><?= $this->Paginator->sort('created', ['name' => 'Created']) ?> / <?= $this->Paginator->sort('modified', ['name' => 'Modified']) ?></th>
                <th class="actions"><?= 'Actions' ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
                $i = 0;
                foreach ($pages as $page):
                $i++;
            ?>
                <tr>
                    <td><?= $this->Number->format($page->id) ?></td>
                    <td>
                        <?= $this->Html->link(h($page->name), ['action' => 'edit', $page->id], ['escape' => false]) ?><br>
                        <small><?= h($page->urlname) ?></small>
                    </td>
                    <td><?= $page->has('parent_page') ? h($page->parent_page->name) : '' ?></td>
                    <td><?= $page->has('menu') ? $this->Html->link(h($page->menu->name), ['controller' => 'Menus', 'action' => 'view', $page->menu->urlname]) : '' ?></td>
                    <td><?= h($page->layout) ?></td>
                    <td><?= h($page->page_type->title) ?></td>
                    <?php if ($_user['is_root']) : ?><td><?= h($page->ip) ?></td><?php endif; ?>
                    <?php if ($_user['is_root']) : ?><td><?= $this->Number->format($page->depth) ?></td><?php endif; ?>
                    <td><?= $this->Switcher->create('in_menu', ['val' => $page->in_menu, 'data-id' => $page->id]); ?></td>
                    <td><?= $this->Switcher->create('active', ['val' => $page->active, 'data-id' => $page->id]); ?></td>
                    <td>
                        <?= $this->Html->badge('<i class="far fa-clock"></i> ' . h($this->Transform->date($page->created)), 'light') ?><br>
                        <?= $this->Html->badge('<i class="fa fa-clock"></i> ' . h($this->Transform->date($page->modified)), 'light') ?>
                    </td>
                    <td class="actions">
                        <div class="btn-group btn-group-xs">
                            <?= $this->Html->link('<i class="fa fa-external-link-alt"></i>', $this->Urlname->generate($page->urlname), ['class' => 'btn btn-default preview', 'escape' => false, 'target' => '_blank', 'title' => __('Preview page'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            <?= $this->Html->link('<i class="fa fa-clone"></i>', ['action' => 'clonePage', $page->id], ['class' => 'btn btn-default', 'escape' => false, 'title' => __('Clone page'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit-contents', $page->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Edit page'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            <?php if (!$page->protected) : ?>
                                <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $page->id], ['class' => 'btn btn-danger', 'confirm' => 'Please confirm removal of page '.h($page->name), 'escape' => false, 'title' => __('Delete page'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            <?php endif; ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?= $this->element('Structure/add-button') ?>
<?= $this->element('pagination') ?>