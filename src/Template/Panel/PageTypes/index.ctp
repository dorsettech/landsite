<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('title') ?></th>
                <th><?= $this->Paginator->sort('model') ?></th>
                <th><?= $this->Paginator->sort('menu') ?></th>
                <th><?= $this->Paginator->sort('sitemap') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pageTypes as $pageType): ?>
                <tr>
                    <td><?= $this->Number->format($pageType->id) ?></td>
                    <td><?= h($pageType->title) ?></td>
                    <td><?= h($pageType->model) ?></td>
                    <td><?= h($pageType->menu) ?></td>
                    <td><?= h($pageType->sitemap) ?></td>
                    <td class="actions">
                        <div class="btn-group btn-group-xs">
                            <?= $this->Html->link('<i class="fa fa-eye"></i>', ['action' => 'view', $pageType->id], ['class' => 'btn btn-default', 'escape' => false]) ?>
                            <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $pageType->id], ['class' => 'btn btn-success', 'escape' => false]) ?>
                            <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $pageType->id], ['class' => 'btn btn-danger', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $pageType->id)]) ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?= $this->element('Structure/add-button') ?>
    <div class="paginator text-center">
        <?= $this->Paginator->numbers() ?>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
