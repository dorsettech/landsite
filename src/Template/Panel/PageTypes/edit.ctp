<div class="pageTypes form large-9 medium-8 columns content">
        <?= $this->Form->create($pageType) ?><fieldset>
        <?php
        echo $this->Form->control('title');
        echo $this->Form->control('model');
        echo $this->Form->control('menu');
        echo $this->Form->control('sitemap');
        ?>
    </fieldset>
    <div class="row">
        <div class="col-xs-6 text-left"><?= $this->Html->link(__('Cancel'), ['action' => 'index'], ['class' => 'btn btn-warning']) ?></div>
        <div class="col-xs-6 text-right"><?= $this->Form->button(__('Save'), ['bootstrap-type' => 'primary']); ?></div>
    </div>
    <?= $this->Form->end() ?>
</div>
