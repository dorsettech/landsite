<?= $this->Form->create($channel, ['type' => 'file']); ?>
<div class="page-wrapper">
    <?= $this->element('Structure/action-buttons') ?>
    <ul class="nav nav-tabs" id="page-tabs" role="tablist">
        <li class="nav-item">
            <?= $this->Html->link(__('Page'), '#tab-page-content', ['class' => 'nav-link settings active', 'id' => 'tab-page', 'aria-controls' => 'tab-page-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link(__('Adverts'), '#tab-adverts-content', ['class' => 'nav-link settings', 'id' => 'tab-redirection', 'aria-controls' => 'tab-adverts-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link(__('SEO'), '#tab-seo-content', ['class' => 'nav-link settings', 'id' => 'tab-seo', 'aria-controls' => 'tab-seo-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
        </li>
        <li class="nav-item">
            <?= $this->Html->link(__('CSS'), '#tab-css-content', ['class' => 'nav-link settings', 'id' => 'tab-seo', 'aria-controls' => 'tab-css-content', 'data-toggle' => 'tab', 'role' => 'tab']) ?>
        </li>
    </ul>
    <div class="tab-content" id="tabs-content">
        <div class="tab-pane fade show active" id="tab-page-content" role="tabpanel" aria-labelledby="tab-seo">
            <h4 class="tab-title"><?= __('Content') ?></h4>
            <div class="row">
                <div class="col-sm-6"><?= $this->Form->control('name'); ?></div>
                <div class="col-sm-6"><?= $this->Form->control('label'); ?></div>
                <div class="col-sm-6"><?= $this->Form->control('slug'); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('description', ['class' => 'ckeditor']); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('description2', ['class' => 'ckeditor']); ?></div>
                <div class="col-sm-8"><?= $this->Form->control('image', ['type' => 'file', 'required' => false]); ?></div>
                <?php if (!empty($channel->image)): ?>
                    <div class="col-sm-4">
                        <div class="pull-left mr-sm-2">
                            <h6><?= __('Current image') ?></h6>
                            <img src="<?= $this->Images->scale($channel->image_path, 60, 60) ?>" class="img-responsive m-b-20">
                        </div>
                        <div class="pull-left">
                            <label class="d-block">&nbsp;</label>
                            <?= $this->Form->control('image-clear', ['type' => 'checkbox', 'label' => __('Delete image')]); ?>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="col-sm-6 col-lg-2"><?= $this->Form->control('position'); ?></div>
                <div class="col-sm-6 col-lg-2 d-flex align-items-center"><?= $this->Form->control('active'); ?></div>
            </div>
        </div>

        <div class="tab-pane fade" id="tab-adverts-content" role="tabpanel" aria-labelledby="tab-seo">
            <h4 class="tab-title"><?= __('Adverts') ?></h4>
            <div class="row">
                <div class="col-sm-12"><?= $this->Form->control('sponsors', ['class' => 'ckeditor']); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('advert1', ['class' => 'ckeditor']); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('advert2', ['class' => 'ckeditor']); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('advert3', ['class' => 'ckeditor']); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('advert4', ['class' => 'ckeditor']); ?></div>
            </div>
        </div>

        <div class="tab-pane fade" id="tab-seo-content" role="tabpanel" aria-labelledby="tab-seo">
            <h4 class="tab-title"><?= __('SEO') ?></h4>
            <div class="row">
                <div class="col-sm-12"><?= $this->Form->control('seo_title', ['label' => ['text' => __('Title SEO'), 'escape' => false], 'id' => 'seo_title']); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('seo_keywords', ['label' => ['text' => __('Keywords SEO'), 'escape' => false], 'id' => 'seo_keywords']); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('seo_description', ['label' => ['text' => __('Description SEO'), 'escape' => false], 'id' => 'seo_description']); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('seo_priority', ['label' => __('Priority SEO'), 'min' => 0, 'max' => 1, 'step' => 0.1]); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('og_title', ['label' => ['text' => __('OG Title'), 'escape' => false], 'required' => false]); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('og_description', ['label' => ['text' => 'OG Description', 'escape' => false], 'required' => false]); ?></div>
                <div class="col-sm-12"><?= $this->Form->control('og_image', ['label' => __('OG Image'), 'type' => 'file', 'required' => false]); ?></div>
                <div class="col-sm-12">
                    <?php if (!empty($channel->og_image)): ?>
                        <strong><?= __('Current OG Image') ?></strong>
                        <img src="<?= $this->Images->scale($channel->og_image_path, 100, 50, 'ratio_fill') ?>" alt="<?= $this->Images->alt($channel->og_image) ?>" class="img-thumbnail" style='margin-bottom:20px;'>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="tab-css-content" role="tabpanel" aria-labelledby="tab-css">
            <h4 class="tab-title"><?= __('CSS') ?></h4>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <?= $this->Form->control('css', ['type' => 'textarea', 'label' => __('Page CSS'), 'class' => 'code-mirror', 'required' => false]); ?>
                </div>
            </div>
        </div>
    </div>

    <?= $this->element('Structure/action-buttons') ?>
</div>
<?= $this->Form->end() ?>
<?php
$this->Html->css('/plugins/codemirror/codemirror.css', ['block' => 'cssHead']);
$this->Html->script('/plugins/codemirror/codemirror.js', ['block' => 'scriptBottom']);
$this->Html->script('/plugins/codemirror/mode/css/css.js', ['block' => 'scriptBottom']);
?>
