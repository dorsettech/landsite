<?= $this->Form->create(null, ['type' => 'file', 'url' => "panel/Gallery/manage/$model/$entityId/$field"]) ?>
<?= $this->Form->control('images[]', ['type' => 'file', 'multiple' => true, 'label' => __('Add images to gallery')]) ?>

<?php if (!empty($files)): ?>
    <div id="galleryForm">
        <div class="row" id="sortable-view">
            <?php foreach ($files as $key => $image): ?>
                <div class="col-sm-2 gallery-item marg-b-sm" data-key="<?= $key ?>">
                    <div style="background:url(<?= $image['path'] ?>) center center no-repeat; background-size: cover; height: 200px; width: 100%; position: relative">
                        <div style="position: absolute; right: 0; bottom: 0;">
                            <a class="btn btn-default editSingleImage">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a class="btn btn-danger deleteSingleImage">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                        <input type="hidden" name="position[<?= $key ?>]" value="<?= $key ?>">
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php else: ?>
    <div class="alert alert-danger text-center">
        <?= __('There are no images yet.') ?>
    </div>
<?php endif; ?>
<div class="clearfix">
    <?= $this->Html->link(__('Back'), ['controller' => $model, 'action' => !empty($action) ? $action : 'edit', !empty($editId) ? $editId : $entityId], ['class' => 'btn btn-warning pull-left']) ?>
    <?= $this->Form->submit(__('Add images'), ['class' => 'btn btn-primary pull-right']) ?>
</div>
<?= $this->Form->end() ?>
<div class="modal fade" id="editImageModal" tabindex="-1" role="dialog" aria-labelledby="editImageModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= __('Edit image') ?></h4>
            </div>
            <div class="modal-body">
                <div class="image-handler marg-b-sm" style="background-repeat: no-repeat; background-position: center center; background-size: contain; height: 200px; width: 100%;"></div>
                <form id="updateSingleImageForm">
                    <div class="form-group">
                        <label><?= __('Image title attribute') ?></label>
                        <input class="form-control" type="text" name="title">
                    </div>
                    <div class="form-group">
                        <label><?= __('Image alt attribute') ?></label>
                        <input class="form-control" type="text" name="alt">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
                <button type="button" class="btn btn-primary" id="saveSingleImage"><?= __('Save changes') ?></button>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('/backend/js/gallery'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        Gallery._model = '<?= $model ?>';
        Gallery._entityId = '<?= $entityId ?>';
        Gallery._field = '<?= $field ?>';
    });
</script>
