<?= $this->Form->create($article, ['type' => 'file']); ?>
<?= $this->element('Structure/action-buttons') ?>
<hr>
<div class="row">
    <div class="col-sm-2"><?= $this->Form->control('author_id', ['options' => $users]); ?></div>
    <div class="col-sm-2"><?= $this->Form->control('category_id', ['options' => $categories]); ?></div>
    <div class="col-sm-2"><?= $this->Form->control('channel_id', ['options' => $channels, 'required' => false, 'empty' => 'Select Channel']); ?></div>
    <div class="col-sm-3"><?= $this->Form->control('title'); ?></div>
    <div class="col-sm-3"><?= $this->Form->control('slug'); ?></div>
    <div class="col-sm-12"><?= $this->Form->control('headline', ['class' => 'ckeditor']); ?></div>
    <div class="col-sm-12"><?= $this->Form->control('body', ['class' => 'ckeditor']); ?></div>
    <div class="col-sm-4"><?= $this->Form->control('image', ['type' => 'file', 'required' => false]); ?></div>
    <div class="col-sm-2"><?= $this->Form->control('publish_date', ['type' => 'text', 'empty' => true, 'class' => 'datetime']); ?></div>
    <div class="col-sm-2"><?= $this->Form->control('status', ['type' => 'select', 'options' => $articleStatuses]); ?></div>
    <div class="col-sm-4 d-flex align-items-center"><?= $this->Form->control('visibility'); ?></div>
</div>
<hr>
<div class="row">
    <div class="col-12"><?= $this->Form->control('meta_title'); ?></div>
    <div class="col-12"><?= $this->Form->control('meta_description'); ?></div>
    <div class="col-12"><?= $this->Form->control('meta_keywords'); ?></div>
</div>
<hr>
<?= $this->element('Structure/action-buttons') ?>
<?= $this->Form->end() ?>
