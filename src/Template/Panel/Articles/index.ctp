<?php

use App\Model\Enum\State;

?>
<?php if ($articles->count() > 0) : ?>
    <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('title') ?></th>
                    <th><?= $this->Paginator->sort('image') ?></th>
                    <th><?= $this->Paginator->sort('status') ?></th>
                    <th><?= $this->Paginator->sort('state') ?></th>
                    <th><?= $this->Paginator->sort('author_id', ['label' => __('Posted by')]) ?></th>
                    <th><?= $this->Paginator->sort('category_id') ?></th>
                    <th><?= $this->Paginator->sort('visibility') ?></th>
                    <th><?= $this->Paginator->sort('publish_date') ?></th>
                    <th><?= $this->Paginator->sort('created', ['name' => 'Created']) ?> / <?= $this->Paginator->sort('modified', ['name' => 'Modified']) ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($articles as $article): ?>
                    <tr>
                        <td><?= $this->Number->format($article->id) ?></td>
                        <td><?= h($article->title) ?></td>
                        <td>
                            <?php if (!empty($article->image)): ?>
                                <?= $this->Html->link($this->Html->image('/' . $this->Images->scale($article->image_path, 30, 30), ['class' => 'img-thumbnail']), $article->image_path, ['class' => 'magnific-image', 'escape' => false]) ?>
                            <?php endif; ?>
                        </td>
                        <td><?= h($article->status) ?></td>
                        <td><?= h($article->state) ?></td>
                        <td>
                            <?php if ($article->has('user')) : ?>
                                <?php if ($article->user->group_id !== 6): ?>
                                    <?= __('The Landsite') ?>
                                <?php elseif ($article->user->has('service')): ?>
                                    <?= $article->user->service->company ?>
                                <?php else: ?>
                                    <?= $article->user->full_name ?>
                                <?php endif; ?>
                            <?php else: ?>
                                <?= '' ?>
                            <?php endif; ?>
                        </td>
                        <td><?= $article->has('category') ? $article->category->name : '' ?></td>
                        <td>
                            <?= $this->Switcher->create('visibility', ['val' => $article->visibility, 'data-id' => $article->id, 'data-model' => 'Articles']); ?>
                        </td>
                        <td><?= h($this->Transform->date($article->publish_date)) ?></td>
                        <td>
                            <?= $this->Html->badge('<i class="far fa-clock"></i> ' . h($this->Transform->date($article->created)), 'light') ?><br>
                            <?= $this->Html->badge('<i class="fa fa-clock"></i> ' . h($this->Transform->date($article->modified)), 'light') ?>
                        </td>

                        <td class="actions">
                            <div class="btn-group btn-group-xs">
                                <?php if ($article->state === State::PENDING): ?>
                                    <?= $this->Html->link('<i class="fa fa-check-circle"></i>', '#', ['class' => 'btn btn-default approve-reject-button', 'escape' => false, 'title' => __('Approve/Reject'), 'data-toggle' => 'modal', 'data-target' => '#approve-reject-modal', 'data-id' => $article->id]) ?>
                                <?php endif; ?>
                                <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $article->id], ['class' => 'btn btn-primary', 'escape' => false, 'title' => __('Edit'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                                <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $article->id], ['class' => 'btn btn-danger', 'confirm' => __('Please confirm removal Article #{0}', $article->id), 'escape' => false, 'title' => __('Delete'), 'data-toggle' => 'tooltip', 'data-placement' => 'top']) ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php else : ?>
    <div class="well text-center text-warning"><?= __('There are no Articles yet.') ?> <?= $this->Html->link(__('Add first Article'), ['action' => 'add']) ?></div>
<?php endif; ?>
<div class="row action-buttons">
    <div class="col text-right">
        <?= $this->Html->link(__('Add Article'), ['action' => 'add'], ['class' => 'btn btn-primary']); ?>
    </div>
</div>
<?= $this->element('pagination') ?>
<?= $this->element('Structure/approve-reject-modal') ?>
