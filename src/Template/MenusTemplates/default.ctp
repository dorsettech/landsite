<?php if (!empty($menu->pages)) : ?>
    <?= $this->Menu->generateDefault($menu->pages, ['className' => 'nav navbar-nav']); ?>
<?php else : ?>
    <div class="alert alert-danger text-center marg-b"><?= __('There are no menu elements.') ?></div>
<?php endif; ?>