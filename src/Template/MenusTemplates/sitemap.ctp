<?php if (!empty($menu->pages)) : ?>
    <div class="dd nestable" id="menu-page">
        <?= $this->Menu->generateDefault($menu->pages, ['className' => 'list-none ahref fa', 'noParent' => false]); ?>
    </div>
<?php else : ?>
    <div><?= __('No page elements.') ?></div>
<?php endif; ?>