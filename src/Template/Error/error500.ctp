<?php

use Cake\Core\Configure;
use Cake\Error\Debugger;

$meta['title'] = __d('cake', 'Error') . ' ' . $code . ': ' . $message;
$this->set('meta', $meta);

?>
<div class="error">
    <div class="error-code animated fadeIn"><?= $code ?></div>
    <div class="error-content animated fadeInUp">
        <div class="error-message"><?= __d('cake', 'An Internal Error Has Occurred') ?></div>
        <div class="error-desc m-b-30">
            <p><?= __d('cake', 'The server encountered something unexpected that didn\'t allow it to complete the request. We apologize.') ?></p>
            <p class="error-details">
                <strong><?= __d('cake', 'Error') ?> <?= $code ?>:</strong> <?= h($message) ?>
            </p>
        </div>
    </div>
</div>

<?php if (Configure::read('debug')): ?>
    <?php
    /**
     * Error message
     */
    $this->start('errorMessage');
    ?>
    <h2><?= h($error->getMessage()) ?></h2>
    <div class="location"><?= h($error->getFile()) ?>, <?= __d('cake', 'line') ?> <?= h($error->getLine()) ?></div>
    <?php
    $this->end();

    /**
     * Error trace
     */
    $this->start('errorTrace');
    ?>
    <ul class="list-group">
        <li class="list-group-item h4"><?= __d('cake', 'Stack trace') ?></li>
        <?php foreach ($error->getTrace() as $i => $stack): ?>
            <li class="list-group-item">
                <?php if (isset($stack['function'])): ?>
                    <?php if (isset($stack['class'])): ?>
                        <div class="stack-function h5"><?= h($stack['class'] . $stack['type'] . $stack['function']) ?></div>
                    <?php else: ?>
                        <div class="stack-function"><?= h($stack['function']) ?></div>
                    <?php endif; ?>
                    <div class="stack-file">
                        <?php if (isset($stack['file'], $stack['line'])): ?>
                            <?= h(Debugger::trimPath($stack['file'])) ?>, <?= __d('cake', 'line') ?> <?= $stack['line'] ?>
                        <?php else: ?>
                            [internal function]
                        <?php endif ?>
                    </div>
                <?php else: ?>
                    [internal function]
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
    </ul>
    <?php
    $this->end();

    /**
     * Details view block
     */
    $this->start('errorInfo');
    ?>
    <?php if (!empty($error->queryString)) : ?>
        <div class="sql-query">
            <strong><?= __d('cake', 'SQL Query') ?>: </strong>
            <?= h($error->queryString) ?>
        </div>
    <?php endif; ?>
    <?php if (!empty($error->params)) : ?>
        <strong><?= __d('cake', 'SQL Query Params') ?>: </strong>
        <?= Debugger::dump($error->params) ?>
    <?php endif; ?>
    <?php
    echo $this->element('auto_table_warning');

    if (extension_loaded('xdebug')):
        xdebug_print_function_stack();
    endif;
    ?>
    <?php
    $this->end();
    ?>
<?php endif; ?>
