<?php

use App\Library\Search;

?>
<?= $this->Form->create(null, ['type' => 'get', 'url' => ['_name' => 'professional-services-listing']]) ?>
<div class="form-row justify-content-center align-items-center">
    <div class="col-12 col-sm-5 col-lg-3 col-xxl">
        <?= $this->Form->control('service_category', ['type' => 'select', 'placeholder' => __('Professional Type'), 'empty' => __('Professional Type'), 'default' => h($data['service_category'] ?? ''), 'options' => $categories, 'class' => 'custom-select', 'label' => false]) ?>
    </div>
    <div class="col-12 col-sm-7 col-lg-3 col-xxl">
        <?= $this->Form->control('keyword', ['placeholder' => __('Search by keyword...'), 'val' => h($data['keyword'] ?? ''), 'class' => 'form-control', 'id' => 'validation02', 'label' => false]) ?>
    </div>
    <div class="col-12 col-sm-7 col-lg-3 col-xxl">
        <?= $this->Form->control('location', ['placeholder' => __('Location'), 'val' => h($data['location'] ?? ''), 'class' => 'form-control', 'id' => 'validation03', 'label' => false]) ?>
    </div>
    <div class="col-12 col-sm-5 col-lg-3 col-xxl-2">
        <?= $this->Form->control('range', ['type' => 'select', 'placeholder' => __('Range'), 'default' => h($data['range'] ?? ''), 'empty' => __('Range'), 'options' => Search::RANGES, 'class' => 'custom-select', 'label' => false]) ?>
    </div>
    <div class="form-group col-auto order-1 order-lg-2 order-xxl-0">
        <?= $this->Form->button('Search professionals', ['type' => 'submit', 'class' => 'btn btn-primary']) ?>
    </div>
</div>
<?= $this->Form->end(); ?>
