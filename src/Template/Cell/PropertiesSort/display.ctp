<?= $this->Form->create(null, ['url' => $sortUrl, 'type' => 'get', 'class' => 'form-sort']) ?>
<?php foreach ($sortQuery as $key => $value) : ?>
    <?php if (is_array($value)) : ?>
        <?php foreach ($value as $k => $v) : ?>
        <?= $this->Form->control($key . '.' . $k, ['type' => 'hidden', 'value' => $v]) ?>
        <?php endforeach; ?>
    <?php elseif ($key != 'sort') : ?>
        <?= $this->Form->control($key, ['type' => 'hidden', 'value' => $value]) ?>
    <?php endif; ?>
<?php endforeach; ?>
<div class="form-row">
    <?= $this->Form->control('sort', ['type' => 'select', 'label' => false, 'placeholder' => __('Sort by'), 'options' => $sortOptions, 'default' => h($sortQuery['sort'] ?? ''), 'class' => 'custom-select submit-onchange']) ?>
</div>
<?= $this->Form->end() ?>