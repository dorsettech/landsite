<?php

use App\Library\Search;

?>
<?= $this->Form->create(null, ['type' => 'get', 'url' => $action]); ?>
<div class="form-row">
    <?php if (empty($purpose)) : ?>
    <div class="form-group col-12 col-sm-6 col-lg-3 col-xxl">
        <?= $this->Form->control('purpose', ['type' => 'select', 'placeholder' => __('All Properties'), 'empty' => __('All Properties'), 'default' => h($data['purpose'] ?? ''), 'options' => Search::TYPES, 'class' => 'custom-select', 'label' => false]) ?>
    </div>
    <?php else : ?>
        <?= $this->Form->control('purpose', ['type' => 'hidden', 'value' => $purpose]) ?>
    <?php endif; ?>
    <div class="form-group col-12 col-sm-6 col-lg-3 col-xxl">
        <?= $this->Form->control('type', ['type' => 'select', 'placeholder' => __('Property Type'), 'empty' => __('Property Type'), 'default' => h($data['type'] ?? ''), 'options' => $propertyTypes, 'class' => 'custom-select property-type', 'label' => false]) ?>
    </div>
    <div class="form-group col-6 col-sm-6 col-lg-3 col-xxl">
        <?= $this->Form->control('min_price', ['type' => 'select', 'placeholder' => __('Min Price'), 'empty' => __('No min'), 'default' => h($data['min_price'] ?? ''), 'options' => Search::getPrices(), 'class' => 'select', 'label' => false]) ?>
    </div>
    <div class="form-group col-6 col-sm-6 col-lg-3 col-xxl">
        <?= $this->Form->control('max_price', ['type' => 'select', 'placeholder' => __('Max Price'), 'empty' => __('No max'), 'default' => h($data['max_price'] ?? ''), 'options' => Search::getPrices(), 'class' => 'select', 'label' => false]) ?>
    </div>
    <div class="form-group col-12 col-sm-7 col-lg col-xxl">
        <?= $this->Form->control('location', ['placeholder' => __('Location'), 'val' => h($data['location'] ?? ''), 'class' => 'form-control', 'id' => 'validation03', 'label' => false]) ?>
    </div>
    <div class="form-group col-12 col-sm-5 col-lg-3 col-xxl">
        <?= $this->Form->control('range', ['type' => 'select', 'placeholder' => __('Range'), 'empty' => __('Range'), 'default' => h($data['range'] ?? ''), 'options' => Search::RANGES, 'class' => 'custom-select', 'label' => false]) ?>
    </div>
    <div class="form-group col-auto order-1 order-lg-2 order-xxl-0">
        <?= $this->Form->button('Search properties', ['type' => 'submit', 'class' => 'btn btn-primary']) ?>
    </div>
    <div class="w-100 d-none d-lg-block"></div>
    <div class="col-12 advanced order-lg-2">
        <div id="advancedsearch" class="collapse show">
            <div class="collapse-in attributes loading-wrapper">
                <?php if (!empty($propertyAttributes)) : ?>
                    <?php foreach ($propertyAttributes as $key => $attribute) : ?>
                        <?= $this->Form->control('attributes.' . $attribute['id'], ['type' => 'checkbox', 'label' => $attribute['name'], 'value' => true, (!empty($data['attributes'][$attribute['id']]) ? 'checked' : '')]) ?>
                    <?php endforeach; ?>
                <?php elseif (!empty($data['type'])) : ?>
                    <p><?= __('There are no additional options for selected property type.') ?></p>
                <?php else : ?>
                    <p><?= __('Please choose property type to display additional options.') ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end(); ?>
