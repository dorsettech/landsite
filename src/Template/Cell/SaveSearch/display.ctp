<?php if ($displaySaveSearch) : ?>
    <section class="save-search">
        <div class="alert alert-dismissible fade show" role="alert">
            <div class="container text-center">
                <?= $this->Form->create(null, ['type' => 'post', 'url' => ['_name' => 'save-search']]) ?>
                <?= $this->Form->control('search', ['type' => 'hidden', 'val' => $type]) ?>
                <?php foreach ($data as $key => $value) : ?>
                    <?php if (is_array($value)) : ?>
                        <?php foreach ($value as $k => $v) : ?>
                            <?= $this->Form->control($key . '.' . $k, ['type' => 'hidden', 'value' => $v]) ?>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <?= $this->Form->control($key, ['type' => 'hidden', 'value' => $value]) ?>
                    <?php endif; ?>
                <?php endforeach; ?>
                <?= $this->Form->button(__('Please click here if you would like to save your search to My Account'), ['type' => 'submit', 'class' => 'btn-link btn-search-save']) ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </section>
<?php endif; ?>