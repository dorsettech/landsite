<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Email</title>
        <style>
            @font-face {
                font-family: 'Work Sans';
                font-style: normal;
                font-weight: 600;
                src: local('Work Sans SemiBold'), local('WorkSans-SemiBold'), url(https://fonts.gstatic.com/s/worksans/v3/QGYpz_wNahGAdqQ43Rh3o4T8lthN2fk.woff2) format('woff2');
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }
            /* latin */
            @font-face {
                font-family: 'Work Sans';
                font-style: normal;
                font-weight: 600;
                src: local('Work Sans SemiBold'), local('WorkSans-SemiBold'), url(https://fonts.gstatic.com/s/worksans/v3/QGYpz_wNahGAdqQ43Rh3o4T8mNhN.woff2) format('woff2');
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }
            @media (max-width: 479px) {
                .mobile-100 {
                    padding-right: 15px !important;
                    padding-left: 15px !important;
                    width: auto !important;
                    display: table;
                }
                .mobile-100 table td {
                    padding-right: 20px !important;
                    padding-left: 20px !important;
                }
                th {
                    display: none;
                }
                td::before {
                    content: attr(data-label);
                    margin-right: 15px;
                    width: 40px;
                    float: left;
                }
            }
        </style>
    </head>
    <body style="margin: 0;">
        <div style="max-width: 600px; margin: 0 auto;" class="email-container">
            <table background="<?= $_projectUrl ?>img/email/bg.jpg" bgcolor="#eeeeee" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; max-width: 600px; background-image: url('<?= $_projectUrl ?>img/email/bg.jpg')">
<!--                <div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; font-family: sans-serif;">
                    (Optional) This text will appear in the inbox preview, but not the email body. It can be used to supplement the email subject line or even summarize the email's contents. Extended text preheaders (~490 characters) seems like a better UX for anyone using a screenreader or voice-command apps like Siri to dictate the contents of an email. If this text is not included, email clients will automatically populate it using the text (including image alt text) at the start of the email's body.
                </div>-->
                <tr>
                    <td style="padding: 30px 20px 0 20px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 0 0 0 0; max-width: 300px;">
                            <tr>
                                <td>
                                    <img src="<?= $_projectUrl ?>img/email/logo.png" alt="The Landsite" width="300" style="display: inline-block; max-width: 100%;" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 30px 10px 30px 10px;">
                        <?= $this->fetch('content') ?>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#1e3b56" style="padding: 30px 20px 30px 20px;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="center">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <?php if (!empty($statics['social_facebook'])): ?>
                                                <td>
                                                    <a href="<?= $statics['social_facebook'] ?>">
                                                        <img src="<?= $_projectUrl ?>img/email/facebook.png" alt="Facebook" width="35" height="35" style="display: block;" border="0" />
                                                    </a>
                                                </td>
                                            <?php endif; ?>
                                            <td bgcolor="#1e3b56" style="font-size: 0; line-height: 0;" width="50"></td>
                                            <?php if (!empty($statics['social_twitter'])): ?>
                                                <td>
                                                    <a href="<?= $statics['social_twitter'] ?>">
                                                        <img src="<?= $_projectUrl ?>img/email/twitter.png" alt="Twitter" width="35" height="35" style="display: block;" border="0" />
                                                    </a>
                                                </td>
                                            <?php endif; ?>
                                            <td bgcolor="#1e3b56"  style="font-size: 0; line-height: 0;" width="50"></td>
                                            <?php if (!empty($statics['social_linkedin'])): ?>
                                                <td>
                                                    <a href="<?= $statics['social_linkedin'] ?>">
                                                        <img src="<?= $_projectUrl ?>img/email/linkedin.png" alt="Twitter" width="35" height="35" style="display: block;" border="0" />
                                                    </a>
                                                </td>
                                            <?php endif; ?>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 30px 0 0 0;">
                            <tr>
                                <td align="center" style="color: #ffffff; font-family: 'Raleway', sans-serif;">
                                    <a href="<?= $_projectUrl.'privacy-policy' ?>" style="color: #ffffff; font-size: 14px;">Privacy Policy</a>
                                    |
                                    <a href="<?= $_membersUrl.'unsubscribe' ?>" style="color: #ffffff; font-size: 14px;">Unsubscribe</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>
