<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
    <head>
        <title><?= $this->fetch('title') ?></title>
    </head>
    <body style="padding: 15px; background: #f6f6f6; text-align: center;">
        <div style="width: 600px; max-width: 100%; margin: 0 auto; text-align: left;">
            <div class="wrapper" style="margin-bottom: 20px; padding: 20px; border: 1px solid #e9e9e9; border-radius: 3px; background: #ffffff;">
                <div class="logo">
                    <img src="<?= $this->Url->build('/', true) ?>img/logo-mail.png" alt="<?= $_service ?>" style="max-width: 280px;" />
                </div>
                <div class="content">
                    <?= $this->fetch('content') ?>
                </div>
            </div>
            <div class="footer-details" style="color: #888888; text-align: center; font-size: 10px;">
                <?= __('Sent from IP: {0} ', $_SERVER['REMOTE_ADDR']) ?>
            </div>
        </div>
    </body>
</html>
