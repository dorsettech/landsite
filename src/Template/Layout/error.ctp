<?= $this->element('head'); ?>
<?= $this->element('header'); ?>
<div class="error-image">
    <?= $this->Html->image('error.jpg', ['class' => 'img-fluid']) ?>
</div>
<div class="error-page">
    <div class="container">
        <?= $this->fetch('content') ?>
    </div>
    <?php
    $errorMessage = $this->fetch('errorMessage');
    $errorTrace   = $this->fetch('errorTrace');
    $errorInfo    = $this->fetch('errorInfo');

    if ($errorMessage || $errorTrace || $errorInfo) :
        ?>
        <section class="debug">
            <div class="container">
                <div class="error-message">
                    <?= $errorMessage ?>
                </div>
                <div class="error-trace">
                    <?= $errorTrace ?>
                </div>
                <div class="error-info">
                    <?= $errorInfo ?>
                </div>
            </div>
        </section>
    <?php endif; ?>
</div>
<?= $this->element('footer'); ?>
<?= $this->element('footer-scripts'); ?>