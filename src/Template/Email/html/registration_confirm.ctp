<table bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; padding: 30px 20px 20px 20px; max-width: 600px;"> <!-- max width is width of white box -->
    <tr>
        <td align="center" style="margin: 0 0 10px 0; font-family: 'Work Sans', sans-serif; font-size: 30px; color: #1e3b56; font-weight: normal;">
            Hi <?= $user->first_name ?>,
        </td>
    </tr>
    <tr>
        <td align="center" style="padding: 10px 0 0 0; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px;">
            Congratulations. You have been successfully registered.
        </td>
    </tr>
    <tr>
        <td align="center" style="padding: 10px 0 0 0; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px; ">
            Please click the link below to verify your email address: <br/><br/>
            <a href="http://<?= $host ?>/verify-email/<?= $token ?>" style="color: #e41a39; word-break: break-all;">http://<?= $host ?>/verify-email/<?= $token ?></a>
        </td>
    </tr>
    <tr>
        <td align="center" style="padding: 10px 0 0 0; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px; ">
            Don't forget finish your email verification. Thank you.
        </td>
    </tr>
</table>
