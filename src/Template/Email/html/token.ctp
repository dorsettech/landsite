<h1><?= __('Hi {0}', $user->first_name) ?></h1>
<p><?= __('It looks like <strong>your token is not valid</strong> on {0} website.', $service) ?></p>
<p><?= __('Don\'t worry, we generated a new one for you. :)') ?></p>
<p><?= __('Please copy new token and use it on next screen after log in:') ?></p>
<h2><small><?= __('Your new token is') ?>:</small> <?= $user->token ?></h2>
<p><?= __('Please note that this token expires on {0}', $tokenExpires) ?></p>
<p>
    -- <br>
    <?= __('Good luck!') ?><br>
    <?= __('B4B Dev Team') ?>
</p>