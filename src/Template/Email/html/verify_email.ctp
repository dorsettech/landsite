<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; padding: 30px 0 0 0; max-width: 600px;"> <!-- max width is width of white box -->
    <tr>
        <td align="center" style="margin: 0 20px 10px 20px; font-family: 'Work Sans', sans-serif; font-size: 30px; color: #1e3b56; font-weight: normal;">
            Hi <?= $user->first_name ?>,
        </td>
    </tr>
    <tr>
        <td align="center" style="padding: 10px 20px 0 20px; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px; ">
            Follow the link below to verify your email address.
        </td>
    </tr>
    <tr>
        <td align="center" style="padding: 20px 20px 30px 20px; font-family: 'Raleway', sans-serif; font-size: 16px;">
            <a href="http://<?= $host ?>/verify-email/<?= $token ?>" style="color: #e41a39; word-break: break-all;">http://<?= $host ?>/verify-email/<?= $token ?></a>
        </td>
    </tr>
</table>
