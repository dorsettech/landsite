<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; padding: 30px 20px 0 20px; max-width: 560px;"> <!-- max width is width of white box -->
    <tr>
        <td align="center" style="margin: 0 0 10px 0; font-family: 'Work Sans', sans-serif; font-size: 30px; color: #1e3b56; font-weight: normal;">
            Hi <?= $member->first_name ?>,
        </td>
    </tr>
    <tr>
        <td align="center" style="padding: 10px 20px 0 20px; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px;">
            Here’s a selection of the latest news added matching your preferences.
        </td>
    </tr>
    <tr>
        <td align="center" style="padding: 10px 20px 0 20px; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px; ">
            Go to news to see all of them
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" style="padding: 0 20px 0 20px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; padding: 0 0 0 0;">
                <tr>
                    <?php $items = $emailData->count(); ?>
                    <?php foreach ($emailData as $key => $article): ?>
                        <td align="center" valign="top" class="mobile-100" style="padding: 20px 10px 0 0;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; padding: 0 0 0 0;">
                                <tr>
                                    <td style="padding: 0 0 0 0; position: relative;">
                                        <a href="<?= $_projectUrl.'articles/'.$article->slug ?>">
                                            <img src="<?= !empty($article->image) ? $_projectUrl.$article->image_path : $_projectUrl.'img/placeholder.png' ?>" alt="<?= $article->slug ?>" width="100%" style="display: block; max-width: 100%;">
                                        </a>
                                    </td>
                                </tr>
                                <?php if (!empty($article->publish_date)): ?>
                                    <tr>
                                        <td style="padding: 10px 0 0 0; ">
                                            <p style="margin: 0; color: #06090a; font-family: 'Raleway', sans-serif; font-size: 10px;">
                                                <?= $article->publish_date->format('jS F Y') ?>
                                            </p>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                <tr>
                                    <td style="padding: 10px 0 0 0; ">
                                        <a href="<?= $_projectUrl.'articles/'.$article->slug ?>" style="color: #6d6d6d; font-family: 'Work Sans', sans-serif; font-size: 16px; text-decoration: none;">
                                            <?= $article->title ?>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 10px 0 0 0;">
                                        <?= $this->Page->emailArticlePostedBy($article, ['link' => true]) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 10px 0 0 0; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 12px;">
                                        <?= $article->headline ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 10px 0 0 0; font-family: 'Work Sans', sans-serif; font-size: 16px;">
                                        <a href="<?= $_projectUrl.'articles/'.$article->slug ?>">
                                            <img src="<?= $_projectUrl.'img/email/button-readmore.png' ?>" alt="read more" width="125px" style="display: block; max-width: 125px;">
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <?php if ($items === $key + 1): ?>
                        </tr>
                    <?php elseif (($key + 1) % 2 === 0): ?>
            </tr>
            <tr>
            <?php endif; ?>
        <?php endforeach; ?>
</table>
</td>
</tr>
<tr>
    <td align="center" style="padding: 30px 0 30px 0; font-family: 'Work Sans', sans-serif; font-size: 16px;">
        <a href="<?= $_projectUrl.'articles' ?>">
            <img src="<?= $_projectUrl.'img/email/button-viewallinsights.png' ?>" alt="View all insights" width="187px" style="display: block; max-width: 187px;">
        </a>
    </td>
</tr>
</table>