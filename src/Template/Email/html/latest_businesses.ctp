<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; padding: 30px 20px 30px 20px; max-width: 600px; "> <!-- max width is width of white box -->
    <tr>
        <td align="center" style="margin: 0 0 10px 0; font-family: 'Work Sans', sans-serif; font-size: 30px; color: #1e3b56; font-weight: normal;">
            Hi <?= $member->first_name ?>,
        </td>
    </tr>
    <tr>
        <td align="center" style="padding: 10px 20px 0 20px; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px; ">
            Please have a look...
        </td>
    </tr>
    <tr>
        <td align="center" style="padding: 50px 0 30px 0;">
            <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; border: 1px solid #ccc; padding: 0 0px 0 0px; max-width: 480px;">
                <tr>
                    <td align="center" colspan="2" bgcolor="#1e3b56" style="padding: 0;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; padding: 10px 10px 10px 10px;">
                            <tr>
                                <td align="center" style="font-family: 'Work Sans', sans-serif; font-size: 30px; color: #ffffff; font-size: 26px; font-weight: normal;">
                                    Latest Businesses Added
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <?php foreach ($emailData as $service): ?>
                    <tr>
                        <td align="center" class="mobile-100" style="padding: 30px 10px 0 0; width: 110px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; padding: 0 0 0 0;">
                                <tr>
                                    <td align="center" style="padding: 0 0 0 20px; position: relative;">
                                        <img src="<?= !empty($service->user->image) ? $_projectUrl.$service->user->image_path : $_projectUrl.'img/placeholder.png' ?>" alt="<?= $service->slug ?>" width="100%" style="display: block; max-width: 100%;">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="center" class="mobile-100" style="padding: 30px 0 0 10px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; padding: 0 20px 0 0;">
                                <tr>
                                    <td style="padding: 0; color: #6d6d6d; font-family: 'Work Sans', sans-serif; font-size: 16px; text-decoration: none;"><?= $service->company ?></td>
                                </tr>
                                <tr>
                                    <td style="padding: 10px 0 0 0; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 12px;">
                                        <?= $service->description_short ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 10px 0 0 0;">
                                        <a href="<?= $_projectUrl.'professional-services/'.$service->urlname ?>" style="color: #487fbd; font-family: 'Raleway', sans-serif; font-size: 12px; text-decoration: none;">View business profile</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td align="center" colspan="2" style="padding: 30px 0 0 0;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; padding: 0 0 0 0;">
                            <tr>
                                <td align="center" bgcolor="#efefef" style="border-top: 1px solid #cccccc; padding: 15px 10px 15px 10px; font-family: 'Work Sans', sans-serif; font-size: 26px; font-weight: normal;">
                                    <a href="<?= $_projectUrl.'professional-services' ?>" style="color: #487fbd; display: block; font-family: 'Raleway', sans-serif; font-size: 12px; text-decoration: none;">View all business profiles</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>