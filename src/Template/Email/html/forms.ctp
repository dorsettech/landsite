<h3>New message from <strong><?= $form->name ?> form</strong> from website <?= $_SERVER['SERVER_NAME'] ?>:</h3>
<?php foreach ($data as $field => $value): ?>
    <p><strong><?= Cake\Utility\Inflector::humanize($field) ?></strong> : <?= $value ?></p>
<?php endforeach; ?>