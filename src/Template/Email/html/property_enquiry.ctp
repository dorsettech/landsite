<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; padding: 30px 0 0 0; max-width: 520px;"> <!-- max width is width of white box -->
    <tr>
        <td align="center" style="margin: 0 0 10px 0; font-family: 'Work Sans', sans-serif; font-size: 30px; color: #1e3b56; font-weight: normal;">
            Hi <?= $data['recipient_name'] ?>,
        </td>
    </tr>
    <tr>
        <td align="center" style="padding: 10px 20px 0 20px; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px;">
            You have received an enquiry for <?= $data['title'] ?> from <?= $data['full_name'] ?>
        </td>
    </tr>
    <tr>
        <td align="center" style="padding: 20px 20px 0 20px; font-family: 'Raleway', sans-serif; font-size: 16px;">
            <a href="<?= $_membersUrl ?>" style="color: #e41a39; word-break: break-all;"><?= $_membersUrl ?></a>
        </td>
    </tr>
    <tr>
        <td style="padding: 20px 0 30px 0;">
            <table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="10" width="100%" style="margin: 0 auto; border: 1px solid #6d6d6d; padding: 10px 10px 10px 10px; max-width: 440px;">
                <?php if (isset($data['full_name']) && !empty($data['full_name'])): ?>
                    <tr>
                        <td valign="top" style="padding: 0 0 0 0; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px; width: 90px;">
                            Full Name:
                        </td>
                        <td valign="top" style="padding: 0 0 0 0; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px;">
                            <?= $data['full_name'] ?>
                        </td>
                    </tr>
                <?php endif; ?>
                <?php if (isset($data['email']) && !empty($data['email'])): ?>
                    <tr>
                        <td valign="top" style="padding: 0 0 0 0; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px; width: 90px;">
                            Email:
                        </td>
                        <td valign="top" style="padding: 0 0 0 0; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px;">
                            <?= $data['email'] ?>
                        </td>
                    </tr>
                <?php endif; ?>
                <?php if (isset($data['phone']) && !empty($data['phone'])): ?>
                    <tr>
                        <td valign="top" style="padding: 0 0 0 0; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px; width: 90px;">
                            Phone:
                        </td>
                        <td valign="top" style="padding: 0 0 0 0; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px;">
                            <?= $data['phone'] ?>
                        </td>
                    </tr>
                <?php endif; ?>
                <?php if (isset($data['message']) && !empty($data['message'])): ?>
                    <tr>
                        <td valign="top" style="padding: 0 0 0 0; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px; width: 90px;">
                            Message:
                        </td>
                        <td valign="top" style="padding: 0 0 0 0; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px;">
                            <i><?= $data['message'] ?></i>
                        </td>
                    </tr>
                <?php endif; ?>
            </table>
        </td>
    </tr>
</table>