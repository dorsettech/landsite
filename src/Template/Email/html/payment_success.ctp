<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; padding: 30px 20px 0 20px; max-width: 540px; "> <!-- max width is width of white box -->
    <tr>
        <td align="center" style="margin: 0 0 10px 0; font-family: 'Work Sans', sans-serif; font-size: 30px; color: #1e3b56; font-weight: normal;">
            Hi <?= $user->first_name ?>,
        </td>
    </tr>
    <tr>
        <td align="center" style="padding: 10px 20px 0 20px; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px; ">
            This is your payment confirmation for <?= ucfirst(strtolower($meta['entity'])) ?>
        </td>
    </tr>
    <tr>
        <td align="center" style="padding: 10px 20px 0 20px; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px; ">
            #<?= $meta['id'] ?> placed on <?= $placed ?>
        </td>
    </tr>
    <tr>
        <td align="center" style="padding: 30px 0 30px 0;">
            <table bgcolor="#fafafa" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; padding: 0 0px 0 0px; max-width: 480px; font-family: sans-serif;">
                <tr>
                    <th align="left" bgcolor="#efefef" style="border-top: 2px solid #ffffff; padding: 10px 10px 10px 20px; color: #6d6d6d;">
                        Item
                    </th>
                    <th align="left" bgcolor="#efefef" style="border-top: 2px solid #ffffff; padding: 10px 20px 10px 10px; width: 130px; color: #6d6d6d;">
                        Price
                    </th>
                </tr>
                <?php
                    foreach ($meta['basket'] as $item) {
                        $item['price'] = '£' . number_format( (float) $item['price'], 2, '.', ',' );;
                        echo '<tr>';
                            echo '<td align="left" class="mobile-100" data-label="Item" style="border-top: 2px solid #ffffff; padding: 10px 10px 10px 20px; color: #6d6d6d;">';
                                echo $item['name'];
                            echo '</td>';
                            echo '<td align="left" class="mobile-100" data-label="Price" style="border-top: 2px solid #ffffff; padding: 10px 20px 10px 10px; width: 130px; color: #6d6d6d;">';
                                echo $item['price'];
                            echo '</td>';
                        echo '</tr>';
                    }
                ?>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" style="padding: 10px 20px 30px 20px; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px; ">
            Paid via <?= $paidBy ?>. To download your invoice please click here: <a href="http://<?= $host ?>/invoice/<?= $invoiceThumb ?>" style="color: #1e3b56; font-weight: bold; text-decoration: none;">INVOICE DOWNLOAD</a>
        </td>
    </tr>
</table>
