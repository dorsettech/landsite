<p>
    Name: <strong><?= $quote->name ?></strong><br>
    Email: <strong><?= $quote->email ?></strong><br>
    Phone number: <strong><?= $quote->phone_number ?></strong><br>
    Interested In: <strong><?= $quote->interested_in ?></strong><br>
    Date: <strong><?= $quote->created->i18nFormat('dd-MM-YYYY HH:mm:ss'); ?></strong><br>
    Message:<br> <strong><?= $quote->message ?></strong>
</p>