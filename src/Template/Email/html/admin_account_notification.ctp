<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; padding: 30px 20px 20px 20px; max-width: 600px;"> <!-- max width is width of white box -->
    <tr>
        <td align="center" style="margin: 0 0 10px 0; font-family: 'Work Sans', sans-serif; font-size: 30px; color: #1e3b56; font-weight: normal;">
            Hi The Landsite,
        </td>
    </tr>
    <tr>
        <td align="center" style="padding: 10px 20px 0 20px; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px;">
            You have an account waiting to be approved for <?= $user->full_name ?>.
        </td>
    </tr>
    <tr>
        <td align="center" style="padding: 10px 20px 0 20px; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px; ">
            Sign in to the admin panel by clicking the link<br>
            <a href="<?= $_projectUrl.$_admin_prefix.'/members' ?>" style="color: #e41a39; word-break: break-all;"><?= $_projectUrl.$_admin_prefix.'/members' ?></a>
        </td>
    </tr>
</table>