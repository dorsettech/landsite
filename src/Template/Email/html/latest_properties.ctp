<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; padding: 30px 10px 0 10px; max-width: 560px;">
    <tr>
        <td align="center" style="margin: 0 0 10px 0; font-family: 'Work Sans', sans-serif; font-size: 30px; color: #1e3b56; font-weight: normal;">
            Hi <?= $member->first_name ?>,
        </td>
    </tr>
    <tr>
        <td align="center" style="padding: 20px 20px 0 20px; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px; ">
            Here is a selection of our most recent properties relevant to your search criteria.
        </td>
    </tr>
    <?php foreach ($emailData as $property): ?>
        <tr>
            <td align="center" style="padding: 20px 0 30px 0;">
                <table bgcolor="#e8e9ec" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; padding: 0 0 0 0; max-width: 460px;">
                    <tr>
                        <td align="center" style="padding: 0 0 0 0;">
                            <a href="<?= $_projectUrl.'properties/'.$property->urlname ?>">
                                <img src="<?= !empty($property->main_image) ? $_projectUrl.$property->main_image : $_projectUrl.'img/placeholder.png' ?>" alt="<?= $property->slug ?>" width="100%" style="display: block; max-width: 100%;">
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 15px 20px 0 20px; ">
                            <a href="<?= $_projectUrl.'properties/'.$property->urlname ?>" style="color: #153643; font-family: 'Work Sans', sans-serif; font-size: 16px; text-decoration: none;"><?= $property->title ?></a>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px 20px 0 20px; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 12px; ">
                            <?= $property->location ?>
                        </td>
                    </tr>
                    <?php if (!empty($property->property_attributes)): ?>
                        <tr>
                            <td style="padding: 10px 20px 0 20px; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 12px; ">
                                <table>
                                    <tr>
                                        <?php foreach ($property->property_attributes as $attribute): ?>
                                            <td>
                                                <img src="<?= $_projectUrl.$attribute->image_path ?>" alt="<?=$attribute->name?>" width="100%" style="margin: 0 3px 0 3px; display: inline-block; width: 30px;">
                                            </td>
                                        <?php endforeach; ?>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    <?php endif; ?>
                    <tr>
                        <td style="padding: 10px 20px 0 20px; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 12px; ">
                            <?= $property->headline ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 20px 0 0 0;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; padding: 0 0 0 0;">
                                <tr>
                                    <?= $this->Properties->emailPrice($property) ?>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    <?php endforeach; ?>
    <tr>
        <td align="center" style="padding: 30px 0 30px 0; font-family: 'Work Sans', sans-serif; font-size: 16px;">
            <a href="<?= $_projectUrl.'properties' ?>">
                <img src="<?= $_projectUrl.'img/email/button-viewmoreproperties.png' ?>" alt="View more properties" width="230" style="display: block; max-width: 230px;">
            </a>
        </td>
    </tr>
</table>