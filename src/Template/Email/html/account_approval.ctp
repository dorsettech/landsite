<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; padding: 30px 0 0 0; max-width: 600px;"> <!-- max width is width of white box -->
    <tr>
        <td align="center" style="margin: 0 0 10px 0; font-family: 'Work Sans', sans-serif; font-size: 30px; color: #1e3b56; font-weight: normal;">
            Hi <?= $user->first_name ?>,
        </td>
    </tr>
    <tr>
        <td align="center" style="padding: 10px 20px 0 20px; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px;">
            Your account has been approved.
        </td>
    </tr>
    <tr>
        <td align="center" style="padding: 10px 20px 0 20px; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px; ">
            Please choose one of the options below and get the most out of our services.
        </td>
    </tr>
    <tr>
        <td style="padding: 10px 20px 20px 20px;">
            <table width="100%">
                <tr>
                    <td style="padding: 20px 0 0 0;">
                        <table bgcolor="#e8e9ec" background="<?= $_projectUrl ?>img/email/image1.jpg" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 0 0 0 0; background-image: url('<?= $_projectUrl ?>img/email/image1.jpg')">
                            <tr>
                                <td style="padding: 50px 20px 50px 20px; ">
                                    <table align="left" bgcolor="#1e3b56" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 0 0 0 0; max-width: 300px;">
                                        <tr>
                                            <td style="padding: 30px 20px 30px 20px; ">
                                                <a href="<?= $_membersUrl ?>" style="color: #fff; font-family: 'Work Sans', sans-serif; font-size: 16px; text-decoration: none;">
                                                    <strong>Add your first property</strong>
                                                    <br>
                                                    Share your land or property with our community of potential buyers in your area, targeting like-minded individuals, each with a passion for commercial property.
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 20px 0 0 0;">
                        <table bgcolor="#e8e9ec" background="<?= $_projectUrl ?>img/email/image1.jpg" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 0 0 0 0; background-image: url('<?= $_projectUrl ?>img/email/image1.jpg')">
                            <tr>
                                <td style="padding: 50px 20px 50px 20px; ">
                                    <table align="right" bgcolor="#e52338" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 0 0 0 0; max-width: 300px;">
                                        <tr>
                                            <td style="padding: 30px 20px 30px 20px; ">
                                                <a href="<?= $_membersUrl ?>" style="color: #fff; font-family: 'Work Sans', sans-serif; font-size: 16px; text-decoration: none;">
                                                    <strong>Showcase your services</strong>
                                                    <br>
                                                    Showcase your business to a targeted audience of like-minded individuals within our land and commercial property community.
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 20px 0 0 0;">
                        <table bgcolor="#e8e9ec" background="<?= $_projectUrl ?>img/email/image1.jpg" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 0 0 0 0; background-image: url('<?= $_projectUrl ?>img/email/image1.jpg')">
                            <tr>
                                <td style="padding: 50px 20px 50px 20px; ">
                                    <table align="left" bgcolor="#1e3b56" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 0 0 0 0; max-width: 300px;">
                                        <tr>
                                            <td style="padding: 30px 20px 30px 20px; ">
                                                <a href="<?= $_projectUrl.'properties' ?>" style="color: #fff; font-family: 'Work Sans', sans-serif; font-size: 16px; text-decoration: none;">
                                                    <strong>Search for a property</strong>
                                                    <br>
                                                    Browse the properties available on our portal. With varied professional properties available across the country, you're certain to find what you're looking for.
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 20px 0 0 0;">
                        <table bgcolor="#e8e9ec" background="<?= $_projectUrl ?>img/email/image1.jpg" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 0 0 0 0; background-image: url('<?= $_projectUrl ?>img/email/image1.jpg')">
                            <tr>
                                <td style="padding: 50px 20px 50px 20px; ">
                                    <table align="right" bgcolor="#e52338" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 0 0 0 0; max-width: 300px;">
                                        <tr>
                                            <td style="padding: 30px 20px 30px 20px; ">
                                                <a href="<?= $_projectUrl.'professional-services' ?>" style="color: #fff; font-family: 'Work Sans', sans-serif; font-size: 16px; text-decoration: none;">
                                                    <strong>Search for a service</strong>
                                                    <br>
                                                    Search our community of professional service providers. The Landsite provides the ideal environment for you to connect directly with service providers in your local area with ease.
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 20px 0 0 0;">
                        <table bgcolor="#e8e9ec" background="<?= $_projectUrl ?>img/email/image1.jpg" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 0 0 0 0; background-image: url('<?= $_projectUrl ?>img/email/image1.jpg')">
                            <tr>
                                <td style="padding: 50px 20px 50px 20px; ">
                                    <table align="left" bgcolor="#1e3b56" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 0 0 0 0; max-width: 300px;">
                                        <tr>
                                            <td style="padding: 30px 20px 30px 20px; ">
                                                <a href="<?= $_projectUrl.'articles' ?>" style="color: #fff; font-family: 'Work Sans', sans-serif; font-size: 16px; text-decoration: none;">
                                                    <strong>Recent insights</strong>
                                                    <br>
                                                    Stay up to date with the latest news and insights from the land and commercial property sector, including market trends and updates.
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>