<table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; padding: 30px 20px 0 20px; max-width: 600px;">
    <tr>
        <td align="center" style="margin: 0 0 10px 0; font-family: 'Work Sans', sans-serif; font-size: 30px; color: #1e3b56; font-weight: normal;">
            Hi <?= $service->user->first_name ?>,
        </td>
    </tr>
    <tr>
        <td align="center" style="padding: 10px 0 30px 0; color: #6d6d6d; font-family: 'Raleway', sans-serif; font-size: 16px;">
            Your professional service <span style="color: #e41a39;"><?= $service->company ?></span> has now been submitted for approval. You will receive an email once it has been reviewed to let you know if there are any further actions required.
        </td>
    </tr>
</table>
