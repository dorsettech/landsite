<?php

namespace App\Traits;

use Cake\Http\Response;

/**
 * GetListTrait
 */
trait GetListTrait
{
    /**
     * Get basic list
     *
     * @param integer $limit Limit results, 200 by default.
     * @return Response|void Returns list of current model: id and display_field. Note: display field can't be virtual field.
     */
    public function getList(int $limit = 200)
    {
        $this->getRequest()->allowMethod(['ajax']);

        $model        = $this->modelAlias;
        $displayField = $this->{$model}->getDisplayField();
        $data         = $this->getRequest()->getData();
        $keyword      = (!empty($data['keyword'])) ? $data['keyword'] : '';

        $conditions = [
            $model . '.' . $displayField . ' LIKE ' => '%' . $keyword . '%'
        ];
        if (is_numeric($keyword)) {
            $conditions = [
                $model . '.id' => $keyword
            ];
        }

        $list = $this->{$model}->find('list', [
            'conditions' => $conditions,
            'order' => [$displayField => 'ASC'],
            'limit' => $limit
        ]);

        $data = $this->convertList($list);

        $this->set('data', $data);
        $this->set('_serialize', 'data');
    }

    /**
     * Convert list
     *
     * @param object $list List finder object.
     * @return array Returns list as a subarrays with id and text fields for use in select2.
     */
    private function convertList($list)
    {
        $data = [];
        foreach ($list as $id => $field) {
            $data[] = [
                'id' => $id,
                'text' => $field . ' #' . $id
            ];
        }

        return $data;
    }
}
