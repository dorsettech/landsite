<?php

namespace App\Traits;

/**
 * Switcher Trait
 *
 * @description Switcher trait for toggle checkboxes.
 */
trait SwitcherTrait
{
    /**
     * Toggle column value
     *
     * @description Toggle state of available columns
     * @return void
     */
    public function toggleColumnValue(): void
    {
        $this->request->allowMethod('POST');

        $status  = AJAX_STATUS_FAIL;
        $message = '';
        $data    = $this->request->getData();
        $model   = $this->request->getParam('controller');
        if (!empty($data['model'])) {
            $model = $data['model'];
        }

        $this->loadModel($model);

        $record = $this->{$model}->get($data['id']);
        if ($record) {
            $record->{$data['column']} = !$record->{$data['column']};
            if ($this->{$model}->save($record)) {
                $status  = AJAX_STATUS_OK;
                $message = __('[#{id}] Field `{column}` is now {type}.', ['id' => $record->id, 'column' => $data['column'], 'type' => (($record->{$data['column']}) ? __('active') : __('inactive'))]);
            } else {
                $status  = AJAX_STATUS_ERROR;
                $message = __('[#{id}] An error occured while updating field `{field}`', ['id' => $record->id, 'column' => $data['column']]);
            }
        }

        $this->set(compact(['status', 'message']));
        $this->set('_serialize', ['status', 'message']);
    }
}
