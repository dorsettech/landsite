<?php

namespace App\Traits;

/**
 * Trait HtmlResponse
 *
 * @package App\Traits
 * @author Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
trait HtmlResponse
{

    public function asHtmlResponse($body)
    {
        return $this->response->withStringBody($body)->withType('text/html');
    }

}
