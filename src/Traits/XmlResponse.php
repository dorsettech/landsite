<?php

namespace App\Traits;

use App\ResponseType;
use Cake\Utility\Xml;

/**
 * Trait XmlResponse
 *
 * @package App\Traits
 * @author Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
trait XmlResponse
{

    protected $responseType = ResponseType::XML;

    public function asXmlResponse($data = [])
    {
        return $this->set($data)->response->withType($this->responseType)->withStringBody(Xml::build(['response' => $this->viewVars])->saveXML());
    }

    public function asMessageResponse($result = true, $message = '', $data = [])
    {
        return $this->asMessage($result, $message, $data)->response->withType($this->responseType)->withStringBody(Xml::build(['response' => $this->viewVars])->saveXML());
    }

    public function asMessage($result = true, $message = '', $data = [])
    {
        $response = array('success' => $result == true, 'msg' => $message);
        return $this->set(array_merge($response, $data));
    }

    public function asDataResponse($result = true, $data = [], $message = '')
    {
        return $this->asData($result, $data, $message)->response->withType($this->responseType)->withStringBody(Xml::build(['response' => $this->viewVars])->saveXML());
    }

    public function asData($result = true, $data = [], $message = '')
    {
        $response = array('success' => $result == true, 'msg' => $message, 'data' => $data);
        return $this->set($response);
    }

    public function asListResponse($list, $root = 'records', $item = 'record', $total = -1)
    {
        return $this->asList($list, $root, $item, $total)->response->withType($this->responseType)->withStringBody(Xml::build(['response' => $this->viewVars])->saveXML());
    }

    public function asList($list, $root = 'records', $item = 'record', $total = -1)
    {
        $item = (!$item) ? 'record' : $item;
        if ($root === null) {
            $response = [
                $item => $list
            ];
        } else {
            $root = (!$root) ? 'records' : $root;
            $response = [
                $root => [
                    $item => $list
                ]
            ];
        }
        if ($total !== -1) {
            $response['total'] = $total;
        }
        return $this->set($response);
    }
}
