<?php

namespace App\Traits;

/**
 * Trait PlainResponse
 *
 * @package App\Traits
 * @author Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
trait PlainResponse
{

    public function asTextResponse($body)
    {
        return $this->response->withStringBody($body)->withType('text/plain');
    }

}
