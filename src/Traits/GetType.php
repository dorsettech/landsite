<?php
/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */

namespace App\Traits;

use App\Model\Enum\MediaType;

/**
 * GetType
 */
trait GetType
{
    /**
     * Get type from given mimes
     *
     * @param string $mime      Mime type.
     * @param array  $mimeTypes Mime types array, type as a key, possible values as a subarray.
     * @return string|null Returns type key from given mime types array.
     */
    public function getTypeFromMime(string $mime, array $mimeTypes): ?string
    {
        foreach ($mimeTypes as $type => $mimes) {
            if (is_numeric(array_search($mime, $mimes))) {
                return $type;
            }
        }

        return null;
    }

    /**
     * Media Type Enum from mime
     *
     * @param string $mime      Mime type.
     * @param array  $mimeTypes Mime types array, type as a key, possible values as a subarray.
     * @return string|null Returns MediaType enum value.
     */
    public function mediaTypeFromMime(string $mime, array $mimeTypes): ?string
    {
        $type = strtoupper($this->getTypeFromMime($mime, $mimeTypes));
        
        return MediaType::getConstList()[$type] ?? null;
    }
}
