<?php

namespace App\Traits;

use Cake\I18n\Time;

trait DiscountValidator
{
    /**
     * Check discount code validity.
     *
     * @param string  $code   Discount code.
     * @param integer $userId User Id.
     *
     * @return array|null
     */
    protected function checkCodeValidity(string $code, int $userId): ?array
    {
        $now = new Time();

        $discount = $this->Discounts->find('all', [
            'conditions' => [
                'code' => $code,
                'active' => true,
                'and' => [
                    'or' => [
                        'user_id IS' => null,
                        'user_id' => $userId
                    ]
                ],
                'or' => [
                    ['date_start <=' => $now, 'date_end >=' => $now],
                    ['date_start IS' => null, 'date_end >=' => $now],
                    ['date_start <=' => $now, 'date_end IS' => null],
                    ['date_start IS' => null, 'date_end IS' => null]
                ]
            ]
        ])->first();

        if ($discount) {
            $usesOverall = $this->DiscountUses->find('all', [
                'discount_id' => $discount->get('id')
            ])->count();
            $usesUser = $this->DiscountUses->find('all', [
                'discount_id' => $discount->get('id'),
                'user_id' => $userId
            ])->count();

            if ($discount->get('per_customer') && $discount->get('per_customer') <= $usesUser) {
                return [
                    'success' => false,
                    'msg' => 'The discount code has already been used by you.'
                ];
            }
            if ($discount->get('per_code') && $discount->get('per_code') <= $usesOverall) {
                return [
                    'success' => false,
                    'msg' => 'The code has already been used the maximum number of times.'
                ];
            }

            return [
                'success' => true,
                'msg' => 'Discount code is successfully validated',
                'discount' => $discount
            ];
        } else {
            return [
                'success' => false,
                'msg' => 'Discount code is not valid or expired'
            ];
        }
    }
}
