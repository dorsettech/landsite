<?php

namespace App\Traits;

use App\ResponseType;

/**
 * Trait JsonResponse
 *
 * @package App\Traits
 * @author Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
trait JsonResponse
{

    protected $responseType = ResponseType::JSON;

    public function asJsonResponse($data = [])
    {
        return $this->set($data)->response->withType($this->responseType)->withStringBody(json_encode($this->viewVars));
    }

    public function asMessageResponse($result = true, $message = '', $data = [])
    {
        return $this->asMessage($result, $message, $data)->response->withType($this->responseType)->withStringBody(json_encode($this->viewVars));
    }

    public function asMessage($result = true, $message = '', $data = [])
    {
        $response = array('success' => $result == true, 'msg' => $message);
        return $this->set(array_merge($response, $data));
    }

    public function asDataResponse($result = true, $data = [], $message = '')
    {
        return $this->asData($result, $data, $message)->response->withType($this->responseType)->withStringBody(json_encode($this->viewVars));
    }

    public function asData($result = true, $data = [], $message = '')
    {
        $response = array('success' => $result == true, 'msg' => $message, 'data' => $data);
        return $this->set($response);
    }

    public function asListResponse($list, $root = 'records', $total = -1)
    {
        return $this->asList($list, $root, $total)->response->withType($this->responseType)->withStringBody(json_encode($this->viewVars));
    }

    public function asList($list, $root = 'records', $total = -1)
    {
        if ($root === null) {
            $response = $list;
        } else {
            $root = (!$root) ? 'records' : $root;
            $response = array(
                $root => $list
            );
        }
        if ($total !== -1) {
            $response['total'] = $total;
        }
        return $this->set($response);
    }
}
