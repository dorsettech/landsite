<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\Traits;

use Cake\ORM\TableRegistry;

/**
 * Trait GalleryTrait
 */
trait GalleryTrait
{
    /**
     * Get single image data
     *
     * @return void
     */
    public function getSingleImageData()
    {
        $this->request->allowMethod('get');
        $galleryData = $this->getGalleryData($this->request->getQuery('model'), $this->request->getQuery('entityId'), $this->request->getQuery('key'));
        $data = $galleryData['image'];

        $this->set(compact('data'));
        $this->set('_serialize', 'data');
    }

    /**
     * Update single image
     *
     * @return void
     */
    public function updateSingleImage()
    {
        $this->request->allowMethod('post');

        $galleryData = $this->getGalleryData($this->request->getData('model'), $this->request->getData('entityId'));
        $galleryData['gallery'][$this->request->getData('key.title')] = $this->request->getData('data.title');
        $galleryData['gallery'][$this->request->getData('key.alt')] = $this->request->getData('data.alt');
        $patchedEntity = $galleryData['model']->patchEntity($galleryData['entity'], ['gallery' => $galleryData['gallery']]);
        if ($galleryData['model']->save($patchedEntity)) {
            $data = ['message' => __('Image updated successfuly.'), 'status' => 200];
        } else {
            $data = ['message' => __('There was error while trying to update image.'), 'status' => 500];
        }

        $this->set(compact('data'));
        $this->set('_serialize', 'data');
    }

    /**
     * Update gallery order
     *
     * @return void
     */
    public function updateGalleryOrder()
    {
        $this->request->allowMethod('post');
        $galleryData = $this->getGalleryData($this->request->getData('model'), $this->request->getData('entityId'));
        $newData = [];

        foreach ($this->request->getData('data') as $oldKey => $newKey) {
            $newData[$newKey] = $galleryData['gallery'][$oldKey];
        }
        ksort($newData);
        $patchedEntity = $galleryData['model']->patchEntity($galleryData['entity'], ['gallery' => $newData]);
        if ($galleryData['model']->save($patchedEntity)) {
            $data = ['message' => __('Positions for gallery have been updated.'), 'status' => 200];
        } else {
            $data = ['message' => __('There was error while trying to update gallery order.'), 'status' => 500];
        }

        $this->set(compact('data'));
        $this->set('_serialize', 'data');
    }

    /**
     * Delete single gallery image
     *
     * @param string|null  $modelName Model name.
     * @param integer|null $entityId  Entity id.
     * @param integer|null $imageKey  Image key.
     *
     * @return void
     */
    public function deleteSingleGalleryImage($modelName, $entityId, $imageKey)
    {
        $this->request->allowMethod('delete');
        $galleryData = $this->getGalleryData($modelName, $entityId, $imageKey);
        @unlink($galleryData['image']['path']);
        unset($galleryData['gallery'][$imageKey]);
        $patchedEntity = $galleryData['model']->patchEntity($galleryData['entity'], ['gallery' => $galleryData['gallery']]);
        if ($galleryData['model']->save($patchedEntity)) {
            $data = ['message' => __('Image removed successfuly.'), 'status' => 200];
        } else {
            $data = ['message' => __('There was error while trying to remove image.'), 'status' => 500];
        }

        $this->set(compact('data'));
        $this->set('_serialize', 'data');
    }

    /**
     * Get gallery data
     *
     * @param string|null  $modelName Model name.
     * @param integer|null $entityId  Entity id.
     * @param integer|null $imageKey  Image key.
     *
     * @return mixed
     */
    private function getGalleryData($modelName, $entityId = null, $imageKey = null)
    {
        $data['model'] = TableRegistry::getTableLocator()->get($modelName);
        if ($entityId) {
            $data['entity'] = $data['model']->get($entityId);
            $data['gallery'] = $data['entity']->gallery;
        }
        if (is_numeric($imageKey)) {
            $data['image'] = $data['gallery'][$imageKey];
        }

        return $data;
    }
}
