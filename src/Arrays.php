<?php

namespace App;

use RecursiveArrayIterator;
use RecursiveIteratorIterator;

/**
 * Array Utilities
 *
 * @package App
 * @author Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
class Arrays {

    /**
     * Flat multidimensional array to one level array.
     *
     * @param array $array
     * @param string $filterKey
     * @return array
     */
    public static function flattenRecursive($array, $filterKey = '') {
        if ($array) {
            $flat = array();
            foreach (new RecursiveIteratorIterator(
                         new RecursiveArrayIterator($array),
                         RecursiveIteratorIterator::SELF_FIRST) as $key => $value) {

                if (!is_array($value)) {
                    if ($filterKey === '' || $filterKey === $key) $flat[] = $value;
                }
            }
            return $flat;
        } else {
            return [];
        }
    }

    /**
     * Searchs element (needle) in given haystack recursively.
     *
     * @param mixed $needle
     * @param array $haystack
     * @return bool
     */
    public static function inArrayRecursive($needle, $haystack) {
        $iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($haystack));

        foreach ($iterator AS $element) {
            if ($element == $needle) {
                return true;
            }
        }

        return false;
    }

    /**
     * Trim all values in given array.
     *
     * @param array $array
     * @return array
     */
    public static function trim($array) {
        if (!is_array($array)) return trim($array);

        return array_map('Arrays::trim', $array);
    }

    /**
     * Display array in human readable format.
     *
     * @param array $array
     * @param string $function dump|export|print
     */
    public static function printArray($array, $function = 'export') {
        echo '<pre>';

        switch ($function) {
            case 'dump':
                var_dump($array);
                break;
            case 'export':
                var_export($array);
                break;
            case 'print':
                print_r($array);
                break;
            default:
                var_export($array);
                break;
        }

        echo '</pre>';
    }

    /**
     * Stripslashes function executed on all values
     * in given array.
     *
     * @param array $array
     * @return array
     */
    public static function stripSlashes($array) {
        if (!is_array($array)) return stripslashes($array);

        return array_map('Arrays::stripSlashes', $array);
    }

    /**
     * Addslashes function executed on all values
     * in given array.
     *
     * @param array $array
     * @return array
     */
    public static function addSlashes($array) {
        if (!is_array($array)) return addslashes($array);

        return array_map('Arrays::addSlashes', $array);
    }

    /**
     * @param $array
     * @return array|string
     */
    public static function utf2iso($array) {
        if (!is_array($array)) return iconv('UTF-8', 'ISO-8859-2', $array);

        return array_map('Arrays::utf2iso', $array);
    }

    /**
     * @param $array
     * @return array|string
     */
    public static function utf2win($array) {
        if (!is_array($array)) return iconv('UTF-8', 'WINDOWS-1250//TRANSLIT', $array);

        return array_map('Arrays::utf2win', $array);
    }

    /**
     * @param $array
     * @return array|string
     */
    public static function maz2utf($array) {

        $conversionTable = array("\x8f" => "\xa1", "\x95" => "\xc6", "\x90" => "\xca", "\x9c" => "\xa3", "\xa5" => "\xd1", "\xa3" => "\xd3",
            "\x98" => "\xa6", "\xa0" => "\xac", "\xa1" => "\xaf", "\x86" => "\xb1", "\x8d" => "\xe6", "\x91" => "\xea",
            "\x92" => "\xb3", "\xa4" => "\xf1", "\xa2" => "\xf3", "\x9e" => "\xb6", "\xa6" => "\xbc", "\xa7" => "\xbf"
        );

        if (!is_array($array)) {
            $array = strtr($array, $conversionTable);
            return iconv("ISO-8859-2", "UTF-8", $array);
        }

        return array_map('Arrays::maz2utf', $array);
    }

    /**
     * Remove duplicates from one dimensional array.
     *
     * @param array $inputArray
     * @param bool $saveKey
     * @return array
     */
    public static function removeDuplicates($inputArray, $saveKey = true) {

        $output_array = array();

        if (is_array($inputArray)) {
            if ($saveKey) {
                foreach ($inputArray as $key => $value) {
                    if (!in_array($value, $output_array)) {
                        $output_array[$key] = $value;
                    }
                }
            } else {
                $index = 0;
                foreach ($inputArray as $value) {
                    if (!in_array($value, $output_array)) {
                        $output_array[$index] = $value;
                        $index++;
                    }
                }
            }
            return $output_array;
        }
        return false;
    }

    /**
     * @param $input
     * @param $callback
     * @return array|null
     */
    public static function filterKey($input, $callback) {
        if (!is_array($input)) {
            trigger_error('filterKey() expects parameter 1 to be array, ' . gettype($input) . ' given', E_USER_WARNING);
            return null;
        }

        if (empty($input)) {
            return $input;
        }

        $filteredKeys = array_filter(array_keys($input), $callback);
        if (empty($filteredKeys)) {
            return array();
        }

        $input = array_intersect_key(array_flip($filteredKeys), $input);

        return $input;
    }

    public static function extend(...$args) {
        while ($extended = array_shift($args))
            if (is_array($extended)) break;

        if (!is_array($extended)) return false;

        while ($array = array_shift($args)) {
            if (is_array($array))
                foreach ($array as $k => $v)
                    $extended[$k] = is_array($v) && isset($extended[$k]) ?
                        self::extend(is_array($extended[$k]) ?
                            $extended[$k] : array(), $v) :
                        $v;
        }

        return $extended;
    }

    /**
     * Get value from one or multi-dimension array by dot syntax.
     *
     * @param $array
     * @param $key
     * @param null $default
     * @return null|mixed
     */
    public static function get(&$array, $key, $default = null) {
        if (strpos($key, '.') === false) $key = "$key.";
        list($index, $key) = explode('.', $key, 2);
        if (!isset($array[$index])) return $default;

        if (strlen($key) > 0)
            return self::get($array[$index], $key, $default);

        return $array[$index];
    }

    /**
     * Set value to one or multi-dimension array by dot syntax.
     *
     * @param $array
     * @param $key
     * @param null $value
     * @return bool
     */
    public static function set(&$array, $key, $value = null) {
        if (strpos($key, '.') === false) $key = "$key.";
        list($index, $key) = explode('.', $key, 2);
        if (!isset($array[$index])) return false;

        if (strlen($key) > 0)
            return self::set($array[$index], $key, $value);

        $array[$index] = $value;
        return true;
    }

    /**
     * Check if array is associative array (non sequential)
     *
     * @param array $arr
     * @return bool
     */
    public static function isAssoc(array $arr) {
        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }
}
