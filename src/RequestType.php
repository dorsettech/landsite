<?php

namespace App;

use App\Model\Enum;

/**
 * Denotes Requests Types.
 *
 * Main purpose - avoiding use literals in sources
 *
 * @package App
 * @author Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
abstract class RequestType extends Enum
{

    CONST
        POST = 'POST',
        GET = 'GET',
        PUT = 'PUT',
        PATCH = 'PATCH',
        DELETE = 'DELETE',
        OPTIONS = 'OPTIONS';

}
