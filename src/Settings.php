<?php

namespace App;

use App\Model\Entity\Setting;
use App\Model\Enum\SettingType;


use Exception;

/**
 * Common Settings class for framework.
 *
 * Provides all functions for managing settings for project which
 * are stored in database.
 *
 * @package App
 * @author Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
class Settings
{

    private static $data = [];
    private static $loaded = false;
    private static $model;

    /**
     * Load all settings from database
     * (usually run once at bootstrap)
     */
    public static function loadAll(): ?bool
    {
        if (self::$loaded) {
            return true;
        }

        try {
            $model = self::model();
            $query = $model->find()->orderAsc('`group`');
            foreach ($query as $setting) {
                if (!isset(self::$data[$setting->get('group')])) {
                    self::$data[$setting->get('group')] = [];
                }
                if (!isset(self::$data[$setting->get('group')][$setting->get('key')])) {
                    self::$data[$setting->get('group')][$setting->get('key')] = $setting;
                }
            }
            self::$loaded = true;
            return true;
        } catch (Exception $e) {
            debug($e);
            return false;
        }
    }

    /**
     * @param null $model
     * @return \Cake\ORM\Table|null
     */
    protected static function model($model = null): ?\Cake\ORM\Table
    {
        if ($model) {
            self::$model = $model;
        }

        if (!self::$model) {
            self::$model = (new \Cake\ORM\Locator\TableLocator)->get('Settings');
        }

        return self::$model;
    }

    /**
     * Alias for Settings::get
     *
     * @param $group
     * @param null $key
     * @return mixed
     */
    public static function ask($group, $key = null)
    {
        return self::get($group, $key);
    }

    /**
     * Get value of setting
     *
     * @param $group
     * @param null $key
     * @return mixed
     */
    public static function get($group, $key = null)
    {
        if ($key === null) {
            $parts = explode('.', $group);
            if (count($parts) !== 2 || !isset(self::$data[$parts[0]][$parts[1]])) {
                return null;
            }
            return self::$data[$parts[0]][$parts[1]]->asType();
        }

        if (!isset(self::$data[$group][$key])) {
            return null;
        }

        return self::$data[$group][$key]->asType();
    }

    /**
     * Alias for Settings::getGroup
     *
     * @param $group
     * @return array|null
     */
    public static function askForGroup($group): ?array
    {
        return self::getGroup($group);
    }

    /**
     * Get group of key/value pairs
     *
     * @param $group
     * @return array|null
     */
    public static function getGroup($group): ?array
    {
        if (!isset(self::$data[$group])) {
            return null;
        }
        $values = array();
        foreach (self::$data[$group] as $key => $setting) {
            $values[$key] = $setting->asType();
        }
        return $values;
    }

    /**
     * Alias for Settings::set
     *
     * @param $group
     * @param $mixed
     * @param null $value
     * @return bool
     */
    public static function update($group, $mixed, $value = null): bool
    {
        return self::set($group, $mixed, $value);
    }

    /**
     * Update setting
     *
     * @param $group
     * @param $mixed
     * @param null $value
     * @return bool
     */
    public static function set($group, $mixed, $value = null): bool
    {
        if (strpos($group, '.') !== false) {
            $parts = explode('.', $group);
            if (count($parts) !== 2 || !isset(self::$data[$parts[0]][$parts[1]])) {
                return false;
            }
            $value = $mixed;
            [$group, $mixed] = $parts;
        }

        $setting = self::$data[$group][$mixed];

        if ($setting->is(SettingType::JSON)) {
            $value = json_encode($value, JSON_UNESCAPED_SLASHES);
        } else if ($setting->is(SettingType::BOOLEAN)) {
            $value = $value ? 'true' : 'false';
        }

        $setting->set('value', $value);

        return self::$model->save($setting) !== false;
    }

    /**
     * Save new setting
     *
     * @param Setting $setting
     * @return bool
     */
    public static function save(Setting $setting): bool
    {
        return self::$model->save($setting) !== false;
    }
}
