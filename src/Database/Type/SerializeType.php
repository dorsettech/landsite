<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\Database\Type;

use Cake\Database\Driver;
use Cake\Database\Type;
use PDO;

/**
 * Class SerializeType
 */
class SerializeType extends Type
{
    /**
     * To php
     *
     * @param mixed  $value  Value to analyze.
     * @param Driver $driver Driver object.
     *
     * @return mixed|null
     */
    public function toPHP($value, Driver $driver)
    {
        if ($value === null) {
            return null;
        }
        if (is_array($value)) {
            return $value;
        }

        return unserialize($value);
    }

    /**
     * Marshal
     *
     * @param mixed $value Value to analyze.
     *
     * @return mixed
     */
    public function marshal($value)
    {
        if (is_array($value) || $value === null) {
            return $value;
        }

        return unserialize($value);
    }

    /**
     * To database
     *
     * @param mixed  $value  Value to analyze.
     * @param Driver $driver Driver object.
     *
     * @return string
     */
    public function toDatabase($value, Driver $driver)
    {
        return serialize($value);
    }

    /**
     * To statement
     *
     * @param mixed  $value  Value to analyze.
     * @param Driver $driver Driver object.
     *
     * @return integer
     */
    public function toStatement($value, Driver $driver)
    {
        if ($value === null) {
            return PDO::PARAM_NULL;
        }

        return PDO::PARAM_STR;
    }
}
