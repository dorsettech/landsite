<?php

namespace App\Database\Type;

use Cake\Database\Driver;
use Cake\Database\Type;
use PDO;

/**
 * ORM SetType
 *
 * @package App\Database\Type
 * @author  Grzegorz Zagrobelny <mobifly@mobifly.pl>
 */
class SetType extends Type
{
    /**
     * @param mixed  $value  Value.
     * @param Driver $driver Driver.
     *
     * @return array|mixed
     */
    public function toPHP($value, Driver $driver)
    {
        if (empty($value)) {
            return [];
        }
        return is_string($value) ? explode(',', $value) : $value;
    }

    /**
     * @param mixed $value Value.
     *
     * @return array|mixed
     */
    public function marshal($value)
    {
        if (is_array($value) || $value === null) {
            return $value;
        }
        return is_string($value) ? explode(',', $value) : $value;
    }

    /**
     * @param mixed  $value  Value.
     * @param Driver $driver Driver.
     *
     * @return mixed|string|null
     */
    public function toDatabase($value, Driver $driver)
    {
        if (empty($value) || (is_array($value) && !$value)) {
            return null;
        }
        if (is_array($value)) {
            return implode(',', $value);
        }
        return $value;
    }

    /**
     * @param mixed  $value  Values.
     * @param Driver $driver Driver.
     *
     * @return integer|mixed
     */
    public function toStatement($value, Driver $driver)
    {
        if (empty($value)) {
            return PDO::PARAM_NULL;
        }
        return PDO::PARAM_STR;
    }
}
