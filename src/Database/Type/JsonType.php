<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-07)
 * @version 1.0
 */

namespace App\Database\Type;

use Cake\Database\Driver;
use Cake\Database\Type;
use PDO;

/**
 * Class JsonType
 */
class JsonType extends Type
{
    /**
     * Convert to php
     *
     * @param mixed  $value  Value to analyze.
     * @param Driver $driver Driver object.
     *
     * @return array|null
     */
    public function toPHP($value, Driver $driver)
    {
        if ($value === null) {
            return null;
        }

        return json_decode($value, true);
    }

    /**
     * Convert to Marshal
     *
     * @param mixed $value Value to analyze.
     *
     * @return mixed
     */
    public function marshal($value)
    {
        if (is_array($value) || $value === null) {
            return $value;
        }

        return json_decode($value, true);
    }

    /**
     * To database
     *
     * @param mixed  $value  Value to analyze.
     * @param Driver $driver Driver object.
     *
     * @return string
     */
    public function toDatabase($value, Driver $driver)
    {
        return json_encode($value);
    }

    /**
     * To statement
     *
     * @param mixed  $value  Value to analyze.
     * @param Driver $driver Driver object.
     *
     * @return integer
     */
    public function toStatement($value, Driver $driver)
    {
        if ($value === null) {
            return PDO::PARAM_NULL;
        }

        return PDO::PARAM_STR;
    }
}
