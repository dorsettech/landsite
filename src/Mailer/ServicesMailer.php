<?php
/**
 * @author  Marcin <marcin.nierobis@econnect4u.pl>
 * @date    date(2019-01-27)
 * @version 1.0
 */

namespace App\Mailer;

use App\Model\Entity\Service;
use Cake\ORM\TableRegistry;

/**
 * ServicesMailer
 */
class ServicesMailer extends DefaultMailer
{

    /**
     * Send a business enguiry message.
     *
     * @param array $data Email data.
     *
     * @return object
     */
    public function businessEnquiry(array $data)
    {
        return $this
            ->to($data['recipient_email'])
            ->setLayout('landsite')
            ->setTemplate('business_enquiry')
            ->setEmailFormat('html')
            ->setSubject(__('Business Enquiry'))
            ->set(['data' => $data]);
    }

    /**
     * Notification about expiring service.
     *
     * @param Service $service Service entity.
     *
     * @return mixed
     */
    public function serviceExpireSoon(Service $service)
    {
        return $this
            ->to($service->user->email)
            ->setLayout('landsite')
            ->setTemplate('service_expire_soon')
            ->setEmailFormat('html')
            ->setSubject(__('Your professional service on The Landsite is due to expire.'))
            ->set(['service' => $service]);
    }

    /**
     * Notification about expired service.
     *
     * @param Service $service Service entity.
     *
     * @return mixed
     */
    public function serviceExpired(Service $service)
    {
        return $this
            ->to($service->user->email)
            ->setLayout('landsite')
            ->setTemplate('service_expired')
            ->setEmailFormat('html')
            ->setSubject(__('Your professional service on The Landsite has expired.'))
            ->set(['service' => $service]);
    }

    /**
     * Send a message about submitting service.
     *
     * @param Service $service Service entity.
     *
     * @return object
     */
    public function serviceWaitingForApproval(Service $service)
    {
        $this->checkAndLoadUser($service);

        return $this
            ->to($service->get('user')->get('email'))
            ->setLayout('landsite')
            ->setTemplate('service_waiting_for_approval')
            ->setEmailFormat('html')
            ->setSubject(__('Your professional service on The Landsite has been submitted and waiting for approve.'))
            ->set(['service' => $service]);
    }

    /**
     * @param Service $service Service entity.
     *
     * @return void
     */
    private function checkAndLoadUser(Service $service): void
    {
        if (!isset($service->user)) {
            $user = TableRegistry::getTableLocator()->get('Users')->find()->where(['id' => $service->get('user_id')])->first();
            $service->set('user', $user);
        }
    }

    /**
     * Send a message about approve service.
     *
     * @param Service $service Service entity.
     *
     * @return object
     */
    public function serviceApprove(Service $service)
    {
        $this->checkAndLoadUser($service);

        return $this
            ->to($service->user->email)
            ->setLayout('landsite')
            ->setTemplate('service_approval')
            ->setEmailFormat('html')
            ->setSubject(__('Your professional service on The Landsite has been approved.'))
            ->set(['service' => $service]);
    }

    /**
     * Send a message about reject service.
     *
     * @param Service $service Service entity.
     *
     * @return object
     */
    public function serviceReject(Service $service)
    {
        $this->checkAndLoadUser($service);

        return $this
            ->to($service->user->email)
            ->setLayout('landsite')
            ->setTemplate('service_reject')
            ->setEmailFormat('html')
            ->setSubject(__('Your professional service on The Landsite has been rejected.'))
            ->set(['service' => $service]);
    }
}
