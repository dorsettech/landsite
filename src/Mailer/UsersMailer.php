<?php
/**
 * @author  Stefan <marcin@econnect4u.pl>
 * @date    date(2019-01-21)
 * @version 1.0
 */

namespace App\Mailer;

use App\Model\Entity\User;
use Cake\Mailer\Email;

/**
 * UsersMailer
 */
class UsersMailer extends DefaultMailer
{
    /**
     * UsersMailer construct
     *
     * @param Email $email Email.
     */
    public function __construct(Email $email = null)
    {
        parent::__construct($email);
    }

    /**
     * Token email
     *
     * @param User   $user    User entity.
     * @param string $expires Expiry date.
     * @return object
     */
    public function token(User $user, string $expires)
    {
        $user->email = $this->debugEmail($user->email);

        $service = $this->getConf('Website.service');

        return $this
            ->to($user->email)
            ->subject(__('New token for {0} website', $service))
            ->set(['user' => $user, 'tokenExpires' => $expires, 'service' => $service]);
    }

    /**
     * Registration confirm
     *
     * @param array $data Email data.
     * @return object
     */
    public function registrationConfirm(array $data)
    {
        return $this
                ->to('marcin.nierobis@econnect4u.pl')
                ->layout('landsite')
                ->template('registration_confirm')
                ->emailFormat('html')
                ->subject(__('You\'ve been registered successfully on The Landsite'));
    }

    /**
     * Registration confirm
     *
     * @param array $data Email data.
     * @return object
     */
    public function accountApproval(array $data)
    {
        return $this
                ->to('marcin.nierobis@econnect4u.pl')
                ->layout('landsite')
                ->template('account_approval')
                ->emailFormat('html')
                ->subject(__('Account Approval'));
    }
}
