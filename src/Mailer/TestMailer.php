<?php
/**
 * @author  Stefan <marcin.nierobis@econnect4u.pl>
 * @date    date(2019-01-27)
 * @version 1.0
 */

namespace App\Mailer;

use App\Mailer\DefaultMailer;
use App\Model\Entity\User;
use App\Model\Entity\Article;
use App\Model\Entity\Property;
use App\Model\Entity\Service;

/**
 * TestMailer
 */
class TestMailer extends DefaultMailer
{

    /**
     * @param User $user User entity.
     *
     * @return mixed
     */
    public function approve(User $user)
    {
        return $this
            ->to('sm@b4b.co.uk')
            ->setLayout('landsite')
            ->setTemplate('account_approval')
            ->setEmailFormat('html')
            ->setSubject(__('Your account on The Landsite has been approved.'))
            ->set(['user' => $user]);
    }

    /**
     * @param User $user User entity.
     *
     * @return mixed
     */
    public function reject(User $user)
    {
        return $this
            ->to('sm@b4b.co.uk')
            ->setLayout('landsite')
            ->setTemplate('account_reject')
            ->setEmailFormat('html')
            ->setSubject(__('Your account on The Landsite has been rejected.'))
            ->set(['user' => $user]);
    }

    /**
     * @param User $user User entity.
     *
     * @return mixed
     */
    public function incomplete(User $user)
    {
        return $this
            ->setTo('sm@b4b.co.uk')
            ->setLayout('landsite')
            ->setTemplate('account_incomplete')
            ->setEmailFormat('html')
            ->setSubject(__('Your profile on The Landsite is incomplete.'))
            ->set(['user' => $user]);
    }

    /**
     * @param User $user User entity.
     *
     * @return mixed
     */
    public function adminNotification(User $user)
    {
        return $this
            ->setTo('sm@b4b.co.uk')
            ->setLayout('landsite')
            ->setTemplate('admin_account_notification')
            ->setEmailFormat('html')
            ->setSubject(__('You have an account waiting to be approved.'))
            ->set(['user' => $user]);
    }

    /**
     * Send a message about submitting case study.
     *
     * @param Article $article Article entity.
     *
     * @return object
     */
    public function caseStudyWaitingForApproval(Article $article)
    {
        return $this
            ->to('marcin.nierobis@econnect4u.pl')
//            ->setBcc('sm@b4b.co.uk')
            ->setLayout('landsite')
            ->setTemplate('case_studies_waiting_for_approval')
            ->setEmailFormat('html')
            ->setSubject(__('Your case study on The Landsite has been submitted and waiting for approve.'))
            ->set(['article' => $article]);
    }
    /**
     * Send a message about approve case study.
     *
     * @param Article $article Article entity.
     *
     * @return object
     */
    public function caseStudyApprove(Article $article)
    {
        return $this
            ->to('marcin.nierobis@econnect4u.pl')
//            ->setBcc('sm@b4b.co.uk')
            ->setLayout('landsite')
            ->setTemplate('case_studies_approval')
            ->setEmailFormat('html')
            ->setSubject(__('Your case study on The Landsite has been approved.'))
            ->set(['article' => $article]);
    }

    /**
     * Send a message about reject case study.
     *
     * @param Article $article Article entity.
     *
     * @return object
     */
    public function caseStudyReject(Article $article)
    {
        return $this
            ->to('marcin.nierobis@econnect4u.pl')
//            ->setBcc('sm@b4b.co.uk')
            ->setLayout('landsite')
            ->setTemplate('case_studies_reject')
            ->setEmailFormat('html')
            ->setSubject(__('Your case study on The Landsite has been rejected.'))
            ->set(['article' => $article]);
    }

    /**
     * Send a message about submitting news article.
     *
     * @param Article $article Article entity.
     *
     * @return object
     */
    public function newsWaitingForApproval(Article $article)
    {
        return $this
            ->to('marcin.nierobis@econnect4u.pl')
//            ->setBcc('sm@b4b.co.uk')
            ->setLayout('landsite')
            ->setTemplate('news_waiting_for_approval')
            ->setEmailFormat('html')
            ->setSubject(__('Your news article on The Landsite has been submitted and waiting for approve.'))
            ->set(['article' => $article]);
    }

    /**
     * Send a message about approve news article.
     *
     * @param Article $article Article entity.
     *
     * @return object
     */
    public function newsApprove(Article $article)
    {
        return $this
            ->to('marcin.nierobis@econnect4u.pl')
//            ->setBcc('sm@b4b.co.uk')
            ->setLayout('landsite')
            ->setTemplate('news_approval')
            ->setEmailFormat('html')
            ->setSubject(__('Your news on The Landsite has been approved.'))
            ->set(['article' => $article]);
    }

    /**
     * Send a message about reject news article.
     *
     * @param Article $article Article entity.
     *
     * @return object
     */
    public function newsReject(Article $article)
    {
        return $this
            ->to('marcin.nierobis@econnect4u.pl')
//            ->setBcc('sm@b4b.co.uk')
            ->setLayout('landsite')
            ->setTemplate('news_reject')
            ->setEmailFormat('html')
            ->setSubject(__('Your news on The Landsite has been rejected.'))
            ->set(['article' => $article]);
    }

        /**
     * Send a property enquire message.
     *
     * @param array $data Email data.
     *
     * @return object
     */
    public function propertyEnquiry(array $data)
    {
        return $this
                ->to('sm@b4b.co.uk')
                ->setBcc('marcin.nierobis@econnect4u.pl')
                ->setLayout('landsite')
                ->setTemplate('property_enquiry')
                ->setEmailFormat('html')
                ->setSubject(__('Property Enquiry'))
                ->set(['data' => $data]);
    }

    /**
     * Notification about expiring property.
     *
     * @param Property $property Property entity.
     * @return mixed
     */
    public function propertyExpireSoon(Property $property)
    {
        return $this
                ->to('sm@b4b.co.uk')
                ->setBcc('marcin.nierobis@econnect4u.pl')
                ->setLayout('landsite')
                ->setTemplate('property_expire_soon')
                ->setEmailFormat('html')
                ->setSubject(__('Your property on The Landsite is due to expire.'))
                ->set(['property' => $property]);
    }

    /**
     * Notification about expired property.
     *
     * @param Property $property Property entity.
     * @return mixed
     */
    public function propertyExpired(Property $property)
    {
        return $this
                ->to('sm@b4b.co.uk')
                ->setBcc('marcin.nierobis@econnect4u.pl')
                ->setLayout('landsite')
                ->setTemplate('property_expired')
                ->setEmailFormat('html')
                ->setSubject(__('Your property on The Landsite has expired.'))
                ->set(['property' => $property]);
    }

    /**
     * Send a business enguiry message.
     *
     * @param array $data Email data.
     *
     * @return object
     */
    public function businessEnquiry(array $data)
    {
        return $this
            ->to('sm@b4b.co.uk')
            ->setBcc('marcin.nierobis@econnect4u.pl')
            ->setLayout('landsite')
            ->setTemplate('business_enquiry')
            ->setEmailFormat('html')
            ->setSubject(__('Business Enquiry'))
            ->set(['data' => $data]);
    }

    /**
     * Notification about expiring service.
     *
     * @param Service $service Service entity.
     *
     * @return mixed
     */
    public function serviceExpireSoon(Service $service)
    {
        return $this
            ->to('sm@b4b.co.uk')
            ->setBcc('marcin.nierobis@econnect4u.pl')
            ->setLayout('landsite')
            ->setTemplate('service_expire_soon')
            ->setEmailFormat('html')
            ->setSubject(__('You professional service on The Landsite is due to expire.'))
            ->set(['service' => $service]);
    }

    /**
     * Notification about expired service.
     *
     * @param Service $service Service entity.
     *
     * @return mixed
     */
    public function serviceExpired(Service $service)
    {
        return $this
            ->to('sm@b4b.co.uk')
            ->setBcc('marcin.nierobis@econnect4u.pl')
            ->setLayout('landsite')
            ->setTemplate('service_expired')
            ->setEmailFormat('html')
            ->setSubject(__('You professional service on The Landsite has expired.'))
            ->set(['service' => $service]);
    }

    /**
     * @param User   $user  User entity.
     * @param string $token Reset password token.
     * @param string $host  Host name.
     *
     * @return mixed
     */
    public function forgotPassword(User $user, string $token, string $host)
    {
        return $this
            ->to('sm@b4b.co.uk')
            ->setBcc('marcin.nierobis@econnect4u.pl')
            ->setLayout('landsite')
            ->setTemplate('forgot_password')
            ->setEmailFormat('html')
            ->setSubject(__('Forgot Password'))
            ->set(['user' => $user, 'token' => $token, 'host' => $host]);
    }
}
