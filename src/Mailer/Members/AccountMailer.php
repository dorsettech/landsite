<?php

namespace App\Mailer\Members;

use App\Mailer\DefaultMailer;
use App\Model\Entity\User;

/**
 * Class AccountMailer
 *
 * @package App\Mailer\Members
 */
class AccountMailer extends DefaultMailer
{
    /**
     * @param User   $user  User entity.
     * @param string $token Email verification token.
     * @param string $host  Host name.
     *
     * @return mixed
     */
    public function register(User $user, string $token, string $host)
    {
        return $this
            ->to($user->get('email'))
            ->setLayout('landsite')
            ->setTemplate('registration_confirm')
            ->setEmailFormat('html')
            ->setSubject(__('You\'ve been registered successfully on The Landsite'))
            ->set(['user' => $user, 'token' => $token, 'host' => $host]);
    }

    /**
     * @param User $user User entity.
     *
     * @return mixed
     */
    public function approve(User $user)
    {
        return $this
            ->to($user->get('email'))
            ->setLayout('landsite')
            ->setTemplate('account_approval')
            ->setEmailFormat('html')
            ->setSubject(__('Your account on The Landsite has been approved.'))
            ->set(['user' => $user]);
    }

    /**
     * @param User $user User entity.
     *
     * @return mixed
     */
    public function reject(User $user)
    {
        return $this
            ->to($user->get('email'))
            ->setLayout('landsite')
            ->setTemplate('account_reject')
            ->setEmailFormat('html')
            ->setSubject(__('Your account on The Landsite has been rejected.'))
            ->set(['user' => $user]);
    }

    /**
     * @param User $user User entity.
     *
     * @return mixed
     */
    public function incomplete(User $user)
    {
        return $this
            ->setTo($user->get('email'))
            ->setLayout('landsite')
            ->setTemplate('account_incomplete')
            ->setEmailFormat('html')
            ->setSubject(__('Your profile on The Landsite is incomplete.'))
            ->set(['user' => $user]);
    }

    /**
     * @param User $user User entity.
     *
     * @return mixed
     */
    public function adminNotification(User $user)
    {
        return $this
            ->setTo('info@thelandsite.co.uk')
            ->setLayout('landsite')
            ->setTemplate('admin_account_notification')
            ->setEmailFormat('html')
            ->setSubject(__('You have an account waiting to be approved.'))
            ->set(['user' => $user]);
    }

    /**
     * @param User   $user  User entity.
     * @param string $token Reset password token.
     * @param string $host  Host name.
     *
     * @return mixed
     */
    public function verify(User $user, string $token, string $host)
    {
        return $this
            ->to($user->get('email'))
            ->setLayout('landsite')
            ->setTemplate('verify_email')
            ->setEmailFormat('html')
            ->setSubject(__('Email Verification'))
            ->set(['user' => $user, 'token' => $token, 'host' => $host]);
    }
}
