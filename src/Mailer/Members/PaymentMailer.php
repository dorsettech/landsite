<?php

namespace App\Mailer\Members;

use App\Mailer\DefaultMailer;
use App\Model\Entity\Payment;
use App\Model\Entity\User;
use App\Model\Enum\PaymentMethod;

/**
 * Class PaymentMailer
 *
 * @package App\Mailer\Members
 */
class PaymentMailer extends DefaultMailer
{
    /**
     * @param User    $user         User entity.
     * @param Payment $payment      Payment entity.
     * @param string  $host         Current Host URL.
     * @param string  $invoiceThumb Invoice thumb.
     *
     * @return mixed
     */
    public function success(User $user, Payment $payment, string $host, string $invoiceThumb)
    {
        $metaPayment = json_decode($payment->get('meta'), true);
        $meta = json_decode($payment->get('payload'), true)['metadata'];
        $meta['basket'] = json_decode($meta['basket'], true);

        switch ($metaPayment['type']) {
            case PaymentMethod::STRIPE:
                $paidBy = 'Stripe';
                break;
            case PaymentMethod::PURCHASE_ORDER_NUMBER:
                $paidBy = 'Purchase Order';
                break;
            default:
                $paidBy = $metaPayment['type'];
        }

        return $this
            ->to($user->get('email'))
            ->setLayout('landsite')
            ->setTemplate('payment_success')
            ->setEmailFormat('html')
            ->setSubject(__('Payment confirmation - The Landsite'))
            ->set([
                'user' => $user,
                'host' => $host,
                'meta' => $meta,
                'paidBy' => $paidBy,
                'placed' => $payment->get('created')->format('d/m/Y'),
                'invoiceThumb' => $invoiceThumb
            ]);
    }

    /**
     * @param User $user User entity.
     *
     * @return mixed
     */
    public function fail(User $user)
    {
        return $this
            ->to($user->get('email'))
            ->setLayout('landsite')
            ->setTemplate('payment_fail')
            ->setEmailFormat('html')
            ->setSubject(__('Payment failed - The Landsite'))
            ->set([
                'user' => $user
            ]);
    }
}
