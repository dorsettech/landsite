<?php

namespace App\Mailer\Members;

use App\Mailer\DefaultMailer;
use App\Model\Entity\User;

/**
 * Class PasswordMailer
 *
 * @package App\Mailer\Members
 */
class PasswordMailer extends DefaultMailer
{
    /**
     * @param User   $user  User entity.
     * @param string $token Reset password token.
     * @param string $host  Host name.
     *
     * @return mixed
     */
    public function token(User $user, string $token, string $host)
    {
        return $this
            ->to($user->get('email'))
            ->setLayout('landsite')
            ->setTemplate('forgot_password')
            ->setEmailFormat('html')
            ->setSubject(__('Forgot Password'))
            ->set(['user' => $user, 'token' => $token, 'host' => $host]);
    }
}
