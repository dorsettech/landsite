<?php
/**
 * @author  Dzidek <marcin.nierobis@econnect4u.pl>
 * @date    date(2019-04-18)
 * @version 1.0
 */

namespace App\Mailer;

use App\Model\Entity\Article;
use Cake\ORM\TableRegistry;

/**
 * ArticlesMailer
 */
class ArticlesMailer extends DefaultMailer
{

    /**
     * Send a message about submitting case study.
     *
     * @param Article $article Article entity.
     *
     * @return object
     */
    public function caseStudyWaitingForApproval(Article $article)
    {
        $this->checkAndLoadUser($article);

        return $this
            ->to($article->get('user')->get('email'))
            ->setLayout('landsite')
            ->setTemplate('case_studies_waiting_for_approval')
            ->setEmailFormat('html')
            ->setSubject(__('Your case study on The Landsite has been submitted and waiting for approve.'))
            ->set(['article' => $article]);
    }

    /**
     * @param Article $article Article.
     *
     * @return void
     */
    private function checkAndLoadUser(Article $article): void
    {
        if (!isset($article->user)) {
            $user = TableRegistry::getTableLocator()->get('Users')->find()->where(['id' => $article->get('author_id')])->first();
            $article->set('user', $user);
        }
    }

    /**
     * Send a message about approve case study.
     *
     * @param Article $article Article entity.
     *
     * @return object
     */
    public function caseStudyApprove(Article $article)
    {
        return $this
            ->to($article->user->email)
            ->setLayout('landsite')
            ->setTemplate('case_studies_approval')
            ->setEmailFormat('html')
            ->setSubject(__('Your case study on The Landsite has been approved.'))
            ->set(['article' => $article]);
    }

    /**
     * Send a message about reject case study.
     *
     * @param Article $article Article entity.
     *
     * @return object
     */
    public function caseStudyReject(Article $article)
    {
        return $this
            ->to($article->user->email)
            ->setLayout('landsite')
            ->setTemplate('case_studies_reject')
            ->setEmailFormat('html')
            ->setSubject(__('Your case study on The Landsite has been rejected.'))
            ->set(['article' => $article]);
    }

    /**
     * Send a message about submitting news article.
     *
     * @param Article $article Article entity.
     *
     * @return object
     */
    public function newsWaitingForApproval(Article $article)
    {
        $this->checkAndLoadUser($article);

        return $this
            ->to($article->get('user')->get('email'))
            ->setLayout('landsite')
            ->setTemplate('news_waiting_for_approval')
            ->setEmailFormat('html')
            ->setSubject(__('Your news article on The Landsite has been submitted and waiting for approve.'))
            ->set(['article' => $article]);
    }

    /**
     * Send a message about approve news article.
     *
     * @param Article $article Article entity.
     *
     * @return object
     */
    public function newsApprove(Article $article)
    {
        return $this
            ->to($article->user->email)
            ->setLayout('landsite')
            ->setTemplate('news_approval')
            ->setEmailFormat('html')
            ->setSubject(__('Your news on The Landsite has been approved.'))
            ->set(['article' => $article]);
    }

    /**
     * Send a message about reject news article.
     *
     * @param Article $article Article entity.
     *
     * @return object
     */
    public function newsReject(Article $article)
    {
        return $this
            ->to($article->user->email)
            ->setLayout('landsite')
            ->setTemplate('news_reject')
            ->setEmailFormat('html')
            ->setSubject(__('Your news on The Landsite has been rejected.'))
            ->set(['article' => $article]);
    }
}
