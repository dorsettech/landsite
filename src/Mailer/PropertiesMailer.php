<?php
/**
 * @author  Dzidek <marcin.nierobis@econnect4u.pl>
 * @date    date(2019-01-27)
 * @version 1.0
 */

namespace App\Mailer;

use App\Model\Entity\Property;
use Cake\ORM\TableRegistry;

/**
 * PropertiesMailer
 */
class PropertiesMailer extends DefaultMailer
{

    /**
     * Send a latest properties message.
     *
     * @return object
     */
    public function latestProperties()
    {
        return $this
                ->to('marcin.nierobis@econnect4u.pl')
                ->setLayout('landsite')
                ->setTemplate('latest_properties')
                ->setEmailFormat('html')
                ->setSubject(__('Latest Properties'));
    }

    /**
     * Send a property enquire message.
     *
     * @param array $data Email data.
     *
     * @return object
     */
    public function propertyEnquiry(array $data)
    {
        return $this
                ->to($data['recipient_email'])
                ->setLayout('landsite')
                ->setTemplate('property_enquiry')
                ->setEmailFormat('html')
                ->setSubject(__('Property Enquiry'))
                ->set(['data' => $data]);
    }

    /**
     * Notification about expiring property.
     *
     * @param Property $property Property entity.
     * @return mixed
     */
    public function propertyExpireSoon(Property $property)
    {
        return $this
                ->to($property->user->email)
                ->setLayout('landsite')
                ->setTemplate('property_expire_soon')
                ->setEmailFormat('html')
                ->setSubject(__('Your property on The Landsite is due to expire.'))
                ->set(['property' => $property]);
    }

    /**
     * Notification about expired property.
     *
     * @param Property $property Property entity.
     * @return mixed
     */
    public function propertyExpired(Property $property)
    {
        return $this
                ->to($property->user->email)
                ->setLayout('landsite')
                ->setTemplate('property_expired')
                ->setEmailFormat('html')
                ->setSubject(__('Your property on The Landsite has expired.'))
                ->set(['property' => $property]);
    }

    /**
     * @param Property $property Property entity.
     *
     * @return void
     */
    private function checkAndLoadUser(Property $property): void
    {
        if (!isset($property->user)) {
            $user = TableRegistry::getTableLocator()->get('Users')->find()->where(['id' => $property->get('user_id')])->first();
            $property->set('user', $user);
        }
    }

    /**
     * Send a message about submitting property.
     *
     * @param Property $property Property entity.
     *
     * @return object
     */
    public function propertyWaitingForApproval(Property $property)
    {
        $this->checkAndLoadUser($property);

        return $this
            ->to($property->get('user')->get('email'))
            ->setLayout('landsite')
            ->setTemplate('property_waiting_for_approval')
            ->setEmailFormat('html')
            ->setSubject(__('Your property on The Landsite has been submitted and waiting for approve.'))
            ->set(['property' => $property]);
    }

    /**
     * Send a message about approve property.
     *
     * @param Property $property Property entity.
     *
     * @return object
     */
    public function propertyApprove(Property $property)
    {
        $this->checkAndLoadUser($property);

        return $this
            ->to($property->user->email)
            ->setLayout('landsite')
            ->setTemplate('property_approval')
            ->setEmailFormat('html')
            ->setSubject(__('Your property on The Landsite has been approved.'))
            ->set(['property' => $property]);
    }

    /**
     * Send a message about reject property.
     *
     * @param Property $property Property entity.
     *
     * @return object
     */
    public function propertyReject(Property $property)
    {
        $this->checkAndLoadUser($property);

        return $this
            ->to($property->user->email)
            ->setLayout('landsite')
            ->setTemplate('property_reject')
            ->setEmailFormat('html')
            ->setSubject(__('Your property on The Landsite has been rejected.'))
            ->set(['property' => $property]);
    }
}
