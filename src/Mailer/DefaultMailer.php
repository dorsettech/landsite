<?php
/**
 * @author  Stefan <marcin@econnect4u.pl>
 * @author  Dawid Katarzynski <dawid.katarzynski@eConnect4u.pl>
 * @date    date(2018-01-08)
 * @version 1.0
 */

namespace App\Mailer;

use Cake\Core\Configure;
use Cake\Mailer\Email;
use Cake\Mailer\Mailer;

/**
 * DefaultMailer
 */
class DefaultMailer extends Mailer
{
    /**
     * Debug mode is active or not
     *
     * @var bool
     */
    protected $debug = false;

    /**
     * Debug email address
     *
     * Put your email address here for testing purposes
     *
     * @var string
     */
    protected $debugMail = '';

    /**
     * Customer email address
     *
     * @var string
     */
    protected $customerEmail = '';

    /**
     * Construct
     *
     * @param Email $email Email.
     */
    public function __construct(Email $email = null)
    {
        parent::__construct($email);

        $this->helpers(['Html', 'Url', 'Number', 'Page']);
        $this->emailFormat('html');
        $this->setLayout('default');
        $this->setDefaultValues();
        $this->setStaticContents();
        $this->viewVars($this->getViewVars());
    }

    /**
     * Set default values
     *
     * @return object
     */
    protected function setDefaultValues()
    {
        $this->customerEmail = $this->getConf('Website.email');

        return $this;
    }

    /**
     * Get view vars
     *
     * @return array
     */
    protected function getViewVars()
    {
        return [
            '_service' => $this->getConf('Website.service'),
            '_admin_prefix' => $this->getConf('Website.admin_prefix'),
            '_projectUrl' => $this->getConf('Website.url'),
            '_membersUrl' => $this->getConf('Website.members_url'),
        ];
    }

    /**
     * Get conf
     *
     * @param  string $name Name of the key from config.
     * @return mixed
     */
    protected function getConf(string $name = null)
    {
        Configure::load('website');

        return Configure::readOrFail($name);
    }

    /**
     * Replace given email
     *
     * @param  string $email Email which should be replaced.
     * @return string       Replaced email with $this->debugEmail if $this->debug is set to true.
     */
    protected function debugEmail(string $email)
    {
        if ($this->debug) {
            return $this->debugMail;
        }

        return $email;
    }

    /**
     * Set static contents for email templates
     *
     * @return void
     */
    protected function setStaticContents()
    {
        $data = [];
        $this->loadModel('staticContents');
        $contents = $this->staticContents->find('all');
        if ($contents) {
            foreach ($contents as $content) {
                $data[$content->var_name] = $content->value;
            }
        }
        $this->set('statics', $data);
    }
}
