<?php
use Migrations\AbstractMigration;

class ChangeHeadlineLimitOnProperties extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('properties')->changeColumn('headline', 'string', [
            'comment' => 'Mandatory - description heading (first paragraph)',
            'limit' => 1000,
            'null' => false
        ])->update();
    }
}
