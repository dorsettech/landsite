<?php

use Migrations\AbstractMigration;

/**
 * ChangeSetOnServices
 */
class ChangeSetOnServices extends AbstractMigration
{

    /**
     * Up method
     *
     * @return void
     */
    public function up()
    {
        $this->table('services')
            ->changeColumn('opening_days', 'set', [
                'values'  => ['0', '1', '2', '3', '4', '5', '6'],
                'default' => null,
                'null'    => true
            ])
            ->update();
    }

    /**
     * Down method
     *
     * @return void
     */
    public function down()
    {
        $this->table('services')
            ->changeColumn('opening_days', 'string', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->update();
    }
}
