<?php

use Migrations\AbstractMigration;

/**
 * Class AddEmailVerifiedToUsers
 */
class AddEmailVerifiedToUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('email_verified', 'boolean', [
            'default' => true,
            'null' => false,
            'comment' => 'Email address verification status',
            'after' => 'phone'
        ]);
        $table->update();
    }
}
