<?php

use Migrations\AbstractMigration;

/**
 * Class AddRejectReasonToUsers
 */
class AddRejectReasonToUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('reject_reason', 'string', [
            'null' => true,
            'limit'=> 255,
            'comment' => 'Reject reason',
            'after' => 'deleted'
        ]);
        $table->update();
    }
}
