<?php
use Migrations\AbstractMigration;

/**
 * Class CreateUserTokens
 */
class CreateUserTokens extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('user_tokens')
            ->addColumn('user_id', 'biginteger', [
                'limit' => 20,
                'null' => false,
                'signed' => false,
            ])->addColumn('token', 'char', [
                'default' => null,
                'limit' => 32,
                'null' => false
            ])
            ->addColumn('used', 'datetime', [
                'default' => null,
                'null' => true
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => true
            ])
            ->addIndex(['token'], ['unique' => true])
            ->addIndex(['user_id']);

        $table->create();

        $table->changeColumn('id', 'biginteger', [
            'default' => null,
            'limit' => 20,
            'null' => false,
            'signed' => false,
            'identity' => true
        ]);

        $table->update();

        $table->addForeignKey(
            'user_id',
            'users',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'CASCADE'
            ]
        );

        $table->update();
    }
}
