<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @author  Stefan <stefan@econnect4u.pl>
 * @date (2019-02-21)
 * @version 1.2
 */
use Migrations\AbstractMigration;

/**
 * Class Initial
 */
class CreateArticleChannels extends AbstractMigration
{

    public $autoId = false;

    /**
     * Create migrations
     *
     * @return void
     */
    public function up()
    {
        $this->table('channels', ['commment' => 'Channels - Each has a landing page (news ,case studies).'])
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('name', 'string', [
                'comment' => 'Mandatory - channel name',
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('label', 'string', [
                'comment' => 'Mandatory - Shorthand for channel name',
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('description', 'text', [
                'comment' => 'Optional - channel description',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('description2', 'text', [
                'comment' => 'Optional - channel description',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('sponsors', 'text', [
                'comment' => 'Optional - sponsors description',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('advert1', 'text', [
                'comment' => 'Optional - advert',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('advert2', 'text', [
                'comment' => 'Optional - advert',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('advert3', 'text', [
                'comment' => 'Optional - advert',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('advert4', 'text', [
                'comment' => 'Optional - advert',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('seo_title', 'string', [
                'default' => null,
                'limit'   => 600,
                'null'    => true,
            ])
            ->addColumn('seo_keywords', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('seo_description', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('seo_priority', 'float', [
                'default'   => '0.8',
                'null'      => true,
                'precision' => 2,
                'scale'     => 1,
            ])
            ->addColumn('og_title', 'string', [
                'default' => null,
                'limit'   => 100,
                'null'    => true,
            ])
            ->addColumn('og_image', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('og_description', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('css', 'text', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('slug', 'string', [
                'comment' => 'Mandatory - unique slug part of URL for channel name',
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('image', 'string', [
                'comment' => 'Optional - channel image',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('position', 'integer', [
                'comment' => 'Sort order value',
                'default' => '0',
                'limit'   => 10,
                'null'    => false,
            ])
            ->addColumn('active', 'boolean', [
                'default' => true,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'slug',
                ],
                ['unique' => true]
            )
            ->create();


        $this->table('articles')
            ->addColumn('channel_id', 'biginteger', [
                'comment' => 'Channel ID (channels.id)',
                'default' => null,
                'limit'   => 20,
                'null'    => true,
                'signed'  => false,
            ])
            ->update();


        $this->table('articles')
            ->addForeignKey(
                'channel_id',
                'channels',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'SET NULL'
                ]
            )
            ->update();

    }

    /**
     * Create migrations
     *
     * @return void
     */
    public function down()
    {
        $this->table('articles')
            ->dropForeignKey(
                'channel_id'
            );

        $this->table('articles')
            ->removeColumn('channel_id')
            ->update();

        $this->table('channels')->drop();
    }

}
