<?php

use Migrations\AbstractMigration;

class AddSendInEmailToServices extends AbstractMigration
{

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('services')->addColumn('send_in_email', 'boolean', [
                'after' => 'visibility',
                'comment' => 'Send in latest businesses emails',
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->update();
    }

}
