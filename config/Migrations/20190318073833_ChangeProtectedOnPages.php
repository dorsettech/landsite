<?php

/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */
use Migrations\AbstractMigration;

/**
 * ChangeProtectedOnPages
 */
class ChangeProtectedOnPages extends AbstractMigration
{

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('pages');
        $table->changeColumn('protected', 'boolean', [
            'default' => false,
            'limit'   => null,
            'null'    => false,
        ])->update();
    }
}
