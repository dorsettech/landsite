<?php
use Migrations\AbstractMigration;

class AddDescriptionShortToServices extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('services');
        $table->addColumn('description_short', 'string', [
            'comment' => 'Mandatory - business short overview (plain)',
            'limit' => 255,
            'null'    => false,
            'after'   => 'description'
        ]);
        $table->update();
    }
}
