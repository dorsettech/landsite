<?php
use Migrations\AbstractMigration;

class ChangePriceRentPerOnProperties extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('properties');
        $table->changeColumn('price_rent_per', 'enum', [
            'values' => ['M', 'Y', 'FT2', 'M2', 'POA'],
            'comment' => 'Property rented per: M - month, Y - year/annum, FT2 - square feet, M2 - square meter, POA - price on application',
            'default' => 'M',
            'null' => false,
            'after' => 'price_rent'
        ])->update();
    }
}
