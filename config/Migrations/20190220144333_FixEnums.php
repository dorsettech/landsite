<?php

use Migrations\AbstractMigration;

/**
 * FixEnums
 */
class FixEnums extends AbstractMigration
{

    /**
     * Up method
     *
     * @return void
     */
    public function up()
    {
        $this->table('articles')
            ->changeColumn('type', 'enum', [
                'values'  => ['INSIGHT', 'CASE_STUDY'],
                'comment' => 'Mandatory - Article type (Insight or Case Study)',
                'default' => null,
                'null'    => false,
            ])
            ->changeColumn('status', 'enum', [
                'values'  => ['DRAFT', 'PUBLISHED'],
                'comment' => 'Primary article statuses: DRAFT - article outline, PUBLISHED - article saved as ready to publish by user',
                'default' => 'DRAFT',
                'null'    => true,
            ])
            ->changeColumn('state', 'enum', [
                'values'  => ['APPROVED', 'PENDING', 'REJECTED'],
                'comment' => 'Administration status: APPROVED - reviewed and approved by admin, PENDING - waiting for review, REJECTED - content rejected by admin',
                'default' => null,
                'null'    => true,
            ])
            ->update();

        $this->table('credentials')
            ->changeColumn('type', 'enum', [
                'values'  => ['ACC', 'ASS'],
                'comment' => 'Mandatory - ACC (Accreditation), ASS (Association)',
                'default' => null,
                'null'    => false,
            ])
            ->update();

        $this->table('payments')
            ->changeColumn('type', 'enum', [
                'values'  => ['PROPERTY', 'SERVICE'],
                'comment' => 'Mandatory - what the payment applies to',
                'default' => null,
                'null'    => true,
            ])
            ->update();

        $this->table('properties')
            ->changeColumn('purpose', 'enum', [
                'values'  => ['SALE', 'RENT', 'BOTH'],
                'comment' => 'Mandatory - property purpose',
                'default' => null,
                'null'    => false,
            ])
            ->changeColumn('status', 'enum', [
                'values'  => ['DRAFT', 'PUBLISHED', 'EXPIRED'],
                'comment' => 'Property statuses: DRAFT - property outline, PUBLISHED - property saved and published on website, EXPIRED - property has expired (ad_expiry_date)',
                'default' => 'DRAFT',
                'null'    => false,
            ])
            ->changeColumn('ad_type', 'enum', [
                'values'  => ['STANDARD', 'FEATURED', 'PREMIUM'],
                'comment' => 'Type of ad/announcement package (defined in spec)',
                'default' => 'STANDARD',
                'null'    => false,
            ])
            ->update();

        $this->table('property_media')
            ->changeColumn('type', 'enum', [
                'values'  => ['IMAGE', 'VIDEO', 'DOCUMENT'],
                'comment' => 'Mandatory - media type (image file, document file or link to video clip). ',
                'default' => null,
                'null'    => false,
            ])
            ->update();

        $this->table('property_searches')
            ->changeColumn('purpose', 'enum', [
                'values'  => ['SALE', 'RENT', 'BOTH'],
                'comment' => 'Optional - property purpose',
                'default' => null,
                'null'    => true,
            ])
            ->update();

        $this->table('services')
            ->changeColumn('status', 'enum', [
                'values'  => ['DRAFT', 'PUBLISHED', 'EXPIRED'],
                'comment' => 'Service statuses: DRAFT - business outline, PUBLISHED - service saved and published on website, EXPIRED - service has expired (ad_expiry_date)',
                'default' => 'DRAFT',
                'null'    => false,
            ])
            ->changeColumn('ad_type', 'enum', [
                'values'  => ['STANDARD', 'FEATURED', 'PREMIUM'],
                'comment' => 'Type of ad/announcement package (defined in spec)',
                'default' => 'STANDARD',
                'null'    => false,
            ])
            ->update();

        $this->table('service_media')
            ->changeColumn('type', 'enum', [
                'values'  => ['IMAGE', 'VIDEO'],
                'comment' => 'Mandatory - media type (image file, link to video clip). ',
                'default' => null,
                'null'    => false,
            ])
            ->update();

        $this->table('settings')
            ->changeColumn('type', 'enum', [
                'values'  => ['BOOL', 'INT', 'FLOAT', 'STRING', 'JSON'],
                'comment' => 'Value type',
                'default' => 'STRING',
                'null'    => false,
            ])
            ->update();

        $this->table('user_details')
            ->changeColumn('dashboard_type', 'enum', [
                'values'  => ['S', 'P', 'SP'],
                'comment' => 'Mandatory - dashboard type (caching current state). NULL - empty, S - seller, P - professional, SP - seller/professional',
                'default' => null,
                'null'    => true,
            ])
            ->update();
    }

    /**
     * Down method
     *
     * @return void
     */
    public function down()
    {
        $this->table('user_details')
            ->changeColumn('dashboard_type', 'string', [
                'comment' => 'Mandatory - dashboard type (caching current state). NULL - empty, S - seller, P - professional, SP - seller/professional',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->update();

        $this->table('settings')
            ->changeColumn('type', 'string', [
                'comment' => 'Value type',
                'default' => 'STRING',
                'limit'   => null,
                'null'    => false,
            ])
            ->update();

        $this->table('service_media')
            ->changeColumn('type', 'string', [
                'comment' => 'Mandatory - media type (image file, link to video clip). ',
                'default' => null,
                'limit'   => null,
                'null'    => false,
            ])
            ->update();

        $this->table('services')
            ->changeColumn('ad_type', 'string', [
                'comment' => 'Type of ad/announcement package (defined in spec)',
                'default' => 'STANDARD',
                'limit'   => null,
                'null'    => false,
            ])
            ->changeColumn('status', 'string', [
                'comment' => 'Service statuses: DRAFT - business outline, PUBLISHED - service saved and published on website, EXPIRED - service has expired (ad_expiry_date)',
                'default' => 'DRAFT',
                'limit'   => null,
                'null'    => false,
            ])
            ->update();

        $this->table('property_searches')
            ->changeColumn('purpose', 'string', [
                'comment' => 'Optional - property purpose',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->update();

        $this->table('property_media')
            ->changeColumn('type', 'string', [
                'comment' => 'Mandatory - media type (image file, document file or link to video clip). ',
                'default' => null,
                'limit'   => null,
                'null'    => false,
            ])
            ->update();

        $this->table('properties')
            ->changeColumn('ad_type', 'string', [
                'comment' => 'Type of ad/announcement package (defined in spec)',
                'default' => 'STANDARD',
                'limit'   => null,
                'null'    => false,
            ])
            ->changeColumn('status', 'string', [
                'comment' => 'Property statuses: DRAFT - property outline, PUBLISHED - property saved and published on website, EXPIRED - property has expired (ad_expiry_date)',
                'default' => 'DRAFT',
                'limit'   => null,
                'null'    => false,
            ])
            ->changeColumn('purpose', 'string', [
                'comment' => 'Mandatory - property purpose',
                'default' => null,
                'limit'   => null,
                'null'    => false,
            ])
            ->update();

        $this->table('payments')
            ->changeColumn('type', 'string', [
                'comment' => 'Mandatory - what the payment applies to',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->update();

        $this->table('credentials')
            ->changeColumn('type', 'string', [
                'comment' => 'Mandatory - Article type (Insight or Case Study)',
                'default' => null,
                'limit'   => null,
                'null'    => false,
            ])
            ->update();

        $this->table('articles')
            ->changeColumn('type', 'string', [
                'comment' => 'Mandatory - Article type (Insight or Case Study)',
                'default' => null,
                'limit'   => null,
                'null'    => false,
            ])
            ->changeColumn('status', 'string', [
                'comment' => 'Primary article statuses: DRAFT - article outline, PUBLISHED - article saved as ready to publish by user',
                'default' => 'DRAFT',
                'limit'   => null,
                'null'    => true,
            ])
            ->changeColumn('state', 'string', [
                'comment' => 'Administration status: APPROVED - reviewed and approved by admin, PENDING - waiting for review, REJECTED - content rejected by admin',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->update();
    }
}
