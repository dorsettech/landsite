<?php
use Migrations\AbstractMigration;

class AddVatToPayments extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('payments')->addColumn('vat','decimal', [
            'comment'   => 'Mandatory - payment vat value',
            'default'   => 0,
            'null'      => false,
            'precision' => 10,
            'scale'     => 2,
            'signed'    => false,
            'after'     => 'amount_pos'
        ])->addColumn('vat_rate','decimal', [
            'comment'   => 'Mandatory - payment vat rate (in percent)',
            'default'   => 0,
            'null'      => false,
            'precision' => 4,
            'scale'     => 2,
            'signed'    => false,
            'after'     => 'vat'
        ])->update();
    }
}
