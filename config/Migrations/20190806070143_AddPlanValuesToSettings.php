<?php
use Migrations\AbstractMigration;

class AddPlanValuesToSettings extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('settings');
        $table->insert([
            [
                'id' => 33,
                'key' => 'service_2-featured',
                'group' => 'plans',
                'value' => 'true',
                'type' => 'BOOL',
                'description' => 'Enable or Disable the plan',
                'editable' => 1
            ], [
                'id' => 34,
                'key' => 'service_2-premium',
                'group' => 'plans',
                'value' => 'true',
                'type' => 'BOOL',
                'description' => 'Enable or Disable the plan',
                'editable' => 1
            ], [
                'id' => 35,
                'key' => 'service_2-standard-days',
                'group' => 'validity',
                'value' => 180,
                'type' => 'INT',
                'description' => 'The ad available for N days from the date it was published as Standard',
                'editable' => 1
            ], [
                'id' => 36,
                'key' => 'service_2-featured-days',
                'group' => 'validity',
                'value' => 180,
                'type' => 'INT',
                'description' => 'The ad available for N days from the date it was published as Featured',
                'editable' => 1
            ], [
                'id' => 37,
                'key' => 'service_2-premium-days',
                'group' => 'validity',
                'value' => 180,
                'type' => 'INT',
                'description' => 'The ad available for N days from the date it was published as Premium',
                'editable' => 1
            ], [
                'id' => 38,
                'key' => 'service_2-standard',
                'group' => 'prices',
                'value' => 400,
                'type' => 'FLOAT',
                'description' => 'Adding a business listing',
                'editable' => 1
            ], [
                'id' => 39,
                'key' => 'service_2-featured',
                'group' => 'prices',
                'value' => 900,
                'type' => 'FLOAT',
                'description' => 'Upgrade to Featured business listing',
                'editable' => 1
            ], [
                'id' => 40,
                'key' => 'service_2-premium',
                'group' => 'prices',
                'value' => 600,
                'type' => 'FLOAT',
                'description' => 'Upgrade to Premium business listing',
                'editable' => 1
            ], [
                'id' => 41,
                'key' => 'service_2-standard',
                'group' => 'plans',
                'value' => 'true',
                'type' => 'BOOL',
                'description' => 'Enable or Disable the plan',
                'editable' => 1
            ]
        ]);
        $table->save();
    }
}
