<?php
/**
 * @author Stefan <marcin@econnect4u.pl>
 */

use Migrations\AbstractMigration;

/**
 * RenameAttributesRelatedTables
 */
class RenameAttributesRelatedTables extends AbstractMigration
{
    /**
     * Up method
     *
     * @return void
     */
    public function up()
    {
        $this->table('property_attributes')
            ->rename('properties_property_attributes');

        $this->table('property_attributes_property_types')
            ->rename('property_attributes');
    }
    /**
     * Down method
     *
     * @return void
     */
    public function down()
    {
        $this->table('property_attributes')
            ->rename('property_attributes_property_types');

        $this->table('properties_property_attributes')
            ->rename('property_attributes');
    }
}
