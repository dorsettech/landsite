<?php
use Migrations\AbstractMigration;

class ChangeDescriptionLimitOnProperties extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('properties')->changeColumn('description', 'text', [
            'comment' => 'Optional - full property description',
            'null' => true,
            'default' => ''
        ])->update();
    }
}
