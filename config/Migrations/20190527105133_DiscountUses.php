<?php
/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */

use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

/**
 * DiscountUses
 */
class DiscountUses extends AbstractMigration
{
    /**
     * Up method
     *
     * @return void
     */
    public function up()
    {
        $this->table('discount_uses', Initial::getDBCollations(['id' => false, 'comment' => 'The Land Site - discount code uses']))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => MysqlAdapter::INT_BIG,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('discount_id', 'integer', [
                'default' => null,
                'limit'   => MysqlAdapter::INT_REGULAR,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('user_id', 'biginteger', [
                'comment' => 'Instead of email, applies to: `null` for all members, any ID for specific member',
                'default' => null,
                'limit'   => MysqlAdapter::INT_BIG,
                'null'    => true,
                'signed'  => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->create();
    }

    /**
     * Down method
     *
     * @return void
     */
    public function down()
    {
        $this->table('discount_uses')
            ->drop();
    }
}
