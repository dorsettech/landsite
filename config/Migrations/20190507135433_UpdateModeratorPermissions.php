<?php

/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */
use Migrations\AbstractMigration;

/**
 * UpdateModeratorPermissions
 */
class UpdateModeratorPermissions extends AbstractMigration
{

    /**
     * Change method
     *
     * @return void
     */
    public function change()
    {
        $permissions = '{"Accreditations":{"index":"0","add":"0","edit":"0","delete":"0","getList":"0"},"Areas":{"index":"0","add":"0","edit":"0","delete":"0","getList":"0"},"Associations":{"index":"0","add":"0","edit":"0","delete":"0","getList":"0"},"CaseStudies":{"index":"0","add":"0","edit":"0","delete":"0","approveOrReject":"0","toggleColumnValue":"0"},"Categories":{"index":"0","add":"0","edit":"0","delete":"0","toggleColumnValue":"0","getList":"0"},"Contents":{"index":"0","delete":"0"},"Cropper":{"index":"0","crop":"0","restore":"0","cropFile":"0"},"CustomCss":{"index":"0","edit":"0","restore":"0"},"Dashboard":{"index":"1","getWebsiteAnalyticsData":"0","getDetailAnalyticsData":"0"},"ExportPdf":{"invoice":"0","order":"0"},"Forms":{"index":"0","add":"0","edit":"0","delete":"0","toggleColumnValue":"0"},"FormsData":{"index":"0","listView":"0","listToCsv":"0","view":"0","delete":"0","exportMessages":"0","exportAllForms":"0","recentMessages":"0"},"Gallery":{"manage":"0","getSingleImageData":"0","updateSingleImage":"0","updateGalleryOrder":"0","deleteSingleGalleryImage":"0"},"Groups":{"index":"0","add":"0","edit":"0","delete":"0","permissions":"0"},"Languages":{"index":"0","add":"0","edit":"0","delete":"0","toggle":"0","setLanguage":"0","toggleColumnValue":"0"},"Loginbans":{"index":"0","add":"0","edit":"0","delete":"0"},"Logs":{"index":"0","viewUser":"0","showVersions":"0","compareVersions":"0","revertVersion":"0","truncate":"0","delete":"0","filter":"0","saveContents":"0"},"Members":{"index":"0","add":"0","edit":"0","delete":"0","view":"0","getList":"0","approveOrReject":"0","toggleColumnValue":"0"},"Menus":{"index":"0","view":"0","add":"0","edit":"0","delete":"0","toggleColumnValue":"0"},"Miscellaneous":{"sitemap":"0"},"News":{"index":"1","add":"1","edit":"1","delete":"0","approveOrReject":"0","toggleColumnValue":"0"},"PageFiles":{"edit":"0","delete":"0","addFilesToPage":"0"},"PageTypes":{"index":"0","add":"0","edit":"0","delete":"0"},"Pages":{"index":"0","add":"0","edit":"0","editContents":"0","delete":"0","clonePage":"0","updatePage":"0","updateElementField":"0","ajaxList":"0","changeRedirectionFrom":"0","editFiles":"0","toggleColumnValue":"0"},"Payments":{"index":"0"},"Products":{"index":"0","add":"0","edit":"0","delete":"0","getList":"0"},"Properties":{"index":"0","add":"0","edit":"0","delete":"0","view":"0"},"PropertyAttributes":{"index":"0","add":"0","edit":"0","delete":"0","getByType":"0","toggleColumnValue":"0"},"PropertyMedia":{"list":"0","add":"0","edit":"0","delete":"0","getTypeFromMime":"0","mediaTypeFromMime":"0"},"PropertySearches":{"index":"0","delete":"0","toggleColumnValue":"0"},"PropertyTypes":{"index":"0","add":"0","edit":"0","delete":"0","toggleColumnValue":"0"},"Redirections":{"index":"0","add":"0","edit":"0","delete":"0"},"SeoAnalyser":{"index":"0","editable":"0","upload":"0"},"ServiceMedia":{"list":"0","add":"0","edit":"0","delete":"0","getTypeFromMime":"0","mediaTypeFromMime":"0"},"ServiceSearches":{"index":"0","delete":"0","toggleColumnValue":"0"},"Services":{"index":"0","add":"0","edit":"0","delete":"0","view":"0"},"Settings":{"prices":"0"},"StaticContentGroups":{"index":"0","add":"0","view":"0","editGroup":"0","edit":"0","delete":"0"},"StaticContents":{"add":"0","edit":"0","delete":"0"},"Users":{"index":"0","add":"0","edit":"1","delete":"0","login":"1","logout":"1","unlockScreen":"1","refreshOnAlertClick":"1","token":"1","generateToken":"1","toggleColumnValue":"0"},"Manager":{"index":"0","upload":"0","file":"0","addFolder":"0","deleteFolder":"0","deleteFile":"0","deleteFiles":"0","renameFile":"0"}}';

        $builder = $this->execute("UPDATE groups SET permissions = '".$permissions."' WHERE id = " . \App\Model\Entity\Group::MODERATOR);
    }
}
