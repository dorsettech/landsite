<?php

use Migrations\AbstractMigration;

class AddPhoneToServices extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $this->table('services')->addColumn('phone', 'string', [
            'comment' => 'Optional - business profile phone number',
            'default' => null,
            'limit' => 50,
            'null' => true,
            'after' => 'company'
        ])->update();
    }
}
