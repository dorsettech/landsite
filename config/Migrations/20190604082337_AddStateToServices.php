<?php
use Migrations\AbstractMigration;

class AddStateToServices extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('services')->addColumn('state', 'enum', [
            'values'  => ['APPROVED', 'PENDING', 'REJECTED'],
            'comment' => 'Administration status: APPROVED - reviewed and approved by admin, PENDING - waiting for review, REJECTED - content rejected by admin',
            'default' => null,
            'after' => 'status',
            'null'    => true
        ])->addColumn('rejection_reason', 'string', [
            'comment' => 'Rejection reason, if state = REJECTED',
            'default' => null,
            'limit'   => 255,
            'after' => 'state',
            'null'    => true
        ])->update();

        $this->execute("UPDATE services SET `state` = 'APPROVED' WHERE `status` = 'PUBLISHED'");
    }
}
