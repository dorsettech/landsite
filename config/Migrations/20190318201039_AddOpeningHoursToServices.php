<?php
use Migrations\AbstractMigration;

class AddOpeningHoursToServices extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('services');
        $table->addColumn('opening_hours', 'text', [
            'comment' => 'Optional - opening hours (array of dates and times)',
            'null'    => true,
            'default' => null,
            'after'   => 'opening_days'
        ]);

        $table->update();
    }
}
