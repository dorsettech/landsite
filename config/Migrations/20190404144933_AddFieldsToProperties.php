<?php

/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */
use Migrations\AbstractMigration;

/**
 * AddFieldsToProperties
 */
class AddFieldsToProperties extends AbstractMigration
{
    /**
     * Up method
     *
     * @return void
     */
    public function up()
    {
        $this->table('properties')
            ->addColumn('seo_title', 'string', [
                'after'   => 'slug',
                'default' => null,
                'limit'   => 600,
                'null'    => true,
            ])
            ->addColumn('seo_keywords', 'string', [
                'after'   => 'seo_title',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('seo_description', 'string', [
                'after'   => 'seo_keywords',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('seo_priority', 'float', [
                'after'     => 'seo_description',
                'default'   => '0.8',
                'null'      => true,
                'precision' => 2,
                'scale'     => 1,
            ])
            ->addColumn('deleted', 'boolean', [
                'after'   => 'views',
                'default' => false,
                'limit'   => null,
                'null'    => false,
            ])
            ->update();
    }

    /**
     * Down method
     *
     * @return void
     */
    public function down()
    {
        $this->table('properties')
            ->removeColumn('deleted')
            ->removeColumn('seo_priority')
            ->removeColumn('seo_description')
            ->removeColumn('seo_keywords')
            ->removeColumn('seo_title')
            ->update();
    }
}
