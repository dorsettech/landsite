<?php
use Migrations\AbstractMigration;

class InsertNewVars4ToStaticContents extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function up(): void
    {
        $now = date('Y-m-d H:i:s');
        $data = [
            [
                'static_group_id' => 9,
                'var_name' => 'plan_standard_price_line_3',
                'value' => '',
                'type' => 0,
                'description' => 'Plan Standard - price (line 3)',
                'created' => $now
            ], [
                'static_group_id' => 9,
                'var_name' => 'plan_premium_price_line_3',
                'value' => '',
                'type' => 0,
                'description' => 'Plan Premium - price (line 3)',
                'created' => $now
            ], [
                'static_group_id' => 9,
                'var_name' => 'plan_featured_price_line_3',
                'value' => '',
                'type' => 0,
                'description' => 'Plan Featured - price (line 3)',
                'created' => $now
            ]
        ];

        $this->insert('static_contents', $data);
    }

    public function down(): void
    {
        $this->execute('DELETE from static_contents WHERE var_name IN ("plan_standard_price_line_3", "plan_premium_price_line_3", "plan_featured_price_line_3")');
    }
}
