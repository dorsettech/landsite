<?php

use Migrations\AbstractMigration;

/**
 * Class AddAdPlanTypeToServices
 */
class AddAdPlanTypeToServices extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change(): void
    {
        $this->table('services')->addColumn('ad_plan_type', 'string', [
            'after' => 'ad_type',
            'null' => false,
            'limit' => 10,
            'comment' => 'Ad Plan Type - default is service',
            'default' => 'service'
        ])->update();
    }
}
