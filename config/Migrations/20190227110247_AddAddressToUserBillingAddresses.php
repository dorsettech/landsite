<?php
use Migrations\AbstractMigration;

class AddAddressToUserBillingAddresses extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('user_billing_addresses');
        $table->addColumn('address', 'string', [
            'comment' => 'Optional - billing address',
            'default' => null,
            'limit'   => 255,
            'null'    => true,
            'after'   => 'postcode'
        ])->update();
    }
}
