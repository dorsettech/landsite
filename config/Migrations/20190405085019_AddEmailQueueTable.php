<?php

use Migrations\AbstractMigration;

class AddEmailQueueTable extends AbstractMigration
{

    /**
     * up function
     *
     * @return void
     */
    public function up()
    {
        $this->table('email_queue', array_merge(Initial::getDBCollations(), ['id' => false]))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 20,
                'null' => false,
                'signed' => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('email', 'string', [
                'comment' => 'Recipient email address',
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('subject', 'string', [
                'comment' => 'Subject of email',
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('content', 'text', [
                'comment' => 'Content of email',
                'default' => null,
                'limit' => 4294967295,
                'null' => false,
            ])
            ->addColumn('is_sent', 'boolean', [
                'comment' => 'Is email sent',
                'default' => false,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('options', 'text', [
                'comment' => 'Options',
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('send_date', 'datetime', [
                'comment' => 'Email send date',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->create();
    }

    /**
     * down function
     *
     * @return void
     */
    public function down()
    {
        $this->table('email_queue')->drop();
    }

}
