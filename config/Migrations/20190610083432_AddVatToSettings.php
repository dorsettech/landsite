<?php

use App\Model\Enum\SettingType;
use Migrations\AbstractMigration;

class AddVatToSettings extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('settings');
        $table->insert([
            'key' => 'vat',
            'group' => 'prices',
            'value' => 20,
            'type' => SettingType::FLOAT,
            'description' => 'VAT rate in percent'
        ])->save();
    }
}
