<?php
/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */

use Migrations\AbstractMigration;

/**
 * RenamePropertiesPropertyAttributesColumn
 */
class RenamePropertiesPropertyAttributesColumn extends AbstractMigration
{
    /**
     * Up method
     *
     * @return void
     */
    public function up()
    {
        $this->table('properties_property_attributes')
            ->renameColumn('attribute_id', 'property_attribute_id')
            ->update();
    }

    /**
     * Down method
     *
     * @return void
     */
    public function down()
    {
        $this->table('properties_property_attributes')
            ->renameColumn('property_attribute_id', 'attribute_id')
            ->update();
    }
}
