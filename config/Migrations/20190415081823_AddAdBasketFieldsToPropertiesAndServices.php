<?php

use Migrations\AbstractMigration;

/**
 * Class AddAdBasketFieldsToPropertiesAndServices
 */
class AddAdBasketFieldsToPropertiesAndServices extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('properties');
        $table->addColumn('ad_basket', 'text', [
            'comment' => 'Latest basket if the transaction is interrupted. Collated with visibility = 0',
            'default' => null,
            'null' => true,
            'after' => 'ad_total_cost'
        ]);
        $table->update();

        $table = $this->table('services');
        $table->addColumn('ad_basket', 'text', [
            'comment' => 'Latest basket if the transaction is interrupted. Collated with visibility = 0',
            'default' => null,
            'null' => true,
            'after' => 'ad_total_cost'
        ]);
        $table->update();
    }
}
