<?php

use Migrations\AbstractMigration;

/**
 * Class AddVisibilityFieldsToPropertiesAndServices
 */
class AddVisibilityFieldsToPropertiesAndServices extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('properties');
        $table->addColumn('visibility', 'boolean', [
            'comment' => 'Extra visibility flag. 1 - visible, 0 - hidden',
            'default' => true,
            'limit' => null,
            'null' => false,
            'after' => 'views'
        ]);
        $table->update();

        $table = $this->table('services');
        $table->addColumn('visibility', 'boolean', [
            'comment' => 'Extra visibility flag. 1 - visible, 0 - hidden',
            'default' => true,
            'limit' => null,
            'null' => false,
            'after' => 'views'
        ]);
        $table->update();
    }
}
