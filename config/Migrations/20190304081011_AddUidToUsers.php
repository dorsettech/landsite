<?php
use Migrations\AbstractMigration;

class AddUidToUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('uid', 'string', [
            'comment' => 'Optional - mandatory only for members',
            'limit' => 32,
            'null'    => true,
            'default' => null,
            'after'   => 'id'
        ]);

        $table->update();

        $orm = (new \Cake\ORM\Locator\TableLocator)->get('Users');
        $exp = $orm->query()->newExpr('REPLACE(UUID(), "-","")');
        $orm->updateAll([
            'uid' => $exp
        ], []);

        $table->changeColumn('uid', 'string', [
            'null'    => false,
        ])->addIndex(['uid'], ['unique' => true]);

        $table->update();
    }
}
