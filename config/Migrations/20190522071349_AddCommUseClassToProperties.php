<?php
use Migrations\AbstractMigration;

class AddCommUseClassToProperties extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('properties')->addColumn('comm_use_class', 'set', [
            'values' => [
                'A1', 'A2', 'A3', 'A4', 'A5', 'B1', 'B2', 'B8', 'C1', 'C2', 'C2A', 'C3', 'D1', 'D2', 'SUI1', 'SUI2'
            ],
            'comment' => 'Optional - Common Use Class',
            'default' => null,
            'null' => true,
            'after' => 'purpose'
        ])->update();
    }
}
