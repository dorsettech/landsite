<?php

use Migrations\AbstractMigration;

class AddEmailQueueMembersTable extends AbstractMigration
{

    /**
     * up function
     *
     * @return void
     */
    public function up()
    {
        $this->table('email_queue_members', array_merge(Initial::getDBCollations(), ['id' => false]))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('first_name', 'string', [
                'comment' => 'Member\'s first name',
                'default' => null,
                'limit'   => 50,
                'null'    => false,
            ])
            ->addColumn('email', 'string', [
                'comment' => 'Member\'s email address',
                'default' => null,
                'limit'   => 50,
                'null'    => false,
            ])
            ->addColumn('email_type', 'enum', [
                'comment' => 'Email type (Insights, Properties (Buying, Selling), Professionals',
                'values'  => ['BUYING', 'SELLING', 'INSIGHTS', 'PROFESSIONALS'],
                'default' => null,
                'null'    => true,
            ])
            ->addColumn('options', 'text', [
                'comment' => 'Additional options',
                'default' => null,
                'limit'   => 4294967295,
                'null'    => true,
            ])
            ->create();
    }

    public function down()
    {
        $this->table('email_queue_members')->drop();
    }

}
