<?php

/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */
use Migrations\AbstractMigration;

/**
 * DiscountCodeLimitChange
 */
class DiscountCodeLimitChange extends AbstractMigration
{
    /**
     * Change
     *
     * @return void
     */
    public function change()
    {
        $this->table('discounts')->changeColumn('code', 'string', [
            'comment' => 'Coupon code name',
            'default' => null,
            'limit'   => 30,
            'null'    => false,
        ])->update();
    }
}
