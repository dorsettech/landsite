<?php

use Migrations\AbstractMigration;

/**
 * @channel `./cake migrations migrate`
 */
class AddAuctionToProperties extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $this->table('properties')->addColumn('auction', 'boolean', [
            'comment' => 'Auction flag',
            'after' => 'under_offer',
            'default' => false,
            'limit' => null,
            'null' => false
        ])->update();
    }
}
