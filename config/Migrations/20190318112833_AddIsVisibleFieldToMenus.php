<?php
/**
 * eConnect4u
 *
 * @author Stefan <marcin.nierobis@econnect4u.pl>
 */

use Migrations\AbstractMigration;

/**
 * AddIsVisibleFieldToMenus
 */
class AddIsVisibleFieldToMenus extends AbstractMigration
{


    /**
     * Up method
     *
     * @return void
     */
    public function up()
    {
        $this->table('menus')
            ->addColumn('is_visible', 'boolean', [
                'after'   => 'image',
                'default' => false,
            ])
            ->update();
    }

    /**
     * Down method
     *
     * @return void
     */
    public function down()
    {
        $this->table('menus')
            ->removeColumn('is_visible')
            ->update();
    }
}
