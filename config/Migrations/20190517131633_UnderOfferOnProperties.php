<?php
/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */

use Migrations\AbstractMigration;

/**
 * UnderOfferOnProperties
 */
class UnderOfferOnProperties extends AbstractMigration
{
    /**
     * Up method
     *
     * @return void
     */
    public function up()
    {
        $this->table('properties')
            ->addColumn('under_offer', 'boolean', [
                'comment' => 'Under offer flag/label',
                'after'   => 'views',
                'default' => false,
                'limit'   => null,
                'null'    => false,
            ])
            ->update();
    }

    /**
     * Down method
     *
     * @return void
     */
    public function down()
    {
        $this->table('properties')
            ->removeColumn('under_offer')
            ->update();
    }
}
