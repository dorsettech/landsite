<?php

use Migrations\AbstractMigration;

class AddUrlToPropertyMedia extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $this->table('property_media')->addColumn('url', 'string', [
            'limit' => 2048,
            'null' => true,
            'default' => null,
            'after' => 'extra',
            'comment' => 'The URL address of the file it coming from. Used by API.'
        ])->update();
    }
}
