<?php

use Migrations\AbstractMigration;

class InsertNewVars3ToStaticContents extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function up(): void
    {
        $now = date('Y-m-d H:i:s');
        $data = [
            [
                'static_group_id' => 9,
                'var_name' => 'image_text',
                'value' => 'Not ready to create an account yet? Browse our platform for FREE...',
                'type' => 0,
                'description' => 'Image - text under the registration form',
                'created' => $now
            ], [
                'static_group_id' => 9,
                'var_name' => 'image_button_label',
                'value' => 'Visit The Landsite',
                'type' => 0,
                'description' => 'Image - Button label',
                'created' => $now
            ], [
                'static_group_id' => 9,
                'var_name' => 'image_button_href',
                'value' => '//thelandsite.co.uk',
                'type' => 0,
                'description' => 'Image - Button URL',
                'created' => $now
            ]
        ];

        $this->insert('static_contents', $data);
    }

    public function down()
    {
        $this->execute('DELETE from static_contents WHERE var_name IN ("image_text", "image_button_label", "image_button_href")');
    }
}
