<?php
use Migrations\AbstractMigration;

class RemoveCoordsFromServicesProperties extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $services = $this->table('services');
        $services->removeColumn('coords');
        $services->update();

        $properties = $this->table('properties');
        $properties->removeColumn('coords');
        $properties->update();
    }
}
