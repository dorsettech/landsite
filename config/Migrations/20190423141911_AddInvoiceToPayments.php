<?php

use Migrations\AbstractMigration;

/**
 * Class AddInvoiceToPayments
 */
class AddInvoiceToPayments extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('payments');
        $table->addColumn('invoice', 'string', [
            'null' => true,
            'limit'=> 255,
            'comment' => 'Invoice file name',
            'after' => 'meta'
        ]);
        $table->update();
    }
}
