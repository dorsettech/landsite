<?php
use Migrations\AbstractMigration;

class ChangeDescriptionOnProperties extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('properties');
        $table->changeColumn('description', 'string', [
            'comment' => 'Field optional as DRAFT has to be saved',
            'default' => '',
            'limit' => 10000,
            'null' => true
        ])->update();
    }
}
