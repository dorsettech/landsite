<?php
use Migrations\AbstractMigration;

class ChangeDescriptionOnServices extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('services');
        $table->changeColumn('description', 'string', [
            'default' => '',
            'null' => true
        ])->update();

        $table->addColumn('location', 'string', [
            'comment' => 'Optional - location string (tip from geocoding api)',
            'limit' => 255,
            'null'    => true,
            'default' => null,
            'after'   => 'postcode'
        ])->update();
    }
}
