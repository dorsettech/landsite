<?php

use Migrations\AbstractMigration;

/**
 * Class AddObjectIdFieldToPayments
 */
class AddObjectIdFieldToPayments extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('payments');
        $table->addColumn('object_id', 'biginteger', [
            'comment' => 'Related object ID ie. service id, property id',
            'default' => null,
            'limit' => 20,
            'null' => true,
            'signed' => false,
            'after' => 'charge_id'
        ]);
        $table->update();
    }
}
