<?php

use App\Model\Entity\Group;
use Cake\ORM\TableRegistry;
use Migrations\AbstractMigration;

class AddProfileCompletionToUserDetails extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change(): void
    {
        $table = $this->table('user_details');

        $table->addColumn('profile_completion', 'boolean', [
            'comment' => '1 - profile completed, 0 - the user is just after registration',
            'after' => 'dashboard_type',
            'default' => false,
            'limit' => null,
            'null' => false
        ]);

        $table->update();

        /**
         * Members Id who filled profile in the past.
         */
        $ids = TableRegistry::getTableLocator()->get('Users')->query()->select(['id'])->where(['token IS NOT' => null, 'group_id' => Group::MEMBERS])->extract('id')->toArray();

        if (count($ids)) {
            TableRegistry::getTableLocator()->get('UserDetails')->updateAll(['profile_completion' => true], ['user_id IN' => $ids]);
        }
    }
}
