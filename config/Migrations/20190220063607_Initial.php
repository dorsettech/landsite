<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @author  Stefan <stefan@econnect4u.pl>
 * @date (2019-02-21)
 * @version 1.2
 */
use Migrations\AbstractMigration;

/**
 * Class Initial
 */
class Initial extends AbstractMigration
{
    public $autoId = false;

    /**
     * DB default collations settings
     *
     * @param mixed $options Additional options as an array or comment as a string.
     * @return array
     */
    public static function getDBCollations($options = [])
    {
        $engine  = !empty($options['engine']) ? $options['engine'] : 'InnoDB';
        $comment = !empty($options['comment']) ? $options['comment'] : '';

        if (is_string($options)) {
            $comment = $options;
        }

        unset($options['engine'], $options['comment']);

        $default = [
            'collation' => 'utf8_general_ci',
            'engine'    => $engine,
            'comment'   => $comment
        ];

        return array_merge($default, $options);
    }

    /**
     * Create migrations
     *
     * @return void
     */
    public function up()
    {
        //clear all tables before start
        $this->down();

        $this->table('areas', self::getDBCollations('The Land Site - areas covered by businesses (professional services)'))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('name', 'string', [
                'comment' => 'Mandatory - Name of area (county)',
                'default' => null,
                'limit'   => 50,
                'null'    => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->create();

        $this->table('articles', self::getDBCollations('The Land Site - articles (insights, case studies)'))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('author_id', 'biginteger', [
                'comment' => 'Mandatory - Article creator ID',
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('posted_by', 'biginteger', [
                'comment' => 'Article publisher ID',
                'default' => null,
                'limit'   => 20,
                'null'    => true,
                'signed'  => false,
            ])
            ->addColumn('category_id', 'biginteger', [
                'comment' => 'Mandatory - category ID (categories.id)',
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('type', 'string', [
                'comment' => 'Mandatory - Article type (Insight or Case Study)',
                'default' => null,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('title', 'string', [
                'comment' => 'Mandatory - Article full title',
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('slug', 'string', [
                'comment' => 'Mandatory - unique slug part of URL for article title',
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('headline', 'text', [
                'comment' => 'Mandatory - article heading (first paragraph)',
                'default' => null,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('body', 'text', [
                'comment' => 'Mandatory - article content',
                'default' => null,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('image', 'string', [
                'comment' => 'Optional - article image',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('status', 'string', [
                'comment' => 'Primary article statuses: DRAFT - article outline, PUBLISHED - article saved as ready to publish by user',
                'default' => 'DRAFT',
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('state', 'string', [
                'comment' => 'Administration status: APPROVED - reviewed and approved by admin, PENDING - waiting for review, REJECTED - content rejected by admin',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('rejection_reason', 'string', [
                'comment' => 'Rejection reason, if state = REJECTED',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('publish_date', 'datetime', [
                'comment' => 'Date when article has been publicated (available on website)',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('visibility', 'boolean', [
                'comment' => 'Extra visibility flag. 1 - visible, 0 - hidden',
                'default' => true,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('meta_title', 'string', [
                'comment' => 'Meta section - title tag',
                'default' => null,
                'limit'   => 70,
                'null'    => true,
            ])
            ->addColumn('meta_description', 'string', [
                'comment' => 'Meta section - description tag',
                'default' => null,
                'limit'   => 160,
                'null'    => true,
            ])
            ->addColumn('meta_keywords', 'string', [
                'comment' => 'Meta section - keywords tag',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'slug',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'author_id',
                ]
            )
            ->addIndex(
                [
                    'category_id',
                ]
            )
            ->addIndex(
                [
                    'posted_by',
                ]
            )
            ->create();

        $this->table('categories', self::getDBCollations('The Land Site - articles categories (insights,case studies), used also as business categories in services (professional services) and for user insights preferences.'))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('name', 'string', [
                'comment' => 'Mandatory - category name',
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('description', 'text', [
                'comment' => 'Optional - category description',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('slug', 'string', [
                'comment' => 'Mandatory - unique slug part of URL for category name',
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('image', 'string', [
                'comment' => 'Optional - category image',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('position', 'integer', [
                'comment' => 'Sort order value',
                'default' => '0',
                'limit'   => 10,
                'null'    => false,
            ])
            ->addColumn('is_recommended', 'boolean', [
                'comment' => '0 - no recommended, 1 - recommended, selected as default',
                'default' => false,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'slug',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('contents', self::getDBCollations())
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 11,
                'null'          => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('page_id', 'integer', [
                'default' => null,
                'limit'   => 11,
                'null'    => false,
            ])
            ->addColumn('position', 'integer', [
                'default' => '999',
                'limit'   => 11,
                'null'    => true,
            ])
            ->addColumn('content_name', 'string', [
                'comment' => 'Old urlname field',
                'default' => 'main',
                'limit'   => 45,
                'null'    => false,
            ])
            ->addColumn('file', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('file2', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('file3', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('file4', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('file5', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('gallery', 'text', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('title', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('title2', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('title3', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('title4', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('title5', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('content', 'text', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('content2', 'text', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('content3', 'text', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('content4', 'text', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('content5', 'text', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('redirect_url', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('redirect_url2', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('redirect_url3', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('redirect_url4', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('redirect_url5', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('visible', 'boolean', [
                'default' => true,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'page_id',
                ]
            )
            ->addIndex(
                [
                    'position',
                ]
            )
            ->addIndex(
                [
                    'visible',
                ]
            )
            ->create();

        $this->table('credentials', self::getDBCollations('The Land Site - credentials for professional services'))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('name', 'string', [
                'comment' => 'Mandatory - Name of credential (county)',
                'default' => null,
                'limit'   => 100,
                'null'    => false,
            ])
            ->addColumn('type', 'string', [
                'comment' => 'Mandatory - ACC (Accreditation), ASS (Association)',
                'default' => null,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->create();

        $this->table('form_data_files', self::getDBCollations())
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 11,
                'null'          => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('form_data_id', 'integer', [
                'default' => null,
                'limit'   => 11,
                'null'    => false,
            ])
            ->addColumn('path', 'string', [
                'default' => null,
                'limit'   => 150,
                'null'    => false,
            ])
            ->addIndex(
                [
                    'form_data_id',
                ]
            )
            ->create();

        $this->table('forms', self::getDBCollations())
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 11,
                'null'          => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit'   => 50,
                'null'    => false,
            ])
            ->addColumn('alias', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('html_class', 'string', [
                'default' => null,
                'limit'   => 50,
                'null'    => true,
            ])
            ->addColumn('emails', 'text', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('send_email', 'boolean', [
                'default' => false,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('thank_you', 'boolean', [
                'default' => false,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('redirect_url', 'string', [
                'default' => null,
                'limit'   => 100,
                'null'    => true,
            ])
            ->addColumn('data', 'text', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('active', 'boolean', [
                'default' => true,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('deleted', 'boolean', [
                'default' => false,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => 'CURRENT_TIMESTAMP',
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => '0000-00-00 00:00:00',
                'limit'   => null,
                'null'    => false,
            ])
            ->addIndex(
                [
                    'name',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('forms_data', self::getDBCollations())
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 11,
                'null'          => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('id_forms', 'integer', [
                'default' => null,
                'limit'   => 11,
                'null'    => true,
            ])
            ->addColumn('data', 'text', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('view', 'boolean', [
                'default' => false,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('ip', 'string', [
                'default' => '0.0.0.0',
                'limit'   => 20,
                'null'    => true,
            ])
            ->addColumn('host', 'string', [
                'default' => '',
                'limit'   => 100,
                'null'    => true,
            ])
            ->addColumn('referer', 'string', [
                'default' => '',
                'limit'   => 100,
                'null'    => true,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => '0000-00-00 00:00:00',
                'limit'   => null,
                'null'    => false,
            ])
            ->addIndex(
                [
                    'id_forms',
                ]
            )
            ->create();

        $this->table('groups', self::getDBCollations())
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 11,
                'null'          => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('parent_id', 'integer', [
                'default' => null,
                'limit'   => 11,
                'null'    => true,
            ])
            ->addColumn('lft', 'integer', [
                'default' => null,
                'limit'   => 11,
                'null'    => true,
            ])
            ->addColumn('rght', 'integer', [
                'default' => null,
                'limit'   => 11,
                'null'    => true,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit'   => 100,
                'null'    => false,
            ])
            ->addColumn('permissions', 'text', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'parent_id',
                ]
            )
            ->create();

        $this->table('i18n', self::getDBCollations())
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 11,
                'null'          => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('locale', 'string', [
                'default' => null,
                'limit'   => 6,
                'null'    => false,
            ])
            ->addColumn('model', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('foreign_key', 'integer', [
                'default' => null,
                'limit'   => 11,
                'null'    => false,
            ])
            ->addColumn('field', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('content', 'text', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'locale',
                ]
            )
            ->addIndex(
                [
                    'model',
                ]
            )
            ->addIndex(
                [
                    'foreign_key',
                ]
            )
            ->addIndex(
                [
                    'field',
                ]
            )
            ->create();

        $this->table('languages', self::getDBCollations())
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 11,
                'null'          => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('position', 'integer', [
                'default' => '99',
                'limit'   => 11,
                'null'    => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit'   => 50,
                'null'    => false,
            ])
            ->addColumn('locale', 'string', [
                'default' => null,
                'limit'   => 6,
                'null'    => false,
            ])
            ->addColumn('is_default', 'boolean', [
                'default' => false,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('active', 'boolean', [
                'default' => true,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('deleted', 'boolean', [
                'default' => false,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'locale',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('loginbans', self::getDBCollations())
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 11,
                'null'          => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('remote_addr', 'string', [
                'default' => null,
                'limit'   => 15,
                'null'    => true,
            ])
            ->addColumn('user', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('ban_expires', 'timestamp', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => 'CURRENT_TIMESTAMP',
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->create();

        $this->table('logs', self::getDBCollations())
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 11,
                'null'          => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('user_id', 'biginteger', [
                'default' => '99',
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('ip', 'string', [
                'default' => null,
                'limit'   => 30,
                'null'    => true,
            ])
            ->addColumn('host', 'string', [
                'default' => null,
                'limit'   => 200,
                'null'    => true,
            ])
            ->addColumn('agent', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('content', 'text', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('entity', 'text', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('model', 'string', [
                'default' => null,
                'limit'   => 50,
                'null'    => true,
            ])
            ->addColumn('foreign_key', 'integer', [
                'default' => null,
                'limit'   => 11,
                'null'    => true,
            ])
            ->addColumn('locale', 'string', [
                'default' => null,
                'limit'   => 30,
                'null'    => true,
            ])
            ->addColumn('url', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('menus', self::getDBCollations())
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 11,
                'null'          => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('position', 'integer', [
                'default' => '99',
                'limit'   => 11,
                'null'    => false,
            ])
            ->addColumn('urlname', 'string', [
                'default' => null,
                'limit'   => 50,
                'null'    => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit'   => 100,
                'null'    => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'urlname',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('page_files', self::getDBCollations())
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 11,
                'null'          => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('page_id', 'integer', [
                'default' => null,
                'limit'   => 11,
                'null'    => false,
            ])
            ->addColumn('type', 'integer', [
                'default' => null,
                'limit'   => 4,
                'null'    => false,
            ])
            ->addColumn('path', 'string', [
                'default' => null,
                'limit'   => 100,
                'null'    => true,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit'   => 100,
                'null'    => true,
            ])
            ->addColumn('description', 'text', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('urlname', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('title', 'string', [
                'default' => null,
                'limit'   => 50,
                'null'    => true,
            ])
            ->addColumn('alt', 'string', [
                'default' => null,
                'limit'   => 50,
                'null'    => true,
            ])
            ->addColumn('target_blank', 'boolean', [
                'default' => false,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('active', 'boolean', [
                'default' => true,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('position', 'integer', [
                'default' => '99',
                'limit'   => 11,
                'null'    => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'page_id',
                ]
            )
            ->create();

        $this->table('page_types', self::getDBCollations())
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 11,
                'null'          => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('title', 'string', [
                'default' => null,
                'limit'   => 100,
                'null'    => false,
            ])
            ->addColumn('model', 'string', [
                'default' => null,
                'limit'   => 50,
                'null'    => true,
            ])
            ->addColumn('menu', 'string', [
                'default' => null,
                'limit'   => 50,
                'null'    => true,
            ])
            ->addColumn('sitemap', 'string', [
                'default' => null,
                'limit'   => 50,
                'null'    => false,
            ])
            ->create();

        $this->table('pages', self::getDBCollations())
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 11,
                'null'          => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('menu_id', 'integer', [
                'default' => null,
                'limit'   => 11,
                'null'    => false,
            ])
            ->addColumn('page_type_id', 'integer', [
                'comment' => 'Structure type',
                'default' => null,
                'limit'   => 11,
                'null'    => false,
            ])
            ->addColumn('parent_id', 'integer', [
                'default' => null,
                'limit'   => 11,
                'null'    => true,
            ])
            ->addColumn('lft', 'integer', [
                'default' => null,
                'limit'   => 11,
                'null'    => true,
            ])
            ->addColumn('rght', 'integer', [
                'default' => null,
                'limit'   => 11,
                'null'    => true,
            ])
            ->addColumn('position', 'integer', [
                'default' => '99999',
                'limit'   => 11,
                'null'    => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit'   => 100,
                'null'    => false,
            ])
            ->addColumn('urlname', 'string', [
                'default' => null,
                'limit'   => 100,
                'null'    => false,
            ])
            ->addColumn('redirection', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('target_blank', 'boolean', [
                'default' => false,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('menu_icon', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('ip', 'string', [
                'default' => null,
                'limit'   => 100,
                'null'    => true,
            ])
            ->addColumn('depth', 'integer', [
                'default' => null,
                'limit'   => 11,
                'null'    => true,
            ])
            ->addColumn('layout', 'string', [
                'default' => 'default',
                'limit'   => 50,
                'null'    => false,
            ])
            ->addColumn('seo_title', 'string', [
                'default' => null,
                'limit'   => 600,
                'null'    => true,
            ])
            ->addColumn('seo_keywords', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('seo_description', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('seo_priority', 'float', [
                'default'   => '0.8',
                'null'      => true,
                'precision' => 2,
                'scale'     => 1,
            ])
            ->addColumn('og_title', 'string', [
                'default' => null,
                'limit'   => 100,
                'null'    => true,
            ])
            ->addColumn('og_image', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('og_description', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('css', 'text', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('active', 'boolean', [
                'default' => true,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('in_menu', 'boolean', [
                'default' => true,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('protected', 'integer', [
                'default' => null,
                'limit'   => 11,
                'null'    => false,
            ])
            ->addColumn('deleted', 'boolean', [
                'default' => false,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'urlname',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'menu_id',
                ]
            )
            ->addIndex(
                [
                    'page_type_id',
                ]
            )
            ->addIndex(
                [
                    'deleted',
                ]
            )
            ->create();

        $this->table('payment_tokens', self::getDBCollations())
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('token', 'string', [
                'comment' => 'Mandatory - One-off token generated by Stripe API (StripeJS)',
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('amount_pos', 'integer', [
                'comment' => 'Mandatory - A positive integer in the smallest currency unit (a zero-decimal currency) representing how much to charge.',
                'default' => null,
                'limit'   => 10,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('payload', 'text', [
                'comment' => 'Mandatory - JSON payload from our app',
                'default' => null,
                'limit'   => 4294967295,
                'null'    => false,
            ])
            ->addColumn('used', 'boolean', [
                'comment' => 'Used Flag 1 - used, 0 - not used',
                'default' => false,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->create();

        $this->table('payments', self::getDBCollations())
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('user_id', 'biginteger', [
                'comment' => 'Mandatory - member ID (user ID)',
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('title', 'string', [
                'comment' => 'Mandatory - payment title (invoice position/item)',
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('amount', 'decimal', [
                'comment'   => 'Mandatory - payment amount',
                'default'   => null,
                'null'      => false,
                'precision' => 10,
                'scale'     => 2,
                'signed'    => false,
            ])
            ->addColumn('amount_pos', 'integer', [
                'comment' => 'Mandatory - payment amount (zero decimal)',
                'default' => null,
                'limit'   => 10,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('currency', 'string', [
                'comment' => 'Mandatory - currency literal symbol, usually GBP',
                'default' => null,
                'limit'   => 3,
                'null'    => false,
            ])
            ->addColumn('type', 'string', [
                'comment' => 'Mandatory - what the payment applies to',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('token_id', 'string', [
                'comment' => 'Mandatory - payment token used in Stripe system',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('charge_id', 'string', [
                'comment' => 'Mandatory - charge token used in Stripe system',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('payload', 'text', [
                'comment' => 'Mandatory - payload generated by StripeJS and sended to Stripe server',
                'default' => null,
                'limit'   => 4294967295,
                'null'    => true,
            ])
            ->addColumn('meta', 'text', [
                'comment' => 'Optional - developer meta data',
                'default' => null,
                'limit'   => 4294967295,
                'null'    => true,
            ])
            ->addColumn('refunded', 'boolean', [
                'comment' => 'Optional - was refunded',
                'default' => false,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('discount', 'decimal', [
                'comment'   => 'Optional - discount amount ',
                'default'   => '0.00',
                'null'      => true,
                'precision' => 10,
                'scale'     => 2,
                'signed'    => false,
            ])
            ->addColumn('discount_type', 'string', [
                'comment' => 'Optional - discount type',
                'default' => null,
                'limit'   => 50,
                'null'    => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('products', self::getDBCollations('The Land Site - products and services offered by businesses (professional services)'))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('name', 'string', [
                'comment' => 'Mandatory - Product name',
                'default' => null,
                'limit'   => 50,
                'null'    => false,
            ])
            ->addColumn('description', 'string', [
                'comment' => 'Optional - Product description',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->create();

        $this->table('properties', self::getDBCollations('The Land Site - properties data'))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('type_id', 'biginteger', [
                'comment' => 'Mandatory - Type of property',
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('user_id', 'biginteger', [
                'comment' => 'Mandatory - user ID (member identifier)',
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('uid', 'string', [
                'comment' => 'Unique string ID. Generated by trigger.',
                'default' => null,
                'limit'   => 32,
                'null'    => false,
            ])
            ->addColumn('purpose', 'string', [
                'comment' => 'Mandatory - property purpose',
                'default' => null,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('title', 'string', [
                'comment' => 'Mandatory - property full title',
                'default' => null,
                'limit'   => 70,
                'null'    => false,
            ])
            ->addColumn('slug', 'string', [
                'comment' => 'Mandatory - slug part of URL for property title (not unique)',
                'default' => null,
                'limit'   => 70,
                'null'    => false,
            ])
            ->addColumn('price_sale', 'decimal', [
                'comment'   => 'Optional - property sale price. One of the prices is required (depending on the type)',
                'default'   => '0.00',
                'null'      => false,
                'precision' => 10,
                'scale'     => 2,
                'signed'    => false,
            ])
            ->addColumn('price_rent', 'decimal', [
                'comment'   => 'Optional - property rent price. One of the prices is required (depending on the type)',
                'default'   => '0.00',
                'null'      => false,
                'precision' => 10,
                'scale'     => 2,
                'signed'    => false,
            ])
            ->addColumn('location', 'string', [
                'comment' => 'Mandatory - property address.',
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('lat', 'decimal', [
                'comment'   => 'Optional. Location latitude as decimal. ',
                'default'   => null,
                'null'      => true,
                'precision' => 10,
                'scale'     => 6,
            ])
            ->addColumn('lng', 'decimal', [
                'comment'   => 'Optional. Location longitude as decimal.',
                'default'   => null,
                'null'      => true,
                'precision' => 10,
                'scale'     => 6,
            ])
            ->addColumn('coords', 'string', [
                'comment' => 'Mandatory (db engine require to add even 0,0 point - spatial index). Location lat/lng as geometry point object.',
                'default' => null,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('postcode', 'string', [
                'comment' => 'Mandatory - property postcode',
                'default' => null,
                'limit'   => 25,
                'null'    => false,
            ])
            ->addColumn('headline', 'string', [
                'comment' => 'Mandatory - description heading (first paragraph). Usually cropped description to few lines.',
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('description', 'string', [
                'comment' => 'Mandatory - property full description',
                'default' => null,
                'limit'   => 10000,
                'null'    => false,
            ])
            ->addColumn('website_url', 'string', [
                'comment' => 'Optional - website link. Extra paid function.',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('status', 'string', [
                'comment' => 'Property statuses: DRAFT - property outline, PUBLISHED - property saved and published on website, EXPIRED - property has expired (ad_expiry_date)',
                'default' => 'DRAFT',
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('publish_date', 'datetime', [
                'comment' => 'Property publication date on website (first date)',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('ad_type', 'string', [
                'comment' => 'Type of ad/announcement package (defined in spec)',
                'default' => 'STANDARD',
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('ad_expiry_date', 'datetime', [
                'comment' => 'The entry (property) expiration date. After this date, the property status is changed to EXPIRED.',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('ad_cost', 'decimal', [
                'comment'   => 'Current cost of the ad (FEATURED, PREMIUM).',
                'default'   => '0.00',
                'null'      => false,
                'precision' => 10,
                'scale'     => 2,
                'signed'    => false,
            ])
            ->addColumn('ad_total_cost', 'decimal', [
                'comment'   => 'The sum of costs if the entry (property) is being prolonged / re-listed.',
                'default'   => '0.00',
                'null'      => false,
                'precision' => 10,
                'scale'     => 2,
                'signed'    => false,
            ])
            ->addColumn('enquiries', 'integer', [
                'comment' => 'Total count of enquiries (counter).',
                'default' => '0',
                'limit'   => 10,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('views', 'integer', [
                'comment' => 'Total count of views (counter).',
                'default' => '0',
                'limit'   => 10,
                'null'    => false,
                'signed'  => false,
            ])
            ->addIndex(
                [
                    'uid',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'type_id',
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('property_attributes', self::getDBCollations('The Land Site - properties attributes'))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('property_id', 'biginteger', [
                'comment' => 'Mandatory - property ID',
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('attribute_id', 'biginteger', [
                'comment' => 'Mandatory - attribute ID',
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('attribute_value', 'boolean', [
                'comment' => 'Mandatory - 1 - Yes, 0 - No (in the future, the column can be updated to another type to reflect more options if necessary)',
                'default' => null,
                'limit'   => null,
                'null'    => false,
            ])
            ->addIndex(
                [
                    'property_id',
                    'attribute_id',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'attribute_id',
                ]
            )
            ->addIndex(
                [
                    'property_id',
                ]
            )
            ->create();

        $this->table('property_attributes_property_types', self::getDBCollations('The Land Site - type attributes (related with properties types, all items represents true/false data type).'))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('type_id', 'biginteger', [
                'comment' => 'Property type ID that attribute belongs to OR NULL if attribute belongs to all types.',
                'default' => null,
                'limit'   => 20,
                'null'    => true,
                'signed'  => false,
            ])
            ->addColumn('name', 'string', [
                'comment' => 'Mandatory - attribute name',
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('description', 'text', [
                'comment' => 'Optional - attribute description',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('image', 'string', [
                'comment' => 'Mandatory (website icons) - attribute icon',
                'default' => '',
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('visibility', 'boolean', [
                'comment' => 'Optional - visibility flag. 1 - visible, 0 - hidden',
                'default' => true,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('position', 'integer', [
                'comment' => 'Optional - sort order value',
                'default' => '0',
                'limit'   => 11,
                'null'    => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'type_id',
                ]
            )
            ->create();

        $this->table('property_enquiries', self::getDBCollations('The Land Site - properties enquiries'))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('property_id', 'biginteger', [
                'comment' => 'Mandatory - property ID',
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('user_id', 'biginteger', [
                'comment' => 'Mandatory - user ID (member identifier)',
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('title', 'string', [
                'comment' => 'Mandatory - prefilled by property title',
                'default' => null,
                'limit'   => 70,
                'null'    => false,
            ])
            ->addColumn('full_name', 'string', [
                'comment' => 'Mandatory - prefilled by user first and last name',
                'default' => null,
                'limit'   => 200,
                'null'    => false,
            ])
            ->addColumn('phone', 'string', [
                'comment' => 'Mandatory - prefilled by user phone number',
                'default' => null,
                'limit'   => 50,
                'null'    => false,
            ])
            ->addColumn('message', 'text', [
                'comment' => 'Optional - user text message',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('created', 'datetime', [
                'comment' => 'Created datetime - same as Date received',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'property_id',
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('property_media', self::getDBCollations('The Land Site - properties media records (files, videos)'))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('property_id', 'biginteger', [
                'comment' => 'Mandatory - property ID',
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('type', 'string', [
                'comment' => 'Mandatory - media type (image file, document file or link to video clip). ',
                'default' => null,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('source', 'string', [
                'comment' => 'Mandatory - file path or Video ID (YouTube - alphanumeric string, Vimeo - numeric string)',
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('extra', 'text', [
                'comment' => 'Optional - extra options/config for file. For example - video service provider etc.',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('position', 'integer', [
                'comment' => 'Optional - sort order value',
                'default' => '0',
                'limit'   => 11,
                'null'    => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'property_id',
                ]
            )
            ->create();

        $this->table('property_searches', self::getDBCollations(['engine' => 'MyISAM', 'comment' => 'The Land Site - properties searches (recent searches section) MyISAM, no FKs - could be used for saving anonymous searches']))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('user_id', 'integer', [
                'comment' => 'Optional - user ID (users.id)',
                'default' => null,
                'limit'   => 11,
                'null'    => true,
            ])
            ->addColumn('location', 'string', [
                'comment' => 'Optional - location string',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('purpose', 'string', [
                'comment' => 'Optional - property purpose',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('type_id', 'biginteger', [
                'comment' => 'Optional - property type ID (properties_types.id)',
                'default' => null,
                'limit'   => 20,
                'null'    => true,
                'signed'  => false,
            ])
            ->addColumn('min_price', 'decimal', [
                'comment'   => 'Optional - min price (sale,rent)',
                'default'   => null,
                'null'      => true,
                'precision' => 10,
                'scale'     => 2,
            ])
            ->addColumn('max_price', 'decimal', [
                'comment'   => 'Optional - max price (sale,rent)',
                'default'   => null,
                'null'      => true,
                'precision' => 10,
                'scale'     => 2,
            ])
            ->addColumn('radius', 'integer', [
                'comment' => 'Optional - radius in miles (mean - search within N miles)',
                'default' => null,
                'limit'   => 5,
                'null'    => true,
                'signed'  => false,
            ])
            ->addColumn('lat', 'decimal', [
                'comment'   => 'Optional - latitude (user location, for radius search)',
                'default'   => null,
                'null'      => true,
                'precision' => 10,
                'scale'     => 6,
            ])
            ->addColumn('lng', 'decimal', [
                'comment'   => 'Optional - longitude (user location, for radius search)',
                'default'   => null,
                'null'      => true,
                'precision' => 10,
                'scale'     => 6,
            ])
            ->addColumn('attributes', 'text', [
                'comment' => 'Optional - property type attribute IDs seperated by comma (properties_attributes.id)',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('alerts', 'boolean', [
                'comment' => 'Optional - 0 - no alerts, 1 - user want to receive alerts with recent properties that fit the options chosen',
                'default' => false,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->create();

        $this->table('property_types', self::getDBCollations('The Land Site - properties types'))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('parent_id', 'biginteger', [
                'comment' => 'Type parent ID (if record is a child) or NULL - simple tree structure',
                'default' => null,
                'limit'   => 20,
                'null'    => true,
                'signed'  => false,
            ])
            ->addColumn('name', 'string', [
                'comment' => 'Mandatory - type name',
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('position', 'integer', [
                'comment' => 'Optional - sort order value',
                'default' => '0',
                'limit'   => 11,
                'null'    => false,
            ])
            ->addColumn('visibility', 'boolean', [
                'comment' => 'Optional - visibility flag. 1 - visible, 0 - hidden',
                'default' => true,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'parent_id',
                ]
            )
            ->create();

        $this->table('property_views', self::getDBCollations(['engine' => 'MyISAM', 'comment' => 'The Land Site - properties views. Statistics entity - keep aware (MyISAM + FIXED rows + triggers + KEY index)']))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('created', 'datetime', [
                'default' => 'CURRENT_TIMESTAMP',
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('property_id', 'biginteger', [
                'comment' => 'Mandatory - property ID. Virtual FK properties.id',
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addIndex(
                [
                    'created',
                    'property_id',
                ]
            )
            ->create();

        $this->table('redirections', self::getDBCollations())
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 11,
                'null'          => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('redirection_from', 'string', [
                'default' => null,
                'limit'   => 100,
                'null'    => false,
            ])
            ->addColumn('redirect_to', 'string', [
                'default' => null,
                'limit'   => 100,
                'null'    => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->create();

        $this->table('service_areas', self::getDBCollations('The Land Site - areas covered by professional services'))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('service_id', 'biginteger', [
                'comment' => 'Mandatory - professional service ID (services.id)',
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('area_id', 'biginteger', [
                'comment' => 'Mandatory - covered area ID (areas.id)',
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'service_id',
                    'area_id',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'area_id',
                ]
            )
            ->addIndex(
                [
                    'service_id',
                ]
            )
            ->create();

        $this->table('service_credentials', self::getDBCollations('The Land Site - credentials which a business has'))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('service_id', 'biginteger', [
                'comment' => 'Mandatory - professional service ID (services.id)',
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('credential_id', 'biginteger', [
                'comment' => 'Mandatory - credential ID (credentials.id)',
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'service_id',
                    'credential_id',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'credential_id',
                ]
            )
            ->addIndex(
                [
                    'service_id',
                ]
            )
            ->create();

        $this->table('service_enquiries', self::getDBCollations('The Land Site - services enquiries '))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('service_id', 'biginteger', [
                'comment' => 'Mandatory - business ID',
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('user_id', 'biginteger', [
                'comment' => 'Mandatory - user ID (member identifier) OR null if guest',
                'default' => null,
                'limit'   => 20,
                'null'    => true,
                'signed'  => false,
            ])
            ->addColumn('full_name', 'string', [
                'comment' => 'Mandatory - prefilled by user first and last name',
                'default' => null,
                'limit'   => 200,
                'null'    => false,
            ])
            ->addColumn('phone', 'string', [
                'comment' => 'Mandatory - prefilled by user phone number',
                'default' => null,
                'limit'   => 50,
                'null'    => false,
            ])
            ->addColumn('email', 'string', [
                'comment' => 'Mandatory - prefilled by member email',
                'default' => null,
                'limit'   => 100,
                'null'    => false,
            ])
            ->addColumn('message', 'text', [
                'comment' => 'Optional - user text message',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('created', 'datetime', [
                'comment' => 'Created datetime - same as Date received',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'service_id',
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('service_media', self::getDBCollations('The Land Site - services media records (files, videos)'))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('service_id', 'biginteger', [
                'comment' => 'Mandatory - service ID',
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('type', 'string', [
                'comment' => 'Mandatory - media type (image file, link to video clip). ',
                'default' => null,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('source', 'string', [
                'comment' => 'Mandatory - file path or Video ID (YouTube - alphanumeric string, Vimeo - numeric string)',
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('extra', 'text', [
                'comment' => 'Optional - extra options/config for file. For example - video service provider etc.',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('position', 'integer', [
                'comment' => 'Optional - sort order value',
                'default' => '0',
                'limit'   => 11,
                'null'    => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'service_id',
                ]
            )
            ->create();

        $this->table('service_products', self::getDBCollations('The Land Site - products offered by professional services'))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('service_id', 'biginteger', [
                'comment' => 'Mandatory - business ID (services.id)',
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('product_id', 'biginteger', [
                'comment' => 'Mandatory - product ID (products.id)',
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'service_id',
                    'product_id',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'product_id',
                ]
            )
            ->addIndex(
                [
                    'service_id',
                ]
            )
            ->create();

        $this->table('service_searches', self::getDBCollations(['engine' => 'MyISAM', 'comment' => 'The Land Site - services searches (recent searches section) MyISAM, no FKs - could be used for saving anonymous searches']))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('user_id', 'integer', [
                'comment' => 'Optional - user ID (users.id)',
                'default' => null,
                'limit'   => 11,
                'null'    => true,
            ])
            ->addColumn('location', 'string', [
                'comment' => 'Optional - location string',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('keywords', 'string', [
                'comment' => 'Optional - keywords string (search by business title,description,products,credentials)',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('category_id', 'biginteger', [
                'comment' => 'Optional - professional category ID (categories.id)',
                'default' => null,
                'limit'   => 20,
                'null'    => true,
                'signed'  => false,
            ])
            ->addColumn('radius', 'integer', [
                'comment' => 'Optional - radius in miles (mean - search within N miles)',
                'default' => null,
                'limit'   => 5,
                'null'    => true,
                'signed'  => false,
            ])
            ->addColumn('lat', 'decimal', [
                'comment'   => 'Optional - latitude (user location, for radius search)',
                'default'   => null,
                'null'      => true,
                'precision' => 10,
                'scale'     => 6,
            ])
            ->addColumn('lng', 'decimal', [
                'comment'   => 'Optional - longitude (user location, for radius search)',
                'default'   => null,
                'null'      => true,
                'precision' => 10,
                'scale'     => 6,
            ])
            ->addColumn('alerts', 'boolean', [
                'comment' => 'Optional - 0 - no alerts, 1 - user want to receive alerts with recent services that fit the options chosen',
                'default' => false,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->create();

        $this->table('service_views', self::getDBCollations(['engine' => 'MyISAM', 'comment' => 'The Land Site - services views. Statistics entity - keep aware (MyISAM + FIXED rows + triggers + KEY index)']))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('created', 'datetime', [
                'default' => 'CURRENT_TIMESTAMP',
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('service_id', 'biginteger', [
                'comment' => 'Mandatory - service ID. Virtual FK services.id',
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addIndex(
                [
                    'created',
                    'service_id',
                ]
            )
            ->create();

        $this->table('services', self::getDBCollations('The Land Site - professional services records (one member = one business/service)'))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('category_id', 'biginteger', [
                'comment' => 'Mandatory - business category ID (categories.id)',
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('user_id', 'biginteger', [
                'comment' => 'Mandatory - user ID (member identifier)',
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('uid', 'string', [
                'comment' => 'Unique string ID. Generated by trigger.',
                'default' => null,
                'limit'   => 32,
                'null'    => false,
            ])
            ->addColumn('company', 'string', [
                'comment' => 'Mandatory - prefilled by user company name (users_details.company)',
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('postcode', 'string', [
                'comment' => 'Mandatory - business postcode',
                'default' => null,
                'limit'   => 25,
                'null'    => false,
            ])
            ->addColumn('location', 'string', [
                'comment' => 'Mandatory - location address.',
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('lat', 'decimal', [
                'comment'   => 'Optional. Location latitude as decimal. ',
                'default'   => null,
                'null'      => true,
                'precision' => 10,
                'scale'     => 6,
            ])
            ->addColumn('lng', 'decimal', [
                'comment'   => 'Optional. Location longitude as decimal. ',
                'default'   => null,
                'null'      => true,
                'precision' => 10,
                'scale'     => 6,
            ])
            ->addColumn('coords', 'string', [
                'comment' => 'Mandatory (db engine require to add even 0,0 point - spatial index). Location lat/lng as geometry point object.',
                'default' => null,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('description', 'string', [
                'comment' => 'Mandatory - business overview',
                'default' => null,
                'limit'   => 10000,
                'null'    => false,
            ])
            ->addColumn('website_url', 'string', [
                'comment' => 'Optional - website link',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('google_url', 'string', [
                'comment' => 'Optional - Google My Business link',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('facebook_url', 'string', [
                'comment' => 'Optional - facebook link',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('linkedin_url', 'string', [
                'comment' => 'Optional - linkedin link',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('twitter_url', 'string', [
                'comment' => 'Optional - twitter link',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('hubspot_url', 'string', [
                'comment' => 'Optional - hubspot link',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('quick_win_1', 'string', [
                'comment' => 'Optional - business advantage 1',
                'default' => null,
                'limit'   => 25,
                'null'    => true,
            ])
            ->addColumn('quick_win_2', 'string', [
                'comment' => 'Optional - business advantage 2',
                'default' => null,
                'limit'   => 25,
                'null'    => true,
            ])
            ->addColumn('quick_win_3', 'string', [
                'comment' => 'Optional - business advantage 3',
                'default' => null,
                'limit'   => 25,
                'null'    => true,
            ])
            ->addColumn('opening_time_from', 'time', [
                'comment' => 'Optional - opening time from (hour)',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('opening_time_to', 'time', [
                'comment' => 'Optional - opening time to (hour)',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('opening_days', 'string', [
                'comment' => 'Optional - opening days of the week (0 - sunday, 6 - saturday)',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('status', 'string', [
                'comment' => 'Service statuses: DRAFT - business outline, PUBLISHED - service saved and published on website, EXPIRED - service has expired (ad_expiry_date)',
                'default' => 'DRAFT',
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('publish_date', 'datetime', [
                'comment' => 'Service publication date on website (first date)',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('ad_type', 'string', [
                'comment' => 'Type of ad/announcement package (defined in spec)',
                'default' => 'STANDARD',
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('ad_expiry_date', 'datetime', [
                'comment' => 'The entry (service) expiration date. After this date, the service status is changed to EXPIRED.',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('ad_cost', 'decimal', [
                'comment'   => 'Current cost of the ad (FEATURED, PREMIUM).',
                'default'   => '0.00',
                'null'      => false,
                'precision' => 10,
                'scale'     => 2,
                'signed'    => false,
            ])
            ->addColumn('ad_total_cost', 'decimal', [
                'comment'   => 'The sum of costs if the entry (service) is being prolonged / re-listed.',
                'default'   => '0.00',
                'null'      => false,
                'precision' => 10,
                'scale'     => 2,
                'signed'    => false,
            ])
            ->addColumn('enquiries', 'integer', [
                'comment' => 'Total count of enquiries (counter).',
                'default' => '0',
                'limit'   => 10,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('views', 'integer', [
                'comment' => 'Total count of views (counter).',
                'default' => '0',
                'limit'   => 10,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'uid',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'user_id',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'category_id',
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('settings', self::getDBCollations('The Land Site - configuration options (especially prices and other options that should be edited by admin)'))
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 11,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('key', 'string', [
                'comment' => 'Setting key',
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('group', 'string', [
                'comment' => 'Setting group',
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('value', 'text', [
                'comment' => 'Value - related with type column',
                'default' => null,
                'limit'   => 4294967295,
                'null'    => true,
            ])
            ->addColumn('type', 'string', [
                'comment' => 'Value type',
                'default' => 'STRING',
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('description', 'text', [
                'comment' => 'Short description',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('editable', 'boolean', [
                'comment' => 'If setting should be editable by user/admin',
                'default' => true,
                'limit'   => null,
                'null'    => false,
            ])
            ->addIndex(
                [
                    'key',
                    'group',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('static_content_groups', self::getDBCollations())
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 11,
                'null'          => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit'   => 100,
                'null'    => false,
            ])
            ->create();

        $this->table('static_contents', self::getDBCollations())
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 11,
                'null'          => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('static_group_id', 'integer', [
                'default' => null,
                'limit'   => 11,
                'null'    => false,
            ])
            ->addColumn('var_name', 'string', [
                'default' => null,
                'limit'   => 50,
                'null'    => false,
            ])
            ->addColumn('value', 'text', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('type', 'integer', [
                'default' => '0',
                'limit'   => 11,
                'null'    => false,
            ])
            ->addColumn('description', 'text', [
                'default' => null,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'var_name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'static_group_id',
                ]
            )
            ->create();

        $this->table('user_billing_addresses', self::getDBCollations('The Land Site - user billing addresses'))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('user_id', 'biginteger', [
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('is_default', 'boolean', [
                'comment' => 'Mandatory - 1 - default address, 0 - other address',
                'default' => true,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('first_name', 'string', [
                'comment' => 'Mandatory - billing First Name',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('last_name', 'string', [
                'comment' => 'Mandatory - billing Last Name',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('email', 'string', [
                'comment' => 'Mandatory - billing email address',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('phone', 'string', [
                'comment' => 'Mandatory - billing phone number',
                'default' => null,
                'limit'   => 50,
                'null'    => true,
            ])
            ->addColumn('company', 'string', [
                'comment' => 'Optional - billing company name',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('postcode', 'string', [
                'comment' => 'Mandatory - billing postcode',
                'default' => null,
                'limit'   => 25,
                'null'    => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('user_details', self::getDBCollations('The Land Site - user details'))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('user_id', 'biginteger', [
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('dashboard_type', 'string', [
                'comment' => 'Mandatory - dashboard type (caching current state). NULL - empty, S - seller, P - professional, SP - seller/professional',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('company', 'string', [
                'comment' => 'Optional field',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('company_description', 'text', [
                'comment' => 'Optional field.',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('location', 'string', [
                'comment' => 'NULL or Empty - All locations',
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('postcode', 'string', [
                'comment' => 'Mandatory only at my details section',
                'default' => null,
                'limit'   => 25,
                'null'    => true,
            ])
            ->addColumn('pref_buying', 'boolean', [
                'comment' => '1- interested in Buying, 0 - no interested',
                'default' => false,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('pref_selling', 'boolean', [
                'comment' => '1 - interested in Selling, 0 - no interested',
                'default' => false,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('pref_professional', 'boolean', [
                'comment' => '1 - interested in Professional Services, 0 - no interested',
                'default' => false,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('pref_insights', 'boolean', [
                'comment' => '1 - interested in Insights, 0 - no interested',
                'default' => false,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('pref_insights_list', 'text', [
                'comment' => 'Preferred insight categories list in JSON Array containing IDs.',
                'default' => null,
                'limit'   => 4294967295,
                'null'    => true,
            ])
            ->addColumn('use_billing_details', 'boolean', [
                'comment' => 'Billing details are the same (1) or not (0) against primary profile data',
                'default' => false,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('marketing_agreement_1', 'boolean', [
                'comment' => 'Contact by email: 1 - agree, 0 - disagree',
                'default' => false,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('marketing_agreement_2', 'boolean', [
                'comment' => 'Contact by phone: 1 - agree, 0 - disagree',
                'default' => false,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('marketing_agreement_3', 'boolean', [
                'comment' => 'Another marketing agreement: 1 - agree, 0 - disagree',
                'default' => false,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('user_saved', self::getDBCollations('The Land Site - saved properties and services'))
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('user_id', 'biginteger', [
                'comment' => 'Mandatory - user ID (member identifier)',
                'default' => null,
                'limit'   => 20,
                'null'    => false,
                'signed'  => false,
            ])
            ->addColumn('property_id', 'biginteger', [
                'comment' => 'Optional - property ID or null (properties.id)',
                'default' => null,
                'limit'   => 20,
                'null'    => true,
                'signed'  => false,
            ])
            ->addColumn('service_id', 'biginteger', [
                'comment' => 'Optional - service ID or null (services.id)',
                'default' => null,
                'limit'   => 20,
                'null'    => true,
                'signed'  => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'property_id',
                ]
            )
            ->addIndex(
                [
                    'service_id',
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('users', self::getDBCollations())
            ->addColumn('id', 'biginteger', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => 20,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('group_id', 'integer', [
                'default' => '2',
                'limit'   => 11,
                'null'    => false,
            ])
            ->addColumn('image', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => true,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit'   => 100,
                'null'    => false,
            ])
            ->addColumn('password', 'string', [
                'default' => null,
                'limit'   => 255,
                'null'    => false,
            ])
            ->addColumn('first_name', 'string', [
                'default' => null,
                'limit'   => 100,
                'null'    => true,
            ])
            ->addColumn('last_name', 'string', [
                'default' => null,
                'limit'   => 100,
                'null'    => true,
            ])
            ->addColumn('job_role', 'string', [
                'default' => null,
                'limit'   => 50,
                'null'    => true,
            ])
            ->addColumn('phone', 'string', [
                'default' => null,
                'limit'   => 50,
                'null'    => true,
            ])
            ->addColumn('active', 'boolean', [
                'default' => true,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('deleted', 'boolean', [
                'default' => false,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('token', 'string', [
                'default' => null,
                'limit'   => 20,
                'null'    => true,
            ])
            ->addColumn('token_authorized', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('token_expires', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(
                [
                    'email',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'group_id',
                ]
            )
            ->addIndex(
                [
                    'active',
                ]
            )
            ->create();

        $this->table('articles')
            ->addForeignKey(
                'author_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'category_id',
                'categories',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'posted_by',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('contents')
            ->addForeignKey(
                'page_id',
                'pages',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('form_data_files')
            ->addForeignKey(
                'form_data_id',
                'forms_data',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('forms_data')
            ->addForeignKey(
                'id_forms',
                'forms',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('groups')
            ->addForeignKey(
                'parent_id',
                'groups',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('logs')
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('page_files')
            ->addForeignKey(
                'page_id',
                'pages',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('pages')
            ->addForeignKey(
                'menu_id',
                'menus',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'page_type_id',
                'page_types',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('payments')
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('properties')
            ->addForeignKey(
                'type_id',
                'property_types',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('property_attributes')
            ->addForeignKey(
                'attribute_id',
                'property_attributes_property_types',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'property_id',
                'properties',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('property_attributes_property_types')
            ->addForeignKey(
                'type_id',
                'property_types',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('property_enquiries')
            ->addForeignKey(
                'property_id',
                'properties',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('property_media')
            ->addForeignKey(
                'property_id',
                'properties',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('property_types')
            ->addForeignKey(
                'parent_id',
                'property_types',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('service_areas')
            ->addForeignKey(
                'area_id',
                'areas',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'service_id',
                'services',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('service_credentials')
            ->addForeignKey(
                'credential_id',
                'credentials',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'service_id',
                'services',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('service_enquiries')
            ->addForeignKey(
                'service_id',
                'services',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'SET_NULL'
                ]
            )
            ->update();

        $this->table('service_media')
            ->addForeignKey(
                'service_id',
                'services',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('service_products')
            ->addForeignKey(
                'product_id',
                'products',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'service_id',
                'services',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('services')
            ->addForeignKey(
                'category_id',
                'categories',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('static_contents')
            ->addForeignKey(
                'static_group_id',
                'static_content_groups',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('user_billing_addresses')
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('user_details')
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('user_saved')
            ->addForeignKey(
                'property_id',
                'properties',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'service_id',
                'services',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('users')
            ->addForeignKey(
                'group_id',
                'groups',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();
    }

    public function down()
    {
        $this->table('articles')
            ->dropForeignKey(
                'author_id'
            )
            ->dropForeignKey(
                'category_id'
            )
            ->dropForeignKey(
                'posted_by'
        );

        $this->table('contents')
            ->dropForeignKey(
                'page_id'
        );

        $this->table('form_data_files')
            ->dropForeignKey(
                'form_data_id'
        );

        $this->table('forms_data')
            ->dropForeignKey(
                'id_forms'
        );

        $this->table('groups')
            ->dropForeignKey(
                'parent_id'
        );

        $this->table('logs')
            ->dropForeignKey(
                'user_id'
        );

        $this->table('page_files')
            ->dropForeignKey(
                'page_id'
        );

        $this->table('pages')
            ->dropForeignKey(
                'menu_id'
            )
            ->dropForeignKey(
                'page_type_id'
        );

        $this->table('payments')
            ->dropForeignKey(
                'user_id'
        );

        $this->table('properties')
            ->dropForeignKey(
                'type_id'
            )
            ->dropForeignKey(
                'user_id'
        );

        $this->table('property_attributes')
            ->dropForeignKey(
                'attribute_id'
            )
            ->dropForeignKey(
                'property_id'
        );

        $this->table('property_attributes_property_types')
            ->dropForeignKey(
                'type_id'
        );

        $this->table('property_enquiries')
            ->dropForeignKey(
                'property_id'
            )
            ->dropForeignKey(
                'user_id'
        );

        $this->table('property_media')
            ->dropForeignKey(
                'property_id'
        );

        $this->table('property_types')
            ->dropForeignKey(
                'parent_id'
        );

        $this->table('service_areas')
            ->dropForeignKey(
                'area_id'
            )
            ->dropForeignKey(
                'service_id'
        );

        $this->table('service_credentials')
            ->dropForeignKey(
                'credential_id'
            )
            ->dropForeignKey(
                'service_id'
        );

        $this->table('service_enquiries')
            ->dropForeignKey(
                'service_id'
            )
            ->dropForeignKey(
                'user_id'
        );

        $this->table('service_media')
            ->dropForeignKey(
                'service_id'
        );

        $this->table('service_products')
            ->dropForeignKey(
                'product_id'
            )
            ->dropForeignKey(
                'service_id'
        );

        $this->table('services')
            ->dropForeignKey(
                'category_id'
            )
            ->dropForeignKey(
                'user_id'
        );

        $this->table('static_contents')
            ->dropForeignKey(
                'static_group_id'
        );

        $this->table('user_billing_addresses')
            ->dropForeignKey(
                'user_id'
        );

        $this->table('user_details')
            ->dropForeignKey(
                'user_id'
        );

        $this->table('user_saved')
            ->dropForeignKey(
                'property_id'
            )
            ->dropForeignKey(
                'service_id'
            )
            ->dropForeignKey(
                'user_id'
        );

        $this->table('users')
            ->dropForeignKey(
                'group_id'
        );

        $tables = [
            'areas',
            'articles',
            'categories',
            'contents',
            'credentials',
            'form_data_files',
            'forms',
            'forms_data',
            'groups',
            'i18n',
            'languages',
            'loginbans',
            'logs',
            'menus',
            'page_files',
            'page_types',
            'pages',
            'payment_tokens',
            'payments',
            'products',
            'properties',
            'property_attributes',
            'property_attributes_property_types',
            'property_enquiries',
            'property_media',
            'property_searches',
            'property_types',
            'property_views',
            'redirections',
            'service_areas',
            'service_credentials',
            'service_enquiries',
            'service_media',
            'service_products',
            'service_searches',
            'service_views',
            'services',
            'settings',
            'static_content_groups',
            'static_contents',
            'user_billing_addresses',
            'user_details',
            'user_saved',
            'users'
        ];

        $_this = $this;
        foreach ($tables as $table) {
            self::dropSelectedTable($_this, $table);
        }
    }

    /**
     * Drop selected table
     *
     * @param object|null $obj       Migration object.
     * @param string|null $tableName Table name.
     *
     * @return void
     */
    public static function dropSelectedTable($obj, $tableName = '')
    {
        $table = $obj->table($tableName);
        if ($table->exists()) {
            $table->drop();
        }
    }
}
