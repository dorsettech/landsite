<?php

use Migrations\AbstractMigration;

/**
 * Class AddStripeCustomerIdToUserDetails
 */
class AddStripeCustomerIdToUserDetails extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('user_details');
        $table->addColumn('stripe_customer_id', 'string', [
            'comment' => 'Customer ID in Stripe payments system',
            'after' => 'marketing_agreement_3',
            'default' => null,
            'limit' => 255,
            'null' => true
        ])->update();
    }
}
