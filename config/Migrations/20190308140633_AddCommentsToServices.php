<?php

use Migrations\AbstractMigration;

/*
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */

/**
 * AddCommentsToServices
 */
class AddCommentsToServices extends AbstractMigration
{
    /**
     * Change
     *
     * @return void
     */
    public function change()
    {
        $this->table('services')
            ->changeColumn('description', 'string', [
                'comment' => 'Mandatory - business overview',
                'default' => '',
                'limit'   => 10000,
                'null'    => true
            ])
            ->changeColumn('opening_days', 'set', [
                'comment' => 'Optional - opening days of the week (0 - sunday, 6 - saturday)',
                'values'  => ['0', '1', '2', '3', '4', '5', '6'],
                'default' => null,
                'null'    => true
            ])
            ->update();
    }
}
