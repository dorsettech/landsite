<?php
use Migrations\AbstractMigration;

class CreateServiceClicks extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('service_clicks')
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => true
            ])
            ->addColumn('service_id', 'biginteger', [
                'limit' => 20,
                'null' => false,
                'signed' => false,
            ])->addColumn('element', 'integer', [
                'default' => 0,
                'limit' => \Phinx\Db\Adapter\MysqlAdapter::INT_TINY,
                'null' => false,
                'signed' => false,
                'comment' => 'Element identifier. Default 0 - website url'
            ])
            ->addIndex(['service_id']);

        $table->create();

        $table->changeColumn('id', 'biginteger', [
            'default' => null,
            'limit' => 20,
            'null' => false,
            'signed' => false,
            'identity' => true
        ]);

        $table->update();

        $table->addForeignKey(
            'service_id',
            'services',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'CASCADE'
            ]
        );

        $table->update();
    }
}
