<?php
use Migrations\AbstractMigration;

class ChangeHeadlineOnProperties extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('properties');
        $table->changeColumn('headline', 'string', [
            'comment' => 'Mandatory - description heading (first paragraph)',
            'limit' => 260,
            'null' => false
        ])->update();
    }
}
