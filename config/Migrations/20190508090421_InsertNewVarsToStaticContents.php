<?php

use Migrations\AbstractMigration;

class InsertNewVarsToStaticContents extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function up(): void
    {
        $now = date('Y-m-d H:i:s');
        $data = [
            [
                'static_group_id' => 9,
                'var_name' => 'content_title2',
                'value' => 'What are benefits of signing up?',
                'type' => 0,
                'description' => 'Second left hand side content title',
                'created' => $now
            ], [
                'static_group_id' => 9,
                'var_name' => 'content_title3',
                'value' => 'What are the costs?',
                'type' => 0,
                'description' => 'Third left hand side content title',
                'created' => $now
            ], [
                'static_group_id' => 9,
                'var_name' => 'content_description3',
                'value' => 'Membership to The Landsite is completely free and allows members full and unlimited access to all properties and professional services in our community, as well as our news & information portal.<br/><br/>To showcase your property or professional service, or to get in touch with property owners or service providers, there is a small fee. Showcasing your property or services with us is easy, and it\'s up to you how much you decide to pay for each profile - view our pricing structure below.',
                'type' => 4,
                'description' => 'Bottom content - paragraph',
                'created' => $now
            ], [
                'static_group_id' => 9,
                'var_name' => 'plan_standard_details',
                'value' => 'Your property or business profile will be visible to a targeted audience who search for what you have to offer. Your profile will be visible in search results only.',
                'type' => 0,
                'description' => 'Standard plan details',
                'created' => $now
            ], [
                'static_group_id' => 9,
                'var_name' => 'plan_premium_details',
                'value' => 'Your property or business profile will be visible in search results, highlighted in gold. It will be prioritised over standard listings, meaning that it always displays at the top of the search results.',
                'type' => 0,
                'description' => 'Premium plan details',
                'created' => $now
            ], [
                'static_group_id' => 9,
                'var_name' => 'plan_featured_details',
                'value' => 'All the benefits of Premium membership but in addition your property or business will be showcased in the sidebar next to property searches in the locations of your \'areas covered\' specified in your business profile.',
                'type' => 0,
                'description' => 'Featured plan details',
                'created' => $now
            ], [
                'static_group_id' => 9,
                'var_name' => 'bottom_slogan',
                'value' => 'No long-term contracts, no hidden pricing, just one streamlined professional property portal. Sign up today!',
                'type' => 0,
                'description' => 'Slogan text on the bottom',
                'created' => $now
            ], [
                'static_group_id' => 9,
                'var_name' => 'privacy_policy_info',
                'value' => 'By clicking the Submit button you agree to our website terms of use, our privacy policy and consent to cookies being stored on your device.',
                'type' => 0,
                'description' => 'Privacy policy information - below registration formSlogan text on the bottom',
                'created' => $now
            ]
        ];

        $this->insert('static_contents', $data);
    }

    public function down()
    {
        $this->execute('DELETE from static_contents WHERE var_name IN ("content_title2", "content_title3", "content_description3", "plan_standard_details", "plan_premium_details", "plan_featured_details", "bottom_slogan", "privacy_policy_info")');
    }
}
