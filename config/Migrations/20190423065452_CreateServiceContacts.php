<?php

use Migrations\AbstractMigration;

/**
 * Class CreateServiceContacts
 */
class CreateServiceContacts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('service_contacts')
            ->addColumn('service_id', 'biginteger', [
                'limit' => 20,
                'null' => false,
                'signed' => false,
            ])->addColumn('name', 'string', [
                'comment' => 'Contact person full name',
                'default' => '',
                'limit' => 255,
                'null' => true
            ])
            ->addColumn('email', 'string', [
                'comment' => 'Contact person email address',
                'default' => '',
                'limit' => 255,
                'null' => true
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => true
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => true
            ])
            ->addIndex(['service_id']);

        $table->create();

        $table->changeColumn('id', 'biginteger', [
            'default' => null,
            'limit' => 20,
            'null' => false,
            'signed' => false,
            'identity' => true
        ]);

        $table->update();

        $table->addForeignKey(
            'service_id',
            'services',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'CASCADE'
            ]
        );

        $table->update();
    }
}
