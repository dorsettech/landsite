<?php
/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */

use Migrations\AbstractMigration;

/**
 * ChangesLogsEntity
 */
class ChangesLogsEntity extends AbstractMigration
{

    /**
     * Change method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('logs');
        $table->changeColumn('entity', 'text', [
            'default' => null,
            'limit'   => Phinx\Db\Adapter\MysqlAdapter::TEXT_LONG,
            'null'    => true,
        ])->update();
    }
}
