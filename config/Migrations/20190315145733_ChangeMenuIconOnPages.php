<?php
/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */

use Migrations\AbstractMigration;

/**
 * ChangeMenuIconOnPages
 */
class ChangeMenuIconOnPages extends AbstractMigration
{

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('pages');
        $table->changeColumn('menu_icon', 'string', [
            'default' => null,
            'limit'   => 255,
            'null'    => true,
        ])->update();
    }
}
