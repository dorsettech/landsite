<?php
use Migrations\AbstractMigration;

/**
 * Class ChangePriceRentQualifierOnProperties
 */
class ChangePriceRentQualifierOnProperties extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change(): void
    {
        $values = ['D', 'POA', 'GP', 'FP', 'OIEO', 'OIRO', 'SBT', 'F', 'SO', 'OO', 'PB', 'PR', 'SE', 'OI', 'CS', 'NL'];

        $this->table('properties')->changeColumn('price_rent_qualifier', 'enum', [
            'values' => $values,
            'comment' => 'Mandatory - price qualifier for rent type',
            'default' => 'D',
            'null' => false,
            'after' => 'price_rent_per'
        ])->update();
    }
}
