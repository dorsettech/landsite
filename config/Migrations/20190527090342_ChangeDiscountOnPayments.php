<?php

use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class ChangeDiscountOnPayments extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('payments');
        $table->renameColumn('discount_type', 'discount_id')->update();
        $table->changeColumn('discount_id', 'integer', [
            'default' => null,
            'limit' => MysqlAdapter::INT_REGULAR,
            'null' => true,
            'signed' => false
        ])->update();
        $table->addIndex(['discount_id'])->update();
        $table->addForeignKey(
            'discount_id',
            'discounts',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'RESTRICT'
            ])->update();

    }
}
