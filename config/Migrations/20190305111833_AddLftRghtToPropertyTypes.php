<?php
/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */

use Cake\ORM\Locator\TableLocator;
use Migrations\AbstractMigration;

/**
 * AddLftRghtToPropertyTypes
 */
class AddLftRghtToPropertyTypes extends AbstractMigration
{
    /**
     * Up method
     *
     * @return void
     */
    public function up()
    {
        $this->table('property_types')
            ->addColumn('lft', 'biginteger', [
                'after'   => 'parent_id',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('rght', 'biginteger', [
                'after'   => 'lft',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->update();

        $tableLocator = new TableLocator();
        $propertyTypes = $tableLocator->get('PropertyTypes');
        $propertyTypes->recover();
    }

    /**
     * Down method
     *
     * @return void
     */
    public function down()
    {
        $this->table('property_types')
            ->removeColumn('lft')
            ->removeColumn('rght')
            ->update();
    }
}
