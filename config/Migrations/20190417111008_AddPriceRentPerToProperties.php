<?php

use Migrations\AbstractMigration;

/**
 * Class AddPriceRentPerToProperties
 */
class AddPriceRentPerToProperties extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('properties');
        $table->addColumn('price_rent_per', 'enum', [
            'values' => ['M', 'Y'],
            'comment' => 'Property rented per: M - month, Y - year/annum',
            'default' => 'M',
            'null' => false,
            'after' => 'price_rent'
        ]);
        $table->update();
    }
}
