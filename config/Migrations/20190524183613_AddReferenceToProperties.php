<?php
use Migrations\AbstractMigration;

class AddReferenceToProperties extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('properties')->addColumn('reference', 'string', [
            'limit' => 255,
            'default' => null,
            'null' => true,
            'after' => 'uid',
            'comment' => 'Optional - property reference'
        ])->update();
    }
}
