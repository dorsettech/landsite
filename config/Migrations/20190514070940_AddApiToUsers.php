<?php

use Migrations\AbstractMigration;

class AddApiToUsers extends AbstractMigration
{
    /**
     * Up method
     *
     * @return void
     */
    public function up()
    {
        $this->table('users')
            ->addColumn('api', 'boolean', [
                'after' => 'active',
                'null' => false,
                'default' => false,
                'comment' => 'Whether the user has access to REST API'
            ])
            ->update();
    }

    /**
     * Down method
     *
     * @return void
     */
    public function down()
    {
        $this->table('users')
            ->removeColumn('api')
            ->update();
    }
}
