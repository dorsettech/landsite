<?php
use Migrations\AbstractMigration;

class RemoveCompanyDescriptionFromUserDetails extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('user_details');
        $table->removeColumn('company_description');
        $table->update();
    }
}
