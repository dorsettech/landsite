<?php
use Migrations\AbstractMigration;

/**
 * Class AddDeleteReasonToProperties
 */
class AddDeleteReasonToProperties extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change(): void
    {
        $this->table('properties')->addColumn('deletion_reason', 'string', [
            'null' => true,
            'default' => null,
            'limit'=> 255,
            'comment' => 'Deletion reason',
            'after' => 'deleted'
        ])->update();
    }
}
