<?php
use Migrations\AbstractMigration;

class AddAddressToUserDetails extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('user_details');
        $table->addColumn('address', 'string', [
            'comment' => 'Optional - user address',
            'default' => null,
            'limit'   => 255,
            'null'    => true,
            'after'   => 'postcode'
        ])->update();
    }
}
