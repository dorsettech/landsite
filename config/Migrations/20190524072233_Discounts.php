<?php

/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */
use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

/**
 * Discounts
 */
class Discounts extends AbstractMigration
{
    /**
     * Up method
     *
     * @return void
     */
    public function up()
    {
        $this->table('discounts', Initial::getDBCollations(['id' => false, 'comment' => 'The Land Site - discount codes']))
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default'       => null,
                'limit'         => MysqlAdapter::INT_REGULAR,
                'null'          => false,
                'signed'        => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('user_id', 'biginteger', [
                'comment' => 'Instead of email, applies to: `null` for all members, any ID for specific member',
                'default' => null,
                'limit'   => MysqlAdapter::INT_BIG,
                'null'    => true,
                'signed'  => false,
            ])
            ->addColumn('name', 'string', [
                'comment' => 'Code name, for optional display in panel or cart',
                'default' => null,
                'limit'   => 200,
                'null'    => true,
            ])
            ->addColumn('code', 'string', [
                'comment' => 'Coupon code name',
                'default' => null,
                'limit'   => 15,
                'null'    => false,
            ])
            ->addColumn('date_start', 'date', [
                'comment' => 'Start date - the date from when the discount code is active',
                'default' => null,
                'limit'   => MysqlAdapter::PHINX_TYPE_DATE,
                'null'    => true,
            ])
            ->addColumn('date_end', 'date', [
                'comment' => 'End date - the date when the discount code expires and can\'t be used anymore',
                'default' => null,
                'limit'   => MysqlAdapter::PHINX_TYPE_DATE,
                'null'    => true,
            ])
            ->addColumn('type', 'enum', [
                'comment' => 'Discount type',
                'values'  => ['FIXEDSUM', 'FREE', 'PERCENTAGE'],
                'default' => null,
                'null'    => false,
            ])
            ->addColumn('value', 'decimal', [
                'comment'   => 'Discount amount - will show up if the admin chooses either Percentage or Fixed sum in the Discount type dropdown',
                'default'   => null,
                'precision' => 10,
                'scale'     => 4,
                'null'      => true,
            ])
            ->addColumn('per_customer', 'integer', [
                'comment' => 'Uses per customer - the admin will have the ability to choose the following options; null for unlimited, any value for max uses',
                'default' => null,
                'limit'   => MysqlAdapter::INT_REGULAR,
                'null'    => true,
            ])
            ->addColumn('per_code', 'integer', [
                'comment' => 'Uses per code - the admin will have the ability to choose the following options; null for unlimited, any value for max uses',
                'default' => null,
                'limit'   => MysqlAdapter::INT_REGULAR,
                'null'    => true,
            ])
            ->addColumn('active', 'boolean', [
                'comment' => 'Status - Enabled/Disabled',
                'default' => true,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('deleted', 'boolean', [
                'default' => false,
                'limit'   => null,
                'null'    => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addIndex(['code'], [
                'unique' => true,
                'name'   => 'discounts_code_idx'
            ])
            ->create();
    }

    /**
     * Down method
     *
     * @return void
     */
    public function down()
    {
        $this->table('discounts')
            ->drop();
    }
}
