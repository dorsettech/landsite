<?php
/**
 * eConnect4u
 *
 * @author Stefan <marcin.nierobis@econnect4u.pl>
 */

use Migrations\AbstractMigration;

/**
 * AddImageFieldToMenus
 */
class AddImageFieldToMenus extends AbstractMigration
{


    /**
     * Up method
     *
     * @return void
     */
    public function up()
    {
        $this->table('menus')
            ->addColumn('image', 'string', [
                'after'   => 'name',
                'limit'   => 255,
                'null'    => true,
            ])
            ->update();
    }

    /**
     * Down method
     *
     * @return void
     */
    public function down()
    {
        $this->table('menus')
            ->removeColumn('image')
            ->update();
    }
}
