<?php
use Migrations\AbstractMigration;

class ChangeDescriptionOnServices2 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('services');
        $table->changeColumn('description', 'string', [
            'default' => '',
            'limit' => 10000,
            'null' => true
        ])->update();
    }
}
