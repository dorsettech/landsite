<?php
use Migrations\AbstractMigration;

class FixPropertiesExpirationDates extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute('UPDATE properties SET ad_expiry_date = DATE_ADD(publish_date, INTERVAL 90 DAY), status = "EXPIRED" WHERE created <= "2019-05-31 23:59:59" AND DATE_ADD(publish_date, INTERVAL 90 DAY) < NOW()');
        $this->execute('UPDATE properties SET ad_expiry_date = DATE_ADD(publish_date, INTERVAL 90 DAY), status = "PUBLISHED" WHERE created <= "2019-05-31 23:59:59" AND DATE_ADD(publish_date, INTERVAL 90 DAY) >= NOW()');
        $this->execute('UPDATE properties SET ad_expiry_date = DATE_ADD(publish_date, INTERVAL 30 DAY), status = "EXPIRED" WHERE created > "2019-05-31 23:59:59" AND DATE_ADD(publish_date, INTERVAL 30 DAY) < NOW()');
        $this->execute('UPDATE properties SET ad_expiry_date = DATE_ADD(publish_date, INTERVAL 30 DAY), status = "PUBLISHED" WHERE created > "2019-05-31 23:59:59" AND DATE_ADD(publish_date, INTERVAL 30 DAY) >= NOW()');
    }

}
