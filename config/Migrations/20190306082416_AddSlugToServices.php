<?php
use Migrations\AbstractMigration;

class AddSlugToServices extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('services');
        $table->addColumn('slug', 'string', [
            'comment' => 'Mandatory - slug part of URL for service/company title (not unique)',
            'limit' => 70,
            'null'    => false,
            'after'   => 'uid'
        ]);

        $table->update();
    }
}
