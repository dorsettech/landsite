<?php
use Migrations\AbstractMigration;

class AddPriceSalePerToProperties extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('properties');
        $table->addColumn('price_sale_per', 'enum', [
            'values' => ['F', 'FT2', 'M2', 'POA'],
            'comment' => 'Property rented per: F - full sale price, FT2 - square feet, M2 - square meter, POA - price on application',
            'default' => 'F',
            'null' => false,
            'after' => 'price_sale'
        ]);
        $table->update();
    }
}
