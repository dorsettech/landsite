<?php

/**
 * eConnect4u
 *
 * @author Stefan <marcin@econnect4u.pl>
 */
use Migrations\AbstractMigration;

/**
 * AddTimestampColumnsToServices
 */
class AddTimestampColumnsToPropertiesTable extends AbstractMigration
{

    /**
     * Up method
     *
     * @return void
     */
    public function up()
    {
        $this->table('properties')
            ->addColumn('created', 'datetime', [
                'after'   => 'views',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->addColumn('modified', 'datetime', [
                'after'   => 'created',
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->update();
    }

    /**
     * Down method
     *
     * @return void
     */
    public function down()
    {
        $this->table('properties')
            ->removeColumn('created')
            ->removeColumn('modified')
            ->update();
    }
}
