<?php
/**
 * eConnect4u
 */
$websiteConfig = require CONFIG . 'website.php';

$databaseConfig = (isset($websiteConfig['Databases']) && !empty($websiteConfig['Databases'])) ? $websiteConfig['Databases'] : false;

if (!is_array($databaseConfig) || !array_key_exists('default', $databaseConfig)) {
    throw new \RuntimeException('Missing database config - default');
}

if (isset($_SERVER['SERVER_NAME'], $databaseConfig[$_SERVER['SERVER_NAME']])) {
    $database = $databaseConfig[$_SERVER['SERVER_NAME']];
} else {
    $database = $databaseConfig['default'];
}
$customCacheEnabled = false;

if (extension_loaded('memcached')) {
    $cacheType = 'Memcached';
} else if (extension_loaded('apc')) {
    $cacheType = 'Apc';
} else {
    $cacheType = 'File';
}

if ($customCacheEnabled) {
    $customCaches = array(
        'models' => '+1 day',
        'core' => '+1 week',
        'long' => '+1 week',
        'short' => '+1 hours'
    );
} else {
    $customCaches = array(
        'models' => '+0 minutes',
        'core' => '+0 minutes',
        'long' => '+0 second',
        'short' => '+0 second'
    );
}

$config = [
    /**
     * Configure basic information about the application.
     *
     * - namespace - The namespace to find app classes under.
     * - encoding - The encoding used for HTML + database connections.
     * - base - The base directory the app resides in. If false this
     *   will be auto detected.
     * - dir - Name of app directory.
     * - webroot - The webroot directory.
     * - wwwRoot - The file path to webroot.
     * - baseUrl - To configure CakePHP to *not* use mod_rewrite and to
     *   use CakePHP pretty URLs, remove these .htaccess
     *   files:
     *      /.htaccess
     *      /webroot/.htaccess
     *   And uncomment the baseUrl key below.
     * - fullBaseUrl - A base URL to use for absolute links.
     * - imageBaseUrl - Web path to the public images directory under webroot.
     * - cssBaseUrl - Web path to the public css directory under webroot.
     * - jsBaseUrl - Web path to the public js directory under webroot.
     * - paths - Configure paths for non class based resources. Supports the
     *   `plugins`, `templates`, `locales` subkeys, which allow the definition of
     *   paths for plugins, view templates and locale files respectively.
     */
    'Website' => $websiteConfig['Website'] ?? null,
    'Domain' => $websiteConfig['Domain'] ?? null,
    'Logger' => $websiteConfig['Logger'] ?? null,
    'App' => [
        'namespace' => 'App',
        'encoding' => env('APP_ENCODING', 'UTF-8'),
        'defaultLocale' => env('APP_DEFAULT_LOCALE', 'en_GB'),
        'base' => false,
        'dir' => 'src',
        'webroot' => 'webroot',
        'wwwRoot' => WWW_ROOT,
        // 'baseUrl' => env('SCRIPT_NAME'),
        'fullBaseUrl' => false,
        'imageBaseUrl' => 'img/',
        'cssBaseUrl' => 'css/',
        'jsBaseUrl' => 'js/',
        'paths' => [
            'plugins' => [ROOT . DS . 'plugins' . DS],
            'templates' => [APP . 'Template' . DS],
            'locales' => [APP . 'Locale' . DS],
        ],
    ],
    /**
     * Security and encryption configuration
     *
     * - salt - A random string used in security hashing methods.
     *   The salt value is also used as the encryption key.
     *   You should treat it as extremely sensitive data.
     */
    'Security' => [
        'salt' => env('SECURITY_SALT', 'de62f516d4967367a2f552e5b2639d02cdede4d44a24d1fe238da9b9215e9704'),
    ],
    /**
     * Apply timestamps with the last modified time to static assets (js, css, images).
     * Will append a querystring parameter containing the time the file was modified.
     * This is useful for busting browser caches.
     *
     * Set to true to apply timestamps when debug is true. Set to 'force' to always
     * enable timestamping regardless of debug value.
     */
    'Asset' => [
        'timestamp' => 'force'
    ],
    /**
     * Configure the cache adapters.
     */
    'Cache' => [
        'default' => [
            'className' => 'File',
            'path' => CACHE,
            'url' => env('CACHE_DEFAULT_URL', null),
        ],
        '_cake_core_' => [
            'className' => 'File',
            'prefix' => 'myapp_cake_core_',
            'path' => CACHE . 'persistent/',
            'serialize' => true,
            'duration' => $customCaches['core'],
            'url' => env('CACHE_CAKECORE_URL', null),
        ],
        '_cake_model_' => [
            'className' => 'File',
            'prefix' => 'myapp_cake_model_',
            'path' => CACHE . 'models/',
            'serialize' => true,
            'duration' => $customCaches['models'],
            'url' => env('CACHE_CAKEMODEL_URL', null),
        ],
        '_cake_routes_' => [
            'className' => 'Cake\Cache\Engine\FileEngine',
            'prefix' => 'myapp_cake_routes_',
            'path' => CACHE,
            'serialize' => true,
            'duration' => '+1 years',
            'url' => env('CACHE_CAKEROUTES_URL', null),
        ],
        'short' => [
            'className' => $cacheType,
            'duration' => $customCaches['short'],
            'probability' => 100,
            'path' => CACHE . 'short' . DS,
        ],
        'long' => [
            'className' => $cacheType,
            'duration' => $customCaches['long'],
            'probability' => 100,
            'path' => CACHE . 'long' . DS,
        ]
    ],
    /**
     * Configure the Error and Exception handlers used by your application.
     *
     * By default errors are displayed using Debugger, when debug is true and logged
     * by Cake\Log\Log when debug is false.
     *
     * In CLI environments exceptions will be printed to stderr with a backtrace.
     * In web environments an HTML page will be displayed for the exception.
     * With debug true, framework errors like Missing Controller will be displayed.
     * When debug is false, framework errors will be coerced into generic HTTP errors.
     *
     * Options:
     *
     * - `errorLevel` - int - The level of errors you are interested in capturing.
     * - `trace` - boolean - Whether or not backtraces should be included in
     *   logged errors/exceptions.
     * - `log` - boolean - Whether or not you want exceptions logged.
     * - `exceptionRenderer` - string - The class responsible for rendering
     *   uncaught exceptions.  If you choose a custom class you should place
     *   the file for that class in src/Error. This class needs to implement a
     *   render method.
     * - `skipLog` - array - List of exceptions to skip for logging. Exceptions that
     *   extend one of the listed exceptions will also be skipped for logging.
     *   E.g.:
     *   `'skipLog' => ['Cake\Network\Exception\NotFoundException', 'Cake\Network\Exception\UnauthorizedException']`
     * - `extraFatalErrorMemory` - int - The number of megabytes to increase
     *   the memory limit by when a fatal error is encountered. This allows
     *   breathing room to complete logging or error handling.
     */
    'Error' => [
        'errorLevel' => E_ALL,
        'exceptionRenderer' => 'Cake\Error\ExceptionRenderer',
        'skipLog' => [],
        'log' => true,
        'trace' => true
    ],
    /**
     * Email configuration.
     *
     * By defining transports separately from delivery profiles you can easily
     * re-use transport configuration across multiple profiles.
     *
     * You can specify multiple configurations for production, development and
     * testing.
     *
     * Each transport needs a `className`. Valid options are as follows:
     *
     *  Mail   - Send using PHP mail function
     *  Smtp   - Send using SMTP
     *  Debug  - Do not send the email, just return the result
     *
     * You can add custom transports (or override existing transports) by adding the
     * appropriate file to src/Mailer/Transport.  Transports should be named
     * 'YourTransport.php', where 'Your' is the name of the transport.
     */
    // 'EmailTransport' => [
    //     'default' => [
    //         'className' => 'Smtp',
    //         'host' => $websiteConfig['MailSmtp']['host'] ?? 'ecp4u.pl',
    //         'port' => $websiteConfig['MailSmtp']['port'] ?? 587,
    //         'timeout' => $websiteConfig['MailSmtp']['timeout'] ?? 30,
    //         'username' => $websiteConfig['MailSmtp']['username'] ?? 'cms@ecp4u.pl',
    //         'password' => $websiteConfig['MailSmtp']['password'] ?? 'WrBQBJ@%TDOF',
    //         'client' => $websiteConfig['MailSmtp']['client'] ?? null,
    //         'tls' => $websiteConfig['MailSmtp']['tls'] ?? null,
    //         'context' => [
    //             'ssl' => [
    //                 'verify_peer' => false,
    //                 'verify_peer_name' => false,
    //                 'allow_self_signed' => true
    //             ],
    //         ],
    //     ],
    //     'debug' => [
    //         'className' => 'Debug'
    //     ],
    // ],
    'EmailTransport' => [
        'default' => [
            'className' => 'Smtp',
            'host' => $websiteConfig['MailSmtp']['host'] ?? 'email-smtp.eu-west-1.amazonaws.com',
            'port' => $websiteConfig['MailSmtp']['port'] ?? 587,
            'timeout' => $websiteConfig['MailSmtp']['timeout'] ?? 30,
            'username' => $websiteConfig['MailSmtp']['username'] ?? 'AKIARJ5VLREQPGPZYTG4',
            'password' => $websiteConfig['MailSmtp']['password'] ?? 'BO4tQzKofabIF23kTK+1NAsks/D5t6nv2cufVLStfs18',
            'client' => $websiteConfig['MailSmtp']['client'] ?? null,
            'tls' => $websiteConfig['MailSmtp']['tls'] ?? true,
            'context' => [
                'ssl' => [
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                ],
            ],
        ],
        'debug' => [
            'className' => 'Debug'
        ],
    ],
    /**
     * Email delivery profiles
     *
     * Delivery profiles allow you to predefine various properties about email
     * messages from your application and give the settings a name. This saves
     * duplication across your application and makes maintenance and development
     * easier. Each profile accepts a number of keys. See `Cake\Mailer\Email`
     * for more information.
     */
    'Email' => [
        'default' => [
            'transport' => 'default',
            'from' => $websiteConfig['MailSmtp']['from'] ?? 'noreply@thelandsite.co.uk',
            //'charset' => 'utf-8',
            //'headerCharset' => 'utf-8',
        ],
        'debug' => [ // returns the result into $email variable instead of sending it
            'transport' => 'debug',
            'from' => 'debug@localhost',
            'charset' => 'utf-8',
            'headerCharset' => 'utf-8'
        ],
    ],
    /**
     * Connection information used by the ORM to connect
     * to your application's datastores.
     * Do not use periods in database name - it may lead to error.
     * See https://github.com/cakephp/cakephp/issues/6471 for details.
     * Drivers include Mysql Postgres Sqlite Sqlserver
     * See vendor\cakephp\cakephp\src\Database\Driver for complete list
     */
    'Datasources' => [
        'default' => [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Mysql',
            'persistent' => false,
            'host' => $database['host'],
            'username' => $database['username'],
            'password' => $database['password'],
            'database' => $database['database'],
            'encoding' => 'utf8',
            'timezone' => 'UTC',
            'port' => 3306,
            'flags' => [],
            'cacheMetadata' => true,
            'log' => false,
            'quoteIdentifiers' => false,
            'url' => env('DATABASE_URL', null),
            //'port' => 'non_standard_port_number',
            //'init' => ['SET GLOBAL innodb_stats_on_metadata = 0']
        ],
    ],
    /**
     * Configures logging options
     */
    'Log' => [
        'debug' => [
            'className' => 'Cake\Log\Engine\FileLog',
            'path' => LOGS,
            'file' => 'debug',
            'levels' => ['notice', 'info', 'debug'],
            'url' => env('LOG_DEBUG_URL', null),
        ],
        'error' => [
            'className' => 'Cake\Log\Engine\FileLog',
            'path' => LOGS,
            'file' => 'error',
            'levels' => ['warning', 'error', 'critical', 'alert', 'emergency'],
            'url' => env('LOG_ERROR_URL', null),
        ],
    ],
    /**
     * Session configuration.
     *
     * Contains an array of settings to use for session configuration. The
     * `defaults` key is used to define a default preset to use for sessions, any
     * settings declared here will override the settings of the default config.
     *
     * ## Options
     *
     * - `cookie` - The name of the cookie to use. Defaults to 'CAKEPHP'.
     * - `cookiePath` - The url path for which session cookie is set. Maps to the
     *   `session.cookie_path` php.ini config. Defaults to base path of app.
     * - `timeout` - The time in minutes the session should be valid for.
     *    Pass 0 to disable checking timeout.
     *    Please note that php.ini's session.gc_maxlifetime must be equal to or greater
     *    than the largest Session['timeout'] in all served websites for it to have the
     *    desired effect.
     * - `defaults` - The default configuration set to use as a basis for your session.
     *    There are four built-in options: php, cake, cache, database.
     * - `handler` - Can be used to enable a custom session handler. Expects an
     *    array with at least the `engine` key, being the name of the Session engine
     *    class to use for managing the session. CakePHP bundles the `CacheSession`
     *    and `DatabaseSession` engines.
     * - `ini` - An associative array of additional ini values to set.
     *
     * The built-in `defaults` options are:
     *
     * - 'php' - Uses settings defined in your php.ini.
     * - 'cake' - Saves session files in CakePHP's /tmp directory.
     * - 'database' - Uses CakePHP's database sessions.
     * - 'cache' - Use the Cache class to save sessions.
     *
     * To define a custom session handler, save it at src/Network/Session/<name>.php.
     * Make sure the class implements PHP's `SessionHandlerInterface` and set
     * Session.handler to <name>
     *
     * To use database sessions, load the SQL file located at config/Schema/sessions.sql
     */
    'Session' => $websiteConfig['Domain']['subdomains'] ? array_merge(
        $websiteConfig['Session'], [
        'ini' => [
            'session.cookie_path' => '/',
            'session.cookie_domain' => ".{$websiteConfig['Domain']['root']}"
        ]
    ]) : $websiteConfig['Session'],
    'Search' => [
        'full_text' => true,
    ],
];
$debugAllowed = [
    //'31.179.149.78',
];
$config['debug'] = (in_array(@$_SERVER['HTTP_HOST'], $debugAllowed, true) || in_array(@$_SERVER['REMOTE_ADDR'], $debugAllowed, true));
$config['Datasources']['default']['log'] = false;

return $config;
