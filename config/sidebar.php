<?php

use Cake\ORM\TableRegistry;

$Menus        = TableRegistry::get('Menus');
$sidebarMenus = $Menus->getSidebarMenus();

$StaticContentGroups = TableRegistry::get('StaticContentGroups');
$sidebarLayouts      = $StaticContentGroups->getSidebarLayouts();

$sidebarArray = [
    [
        "controller" => "Dashboard",
        "icon"       => "home",
        "label"      => "Dashboard",
        "enable"     => true,
    ],
    [
        "icon"     => "menu",
        "label"    => "CMS",
        "enable"   => true,
        "children" => [
            [
                "controller" => "Pages",
                "action"     => "add",
                "label"      => "Add page",
                "enable"     => true,
            ],
            [
                "controller" => "Pages",
                "action"     => "index",
                "label"      => "All pages",
                "enable"     => true,
            ],
            [
                "label"    => "Menus",
                "enable"   => true,
                "children" => array_merge($sidebarMenus, [[
                    "controller" => "Menus",
                    "action"     => "index",
                    "label"      => "Manage",
                    "enable"     => true,
                ]]),
            ],
            [
                "label"    => "Layout settings",
                "enable"   => true,
                "children" => array_merge([[
                    'controller' => 'StaticContentGroups',
                    'action'     => 'add',
                    'label'      => 'Add setting group',
                    // 'enable'     => ($_user['is_root']) ? true : false
                    ]], $sidebarLayouts)
            ],
            [
                "controller" => "CustomCss",
                "label"      => "Custom CSS",
                "enable"     => true,
            ],
            [
                "controller" => "SeoAnalyser",
                "label"      => "Seo analyser",
                "enable"     => true
            ]
        ]
    ],
    [
        "icon"       => "account_box",
        "controller" => "Members",
        "action"     => "index",
        "label"      => "Members",
        "enable"     => true,
    ],
    [
        "icon"     => "credit_card",
        "label"    => "Payments",
        "enable"   => true,
        "children" => [
            [
                "controller" => "Payments",
                "action"     => "index",
                "label"      => "Payments list",
                "enable"     => true,
            ],
            [
                "controller" => "Settings",
                "action"     => "prices",
                "label"      => "Prices",
                "enable"     => true,
            ],
        ]
    ],
    [
        "icon"       => "table_chart",
        "controller" => "Reports",
        "action"     => "index",
        "label"      => "Reports",
        "enable"     => true
    ],
    [
        "icon"     => "dvr",
        "label"    => "News",
        "enable"   => true,
        "children" => [
            [
                "controller" => "News",
                "action"     => "add",
                "label"      => "Add new",
                "enable"     => true,
            ],
            [
                "controller" => "News",
                "action"     => "index",
                "label"      => "List",
                "enable"     => true,
            ],
        ]
    ],
    [
        "icon"     => "business",
        "label"    => "Properties",
        "enable"   => true,
        "children" => [
            [
                "controller" => "Properties",
                "action"     => "index",
                "label"      => "List",
                "enable"     => true,
            ],
            [
                "controller" => "PropertyTypes",
                "action"     => "index",
                "label"      => "Property Types",
                "enable"     => true,
            ],
            [
                "controller" => "PropertyAttributes",
                "action"     => "index",
                "label"      => "Property Attributes",
                "enable"     => true,
            ],
            [
                "controller" => "PropertySearches",
                "action"     => "index",
                "label"      => "Searches",
                "enable"     => true,
            ],
        ]
    ],
    [
        "icon"     => "local_play",
        "label"    => "Professional Services",
        "enable"   => true,
        "children" => [
            [
                "controller" => "Services",
                "action"     => "index",
                "label"      => "List",
                "enable"     => true,
            ],
            [
                "controller" => "Areas",
                "action"     => "index",
                "label"      => "Areas we cover",
                "enable"     => true,
            ],
            [
                "controller" => "Accreditations",
                "action"     => "index",
                "label"      => "Accreditations",
                "enable"     => false,
            ],
            [
                "controller" => "Associations",
                "action"     => "index",
                "label"      => "Associations",
                "enable"     => false,
            ],
            [
                "controller" => "CaseStudies",
                "action"     => "index",
                "label"      => "Case Studies",
                "enable"     => true,
            ],
            [
                "controller" => "Categories",
                "action"     => "index",
                "label"      => "Categories",
                "enable"     => true,
            ],
            [
                "controller" => "Channels",
                "action"     => "index",
                "label"      => "Channels",
                "enable"     => true,
            ],
            [
                "controller" => "Products",
                "action"     => "index",
                "label"      => "Products & Services",
                "enable"     => true,
            ],
            [
                "controller" => "ServiceSearches",
                "action"     => "index",
                "label"      => "Searches",
                "enable"     => true,
            ],
        ]
    ],
    [
        "controller" => "Discounts",
        "action"     => "index",
        "icon"       => "money",
        "label"      => "Discounts",
        "enable"     => true,
    ],
    [
        "plugin"     => "FileManager",
        "controller" => "Manager",
        "action"     => "index",
        "icon"       => "photo_library",
        "label"      => "Media Library",
        "enable"     => true,
    ],
    [
        "controller" => "EmailQueue",
        "action"     => "index",
        "icon"       => "list_alt",
        "label"      => "Emails List",
        "enable"     => true,
    ],
    [
        "icon"     => "mail_outline",
        "label"    => "Contact Forms",
        "enable"   => true,
        "children" => [
            [
                "controller" => "Forms",
                "action"     => "add",
                "label"      => "Add contact form",
                "enable"     => true,
            ],
            [
                "controller" => "Forms",
                "action"     => "index",
                "label"      => "List",
                "enable"     => true,
            ]
        ]
    ],
    [
        "icon"     => "fingerprint",
        "label"    => "Access",
        "enable"   => true,
        "children" => [
            [
                "controller" => "Users",
                "action"     => "index",
                "label"      => "Users",
                "enable"     => true,
            ],
            [
                "controller" => "Groups",
                "action"     => "index",
                "label"      => "Groups",
                "enable"     => true,
            ]
        ],
    ],
    [
        "icon"     => "settings",
        "label"    => "Settings",
        "enable"   => true,
        "children" => [
            [
                "controller" => "Settings",
                "action"     => "prices",
                "label"      => "Prices",
                "enable"     => true,
            ],
            [
                "controller" => "Languages",
                "label"      => "Languages",
                "enable"     => true,
            ],
            [
                "controller" => "Miscellaneous",
                "action"     => "sitemap",
                "label"      => "Sitemap",
                "enable"     => true,
            ],
            [
                "controller" => "Redirections",
                "label"      => "Redirections",
                "enable"     => true,
            ],
            [
                "controller" => "Logs",
                "action"     => "index",
                "label"      => "Logs",
                "enable"     => true,
            ],
            [
                "controller" => "Loginbans",
                "action"     => "index",
                "label"      => "Login Bans",
                "enable"     => true,
            ],
        ]
    ]
];

$pluginsDIR = ROOT . DS . 'plugins';
$plugins    = (new Cake\Filesystem\Folder($pluginsDIR))->read();
if (isset($plugins[0]) && !empty($plugins[0])) {
    foreach ($plugins[0] as $plugin) {
        $file = $pluginsDIR . DS . $plugin . DS . 'src' . DS . 'Template' . DS . 'Panel' . DS . 'Element' . DS . 'sidebar-plugin.ctp';
        if (file_exists($file)) {
            $sidebarArray[] = require_once $file;
        }
    }
}

return ['Sidebar' => $sidebarArray];
