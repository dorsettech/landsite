-- Zrzut struktury wyzwalacz thelandsite.properties_before_insert
DROP TRIGGER IF EXISTS `properties_before_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `properties_before_insert` BEFORE INSERT ON `properties` FOR EACH ROW BEGIN
	IF NEW.uid IS NULL OR NEW.uid = "" THEN
		SET NEW.uid = REPLACE(UUID(), "-","");
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Zrzut struktury wyzwalacz thelandsite.property_enquiries_after_delete
DROP TRIGGER IF EXISTS `property_enquiries_after_delete`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
DELIMITER //
CREATE TRIGGER `property_enquiries_after_delete` AFTER DELETE ON `property_enquiries` FOR EACH ROW BEGIN
	UPDATE properties p SET p.enquiries = p.enquiries - 1 WHERE p.id = OLD.property_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Zrzut struktury wyzwalacz thelandsite.property_enquiries_after_insert
DROP TRIGGER IF EXISTS `property_enquiries_after_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
DELIMITER //
CREATE TRIGGER `property_enquiries_after_insert` AFTER INSERT ON `property_enquiries` FOR EACH ROW BEGIN
	UPDATE properties p SET p.enquiries = p.enquiries + 1 WHERE p.id = NEW.property_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Zrzut struktury wyzwalacz thelandsite.property_views_after_delete
DROP TRIGGER IF EXISTS `property_views_after_delete`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
DELIMITER //
CREATE TRIGGER `property_views_after_delete` AFTER DELETE ON `property_views` FOR EACH ROW BEGIN
	UPDATE properties p SET p.views = p.views - 1 WHERE p.id = OLD.property_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Zrzut struktury wyzwalacz thelandsite.property_views_after_insert
DROP TRIGGER IF EXISTS `property_views_after_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
DELIMITER //
CREATE TRIGGER `property_views_after_insert` AFTER INSERT ON `property_views` FOR EACH ROW BEGIN
	UPDATE properties p SET p.views = p.views + 1 WHERE p.id = NEW.property_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Zrzut struktury wyzwalacz thelandsite.services_before_insert
DROP TRIGGER IF EXISTS `services_before_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `services_before_insert` BEFORE INSERT ON `services` FOR EACH ROW BEGIN
	IF NEW.uid IS NULL OR NEW.uid = "" THEN
		SET NEW.uid = REPLACE(UUID(), "-","");
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Zrzut struktury wyzwalacz thelandsite.service_enquiries_after_delete
DROP TRIGGER IF EXISTS `service_enquiries_after_delete`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
DELIMITER //
CREATE TRIGGER `service_enquiries_after_delete` AFTER DELETE ON `service_enquiries` FOR EACH ROW BEGIN
	UPDATE services s SET s.enquiries = s.enquiries - 1 WHERE s.id = OLD.service_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Zrzut struktury wyzwalacz thelandsite.service_enquiries_after_insert
DROP TRIGGER IF EXISTS `service_enquiries_after_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
DELIMITER //
CREATE TRIGGER `service_enquiries_after_insert` AFTER INSERT ON `service_enquiries` FOR EACH ROW BEGIN
	UPDATE services s SET s.enquiries = s.enquiries + 1 WHERE s.id = NEW.service_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Zrzut struktury wyzwalacz thelandsite.service_views_after_delete
DROP TRIGGER IF EXISTS `service_views_after_delete`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
DELIMITER //
CREATE TRIGGER `service_views_after_delete` AFTER DELETE ON `service_views` FOR EACH ROW BEGIN
	UPDATE services s SET s.views = s.views - 1 WHERE s.id = OLD.service_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Zrzut struktury wyzwalacz thelandsite.service_views_after_insert
DROP TRIGGER IF EXISTS `service_views_after_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
DELIMITER //
CREATE TRIGGER `service_views_after_insert` AFTER INSERT ON `service_views` FOR EACH ROW BEGIN
	UPDATE services s SET s.views = s.views + 1 WHERE s.id = NEW.service_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

DROP TRIGGER IF EXISTS `users_before_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
DELIMITER //
CREATE TRIGGER `users_before_insert` BEFORE INSERT ON `users` FOR EACH ROW BEGIN
    IF NEW.uid IS NULL OR NEW.uid = "" THEN
        SET NEW.uid = REPLACE(UUID(), "-","");
    END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;
