<?php
return [
    'nextActive' => '<li class="page-item"><a href="{{url}}" class="page-link next"><i class="fas fa-chevron-right"></i></a></li>',
    'nextDisabled' => '<li class="page-item disabled"><a class="page-link next"><i class="fas fa-chevron-right"></i></a></li>',
    'prevActive' => '<li class="page-item"><a href="{{url}}" class="page-link prev"><i class="fas fa-chevron-left"></i></a></li>',
    'prevDisabled' => '<li class="page-item disabled"><a class="page-link prev"><i class="fas fa-chevron-left"></i></a></li>',
    'counterRange' => '{{start}} - {{end}} of {{count}}',
    'counterPages' => '{{page}} of {{pages}}',
    'first' => '<li class="page-item"><a href="{{url}}" class="page-link">{{text}}</a></li>',
    'last' => '<li class="page-item"><a href="{{url}}" class="page-link">{{text}}</a></li>',
    'number' => '<li class="page-item"><a href="{{url}}" class="page-link">{{text}}</a></li>',
    'current' => '<li class="page-item active"><a href="{{url}}" class="page-link">{{text}}</a></li>',
    'ellipsis' => '<li class="ellipsis disabled"><a>&hellip;</a></li>',
    'sort' => '<a href="{{url}}">{{text}}</a>',
    'sortAsc' => '<a class="asc" href="{{url}}">{{text}}</a>',
    'sortDesc' => '<a class="desc" href="{{url}}">{{text}}</a>',
    'sortAscLocked' => '<a class="asc locked" href="{{url}}">{{text}}</a>',
    'sortDescLocked' => '<a class="desc locked" href="{{url}}">{{text}}</a>',
];
