<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use App\Routing\SubdomainRouter;
use App\Routing\SubdomainRouterBuilder;
use Cake\Core\Configure;
use Cake\Routing\Route\DashedRoute;
use Cake\Routing\Route\InflectedRoute;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

Router::defaultRouteClass(DashedRoute::class);

/*
 * Subdomain/s routing
 *
 * Has to be placed before all the rest of routing scopes and prefixes.
 */
$subdomains = Configure::read('Domain.subdomains');
if ($subdomains) {
    SubdomainRouter::scope('/', static function (SubdomainRouterBuilder $routes) use ($subdomains) {
        array_walk($subdomains, static function ($subdomain) use ($routes) {
            $routes->setHost("$subdomain." . strtr($_SERVER['HTTP_HOST'], ["$subdomain." => '', 'www.' => '']));
            if ($subdomain === 'members') {
                $routes->setExtensions(['json']);
                $routes->connect('/:controller', ['prefix' => $subdomain, 'plugin' => null]);
                $routes->connect('/:controller/:action', ['prefix' => $subdomain, 'plugin' => null]);
                $routes->connect('/', ['prefix' => $subdomain, 'controller' => 'Dashboard', 'action' => 'index']);
                $routes->connect('/login', ['prefix' => $subdomain, 'controller' => 'User', 'action' => 'index']);
                $routes->connect('/logout', ['prefix' => $subdomain, 'controller' => 'User', 'action' => 'logout']);
                $routes->connect('/register', ['prefix' => $subdomain, 'controller' => 'User', 'action' => 'register']);

                $routes->connect('/forumredirect', ['prefix' => $subdomain, 'controller' => 'Dashboard', 'action' => 'forumredirect']);

                //$routes->connect('/unsubscribe/:token', ['prefix' => $subdomain, 'controller' => 'User', 'action' => 'unsubscribe'], ['pass' => ['token']]);
                $routes->connect('/unsubscribe', ['prefix' => $subdomain, 'controller' => 'User', 'action' => 'unsubscribe']);

                $routes->connect('/forgot-password', ['prefix' => $subdomain, 'controller' => 'User', 'action' => 'password']);
                $routes->connect('/reset-password/:token', ['prefix' => $subdomain, 'controller' => 'User', 'action' => 'password'], ['pass' => ['token']]);
                $routes->connect('/verify-email/:token', ['prefix' => $subdomain, 'controller' => 'User', 'action' => 'verifyEmail'], ['pass' => ['token']]);
                $routes->connect('/statics/map/:flag', ['prefix' => $subdomain, 'controller' => 'Statics', 'action' => 'index'], ['pass' => ['flag']]);
                $routes->connect('/invoice/:thumb', ['prefix' => $subdomain, 'controller' => 'Payment', 'action' => 'invoice'], ['pass' => ['thumb']]);
                $routes->connect('/upgrade-your-browser', ['prefix' => $subdomain, 'controller' => 'App', 'action' => 'upgradeBrowser']);
            } elseif ($subdomain === 'api') {
                /**
                 * API should support headers instead of extensions (compatibility with rightmove.co.uk api)
                 */
                $routes->setExtensions(null);

                /**
                 * API version 1 routes
                 */
                $routes->prefix('v1', ['path' => '/v1'], static function (RouteBuilder $routes) use ($subdomain) {
                    $routes->setRouteClass(InflectedRoute::class);
                    $routes->connect('/:controller', ['prefix' => $subdomain]);
                    $routes->connect('/:controller/:action', ['prefix' => $subdomain]);
                    $routes->connect('/version', ['prefix' => $subdomain, 'controller' => 'Api', 'action' => 'version']);
                    $routes->connect('/hello', ['prefix' => $subdomain, 'controller' => 'Api', 'action' => 'hello']);
                    $routes->connect('/login', ['prefix' => $subdomain, 'controller' => 'Api', 'action' => 'login']);
                    $routes->connect('/token', ['prefix' => $subdomain, 'controller' => 'Api', 'action' => 'token']);
                });
            }
        });

        $routes->fallbacks(InflectedRoute::class);
    });
}

Router::prefix(Configure::read('Website.admin_prefix'), static function (RouteBuilder $routes) {
    $routes->setExtensions(['json']);

    $routes->connect('/:controller/:action/*', ['prefix' => 'panel', 'plugin' => null]);
    $routes->connect('/:controller/*', ['prefix' => 'panel', 'action' => 'index', 'plugin' => null]);
    $routes->connect('/reports/download/:id', ['prefix' => 'panel', 'controller' => 'Reports', 'action' => 'download'], ['pass' => ['id']]);
    $routes->redirect('/', ['prefix' => 'panel', 'controller' => 'Dashboard']);

    $routes->fallbacks(DashedRoute::class);
});

Router::scope('/', static function (RouteBuilder $routes) {

    $routes->connect('/', ['prefix' => 'frontend', 'controller' => 'Pages', 'action' => 'index']);
    $routes->connect('/*', ['prefix' => 'frontend', 'controller' => 'Pages', 'action' => 'view']);

    $routes->connect('/articles', ['prefix' => 'frontend', 'controller' => 'Articles', 'action' => 'index'], ['_name' => 'news-listing']);
    $routes->connect('/articles/*', ['prefix' => 'frontend', 'controller' => 'Articles', 'action' => 'view'], ['_name' => 'news-individual']);
    $routes->connect('/channels', ['prefix' => 'frontend', 'controller' => 'Channels', 'action' => 'index'], ['_name' => 'channels-listing']);
    $routes->connect('/channels/*', ['prefix' => 'frontend', 'controller' => 'Channels', 'action' => 'view'], ['_name' => 'channels-individual']);
    $routes->connect('/properties', ['prefix' => 'frontend', 'controller' => 'Properties', 'action' => 'index'], ['_name' => 'properties-listing']);
    $routes->connect('/properties-by/:id_:slug', ['prefix' => 'frontend', 'controller' => 'Properties', 'action' => 'index'], ['_name' => 'service-properties', 'id' => '[0-9]+', 'pass' => ['id', 'slug']]);
    $routes->connect('/properties/send-enquiry', ['prefix' => 'frontend', 'controller' => 'Properties', 'action' => 'sendEnquiry'], ['_name' => 'properties-enquiry']);
    $routes->connect('/properties/*', ['prefix' => 'frontend', 'controller' => 'Properties', 'action' => 'view'], ['_name' => 'properties-individual']);
    $routes->connect('/case-studies', ['prefix' => 'frontend', 'controller' => 'CaseStudies', 'action' => 'index'], ['_name' => 'case-studies-listing']);
    $routes->connect('/case-studies/*', ['prefix' => 'frontend', 'controller' => 'CaseStudies', 'action' => 'view'], ['_name' => 'case-studies-individual']);
    $routes->connect('/professional-services', ['prefix' => 'frontend', 'controller' => 'ProfessionalServices', 'action' => 'index'], ['_name' => 'professional-services-listing']);
    $routes->connect('/professional-services/send-enquiry', ['prefix' => 'frontend', 'controller' => 'ProfessionalServices', 'action' => 'sendEnquiry'], ['_name' => 'professional-services-enquiry']);
    $routes->connect('/professional-services/*', ['prefix' => 'frontend', 'controller' => 'ProfessionalServices', 'action' => 'view'], ['_name' => 'professional-services-individual']);
    $routes->connect('/search/save', ['prefix' => 'frontend', 'controller' => 'Search', 'action' => 'save'], ['_name' => 'save-search']);

    $routes->connect('/buddy-test', ['prefix' => 'frontend', 'controller' => 'BuddyController', 'action' => 'view']);

    $routes->setExtensions(['xml']);
    $routes->connect('/sitemap/:action', ['prefix' => 'frontend', 'controller' => 'Sitemap']);
    $routes->connect('/sitemap', ['prefix' => 'frontend', 'controller' => 'Sitemap', 'action' => 'index']);
    $routes->connect('/forms_data/save', ['prefix' => 'frontend', 'controller' => 'FormsData', 'action' => 'save']);
    $routes->connect('/professional-services/save-clicks', ['prefix' => 'frontend', 'controller' => 'ProfessionalServices', 'action' => 'save-clicks']);
    $routes->connect('/professional-services/save', ['prefix' => 'frontend', 'controller' => 'ProfessionalServices', 'action' => 'save']);
    $routes->connect('/properties/save', ['prefix' => 'frontend', 'controller' => 'Properties', 'action' => 'save']);

    $routes->setExtensions(['json']);
    $routes->connect('/property-attributes/get-by-type/*', ['prefix' => 'frontend', 'controller' => 'PropertyAttributes', 'action' => 'getByType'], ['_name' => 'property-attributes-list']);

    $routes->fallbacks('InflectedRoute');
});
