<?php

/**
 * @author eConnect4u
 * @date (2019-03-05)
 * @version 1.0
 */

use Migrations\AbstractSeed;

/**
 * Menus seed.
 */
class MenusSeed extends AbstractSeed
{
    /**
     * Run Method
     *
     * @return void
     */
    public function run()
    {
        $now = GlobalSeed::getCurrentDate();
        $data = [
            [
                'id' => '1',
                'position' => '1',
                'urlname' => 'main-menu',
                'name' => 'Main menu',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '2',
                'position' => '99',
                'urlname' => 'footer-menu',
                'name' => 'Footer menu',
                'created' => $now,
                'modified' => $now,
            ],
        ];
        GlobalSeed::truncateTable($this, 'menus', $data);
    }
}
