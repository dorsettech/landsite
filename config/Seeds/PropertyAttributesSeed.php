<?php

/**
 * @author eConnect4u
 * @date (2019-03-05)
 * @version 1.0
 */

use Migrations\AbstractSeed;

/**
 * PropertyAttributes seed.
 */
class PropertyAttributesSeed extends AbstractSeed
{
    /**
     * Run Method
     *
     * @return void
     */
    public function run()
    {
        $now = GlobalSeed::getCurrentDate();
        $data = [
            [
                'id' => '1',
                'type_id' => '1',
                'name' => 'Has Planning',
                'description' => null,
                'image' => '',
                'visibility' => '1',
                'position' => '0',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '2',
                'type_id' => '1',
                'name' => 'Land Vacant',
                'description' => null,
                'image' => '',
                'visibility' => '1',
                'position' => '0',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '3',
                'type_id' => '1',
                'name' => 'Property and land combined',
                'description' => null,
                'image' => '',
                'visibility' => '1',
                'position' => '0',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '4',
                'type_id' => '1',
                'name' => 'Mixed use',
                'description' => 'Residential and Commercial',
                'image' => '',
                'visibility' => '1',
                'position' => '0',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '5',
                'type_id' => '3',
                'name' => 'Retail',
                'description' => null,
                'image' => '',
                'visibility' => '1',
                'position' => '0',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '6',
                'type_id' => '3',
                'name' => 'Industrial',
                'description' => null,
                'image' => '',
                'visibility' => '1',
                'position' => '0',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '7',
                'type_id' => '3',
                'name' => 'Warehousing',
                'description' => null,
                'image' => '',
                'visibility' => '1',
                'position' => '0',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '8',
                'type_id' => '3',
                'name' => 'Leisure and Licensed',
                'description' => null,
                'image' => '',
                'visibility' => '1',
                'position' => '0',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '9',
                'type_id' => '2',
                'name' => 'Filter 1',
                'description' => null,
                'image' => '',
                'visibility' => '1',
                'position' => '0',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '10',
                'type_id' => '2',
                'name' => 'Filter 2',
                'description' => null,
                'image' => '',
                'visibility' => '1',
                'position' => '0',
                'created' => $now,
                'modified' => $now,
            ],
        ];
        GlobalSeed::truncateTable($this, 'property_attributes', $data);
    }
}
