<?php

use Migrations\AbstractSeed;

/**
 * Areas seed.
 */
class AreasSeed extends AbstractSeed
{
    /**
     * Run Method
     *
     * @return void
     */
    public function run()
    {
        $now = GlobalSeed::getCurrentDate();
        $data = [
            [
                'id' => '1',
                'name' => 'Bournemouth',
                'created' => $now,
            ],
            [
                'id' => '2',
                'name' => 'Poole',
                'created' => $now,
            ],
            [
                'id' => '3',
                'name' => 'Dorset',
                'created' => $now,
            ],
        ];
        GlobalSeed::truncateTable($this, 'areas', $data);
    }
}
