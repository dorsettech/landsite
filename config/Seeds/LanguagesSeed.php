<?php

/**
 * @author eConnect4u
 * @date (2019-03-05)
 * @version 1.0
 */

use Migrations\AbstractSeed;

/**
 * Languages seed.
 */
class LanguagesSeed extends AbstractSeed
{
    /**
     * Run Method
     *
     * @return void
     */
    public function run()
    {
        $now = GlobalSeed::getCurrentDate();
        $data = [
            [
                'id' => '1',
                'position' => '1',
                'name' => 'English',
                'locale' => 'en',
                'is_default' => '1',
                'active' => '1',
                'deleted' => '0',
                'created' => $now,
                'modified' => $now,
            ],
        ];
        GlobalSeed::truncateTable($this, 'languages', $data);
    }
}
