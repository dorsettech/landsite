<?php

/**
 * @author eConnect4u
 * @date (2019-03-05)
 * @version 1.0
 */

use Migrations\AbstractSeed;

/**
 * Contents seed.
 */
class ContentsSeed extends AbstractSeed
{
    /**
     * Run Method
     *
     * @return void
     */
    public function run()
    {
        $now = GlobalSeed::getCurrentDate();
        $data = [
            [
                'id' => '1',
                'page_id' => '1',
                'position' => '999',
                'content_name' => 'main',
                'file' => null,
                'file2' => null,
                'file3' => null,
                'file4' => null,
                'file5' => null,
                'gallery' => null,
                'title' => null,
                'title2' => null,
                'title3' => null,
                'title4' => null,
                'title5' => null,
                'content' => '<p>Lorem ipsum dolor sit amet</p>
',
                'content2' => null,
                'content3' => null,
                'content4' => null,
                'content5' => null,
                'redirect_url' => null,
                'redirect_url2' => null,
                'redirect_url3' => null,
                'redirect_url4' => null,
                'redirect_url5' => null,
                'visible' => '1',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '2',
                'page_id' => '2',
                'position' => '999',
                'content_name' => 'main',
                'file' => null,
                'file2' => null,
                'file3' => null,
                'file4' => null,
                'file5' => null,
                'gallery' => null,
                'title' => null,
                'title2' => null,
                'title3' => null,
                'title4' => null,
                'title5' => null,
                'content' => '<h1>Lorem ipsum dolor sit amet</h1>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac sagittis sapien. Fusce vehicula dolor fringilla massa maximus consectetur. Cras porta ante eu auctor laoreet. Praesent lobortis lectus molestie justo fermentum condimentum. Integer ornare nulla ac eleifend eleifend. Curabitur tempor at turpis vel accumsan. Vivamus at suscipit ex. Mauris mollis mi non leo porta vestibulum. Nullam nunc augue, laoreet ut magna non, luctus pretium est. Proin magna magna, dignissim at tempus eget, volutpat fermentum purus.</p>

<p><img alt="title" class="img-fluid" src="img/image1.jpg" style="padding-right: 10px; width: 50%;" /><img alt="title" class="img-fluid" src="img/image1.jpg" style="padding-left: 10px; width: 50%;" /></p>

<p>Morbi non elit mattis, lacinia odio quis, fermentum neque. Maecenas venenatis, libero nec eleifend tristique, nisi nisl sollicitudin erat, eleifend volutpat purus lacus non diam. Etiam commodo dolor a magna bibendum accumsan. Vestibulum fringilla euismod purus a ultricies.<br />
<br />
Ut ac suscipit turpis, sed faucibus dui. Proin tristique placerat consequat. In hac habitasse platea dictumst. Nullam at odio at nunc pulvinar condimentum sit amet ac enim. Duis lobortis, velit quis efficitur finibus, enim ligula mollis elit, quis tempor lacus arcu ac libero. In nec sapien pretium diam tincidunt faucibus. Sed mi lectus, pretium ac cursus ac, vulputate ac sapien. Sed at semper tellus. Phasellus tristique bibendum leo vel suscipit. Praesent varius, turpis at suscipit porta, turpis lacus volutpat nibh, eu malesuada magna ipsum vitae massa. Curabitur lacus neque, cursus ut laoreet ut, vestibulum nec ipsum. Aliquam gravida tortor a neque consectetur egestas. Duis pellentesque sagittis eros eget vulputate. Aliquam non tristique dolor, sed posuere risus. Duis ligula elit, aliquam vel tortor eget, finibus mollis sem.</p>
',
                'content2' => null,
                'content3' => null,
                'content4' => null,
                'content5' => null,
                'redirect_url' => null,
                'redirect_url2' => null,
                'redirect_url3' => null,
                'redirect_url4' => null,
                'redirect_url5' => null,
                'visible' => '1',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '3',
                'page_id' => '1',
                'position' => '999',
                'content_name' => 'example',
                'file' => null,
                'file2' => null,
                'file3' => null,
                'file4' => null,
                'file5' => null,
                'gallery' => null,
                'title' => '',
                'title2' => '',
                'title3' => '',
                'title4' => '',
                'title5' => '',
                'content' => '',
                'content2' => '',
                'content3' => '',
                'content4' => '',
                'content5' => '',
                'redirect_url' => '',
                'redirect_url2' => null,
                'redirect_url3' => null,
                'redirect_url4' => null,
                'redirect_url5' => null,
                'visible' => '1',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '4',
                'page_id' => '1',
                'position' => '999',
                'content_name' => 'slider',
                'file' => 'files/contents/image1.jpg',
                'file2' => null,
                'file3' => null,
                'file4' => null,
                'file5' => null,
                'gallery' => null,
                'title' => 'The world of UK Property is now at your fingertips.',
                'title2' => 'What do you want to do today:',
                'title3' => null,
                'title4' => null,
                'title5' => null,
                'content' => null,
                'content2' => null,
                'content3' => null,
                'content4' => null,
                'content5' => null,
                'redirect_url' => null,
                'redirect_url2' => null,
                'redirect_url3' => null,
                'redirect_url4' => null,
                'redirect_url5' => null,
                'visible' => '1',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '5',
                'page_id' => '1',
                'position' => '999',
                'content_name' => 'slider_boxes',
                'file' => null,
                'file2' => null,
                'file3' => null,
                'file4' => null,
                'file5' => null,
                'gallery' => null,
                'title' => null,
                'title2' => null,
                'title3' => null,
                'title4' => null,
                'title5' => null,
                'content' => '<div class="content">
<div class="heading">Let or Rent??</div>

<div class="image"><img alt="" src="img/slidericon1.png" /></div>
</div>

<div class="hover">
<div class="heading">Let or Rent?</div>

<div class="dsc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam laoreet, felis at iaculis varius, ipsum nisi pulvinar felis, a ullamcorper felis turpis sit amet risus. Donec at euismod sem. Aliquam iaculis orci ut risus facilisis luctus. Vestibulum cursus ornare libero et consequat.</div>
<a class="btn btn-primary" href="#">Read more <i class="fas fa-arrow-right">&nbsp;</i></a></div>
',
                'content2' => '<div class="content">
<div class="heading">Offer a service?</div>

<div class="image"><img alt="" src="img/slidericon1.png" /></div>
</div>

<div class="hover">
<div class="heading">Offer a service?</div>

<div class="dsc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam laoreet, felis at iaculis varius, ipsum nisi pulvinar felis, a ullamcorper felis turpis sit amet risus. Donec at euismod sem. Aliquam iaculis orci ut risus facilisis luctus. Vestibulum cursus ornare libero et consequat.</div>
<a class="btn btn-primary" href="#">Read more <i class="fas fa-arrow-right">&nbsp;</i></a></div>
',
                'content3' => '<div class="content">
<div class="heading">Learn?</div>

<div class="image"><img alt="" src="img/slidericon1.png" /></div>
</div>

<div class="hover">
<div class="heading">Learn?</div>

<div class="dsc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam laoreet, felis at iaculis varius, ipsum nisi pulvinar felis, a ullamcorper felis turpis sit amet risus. Donec at euismod sem. Aliquam iaculis orci ut risus facilisis luctus. Vestibulum cursus ornare libero et consequat.</div>
<a class="btn btn-primary" href="#">Read more </a></div>
',
                'content4' => '<div class="content">
<div class="heading">Learn?</div>

<div class="image"><img alt="" src="img/slidericon1.png" /></div>
</div>

<div class="hover">
<div class="heading">Learn?</div>

<div class="dsc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam laoreet, felis at iaculis varius, ipsum nisi pulvinar felis, a ullamcorper felis turpis sit amet risus. Donec at euismod sem. Aliquam iaculis orci ut risus facilisis luctus. Vestibulum cursus ornare libero et consequat.</div>
<a class="btn btn-primary" href="#">Read more </a></div>
',
                'content5' => null,
                'redirect_url' => null,
                'redirect_url2' => null,
                'redirect_url3' => null,
                'redirect_url4' => null,
                'redirect_url5' => null,
                'visible' => '1',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '6',
                'page_id' => '1',
                'position' => '999',
                'content_name' => 'buy_tab',
                'file' => 'files/contents/image1_1.jpg',
                'file2' => null,
                'file3' => null,
                'file4' => null,
                'file5' => null,
                'gallery' => null,
                'title' => 'Register as a buyer',
                'title2' => null,
                'title3' => null,
                'title4' => null,
                'title5' => null,
                'content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam laoreet, felis at iaculis varius, ipsum nisi pulvinar felis, a ullamcorper felis turpis sit amet risus. Donec at euismod sem. Aliquam iaculis orci ut risus facilisis luctus. Vestibulum cursus ornare libero et consequat. Ut eleifend nisi in mi bibendum lacinia. Nulla convallis, felis nec accumsan venenatis, turpis felis molestie sapien, quis fringilla dolor ipsum a risus. Quisque massa ante, finibus maximus urna a, consequat hendrerit lacus. Nam facilisis a purus id varius. Sed quis interdum massa.</p>
',
                'content2' => null,
                'content3' => null,
                'content4' => null,
                'content5' => null,
                'redirect_url' => null,
                'redirect_url2' => null,
                'redirect_url3' => null,
                'redirect_url4' => null,
                'redirect_url5' => null,
                'visible' => '1',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '7',
                'page_id' => '1',
                'position' => '999',
                'content_name' => 'let_tab',
                'file' => null,
                'file2' => null,
                'file3' => null,
                'file4' => null,
                'file5' => null,
                'gallery' => null,
                'title' => null,
                'title2' => null,
                'title3' => null,
                'title4' => null,
                'title5' => null,
                'content' => '<div class="col-4">
<div class="collapser-left">
<div data-target="#collapse1" data-toggle="collapse" type="button">Lorem ipsum dolor sit amet 1</div>

<div class="collapsed" data-target="#collapse2" data-toggle="collapse" type="button">Lorem ipsum dolor sit amet 2</div>

<div class="collapsed" data-target="#collapse3" data-toggle="collapse" type="button">Lorem ipsum dolor sit amet 3</div>

<div class="extender">&nbsp;</div>
</div>
</div>

<div class="col-12 col-md-8 collapser-right">
<div>
<div data-target="#collapse1" data-toggle="collapse" type="button">Lorem ipsum dolor sit amet 1</div>

<div class="collapse show" data-parent="#tab_collapses2" id="collapse1">
<div class="content">
<div class="heading">Nam facilisis a purus id varius</div>

<p>1 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam laoreet, felis at iaculis varius, ipsum nisi pulvinar felis, a ullamcorper felis turpis sit amet risus. Donec at euismod sem. Aliquam iaculis orci ut risus facilisis luctus. Vestibulum cursus ornare libero et consequat. Ut eleifend nisi in mi bibendum lacinia. Nulla convallis, felis nec accumsan venenatis, turpis felis molestie sapien, quis fringilla dolor ipsum a risus. Quisque massa ante, finibus maximus urna a, consequat hendrerit lacus. Nam facilisis a purus id varius. Sed quis interdum massa. Duis pharetra fermentum lectus a pulvinar. Nulla non lacus ultricies, suscipit quam eu, euismod nisi. Nam odio lectus, eleifend sit amet purus eget, dignissim porta purus. Mauris a quam ac erat rhoncus sodales sed eu urna.</p>
</div>
</div>
</div>

<div>
<div class="collapsed" data-target="#collapse2" data-toggle="collapse" type="button">Lorem ipsum dolor sit amet 2</div>

<div class="collapse" data-parent="#tab_collapses2" id="collapse2">
<div class="content">
<div class="heading">Nam facilisis a purus id varius</div>

<p>2 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam laoreet, felis at iaculis varius, ipsum nisi pulvinar felis, a ullamcorper felis turpis sit amet risus. Donec at euismod sem. Aliquam iaculis orci ut risus facilisis luctus. Vestibulum cursus ornare libero et consequat. Ut eleifend nisi in mi bibendum lacinia. Nulla convallis, felis nec accumsan venenatis, turpis felis molestie sapien, quis fringilla dolor ipsum a risus. Quisque massa ante, finibus maximus urna a, consequat hendrerit lacus. Nam facilisis a purus id varius. Sed quis interdum massa. Duis pharetra fermentum lectus a pulvinar. Nulla non lacus ultricies, suscipit quam eu, euismod nisi. Nam odio lectus, eleifend sit amet purus eget, dignissim porta purus. Mauris a quam ac erat rhoncus sodales sed eu urna.</p>
</div>
</div>
</div>

<div>
<div class="collapsed" data-target="#collapse3" data-toggle="collapse" type="button">Lorem ipsum dolor sit amet 3</div>

<div class="collapse" data-parent="#tab_collapses2" id="collapse3">
<div class="content">
<div class="heading">Nam facilisis a purus id varius</div>

<p>3 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam laoreet, felis at iaculis varius, ipsum nisi pulvinar felis, a ullamcorper felis turpis sit amet risus. Donec at euismod sem. Aliquam iaculis orci ut risus facilisis luctus. Vestibulum cursus ornare libero et consequat. Ut eleifend nisi in mi bibendum lacinia. Nulla convallis, felis nec accumsan venenatis, turpis felis molestie sapien, quis fringilla dolor ipsum a risus. Quisque massa ante, finibus maximus urna a, consequat hendrerit lacus. Nam facilisis a purus id varius. Sed quis interdum massa. Duis pharetra fermentum lectus a pulvinar. Nulla non lacus ultricies, suscipit quam eu, euismod nisi. Nam odio lectus, eleifend sit amet purus eget, dignissim porta purus. Mauris a quam ac erat rhoncus sodales sed eu urna.</p>
</div>
</div>
</div>
</div>
',
                'content2' => null,
                'content3' => null,
                'content4' => null,
                'content5' => null,
                'redirect_url' => null,
                'redirect_url2' => null,
                'redirect_url3' => null,
                'redirect_url4' => null,
                'redirect_url5' => null,
                'visible' => '1',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '8',
                'page_id' => '1',
                'position' => '999',
                'content_name' => 'rent_tab',
                'file' => 'files/contents/image1_2.jpg',
                'file2' => null,
                'file3' => null,
                'file4' => null,
                'file5' => null,
                'gallery' => null,
                'title' => 'Register as a Commercial Renter',
                'title2' => null,
                'title3' => null,
                'title4' => null,
                'title5' => null,
                'content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam laoreet, felis at iaculis varius, ipsum nisi pulvinar felis, a ullamcorper felis turpis sit amet risus. Donec at euismod sem. Aliquam iaculis orci ut risus facilisis luctus. Vestibulum cursus ornare libero et consequat. Ut eleifend nisi in mi bibendum lacinia. Nulla convallis, felis nec accumsan venenatis, turpis felis molestie sapien, quis fringilla dolor ipsum a risus. Quisque massa ante, finibus maximus urna a, consequat hendrerit lacus. Nam facilisis a purus id varius. Sed quis interdum massa.</p>
',
                'content2' => null,
                'content3' => null,
                'content4' => null,
                'content5' => null,
                'redirect_url' => null,
                'redirect_url2' => null,
                'redirect_url3' => null,
                'redirect_url4' => null,
                'redirect_url5' => null,
                'visible' => '1',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '9',
                'page_id' => '1',
                'position' => '999',
                'content_name' => 'sell_tab',
                'file' => null,
                'file2' => null,
                'file3' => null,
                'file4' => null,
                'file5' => null,
                'gallery' => null,
                'title' => null,
                'title2' => null,
                'title3' => null,
                'title4' => null,
                'title5' => null,
                'content' => '<div class="col-4">
<div class="collapser-left">
<div data-target="#collapse1" data-toggle="collapse" type="button">Lorem ipsum dolor sit amet 1</div>

<div class="collapsed" data-target="#collapse2" data-toggle="collapse" type="button">Lorem ipsum dolor sit amet 2</div>

<div class="collapsed" data-target="#collapse3" data-toggle="collapse" type="button">Lorem ipsum dolor sit amet 3</div>

<div class="extender">&nbsp;</div>
</div>
</div>

<div class="col-12 col-md-8 collapser-right">
<div>
<div data-target="#collapse1" data-toggle="collapse" type="button">Lorem ipsum dolor sit amet 1</div>

<div class="collapse show" data-parent="#tab_collapses1" id="collapse1">
<div class="content">
<div class="heading">Nam facilisis a purus id varius</div>

<p>1 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam laoreet, felis at iaculis varius, ipsum nisi pulvinar felis, a ullamcorper felis turpis sit amet risus. Donec at euismod sem. Aliquam iaculis orci ut risus facilisis luctus. Vestibulum cursus ornare libero et consequat. Ut eleifend nisi in mi bibendum lacinia. Nulla convallis, felis nec accumsan venenatis, turpis felis molestie sapien, quis fringilla dolor ipsum a risus. Quisque massa ante, finibus maximus urna a, consequat hendrerit lacus. Nam facilisis a purus id varius. Sed quis interdum massa. Duis pharetra fermentum lectus a pulvinar. Nulla non lacus ultricies, suscipit quam eu, euismod nisi. Nam odio lectus, eleifend sit amet purus eget, dignissim porta purus. Mauris a quam ac erat rhoncus sodales sed eu urna.</p>
</div>
</div>
</div>

<div>
<div class="collapsed" data-target="#collapse2" data-toggle="collapse" type="button">Lorem ipsum dolor sit amet 2</div>

<div class="collapse" data-parent="#tab_collapses1" id="collapse2">
<div class="content">
<div class="heading">Nam facilisis a purus id varius</div>

<p>2 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam laoreet, felis at iaculis varius, ipsum nisi pulvinar felis, a ullamcorper felis turpis sit amet risus. Donec at euismod sem. Aliquam iaculis orci ut risus facilisis luctus. Vestibulum cursus ornare libero et consequat. Ut eleifend nisi in mi bibendum lacinia. Nulla convallis, felis nec accumsan venenatis, turpis felis molestie sapien, quis fringilla dolor ipsum a risus. Quisque massa ante, finibus maximus urna a, consequat hendrerit lacus. Nam facilisis a purus id varius. Sed quis interdum massa. Duis pharetra fermentum lectus a pulvinar. Nulla non lacus ultricies, suscipit quam eu, euismod nisi. Nam odio lectus, eleifend sit amet purus eget, dignissim porta purus. Mauris a quam ac erat rhoncus sodales sed eu urna.</p>
</div>
</div>
</div>

<div>
<div class="collapsed" data-target="#collapse3" data-toggle="collapse" type="button">Lorem ipsum dolor sit amet 3</div>

<div class="collapse" data-parent="#tab_collapses1" id="collapse3">
<div class="content">
<div class="heading">Nam facilisis a purus id varius</div>

<p>3 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam laoreet, felis at iaculis varius, ipsum nisi pulvinar felis, a ullamcorper felis turpis sit amet risus. Donec at euismod sem. Aliquam iaculis orci ut risus facilisis luctus. Vestibulum cursus ornare libero et consequat. Ut eleifend nisi in mi bibendum lacinia. Nulla convallis, felis nec accumsan venenatis, turpis felis molestie sapien, quis fringilla dolor ipsum a risus. Quisque massa ante, finibus maximus urna a, consequat hendrerit lacus. Nam facilisis a purus id varius. Sed quis interdum massa. Duis pharetra fermentum lectus a pulvinar. Nulla non lacus ultricies, suscipit quam eu, euismod nisi. Nam odio lectus, eleifend sit amet purus eget, dignissim porta purus. Mauris a quam ac erat rhoncus sodales sed eu urna.</p>
</div>
</div>
</div>
</div>
',
                'content2' => null,
                'content3' => null,
                'content4' => null,
                'content5' => null,
                'redirect_url' => null,
                'redirect_url2' => null,
                'redirect_url3' => null,
                'redirect_url4' => null,
                'redirect_url5' => null,
                'visible' => '1',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '10',
                'page_id' => '1',
                'position' => '999',
                'content_name' => 'register_tab',
                'file' => 'files/contents/image1_3.jpg',
                'file2' => null,
                'file3' => null,
                'file4' => null,
                'file5' => null,
                'gallery' => null,
                'title' => 'Register as a Professional',
                'title2' => null,
                'title3' => null,
                'title4' => null,
                'title5' => null,
                'content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam laoreet, felis at iaculis varius, ipsum nisi pulvinar felis, a ullamcorper felis turpis sit amet risus. Donec at euismod sem. Aliquam iaculis orci ut risus facilisis luctus. Vestibulum cursus ornare libero et consequat. Ut eleifend nisi in mi bibendum lacinia. Nulla convallis, felis nec accumsan venenatis, turpis felis molestie sapien, quis fringilla dolor ipsum a risus. Quisque massa ante, finibus maximus urna a, consequat hendrerit lacus. Nam facilisis a purus id varius. Sed quis interdum massa.</p>
',
                'content2' => null,
                'content3' => null,
                'content4' => null,
                'content5' => null,
                'redirect_url' => null,
                'redirect_url2' => null,
                'redirect_url3' => null,
                'redirect_url4' => null,
                'redirect_url5' => null,
                'visible' => '1',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '11',
                'page_id' => '1',
                'position' => '999',
                'content_name' => 'search_tab',
                'file' => null,
                'file2' => null,
                'file3' => null,
                'file4' => null,
                'file5' => null,
                'gallery' => null,
                'title' => 'Search for a Professional',
                'title2' => null,
                'title3' => null,
                'title4' => null,
                'title5' => null,
                'content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam laoreet, felis at iaculis varius, ipsum nisi pulvinar felis, a ullamcorper felis turpis sit amet risus. Donec at euismod sem. Aliquam iaculis orci ut risus facilisis luctus. Vestibulum cursus ornare libero et consequat. Ut eleifend nisi in mi bibendum lacinia. Nulla convallis, felis nec accumsan venenatis, turpis felis molestie sapien, quis fringilla dolor ipsum a risus.</p>
',
                'content2' => null,
                'content3' => null,
                'content4' => null,
                'content5' => null,
                'redirect_url' => null,
                'redirect_url2' => null,
                'redirect_url3' => null,
                'redirect_url4' => null,
                'redirect_url5' => null,
                'visible' => '1',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '12',
                'page_id' => '5',
                'position' => '999',
                'content_name' => 'main',
                'file' => null,
                'file2' => null,
                'file3' => null,
                'file4' => null,
                'file5' => null,
                'gallery' => null,
                'title' => null,
                'title2' => null,
                'title3' => null,
                'title4' => null,
                'title5' => null,
                'content' => '<h1>Lorem ipsum dolor sit amet</h1>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac sagittis sapien. Fusce vehicula dolor fringilla massa maximus consectetur. Cras porta ante eu auctor laoreet. Praesent lobortis lectus molestie justo fermentum condimentum. Integer ornare nulla ac eleifend eleifend. Curabitur tempor at turpis vel accumsan. Vivamus at suscipit ex. Mauris mollis mi non leo porta vestibulum. Nullam nunc augue, laoreet ut magna non, luctus pretium est. Proin magna magna, dignissim at tempus eget, volutpat fermentum purus.</p>

<p><img alt="title" class="img-fluid" src="img/image1.jpg" style="padding-right: 10px; width: 50%;" /><img alt="title" class="img-fluid" src="img/image1.jpg" style="padding-left: 10px; width: 50%;" /></p>

<p>Morbi non elit mattis, lacinia odio quis, fermentum neque. Maecenas venenatis, libero nec eleifend tristique, nisi nisl sollicitudin erat, eleifend volutpat purus lacus non diam. Etiam commodo dolor a magna bibendum accumsan. Vestibulum fringilla euismod purus a ultricies.<br />
<br />
Ut ac suscipit turpis, sed faucibus dui. Proin tristique placerat consequat. In hac habitasse platea dictumst. Nullam at odio at nunc pulvinar condimentum sit amet ac enim. Duis lobortis, velit quis efficitur finibus, enim ligula mollis elit, quis tempor lacus arcu ac libero. In nec sapien pretium diam tincidunt faucibus. Sed mi lectus, pretium ac cursus ac, vulputate ac sapien. Sed at semper tellus. Phasellus tristique bibendum leo vel suscipit. Praesent varius, turpis at suscipit porta, turpis lacus volutpat nibh, eu malesuada magna ipsum vitae massa. Curabitur lacus neque, cursus ut laoreet ut, vestibulum nec ipsum. Aliquam gravida tortor a neque consectetur egestas. Duis pellentesque sagittis eros eget vulputate. Aliquam non tristique dolor, sed posuere risus. Duis ligula elit, aliquam vel tortor eget, finibus mollis sem.</p>
',
                'content2' => null,
                'content3' => null,
                'content4' => null,
                'content5' => null,
                'redirect_url' => null,
                'redirect_url2' => null,
                'redirect_url3' => null,
                'redirect_url4' => null,
                'redirect_url5' => null,
                'visible' => '1',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '13',
                'page_id' => '5',
                'position' => '999',
                'content_name' => 'sidebar',
                'file' => null,
                'file2' => null,
                'file3' => null,
                'file4' => null,
                'file5' => null,
                'gallery' => null,
                'title' => null,
                'title2' => null,
                'title3' => null,
                'title4' => null,
                'title5' => null,
                'content' => '<div class="box">
<div class="heading">Related Services Categories</div>

<ul class="list-unstyled columns">
<li>Lorem ipsum 1</li>
<li>Lorem ipsum dolor sit amet 2</li>
<li>Lorem ipsum 3</li>
<li>Lorem ipsum 4</li>
<li>Lorem ipsum dolor sit amet 5</li>
<li>Lorem ipsum 6</li>
</ul>
</div>

<hr />
<p><a class="btn btn-secondary" href="#">Search for Properties</a></p>

<hr />',
                'content2' => null,
                'content3' => null,
                'content4' => null,
                'content5' => null,
                'redirect_url' => null,
                'redirect_url2' => null,
                'redirect_url3' => null,
                'redirect_url4' => null,
                'redirect_url5' => null,
                'visible' => '1',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '14',
                'page_id' => '3',
                'position' => '999',
                'content_name' => 'address_details',
                'file' => null,
                'file2' => null,
                'file3' => null,
                'file4' => null,
                'file5' => null,
                'gallery' => null,
                'title' => null,
                'title2' => null,
                'title3' => null,
                'title4' => null,
                'title5' => null,
                'content' => '<div><a href="tel:01202684400"><i class="fas fa-phone">&nbsp;</i><br />
Phone: 01202 684400 </a></div>

<div><a href="mailto:info@thelandsite.com"><i class="far fa-envelope">&nbsp;</i><br />
Email: info@thelandsite.com </a></div>
',
                'content2' => null,
                'content3' => null,
                'content4' => null,
                'content5' => null,
                'redirect_url' => null,
                'redirect_url2' => null,
                'redirect_url3' => null,
                'redirect_url4' => null,
                'redirect_url5' => null,
                'visible' => '1',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '15',
                'page_id' => '3',
                'position' => '999',
                'content_name' => 'banner',
                'file' => 'files/contents/image1_4.jpg',
                'file2' => null,
                'file3' => null,
                'file4' => null,
                'file5' => null,
                'gallery' => null,
                'title' => null,
                'title2' => null,
                'title3' => null,
                'title4' => null,
                'title5' => null,
                'content' => '<div class="heading">Contact us</div>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac sagittis sapien. Fusce vehicula dolor fringilla massa maximus consectetur. Cras porta ante eu auctor laoreet. Praesent lobortis lectus molestie justo fermentum condimentum.</p>
',
                'content2' => null,
                'content3' => null,
                'content4' => null,
                'content5' => null,
                'redirect_url' => null,
                'redirect_url2' => null,
                'redirect_url3' => null,
                'redirect_url4' => null,
                'redirect_url5' => null,
                'visible' => '1',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '16',
                'page_id' => '2',
                'position' => '999',
                'content_name' => 'sidebar',
                'file' => null,
                'file2' => null,
                'file3' => null,
                'file4' => null,
                'file5' => null,
                'gallery' => null,
                'title' => null,
                'title2' => null,
                'title3' => null,
                'title4' => null,
                'title5' => null,
                'content' => '<div class="box">
<div class="heading">Related Services Categories</div>

<ul class="list-unstyled columns">
<li>Lorem ipsum 1</li>
<li>Lorem ipsum dolor sit amet 2</li>
<li>Lorem ipsum 3</li>
<li>Lorem ipsum 4</li>
<li>Lorem ipsum dolor sit amet 5</li>
<li>Lorem ipsum 6</li>
</ul>
</div>

<hr />
<p><a class="btn btn-secondary" href="#">Search for Properties</a></p>

<hr />',
                'content2' => null,
                'content3' => null,
                'content4' => null,
                'content5' => null,
                'redirect_url' => null,
                'redirect_url2' => null,
                'redirect_url3' => null,
                'redirect_url4' => null,
                'redirect_url5' => null,
                'visible' => '1',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '17',
                'page_id' => '7',
                'position' => '999',
                'content_name' => 'main',
                'file' => null,
                'file2' => null,
                'file3' => null,
                'file4' => null,
                'file5' => null,
                'gallery' => null,
                'title' => null,
                'title2' => null,
                'title3' => null,
                'title4' => null,
                'title5' => null,
                'content' => '<div class="heading">Insights</div>

<p>Want to contribute? If you have any useful information about this industry please contact us and we&rsquo;ll publish it</p>
',
                'content2' => null,
                'content3' => null,
                'content4' => null,
                'content5' => null,
                'redirect_url' => null,
                'redirect_url2' => null,
                'redirect_url3' => null,
                'redirect_url4' => null,
                'redirect_url5' => null,
                'visible' => '1',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '18',
                'page_id' => '9',
                'position' => '999',
                'content_name' => 'main',
                'file' => null,
                'file2' => null,
                'file3' => null,
                'file4' => null,
                'file5' => null,
                'gallery' => null,
                'title' => null,
                'title2' => null,
                'title3' => null,
                'title4' => null,
                'title5' => null,
                'content' => '<div class="heading">Case studies</div>

<p>Want to contribute? If you have any useful information about this industry please contact us and we&rsquo;ll publish it</p>
',
                'content2' => null,
                'content3' => null,
                'content4' => null,
                'content5' => null,
                'redirect_url' => null,
                'redirect_url2' => null,
                'redirect_url3' => null,
                'redirect_url4' => null,
                'redirect_url5' => null,
                'visible' => '1',
                'created' => $now,
                'modified' => $now,
            ],
        ];
        GlobalSeed::truncateTable($this, 'contents', $data);
    }
}
