<?php

/**
 * @author eConnect4u
 * @date (2019-03-06)
 * @version 1.0
 */

use Migrations\AbstractSeed;

/**
 * Articles seed.
 */
class ArticlesSeed extends AbstractSeed
{
    /**
     * Run Method
     *
     * @return void
     */
    public function run()
    {
        $now = GlobalSeed::getCurrentDate();
        $data = [
            [
                'id' => '1',
                'author_id' => '8',
                'posted_by' => '6',
                'category_id' => '6',
                'type' => 'INSIGHT',
                'title' => 'Lorem ipsum dolor sit amet',
                'slug' => 'lorem-ipsum-dolor-sit-amet',
                'headline' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam consectetur efficitur augue ut luctus. Integer quis dui eget arcu sagittis sagittis et at urna. Duis vel nunc a nisl scelerisque consequat ut id dolor.',
                'body' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam consectetur efficitur augue ut luctus. Integer quis dui eget arcu sagittis sagittis et at urna. Duis vel nunc a nisl scelerisque consequat ut id dolor. Aliquam eu velit vehicula, accumsan justo ac, pharetra dui. Aliquam at arcu sagittis, congue lorem a, vehicula justo. Ut vehicula magna in massa dapibus aliquam. Pellentesque in dui sagittis, molestie mauris sit amet, aliquam lectus.

Phasellus luctus metus dictum sapien hendrerit iaculis. Nam pretium condimentum erat eget tristique. Quisque dictum tempus feugiat. Ut sit amet dolor enim. Suspendisse in neque eu neque aliquam vestibulum in pharetra sem. Etiam volutpat eros a orci pharetra, nec convallis tortor placerat. Aliquam mattis volutpat odio vel vulputate. Aenean purus tortor, aliquam vel bibendum in, varius a tellus. Sed nibh mi, consequat eget purus et, vestibulum blandit lorem. Aenean quis sapien in nisi dapibus euismod. Integer cursus imperdiet sem a pharetra. Phasellus mollis scelerisque auctor. Curabitur sed magna maximus, rutrum mi eu, condimentum nisl.

Pellentesque at metus quis justo aliquam tristique. Nullam eget tincidunt purus, in dictum tortor. Integer quis mi in purus vestibulum congue nec nec ipsum. Mauris porta pharetra massa. In porta, ligula ac suscipit auctor, nulla libero porta ex, ut finibus lorem eros eget velit. Aenean congue est ac neque euismod lacinia eu ut odio. Quisque tincidunt tortor at sapien efficitur luctus. Quisque mattis facilisis nulla, sit amet blandit nunc porta ac. Phasellus feugiat, nibh in cursus malesuada, arcu sapien mollis dui, at pharetra odio massa a sem.',
                'image' => 'img/image1.jpg',
                'status' => 'PUBLISHED',
                'state' => 'APPROVED',
                'rejection_reason' => null,
                'publish_date' => '2019-02-21 00:00:00',
                'visibility' => '1',
                'meta_title' => null,
                'meta_description' => null,
                'meta_keywords' => null,
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '2',
                'author_id' => '8',
                'posted_by' => '6',
                'category_id' => '9',
                'type' => 'INSIGHT',
                'title' => 'In et felis sagittis',
                'slug' => 'in-et-felis-sagittis',
                'headline' => 'Mauris molestie ornare libero at malesuada. Integer feugiat velit posuere diam accumsan congue. Integer at enim sed urna malesuada posuere.',
                'body' => 'Proin eu augue gravida, malesuada tellus quis, lobortis sapien. Suspendisse ipsum est, pharetra a orci sed, ultricies ornare eros. Aenean finibus dui id leo mattis, et euismod libero convallis. Nullam et velit varius, accumsan velit eget, feugiat ligula. Praesent magna nunc, pellentesque ac nisl id, pulvinar finibus eros. Curabitur lacinia non sapien a tempus. Curabitur elementum rutrum ullamcorper. Vestibulum ut purus blandit, laoreet magna eget, elementum metus. Phasellus laoreet lacus non enim convallis scelerisque. Nam hendrerit nulla orci, in egestas sem hendrerit sit amet.

Nulla ut libero sed justo dignissim sagittis. In quis nulla dictum, viverra nibh et, lacinia mi. Integer vulputate quam ac tincidunt placerat. Morbi egestas orci vitae massa tincidunt, in hendrerit erat imperdiet. Cras lobortis ante nec velit tincidunt consectetur sit amet ullamcorper nibh. In maximus, eros vel interdum laoreet, enim odio porta nibh, non mollis mi justo ut risus. Suspendisse luctus sollicitudin neque consequat tincidunt. Mauris finibus, nisi sit amet cursus rhoncus, nisi tellus sodales sem, nec tempus ipsum est vel enim. Integer id quam metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce rhoncus at lacus aliquam tempus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc dui arcu, elementum sed elit at, sollicitudin tempus enim. Aenean turpis mi, gravida a mattis at, posuere eget ipsum.

In vulputate neque nisl, eu viverra eros elementum eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras bibendum quis mi eu finibus. Nam quis sapien urna. Quisque tempor orci dignissim ligula commodo, sit amet fringilla enim facilisis. Suspendisse pretium, sapien ut euismod rutrum, quam nibh accumsan libero, vel efficitur odio nisi non turpis. Mauris malesuada nisi dolor, sit amet iaculis nisi placerat in. Sed sed ipsum erat. Curabitur at nisi erat. Quisque sed ligula tempor, aliquam nulla et, maximus purus. Maecenas lectus libero, convallis ut vestibulum vel, varius in dui. Duis sed dui tincidunt, laoreet felis eu, viverra sapien. Suspendisse vel lacus accumsan, rhoncus nisi eu, accumsan tortor.',
                'image' => 'img/image1.jpg',
                'status' => 'PUBLISHED',
                'state' => 'APPROVED',
                'rejection_reason' => null,
                'publish_date' => '2019-03-03 00:00:00',
                'visibility' => '1',
                'meta_title' => null,
                'meta_description' => null,
                'meta_keywords' => null,
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '4',
                'author_id' => '8',
                'posted_by' => '6',
                'category_id' => '6',
                'type' => 'CASE_STUDY',
                'title' => 'Maecenas in mi tristique',
                'slug' => 'maecenas-in-mi-tristique',
                'headline' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam consectetur efficitur augue ut luctus. Integer quis dui eget arcu sagittis sagittis et at urna. Duis vel nunc a nisl scelerisque consequat ut id dolor.',
                'body' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam consectetur efficitur augue ut luctus. Integer quis dui eget arcu sagittis sagittis et at urna. Duis vel nunc a nisl scelerisque consequat ut id dolor. Aliquam eu velit vehicula, accumsan justo ac, pharetra dui. Aliquam at arcu sagittis, congue lorem a, vehicula justo. Ut vehicula magna in massa dapibus aliquam. Pellentesque in dui sagittis, molestie mauris sit amet, aliquam lectus.

Phasellus luctus metus dictum sapien hendrerit iaculis. Nam pretium condimentum erat eget tristique. Quisque dictum tempus feugiat. Ut sit amet dolor enim. Suspendisse in neque eu neque aliquam vestibulum in pharetra sem. Etiam volutpat eros a orci pharetra, nec convallis tortor placerat. Aliquam mattis volutpat odio vel vulputate. Aenean purus tortor, aliquam vel bibendum in, varius a tellus. Sed nibh mi, consequat eget purus et, vestibulum blandit lorem. Aenean quis sapien in nisi dapibus euismod. Integer cursus imperdiet sem a pharetra. Phasellus mollis scelerisque auctor. Curabitur sed magna maximus, rutrum mi eu, condimentum nisl.

Pellentesque at metus quis justo aliquam tristique. Nullam eget tincidunt purus, in dictum tortor. Integer quis mi in purus vestibulum congue nec nec ipsum. Mauris porta pharetra massa. In porta, ligula ac suscipit auctor, nulla libero porta ex, ut finibus lorem eros eget velit. Aenean congue est ac neque euismod lacinia eu ut odio. Quisque tincidunt tortor at sapien efficitur luctus. Quisque mattis facilisis nulla, sit amet blandit nunc porta ac. Phasellus feugiat, nibh in cursus malesuada, arcu sapien mollis dui, at pharetra odio massa a sem.',
                'image' => 'img/image1.jpg',
                'status' => 'PUBLISHED',
                'state' => 'APPROVED',
                'rejection_reason' => null,
                'publish_date' => '2019-02-21 00:00:00',
                'visibility' => '1',
                'meta_title' => null,
                'meta_description' => null,
                'meta_keywords' => null,
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '5',
                'author_id' => '8',
                'posted_by' => '6',
                'category_id' => '9',
                'type' => 'CASE_STUDY',
                'title' => 'Nunc ultrices felis in',
                'slug' => 'nunc-ultrices-felis-in',
                'headline' => 'Mauris molestie ornare libero at malesuada. Integer feugiat velit posuere diam accumsan congue. Integer at enim sed urna malesuada posuere.',
                'body' => 'Proin eu augue gravida, malesuada tellus quis, lobortis sapien. Suspendisse ipsum est, pharetra a orci sed, ultricies ornare eros. Aenean finibus dui id leo mattis, et euismod libero convallis. Nullam et velit varius, accumsan velit eget, feugiat ligula. Praesent magna nunc, pellentesque ac nisl id, pulvinar finibus eros. Curabitur lacinia non sapien a tempus. Curabitur elementum rutrum ullamcorper. Vestibulum ut purus blandit, laoreet magna eget, elementum metus. Phasellus laoreet lacus non enim convallis scelerisque. Nam hendrerit nulla orci, in egestas sem hendrerit sit amet.

Nulla ut libero sed justo dignissim sagittis. In quis nulla dictum, viverra nibh et, lacinia mi. Integer vulputate quam ac tincidunt placerat. Morbi egestas orci vitae massa tincidunt, in hendrerit erat imperdiet. Cras lobortis ante nec velit tincidunt consectetur sit amet ullamcorper nibh. In maximus, eros vel interdum laoreet, enim odio porta nibh, non mollis mi justo ut risus. Suspendisse luctus sollicitudin neque consequat tincidunt. Mauris finibus, nisi sit amet cursus rhoncus, nisi tellus sodales sem, nec tempus ipsum est vel enim. Integer id quam metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce rhoncus at lacus aliquam tempus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc dui arcu, elementum sed elit at, sollicitudin tempus enim. Aenean turpis mi, gravida a mattis at, posuere eget ipsum.

In vulputate neque nisl, eu viverra eros elementum eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras bibendum quis mi eu finibus. Nam quis sapien urna. Quisque tempor orci dignissim ligula commodo, sit amet fringilla enim facilisis. Suspendisse pretium, sapien ut euismod rutrum, quam nibh accumsan libero, vel efficitur odio nisi non turpis. Mauris malesuada nisi dolor, sit amet iaculis nisi placerat in. Sed sed ipsum erat. Curabitur at nisi erat. Quisque sed ligula tempor, aliquam nulla et, maximus purus. Maecenas lectus libero, convallis ut vestibulum vel, varius in dui. Duis sed dui tincidunt, laoreet felis eu, viverra sapien. Suspendisse vel lacus accumsan, rhoncus nisi eu, accumsan tortor.',
                'image' => 'img/image1.jpg',
                'status' => 'PUBLISHED',
                'state' => 'APPROVED',
                'rejection_reason' => null,
                'publish_date' => '2019-03-03 00:00:00',
                'visibility' => '1',
                'meta_title' => null,
                'meta_description' => null,
                'meta_keywords' => null,
                'created' => $now,
                'modified' => $now,
            ],
        ];
        GlobalSeed::truncateTable($this, 'articles', $data);
    }
}
