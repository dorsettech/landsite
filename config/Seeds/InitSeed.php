<?php

/**
 * @author Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-05)
 * @version 1.0
 */

use Migrations\AbstractSeed;

/**
 * Init Seed
 */
class InitSeed extends AbstractSeed
{
    /**
     * Run Method
     *
     * @return void
     */
    public function run()
    {
        $this->call('ContentsSeed');
        $this->call('FormsSeed');
        $this->call('GroupsSeed');
        $this->call('LanguagesSeed');
        $this->call('MenusSeed');
        $this->call('PagesSeed');
        $this->call('PageFilesSeed');
        $this->call('PageTypesSeed');
        $this->call('StaticContentGroupsSeed');
        $this->call('StaticContentsSeed');
        $this->call('UsersSeed');
        // TheLandSite
        $this->call('UserBillingAddressesSeed');
        $this->call('UserDetailsSeed');
        $this->call('CategoriesSeed');
        $this->call('AreasSeed');
        $this->call('ProductsSeed');
        $this->call('ServicesSeed');
        $this->call('PropertiesSeed');
        $this->call('PropertyAttributesSeed');
        $this->call('PropertyEnquiriesSeed');
        $this->call('PropertyTypesSeed');
        $this->call('PropertyViewsSeed');
        $this->call('SettingsSeed');
        $this->call('ArticlesSeed');
    }
}
