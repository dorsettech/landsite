<?php

/**
 * @author eConnect4u
 * @date (2019-03-05)
 * @version 1.0
 */

use Migrations\AbstractSeed;

/**
 * PropertyTypes seed.
 */
class PropertyTypesSeed extends AbstractSeed
{
    /**
     * Run Method
     *
     * @return void
     */
    public function run()
    {
        $now = GlobalSeed::getCurrentDate();
        $data = [
            [
                'id' => '1',
                'parent_id' => null,
                'lft' => '1',
                'rght' => '2',
                'name' => 'Land',
                'position' => '0',
                'visibility' => '1',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '2',
                'parent_id' => null,
                'lft' => '3',
                'rght' => '4',
                'name' => 'Offices',
                'position' => '0',
                'visibility' => '1',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '3',
                'parent_id' => null,
                'lft' => '5',
                'rght' => '6',
                'name' => 'Commercial Property',
                'position' => '0',
                'visibility' => '1',
                'created' => $now,
                'modified' => $now,
            ],
        ];
        GlobalSeed::truncateTable($this, 'property_types', $data);
    }
}
