<?php

/**
 * @author eConnect4u
 * @date (2019-03-05)
 * @version 1.0
 */

use Migrations\AbstractSeed;

/**
 * Groups seed.
 */
class GroupsSeed extends AbstractSeed
{
    /**
     * Run Method
     *
     * @return void
     */
    public function run()
    {
        $now = GlobalSeed::getCurrentDate();
        $data = [
            [
                'id' => '1',
                'parent_id' => null,
                'lft' => '1',
                'rght' => '12',
                'name' => 'Guest',
                'permissions' => null,
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '2',
                'parent_id' => '1',
                'lft' => '2',
                'rght' => '9',
                'name' => 'User',
                'permissions' => '{"Contents":{"index":"0","delete":"0"},"Cropper":{"index":"0","crop":"0","restore":"0","cropFile":"0"},"CustomCss":{"index":"0","edit":"0","restore":"0"},"Dashboard":{"index":"1","getWebsiteAnalyticsData":"1","getDetailAnalyticsData":"1"},"Forms":{"index":"0","add":"0","edit":"0","delete":"0","toggleColumnValue":"0"},"FormsData":{"index":"0","listView":"0","listToCsv":"0","view":"0","delete":"0","exportMessages":"0","exportAllForms":"0","recentMessages":"0"},"Gallery":{"manage":"0","getSingleImageData":"0","updateSingleImage":"0","updateGalleryOrder":"0","deleteSingleGalleryImage":"0"},"Groups":{"index":"0","add":"0","edit":"0","delete":"0","permissions":"0"},"Languages":{"index":"0","add":"0","edit":"0","delete":"0","toggle":"0","setLanguage":"0","toggleColumnValue":"0"},"Loginbans":{"index":"0","add":"0","edit":"0","delete":"0"},"Logs":{"index":"0","viewUser":"0","showVersions":"0","compareVersions":"0","revertVersion":"0","truncate":"0","delete":"0","filter":"0","saveContents":"0"},"Menus":{"index":"0","view":"0","add":"0","edit":"0","delete":"0"},"Miscellaneous":{"sitemap":"0"},"PageFiles":{"edit":"0","delete":"0","addFilesToPage":"0"},"PageTypes":{"index":"0","add":"0","edit":"0","delete":"0"},"Pages":{"index":"0","add":"0","edit":"0","editContents":"0","delete":"0","clonePage":"0","updatePage":"0","updateElementField":"0","ajaxList":"0","changeRedirectionFrom":"0","editFiles":"0","toggleColumnValue":"0"},"Redirections":{"index":"0","add":"0","edit":"0","delete":"0"},"SeoAnalyser":{"index":"0","editable":"0","upload":"0"},"StaticContentGroups":{"index":"0","add":"0","view":"0","editGroup":"0","edit":"0","delete":"0"},"StaticContents":{"add":"0","edit":"0","delete":"0"},"Users":{"index":"0","add":"0","edit":"1","delete":"0","login":"1","logout":"1","unlockScreen":"1","refreshOnAlertClick":"1","token":"1","generateToken":"1","toggleColumnValue":"1"},"Manager":{"index":"0","upload":"0","file":"0","addFolder":"0","deleteFolder":"0","deleteFile":"0","deleteFiles":"0","renameFile":"0"}}',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '3',
                'parent_id' => '2',
                'lft' => '3',
                'rght' => '8',
                'name' => 'Moderator',
                'permissions' => '{"Contents":{"index":"1","delete":"0"},"Cropper":{"index":"1","crop":"1","restore":"1","cropFile":"1"},"CustomCss":{"index":"1","edit":"1","restore":"1"},"Dashboard":{"index":"1","getWebsiteAnalyticsData":"1","getDetailAnalyticsData":"1"},"Forms":{"index":"1","add":"1","edit":"1","delete":"0","toggleColumnValue":"1"},"FormsData":{"index":"1","listView":"1","listToCsv":"1","view":"1","delete":"0","exportMessages":"0","exportAllForms":"0","recentMessages":"1"},"Gallery":{"manage":"1","getSingleImageData":"1","updateSingleImage":"1","updateGalleryOrder":"1","deleteSingleGalleryImage":"1"},"Groups":{"index":"0","add":"0","edit":"0","delete":"0","permissions":"0"},"Languages":{"index":"1","add":"0","edit":"0","delete":"0","toggle":"0","setLanguage":"1","toggleColumnValue":"0"},"Loginbans":{"index":"0","add":"0","edit":"0","delete":"0"},"Logs":{"index":"0","viewUser":"0","showVersions":"0","compareVersions":"0","revertVersion":"0","truncate":"0","delete":"0","filter":"0","saveContents":"0"},"Menus":{"index":"0","view":"0","add":"0","edit":"0","delete":"0"},"Miscellaneous":{"sitemap":"1"},"PageFiles":{"edit":"1","delete":"1","addFilesToPage":"1"},"PageTypes":{"index":"0","add":"0","edit":"0","delete":"0"},"Pages":{"index":"1","add":"1","edit":"1","editContents":"1","delete":"0","clonePage":"1","updatePage":"1","updateElementField":"1","ajaxList":"1","changeRedirectionFrom":"1","editFiles":"1","toggleColumnValue":"1"},"Redirections":{"index":"1","add":"1","edit":"1","delete":"0"},"SeoAnalyser":{"index":"1","editable":"1","upload":"1"},"StaticContentGroups":{"index":"1","add":"0","view":"1","editGroup":"1","edit":"1","delete":"0"},"StaticContents":{"add":"0","edit":"1","delete":"0"},"Users":{"index":"0","add":"0","edit":"1","delete":"0","login":"1","logout":"1","unlockScreen":"1","refreshOnAlertClick":"1","token":"1","generateToken":"1","toggleColumnValue":"1"},"Manager":{"index":"1","upload":"1","file":"1","addFolder":"1","deleteFolder":"1","deleteFile":"1","deleteFiles":"1","renameFile":"1"}}',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '4',
                'parent_id' => '3',
                'lft' => '4',
                'rght' => '7',
                'name' => 'Administrator',
                'permissions' => '{"Contents":{"index":"1","delete":"1"},"Cropper":{"index":"1","crop":"1","restore":"1","cropFile":"1"},"CustomCss":{"index":"1","edit":"1","restore":"1"},"Dashboard":{"index":"1","getWebsiteAnalyticsData":"1","getDetailAnalyticsData":"1"},"Forms":{"index":"1","add":"1","edit":"1","delete":"1","toggleColumnValue":"1"},"FormsData":{"index":"1","listView":"1","listToCsv":"1","view":"1","delete":"1","exportMessages":"1","exportAllForms":"1","recentMessages":"1"},"Gallery":{"manage":"1","getSingleImageData":"1","updateSingleImage":"1","updateGalleryOrder":"1","deleteSingleGalleryImage":"1"},"Groups":{"index":"1","add":"1","edit":"1","delete":"1","permissions":"0"},"Languages":{"index":"1","add":"1","edit":"1","delete":"1","toggle":"1","setLanguage":"1","toggleColumnValue":"1"},"Loginbans":{"index":"1","add":"1","edit":"1","delete":"1"},"Logs":{"index":"1","viewUser":"1","showVersions":"1","compareVersions":"1","revertVersion":"1","truncate":"0","delete":"0","filter":"1","saveContents":"1"},"Menus":{"index":"1","view":"1","add":"1","edit":"1","delete":"1"},"Miscellaneous":{"sitemap":"1"},"PageFiles":{"edit":"1","delete":"1","addFilesToPage":"1"},"PageTypes":{"index":"1","add":"1","edit":"1","delete":"1"},"Pages":{"index":"1","add":"1","edit":"1","editContents":"1","delete":"1","clonePage":"1","updatePage":"1","updateElementField":"1","ajaxList":"1","changeRedirectionFrom":"1","editFiles":"1","toggleColumnValue":"1"},"Redirections":{"index":"1","add":"1","edit":"1","delete":"1"},"SeoAnalyser":{"index":"1","editable":"1","upload":"1"},"StaticContentGroups":{"index":"1","add":"1","view":"1","editGroup":"1","edit":"1","delete":"1"},"StaticContents":{"add":"1","edit":"1","delete":"1"},"Users":{"index":"1","add":"1","edit":"1","delete":"1","login":"1","logout":"1","unlockScreen":"1","refreshOnAlertClick":"1","token":"1","generateToken":"1","toggleColumnValue":"1"},"checkAllUsers":"1","Manager":{"index":"1","upload":"1","file":"1","addFolder":"1","deleteFolder":"1","deleteFile":"1","deleteFiles":"1","renameFile":"1"}}',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '5',
                'parent_id' => '4',
                'lft' => '5',
                'rght' => '6',
                'name' => 'Root',
                'permissions' => '{"Contents":{"index":"1","delete":"1"},"Cropper":{"index":"1","crop":"1","restore":"1","cropFile":"1"},"CustomCss":{"index":"1","edit":"1","restore":"1"},"Dashboard":{"index":"1","getWebsiteAnalyticsData":"1","getDetailAnalyticsData":"1"},"Forms":{"index":"1","add":"1","edit":"1","delete":"1","toggleColumnValue":"1"},"FormsData":{"index":"1","listView":"1","listToCsv":"1","view":"1","delete":"1","exportMessages":"1","exportAllForms":"1","recentMessages":"1"},"Gallery":{"manage":"1","getSingleImageData":"1","updateSingleImage":"1","updateGalleryOrder":"1","deleteSingleGalleryImage":"1"},"Groups":{"index":"1","add":"1","edit":"1","delete":"1","permissions":"1"},"Languages":{"index":"1","add":"1","edit":"1","delete":"1","toggle":"1","setLanguage":"1","toggleColumnValue":"1"},"Loginbans":{"index":"1","add":"1","edit":"1","delete":"1"},"Logs":{"index":"1","viewUser":"1","showVersions":"1","compareVersions":"1","revertVersion":"1","truncate":"1","delete":"1","filter":"1","saveContents":"1"},"Menus":{"index":"1","view":"1","add":"1","edit":"1","delete":"1"},"Miscellaneous":{"sitemap":"1"},"PageFiles":{"edit":"1","delete":"1","addFilesToPage":"1"},"PageTypes":{"index":"1","add":"1","edit":"1","delete":"1"},"Pages":{"index":"1","add":"1","edit":"1","editContents":"1","delete":"1","clonePage":"1","updatePage":"1","updateElementField":"1","ajaxList":"1","changeRedirectionFrom":"1","editFiles":"1","toggleColumnValue":"1"},"Redirections":{"index":"1","add":"1","edit":"1","delete":"1"},"SeoAnalyser":{"index":"1","editable":"1","upload":"1"},"StaticContentGroups":{"index":"1","add":"1","view":"1","editGroup":"1","edit":"1","delete":"1"},"StaticContents":{"add":"1","edit":"1","delete":"1"},"Users":{"index":"1","add":"1","edit":"1","delete":"1","login":"1","logout":"1","unlockScreen":"1","refreshOnAlertClick":"1","token":"1","generateToken":"1","toggleColumnValue":"1"},"Manager":{"index":"1","upload":"1","file":"1","addFolder":"1","deleteFolder":"1","deleteFile":"1","deleteFiles":"1","renameFile":"1"}}',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '6',
                'parent_id' => '1',
                'lft' => '10',
                'rght' => '11',
                'name' => 'Members',
                'permissions' => '{"Contents":{"index":"0","delete":"0"},"Cropper":{"index":"0","crop":"0","restore":"0","cropFile":"0"},"CustomCss":{"index":"0","edit":"0","restore":"0"},"Dashboard":{"index":"0","getWebsiteAnalyticsData":"0","getDetailAnalyticsData":"0"},"Forms":{"index":"0","add":"0","edit":"0","delete":"0","toggleColumnValue":"0"},"FormsData":{"index":"0","listView":"0","listToCsv":"0","view":"0","delete":"0","exportMessages":"0","exportAllForms":"0","recentMessages":"0"},"Gallery":{"manage":"0","getSingleImageData":"0","updateSingleImage":"0","updateGalleryOrder":"0","deleteSingleGalleryImage":"0"},"Groups":{"index":"0","add":"0","edit":"0","delete":"0","permissions":"0"},"Languages":{"index":"0","add":"0","edit":"0","delete":"0","toggle":"0","setLanguage":"0","toggleColumnValue":"0"},"Loginbans":{"index":"0","add":"0","edit":"0","delete":"0"},"Logs":{"index":"0","viewUser":"0","showVersions":"0","compareVersions":"0","revertVersion":"0","truncate":"0","delete":"0","filter":"0","saveContents":"0"},"Menus":{"index":"0","view":"0","add":"0","edit":"0","delete":"0"},"Miscellaneous":{"sitemap":"0"},"PageFiles":{"edit":"0","delete":"0","addFilesToPage":"0"},"PageTypes":{"index":"0","add":"0","edit":"0","delete":"0"},"Pages":{"index":"0","add":"0","edit":"0","editContents":"0","delete":"0","clonePage":"0","updatePage":"0","updateElementField":"0","ajaxList":"0","changeRedirectionFrom":"0","editFiles":"0","toggleColumnValue":"0"},"Redirections":{"index":"0","add":"0","edit":"0","delete":"0"},"SeoAnalyser":{"index":"0","editable":"0","upload":"0"},"StaticContentGroups":{"index":"0","add":"0","view":"0","editGroup":"0","edit":"0","delete":"0"},"StaticContents":{"add":"0","edit":"0","delete":"0"},"Users":{"index":"0","add":"0","edit":"0","delete":"0","login":"0","logout":"0","unlockScreen":"0","refreshOnAlertClick":"0","token":"0","generateToken":"0","toggleColumnValue":"0"},"Manager":{"index":"0","upload":"0","file":"0","addFolder":"0","deleteFolder":"0","deleteFile":"0","deleteFiles":"0","renameFile":"0"}}',
                'created' => $now,
                'modified' => $now,
            ],
        ];
        GlobalSeed::truncateTable($this, 'groups', $data);
    }
}
