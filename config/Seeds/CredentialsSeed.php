<?php

/**
 * @author eConnect4u
 * @date (2019-03-07)
 * @version 1.0
 */

use Migrations\AbstractSeed;

/**
 * Credentials seed.
 */
class CredentialsSeed extends AbstractSeed
{
    /**
     * Run Method
     *
     * @return void
     */
    public function run()
    {
        $now = GlobalSeed::getCurrentDate();
        $data = [
            [
                'id' => '1',
                'name' => 'Accreditation 1',
                'type' => 'ACC',
                'created' => $now,
            ],
            [
                'id' => '2',
                'name' => 'Accreditation 2',
                'type' => 'ACC',
                'created' => $now,
            ],
            [
                'id' => '3',
                'name' => 'Accreditation 3',
                'type' => 'ACC',
                'created' => $now,
            ],
            [
                'id' => '4',
                'name' => 'Accreditation 4',
                'type' => 'ACC',
                'created' => $now,
            ],
            [
                'id' => '5',
                'name' => 'Accreditation 5',
                'type' => 'ACC',
                'created' => $now,
            ],
            [
                'id' => '6',
                'name' => 'Association 1',
                'type' => 'ASS',
                'created' => $now,
            ],
            [
                'id' => '7',
                'name' => 'Association 2',
                'type' => 'ASS',
                'created' => $now,
            ],
            [
                'id' => '8',
                'name' => 'Association 3',
                'type' => 'ASS',
                'created' => $now,
            ],
            [
                'id' => '9',
                'name' => 'Association 4',
                'type' => 'ASS',
                'created' => $now,
            ],
            [
                'id' => '10',
                'name' => 'Association 5',
                'type' => 'ASS',
                'created' => $now,
            ],
        ];
        GlobalSeed::truncateTable($this, 'credentials', $data);
    }
}
