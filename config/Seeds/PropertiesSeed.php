<?php

/**
 * @author eConnect4u
 * @date (2019-03-05)
 * @version 1.0
 */

use Migrations\AbstractSeed;

/**
 * Properties seed.
 */
class PropertiesSeed extends AbstractSeed
{
    /**
     * Run Method
     *
     * @return void
     */
    public function run()
    {
        $now = GlobalSeed::getCurrentDate();
        $data = [
            [
                'id' => '1',
                'type_id' => '3',
                'user_id' => '21',
                'uid' => '1b190e06310011e9999bd45ddf137daa',
                'purpose' => 'SALE',
                'title' => 'Test property',
                'slug' => 'test-property',
                'price_sale' => '0.00',
                'price_rent' => '0.00',
                'location' => 'Bournemoun',
                'lat' => null,
                'lng' => null,
                'postcode' => 'BH7878',
                'headline' => 'Testing',
                'description' => 'Full desc',
                'website_url' => null,
                'status' => 'DRAFT',
                'publish_date' => null,
                'ad_type' => 'STANDARD',
                'ad_expiry_date' => null,
                'ad_cost' => '0.00',
                'ad_total_cost' => '0.00',
                'enquiries' => '1',
                'views' => '1',
                'created' => $now,
                'modified' => $now,
            ],
        ];
        GlobalSeed::truncateTable($this, 'properties', $data);
    }
}
