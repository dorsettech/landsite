<?php

/**
 * @author eConnect4u
 * @date (2019-03-05)
 * @version 1.0
 */

use Migrations\AbstractSeed;

/**
 * PropertyViews seed.
 */
class PropertyViewsSeed extends AbstractSeed
{
    /**
     * Run Method
     *
     * @return void
     */
    public function run()
    {
        $now = GlobalSeed::getCurrentDate();
        $data = [
            [
                'id' => '1',
                'created' => $now,
                'property_id' => '1',
            ],
        ];
        GlobalSeed::truncateTable($this, 'property_views', $data);
    }
}
