<?php

/**
 * @author  Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-13)
 * @version 1.0
 */

use Migrations\AbstractSeed;
use Cake\ORM\TableRegistry;
use Faker\Factory;

/**
 * News seed.
 */
class UniversalSeed extends AbstractSeed
{
    /**
     * Config
     *
     * @var array
     */
    protected $_config = [
        'rows' => 10,
        'table' => 'articles',
    ];

    /**
     * Run Method.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $data = [];
        $modelObject = TableRegistry::getTableLocator()->get($this->_config['table']);
        $table = $this->table($this->_config['table']);
        $schema = $modelObject->getSchema();
        for ($i = 0; $i < $this->_config['rows']; $i++) {
            foreach ($schema->columns() as $column) {
                $columnSettings = $schema->getColumn($column);
                switch ($columnSettings['type']) {
                    case "integer":
                        if (!isset($columnSettings['autoIncrement']) || (isset($columnSettings['autoIncrement']) && !$columnSettings['autoIncrement'])) {
                            $data[$i][$column] = $faker->numberBetween(0, 10000);
                        }
                        break;
                    case "boolean":
                        $data[$i][$column] = $faker->boolean(50);
                        break;
                    case "string":
                        switch ($column) {
                            case 'image':
                                $data[$i][$column] = str_replace('webroot/', '', $faker->image('webroot/files/seeder', 640, 480, 'city', true, 'test'));
                                break;
                            case 'urlname':
                            case 'slug':
                                $data[$i][$column] = $faker->slug();
                                break;
                            case 'link':
                                $data[$i][$column] = $faker->url();
                                break;
                            default:
                                $data[$i][$column] = $faker->text($columnSettings['length']);
                                break;
                        }
                        break;
                    case "text":
                        if ($column !== 'gallery') {
                            $data[$i][$column] = $faker->text();
                        }
                        break;
                    case "decimal":
                        $data[$i][$column] = $faker->randomFloat($columnSettings['precision']);
                        break;
                    case "float":
                        $data[$i][$column] = $faker->randomFloat($columnSettings['precision']);
                        break;
                    case "datetime":
                        $data[$i][$column] = $faker->dateTime()->format('Y-m-d H:i:s');
                        break;
                    case "timestamp":
                        $data[$i][$column] = $faker->dateTime()->format('Y-m-d H:i:s');
                        break;
                    case "date":
                        $data[$i][$column] = $faker->date();
                        break;
                }
            }
        }

        $table->insert($data)->save();
    }
}
