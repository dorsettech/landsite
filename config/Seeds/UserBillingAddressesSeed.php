<?php

/**
 * @author eConnect4u
 * @date (2019-03-06)
 * @version 1.0
 */

use Migrations\AbstractSeed;

/**
 * UserBillingAddresses seed.
 */
class UserBillingAddressesSeed extends AbstractSeed
{
    /**
     * Run Method
     *
     * @return void
     */
    public function run()
    {
        $now = GlobalSeed::getCurrentDate();
        $data = [
            [
                'id' => '1',
                'user_id' => '22',
                'is_default' => '1',
                'first_name' => null,
                'last_name' => null,
                'email' => null,
                'phone' => null,
                'company' => null,
                'postcode' => null,
                'address' => null,
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '2',
                'user_id' => '23',
                'is_default' => '1',
                'first_name' => null,
                'last_name' => null,
                'email' => null,
                'phone' => null,
                'company' => null,
                'postcode' => null,
                'address' => null,
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '3',
                'user_id' => '24',
                'is_default' => '1',
                'first_name' => null,
                'last_name' => null,
                'email' => null,
                'phone' => null,
                'company' => null,
                'postcode' => null,
                'address' => null,
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '4',
                'user_id' => '25',
                'is_default' => '1',
                'first_name' => null,
                'last_name' => null,
                'email' => null,
                'phone' => null,
                'company' => null,
                'postcode' => null,
                'address' => null,
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '5',
                'user_id' => '26',
                'is_default' => '1',
                'first_name' => null,
                'last_name' => null,
                'email' => null,
                'phone' => null,
                'company' => null,
                'postcode' => null,
                'address' => null,
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '6',
                'user_id' => '27',
                'is_default' => '1',
                'first_name' => null,
                'last_name' => null,
                'email' => null,
                'phone' => null,
                'company' => null,
                'postcode' => null,
                'address' => null,
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '7',
                'user_id' => '28',
                'is_default' => '1',
                'first_name' => null,
                'last_name' => null,
                'email' => null,
                'phone' => null,
                'company' => null,
                'postcode' => null,
                'address' => null,
                'created' => $now,
                'modified' => $now,
            ],
        ];
        GlobalSeed::truncateTable($this, 'user_billing_addresses', $data);
    }
}
