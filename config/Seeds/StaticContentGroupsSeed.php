<?php

/**
 * @author eConnect4u
 * @date (2019-03-05)
 * @version 1.0
 */

use Migrations\AbstractSeed;

/**
 * StaticContentGroups seed.
 */
class StaticContentGroupsSeed extends AbstractSeed
{
    /**
     * Run Method
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'Configuration',
            ],
            [
                'id' => '2',
                'name' => 'Address',
            ],
            [
                'id' => '3',
                'name' => 'HEAD/BODY options',
            ],
            [
                'id' => '4',
                'name' => 'Social Media',
            ],
            [
                'id' => '5',
                'name' => 'Footer',
            ],
            [
                'id' => '6',
                'name' => 'Map',
            ],
            [
                'id' => '7',
                'name' => 'Google Analytics Reports',
            ],
            [
                'id' => '8',
                'name' => 'Global Sections',
            ],
            [
                'id' => '9',
                'name' => 'Account Creation',
            ],
            [
                'id' => '10',
                'name' => 'Member Dashboard',
            ],
            [
                'id' => '11',
                'name' => 'Marketing Preferences',
            ],
            [
                'id' => '12',
                'name' => 'Forms',
            ],
        ];
        GlobalSeed::truncateTable($this, 'static_content_groups', $data);
    }
}
