<?php

/**
 * @author eConnect4u
 * @date (2019-03-05)
 * @version 1.0
 */

use Migrations\AbstractSeed;

/**
 * Forms seed.
 */
class FormsSeed extends AbstractSeed
{
    /**
     * Run Method
     *
     * @return void
     */
    public function run()
    {
        $now = GlobalSeed::getCurrentDate();
        $data = [
            [
                'id' => '1',
                'name' => 'Contact form',
                'alias' => 'contact',
                'html_class' => '',
                'emails' => '{"main":["marcin.nierobis@econnect4u.pl"],"cc":"","bcc":""}',
                'send_email' => '1',
                'thank_you' => '0',
                'redirect_url' => 'thank-you',
                'data' => '{"first_name":{"label":"","validation_text":"Name","placeholder":"First Name","class":"form-control","type":"text","value":"","validation":"min_length","rule":"3","required":"1"},"surname":{"label":"","validation_text":"Surnanme","placeholder":"Surname","class":"form-control","type":"text","value":"","validation":"","rule":"","required":"1"},"email":{"label":"","validation_text":"Email address","placeholder":"Email","class":"form-control","type":"email","value":"","validation":"","rule":"","required":"1"},"phone":{"label":"","validation_text":"Phone number","placeholder":"Phone number","class":"form-control","type":"number","value":"","validation":"","rule":"","required":"1"},"postcode":{"label":"","validation_text":"Postcode","placeholder":"Postcode","class":"form-control","type":"text","value":"","validation":"","rule":"","required":"0"},"company":{"label":"","validation_text":"Company","placeholder":"Company","class":"form-control","type":"text","value":"","validation":"","rule":"","required":"0"},"message":{"label":"","validation_text":"Message is mandatory","placeholder":"Type your message here...","class":"form-control","type":"textarea","value":"","validation":"none","rule":"","required":"0"},"receive_news":{"label":"","validation_text":"","placeholder":"","class":"","type":"checkbox","value":"Yes","validation":"","rule":"","required":"0"},"submit":{"label":"Send <i class=\\"far fa-envelope\\"><\\/i>","validation_text":"","placeholder":"","class":"btn btn-primary","type":"submit","value":"","validation":"","rule":"","required":"0"}}',
                'active' => '1',
                'deleted' => '0',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '2',
                'name' => 'Newsletter',
                'alias' => 'newsletter',
                'html_class' => 'input-group',
                'emails' => '{"main":"","cc":"","bcc":""}',
                'send_email' => '0',
                'thank_you' => '0',
                'redirect_url' => '',
                'data' => '{"email":{"label":"","validation_text":"Email","placeholder":"Enter your email here","class":"form-control","type":"email","value":"","validation":"","rule":"","required":"1"},"submit":{"label":"Submit","validation_text":"","placeholder":"","class":"btn","type":"submit","value":"","validation":"","rule":"","required":"0"}}',
                'active' => '1',
                'deleted' => '0',
                'created' => $now,
                'modified' => $now,
            ],
        ];
        GlobalSeed::truncateTable($this, 'forms', $data);
    }
}
