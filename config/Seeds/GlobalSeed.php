<?php

/**
 * @author Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-05)
 * @version 1.0
 */

/**
 * Global Seed
 */
class GlobalSeed extends \Phinx\Seed\AbstractSeed
{
    /**
     * Truncate table if exist
     *
     * @param object|null $obj       Migration object.
     * @param string|null $tableName Table name.
     * @param array       $data      Array data.
     *
     * @return void
     */
    public static function truncateTable($obj, $tableName = '', array $data = [])
    {
        if ($obj->hasTable($tableName)) {
            $obj->execute('SET FOREIGN_KEY_CHECKS = 0;');
            $table = $obj->table($tableName);
            $table->truncate();
            $table->insert($data)->save();
            $obj->execute('SET FOREIGN_KEY_CHECKS = 1;');
        }
    }

    /**
     * Get current date
     *
     * @return string
     */
    public static function getCurrentDate()
    {
        return date('Y-m-d H:i:s');
    }
}
