<?php

/**
 * @author eConnect4u
 * @date (2019-03-05)
 * @version 1.0
 */

use Migrations\AbstractSeed;

/**
 * Products seed.
 */
class ProductsSeed extends AbstractSeed
{
    /**
     * Run Method
     *
     * @return void
     */
    public function run()
    {
        $now = GlobalSeed::getCurrentDate();
        $data = [
            [
                'id' => '1',
                'name' => 'Service 1',
                'description' => null,
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '2',
                'name' => 'Service 2',
                'description' => null,
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '3',
                'name' => 'Service 3',
                'description' => null,
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '4',
                'name' => 'Service 4',
                'description' => null,
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '5',
                'name' => 'Driving',
                'description' => null,
                'created' => $now,
                'modified' => $now,
            ],
        ];
        GlobalSeed::truncateTable($this, 'products', $data);
    }
}
