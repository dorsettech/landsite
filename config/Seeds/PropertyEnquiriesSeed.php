<?php

/**
 * @author eConnect4u
 * @date (2019-03-05)
 * @version 1.0
 */

use Migrations\AbstractSeed;

/**
 * PropertyEnquiries seed.
 */
class PropertyEnquiriesSeed extends AbstractSeed
{
    /**
     * Run Method
     *
     * @return void
     */
    public function run()
    {
        $now = GlobalSeed::getCurrentDate();
        $data = [
            [
                'id' => '1',
                'property_id' => '1',
                'user_id' => '21',
                'title' => 'Test',
                'full_name' => 'Joe Doe',
                'phone' => '343434',
                'message' => null,
                'created' => $now,
            ],
        ];
        GlobalSeed::truncateTable($this, 'property_enquiries', $data);
    }
}
