<?php

/**
 * @author eConnect4u
 * @date (2019-03-05)
 * @version 1.0
 */

use Migrations\AbstractSeed;

/**
 * PageTypes seed.
 */
class PageTypesSeed extends AbstractSeed
{
    /**
     * Run Method
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'title' => 'Regular page',
                'model' => null,
                'menu' => null,
                'sitemap' => '',
            ],
        ];
        GlobalSeed::truncateTable($this, 'page_types', $data);
    }
}
