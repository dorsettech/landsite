<?php

/**
 * @author eConnect4u
 * @date (2019-03-06)
 * @version 1.0
 */

use Migrations\AbstractSeed;

/**
 * UserDetails seed.
 */
class UserDetailsSeed extends AbstractSeed
{
    /**
     * Run Method
     *
     * @return void
     */
    public function run()
    {
        $now = GlobalSeed::getCurrentDate();
        $data = [
            [
                'id' => '1',
                'user_id' => '22',
                'dashboard_type' => null,
                'company' => 'LMA Architects',
                'location' => null,
                'postcode' => 'BH17 7BX',
                'address' => 'BESPOKE 4 BUSINESS MARKETING, 1U, UNIT 10, ALBANY PARK, CABOT LANE, POOLE',
                'pref_buying' => '0',
                'pref_selling' => '0',
                'pref_professional' => '1',
                'pref_insights' => '0',
                'pref_insights_list' => '2',
                'use_billing_details' => '0',
                'marketing_agreement_1' => '1',
                'marketing_agreement_2' => '0',
                'marketing_agreement_3' => '1',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '2',
                'user_id' => '23',
                'dashboard_type' => null,
                'company' => 'Ellis Jones Solicitors',
                'location' => null,
                'postcode' => 'BH17 7BX',
                'address' => 'WESSEX WATER PLC, 2X, POOLE SEWAGE TREATMENT WORKS, CABOT LANE, POOLE',
                'pref_buying' => '0',
                'pref_selling' => '0',
                'pref_professional' => '1',
                'pref_insights' => '0',
                'pref_insights_list' => '2',
                'use_billing_details' => '0',
                'marketing_agreement_1' => '0',
                'marketing_agreement_2' => '0',
                'marketing_agreement_3' => '0',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '3',
                'user_id' => '24',
                'dashboard_type' => null,
                'company' => 'Living-Space Construction',
                'location' => null,
                'postcode' => 'BH17 7BX',
                'address' => 'BRITISH RED CROSS, 1H, UNIT 1, ALBANY PARK, CABOT LANE, POOLE',
                'pref_buying' => '0',
                'pref_selling' => '0',
                'pref_professional' => '1',
                'pref_insights' => '0',
                'pref_insights_list' => '2',
                'use_billing_details' => '0',
                'marketing_agreement_1' => '0',
                'marketing_agreement_2' => '0',
                'marketing_agreement_3' => '0',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '4',
                'user_id' => '25',
                'dashboard_type' => null,
                'company' => 'Colmar Construction',
                'location' => null,
                'postcode' => 'BH17 7BX',
                'address' => 'NEW FOREST MOTOR FACTORS LTD, 2D, UNIT 16-17, ALBANY PARK, CABOT LANE, POOLE',
                'pref_buying' => '0',
                'pref_selling' => '0',
                'pref_professional' => '1',
                'pref_insights' => '0',
                'pref_insights_list' => '2',
                'use_billing_details' => '0',
                'marketing_agreement_1' => '0',
                'marketing_agreement_2' => '0',
                'marketing_agreement_3' => '0',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '5',
                'user_id' => '26',
                'dashboard_type' => null,
                'company' => 'Redline Surveys',
                'location' => null,
                'postcode' => 'BH17 7BX',
                'address' => 'OAK INNOVATION LTD, 3G, UNIT 7, ALBANY PARK, CABOT LANE, POOLE',
                'pref_buying' => '0',
                'pref_selling' => '0',
                'pref_professional' => '1',
                'pref_insights' => '0',
                'pref_insights_list' => '2',
                'use_billing_details' => '0',
                'marketing_agreement_1' => '0',
                'marketing_agreement_2' => '0',
                'marketing_agreement_3' => '0',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '6',
                'user_id' => '27',
                'dashboard_type' => null,
                'company' => 'JV Architects',
                'location' => null,
                'postcode' => 'BH17 7BX',
                'address' => 'VIRGIN ACTIVE, 4G, CABOT LANE, POOLE',
                'pref_buying' => '0',
                'pref_selling' => '0',
                'pref_professional' => '1',
                'pref_insights' => '0',
                'pref_insights_list' => '2',
                'use_billing_details' => '0',
                'marketing_agreement_1' => '0',
                'marketing_agreement_2' => '0',
                'marketing_agreement_3' => '0',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '7',
                'user_id' => '28',
                'dashboard_type' => null,
                'company' => 'MOBIFLY GRZEGORZ ZAGROBELNY',
                'location' => null,
                'postcode' => '59-500',
                'address' => 'Kozów, 30',
                'pref_buying' => '0',
                'pref_selling' => '0',
                'pref_professional' => '1',
                'pref_insights' => '0',
                'pref_insights_list' => '2',
                'use_billing_details' => '0',
                'marketing_agreement_1' => '0',
                'marketing_agreement_2' => '0',
                'marketing_agreement_3' => '0',
                'created' => $now,
                'modified' => $now,
            ],
        ];
        GlobalSeed::truncateTable($this, 'user_details', $data);
    }
}
