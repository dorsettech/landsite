<?php

/**
 * @author eConnect4u
 * @date (2019-03-06)
 * @version 1.0
 */

use Migrations\AbstractSeed;

/**
 * StaticContents seed.
 */
class StaticContentsSeed extends AbstractSeed
{
    /**
     * Run Method
     *
     * @return void
     */
    public function run()
    {
        $now = GlobalSeed::getCurrentDate();
        $data = [
            [
                'id' => '1',
                'static_group_id' => '1',
                'var_name' => 'available_app_types',
                'value' => 'doc|docx|odt|pdf|jpg|jpeg|png',
                'type' => '0',
                'description' => 'Possible job application file upload extensions',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '2',
                'static_group_id' => '1',
                'var_name' => 'available_file_types',
                'value' => 'doc|odt|pdf',
                'type' => '0',
                'description' => 'Possible file upload extensions',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '3',
                'static_group_id' => '1',
                'var_name' => 'available_image_types',
                'value' => 'jpg|jpeg|png',
                'type' => '0',
                'description' => 'Possible image upload extensions',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '4',
                'static_group_id' => '1',
                'var_name' => 'copy',
                'value' => 'The Land Site',
                'type' => '0',
                'description' => 'Copyright info used in footer',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '5',
                'static_group_id' => '1',
                'var_name' => 'dir_files',
                'value' => 'files',
                'type' => '0',
                'description' => 'Main files directory',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '6',
                'static_group_id' => '1',
                'var_name' => 'enabled_sass_files',
                'value' => 'Available sass files to edit by admin',
                'type' => '0',
                'description' => '_variables.scss',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '7',
                'static_group_id' => '1',
                'var_name' => 'file_max_size',
                'value' => '10485760',
                'type' => '0',
                'description' => 'File maximum size in bytes; default: 10MB (10*1024*1024)',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '8',
                'static_group_id' => '1',
                'var_name' => 'image_max_height',
                'value' => '2000',
                'type' => '0',
                'description' => 'Maximum height of uploaded image',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '9',
                'static_group_id' => '1',
                'var_name' => 'image_max_width',
                'value' => '2000',
                'type' => '0',
                'description' => 'Maximum width of uploaded image',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '10',
                'static_group_id' => '1',
                'var_name' => 'service',
                'value' => 'The Land Site',
                'type' => '0',
                'description' => 'Website name',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '11',
                'static_group_id' => '2',
                'var_name' => 'address',
                'value' => '<address><strong><span class="navy">Lorem Ipsum Ltd</span></strong><br />12 Lorem ipsum, Dolor<br />Sit Amet, AB3 45CD<br /><abbr title="Phone">P:</abbr> 01234567890</address>',
                'type' => '4',
                'description' => 'Address used in layouts (footer etc.)',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '12',
                'static_group_id' => '2',
                'var_name' => 'phone',
                'value' => '01202 675564',
                'type' => '0',
                'description' => 'Phone number in header',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '13',
                'static_group_id' => '2',
                'var_name' => 'mobile',
                'value' => '',
                'type' => '0',
                'description' => 'Mobile number used in layouts (header, footer etc.)',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '14',
                'static_group_id' => '3',
                'var_name' => 'scripts_head',
                'value' => '',
                'type' => '5',
                'description' => 'JS scripts included in HEAD section.',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '15',
                'static_group_id' => '3',
                'var_name' => 'scripts_body_top',
                'value' => '',
                'type' => '5',
                'description' => 'JS scripts included right after BODY tag. ',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '16',
                'static_group_id' => '3',
                'var_name' => 'scripts_body_end',
                'value' => '',
                'type' => '5',
                'description' => 'JS scripts added befor closing BODY tag. ',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '17',
                'static_group_id' => '4',
                'var_name' => 'social_facebook',
                'value' => 'http://facebook.com',
                'type' => '0',
                'description' => 'Full URL (including http:// or https://) to fanpage on Facebook',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '18',
                'static_group_id' => '4',
                'var_name' => 'social_pinterest',
                'value' => '',
                'type' => '0',
                'description' => 'Full URL (including http:// or https://) to page on Pinterest',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '19',
                'static_group_id' => '4',
                'var_name' => 'social_youtube',
                'value' => '',
                'type' => '0',
                'description' => 'Full URL (including http:// or https://) to channel on youtube',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '20',
                'static_group_id' => '4',
                'var_name' => 'social_instagram',
                'value' => 'https://www.instagram.com/',
                'type' => '0',
                'description' => 'Full URL (including http:// or https://) to fanpage on Instragram',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '21',
                'static_group_id' => '4',
                'var_name' => 'social_gplus',
                'value' => 'https://google.com',
                'type' => '0',
                'description' => 'Full URL (including http:// or https://) to page on Google+',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '22',
                'static_group_id' => '4',
                'var_name' => 'social_twitter',
                'value' => 'https://twitter.com',
                'type' => '0',
                'description' => 'Full URL (including http:// or https://) to fanpage on Twitter',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '23',
                'static_group_id' => '4',
                'var_name' => 'social_linkedin',
                'value' => 'http://linked.in',
                'type' => '0',
                'description' => 'Full URL (including http:// or https://) to profile on LinkedIn',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '24',
                'static_group_id' => '1',
                'var_name' => 'cookie_policy',
                'value' => '<p>This site uses some unobtrusive cookies to store information on your computer. By using our site you accept our <a href="/cookie-policy">Terms And Conditions and Privacy Policy</a>.</p>
',
                'type' => '4',
                'description' => 'Cookie policy message',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '25',
                'static_group_id' => '6',
                'var_name' => 'map_address',
                'value' => 'BH14 9LU, UK',
                'type' => '0',
                'description' => 'Map address to display on map.',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '27',
                'static_group_id' => '6',
                'var_name' => 'map_latitude',
                'value' => '55.3477621',
                'type' => '0',
                'description' => 'Map latitude value',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '28',
                'static_group_id' => '6',
                'var_name' => 'map_longitude',
                'value' => '-3.8182996',
                'type' => '0',
                'description' => 'Map longitude value',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '29',
                'static_group_id' => '6',
                'var_name' => 'map_zoom',
                'value' => '8',
                'type' => '0',
                'description' => 'Map zoom value',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '30',
                'static_group_id' => '6',
                'var_name' => 'map_title',
                'value' => 'Bespoke 4 Business',
                'type' => '0',
                'description' => 'Title of map marker',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '31',
                'static_group_id' => '7',
                'var_name' => 'ga_view_id',
                'value' => '59426091',
                'type' => '0',
                'description' => 'Can be found at the bottom of the page (under "view") here: https://ga-dev-tools.appspot.com/account-explorer/. Remember to select the correct account! Leave blank to disable google analytics widget.',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '32',
                'static_group_id' => '7',
                'var_name' => 'ga_days_back',
                'value' => '30',
                'type' => '1',
                'description' => 'How many days back from today should the analytics display?',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '33',
                'static_group_id' => '8',
                'var_name' => 'newsletter_section_content',
                'value' => '<div class="heading">Sign up to our Newsletter</div>

<p>Lorem ipsum dolor sit amet, consectetur elit. Nullam laoreet, felis at iaculis varius, pulvinar<br />
felis, a ullamcorper felis turpis. Donec at euismod sem.</p>
',
                'type' => '4',
                'description' => 'Content of the newsletter section.',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '34',
                'static_group_id' => '5',
                'var_name' => 'footer_company_info',
                'value' => '<div class="logo-footer"><a href="/"><img alt="Logo" class="img-fluid" src="img/logo_white.png" /> </a></div>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla a ullamcorper arcu.<br />
<br />
Nam tempus elementum mauris ornare efficitur.</p>
',
                'type' => '4',
                'description' => 'Company Information in footer.',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '35',
                'static_group_id' => '5',
                'var_name' => 'footer_sellers',
                'value' => '<div class="d-none d-md-block" type="button">Sellers</div>

<div class="d-md-none" data-target="#footer1" data-toggle="collapse" type="button">Sellers</div>

<div class="collapse show" data-parent="#footer_collapse" id="footer1">
<ul class="list-unstyled">
<li><a href="#">Lorem ipsum</a></li>
<li><a href="#">Anim pariatur cliche</a></li>
<li><a href="#">Ad vegan excepteur butcher</a></li>
<li><a href="#">Brunch 3 wolf moon tempor</a></li>
<li><a href="#">Nihil anim</a></li>
</ul>
</div>
',
                'type' => '4',
                'description' => 'Sellers section in footer',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '36',
                'static_group_id' => '5',
                'var_name' => 'footer_buyers',
                'value' => '<div class="d-none d-md-block" type="button">Buyers</div>

<div class="d-md-none collapsed" data-target="#footer2" data-toggle="collapse" type="button">Buyers</div>

<div class="collapse" data-parent="#footer_collapse" id="footer2">
<ul class="list-unstyled">
<li><a href="#">Lorem ipsum</a></li>
<li><a href="#">Anim pariatur cliche</a></li>
<li><a href="#">Ad vegan excepteur butcher</a></li>
<li><a href="#">Brunch 3 wolf moon tempor</a></li>
<li><a href="#">Nihil anim</a></li>
</ul>
</div>
',
                'type' => '4',
                'description' => 'Buyers section in footer',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '37',
                'static_group_id' => '5',
                'var_name' => 'footer_professionals',
                'value' => '<div class="d-none d-md-block" type="button">Professionals</div>

<div class="d-md-none collapsed" data-target="#footer3" data-toggle="collapse" type="button">Professionals</div>

<div class="collapse" data-parent="#footer_collapse" id="footer3">
<ul class="list-unstyled">
<li><a href="#">Lorem ipsum</a></li>
<li><a href="#">Anim pariatur cliche</a></li>
<li><a href="#">Ad vegan excepteur butcher</a></li>
<li><a href="#">Brunch 3 wolf moon tempor</a></li>
<li><a href="#">Nihil anim</a></li>
</ul>
</div>
',
                'type' => '4',
                'description' => 'Professionals section in footer',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '38',
                'static_group_id' => '5',
                'var_name' => 'footer_info',
                'value' => '<div class="d-none d-md-block" type="button">Info</div>

<div class="d-md-none collapsed" data-target="#footer4" data-toggle="collapse" type="button">Info</div>

<div class="collapse" data-parent="#footer_collapse" id="footer4">
<ul class="list-unstyled">
<li><a href="#">Lorem ipsum</a></li>
<li><a href="#">Anim pariatur cliche</a></li>
<li><a href="#">Ad vegan excepteur butcher</a></li>
<li><a href="#">Brunch 3 wolf moon tempor</a></li>
<li><a href="#">Nihil anim</a></li>
<li><a href="#">Ad vegan excepteur butcher</a></li>
</ul>
</div>
',
                'type' => '4',
                'description' => 'Information section in footer',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '39',
                'static_group_id' => '5',
                'var_name' => 'footer_copyright',
                'value' => '<div class="col-auto">Copyright 2018 The Land Site</div>',
                'type' => '4',
                'description' => 'Copyright section in footer',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '40',
                'static_group_id' => '8',
                'var_name' => 'get_your_business',
                'value' => '<p><a class="btn btn-secondary" href="#">Get your business listed with us</a></p>
',
                'type' => '4',
                'description' => '"Get your business..." link',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '41',
                'static_group_id' => '8',
                'var_name' => 'recent_insights_section_description',
                'value' => '<div class="heading">Recent Insights</div>

<p>Want to contribute? If you have any useful information about this industry please contact us and we&rsquo;ll publish it</p>
',
                'type' => '4',
                'description' => 'The main description of subpages Recent Insights section.',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '42',
                'static_group_id' => '1',
                'var_name' => 'login_title',
                'value' => 'The <strong>Land Site</strong>',
                'type' => '0',
                'description' => 'Title on panel login page',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '43',
                'static_group_id' => '1',
                'var_name' => 'login_slogan',
                'value' => 'Lorem ipsum dolor sit amet.',
                'type' => '0',
                'description' => 'Slogan used on Panel Login page',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '44',
                'static_group_id' => '9',
                'var_name' => 'content_title',
                'value' => 'The benefits of having an account',
                'type' => '0',
                'description' => 'Left hand side content title',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '45',
                'static_group_id' => '9',
                'var_name' => 'content_description1',
                'value' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ac vehicula augue. Donec eget finibus ex, a dictum tortor. Phasellus condimentum lorem non finibus cursus. Mauris et quam eleifend, venenatis arcu dignissim, dapibus risus. Mauris cursus vitae eros non vestibulum. Sed a dui efficitur dolor sagittis fringilla at eu ipsum. Nunc non eleifend sapien.</p>

<p>Donec tempus ullamcorper justo et placerat. Cras pulvinar pretium dolor eu ultrices. Suspendisse suscipit quam et lorem.</p>
',
                'type' => '4',
                'description' => 'Left hand side top description',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '46',
                'static_group_id' => '9',
                'var_name' => 'content_description2',
                'value' => '<ul class="list-unstyled">
<li>Lorem ipsum dolor sit amet</li>
<li>Consectetur adipiscing elit</li>
<li>Aliquam ac vehicula augue</li>
<li>Donec eget finibus ex</li>
<li>Phasellus condimentum lorem non finibus cursus</li>
<li>Mauris et quam eleifend</li>
<li>Venenatis arcu dignissim</li>
<li>Mauris cursus vitae eros non vestibulum</li>
<li>Sed a dui efficitur dolor sagittis fringilla at eu ipsum</li>
<li>Suspendisse suscipit quam et lorem</li>
<li>Cras pulvinar pretium dolor eu ultrices</li>
</ul>
',
                'type' => '4',
                'description' => 'Left hand side bottom description',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '47',
                'static_group_id' => '9',
                'var_name' => 'content_slogan',
                'value' => 'One account... Endless possibilities',
                'type' => '0',
                'description' => 'Left hand side bottom slogan',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '48',
                'static_group_id' => '9',
                'var_name' => 'toggle1_title',
                'value' => 'Buying',
                'type' => '0',
                'description' => 'First toggle title (buying)',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '49',
                'static_group_id' => '9',
                'var_name' => 'toggle1_description',
                'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'type' => '3',
                'description' => 'First toggle description (buying)',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '50',
                'static_group_id' => '9',
                'var_name' => 'toggle2_title',
                'value' => 'Selling',
                'type' => '0',
                'description' => 'Second toggle title (selling)',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '51',
                'static_group_id' => '9',
                'var_name' => 'toggle2_description',
                'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'type' => '3',
                'description' => 'Second toggle description (buying)',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '52',
                'static_group_id' => '9',
                'var_name' => 'toggle3_title',
                'value' => 'Professional Services',
                'type' => '0',
                'description' => 'Third toggle title (professional services)',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '53',
                'static_group_id' => '9',
                'var_name' => 'toggle3_description',
                'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'type' => '3',
                'description' => 'Third toggle description (professional services)',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '54',
                'static_group_id' => '9',
                'var_name' => 'toggle4_title',
                'value' => 'Insights',
                'type' => '0',
                'description' => 'Third toggle title (insights)',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '55',
                'static_group_id' => '9',
                'var_name' => 'toggle4_description',
                'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'type' => '3',
                'description' => 'Third toggle description (professional services)',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '56',
                'static_group_id' => '9',
                'var_name' => 'categories_title',
                'value' => 'What news do you want to hear about?',
                'type' => '0',
                'description' => 'Categories selection heading',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '57',
                'static_group_id' => '9',
                'var_name' => 'categories_description',
                'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ac vehicula augue. Donec eget finibus ex, a dictum tortor. Phasellus condimentum lorem non finibus cursus.',
                'type' => '3',
                'description' => 'Categories selection - first paragraph',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '58',
                'static_group_id' => '10',
                'var_name' => 'box1_title',
                'value' => 'Add your first property today',
                'type' => '0',
                'description' => 'Box1 title',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '59',
                'static_group_id' => '10',
                'var_name' => 'box1_description',
                'value' => 'Click here to add your first property today',
                'type' => '0',
                'description' => 'Box1 description',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '60',
                'static_group_id' => '10',
                'var_name' => 'box1_icon',
                'value' => null,
                'type' => '7',
                'description' => 'Box1 icon',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '61',
                'static_group_id' => '10',
                'var_name' => 'box1_background',
                'value' => null,
                'type' => '7',
                'description' => 'Box1 background',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '62',
                'static_group_id' => '10',
                'var_name' => 'box2_title',
                'value' => 'Showcase your professional services',
                'type' => '0',
                'description' => 'Box2 title',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '63',
                'static_group_id' => '10',
                'var_name' => 'box2_description',
                'value' => 'Click here to add your first business listing',
                'type' => '0',
                'description' => 'Box2 description',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '64',
                'static_group_id' => '10',
                'var_name' => 'box2_icon',
                'value' => null,
                'type' => '7',
                'description' => 'Box2 icon',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '65',
                'static_group_id' => '10',
                'var_name' => 'box2_background',
                'value' => null,
                'type' => '7',
                'description' => 'Box2 background',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '66',
                'static_group_id' => '10',
                'var_name' => 'box3_title',
                'value' => 'Search for a property today',
                'type' => '0',
                'description' => 'Box3 title',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '67',
                'static_group_id' => '10',
                'var_name' => 'box3_description',
                'value' => 'Click here to search now',
                'type' => '0',
                'description' => 'Box3 description',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '68',
                'static_group_id' => '10',
                'var_name' => 'box3_icon',
                'value' => null,
                'type' => '7',
                'description' => 'Box3 icon',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '69',
                'static_group_id' => '10',
                'var_name' => 'box3_background',
                'value' => null,
                'type' => '7',
                'description' => 'Box3 background',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '70',
                'static_group_id' => '10',
                'var_name' => 'box4_title',
                'value' => 'Search for professional services',
                'type' => '0',
                'description' => 'Box4 title',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '71',
                'static_group_id' => '10',
                'var_name' => 'box4_description',
                'value' => 'Click here to search now',
                'type' => '0',
                'description' => 'Box4 description',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '72',
                'static_group_id' => '10',
                'var_name' => 'box4_icon',
                'value' => null,
                'type' => '7',
                'description' => 'Box4 icon',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '73',
                'static_group_id' => '10',
                'var_name' => 'box4_background',
                'value' => null,
                'type' => '7',
                'description' => 'Box4 background',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '74',
                'static_group_id' => '11',
                'var_name' => 'marketing1_title',
                'value' => 'Marketing preference 1',
                'type' => '0',
                'description' => 'Marketing preference 1 title',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '75',
                'static_group_id' => '11',
                'var_name' => 'marketing1_description',
                'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris suscipit et ex mattis sollicitudin. Integer elementum mattis nulla, id molestie nunc vulputate nec. Aliquam vestibulum felis at magna hendrerit sollicitudin. Aliquam placerat ultrices ligula eget pretium. Ut posuere rutrum ultrices. Curabitur eget elit ut massa dapibus convallis. Nullam ullamcorper sagittis neque et fermentum. Pellentesque et ornare leo, sed malesuada nisl. Fusce egestas eget metus sit amet semper.',
                'type' => '3',
                'description' => 'Marketing preference 1 description',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '76',
                'static_group_id' => '11',
                'var_name' => 'marketing2_title',
                'value' => 'Marketing preference 2',
                'type' => '0',
                'description' => 'Marketing preference 2 title',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '77',
                'static_group_id' => '11',
                'var_name' => 'marketing2_description',
                'value' => 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus dictum vitae velit id porttitor. Mauris eu interdum velit, et iaculis mi. Cras vehicula nibh condimentum laoreet viverra. Suspendisse potenti. Mauris rutrum est sit amet pretium lacinia. Duis eu fringilla quam. Nam dignissim nisi ac sodales dictum. Sed nec nibh vitae tortor porttitor luctus. Pellentesque sed ante diam. In hac habitasse platea dictumst. ',
                'type' => '3',
                'description' => 'Marketing preference 2 description',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '78',
                'static_group_id' => '11',
                'var_name' => 'marketing3_title',
                'value' => 'Marketing preference 3',
                'type' => '0',
                'description' => 'Marketing preference 3 title',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '79',
                'static_group_id' => '11',
                'var_name' => 'marketing3_description',
                'value' => 'Vivamus ac nisi leo. Curabitur volutpat, lacus sed elementum tempus, urna mauris dapibus nulla, ac auctor felis enim sit amet arcu. Curabitur ac diam at ex lacinia condimentum. In et laoreet sem. Etiam blandit et metus ac vehicula. Nam mollis lacus et sodales faucibus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus vitae diam dui. Ut egestas, ipsum mollis interdum hendrerit, leo neque pharetra nibh, eget placerat quam arcu at sem. ',
                'type' => '3',
                'description' => 'Marketing preference 3 description',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '80',
                'static_group_id' => '12',
                'var_name' => 'terms_and_conditions_label',
                'value' => '<p><strong>Terms &amp; Conditions</strong> - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc et sapien nec lorem scelerisque cursus vitae porttitor elit.</p>
',
                'type' => '4',
                'description' => 'Label of terms and conditions on forms',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '81',
                'static_group_id' => '8',
                'var_name' => 'want_to_contribute',
                'value' => '<div class="heading text-center">Want to contribute?</div>

<p class="text-center">If you have any useful information about this industry, please contact us and we&rsquo;ll publish it</p>
',
                'type' => '4',
                'description' => 'Want to Contribute section in sidebar',
                'created' => $now,
                'modified' => $now,
            ],
            [
                'id' => '82',
                'static_group_id' => '8',
                'var_name' => 'og_image',
                'value' => 'files/contents/image1_5.jpg',
                'type' => '7',
                'description' => 'Default OG Image',
                'created' => $now,
                'modified' => $now,
            ],
        ];
        GlobalSeed::truncateTable($this, 'static_contents', $data);
    }
}
