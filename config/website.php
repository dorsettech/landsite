<?php
/**
 * @author Dawid Katarzynski <dawid.katarzynski@econnect4u.pl>
 * @date (2018-11-06)
 * @version 1.0
 */
return [
    'Website'        => [
        'admin_prefix'           => 'panel.ls', // add prefix if needed
        'admin_panel'            => 'Dashboard', // default controller after login to panel
        // When going live:
        'service'                => 'The Landsite', // change it to client's company name panel
        'copy'                   => 'The Landsite', // change it to client's company name
        'server_name_production' => 'thelandsite.co.uk', // change it to live domain
        'email'                  => 'marcin+thelandsite@econnect4u.pl',           // change it to client email
	'url'                    => 'https://www.thelandsite.co.uk/', // change it to live domain
	'members_url'            => 'https://members.thelandsite.co.uk/', // change it to live domain

    ],
    'Domain'         => [
        'root'       => 'thelandsite.co.uk',
        'subdomains' => [
            'members' => 'members'
        ]
    ],
    'Logger'         => [
        'password' => 'sMRCwXIyetnjKXB'
    ],
    'Session'        => [
        'cookie'   => 'CMS5',
        'defaults' => 'php',
        'timeout'  => 30                                                        // Session timeout in minutes
    ],
    'MailSmtp'       => [// Email configuration
        'host'     => 'email-smtp.eu-west-1.amazonaws.com', // use separate Sengrid account for each website
        'port'     => 587,
        'username' => 'AKIARJ5VLREQPGPZYTG4',
        'password' => 'BO4tQzKofabIF23kTK+1NAsks/D5t6nv2cufVLStfs18',
        'tls'      => true,
        'from'     => ['noreply@thelandsite.co.uk' => 'The Landsite']    // change it to proper email address and name as it's default email which will be used for email delivering
    ],
    'Databases'      => [// Database configuration
        'default' => [
            'host'     => 'localhost',
            'username' => 'prod_thelands_usr',
            'password' => '7pRjs8\?xA9B',
            'database' => 'prod_thelands_db'
        ]
    ],
    'SeoAnalyzer'    => [
        'limit' => [
            'urlname'         => [
                'min' => 1,
                'max' => 70
            ],
            'seo_title'       => [
                'min' => 2,
                'max' => 70
            ],
            'seo_keywords'    => [
                'min' => 5,
                'max' => 100
            ],
            'seo_description' => [
                'min' => 5,
                'max' => 300
            ],
            'og_title'        => [
                'min' => 2,
                'max' => 70
            ],
            'og_image'        => [
                'min' => 5
            ],
            'og_description'  => [
                'min' => 5,
                'max' => 300
            ]
        ]
    ],
    'Media'          => [
        'max_image_width'  => 1920,
        'max_image_height' => 1920,
        'max_image_size'   => 10 * 1024 * 1024,
        'max_doc_size'     => 20 * 1024 * 1024,
        'max_default_size' => 5 * 1024 * 1024,
        'mime_types_allowed' => [
            'image' => [
                'image/jpeg',
                'image/png',
                'image/webp'
            ],
            'document' => [
                'application/msword',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'application/pdf'
            ]
        ],
        'upload_tmp_prefix' => 'tmp-'
    ],
    'ServerSettings' => [
        'memory_limit'        => '256M',
        'upload_max_filesize' => '50M',
        'post_max_size'       => '50M',
        'max_execution_time'  => 30,
        'max_input_time'      => 60,
        'max_input_vars'      => 1000
    ],
    'Token'          => [
        'days_valid' => 14,
    ]
];
