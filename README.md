# CakePHP Application Skeleton

[![Build Status](https://img.shields.io/travis/cakephp/app/master.svg?style=flat-square)](https://travis-ci.org/cakephp/app)
[![License](https://img.shields.io/packagist/l/cakephp/app.svg?style=flat-square)](https://packagist.org/packages/cakephp/app)

A skeleton for creating applications with [CakePHP](http://cakephp.org) 3.x.

The framework source code can be found here: [cakephp/cakephp](https://github.com/cakephp/cakephp).

## Installation

1. Download [Composer](http://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Use: `git clone YOUR PATH TO REPOSITORY`

## Configuration

Read and edit `config/app.php` and setup the 'Datasources' in `config/website.php` and any other
configuration relevant for your application.

## DB Migrations
This will create database schema and apply all the migrations with seeds:
```bash
bin/cake CmsInit
```

Migrations apply after initial:
```bash
bin/cake migrations migrate
```
Revert last migration:
```bash
bin/cake migrations rollback
```

## Seed regenerator
Optional tool, seed regenerator.
Regenerates all seed files based on files already in config/Seeds directory.
If you want to provide custom seed template, then TEMPORARILY replace file
*/vendor/cakephp/migrations/src/Template/Bake/Seed/seed.ctp*
with the one from *src/Template/Bake/Seed/seed.ctp* - please make sure to backup
the original one and restore it after finished baking the seeder files

@todo: Force CakePHP to actually read this template instead of asking developer
to do these evil shenanigans in file system via FTP / SFTP.
```bash
bin/cake RegenerateSeeds
```

## NPM init

NPM init will install all necessary node modules which will be needed for compiling assets (SCSS, JS and images).

Execute below while in main folder application
```npm install``` 

Front end source files can be located in `src` folder, while panel source files should be placed under `src/panel`

After that Gulp compiler can be executed:
```
gulp
gulp frontend
gulp all
```
