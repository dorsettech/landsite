<?php
/*
 * BlueFrontier Helper(?)
 *
 */
namespace BlueFrontier;

\BlueFrontier\Helper::init();
class Helper
{
	
	const GROUP_GUEST          = 1;
	const GROUP_USER           = 2;
	const GROUP_MODERATOR      = 3;
	const GROUP_ADMINISTRATOR  = 4;
	const GROUP_ROOT           = 5;
	const GROUP_MEMBERS        = 6;
	
    private static $_config;
	
    public static function init()
    {
		self::$_config =  require(__DIR__  . '/config/website.php');
    }
	
	
	/************************************************************************************
	 ************************************************************************************
	 *
	 *    Database Connection
	 *
	 ************************************************************************************
	 ************************************************************************************/
	public static function dbConnection()
	{
		if (!self::$_config)
			self::init();
		
		$db = self::$_config['Databases']['default'];
		$db_conn     = @mysqli_connect($db['host'], $db['username'], $db['password'], $db['database']);
		if (!$db_conn || $db_conn->connect_error)
			return null;
		
		$db_conn->set_charset('utf8');	
		
		return $db_conn;
	}
	
	
	/************************************************************************************
	 ************************************************************************************
	 *
	 *    Unsubscribe Email From Newsletter
	 *
	 ************************************************************************************
	 ************************************************************************************/
	public static function unsubscribeEmail($email)
	{
		$db_conn = self::dbConnection();
		if ($db_conn)
		{
			// Update user preferences
			$sql = "UPDATE `users`
					   SET `email_queue_optin` = 0 
					 WHERE `email` = '" . mysqli_real_escape_string($db_conn, $email) . "'
					";
			mysqli_query($db_conn, $sql);
			
			// Delete users email from queue
			$sql = "DELETE FROM `email_queue_members`
					 WHERE `email` = '" . mysqli_real_escape_string($db_conn, $email) . "'
					";
			mysqli_query($db_conn, $sql);
		}
	}
	
	
	/************************************************************************************
	 ************************************************************************************
	 *
	 *    Members To Newsletter
	 *
	 ************************************************************************************
	 ************************************************************************************/
	public static function membersToNewsletter()
	{
		$db_conn = self::dbConnection();
		if ($db_conn)
		{
			$data = [];
			
			/*
			 * Email List Preferences
			 */
			/*
			 * Email List Types
			 */
			$email_types = [];
			$sql = "SELECT * FROM `email_lists`
					 ORDER BY `id` ASC
					";
			$emails = @mysqli_query($db_conn, $sql);
			if ($emails)
			{
				while ($email = $emails->fetch_assoc())
				{
					$email_types[] = $email;
				}
			}

			$email_pref = [
							'buying'         => true,
							'selling'        => true,
							'professionals'  => true,
							'insights'       => true,
						 ];
			
			
			/*
			 * Users
			 */
			$sql = "SELECT * FROM `users` AS `u` 
						INNER JOIN `user_details` AS `ud` ON (`ud`.`user_id` = `u`.`id`) 
					WHERE `u`.`email_queue_imported` = 0 
					  AND `u`.`email_queue_optin` = 1 
					  AND `u`.`group_id` = " . self::GROUP_MEMBERS . " 
					  AND `u`.`email_verified` = 1 
					  AND `u`.`active` = 1 
					  AND `u`.`deleted` = 0 
					";
			$users = @mysqli_query($db_conn, $sql);
			if ($users)
			{
				// - build email data
				while ($user = $users->fetch_assoc())
				{
					foreach ($email_types as $email_type)
					{
						$_email_active   = boolval($email_type['active']);
						$_email_pref_key = $email_type['pref_key'];
						$_email_name     = $email_type['name'];
						
						if ($_email_active)
						{
							
							if (isset($user[ $_email_pref_key ]) && boolval($user[ $_email_pref_key ]) === true)
							{
								//
								$toAdd = [
											'first_name' => $user['first_name'],
											'last_name'  => $user['last_name'],
											'email'      => $user['email'],
											'email_type' => $_email_name,
										 ];
								if ($_email_name == 'INSIGHTS')
									$toAdd['options']  = $user['pref_insights_list'];
								
								//
								$data[] = $toAdd;
							}
							
						}
						
					}
					
				}
				// - make as added
				$sql = "UPDATE `users`
						   SET `email_queue_imported` = 1 
						 WHERE `email_queue_imported` = 0
						   AND `email_queue_optin` = 1 
						";
				mysqli_query($db_conn, $sql);
			}
			
			/*
			 * Service Contacts
			 */
			$sql = "SELECT * FROM `service_contacts`
					WHERE `email_queue_imported` = 0 
					  AND `email_queue_optin` = 1 
					";
			$contacts = @mysqli_query($db_conn, $sql);
			if ($contacts)
			{
				// - build email data
				while ($contact = $contacts->fetch_assoc())
				{
					
					foreach ($email_types as $email_type)
					{
						$_email_active = boolval($email_type['active']);
						$_email_name   = $email_type['name'];
						
						if ($_email_active)
						{
							//
							$toAdd = [
										'first_name' => $contact['name'],
										'email'      => $contact['email'],
										'email_type' => $_email_name,
									 ];
							if ($_email_name == 'INSIGHTS')
								$toAdd['options']  = 'ALL';
								
							//
							$data[] = $toAdd;
						}
						
					}
					
				}
				// - make as added
				$sql = "UPDATE `service_contacts`
						   SET `email_queue_imported` = 1 
						 WHERE `email_queue_imported` = 0
						   AND `email_queue_optin` = 1 
						";
				mysqli_query($db_conn, $sql);
			}
			
			/*
			 * Insert into Database
			 */
			// - report
			print(count($data).' records found.' . PHP_EOL);
			// - save
			foreach ($data as $dat)
			{
				// - build insert sql
				$fields = array_keys($dat);
				$values = array_values($dat);
				foreach($values as &$val)
					$val = mysqli_real_escape_string($db_conn, $val);
				// - execute insert sql
				$sql = "INSERT INTO `email_queue_members`
						(`" . implode('`, `', $fields) . "`)
						VALUES ('" . implode("', '", $values) . "')
						";
				mysqli_query($db_conn, $sql);
			}
			// - report
			print('Saved.' . PHP_EOL);
			print('Everything went well.' . PHP_EOL);
		}
	}
	
	
}

